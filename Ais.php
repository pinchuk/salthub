<?php
/**
 * Class Ais
 * @property Vessel $vessel
 * @property Boat $boat
 * @property TimeMark $TimeMark
 */
class Ais
{
    public $url = 'http://data.aishub.net/xml.php?username=AH_2071_BED03BFA&arch=0&human=1';
    public $data;
    public $vessels;
    public $vessel;
    public $boats;
    public $boat;
    public $API;
    public $rootDirectory = '/var/www/salthub.com/public.html';
    public $TimeMark;

    public function reset()
    {
        $this->vessel   = false;
        $this->boat     = false;
    }
    public function initData()
    {
        $xml_data       = file_get_contents($this->url);
        file_put_contents( "/tmp/ais", $xml_data );
        $this->data     = xml2array( "/tmp/ais" );
        $this->vessels  = $this->data['VESSELS']['vessel'];
    }

    public function initDataLocal()
    {
        ini_set('display_errors', 1);
        $nameDataFile = $this->rootDirectory."/data.xml";
        $this->data     = xml2array( $nameDataFile );
        $this->vessels  = $this->data['VESSELS']['vessel'];
    }

    public function import()
    {                                                           // This part is only debugging            
                                                                $this->TimeMark = new TimeMark();
                                                      //        $this->TimeMark->toFile = true;
                                                                $this->TimeMark->rootDirectory = $this->rootDirectory;
                                                                $this->TimeMark->start();
                                                                $this->TimeMark->setMark('start');
                                                                ini_set('display_errors', 1);
        $this->initData();
                                                                $this->TimeMark->setMark('initData');

                                                                $for_time = new TimeMark();
                                                                $for_time->microtime = true;
        
        for( $c = 0; $c < (count( $this->vessels )); $c++ )
        { 
                                                                $a=array();
                                                                $for_time->start();
            $this->reset();
            if($this->setVessel($c) && $this->vessel->validation())
            {
                                                                $a[]=$for_time->setIntMark(1);
                if($this->setBoat())
                {
                                                                $a[]=$for_time->setIntMark(2);
                    $this->boat->update();
                                                                $a[]=$for_time->setIntMark(3);
                                        
                }
                else
                {
                                                                $a[]=$for_time->setIntMark(4);
                    $this->boat->insert();
                                                                $a[]=$for_time->setIntMark(5);
                }
                if(!$this->boat->error)
                {
                                                                $a[]=$for_time->setIntMark(6);
                    $this->pageActivity();
                                                                $a[]=$for_time->setIntMark('page');
                    $this->feedActivity();
                                                                $a[]=$for_time->setIntMark('feed');
                    $this->addToHistory();
                                                                $a[]=$for_time->setIntMark(9);
                }
            }
                                                                $a[]=$for_time->setIntMark('Item for '.$c);
                                                                
                                                                if($for_time->getIntTotalTime()>0.3)
                                                                {
                                                                    echo implode("\n",$a);
                                                                    $for_time->getTotalTime();    
                                                                }
        }
                                                                $this->TimeMark->setMark('for');
        $this->saveHistory();
                                                                $this->TimeMark->setMark('saveHistory');
        $this->finalUpdate();
                                                                $this->TimeMark->setMark('finalUpdate');
                                                                $this->TimeMark->getTotalTime();
    }

    public function finalUpdate()
    {
        //Select all boats which had contact and not received data (lost contact)
        $q = mysql_query("
            select gid
            from boats
            where
                ais_contact_state = 0 AND
                in_range = 1
        ");
        $this->TimeMark->setMark('finalUpdate:: select gid where ais_contact_state=0 and in_range=1');
        while( $r = mysql_fetch_array( $q ) )
        {
            //These are vessels that we've recently been in contact with, but aren't any more.
            $this->API->feedAdd( "VP4", 0, -1, -1, $r['gid'] );
            //$this->API->sendNotificationToPage( NOTIFY_VESSEL_OUT_OF_RANGE, array( "from" => $from = -1 , "gid" => $r['gid']) );
        }
        $this->TimeMark->setMark('finalUpdate:: while - feedAdd VP4');
        //Mark all boats which had contact and not received data (lost contact)
        mysql_query("
            update boats
            set in_range = 0
            where
                ais_contact_state = 0 AND
                in_range = 1
        ");
        $this->TimeMark->setMark('finalUpdate:: set in_range = 0 where ais_contact_state=0 and in_range=1');
        //Mark all boats which not had contact and receive data (get contact)
        mysql_query("
            update boats
            set in_range = 1
            where
                ais_contact_state = 1 AND
                in_range = 0
        ");
        $this->TimeMark->setMark('finalUpdate:: set in_range = 1 where ais_contact_state=1 and in_range=0');
        //Unmark all boats which recieved data
        mysql_query("
            update boats
            set ais_contact_state=0
            WHERE
              ais_contact_state=1
        ");
        $this->TimeMark->setMark('finalUpdate:: set ais_contact_state=0 where ais_contact_state=1');
        mysql_query( "update boats set y_cell=ROUND( (lat + 90) / 2.5 ), x_cell=ROUND( (lon + 180) / 2.5 )" );  //This query can be moved to a different script
    }
    public function getCnfForHistory()
    {
        $cnf = array(
            'id'            => 'idForHistory',
            'heading'       => 'heading',
            'lat'           => 'lat',
            'lon'           => 'lon',
            'last_update'   => 'last_update',
            'cog'           => 'cog',
            'sog'           => 'sog',
            'rot'           => 'rot',
            'navstat'       => 'navstat',
            'dest'          => 'destination',
            'eta'           => 'eta',
            'gid'           => 'gid'
        );
        foreach($this->vessel->marks as $mark=>$time)
        {
            $cnf[$mark] = $mark."_delete";
        }
        return $cnf;
    }
    public function addToHistory()
    {
        $columns_boats_history = array();

        if(count($this->boat->feedActivity)>0)
        {
            foreach($this->vessel->marks as $mark=>$time)
            {
                $mark = $mark."_delete";
                $this->boat->$mark = 0;
            }
        }

        foreach($this->getCnfForHistory() as $nameFieldOfoHistory=>$nameFieldOfBoat)
        {

            $columns_boats_history[$nameFieldOfoHistory] = $this->boat->$nameFieldOfBoat;
        }
        $this->insert_into_file($columns_boats_history);
    }
    public function saveHistory()
    {//toDo Add 3 column 
        $columns_boats_history = array();
        foreach($this->getCnfForHistory() as $nameFieldOfoHistory=>$nameFieldOfBoat)
        {
            $columns_boats_history[$nameFieldOfoHistory] = 0;
        }
// Load data into infobright
//        $this->load_into_infobright($columns_boats_history, 'boat' , false, 'ibright');
// And into standart DB
        $this->load_into_infobright($columns_boats_history);
    }

    public function setBoat()
    {
        $this->boat = new Boat($this->vessel);
        if($this->boat->init($this->boats))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    /*
     * Insert into file for InfoBright
     * @param : $columns(array) with list of values
     * @return: int|false
     */
    public function insert_into_file($columns = array(), $postfix = 'boat')
    {
        foreach ($columns as $key => $value) {
            $columns[$key] = '"'.$value.'"';
        }
        $content = implode("\t", $columns);
        $content .= "\n";
        $date = date('Y-m-d');
        $filename = '/tmp/salthub.boats_ais_history.' . $date . '.data_'.$postfix;
        return file_put_contents($filename, $content, FILE_APPEND);
    }
    /*
     * Loads collected data into database
     * 
     */
    public function load_into_infobright($columns, $postfix = 'boat' , $unlink = true, $database = 'dev', $table = 'boats_ais_history') 
    {  
        $column_keys = array_keys($columns);
        $column_names = implode(', ', $column_keys);
        $date = date('Y-m-d');
        $sql = "LOAD DATA LOCAL INFILE 
                    '/tmp/salthub.boats_ais_history.".$date.".data_".$postfix."' 
                INTO TABLE ".$table." 
                FIELDS TERMINATED BY '\t' 
                ENCLOSED BY '\"' 
                LINES TERMINATED BY '\n'
                    ";

        switch($database){
            case 'ibright':
                // Connect to Infobright instead of Standart DB Server    
                mysql_close();
                $ibright = mysql_connect("10.179.38.20:5029", "salt_loader","xcjjsd3j892aAAsd");
                mysql_select_db("test_salthub_tracking_data", $ibright);
                break;
        }

        mysql_query($sql);

        if(mysql_error()){
            echo mysql_error();
            echo "  in  $sql --";
        }else{
           echo ' Success loading ';
        }
        if($unlink){
            if(unlink('/tmp/salthub.boats_ais_history.'.$date.'.data_'.$postfix)){
                echo ' File deleted ';
           }           
        }    
        // Return to standart MySQL Server    
        if($database == 'ibright'){
            mysql_close($ibright);        
            include $this->rootDirectory."/inc/sql.php";
        }

    }

    public function setVessel($id)
    {
        $this->vessel = false;
        if(isset($this->vessels[$id."_attr"]))
        {
            $this->vessel               = new Vessel();
            $this->vessel->name         = isset($this->vessels[$id."_attr"]['NAME'])?addslashes( ucfirst( strtolower($this->vessels[$id."_attr"]['NAME']) ) ):null;
            $this->vessel->imo          = $this->vessels[$id."_attr"]['IMO'];
            $this->vessel->mmsi         = isset($this->vessels[$id."_attr"]['MMSI'])?$this->vessels[$id."_attr"]['MMSI']:null;
            $this->vessel->heading      = $this->vessels[$id."_attr"]['HEADING'];
            $this->vessel->lat          = $this->vessels[$id."_attr"]['LATITUDE'];
            $this->vessel->lon          = $this->vessels[$id."_attr"]['LONGITUDE'];
            $this->vessel->last_update  = $this->vessels[$id."_attr"]['TIME'];
            $this->vessel->marks        = MarkTimeForDeleteBoat::getMarks($this->vessel->last_update);
            $this->vessel->callsign     = $this->vessels[$id."_attr"]['CALLSIGN'];
            $this->vessel->cog          = $this->vessels[$id."_attr"]['COG'];
            $this->vessel->sog          = $this->vessels[$id."_attr"]['SOG'];
            $this->vessel->rot          = isset($this->vessels[$id."_attr"]['ROT'])?$this->vessels[$id."_attr"]['ROT']:null;
            $this->vessel->navstat      = $this->vessels[$id."_attr"]['NAVSTAT'];
            $this->vessel->type         = $this->vessels[$id."_attr"]['TYPE'];
            $this->vessel->A            = $this->vessels[$id."_attr"]['A'];
            $this->vessel->B            = $this->vessels[$id."_attr"]['B'];
            $this->vessel->C            = $this->vessels[$id."_attr"]['C'];
            $this->vessel->D            = $this->vessels[$id."_attr"]['D'];
            $this->vessel->length       = round( ($this->vessels[$id."_attr"]['A'] + $this->vessels[$id."_attr"]['B']) * 3.2808, 2 );
            $this->vessel->width        = round( ($this->vessels[$id."_attr"]['C'] + $this->vessels[$id."_attr"]['D']) * 3.2808, 2 );
            $this->vessel->dest         = addslashes( $this->vessels[$id."_attr"]['DEST'] );
            $this->vessel->eta          = addslashes( $this->vessels[$id."_attr"]['ETA'] );
            $this->vessel->draught      = addslashes( $this->vessels[$id."_attr"]['DRAUGHT'] );
        }
        return $this->vessel;
    }
    public function pageActivity()
    {
        foreach($this->boat->pageActivity as $method)
        {
            $method = 'pageActivity_'.$method;
            $this->$method();
        }
    }
    public function pageActivity_newType()
    {
        mysql_query("
            update pages
            set cat='".$this->boat->new_type."'
            where
                gid='".$this->boat->gid."' and
                cat=0
         ");
    }
    public function pageActivity_newName()
    {
        mysql_query("
            update pages
            set gname='".$this->boat->name."'
            where
                gid='".$this->boat->gid."'
            limit 1
         ");
    }
    public function pageActivity_newMMSI()
    {
        mysql_query("
            update pages
            set glink='".$this->boat->bid."'
            where
                gid='".$this->boat->gid."'
         ");
    }
    public function feedActivity()
    {
        if(navstatToStr($this->boat->navstat) !== navstatToStr($this->boat->oldNavstat))
        {
            $this->boat->feedActivity[] = 'changeNavstat';
        }
        foreach($this->boat->feedActivity as $method)
        {
            $method = 'feedActivity_'.$method;
            $this->$method();
        }

    }
    public function feedActivity_newName()
    {
        $this->API->feedAdd( "VP2", $this->boat->idForHistory, -1, -1, $this->boat->gid);
        $this->API->sendNotificationToPage( NOTIFY_VESSEL_NAME_CHANGE, array( "from" => -1 , "gid" => $this->boat->gid) );
    }
    public function feedActivity_changeMeasurements()
    {
        $this->API->feedAdd( "VP3", $this->boat->idForHistory, -1, -1, $this->boat->gid);
        $this->API->sendNotificationToPage( NOTIFY_VESSEL_SPEC_CHANGE, array( "from" => -1 , "gid" => $this->boat->gid) );
    }
    public function feedActivity_changeNavstat()
    {
        $this->API->feedAdd( "VP1", $this->boat->idForHistory, -1, -1, $this->boat->gid, $this->boat->last_update);
        $this->API->sendNotificationToPage( NOTIFY_VESSEL_NAVSTAT_CHANGE, array( "from" => -1 , "gid" => $this->boat->gid) );
    }

}
/**
 * Class Boat
 * @property Vessel $vessel
 */
class Boat
{
    public $last_update;
    public $new_type;
    public $oldNavstat;
    public $gid;
    public $name;
    public $idForHistory;
    public $bid;
    public $sett;
    public $vessel;
    public $error = false;
    public $pageActivity=array();
    public $feedActivity=array();

    public function __construct($vessel)
    {
        $this->vessel = &$vessel;
    }
    public function updateFormVessel()
    {
        $cnf = array(
            'imo'           => 'imo',
            'mmsi'          => 'mmsi',
            'callsign'      => 'callsign',
            'eta'           => 'eta',
            'cog'           => 'cog',
            'sog'           => 'sog',
            'last_update'   => 'last_update',
            'lat'           => 'lat',
            'lon'           => 'lon',
            'heading'       => 'heading',
            'rot'           => 'rot',
            'navstat'       => 'navstat',
            'destination'   => 'dest'
        );
        $this->oldNavstat = $this->navstat;
        foreach($cnf as $keyBoat=>$keyVessel)
        {
            $this->$keyBoat         = $this->vessel->$keyVessel;
            $this->sett[$keyBoat]   = $this->$keyBoat;
        }
    }
    public function insertFormVessel()
    {
        $cnf = array(
            'imo'           => 'imo',
            'mmsi'          => 'mmsi',
            'callsign'      => 'callsign',
            'breadth'       => 'width',
            'length'        => 'length',
            'eta'           => 'eta',
            'cog'           => 'cog',
            'sog'           => 'sog',
            'last_update'   => 'last_update',
            'lat'           => 'lat',
            'lon'           => 'lon',
            'heading'       => 'heading',
            'rot'           => 'rot',
            'navstat'       => 'navstat',
            'destination'   => 'dest',
            'name'          => 'name'
        );
        foreach($cnf as $keyBoat=>$keyVessel)
        {
            $this->$keyBoat         = $this->vessel->$keyVessel;
            $this->sett[$keyBoat]   = $this->$keyBoat;
        }
    }
    public function isChanged()
    {   // If we recieve the same status "Moored" or "Anchored" - deny update
        if(
            (navstatToStr($this->navstat) == navstatToStr($this->vessel->navstat)) && 
            (
              (navstatToStr($this->vessel->navstat) == "Anchored") ||
              (navstatToStr($this->vessel->navstat) == "Moored")
            )     
          )
        {
           return false; 
        }
        // If we recieve the same status "Underway" and coordinates/speed not change - deny update
        if
        (
            (navstatToStr($this->navstat)   == navstatToStr($this->vessel->navstat)) &&
            (round($this->lat, 4)           == round($this->vessel->lat, 4)) &&
            (round($this->lon, 4)           == round($this->vessel->lon, 4)) &&
            ($this->sog                     == $this->vessel->sog)
        )
        {
            return false;
        }
        // If status or coordinates/speed changed - allow update    
        return true;
        
    }
    public function getSett()
    {
        $result = array();
        foreach($this->sett as $name=>$val)
        {
            if($name=='rot' && $val=='')
            {
                $result[] =  $name."=0";
            }
            elseif($name=='last_update')
            {
                $result[] =  $name."='".str_replace(' GMT', '', $val)."'";
            }
            else
            {
                $result[] =  $name."='".$val."'";
            }

        }
        return implode(', ', $result);
    }
    public function getNameFieldForInsert()
    {
        $result = array();
        foreach($this->sett as $name=>$val)
        {
            $result[] =  $name;
        }
        return implode(', ', $result);
    }
    public function getValueFieldForInsert()
    {
        $result = array();
        foreach($this->sett as $name=>$val)
        {
            $result[] =  "'".$val."'";
        }
        return implode(', ', $result);
    }
    public function chengeName()
    {
        if( strtolower( addslashes( trim( $this->name ) ) ) != strtolower( $this->vessel->name ) )
        {
            $this->name             = $this->vessel->name;
            $this->sett['name']     = $this->name;
//            if( !preg_match_all('/'.$this->vessel->name.'/', $this->exnames, $m) )
            if(strpos($this->exnames, $this->vessel->name) === false)
            {
                $this->exnames          = $this->exnames.' '.$this->vessel->name;
                $this->sett['exnames']  = $this->exnames;
            }
            $this->pageActivity[] = 'newName';
            $this->feedActivity[] = 'newName';
        }
    }
    public function changeMeasurements()
    {
        if($this->breadth != $this->vessel->width || $this->length != $this->vessel->length)
        {
            $this->breadth          = $this->vessel->width;
            $this->sett['breadth']  = $this->breadth;
            $this->length           = $this->vessel->length;
            $this->sett['length']   = $this->length;
            $this->feedActivity[] = 'changeMeasurements';
        }
    }
    public function update()
    {
        $this->error = true;
        $this->sett = array(
            'ais_contact_state' => 1
        );        
        if(!$this->isChanged())
        {
            $this->updateDB();
            return;
        }
        $this->idForHistory = (int)(microtime(true)*10000);
        $this->new_type = $this->ais_to_salthub_vessel_type( $this->vessel->type );
        if( $this->new_type > -1 )
        {
            $this->pageActivity[] = 'newType';
        }
        $this->chengeName();
        $this->changeMeasurements();
        $this->updateFormVessel();
        if(!$this->updateDB())
        {
            return;
        }
        $this->error = false;
    }
    public function updateDB()
    {
        $sql = "
            UPDATE boats
            SET
                ".$this->getSett()."
            WHERE
                id='".$this->id."'
        ";
        mysql_query( $sql );
        if( mysql_error() )
        {
            return false;
        }
        return true;
    }
    public function insert()
    {
        $this->error = true;
        $this->idForHistory = (int)(microtime(true)*10000);
        mysql_query("
            INSERT INTO pages (gname, type, created)
            VALUES ( '".$this->vessel->name."', 1389, Now() )
        ");
        $this->gid = mysql_insert_id();
        $this->sett = array(
            'ais_contact_state' => 1,
            'gid'               => $this->gid
        );
        $this->insertFormVessel();
        $sql = "
            INSERT INTO boats ( ".$this->getNameFieldForInsert()." )
            VALUES (".$this->getValueFieldForInsert().")
        ";
        mysql_query($sql);
        $this->bid = mysql_insert_id();
        if( mysql_error() )
        {
            return;
        }
        $this->pageActivity[] = 'newMMSI';
        $this->error = false;
    }
    public function initMark()
    {
        $sett = array();
        $purOff = false;
        $insert = false;
        foreach($this->vessel->marks as $mark=>$newTime)
        {
            $markDelete = $mark.'_delete';
            if($purOff)
            {
                $this->$markDelete  = 1;
                continue;
            }
            $oldTime = $this->$mark;
//checking for the existence of boat - begin
            $sql_emp = "
                SELECT COUNT(*) as qty
                FROM boats_mtd
                WHERE boat_id = ".(int)$this->id."
            ";
            $res_emp = mysql_query($sql_emp);
            $is_empty = mysql_fetch_assoc($res_emp);
            if((int)$is_empty['qty'] == 0)
            {
                $insert=true;
            }
//checking for the existence of boat - end
            if((int)$oldTime == (int)$newTime)
            {
                $this->$markDelete = 1;
                $purOff            = true;
            }
            else
            {
                $this->$markDelete  = 0;
                $this->$mark        = $newTime;
                $sett[$mark]        = $newTime;
                
            }
        }
        if(count($sett) > 0)
        {
            if($insert)
            {
                $resultN = array('boat_id');
                $resultV = array($this->id);
                foreach($sett as $name=>$val)
                {
                    $resultN[] =  $name;
                    $resultV[] =  $val;
                }
                $sql = "
                    INSERT INTO boats_mtd ( ".implode(', ', $resultN)." )
                    VALUES (".implode(', ', $resultV).")
                ";
                mysql_query($sql);
            }
            else
            {
                $result = array();
                foreach($sett as $name=>$val)
                {
                    $result[] =  $name."=".$val;
                }
                $sett = implode(', ', $result);
                $sql = "
                    UPDATE boats_mtd
                    SET
                        ".$sett."
                    WHERE
                        boat_id=".$this->id."
                ";
                mysql_query($sql);
            }
        }
    }
    public function init($boats)
    {
        $this->boats = &$boats;
        if($this->initByMmsi() || $this->initByImo())
        {
            $this->initMark();
            return true;
        }
        return false;
    }
    public function initByMmsi()
    {
        if($this->vessel->mmsi > 150)
        {
            $marks = array();
            foreach($this->vessel->marks as $mark=>$time)
            {
                $marks[]='mark.'.$mark;
            }
            $q = mysql_query("
                SELECT
                    boats.*,
                    ".implode(', ', $marks)."
                FROM boats
                LEFT JOIN boats_mtd AS mark ON boats.id = mark.boat_id
                WHERE
                  boats.mmsi=".$this->vessel->mmsi."
            ");
            if( $boat = mysql_fetch_assoc( $q ) )
            {
                foreach($boat as $key=>$val)
                {
                    $this->$key = $val;
                }
                return true;
            }
        }
        return false;
    }
    public function initByImo()
    {
        if($this->vessel->imo > 150)
        {
            $marks = array();
            foreach($this->vessel->marks as $mark=>$time)
            {
                $marks[]='mark.'.$mark;
            }
            $q = sql_query("
                SELECT
                    boats.*,
                    ".implode(', ', $marks)."
                FROM boats
                LEFT JOIN boats_mtd AS mark ON boats.id = mark.boat_id
                WHERE
                  boats.imo=".$this->vessel->imo."
            ");
            if( $boat = mysql_fetch_assoc( $q ) )
            {
                foreach($boat as $key=>$val)
                {
                    $this->$key = $val;
                }
                return true;
            }
        }
        return false;
    }
    function ais_to_salthub_vessel_type( $ais )
    {
        $result = -1;
        if( $ais >= 20 && $ais <= 29 ) $result = 1228; //Wing in ground
        if( $ais == 30 ) $result = 1216; //Fishing
        if( $ais == 31 || $ais == 32 || $ais == 33 || $ais == 52 || $ais == 53 || $ais == 54 ) $result = 1226; //Work Boat
        if( $ais == 36 ) $result = 1222; //Sailing
        if( $ais == 34 || $ais == 50 ) $result = 1224; //Special craft
        if( $ais == 37 ) $result = 1221; //Yacht
        if( $ais >= 40 && $ais <= 49 ) $result = 1228; //HSLC
        if( $ais == 51 || $ais == 58 || $ais == 35 ) $result = 1223; //Search & Rescue
        if( $ais >= 70 && $ais <= 79 ) $result = 1215; //Cargo
        if( $ais >= 80 && $ais <= 89 ) $result = 1225; //Tanker
        if( $ais >= 90 && $ais <= 99 ) $result = 1227; //Unspecified
        return $result;
    }
}
class Vessel
{
    //LINK  - http://www.aishub.net/xml-description.html
    public $name; //vessel's name (max.20 chars)
    public $imo; //Navigational Status
    public $mmsi; //Maritime Mobile Service Identity
    public $heading; //current heading of the AIS vessel at the time of the last message; value in degrees, HEADING=511 means "not available"
    public $lat; //geographical latitude; AIS format - in 1/10000 minute i.e. degrees multiplied by 600000; Human readable format - degrees
    public $lon; //geographical longitude; AIS format - in 1/10000 minute i.e. degrees multiplied by 600000; Human readable format - degrees
    public $last_update;
    public $callsign; //vessel's callsign
    public $cog; //Course Over Ground; AIS format - in 1/10 degrees i.e. degrees multiplied by 10. COG=3600 means "not available"; Human readable format - degrees. COG=360.0 means "not available"
    public $sog; //Speed Over Ground; AIS format - in 1/10 knots i.e. knots multiplied by 10. SOG=1024 means "not available"; Human readable format - knots. SOG=102.4 means "not available"
    public $rot; //Rate of Turn
    public $navstat; //Navigational Status
    public $type; //vessel's type (more details LINK)
    public $length;
    public $width;
    public $dest; //vessel's destination
    public $eta; //estimated time of arrival; AIS format (see LINK).; Human readable format - UTC date/time
    public $A; //Dimension to Bow (meters)
    public $B; //Dimension to Stern (meters)
    public $C; //Dimension to Port (meters)
    public $D; //Dimension to Starboard (meters)
    public $draught; //AIS format - in 1/10 meters i.e. draught multiplied by 10.; Human readable format - meters
    public $time; //data timestamp; AIS format - unix timestamp; Human readable format - UTC
    public $pac; //Position Accuracy; 0 - low accuracy; 1 - high accuracy;
    public $device; //positioning device type (more details LINK)

    public $marks;

    public function validation()
    {
        // No name - exit
        if( trim( $this->name ) == "" )
        {
            return false;
        }
        // No MMSI - exit
        if($this->mmsi == "")
        {
            return false;
        }
        // Clean name or exit
        $this->cleanName();
        if(!$this->name)
        {
            return false;
        }
        return true;
    }
    public function getArrayForHystory()
    {
        $result = array();
        $conf = array(
            'name',
            'imo',
            'mmsi',
            'heading',
            'lat',
            'lon',
            'last_update',
            'callsign',
            'cog',
            'sog',
            'rot',
            'navstat',
            'type',
            'length',
            'width',
            'dest',
            'eta'
        );
        foreach($conf as $val)
        {
            $result[$val] = $this->$val;
        }
        return $result;
    }

    /**
     * Cleans the name from bad symbols after checks it
     */
    function cleanName()
    {
        if (!$this->isAllowed($this->name)) {
            $this->name = false;
        }
        $bad_list = array('"', '_');
        $replace_list = array('', ' ');
        $this->name = str_replace($bad_list, $replace_list, $this->name);
        $this->name = trim($this->name);
        if (empty($this->name)) {
            $this->name =  false;
        }

    }

    /* Checks is name correct
     * @return: true | false
     */
    function isAllowed()
    {
        $is_denied = strpbrk($this->name, "\\!@#$%^*+=<>?~`:;");
        return !$is_denied;
    }

}
class TimeMark
{
    public $startTime;
    public $totalStartTime;
    public $current;
    public $toFile = false;
    public $rootDirectory;
    public $microtime = false;
    
    public function getTime(){
        if($this->microtime){
            return microtime();
        }else{
            return time();
        }
        
    }

    public function start()
    {        
        $this->startTime = $this->getTime();
        $this->totalStartTime = $this->startTime;
    }
    public function setMark($mark)
    {
        $this->current = $this->getTime();
        $content = $mark.' ('.($this->current-$this->startTime).')';
        $this->logWriter($content);
        $this->startTime = $this->current;
    }
    public function setIntMark($mark)
    {
        $this->current = $this->getTime();
        $content = $mark.' ('.($this->current-$this->startTime).')';
        $this->startTime = $this->current;
        return $content;
    }
    public function getTotalTime()
    {
        $content = 'Total time ('.($this->current-$this->totalStartTime).')';
        $this->logWriter($content);
    }

    public function getIntTotalTime()
    {
        return $this->current - $this->totalStartTime;
    }    
    
    public function logWriter($content)
    {
        if($this->toFile)
        {
            file_put_contents($this->rootDirectory.'/logAis.log', $content, FILE_APPEND);;
        }
        else
        {
            echo $content."\n\r";
        }

    }

}
class MarkTimeForDeleteBoat
{
    // differnce between current and UTC time (in hours)
    public $time_diff = 4;
    
    // periods of data keeping(in hours)
    public $intervals = array(
        'interval_a' => 12, // half day - every 10 min
        'interval_b' => 72, // 3 days - every mark_a 
        'interval_c' => 168,// 7 days - every mark_b
        'interval_d' => 240// 10 days - every mark_c
    );
    
    //dimension in seconds
    //if you add new mark then you should add new mark to tables: boat_mtd and boats_ais_history
    public static $marks = array(
        'mark_a' => 3600,   //1 hour
        'mark_b' => 43200,  //12 hour
        'mark_c' => 86400   //1 day
    );
    
    static function getMarks($datetime)
    {
        $result = array();
        $time = strtotime($datetime);
        foreach(self::$marks as $name=>$val)
        {
            $result[$name] = floor(($time)/$val)*$val;
        }
        return (object)$result;
    }
    
    public function deleteUnnecessaryUpdates(){
        
        $now = "(NOW() - INTERVAL ".$this->time_diff." HOUR)";
        $del_sql = "DELETE
                    FROM boats_ais_history
                    WHERE (mark_a = 1
                             AND last_update < DATE_SUB(".$now.", INTERVAL ".$this->intervals['interval_a']." HOUR))
                         OR (mark_b = 1
                             AND last_update < DATE_SUB(".$now.", INTERVAL ".$this->intervals['interval_b']." HOUR))
                         OR (mark_c = 1
                             AND last_update < DATE_SUB(".$now.", INTERVAL ".$this->intervals['interval_c']." HOUR))
                         OR (last_update < DATE_SUB(".$now.", INTERVAL ".$this->intervals['interval_d']." HOUR))";
        
           mysql_query($del_sql);
        if (mysql_error()) {
            echo mysql_error();
            echo $del_sql;
            return false;
        } else {
            return true;
        }
       
    }

    public function deleteUnnecessaryFids(){
        
//        $now = "(NOW() - INTERVAL ".$this->time_diff." HOUR)";
//        
//        $del_sql = "DELETE
//                    FROM feed
//                    WHERE TYPE IN ('VP1', 'VP2', 'VP3', 'VP4')
//                             AND last_update < DATE_SUB(".$now.", INTERVAL ".$this->intervals['interval_d']." HOUR)
//                             AND last_update > 0";
        $del_sql = "DELETE
                    FROM feed
                    WHERE TYPE IN ('VP1', 'VP2', 'VP3', 'VP4')
                             AND ts < DATE_SUB(NOW(), INTERVAL ".$this->intervals['interval_d']." HOUR)";
        
           mysql_query($del_sql);
        if (mysql_error()) {
            echo mysql_error();
            echo $del_sql;
            return false;
        } else {
            return true;
        }
       
    }    
}