<?php

/* Additional functions by AppDragon */

/*
 * Loads collected data into database
 * 
 */
function load_into_infobright($columns, $postfix = 'boat' , $unlink = true, $database = 'dev', $table = 'boats_ais_history') {
    
    $column_keys = array_keys($columns);
    $column_names = implode(', ', $column_keys);
    $date = date('Y-m-d');
    
    $sql = "LOAD DATA LOCAL INFILE 
                '/tmp/salthub.boats_ais_history.".$date.".data_".$postfix."' 
            INTO TABLE ".$table." 
            FIELDS TERMINATED BY '\t' 
            ENCLOSED BY '\"' 
            LINES TERMINATED BY '\n'
            (".$column_names.");";
    
    switch($database){
        case 'ibright':
            // Connect to Infobright instead of Standart DB Server    
            mysql_close();
            $ibright = mysql_connect("10.179.38.20:5029", "salt_loader","xcjjsd3j892aAAsd");
            mysql_select_db("test_salthub_tracking_data", $ibright);
            break;
    }

    mysql_query($sql);
    
    if(mysql_error()){
        echo mysql_error();
        echo "  in  $sql --";
    }else{
       echo ' Success loading ';
    }
    if($unlink){
        if(unlink('/tmp/salthub.boats_ais_history.'.$date.'.data_'.$postfix)){
            echo ' File deleted ';
       }           
    }    
    // Return to standart MySQL Server    
    if($database == 'ibright'){
        mysql_close($ibright);        
        include $_SERVER['DOCUMENT_ROOT']."/inc/sql.php";
    }
 
}

/*
 * Insert into file for InfoBright
 * @param : $columns(array) with list of values
 * @return: int|false
 */

function insert_into_file($columns = array(), $postfix = 'boat') {
    foreach ($columns as $key => $value) {
        $columns[$key] = '"'.$value.'"';
    }
    
    $content = implode("\t", $columns);
    $content .= "\n";
    $date = date('Y-m-d');
    $filename = '/tmp/salthub.boats_ais_history.' . $date . '.data_'.$postfix;
    return file_put_contents($filename, $content, FILE_APPEND);
}

/* Checks is name correct
 * @param: $name(string)
 * @param: $charlist(string)
 * @return: true | false
 */

function isAllowed($name, $charlist = '!@#$%^*+=<>?~`:;') {
    $is_denied = strpbrk($name, $charlist);
    return !$is_denied;
}

/* Cleans the name from bad symbols after checks it
 * @param: $name(string)
 * @return: $clean_name(string) | false 
 */
function cleanName($name) {
    if (!isAllowed($name)) {
        return false;
    }
    $bad_list = array('"', '_');
    $replace_list = array('', ' ');
    $name = str_replace($bad_list, $replace_list, $name);
    $name = trim($name);
    if (!empty($name)) {
        return $name;
    } else {
        return false;
    }
}

/* Gets list of updates, that we should leave
 * @param: $gid(int) - boat/page id
 * @param: $days(float) - period of search in days(e.g. 3 days or 0.5 days)
 * @param: $table(string) - name of table
 * @return: $res_string(string) - list of ids
 */
function getNecessaryUpdates($gid, $days = 3, $table = 'boats_ais_history') {
    $format = 'Y-m-d H:i:s';
    $date_ts = time() - 3600 * 24 * $days;

    switch ($days) {        
    
        case 3:
            $date_ts_end = time() - 3600 * 24 * 0.5;
            $interval = 60;
            break;
        case 10:
            $date_ts_end = time() - 3600 * 24 * 3;
            $interval = 60 * 12;
            break;
        case 14:
            $date_ts_end = time() - 3600 * 24 * 10;
            $interval = 60 * 24;
            break;
        default:
            return false;
    }

    $date = date($format, $date_ts);
    $date_end = date($format, $date_ts_end);
    $res_string = '';

    // Check if Updates exists
    $check_sql =
            "SELECT COUNT(*) as qty FROM " . $table . " 
        WHERE gid = " . $gid . "
        AND last_update BETWEEN '" . $date . "'
        AND '" . $date_end . "'";    
    $check_result = mysql_query($check_sql);
    $num_updates = mysql_fetch_assoc($check_result);

    // If not exist - exit
    if($num_updates['qty'] == 0){
        return $res_string;
    }
    // if exist - continue
    while ($date_ts <= $date_ts_end) {
        $date = date($format, $date_ts);
        $sql =
                "SELECT id FROM " . $table . "
            WHERE 
                        gid = " . $gid . "
            AND last_update >= '" . $date . "'
            AND last_update < DATE_ADD('" . $date . "', INTERVAL " . $interval . " MINUTE)
        LIMIT 1";
        $date_ts = $date_ts + 60 * $interval;

        $result = mysql_query($sql);
        if (mysql_error()) {
            echo mysql_error();
            echo $sql;
            return false;
        } else {
            while ($r = mysql_fetch_assoc($result)) {
                $res_string .= $r['id'] . ',';
            }
        }
    }
    $res_string = rtrim($res_string, ',');
    return $res_string;
}

/* Delete updates, that not in necessary list
 * @param: $gid(int) - boat/page id
 * @param: $days(float) - period of search in days(e.g. 3 days or 0.5 days)
 * @param: $table(string) - name of table
 * @return: (bool) 
 */

function deleteUnnecessaryUpdates($gid, $days = 3, $table = 'boats_ais_history') {

    $format = 'Y-m-d H:i:s';
    $date_ts = time() - 3600 * 24 * $days;
    switch ($days) {
        case 3:
            $date_ts_end = time() - 3600 * 24 * 0.5;
            $interval = 60;
            break;
        case 10:
            $date_ts_end = time() - 3600 * 24 * 3;
            $interval = 60 * 12;
            break;
        case 14:
            $date_ts_end = time() - 3600 * 24 * 10;
            $interval = 60 * 24;
            break;
        default:
            return false;
    }

    $date = date($format, $date_ts);
    $date_end = date($format, $date_ts_end);

    $list_id = getNecessaryUpdates($gid, $days, $table);
    if (empty($list_id)) {
        return false;
    }
    $sql =
            "DELETE FROM " . $table . " 
        WHERE gid = " . $gid . "
        AND last_update BETWEEN '" . $date . "'
        AND '" . $date_end . "'    
        AND id NOT IN (" . $list_id . ")";

    mysql_query($sql);
    if (mysql_error()) {
        echo mysql_error();
        echo $sql;
        return false;
    } else {
        return true;
    }
}

/*
 * 
 */
function deleteOldUpdates($table = 'boats_ais_history'){

    $sql = "DELETE FROM " .$table. "
        WHERE last_update < (NOW() - INTERVAL 14 DAY)";   
    
    mysql_query($sql);
    
    if (mysql_error()) {
        echo mysql_error();
        echo $sql;
        return false;
    } else {
        return true;
    }
}

/*
 * 
 */
function thinOutUpdates($gid, $table = 'boats_ais_history') {
    deleteUnnecessaryUpdates($gid, 3, $table);
    deleteUnnecessaryUpdates($gid, 10, $table);
    deleteUnnecessaryUpdates($gid, 14, $table);
}

?>
