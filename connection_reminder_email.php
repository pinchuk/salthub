<?php
/*
Sends out notifications to users who have pending connections 7 days, 30 days, and 60 days after the initial request.

*/

date_default_timezone_set('UTC');

$_SERVER['DOCUMENT_ROOT'] = "/var/www/salthub.com/public_html";
include $_SERVER['DOCUMENT_ROOT'] . "/inc/inc.php";
error_reporting(ERROR_SET_1);

$site = "s";
$_SERVER['HTTP_HOST'] = "www." . $siteName . ".com";

$x = mysql_query("select notifications.nid,users.*, notifications.from_uid, notifications.ts from notifications
                    inner join users on users.uid=notifications.uid
                    where notifications.type='f' AND ( DATE(notifications.ts) = DATE_ADD( DATE(NOW()), INTERVAL -7 DAY ) OR DATE(notifications.ts) = DATE_ADD( DATE(NOW()), INTERVAL -30 DAY ) OR DATE(notifications.ts) = DATE_ADD( DATE(NOW()), INTERVAL -60 DAY ))");

while ($r = mysql_fetch_array($x, MYSQL_ASSOC))
{
  $to = $r['uid'];
  $from = $r['from_uid'];

  //Check to see if these people are connected currently.
  $isConnected = quickQuery( "select COUNT(*) from friends where (id1=$to AND id2=$from) OR (id1=$from AND id2=$to)" );

  if( $isConnected == 0 )
  {
    $API->uid = $r['uid'];

    foreach (array_keys($_SESSION) as $k)
    	unset($_SESSION[$k]);

    foreach ($r as $k => $v)
    	$_SESSION[$k] = $v;

  	$API->name = $_SESSION['name'];
	  $API->pic = $_SESSION['pic'];
  	$API->username = $_SESSION['username'];

    $from_name = quickQuery( "select name from users where uid='$from'" );

    if( $from_name != '' ) //Make sure this user still exists!
    {
      $subject = "Reminder about your invitation from $from_name";

      $month_and_day = date( "F j", strtotime( $r['ts'] ) );

	  $link = 'http://www.' . $siteName . '.com/notif_remind.php?nid=' . $r['nid'];
	  
      $body = "This is a reminder that on $month_and_day, <a href=\"http://www.salthub.com/user/_$from\">$from_name</a> sent you an invitation to become part of his or her professional network on SaltHub. <br /><br />";
      $body .= '[ORANGEBOX]<b>To connect, follow these links to ' . $from_name . '\'s log:<br><br><a href="' . $link . '">View log</a><br><br><a href="' . $link . '&amp;a=1">Complete connection</a></b>[/ORANGEBOX]<br />';
      $body .= "<br />About SaltHub.com: SaltHub is a network for the maritime industry that provides access to resources, vessels, media, news and tools. Login to SaltHub and connect with professionals, businesses and explore new opportunities.<br /><br />Thanks,<br />The SaltHub Team<br /></div>";

      if( $r['email'] != "" )
      {
	if ($argv[1])
		echo "Sending email to: " . $r['name'] . " (" . $r['email'] . ")\n";
	else
		emailAddress( $r['email'], $subject, $body );
      }
    }
  }
}

mysql_close();
?>
