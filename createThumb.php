<?
function createThumb( $width, $height, $crop, $photos, $ID, $hash, $temp )
{
  if( $photos )
    $file = "photos/$ID/$hash.jpg";                     //?
  else
    $file = "videos/$ID/$hash.jpg";                     //?

  $ext = substr(strtolower($file), -3);
  $thumb = array( $width, $height );

  if (substr($file, 0, 1) == "/")
  	$file = substr($file, 1);

  if (strpos($file, "../")) die('error 1');
  $x = split("/", $file);


  $parts = explode("/", $file);
  $parts[1] = getPathFromPhotoID($parts[1]);
  $file = implode("/", $parts);

  $targetDir = dirname($file) . "/thumbs/" . ($crop ? "" : "N") . $thumb[0] . "x" . $thumb[1];

  if (@filemtime($targetDir . "/" . basename($file)) > 1312206736)
  {
  	return $_SERVER["DOCUMENT_ROOT"] . "/" . $targetDir . "/" . basename($file);
  }

 	@mkdir($targetDir, 0777, true);

  $basename = basename($file);
  if (!file_exists($_SERVER["DOCUMENT_ROOT"] . "/" . $file))
  {
    echo "File not found for photo ID: " . $ID . "; $file\n";
    return "";
  }

  if (strlen($x[2]) == 15) //youtube request
  {
  	$file = "http://i.ytimg.com/vi/" . substr($x[2], 0, 11) . "/1.jpg";
  	//die($file);
  }

  if ($ext == "png")
  {
  	$img = imagecreatefrompng($_SERVER["DOCUMENT_ROOT"] . "/" . $file);
  }
  elseif ($ext == "gif")
  {
  	$img = imagecreatefromgif($_SERVER["DOCUMENT_ROOT"] . "/" . $file);
  }
  else
  {
  	$img = @imagecreatefromjpeg($_SERVER["DOCUMENT_ROOT"] . "/" . $file);
  	if (!$img)
  		$img = @imagecreatefrompng($_SERVER["DOCUMENT_ROOT"] . "/" . $file);
  	if (!$img)
  		$img = @imagecreatefromgif($_SERVER["DOCUMENT_ROOT"] . "/" . $file);

  }

  $width = imageSX($img);
  $height = imageSY($img);

  if (!$width || !$height) {
  	echo "ERROR:Invalid width or height";
  	exit(0);
  }

  // Build the thumbnail
  $result = scaleImage($width, $height, $thumb[0], $thumb[1], $crop);

  $target_width = $result[0];
  $target_height = $result[1];
  $widthOffset = $result[2];
  $heightOffset = $result[3];

  $new_img = ImageCreateTrueColor($target_width, $target_height);

  if (!@imagecopyresampled($new_img, $img, 0, 0, round($widthOffset / 2), round($heightOffset / 2), $target_width, $target_height, $width - $widthOffset, $height - $heightOffset)) {
  	echo "ERROR:Could not resize image\n";
  	return "";
  }

  $quality = 90;

	imagejpeg($new_img, $_SERVER["DOCUMENT_ROOT"] . "/" . $targetDir . "/" . $basename, $quality);
  imagedestroy($new_img);
  imagedestroy($img);

  return $_SERVER["DOCUMENT_ROOT"] . "/" . $targetDir . "/" . $basename;

}
?>
