CREATE TABLE
        `boats_mtd`
        (
                `id` INT(11) NOT NULL AUTO_INCREMENT,
                `mark_a` BIGINT(20) NULL,
                `mark_b` BIGINT(20) NULL,
                `mark_c` BIGINT(20) NULL,
                PRIMARY KEY(`id`)
        )
ENGINE = InnoDB;
ALTER TABLE `boats_mtd` ADD COLUMN `boat_id` INT(11) NOT NULL;
alter table `boats_ais_history` add column `mark_a` tinyint, add column `mark_b` tinyint, add column `mark_c` tinyint;