<?php

// this script cycles through each album in the database to look for main
// images that are private.  if a main image is private, it will find
// another image in the album that isn't private and set it as the main
// image.  if there are no non-private images available, it will make
// the current main image public.


include 'public_html/inc/sql.php';

$x = mysql_query('select albums.id as aid,mainimage,privacy from albums inner join photos on albums.mainimage=photos.id where privacy > 0');

if (mysql_num_rows($x) > 0)
	while ($y = mysql_fetch_array($x, MYSQL_ASSOC))
	{
		echo $y['aid'] . "\t";
		
		$z = mysql_query('select id from photos where aid=' . $y['aid'] . ' and privacy=0');
		
		if (mysql_num_rows($z) == 0) // no public images available
		{
			echo 'Setting ' . $y['mainimage'] . ' public';
			mysql_query('update photos set privacy=0 where id=' . $y['mainimage']);
		}
		else
		{
			$new = mysql_fetch_array($z, MYSQL_ASSOC);
			echo 'Setting ' . $new['id'] . ' as main image';
			mysql_query('update albums set mainimage=' . $new['id'] . ' where id=' . $y['aid']);
		}
		
		if (mysql_affected_rows() == 0)
			echo ' - NO UPDATE MADE!';
		
		echo "\n";
	}

?>