<?php
/*
This is the "welcome page", that shows up at /welcome .  Displays the user feed and various
media modules.

Change Log:
8/24/2012 - If the user doesn't have a sector or occupation selected, they are forwarded to /signup/update_profile.php

*/

include "inc/inc.php";

if( isset( $_SESSION['joingid'] ) )
{
  $_GET['joingid'] = $_SESSION['joingid'];
  unset( $_SESSION['joingid'] );
}

if( isset( $_GET['joingid'] ) )
{
  $url = $API->getPageURL( $_GET['joingid'] );
  header( "Location: " . $url );
  exit;
}

if( !$isDevServer ) // i.e., if this is the 'live server'...
{
  if( !empty( $_SERVER['HTTPS'] ) ) //We want to take people off of the secure site if they're still on it...
  {
    header( "Location: http://www." . $siteName . ".com/welcome" );
    exit;
  }
}

//If we're logged in, make sure that we're an active user.
if( $API->isLoggedIn() ) // Users who deactivated their account will have their account reactivated here.
{
  $active = quickQuery( "select active from users where uid='" . $API->uid . "' and sector>0 AND occupation != ''" );
  if( intval( $active ) != 1 )
  {
    sql_query( "update users set active='1' where uid='" . $API->uid . "'" );
    sql_query( "update photos set reported=0 where uid='" . $API->uid . "' and reported=2" ); //Inactive users will have their content marked as reported to prevent them from showing in search results.
    sql_query( "update videos set reported=0 where uid='" . $API->uid . "' and reported=2" );
  }
}

if( empty( $_COOKIE['skipfbconnect'] ) )
{
  if( !$API->fbValidateSession() )
    header( "Location: /facebook_error.php" );
}

$API->requireLogin();

$scripts[] = "/gettrending.js.php?l1=4&l2=4";
$scripts[] = "/profile/profile.js";
$scripts[] = "/likeus.js.php";
$scripts[] = "/comments.js";

//Support for "My Vessels"
$css[] = "/vessels/vessel_map.css";
$scripts[] = "https://maps.googleapis.com/maps/api/js?sensor=false&libraries=visualization";
$scripts[] = "/vessels/vessel_my_vessels_map.js.php?zoom=1&vessel_forward=1";

include "header.php";
include "profile/getlogentries.php";
include "inc/mod_comments.php";

$mediaCount = $API->getMediaCount();


?>

<div class="bigtext" style="color: #555; width:750px;">
	<div style="float:left;">Welcome to <?=$siteName?>, <?=$API->name?>!</div>
<?
  //If this is not a company, add "My Pages" drop down here.
  //if(quickQuery( "select count(*) from page_members where uid='" . $API->uid . "' and admin=1" ) > 0 )
  {
?>
    <div style="float:left; height:18px; font-size:9pt; padding:5px; margin-left:15px;">
      <div class="navdropcontain" onmouseover="javascript:showDropDown('dd-mypages');" onmouseout="javascript:hideDropDown('dd-mypages');" style="position:relative;">
    		<div id="dd-mypages" class="navdrop" style="width: 252px; top: 20px; left: 0px; font-weight:300;">
          <? include "./profile/my_pages.php"; ?>
        </div>
        <div style="float:left"><img src="/images/page.png" width="16" height="16" alt="" /></div>
        <div style="float:left; padding-left:3px; font-weight:300;"><?= current( explode( " ", $API->name ) ); ?>'s Pages</div>
        <div style="float:left; padding-left:3px; padding-top:7px;"><img src="/images/arrow_down.png" width="9" height="8" alt="" /></div>
      </div>
    </div>
<?
  }
?>
</div>

<div style="clear: both;"></div>

<div class="contentborder">

	<div style="float: left; padding-right: 25px; border-right: 2px solid #808080;">
<?
  include_once "inc/contacts.php";
?>
    <script language="javascript" type="text/javascript" src="/invite.js?rand=<?=rand()?>"></script>
    <script language="javascript" type="text/javascript">
    <!--
    var invites = 0;
    -->
    </script>

    <div style="width:445px; padding:5px; background-color:rgb(237,239,244); border:1px solid silver; margin-bottom:10px;">
      <div class="strong" style="margin: 5px 0 0 5px;">Find your contacts on <?=$siteName?></div>

      <div style="padding-left:5px;">
      <?php showContactsTop(); ?>
      </div>
    </div>

		<div id="divtb1container">
  		<div class="welcomebox" id="divtb1">
  			<div class="strong" style="padding: 0;">Get started!</div>
  			<script language="javascript" type="text/javascript">
  			<!--
    		tbLayout = 2;
  			currentLog = <?=$API->uid?>;
  			tbPlace();
  			//-->
  			</script>
  		</div>
 		</div>

		<div class="subhead" style="margin: 5px 0 -5px;">
			Activities
		</div>

    <?
    $filter = $_GET['f'];
    if( empty( $filter ) || $filter == 0 )
      if( $API->getToDoItemsCompleted() < 3 )
        $filter = 5;
      else
        $filter = 2;

    ?>
    <div class="logentryfilters" style="margin-left:0px; padding-left:0px;">
      <table width="100%">
        <tr>
          <!--<td><img src="/images/world.png" width="16" height="16" alt="" /></td><td nowrap><a href="/welcome?f=1"><? if( $filter == 1 ) echo "<b>"; ?>all<? if( $filter == 1 ) echo "</b>"; ?></a></td>-->
          <td><img style="margin-left:0px;" src="/images/world.png" width="16" height="16" alt="" /></td><td nowrap><a href="/welcome?f=1"><? if( $filter == 1 ) echo "<b>"; ?>news<? if( $filter == 1 ) echo "</b>"; ?></a></td>
          <td><img src="/images/blue_man.png" width="16" height="16" alt="" /></td><td nowrap><a href="/welcome?f=2"><? if( $filter == 2 ) echo "<b>"; ?>my connections<? if( $filter == 2 ) echo "</b>"; ?></a></td>
          <td><img src="/images/images.png" width="16" height="16" alt="" /></td><td nowrap><a href="/welcome?f=3"><? if( $filter == 3 ) echo "<b>"; ?>photo related<? if( $filter == 3 ) echo "</b>"; ?></a></td>
          <td><img src="/images/television.png" width="16" height="16" alt="" /></td><td nowrap><a href="/welcome?f=4"><? if( $filter == 4 ) echo "<b>"; ?>video related<? if( $filter == 4 ) echo "</b>"; ?></a></td>
          <td><img src="/images/check.png" width="16" height="16" alt="" /></td><td nowrap><a href="/welcome?f=5"><? if( $filter == 5 ) echo "<b>"; ?>to do<? if( $filter == 5 ) echo "</b>"; ?></a></td>
        </tr>
      </table>
    </div>

<? if( $filter < 5 ) { ?>
		<div class="strong" style="margin: 5px 0 0 5px;">Activity</div>

		<div class="trending" style="width: 431px; padding: 10px; overflow: hidden; margin-bottom:5px;">
			<?
      $sector = quickQuery( "select sector from users where uid='" . $API->uid . "'" );
   		echo '<div style="clear: both;" id="feeds">';
      switch( $filter )
      {
        case 1: $more_type = -8; $gleResult = getLogEntries(array("gid" => 0, "uid" => -8, "mini" => 0, "feedLimit" => 10, "cat" => $sector)); break;
        case 2: $more_type = -3; $gleResult = getLogEntries(array("gid" => 0, "uid" => -3, "mini" => 0, "feedLimit" => 10)); break;
        case 3: $more_type = -4; $gleResult = getLogEntries(array("gid" => 0, "uid" => -4, "mini" => 0, "feedLimit" => 10)); break;
        case 4: $more_type = -5; $gleResult = getLogEntries(array("gid" => 0, "uid" => -5, "mini" => 0, "feedLimit" => 10)); break;
        default: $more_type = -2; $gleResult = getLogEntries(array("gid" => 0, "uid" => -2, "mini" => 0, "feedLimit" => 10)); break;
      }
      echo '</div>';

  		if ($gleResult['more']) // there are more feed entries left
  		{

  		?>
  		<div id="feedshowmore" class="feedshowmore">
  			<a href="javascript:void(0);" onclick="javascript:showMoreLogEntries();">show more &#0133;</a>
  		</div>
      <?
      }
      ?>


			<div style="clear: both;"></div>
		</div>
<? } else if( $filter == 5 ) { ?>
		<div class="strong" style="margin: 5px 0 0 5px;">What to do first</div>

      <? include( "profile/user_todo_list.php" ); ?>
<? } ?>
	</div>


	<div style="width: 442px; float: right;">
		<div class="welcomebox">
			<div style="float: left;">
				<div class="strong" style="padding-bottom: 10px;">Profile snapshot</div>
			</div>
			<div style="float: right; font-size: 8pt; padding-top: 2px;">
<?
$r = queryArray( "select twid, fbid from users where uid='" . $API->uid . "'" );
?>

					<img src="/images/facebook.png" alt="" />&nbsp; <?= ($r['fbid'] > 0) ? '<a href="http://www.facebook.com/" target="_blank">view' : '<a href="/settings"><span style="color: #339900;">enable</span>'?></a> &nbsp; &nbsp;
					<img src="/images/twitter.png" alt="" />&nbsp; <?= ($r['twid'] > 0) ? '<a href="http://www.twitter.com/" target="_blank">view' : '<a href="javascript:void(0);" onclick="javascript:showEnableAcct(1);"><span style="color: #339900;">enable</span>'?></a>

			</div>
			<div style="clear: left; float: left; padding-right: 7px;">
				<a href="<?=$API->getProfileURL()?>"><img width="48" height="48" src="<?=$API->getThumbURL(1, 48, 48, $API->getUserPic())?>" alt="" /></a>
			</div>
			<div style="float: left; width: 375px; padding-bottom: 3px; border-bottom: 1px solid #c0c0c0;">
				<div style="font-size: 8pt;">
					Today's date:&nbsp; <?=date("F j, Y")?>
					<div style="float: right;">
						Member since:&nbsp; <?=date("F j, Y", strtotime( quickQuery( "select joined from users where uid='{$API->uid}'") ) )?>
					</div>
				</div>
			</div>
			<div class="stat"><a href="javascript:void(0);" onclick="javascript:showFriendsPopup(<?=$API->uid?>);">connections <span>(<?=$API->getFriendsCount()?>)</span></a></div>
			<div class="stat"><a href="<?=$API->getProfileURL()?>/videos">videos <span>(<?=$mediaCount[0]?>)</span></a></div>
			<div class="stat"><a href="<?=$API->getProfileURL()?>/photos">photos <span>(<?=$mediaCount[1]?>)</span></a></div>
			<div class="stat" style="border: 0;"><a href="/pages">pages <span>(<?=quickQuery("select count(*) from page_members where uid={$API->uid}")?>)</span></a></div>

			<div style="clear: both; padding: 10px 0;">
				<div style="width: 143px; float: left;">
					<a href="<?=$API->getProfileURL()?>/logbook"><img src="/images/book_open.png" alt="" />&nbsp; view my log book</a>
				</div>
				<div style="width: 143px; float: left; text-align: center;">
					<a href="<?=$API->getProfileURL()?>/about"><img src="/images/comment_edit.png" alt="" />&nbsp; enhance my profile</a>
				</div>
				<div style="float: right;">
					<a href="/settings"><img src="/images/application_edit.png" alt="" />&nbsp; account settings</a>
				</div>
			</div>
    	<div style="clear: both; height: 10px;"></div>

		</div>

<?
if( quickQuery( "select pic from users where uid='" . $API->uid . "'" ) == 0 )
{
?>
  <table cellpadding="5" cellspacing="0" align="center" bgcolor="#FFF9D7" style="border: 1px solid #E2C822; margin-top:10px; margin-bottom:10px;" width="430">
  <tr><td>
    <div style="padding: 9px 6px; font-weight: bold; font-size: 10pt; color: #000;">
      You currently don't have a profile photo selected. <a href="/settings">Selecting a profile photo</a> will help your contacts find and recognize you.
    </div>
  </td></tr>
  </table>
<?
}
?>

		<div class="subhead" style="margin: 5px 0 -5px;">
			Connect and Engage
		</div>

 		<?php
      $limit = 3;
      ob_start();
      include "get_suggestions.php";
      $data = ob_get_contents();
      ob_end_clean();
    ?>

    <?
    if( $num_results > 0 )
    {
    ?>
		<div class="strong" style="margin: 15px 0 0 5px;">Suggestions and Requests</div>

		<div style="border: 1px solid #d8dfea; padding-left: 5px; margin-top: 10px;">
    	<div id="suggestion_container" class="pymksmall">
        <? echo $data; ?>
    	</div>
			<div style="clear: both; padding: 10px; text-align: center;">
				<a href="javascript:void(0);" onclick="javascript:suggestionsShuffle();" style="font-size: 9pt; text-decoration: none;">show more &hellip;</a>
			</div>
    </div>
    <?
    }
    ?>

		<div class="strong" style="margin: 15px 0 0 5px;">People where you work, past and present</div>

    <div style="margin: 0px 0 0 0px; width:442px;">
      <? include "inc/people_where_you_work_interface.php"; ?>
    </div>


		<div class="strong" style="margin: 15px 0 0 5px;">People you may know</div>

		<div style="border: 1px solid #d8dfea; padding-left: 5px; margin-top: 10px;">
    	<div id="pymk_container0" class="pymksmall" style="height:118px">
    		<?php $limit = 6;  $_GET['like'] = $params['q']; //include "getpymk.php"; //now being loaded via javascript ?>
    	</div>
			<div style="clear: both; padding: 10px; text-align: center;">
				<a href="javascript:void(0);" onclick="javascript:pymkShuffle(0);" style="font-size: 9pt; text-decoration: none;">show more &hellip;</a>
			</div>
    </div>

   	<div style="clear: both; height: 10px;"></div>


		<div class="subhead" style="margin: 5px 0 5px;">
			Trending and Active on <?=$siteName?>
		</div>

    <div class="strong" style="margin: 15px 0 10px 5px;">My Vessels</div>
<?
// Changed by AppDragon
/*
$sql = "SELECT COUNT(*) FROM boats
        LEFT JOIN pages ON pages.gid=boats.gid
        LEFT JOIN page_members ON page_members.gid=boats.gid
        WHERE page_members.uid=" . $API->uid . " AND pages.type=" . PAGE_TYPE_VESSEL;
*/
$sql = "SELECT COUNT(*) FROM boats as boats
        LEFT JOIN pages ON pages.gid=boats.gid
        LEFT JOIN page_members ON page_members.gid=boats.gid
        WHERE page_members.uid=" . $API->uid . " AND pages.type=" . PAGE_TYPE_VESSEL;
if( quickQuery( $sql ) > 0 )
{
?>
    <div style="border: 1px solid #d8dfea; padding: 5px; margin-top: 10px;">
      <div style="width:430px; height: 284px;" id="map_canvas"></div>
      <div style="font-size:8pt;">Click to zoom, select a vessel for details or <a href="/vessels/vessel_my_vessels.php">view a larger map</a>.</div>
    </div>
<?
}
else
{
?>
    <div style="border: 1px solid #d8dfea; padding: 5px; margin-top: 10px;">
      <a href="/vessels"><img src="/images/my_vessels.jpg" width="429" height="284" alt="" /></a>
    </div>
<?
}
?>

		<div class="strong" style="margin: 15px 0 10px 5px;">Vessels</div>

		<div style="border: 1px solid #d8dfea; padding: 5px; margin-top: 10px;">
      <div id="VesselPreview">
<?
    $type = "V";
    include( 'get_trending_pages.php' );
?>
      </div>
  	<div style="clear: both; padding: 10px; text-align: center;">
  		<a href="javascript:void(0);" onclick="javascript:Shuffle( 'VesselPreview', 'get_trending_pages.php?type=V&page=' + VesselPage ); VesselPage++;" style="font-size: 9pt; text-decoration: none;">show more &hellip;</a>
  	</div>
    </div>

		<div class="strong" style="margin: 15px 0 10px 5px;">Pages</div>

		<div style="border: 1px solid #d8dfea; padding: 5px; margin-top: 10px;">
      <div id="PagesPreview">
<?
    $type = "";
    include( 'get_trending_pages.php' );
?>
      </div>
  	<div style="clear: both; padding: 10px; text-align: center;">
  		<a href="javascript:void(0);" onclick="javascript:Shuffle( 'PagesPreview', 'get_trending_pages.php?type=&page=' + PagesPage ); PagesPage++;" style="font-size: 9pt; text-decoration: none;">show more &hellip;</a>
  	</div>
    </div>

		<div class="strong" style="margin: 15px 0 0 5px;">Videos</div>

		<div style="border: 1px solid #d8dfea; padding: 5px; margin-top: 10px;">
      <div id="VideoPreview">
<?
    $type = "V";
    include( 'get_media_preview.php' );
?>
      </div>

			<div style="clear: both; padding: 10px; text-align: center;">
				<a href="javascript:void(0);" onclick="javascript:Shuffle( 'VideoPreview', 'get_media_preview.php?type=V&page=' + VidPage ); VidPage++;" style="font-size: 9pt; text-decoration: none;">show more &hellip;</a>
			</div>
    </div>

		<div class="strong" style="margin: 15px 0 10px 5px;">Photos</div>

		<div style="border: 1px solid #d8dfea; padding: 5px; margin-top: 10px;">
      <div id="PhotoPreview">
<?
    $type = "P";
    include( 'get_media_preview.php' );
?>
      </div>
  	<div style="clear: both; padding: 10px; text-align: center;">
  		<a href="javascript:void(0);" onclick="javascript:Shuffle( 'PhotoPreview', 'get_media_preview.php?type=P&page=' + PhotoPage ); PhotoPage++;" style="font-size: 9pt; text-decoration: none;">show more &hellip;</a>
  	</div>
    </div>
	</div>


	<div style="clear: both;"></div>
</div>



<div id="fb-root"></div>
<script language="javascript" type="text/javascript">
<!--

var lastCommentId = new Array();

var fidold = <?=intval($gleResult['fidold'])?>;
var fidnew = <?=intval($gleResult['fidnew'])?>;
var VidPage = 1;
var PhotoPage = 1;
var VesselPage = 1;
var PagesPage = 1;

function showNewLogEntries()
{
  if( disableWallPosts ) return;

	url = "/profile/getlogentries.php?gid=<?=0?>&uid=<? echo $more_type ?>&newerthan=" + fidnew;

	getAjax(url, function(data)
	{
    if( data == "" || data == null ) return;

		eval("json = " + data + ";");

		if (json.fidnew == null)
			return; //nothing new

		addFeed(json.html, true);

		fidnew = json.fidnew;
	});
}

function addFeed(html, top)
{
	newdiv = document.createElement("div");
	newdiv.innerHTML = html;
	newdiv.id = "newfeed" + new Date().getTime();

	e = document.getElementById("feeds");

  if( !e ) return;

	if (top)
		e.insertBefore(newdiv, e.firstChild);
	else
		e.appendChild(newdiv);

	h = getHeight(newdiv);
	slidedown(newdiv.id, h, true);
}

function showMoreLogEntries()
{
	url = "/profile/getlogentries.php?gid=<?=0?>&uid=<? echo $more_type ?>&olderthan=" + fidold;

	getAjax(url, function(data)
	{
		eval("json = " + data + ";");

		if (json.more == "0") //reached the end of the user's feed
			document.getElementById("feedshowmore").style.display = "none";
		else if (json.fidnew == null)
			return; //didn't return any results - user probably should not have gotten here

		addFeed(json.html, false);

		fidold = json.fidold;
	});
}

<?
if( empty( $_SESSION['from_admin'] ) )
{
  $showActivate = false;
  $showUpdateProfile = false;

  $user_data = queryArray( "select sector, occupation, verify from users where uid='" . $API->uid . "'" );

  if( $user_data['sector'] == 0 || $user_data['occupation'] == "" )
    $showUpdateProfile = true;

  if( $user_data['verify'] == 0 )
    $showActivate = true;

  if( $showActivate || $showUpdateProfile )
  {
  ?>
    getAjax( "/signup/newuser_popup.php?a=1", function( data ) {
      showPopUp("", data, 0, 0, true );
    } );
  <?
  }
  else
  {
    $firstLogin = quickQuery( "select first_login from users where uid='" . $API->uid . "'" );

    if( isset( $_GET['n'] ) || $firstLogin == 1 ) {
      if( $API->startFBSession() )
      {
  ?>
        showSite(0);
  <?
      }
    ?>
      showLikeUs();

      _gaq.push(['_trackEvent', 'visitor', 'signup']);
    <?
      mysql_query( "update users set first_login=0 where uid='" . $API->uid . "' limit 1" );
    }
  }
}
?>

function checkForm()
{
	valid = checkEmail(document.getElementById("email").value);

	if (valid)
		return true;
	else
	{
		alert( "Please enter a valid e-mail address." );
		return false;
	}
}

function UpdateProfileInformation()
{
  co = document.getElementById( 'company' );
  sec = document.getElementById( 'sector' );
  occ = document.getElementById('occupation');

  postAjax("/signup/newuser_updateProfile.php", "co=" + escape(co.value) + "&occ=" + escape(occ.value) + "&sector=" + selectedValue( sec ), function (data)
  {
    prodiv = document.getElementById( 'profilediv' );
    actdiv = document.getElementById( 'activatediv' );
    if( prodiv )
    {
      prodiv.style.display='none';
      if( !actdiv )
      {
        closePopUp();
        pymkShuffle(0);
        Shuffle( 'VesselPreview', 'get_trending_pages.php?type=V&amp;page=' + VesselPage );
      }
    }
  } );
}

function CheckProfileForm()
{
  co = document.getElementById( 'company' );
  sec = document.getElementById( 'sector' );
  occ = document.getElementById('occupation');
  pdb = document.getElementById('profiledonebutton' );

  enable = false;

  if( co.value != "" && selectedValue( sec ) != 0 && occ.value != "" )
    enable = true;

  pdb.style.display = ( enable ? "" : "none" );

}
/*AppDragon*/
setInterval("showNewLogEntries()", 10000);
pymkShuffle(0);

//-->
</script>



<?php
include "inc/embedmedia.php";

include "footer.php";


?>