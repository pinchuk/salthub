<?php
set_time_limit(35000);
ini_set('memory_limit', '512M');
$start_time = time();
error_reporting(0);

$_SERVER['DOCUMENT_ROOT'] = "/var/www/salthub.com/public_html";
include $_SERVER['DOCUMENT_ROOT'] . "/inc/inc.php";
include $_SERVER['DOCUMENT_ROOT'] . "/vessels/vessel_functions.php";
// Additional functions by AppDragon
require_once 'add_functions.inc.php';

$isDevServer = $argv[1];

sql_query( "update static set content='" . time()  . "' where content='0' and id='lock_ais_import'" );

/*
This ensures that only oen script is running.  If it has been 30 minutes since the last one, run again. (assume that something happened to the last run)
*/
if( mysql_affected_rows() == 0 )
{
  $lastRun = quickQuery( "select content from static where id='lock_ais_import'" );

  if( $lastRun > 0 )
  {
    if( time() - $lastRun > 1800 ) // are we 30 minutes from the last run?
    {
      sql_query( "update static set content='" . time()  . "' where id='lock_ais_import'" );
      $lastRun = 0;
    } // else fall through to the exit;
  }

  if( $lastRun != 0 )
  {
    echo "Another script already running!";
    //exit; //Another script is running currently
  }
}

$start_ais = time();

$site = "s";
$_SERVER['HTTP_HOST'] = "www." . $siteName . ".com";

sql_query("DELETE FROM ais_data");
//sql_query( "TRUNCATE TABLE ais_data" );

$url = "http://data.aishub.net/xml.php?username=AH_2071_BED03BFA&arch=0&human=1";
//http://www.aishub.net/xml-description.html
$xml_data = file_get_contents( $url );

file_put_contents( "/tmp/ais", $xml_data );

$data = xml2array( "/tmp/ais" );
$vessels = $data['VESSELS']['vessel'];

for( $c = 0; $c < (sizeof( $vessels ) / 2); $c++ )
{
  $name         = addslashes( ucfirst( strtolower($vessels[$c."_attr"]['NAME']) ) );
  $imo          = $vessels[$c."_attr"]['IMO'];
  $mmsi         = $vessels[$c."_attr"]['MMSI'];
  $heading      = $vessels[$c."_attr"]['HEADING'];
  $lat          = $vessels[$c."_attr"]['LATITUDE'];
  $lon          = $vessels[$c."_attr"]['LONGITUDE'];
  $last_update  = $vessels[$c."_attr"]['TIME'];
  $callsign     = $vessels[$c."_attr"]['CALLSIGN'];

  $cog          = $vessels[$c."_attr"]['COG'];
  $sog          = $vessels[$c."_attr"]['SOG'];
  $rot          = $vessels[$c."_attr"]['ROT'];
  $navstat      = $vessels[$c."_attr"]['NAVSTAT'];
  $type         = $vessels[$c."_attr"]['TYPE'];
  $length       = round( ($vessels[$c."_attr"]['A'] + $vessels[$c."_attr"]['B']) * 3.2808, 2 );
  $width        = round( ($vessels[$c."_attr"]['C'] + $vessels[$c."_attr"]['D']) * 3.2808, 2 );
  $dest         = addslashes( $vessels[$c."_attr"]['DEST'] );
  $eta          = addslashes( $vessels[$c."_attr"]['ETA'] );
  
 /*
  if( $imo != "" || $mmsi!="" || $name!="" ){
      
    $sql_ais = "insert into ais_data (name, imo, mmsi, heading, lat, lon, last_update, callsign, cog, sog, rot, navstat, type, length, width, dest, eta) values ('$name', '$imo', '$mmsi', '$heading', '$lat', '$lon', '$last_update','$callsign', '$cog', '$sog', '$rot', '$navstat', '$type', '$length', '$width', '$dest', '$eta')" ;      
    sql_query( $sql_ais);
    
  }
  if( mysql_error() ) {
   echo mysql_error();
   echo ' -- Error while importing AIS -- ';
   echo $sql_ais;
  }

  */
/* Changed by AppDragon*/
  // No name - exit
  if( trim( $name ) == "" ) continue;
  // No MMSI - exit
  if($mmsi == "") continue;
  // Clean name or exit
  $name = cleanName($name);
  if(!$name) continue;  
  
    $ais_columns = compact('name', 'imo', 'mmsi', 'heading', 'lat', 'lon', 'last_update', 'callsign', 'cog', 'sog', 'rot', 'navstat', 'type', 'length', 'width', 'dest', 'eta');
    if(insert_into_file($ais_columns, 'ais')){
        echo "AIS in file successfull";
        echo " -- ";     
    }
  
  
}

load_into_infobright($ais_columns, 'ais', true, 'dev', 'ais_data');


$end_ais = time();
$ais_exec = $end_ais - $start_ais;
//AIS data import complete


//Connect ais_data to BOATS tables

sql_query( "update ais_data, boats set ais_data.gid=boats.gid, ais_data.navstat_old=boats.navstat where boats.imo=ais_data.imo AND ais_data.imo>150" );
sql_query( "update ais_data, boats set ais_data.gid=boats.gid, ais_data.navstat_old=boats.navstat where boats.mmsi=ais_data.mmsi AND ais_data.mmsi>150" );


if ($argv[1]) echo "data imported<br/>";

sql_query( "FLUSH TABLES" );

$start_boats_history = time();
if ($argv[1]) echo "Update boat data for all boats that are connected to the AIS data now.<br />";
//Update boat data for all boats that are connected to the AIS data now.
$q = sql_query( "select * from ais_data where gid>0" );
while( $ais = mysql_fetch_array( $q ) )
{

  $gid          = $ais['gid'];
  $name         = addslashes( ucwords( strtolower( trim( $ais['name'] ) ) ) );
  $length       = floatval( $ais['length'] );
  $width        = floatval( $ais['width'] );
  $dest         = addslashes( $ais['dest'] );
  $eta          = addslashes( $ais['eta'] );
  $cog          = $ais['cog'];
  $sog          = $ais['sog'];
  $last_update  = $ais['last_update'];
  $lat          = $ais['lat'];
  $lon          = $ais['lon'];
  $heading      = $ais['heading'];
  $navstat      = $ais['navstat'];
  $navstat_old  = $ais['navstat_old'];
  $rot          = $ais['rot'];
  $callsign     = addslashes( $ais['callsign'] );
  $imo          = $ais['imo'];
  $mmsi         = $ais['mmsi'];
  $type         = $ais['type'];
  
/* Changed by AppDragon*/
  // No name - exit
  if( trim( $name ) == "" ) continue;
  // No MMSI - exit
  if($mmsi == "") continue;
  // Clean name or exit
  $name = cleanName($name);
  if(!$name) continue;  
  
  // Parameters doesn't changes - exit
  $check_sql = "SELECT navstat, sog, lat, lon FROM boats WHERE gid = $gid";
  $res_params = mysql_query($check_sql);
  $boat_params = mysql_fetch_assoc($res_params);
  if((navstatToStr($boat_params['navstat']) == navstatToStr($navstat)) && 
             (round($boat_params['lat'], 4) == round($lat, 4))         && 
             (round($boat_params['lon'], 4) == round($lon, 4))         &&
             ($boat_params['sog']           == $sog))                    {
      continue;
  }

  
  $sql = "update boats set ais_contact_state=1, imo='$imo', mmsi='$mmsi', callsign='$callsign', eta='$eta', cog='$cog', sog='$sog', last_update='$last_update', lat='$lat', lon='$lon', heading='$heading', rot='$rot' where gid='$gid'";
  sql_query( $sql );

  if( mysql_error() )
  {
    if ($argv[1]) echo $sql;
    echo mysql_error();
//    sql_query( "update static set content='0' where id='lock_ais_import'" );
//    exit;
  }
  else
    if ($argv[1]) echo "Updated $name, $imo, $mmsi<br />";
    
// OLD query - generating id
/*
  sql_query( "insert into boats_ais_history ( heading, lat, lon, last_update, cog, sog, rot, navstat, dest, eta, gid) values ('$heading', '$lat', '$lon', '$last_update', '$cog', '$sog', '$rot', '$navstat', '$dest', '$eta', '$gid')" );
  if( mysql_error() ) {
   echo mysql_error();
   echo ' -- Error while insert in boats_ais_history -- ';
  }                                                                   

  
  // Old link
  $link = mysql_insert_id();

  /*Save updates in file*/  
    // New Link   
    //Generate 14-digit id
    $id = (int)(microtime(true)*10000);
    $columns_boats_history = compact('id', 'heading', 'lat', 'lon', 'last_update', 'cog', 'sog', 'rot', 'navstat', 'dest', 'eta', 'gid');

  if(insert_into_file($columns_boats_history)){
      echo "Write in file successfull";
      echo " -- ";
  }     
  
  /*Thin out updates*/
//  thinOutUpdates($gid);

  $new_type = ais_to_salthub_vessel_type( $type );

  if( $new_type > -1 )
    sql_query( "update pages set cat='$new_type' where gid='$gid' and cat=0" );

  $oldName = quickQuery( "select name from boats where gid='$gid'" );
  if( strtolower( addslashes( trim( $oldName ) ) ) != strtolower( $name ) )
  {
    if( quickQuery( "select count(*) from boats where gid='$gid' and exnames LIKE '%$name%'" ) == 0 )
    {
      sql_query( "update boats set name='$name', exnames=CONCAT( exnames, ' $oldName' ) where gid='$gid'" );
    }
    else
    {
      sql_query( "update boats set name='$name' where gid='$gid'" );
    }

    sql_query( "update pages set gname='$name' where gid='$gid' limit 1" );

    //This is where you'd trigger an event indicating the name had change
    $API->feedAdd( "VP2", $id, -1, -1, $gid);
    $API->sendNotificationToPage( NOTIFY_VESSEL_NAME_CHANGE, array( "from" => $from = -1 , "gid" => $gid) );
  }

  sql_query( "update boats set navstat='$navstat', destination='$dest' where gid='$gid'" );

  if( mysql_affected_rows() > 0 )
  {
    if(navstatToStr($navstat) !== navstatToStr($navstat_old))
    {
    //Update to navstat
    $API->feedAdd( "VP1", $id, -1, -1, $gid);
    $API->sendNotificationToPage( NOTIFY_VESSEL_NAVSTAT_CHANGE, array( "from" => $from = -1 , "gid" => $gid) );
    }
  }

  if( $width > 0.0 || $length > 0.0 )
  {
    sql_query( "update boats set breadth='$width', length='$length' where gid='$gid' AND (breadth!='$width' OR length!='$length')" );
    if( mysql_affected_rows() > 0 )
    {
      //Update to specifications
      $API->feedAdd( "VP3", $id, -1, -1, $gid);
      $API->sendNotificationToPage( NOTIFY_VESSEL_SPEC_CHANGE, array( "from" => $from = -1 , "gid" => $gid) );
    }
  }
 
}

$end_boats_history = time();
$boats_history_exec = $end_boats_history - $start_boats_history;

$load_boats_history = time();
// Load data into infobright
load_into_infobright($columns_boats_history, 'boat' , false, 'ibright');
// And into standart DB
load_into_infobright($columns_boats_history);
$end_load_boats_history = time();

$load_history_exec = $end_load_boats_history - $load_boats_history;

$new_pages_start = time();
if ($argv[1]) echo "Add new pages from any of the AIS data that don't have gids assigned<br />";

//Add new pages from any of the AIS data that don't have gids assigned

$q = sql_query( "select * from ais_data where gid=0" );

while( $ais = mysql_fetch_array( $q ) )
{
  $ais_id = $ais['id'];

  $name = addslashes($ais['name']);
  $callsign = addslashes($ais['callsign']);
  $imo = $ais['imo'];
  $mmsi = $ais['mmsi'];

  $name         = addslashes( ucwords( strtolower( $ais['name'] ) ) );
  $length       = $ais['length'];
  $width        = $ais['width'];
  $dest         = addslashes( $ais['dest'] );
  $eta          = addslashes( $ais['eta'] );
  $cog          = $ais['cog'];
  $sog          = $ais['sog'];
  $last_update  = $ais['last_update'];
  $lat          = $ais['lat'];
  $lon          = $ais['lon'];
  $heading      = $ais['heading'];
  $navstat      = $ais['navstat'];
  $rot          = $ais['rot'];
  $callsign     = addslashes( $ais['callsign'] );

/* Changed by AppDragon
   if( $imo == "" && $mmsi == "" ) continue;
*/
  // No name - exit
  if( trim( $name ) == "" ) continue;
  // No MMSI - exit
  if($mmsi == "") continue;
  // Clean name or exit
  $name = cleanName($name);
  if(!$name) continue;
  
  // Check if current MMSI already exists 
  $mmsi_qty = 0; 
  if((int)$mmsi > 0){
      $res = sql_query("SELECT * FROM boats WHERE mmsi = '". $mmsi . "'");
      $mmsi_qty = mysql_num_rows($res);
  } 
  
  // MMSI exists - UPDATE boats and do not create new page
  if($mmsi_qty > 0){
      echo "\r\n -- UPDATE boats -- \r\n";
      $sql = "UPDATE boats SET ais_contact_state = '1', callsign = '". $callsign ."', imo = '". $imo ."', breadth = '". $width ."', length = '". $length ."', destination = '". $dest ."', eta = '". $eta ."', cog = '". $cog ."', sog = '". $sog ."', last_update = '". $last_update ."', lat = '" . $lat . "', lon = '". $lon ."', heading = '". $heading ."', navstat = '". $navstat ."', rot = '". $rot ."', name = '". $name ."'  WHERE mmsi = '". $mmsi . "'";
  }else{
  // new MMSI - INSERT into boats and create new page 
      echo "\r\n -- INSERT into boats AND pages -- \r\n";
      sql_query( "insert into pages (gname, type, created) values ( '$name',1389,Now() )" );
      $gid = mysql_insert_id();
      $sql = "insert into boats ( ais_contact_state, gid, callsign, imo, mmsi, breadth, length, destination, eta, cog, sog, last_update, lat, lon, heading, navstat, rot, name ) values (1, $gid, '$callsign', '$imo', '$mmsi', '$width', '$length', '$dest', '$eta', '$cog', '$sog', '$last_update', '$lat', '$lon', '$heading', '$navstat', '$rot', '$name')";
  }
  
  
  sql_query( $sql );

  $bid = mysql_insert_id();

  if( mysql_error() )
  {
    if ($argv[1]) echo $sql;
    echo mysql_error();
  }
  else
    if ($argv[1]) echo "Added $name, $imo, $mmsi<br />";

  if($mmsi_qty > 0){
      // existing MMSI         
      sql_query( "update pages set gname='$name' where gid='$gid'" );
  }else{
      // new MMSI         
      sql_query( "update pages set glink='$bid' where gid='$gid'" );
  }
  // Mark current vessel connected to AIS
  sql_query( "update ais_data set gid='$gid' where id='$ais_id' LIMIT 1" );      
    
 /*
  sql_query( "insert into boats_ais_history ( heading, lat, lon, last_update, cog, sog, rot, navstat, dest, eta, gid) values ('$heading', '$lat', '$lon', '$last_update', '$cog', '$sog', '$rot', '$navstat', '$dest', '$eta', '$gid')" );
  if( mysql_error() ) echo mysql_error();
 
  // Old link
  $link = mysql_insert_id();
  */
      // New Link   
    //Generate 14-digit id
    $id = (int)(microtime(true)*10000);
    $columns_boats_history = compact('id', 'heading', 'lat', 'lon', 'last_update', 'cog', 'sog', 'rot', 'navstat', 'dest', 'eta', 'gid');

    if(insert_into_file($columns_boats_history)){
        echo "Write in file successfull";
        echo " -- ";
    }    
  
    /*Thin out updates*/
 //   thinOutUpdates($gid);
}
$new_pages_end = time();
$new_pages_exec = $new_pages_end - $new_pages_start;

$load_pages = time();
// Load data into infobright
load_into_infobright($columns_boats_history, 'boat', false, 'ibright');
// And into standart DB
load_into_infobright($columns_boats_history);
$end_load_pages = time();

$load_pages_exec = $end_load_pages - $load_pages;
// Delete updates older than 2 weeks
//deleteOldUpdates();


//Search for vessels that are not out of range
//$q = sql_query( "select gid from boats where ais_contact_state=1 AND last_update<DATE_SUB(Now(), INTERVAL 2 HOUR)" );

//Select all boats which had contact and not received data (lost contact)
$q = sql_query("
    select gid
    from boats
    where
        ais_contact_state = 0 AND
        in_range = 1
");

$select_contact = time();
$select_exec = $select_contact - $end_load_pages; 

while( $r = mysql_fetch_array( $q ) )
{
  //These are vessels that we've recently been in contact with, but aren't any more.
  $API->feedAdd( "VP4", 0, -1, -1, $r['gid'] );
  //$API->sendNotificationToPage( NOTIFY_VESSEL_OUT_OF_RANGE, array( "from" => $from = -1 , "gid" => $r['gid']) );
}
$out_of_range = time();
$out_of_range_exec = $out_of_range - $select_contact;

//Mark the out of range vessels so that the "out of range" notifications don't keep coming up.
//quickQuery( "update boats set ais_contact_state=0 WHERE ais_contact_state=1 AND last_update<DATE_SUB(Now(), INTERVAL 2 HOUR)" );

//Mark all boats which had contact and not received data (lost contact)
quickQuery("
    update boats
    set in_range = 0
    where
        ais_contact_state = 0 AND
        in_range = 1
");

//Mark all boats which not had contact and receive data (get contact)
quickQuery("
    update boats
    set in_range = 1
    where
        ais_contact_state = 1 AND
        in_range = 0
");

//Unmark all boats which recieved data

quickQuery("
    update boats
    set ais_contact_state=0
    WHERE
      ais_contact_state=1
");


function ais_to_salthub_vessel_type( $ais )
{
  $result = -1;

  if( $ais >= 20 && $ais <= 29 ) $result = 1228; //Wing in ground
  if( $ais == 30 ) $result = 1216; //Fishing
  if( $ais == 31 || $ais == 32 || $ais == 33 || $ais == 52 || $ais == 53 || $ais == 54 ) $result = 1226; //Work Boat
  if( $ais == 36 ) $result = 1222; //Sailing
  if( $ais == 34 || $ais == 50 ) $result = 1224; //Special craft
  if( $ais == 37 ) $result = 1221; //Yacht
  if( $ais >= 40 && $ais <= 49 ) $result = 1228; //HSLC
  if( $ais == 51 || $ais == 58 || $ais == 35 ) $result = 1223; //Search & Rescue
  if( $ais >= 70 && $ais <= 79 ) $result = 1215; //Cargo
  if( $ais >= 80 && $ais <= 89 ) $result = 1225; //Tanker
  if( $ais >= 90 && $ais <= 99 ) $result = 1227; //Unspecified

  return $result;
}
$disconnect_ais = time();
$disconnect_exec = $disconnect_ais - $out_of_range;
// Changed by AppDragon
sql_query( "update boats set y_cell=ROUND( (lat + 90) / 2.5 ), x_cell=ROUND( (lon + 180) / 2.5 )" );  //This query can be moved to a different script

$setYcell = time();
$setYcell_exec = $setYcell - $disconnect_ais;
sql_query( "update static set content='0' where id='lock_ais_import'" );

sql_query( "FLUSH TABLES" );

mysql_close();

$end_import = time();
$import_exec = $end_import - $start_time; 


echo "Time report";
echo "\r\n";
echo "AIS time: $ais_exec ";
echo "\r\n";
echo "Boats_history_exec time: $boats_history_exec ";
echo "\r\n";
echo "Load_history_exec time: $load_history_exec ";
echo "\r\n";
echo "New_pages_exec time: $new_pages_exec ";
echo "\r\n";
echo "Load_pages_exec time: $load_pages_exec ";
echo "\r\n";
echo "Select exec: $select_exec ";
echo "\r\n";
echo "Out of range time: $out_of_range_exec ";
echo "\r\n";
echo "Disconnect time: $disconnect_exec ";
echo "\r\n";
echo "Set yCell time: $setYcell_exec ";
echo "\r\n";


?>

Done
