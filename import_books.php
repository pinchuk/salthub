<?
// #! /usr/bin/php

// /usr/bin/php import_books.php


$_SERVER["DOCUMENT_ROOT"] = "/home/mediabirdy.com/public_html";

$_SERVER['HTTP_HOST'] = SERVER_HOST;
$site = "s";

$fromEmail = true;


include $_SERVER["DOCUMENT_ROOT"] . "/inc/inc.php";
include $_SERVER["DOCUMENT_ROOT"] . "/upload/video.php";
include $_SERVER["DOCUMENT_ROOT"] . "/upload/photo.php";
include $_SERVER["DOCUMENT_ROOT"] . "/upload/newalbum.php";


function getBetween( $start, $end, $str )
{
  $s_pos = strpos( $str, $start );
  if( $s_pos === false ) return "";

  $s_pos += strlen( $start );

  $e_pos = strpos( $str, $end, $s_pos );
  if( $e_pos === false ) return "";

  return substr( $str, $s_pos, $e_pos - $s_pos );
}

$html = file_get_contents("http://www.bluewaterweb.com/nauticalbooks/default.asp");

$chunks = explode( "<nobr>", $html );

for( $c = 1; $c < sizeof( $chunks ); $c++ )
{
  $category_url = trim( getBetween( "<a href='https", "'>", $chunks[$c] ) );

  $html = file_get_contents('http' . $category_url);
  $chunks2 = explode( "<td width='70%' align='left' valign='top'><p>", $html );

  for( $d = 1; $d < sizeof( $chunks2 ); $d++ )
  {
    $ISBN_10 = trim( getBetween( "SKU=", "&", $chunks2[$d] ) );
    $book_name = trim( getBetween( "'>", "</a>", $chunks2[$d] ) );
    $book_url = trim( getBetween( "<a href='https", "'>", $chunks2[$d] ) );

    $book_name = str_replace( "*", "", $book_name );

    $html = file_get_contents('http' . $book_url);
    $chunks3 = explode( '<td width="100%" valign="top">', $html );

    $ISBN_13 = trim( getBetween( "ISBN:", "<br>", $chunks3[1] ) );
    $author = trim( getBetween( "Author:", "<br>", $chunks3[1] ) );
    $desc = trim( strip_tags( getBetween( "<br><br>", "</p>", $chunks3[1] ) ) );

    if( strlen( $author ) > 0 )
      $desc = "ISBN: " . $ISBN_10 . "\n\n" . $desc;

    if( strlen( $author ) > 0 )
      $desc = "Author: " . $author . "\n" . $desc;

    $img = trim( strip_tags( getBetween( '<br><img src="', '"', $chunks3[1] ) ) );

    if( $desc == "" ) continue;

    if( strlen( $img ) > 0 )
    {
      $img = str_replace( "https", "http", $img );
    }
    else
    {
      //Attempt the picture from Amazon?
      $html = file_get_contents('http://www.amazon.com/gp/product/images/' . $ISBN_10 . '/');
      $img = trim( getBetween( '<div id="imageViewerDiv"><img src="', '"', $html ) );
    }


    $book_name = addslashes( $book_name );
    $desc = addslashes( $desc );
    $author = addslashes( $author );

    if( quickQuery( "select count(*) from groups where gname='$book_name' and cat=105" ) > 0 ) continue;

    sql_query( "insert into books (ISBN10, author, name) values ('$ISBN_10', '$author', '$book_name')" );
    $bid = mysql_insert_id();

    //Select random person to create group
    $API->uid = quickQuery("select uid from users where password='dummy' order by rand()");

    $err = false;
    if( strlen( $img ) > 0 )
    {
      try
      {
        $contents = file_get_contents($img);
        $source = "/tmp/temp.jpg";
        file_put_contents($source, $contents);
      }
      catch( Exception $e)
      {
        echo "Error saving pic: $img \n";
        $err = true;
      }

    	if (!file_exists($source))
    	{
    		echo "!! Photo missing, skipping photo import\n";
        $err = true;
    	}
    }
    else
      $err = true;

    sql_query( "insert into groups (gname, cat, subcat, created, active, descr, pid ) values ('$book_name',105,0,NOW(), 1, '$desc', 0 )" );
    $gid = mysql_insert_id();

    if( !$err )
    {
    	$hash = md5(microtime());
  	  $pid = processPhoto();

      //Create album
      mysql_query("insert into albums (uid,title,descr) values ({$API->uid},{$gname},{$gname})");
      $aid = mysql_insert_id();

  		mysql_query("update photos set aid=$aid,privacy=0,cat=1,created='" . date("Y-m-d", time() - 86400 * rand(0, 365)) . "' where id=$pid");
      mysql_query("insert into group_media (type,id,gid,uid) values ('P',$pid,$gid,{$API->uid})");
      mysql_query("update albums set mainImage={$pid} where id={$aid}");
      mysql_query("update groups set pid={$pid} where gid={$gid}");

    }

    echo "Imported: $book_name - $ISBN_10 \n";
//    echo $ISBN_13 . ", " . $author . ", " . $desc . "\n";

  }
}


?>

Done
