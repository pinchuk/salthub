<?
// #! /usr/bin/php

///usr/bin/php import_schools.php


$_SERVER["DOCUMENT_ROOT"] = "/home/mediabirdy.com/public_html";

$_SERVER['HTTP_HOST'] = SERVER_HOST;
$site = "s";

$fromEmail = true;


include $_SERVER["DOCUMENT_ROOT"] . "/inc/inc.php";
include $_SERVER["DOCUMENT_ROOT"] . "/upload/video.php";
include $_SERVER["DOCUMENT_ROOT"] . "/upload/photo.php";
include $_SERVER["DOCUMENT_ROOT"] . "/upload/newalbum.php";

$html = file_get_contents("http://www.utexas.edu/world/univ/alpha/");

$chunks = explode( "<LI>", $html );

for( $c = 1; $c < sizeof( $chunks ); $c++ )
{
  $name_chunks = explode( 'HREF="', $chunks[$c] );
  $url = current( explode( '"', $name_chunks[1] ) );

  $school_name = strip_tags( $chunks[$c] );

  if( stristr( $school_name, " System " ) !== false )
    continue; //Skipping school "systems", as the campuses are listed seperately.

  $name_chunks = explode( "(", $school_name );

  if( sizeof( $name_chunks ) == 1 )
  {
    $state = "";
    $school_name = trim( $school_name );
  }
  else
  {
    $school_name = trim( $name_chunks[0] );
    $state = trim( current( explode( ")", $name_chunks[1] ) ) );
  }

  $school_name = str_replace( "&ndash;", "-", $school_name );
  $school_name = str_replace( "&amp;", "&", $school_name );

  if( $school_name == "" ) continue;

  $school_name = addslashes( $school_name );

  if( quickQuery( "select count(*) from groups where gname='$school_name'" ) > 0 ) continue; //Don't import if we already have this school

  echo $school_name . ", " . $state . ", " . $url . "\n";

  $data = getWikipediaInfo( $school_name );

  if( isset( $data['descr'] ) )
    $desc = addslashes($data['descr']);
  else
    $desc = "";

  $gname = $school_name;
  $pid = 0;
  $err = false;

  //Select random person to create group
  $API->uid = quickQuery("select uid from users where password='dummy' order by rand()");

  if( isset( $data['img'] ) )
  {
    try
    {
      $contents = file_get_contents($data['img']);
      $source = "/tmp/temp.jpg";
      file_put_contents($source, $contents);
    }
    catch( Exception $e)
    {
      echo "Error saving pic: $url \n";
      $err = true;
    }

  	if (!file_exists($source))
  	{
  		echo "!! Photo missing, skipping photo import\n";
      $err = true;
  	}
  }
  else
    $err = true;

  sql_query( "insert into groups (gname, address, cat, subcat, www, created, active, descr ) values ('$gname','$state',113,0,'$url',NOW(), 1, '$desc' )" );
  $gid = mysql_insert_id();

  if( !$err )
  {
  	$hash = md5(microtime());
	  $pid = processPhoto();

    //Create album
    mysql_query("insert into albums (uid,title,descr) values ({$API->uid},{$gname},{$gname})");
    $aid = mysql_insert_id();

		mysql_query("update photos set aid=$aid,privacy=0,cat=1,created='" . date("Y-m-d", time() - 86400 * rand(0, 365)) . "' where id=$pid");
    mysql_query("insert into group_media (type,id,gid,uid) values ('P',$pid,$gid,{$API->uid})");
    mysql_query("update albums set mainImage={$pid} where id={$aid}");
    mysql_query("update groups set pid={$pid} where gid={$gid}");

  }
}


?>

Done
