#! /usr/local/bin/php
<?php

if ($argv[1] != "ok")
	die("Will cause data loss - not executing.\n");

$types = array(
			"Cargo" => array("img" => 50, "type" => 1215),
			"Fishing" => array("img" => 1211, "type" => 1216),
			"High Speed Craft" => array("img" => 52, "type" => 1217),
			"Other" => array("img" => 1214, "type" => 1218),
			"Passenger" => array("img" => 52, "type" => 1219),
			"Pleasure Craft" => array("img" => 53, "type" => 1221),
			"Sailing Vessel" => array("img" => 55, "type" => 1222),
			"Search & Rescue" => array("img" => 54, "type" => 1223),
			"Special Craft" => array("img" => 1214, "type" => 1224),
			"Tanker" => array("img" => 51, "type" => 1225),
			"Tug" => array("img" => 51, "type" => 1226),
			"Unspecified" => array("img" => 1214, "type" => 1227),
			"Wing in Grnd" => array("img" => 1213, "type" => 1228),
		);

$_SERVER["DOCUMENT_ROOT"] = "/home/mediabirdy.com/public_html";

$ft = 3.2808399; //feet per meter

$fromEmail = true;

$_SERVER['HTTP_HOST'] = SERVER_HOST;
$site = "s";
	
include $_SERVER["DOCUMENT_ROOT"] . "/inc/inc.php";
include $_SERVER["DOCUMENT_ROOT"] . "/upload/video.php";
include $_SERVER["DOCUMENT_ROOT"] . "/upload/photo.php";
include $_SERVER["DOCUMENT_ROOT"] . "/upload/newalbum.php";

//mysql_query("delete from albums where id > 94968");
//mysql_query("delete from photos where id > 44781");
//mysql_query("delete from groups where gid > 22075");
//mysql_query("delete from boats");

$path = "/mnt/media/boatdata";
$handle = opendir($path);

$last_mmsi = 0;

while (false !== ($file = readdir($handle)))
	if (strpos($file, "boats_json-") !== false)
		$files[] = $file;

sort($files);

foreach ($files as $file)
{	
	$data = json_decode(file_get_contents("$path/$file"), true);
	
	echo "\n-- Opened $file\n";
	
	foreach ($data as $mmsi => $boat)
	{
		if ($mmsi != $last_mmsi)
		{
			// new boat, add it to the db
			
			echo "\n";
			
			$type = $types[$boat['Ship Type']];
			
			if (empty($type['img']))
				$type = $types['Other'];
			
			$type_name = quickQuery("select catname from categories where cat={$type['type']}");
			$img_name = quickQuery("select catname from categories where cat={$type['img']}");
			
			$query['id'] = $mmsi;
			$query['name'] = $boat['name'];
			$query['shiptype'] = $type['type'];
			$query['year'] = $boat['Year Built'];
			$query['length'] = $boat['Length'] * $ft; // convert to feet
			$query['breadth'] = $boat['Breadth'] * $ft;
			$query['deadweight'] = $boat['DeadWeight'];
			$query['speedmax'] = $boat['SpeedMax'];
			$query['speedavg'] = $boat['SpeedAvg'];
			$query['flag'] = $boat['Flag'];
			$query['callsign'] = $boat['Call Sign'];
			$query['imo'] = $boat['IMO'];
			$query['draught'] = $boat['Draught'] * $ft;
			
			foreach ($query as $field => $value)
				$query[$field] = "'" . addslashes($value) . "'";
			
            // Changed by AppDragon
			//$q = "insert into boats (" . implode(",", array_keys($query)) . ") values (" . implode(",", $query) . ")";
			$q = "insert into boats (" . implode(",", array_keys($query)) . ") values (" . implode(",", $query) . ")";
			mysql_query($q);
			
			// create group
			
			if (mysql_affected_rows() == -1)
			{
				echo ">> Vessel already exists ($mmsi)\t{$query['name']} - {$type_name}\n";
				$gid = quickQuery("select gid from groups where glink={$mmsi} and gtype='B'");
			}
			else
			{
				echo ">> New vessel created ($mmsi)\t{$query['name']} - {$type_name}\n";
				$q = "insert into groups (gname,cat,gtype,glink) values ({$query['name']},112,'B',{$mmsi})";
				mysql_query($q);
				$gid = mysql_insert_id();
			}
			
			$API->uid = quickQuery("select uid from users where password='dummy' order by rand()");
			mysql_query("insert into group_members (uid,gid,admin) values ({$API->uid},$gid,0)");
			
			echo ">> New group created ($gid)\n";
			
			// create album
			
			$q = "insert into albums (uid,title,descr) values ({$API->uid},{$query['name']},{$query['name']})";
			mysql_query($q);
			
			$aid = mysql_insert_id();
			echo ">> New album created ($aid)\n";
			
			$i = 1;
		}
		
		// add photos
		
		foreach ($boat['photos'] as $url)
		{
			$source = "/mnt/ss/boatpics/boatpics/" . end(explode("/", $url));
			
			if (!file_exists($source))
			{
				echo "!! Photo missing, skipping ...\n";
				continue;
			}

			$hash = md5(microtime());
			$pid = processPhoto();
			
			mysql_query("update photos set aid=$aid,privacy=0,cat={$type['img']},created='" . date("Y-m-d", time() - 86400 * rand(0, 365)) . "' where id=$pid");
			mysql_query("insert into group_media (type,id,gid,uid) values ('P',$pid,$gid,{$API->uid})");
			echo ">> Photo added to group ($pid/" . $i++ . ")\t{$img_name}\n";
		}
		
		mysql_query("update groups set pid={$pid} where gid={$gid}");
		mysql_query("update albums set mainImage={$pid} where id={$aid}");
		echo ">> Group/album main image set to {$pid}\n";
		
		$last_mmsi = $mmsi;
	}
}

closedir($handle);

?>
