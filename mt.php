<?php

$pages = 99999; // will be reset shortly

for ($page = 9294; $page <= $pages; $page++)
{
	echo "Fetching page #{$page}" . ($pages > 1 ? "/$pages" : "") . " ...\n";
	
	$html_page = file_get_contents("http://www.marinetraffic.com/ais/showallthumbs.aspx?var_page={$page}");
	
	if ($pages == 99999) // find correct # of pages
		$pages = between($html_page, "&nbsp;&nbsp;page $page/", " &nbsp;");
	
	$boats_html = array_slice(explode("<h2>", $html_page), 2);
	
	foreach ($boats_html as $boat_html)
	{
		$mmsi = between($boat_html, "shipdetails.aspx?mmsi=", "'>");
		
		if (empty($mmsi))
			continue;
		
		$name = substr($boat_html, 0, strpos($boat_html, "<"));
		$photo_id = str_pad(between($boat_html, "photoid=", "'>"), 7, "0", STR_PAD_LEFT);
		$photo_url = "http://photos.marinetraffic.com/photos/" . substr($photo_id, 0, 3) . "/{$photo_id}.jpg";
		
		if (isset($boats[$mmsi]))
			echo "- Already have info for boat #{$mmsi}: {$name}\n";
		else
		{
			// don't have any info on this boat - so we will get it now
			echo "- Getting info for boat #{$mmsi}: {$name} ...\n";
			
			$boats[$mmsi]['name'] = $name;
			
			$details_html = file_get_contents("http://www.marinetraffic.com/ais/shipdetails.aspx?mmsi={$mmsi}");
			$parsed = explode("|", strip_tags(str_replace("&nbsp;", "", str_replace("<br/>", "|", str_replace("<h2>", "|", str_replace("</h2>", "|", strip_tags(between($details_html, "<h2>Vessel's Details</h2>", "</div>"), "<br/><h2>")))))));
			
			foreach ($parsed as $line)
			{
				$data = explode(":", $line, 2);
				if (count($data) < 2 || trim($data[1]) == "")
					continue;
				
				$key = trim($data[0]);
				$val = trim(html_entity_decode($data[1]));
				
				switch ($key)
				{
					case "Length x Breadth":
						$tmp = explode(" ", $val);
						$boats[$mmsi]['Length'] = $tmp[0];
						$key = "Breadth";
						$val = $tmp[3];
						break;
					
					case "DeadWeight":
					case "Draught":
						$val = intval($val);
						break;
						
					case "IMO":
						$tmp = explode(" ", $val);
						$boats[$mmsi]['IMO'] = intval($tmp[0]);
						$key = "MMSI";
						$val = $tmp[2];
						break;
					
					case "Flag":
						$tmp = explode(" ", $val);
						$boats[$mmsi]['Flag'] = $tmp[0];
						$key = "FlagCode";
						$val = preg_replace("/[^A-Z\s]/", "", $tmp[1]);
						break;
					
					case "Speed recorded (Max / Average)":
						$tmp = explode(" ", $val);
						$boats[$mmsi]['SpeedMax'] = $tmp[0];
						$key = "SpeedAvg";
						$val = $tmp[2];
						break;
						
					case "Info Received":
						$tmp = explode(" ", $val, 3);
						$val = implode(" ", array_splice($tmp, 0, 2));
						break;
					
					case "Latitude / Longitude":
						$tmp = explode(" ", $val);
						$boats[$mmsi]['Latitude'] = floatval($tmp[0]);
						$key = "Longitude";
						$val = floatval($tmp[2]);
						break;

				}
				
				$boats[$mmsi][$key] = $val;
			}
		}
		
		$boats[$mmsi]['photos'][] = $photo_url;
	}
	
	echo "Saving data from page #{$page} to file ...\n";
	
	//file_put_contents("/tmp/boats-" . str_pad($page, 5, "0", STR_PAD_LEFT), var_export($boats, true));
	file_put_contents("/tmp/boats_json-" . str_pad($page, 5, "0", STR_PAD_LEFT), json_encode($boats));
	
	unset($boats);
}

echo "Data exported to /tmp/boats and /tmp/boats_json\n\n";

function between($string, $start, $end)
{
	$string = " " . $string;
	$ini = strpos($string, $start);
	if ($ini == 0) return "";
	$ini += strlen($start);
	$len = strpos($string, $end, $ini) - $ini;
	return substr($string, $ini, $len);
}

?>