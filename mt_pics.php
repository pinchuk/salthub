<?php

// this script downloads all the photos of the boats whose data is saved in /tmp/boats_json-*
// and saves them into /tmp/boatpics

// cat /tmp/picsurls | parallel -P 20 'wget -nv {} -O "/tmp/boatpics/`echo {}|cut -d/ -f6`"'

$html_page = file_get_contents("http://www.marinetraffic.com/ais/showallthumbs.aspx?var_page={$page}");
//$pages = between($html_page, "&nbsp;&nbsp;page 1/", " &nbsp;");

$photos = between($html_page, "<h2>", " photo(s) found");

@mkdir("/tmp/boatpics");
unlink("/tmp/picsurls");
$i = 1;

$l = fopen("/tmp/picsurls", "w");

for ($page = 8677; $page <= 9294; $page++)
{
	$data = json_decode(file_get_contents("/tmp/boats_json-" . str_pad($page, 5, "0", STR_PAD_LEFT)), true);
	
	foreach ($data as $boat)
		foreach ($boat['photos'] as $url)
		{
			//echo ">> Getting picture #" . $i . "/{$photos} (" . round($i++ / $photos, 2) . "%) ...\n";
			$file = end(explode("/", $url));
			//system("curl -C - -# -o /tmp/boatpics/$file $url");
			fwrite($l, $url . "\n");
		}
}

fclose($l);


function between($string, $start, $end)
{
	$string = " " . $string;
	$ini = strpos($string, $start);
	if ($ini == 0) return "";
	$ini += strlen($start);
	$len = strpos($string, $end, $ini) - $ini;
	return substr($string, $ini, $len);
}

?>