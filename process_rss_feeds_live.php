<?
/*
Downloads RSS Feed updates and adds them to the SaltHub Feed.

(Look at /directories.php to see feed updates)
*/


$_SERVER['DOCUMENT_ROOT'] = "/var/www/salthub.com/public_html";
include $_SERVER['DOCUMENT_ROOT'] . "/inc/inc.php";
error_reporting(ERROR_SET_1);

$q = mysql_query( "select gid, rss_url from pages where rss_url != ''" );

function getBetween( $start, $end, $str )
{
  $s_pos = strpos( $str, $start );
  if( $s_pos === false ) return "";

  $s_pos += strlen( $start );

  $e_pos = strpos( $str, $end, $s_pos );
  if( $e_pos === false ) return "";

  return substr( $str, $s_pos, $e_pos - $s_pos );
}

while( $r = mysql_fetch_array( $q ) )
{
  $gid = $r['gid'];
  $url = $r['rss_url'];

  $result = xml2array( $url );

  if( sizeof( $result ) > 0 && isset( $result['rss']['channel']['item'] ) )
  {
    for( $c = sizeof( $result['rss']['channel']['item'] ) - 1; $c >= 0; $c-- )
    {
      $value = $result['rss']['channel']['item'][$c];

      $title   = addslashes( $value['title'] );
      $preview = addslashes( $value['description'] );
      $link    = addslashes( $value['link'] );
      $guid    = addslashes( @$value['guid'] );

      $preview_prefix = "";
      $image = getBetween( "<img", ">", $preview );
      $source = "";

      if( $image != "" )
      {
        $source = getBetween( 'src=\"', '\"', $image );

        if( $source != "" )
        {

          $image_info = getimagesize( $source );
          if( $image_info[0] > 50 && $image_info[1] > 50 )
          {
            //We only want the first image in the story, and only if the first image is over 50x50.
            $preview_prefix = "<div><img " . str_replace( '\"', '"', $image ) . "></div>";
          }
          else
          {
            $source = "";
          }
        }
      }


      $preview = iconv("UTF-8", "UTF-8//TRANSLIT", $preview);
      $title = iconv("UTF-8", "UTF-8//TRANSLIT", $title);

      $preview = $preview_prefix . "<div>" . strip_tags( $preview ) . "</div>";
      $source = addslashes( $source );

      //Duplicates will show an error here, this is intentional.
      mysql_query( "insert into rss_feed (`gid`, `preview`, `link`, `title`, `guid`, `image`) values ( '$gid', '$preview', '$link', '$title', '$guid', '$source' )" );

      if( mysql_error() == "" )
      {
/*
        //for testing
        if( $source != "" )
        {
          //print_r( $value );
          echo "Source: " . $source;
          echo $preview;
          exit;
        }
*/
        //If no error, then add feed item.
        $link_id = mysql_insert_id();
        mysql_query( "insert into feed ( type, link, ts, uid, uid_by, gid ) values ('rss', $link_id, Now(), 0, 0, $gid )" );
        
        break;
      }
    }
  }
}

?>
