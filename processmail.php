#! /usr/local/bin/php
<?php

$_SERVER["DOCUMENT_ROOT"] = "/var/www/salthub.com/public_html";

$fromEmail = true;
include $_SERVER["DOCUMENT_ROOT"] . "/inc/mailparse/MimeMailParser.class.php";

$_SERVER['HTTP_HOST'] = SERVER_HOST;

$raw_mail = file_get_contents("php://stdin");

file_put_contents('/tmp/raw_mail', $raw_mail);

$Parser = new MimeMailParser();
$Parser->setText($raw_mail);

$mail['to'] =  preg_replace("/[^a-zA-Z0-9_\s]/", "", current(explode("@", $Parser->getHeader('to'))));
$mail['domain'] =  strtolower(end(explode("@", $Parser->getHeader('to'))));
//$from = $Parser->getHeader('from');
$mail['subj'] = htmlentities(trim(addslashes($Parser->getHeader('subject'))));

$mail['descr'] = htmlentities(trim(addslashes($Parser->getMessageBody('text'))));

if (empty($mail['descr']))
	$mail['descr'] = strip_tags(trim(addslashes($Parser->getMessageBody('html'))));

$mail['hash'] = md5(microtime());
mkdir("/tmp/" . $mail['hash']);

if ($mail['domain'] == "mediabirdy.com")
	$site = "m";
else
	$site = "s";
	
include_once $_SERVER["DOCUMENT_ROOT"] . "/inc/inc.php";
include $_SERVER["DOCUMENT_ROOT"] . "/upload/video.php";
include $_SERVER["DOCUMENT_ROOT"] . "/upload/photo.php";
include $_SERVER["DOCUMENT_ROOT"] . "/upload/newalbum.php";

//echo "Site is $site ({$mail['domain']}).\n";

//mysql_query('delete from feed where 11337 in (uid,uid_by)');

foreach ($Parser->getAttachments() as $attach)
{
	$hash = md5(microtime());
	$f = "/tmp/$hash." . $attach->extension;
	$mail['files'][] = $f;
	$l = fopen($f, "w");
	fwrite($l, $attach->content);
	fclose($l);
}

if (empty($mail['to'])) die("die 1");

$API->uid = quickQuery("select uid from users where active=1 and phone='" . addslashes($mail['to']) . "'");
if (empty($API->uid)) die("die 2"); //don't know who this is - cya!

if (count($mail['files']) == 0)
{
	//is status update
	if (empty($mail['descr']))
		$_POST['post'] = $mail['subj'];
	else
		$_POST['post'] = $mail['descr'];

  if( $site == "m" )
  	include $_SERVER["DOCUMENT_ROOT"] . "/updatesocialstatus.php";
  else
  	include $_SERVER["DOCUMENT_ROOT"] . "/wallpost.php";

	die("die 3");
}

if (empty($mail['descr']) )
	$mail['descr'] = "No description";

if (empty($mail['subj']))
	$mail['subj'] = "Untitled";
	
foreach ($mail['files'] as $file)
{
	$f = preg_split('/[\.\/]/', $file);
	$ext = $f[3];
	$hash = $f[2];
	$source = "/tmp/$hash.$ext";

	$info = getimagesize($source);
	
	if ($info['mime']) //it's an image!!
	{
		echo "\n\nguessing image\n\n";
		$id = processPhoto();
		if ($id > 0)
			//store the info - we will be making an album later
			$photoIds[$id] = array("title" => $mail['subj'], "descr" => $mail['descr']);
	}
	else //video or just rubbish - we'll let the video script decide for us
	{
		echo "\n\nguessing video\n\n";
		// rename the file so the script recognizes it
		$target = "/tmp/vidupload-$hash";
		rename($source, $target);
		$source = $target;

		$vid = processVideo();
		if ($vid['id'] > 0)
		{
			mysql_query("update videos set title='" . $mail['subj'] . "',descr='" . $mail['descr'] . "' where id=" . $vid['id']);
			//die($vid['cmd']);
			$cmd = current(explode('>', $vid['cmd']));
			echo "\n\nRunning $cmd ...\n\n";
			exec($cmd, $out);
		}
	}
}

if (is_array($photoIds)) //now we make the album
{
	$_POST = array(); // POST will contain the album information for the /upload/newalbum.php script

	foreach ($photoIds as $id => $photo)
	{
		if (empty($_POST['m'])) //set the first image as the mainimage
				$_POST['m'] = $id;

		$_POST['title'] = $photo['title'];
		$_POST['descr'] = $photo['descr'];
		$_POST['ids'] .= "," . $id;
	}

	$_POST['ids'] = substr($_POST['ids'], 1); //get rid of the comma in the front
	
	//create the album
	createAlbum();
	
	mysql_query("update photos set privacy=0,cat=1 where id in ({$_POST['ids']})");
}

?>
