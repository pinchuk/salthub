<?php
include_once "inc/inc.php";
include_once "inc/recaptchalib.php";

$title = "About Advertising | $siteName - Professional Maritime Network";
$descr = "The $siteName Ad platform allows individuals and businesses (small and large) to promote their service or offerings in minutes.";

$page_header = "About Advertising $siteName";
  $sub_header = $siteName . ' is the leading social network for professionals and businesses who make a living on and around the water.';

$scripts[] = "/index.js";
$noheader = true;

if( sizeof( $_REQUEST ) > 0 )
{
  echo '<html><title></title><link rel="stylesheet" href="/style_s.css" type="text/css" /><link rel="stylesheet" href="/style.css" type="text/css" /><body>';
}
else
{
  if( $API->isLoggedIn() )
  {
    $background = "url('images/bggraygrad2.png'); background-repeat:repeat-x; margin-top:-5px; width:100%;";
    include "header.php";
  }
  else
  {
    include "signup/newheader.php";
    include "header_gradient.php";
  }
}
?>

<style>
.subhead2
{
  font-size: 14px;
  font-weight: bold;
  margin-top:15px;
}

.subheadcontent2
{
  font-size: 13px;
}

</style>

<?
if( sizeof( $_REQUEST ) == 0 )
{
?>
<div class="graygradient">
	<div class="headcontainer">
		<div class="entryslide">
			<?=$htmlSignIn?>
			<div class="slidecontent">
				<span class="title" style="font-size:30pt;">Promote Your Company</span>

				<div class="text">

        The <?=$siteName?> Ad platform allows individuals and businesses (small and large) to promote their service or offerings in minutes. The platform is easy to use with no experience necessary. Join <?=$siteName?>, build your creative using an image + text, target your professional audience and set your budget. No contracts, no hassles; just fast and effective.

				</div>


				<div class="quotecontainer">
					<div class="quote">
						<div class="open"></div>
						<div class="text" style="width:440px;">It's the only ad platform that provides an end to end network + solution, enabling us to reach industry specific decision makers.</div>
						<div class="close"></div>
						<div class="person">&mdash; Ian, <?=$siteName?> member since 2012</div>
					</div>
				</div>

			</div>

			<img src="/images/520x320 megaphone.jpg" class="bigimage" alt="" />
		</div>
	</div>
</div>

<div style="padding: 10px 5px 5px 5px; font-family: arial; font-size: 11pt; width:1050px; margin-left: auto; margin-right:auto;">
<? } else { ?>
<div style="padding: 10px 5px 5px 5px; font-family: arial; font-size: 11pt; width:550px; margin-left: 0px;">
<? } ?>
  <div style="font-family: tahoma; font-size: 20pt; color: #7f7f7f; padding-bottom:5px; padding-top: 12px;">About the Ad Platform on <?=$siteName?></div>

  <div style="float:left; width:470px;">
		<div class="subhead2">The <?=$siteName?> Ad Platform</div>
    <div class="subheadcontent2">
      A self-serve end to end solution that allows fast and effective marketing to individuals and businesses located in the maritime industries. You control your costs and only get billed when your ads are seen or clicked on.
    </div>

  	<div class="subhead2">Ad Location, Look and Feel</div>
    <div class="subheadcontent2">
      <ul>
        <li>119 x 95 image</li>
        <li>Ad title = 24 characters</li>
        <li>Ad body  = 148 characters</li>
        <li>Ad links to your website</li>
        <li>Ads are positioned in the right column</li>
      </ul>
    </div>

    <div class="subhead2">Example Advertisements</div>

    <div>
    	<div class="toptool">
    		<h3>Promote your Company</h3>
    		<img src="/images/mega.png" alt="" />
    		<div>Go ahead and create your ad in minutes.&nbsp; Start targeting your services and products on <?=$siteName?>.</div>
    	</div>
    	<div class="toptool">
    		<h3>Add your Business</h3>
    		<img src="/images/business.png" alt="" />
    		<div>Add your business or service to the <?=$siteName?> Directory and get discovered.</div>
    	</div>
    </div>
    <div style="clear:both;"></div>

    <div class="subhead2">Targeting Ads</div>
    <div class="subheadcontent2">
    Your ads and who you target is totally up to you. Your target can be wide or narrow or somewhere in between. You can target using the following options:<br />

    <ul>
      <li>Industry</li>
      <li>Service or Business Type</li>
      <li>Occupation</li>
      <li>Page Type</li>
      <li>Geographic Location</li>
      <li>Keywords</li>
      <li>Any Combination</li>
    </ul>
    </div>


    <div class="subhead2">Ad Locations</div>
    <div class="subheadcontent2">
      Your ads will be seen in the companion position and in columns at the right when companion positions are not available. <?=$siteName?> members will also have the ability to see similar ads with similar targets. This feature is available to SaltHub members thru the SaltHub ad directory.
    </div>

    <div>
<br />
    (companion)<br />
    <img src="images/companion ad 471X285.jpg" width="471" height="285" alt="" /><br /><br />
    (right column)<br />
    <img src="images/right column ad 471X285.jpg" width="471" height="285" alt="" />
    </div>
  </div>

  <div style="float:left; width:470px; margin-left:20px;">
    	<div class="subhead2">Go Ahead and Get Started</div>
       <div class="subheadcontent2">
All you need is an account and a credit card to get started.<br /><br />
After you login or create your account, take the following steps.<br />

<ol>
  <li>Launch the <?=$siteName?> ad platform from your <a href="/settings"><b>account settings</b></a> located at the top right of <?=$siteName?>.</li>
  <li>Select "<a href="/adv/create.php"><b>create a new ad campaign</b></a>" and build your first ad.</li>
  <li>In campaign settiings, target your demographic by industry, occupation, page type, location and keywords.</li>
  <li>Select your run time</li>
  <li>Determine your budget and run either CPM or CPC ads.</li>
  <li>Build variations of your ad to determine what works best.</li>
  <li>Start and pause your ads at any time</li>
</ol>
      </div>

    	<div class="subhead2">Costs</div>
       <div class="subheadcontent2">
Spend as little as $10.00 a day. You control what you spend each day and how long your ads will run. If you want to target a wider or narrower scope; refine your target by adjusting demographics and key words. Start and pause your ads as often as you wish and create variations to see what works best.
      </div>

    	<div class="subhead2">Billing and Charges</div>
       <div class="subheadcontent2">
A credit card is required to set up your account. As you accrue ad charges on your account, your credit card will be charged on a periodic basis. The frequency that we charge your credit card may be daily, weekly and monthly and is determined by your daily budget and the accumulation of charges.
      </div>

    	<div class="subhead2">Ad Credits</div>
       <div class="subheadcontent2">
Ad credits may be applied to your account at anytime. All you have to do is redeem the credit code and submit your credit card information. Once approved, your credits will be applied first and your card will be charged for any overages. Note: Ads will not run without a credit card on file.
      </div>

    	<div class="subhead2">Contracts &amp; Approval Process</div>
       <div class="subheadcontent2">
The SaltHub ad platform is a better way to promote your service or offering.  No contracts, no hassles; just fast and effective. Once you create and submit your ad, the SaltHub team will review it, usually within 24 hours. If your ad is approved, it will begin running immediately. You can then refine your target, pause or increase / decrease your budget at any time.
      </div>

    	<div class="subhead2">Questions &amp; Support</div>
       <div class="subheadcontent2">
If we didn't cover what you were looking for, go ahead and contact us. A <?=$siteName?> team member will be ready to assist you with getting your ads up and running.
<br />
<br />
<a href="javascript:void(0);" onclick="javascript: openSendMessagePopup( '','','SaltHub', 1187301, 0,0, 1 );">Send Email</a> <br />
Snail Mail: 1323 SE 17th St., Suite 690, Ft. Lauderdale, FL. 33316<br />
<?=$siteName?> <a href="/tos.php">Terms of Service</a><br />
      </div>

    	<div class="subhead2">Give it a Try</div>
      <div class="subheadcontent2">
Setting up your account is free and you can cancel at anytime. We suggest you go ahead and give it a try. We think you'll like what you find.
      </div>

    <div style="text-align:center; margin-top:20px;">
    <input type="button" class="button" onclick="window.document.location='/adv/create.php';" value="Create a new Ad Campaign" style="padding:20px; font-weight:bold;"/>

    <div style="font-size:8pt;">
    (Requires Login)
    </div>

    </div>
  </div>

</div>

<div style="clear: both;"></div>

<?php
if( sizeof( $_REQUEST ) > 0 )
  echo "</body></html>";
else
  include "signup/newfooter.php";
?>
