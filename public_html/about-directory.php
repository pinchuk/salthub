<?php
/*
Contains detailed information about "Pages" capability for companies

*/

$noLogin=true;

require( "inc/inc.php" );

$title = "Pages | $siteName";
$descr = "Pages bring you closer to customers, peers, and followers.  It provides the ability to be heard, discoverd, and so much more. Claim or create yours.";


include_once "inc/recaptchalib.php";
$publickey = "6LciL78SAAAAANVcbxyOQtkOkEyJN1YaGs_HUe4M";
$privatekey = "6LciL78SAAAAAKjsEQNYz2kD7Dm__mk4jshxleAk";

if ($site == "s" && !$API->isLoggedIn())
{
/*  $title = "Pages | <?=$siteName ?> - Be heard - Get discovered - Connect";
  $descr = "$siteName is the leading social network for professionals and businesses who make a living on and around the water. Connect, access knowledge, insights and be discovered.";
  $page_header = $siteName . " Pages";
  $sub_header = $siteName . ' is the leading social network for professionals and businesses who make a living on and around the water. Claim or create your page.';*/
	$scripts[] = "/index.js";
	$noheader = true;
	include "signup/newheader.php";
	include "header_gradient.php";
}
else
{
  $background = "url('images/bggraygrad2.png'); background-repeat:repeat-x; margin-top:-5px; width:100%;";
  include "header.php";
}

?>
<script type="text/javascript">
<!--
function printContent(id){
str=document.getElementById(id).innerHTML
newwin=window.open('','printwin','left=100,top=100,width=600,height=480')
newwin.document.write('<HTML>\n<HEAD>\n')
newwin.document.write('<TITLE>Print Page</TITLE>\n')
newwin.document.write('<script>\n')
newwin.document.write('function chkstate(){\n')
newwin.document.write('if(document.readyState=="complete"){\n')
newwin.document.write('window.close()\n')
newwin.document.write('}\n')
newwin.document.write('else{\n')
newwin.document.write('setTimeout("chkstate()",2000)\n')
newwin.document.write('}\n')
newwin.document.write('}\n')
newwin.document.write('function print_win(){\n')
newwin.document.write('window.print();\n')
newwin.document.write('chkstate();\n')
newwin.document.write('}\n')
newwin.document.write('<\/script>\n')
newwin.document.write('</HEAD>\n')
newwin.document.write('<BODY onload="print_win()">\n')
newwin.document.write(str)
newwin.document.write('</BODY>\n')
newwin.document.write('</HTML>\n')
newwin.document.close()
}
//-->
</script>


<div class="graygradient">
	<div class="headcontainer">
		<div class="entryslide">
			<?=$htmlSignIn?>
			<div class="slidecontent">
				<span class="title">Add Your Business</span>

				<div class="text">
          The <?=$siteName?> Directory is the ultimate online resource used by decision makers in the maritime industry. Listings are social with the most  comprehensive set of tools on the web to promote your business.
				</div>

				<div class="quotecontainer">
					<div class="quote">
						<div class="open"></div>
						<div class="text" style="width: 440px;">Adding our page to the <?=$siteName?> Directory provides frontline visibility for a fraction of what other services charge.</div>
						<div class="close"></div>
						<div class="person">&mdash; Ian, <?=$siteName?> member since 2012</div>
					</div>
				</div>
			</div>

			<img src="/images/520x320 Business Book.jpg" class="bigimage" alt="" />
		</div>
	</div>
</div>


<div id="printme">

<style>
.subhead2
{
  font-size: 14px;
  font-weight: bold;
  margin-top:15px;
}

.subheadcontent2
{
  font-size: 13px;
}
</style>

<div style="padding: 10px 5px 5px 5px; font-family: arial; font-size: 11pt; width:1050px; margin-left: auto; margin-right:auto;">

  <div style="font-family: tahoma; font-size: 20pt; color: #7f7f7f; padding-bottom:5px; padding-top: 12px;">About the Business Directory on <?=$siteName?></div>

  <div style="float:left; width:470px;">
		<div class="subhead2">Business Directory on <?=$siteName?></div>
    <div class="subheadcontent2">
      The Business Directory is made up of premium <a href="https://www.salthub.com/about-pages.php" style="font-weight:bold;">pages</a> from <?=$siteName?>. Create your page and then add it to the Business Directory. It's that easy! Once submitted, your listing becomes social and users can now see how many of their connections use your Business or Service. Users can also drill down to exactly what they are looking for in seconds, making the Business Directory the most effective and recognized way to get your company out in front of the decision makers that matter most.
    </div>

  	<div class="subhead2">Directory Access</div>
    <div class="subheadcontent2">
      Access to the directory is available to all visitors of <?=$siteName?> (membership or login not required). This means that members of <?=$siteName?> and people who visit the <?=$siteName?> website, but have not yet joined, can find your business of service when using the <?=$siteName?> Directory.
    </div>

    <div class="subhead2">Directory Look and Feel</div>
    <div class="subheadcontent2">
      The directory has a clean layout and is very easy to navigate. It's also social, allowing users to see how many of their connections use the services and companies that appear in their search results. (to learn more, see "Listings are Social" below). Users can also identify exactly what they are looking for through company logos, products and services offered, by industry and key words.
    </div>

    <div class="subhead2">
      <img src="images/directory_look.png" width="469" height="290" alt="" />
    </div>

    <div class="subhead2">Directory Listings are Social</div>
    <div class="subheadcontent2">
      <?=$siteName?> business listings are social. Search results are displayed by showing the companies and services your connections use first. This allows users to see the number of their connections that use a business and enables them to immediately qualify companies. This is often the deciding factor and one of the big advantages to using and being listed in the <?=$siteName?> Directory.
    </div>

    <div class="subhead2">
      <img src="images/directory_Social.png" width="469" height="149" alt="" />
    </div>

    <div class="subhead2">Directory Search</div>
    <div class="subheadcontent2">
      Using the main search on <?=$siteName?> is very powerful and has been designed to give users quick and easy access to the companies, schools, vessels and people they already know; however, the <?=$siteName?> Directory Search Tool allows users to drill down by Keyword, Business Sector, Categories and Location. These advance search options make discovering what a person is looking for quick and easy, even if they don't know the service or company exists. Also, if a user does not find what they want, they can refine their search for a broader list of results.
    </div>

    <div class="subhead2">
      <img src="images/directory_search.png" width="188" height="218" alt="" />
    </div>

    <div class="subhead2">Business News by Sector</div>
    <div class="subheadcontent2">
      Once you add your Business Page to the <?=$siteName?> Business Directory; posting updates, news and media about your company or service will now also appear on the home page of the Business Directory.  The Directory categorizes news by sector and any verified company, big or small, can use it. In addition to many other features, this allows your company to supply a continuous stream of information to the people that matter most to your company. It also enables you to connect with industry peers and customers.
    </div>

    <div class="subhead2">
      <img src="images/directory_feed.png" width="471" height="290" alt="" />
    </div>

  </div>

  <div style="float:left; width:470px; margin-left:20px;">
    	<div class="subhead2">Go Ahead and Get Started</div>
       <div class="subheadcontent2">
All you need is an account to get started.<br /><br />
After you login or create your account, take the following steps. If you have a page see "<a href="#addmybusiness">Add my Business Page</a>" below.<br />

<ol>
  <li>Launch the <?=$siteName ?> page platform from your <a href="/settings">account</a> located at the top right of <?=$siteName ?>.</li>
  <li>Select "<a href="/pages/page_create.php">create a page</a>" and build your first page.</li>
  <li>Under "category type" select the appropriate category for your page.</li>
  <li>Select your primary sector and category for your business or service, this is included with your page.  Charges for additional categories and sectors apply and can be chosen after page creation.  <a href="/pages/page_create.php">Learn More</a></li>
  <li>Add your Page Name, Website and Privacy setting.</li>
  <li>Agree to the terms of Service and select "Next"<br /><br /><i>Your page has now been created. For best results, we strongly recommend completing the following items.</i><br /><br /></li>
  <li>Like and Tweet your page on facebook and twitter (absolutely do this)<br /><img src="images/FB_Twit_like.png" width="164" height="20" alt="" style="padding-top:8px; padding-bottom:2px;"/></li>
  <li>Upload your company logo for desired photo (recommended)</li>
  <li>Include descriptive text with unlimited characters</li>
  <li>Select the Products and services you offer</li>
  <li>Select the Industries you'd like to target</li>
  <li>Add your contact information and address</li>
  <li>List what you can be contacted for in order to show up in <?=$siteName ?> searches</li>
  <li>Add Photos, Videos, Updates or current events related to your company</li>
  <li>Add people you know to your page (recommended)</li>
  <li><a href="http://www.salthub.com/pages/claim_company.php">Verify your page</a> (recommended)</li>
</ol>
      </div>

      <a name="addmybusiness"></a>
      <div style="background-color:#f2fbff;">
      	<div class="subhead2">Add my Business Page to the <?=$siteName?> Business Directory</div>
        <div class="subheadcontent2">
          Go to your existing page on <?=$siteName?>. In the top left below your company logo or desired photo select edit.  You will now see your page in edit mode, allowing you to make edits to any and all fields. This is where you will make your selection to be added to the <?=$siteName?> Business Directory.
          <ol>
            <li>Select any additional Categories you would like to have your company listed under. We recommend being listed under a minimum of two Categories.</li>
            <li>Select any additional Industries you would like to have your company or service target.</li>
            <li><span style="color:rgb(215, 94, 28);">Towards the bottom of the page you will see where you can add your page to the directory. Go ahead and check that box.</span></li>
            <li>Save your changes and check out.</li>
          </ol>
        </div>
      </div>

    	<div class="subhead2">Industry Sectors and Categories</div>
      <div class="subheadcontent2">
        For business and services, your primary sector and category is included with your page. Charges for additional categories and sectors apply and can be chosen for page creation. This allows your company to be found easier using advanced search, only found in the <a href="/directory.php"><?=$siteName ?> Business Directory</a>.
      </div>

    	<div class="subhead2">Costs</div>
      <div class="subheadcontent2">
        Spend as little as 81 cents a day to include your <?=$siteName ?> page in the <a href="/directory.php"><?=$siteName ?> Business Directory</a>. Or for even more exposure, purchase additional categories and industries related to your business.
      </div>


    	<div class="subhead2">Billing and Charges</div>
      <div class="subheadcontent2">
        A credit card is required to set up your account. When you add categories, industry sectors or list your page in the Business Directory and accrue charges on your account, your credit card will be charged on a periodic basis. The frequency that we charge your credit card may be weekly, monthly , annually and is determined by your selections and the time which you added your page to the Business Directory.
      </div>

    	<div class="subhead2">Credits</div>
      <div class="subheadcontent2">
        Credits may be applied to your account at any time. All you have to do is redeem the credit code and submit your credit card information. Once approved, your credits will be applied first and your card will be charged for any overages. Note: Your page will not be added to the Business Directory without a credit card on file.
      </div>

    	<div class="subhead2">Contracts &amp; Approval Process</div>
      <div class="subheadcontent2">
        The <?=$siteName ?> Page platform and Business Directory is a better way to promote your service or offering.  No contracts, no hassles; just fast and effective. Once you create, submit and verify your Page, it goes live and will show up in search results and in the Business Directory Neds Feed immediately. You can then refine your Sectors and Business Sectors at any time.
      </div>

    	<div class="subhead2">Questions &amp; Support</div>
      <div class="subheadcontent2">
        If we didn't cover what you were looking for, go ahead and contact us. A <?=$siteName ?> team member will be ready to assist you with getting your pages up and running.
        <br /><br />
        <a href="javascript:void(0);" onclick="javascript: openSendMessagePopup( '','','<?=$siteName ?>', 1187301, 0,0, 1 );">Send Email</a> <br />
        Snail Mail: 1323 SE 17th St., Suite 690, Ft. Lauderdale, FL. 33316<br />
        <?=$siteName?> <a href="/tos.php">Terms of Service</a><br />
      </div>

    	<div class="subhead2">Give it a Try</div>
      <div class="subheadcontent2">
Setting up your account is free and you can cancel at anytime. We suggest you go ahead and give it a try. We think you'll like what you find.
      </div>


    <div style="text-align:center; margin-top:20px;">
      <a href="javascript:void(0);" onclick="javascript:printContent('printme');"><img src="images/printer.png" width="16" height="16" alt="" /> Print this page first</a><br /><span style="font-size:8pt;">then</span>
    </div>
    <div style="text-align:center;">
    <input type="button" class="button" onclick="window.document.location='/pages/page_create.php';" value="Create a new Page" style="padding:20px; font-weight:bold;"/>
    <div style="font-size:8pt;">
    (Requires Login)
    </div>
    </div>
  </div>

</div>
</div>

<div style="clear: both; margin-bottom: 20px;"></div>

<?php include "signup/newfooter.php"; ?>