<?php
/*
Contains detailed information about "Pages" capability for companies

*/

$noLogin=true;

require( "inc/inc.php" );

$title = "Pages | $siteName";
$descr = "Pages bring you closer to customers, peers, and followers.  It provides the ability to be heard, discoverd, and so much more. Claim or create yours.";


include_once "inc/recaptchalib.php";
$publickey = "6LciL78SAAAAANVcbxyOQtkOkEyJN1YaGs_HUe4M";
$privatekey = "6LciL78SAAAAAKjsEQNYz2kD7Dm__mk4jshxleAk";

if ($site == "s" && !$API->isLoggedIn())
{
/*  $title = "Pages | <?=$siteName ?> - Be heard - Get discovered - Connect";
  $descr = "$siteName is the leading social network for professionals and businesses who make a living on and around the water. Connect, access knowledge, insights and be discovered.";
  $page_header = $siteName . " Pages";
  $sub_header = $siteName . ' is the leading social network for professionals and businesses who make a living on and around the water. Claim or create your page.';*/
	$scripts[] = "/index.js";
	$noheader = true;
	include "signup/newheader.php";
	include "header_gradient.php";
}
else
{
  $background = "url('images/bggraygrad2.png'); background-repeat:repeat-x; margin-top:-5px; width:100%;";
  include "header.php";
}

?>

<script type="text/javascript">
<!--
function printContent(id){
str=document.getElementById(id).innerHTML
newwin=window.open('','printwin','left=100,top=100,width=600,height=480')
newwin.document.write('<HTML>\n<HEAD>\n')
newwin.document.write('<TITLE>Print Page</TITLE>\n')
newwin.document.write('<script>\n')
newwin.document.write('function chkstate(){\n')
newwin.document.write('if(document.readyState=="complete"){\n')
newwin.document.write('window.close()\n')
newwin.document.write('}\n')
newwin.document.write('else{\n')
newwin.document.write('setTimeout("chkstate()",2000)\n')
newwin.document.write('}\n')
newwin.document.write('}\n')
newwin.document.write('function print_win(){\n')
newwin.document.write('window.print();\n')
newwin.document.write('chkstate();\n')
newwin.document.write('}\n')
newwin.document.write('<\/script>\n')
newwin.document.write('</HEAD>\n')
newwin.document.write('<BODY onload="print_win()">\n')
newwin.document.write(str)
newwin.document.write('</BODY>\n')
newwin.document.write('</HTML>\n')
newwin.document.close()
}
//-->
</script>

<div class="graygradient">
	<div class="headcontainer">
		<div class="entryslide">
			<?=$htmlSignIn?>
			<div class="slidecontent">
				<span class="title"><?=$siteName?> Pages</span>

				<div class="text">
					Creating your page has many advantages.&nbsp; It brings you closer to customers, peers, and followers.&nbsp; It provides the ability to be heard,
					discoverd, and so much more.&nbsp; Create your page below.
				</div>
				
				<div class="quotecontainer">
					<div class="quote">
						<div class="open"></div>
						<div class="text">Adding my page to <?=$siteName?> put my company out in front of<br />decision makers.&nbsp; They are the ones I need to connect with.</div>
						<div class="close"></div>
						<div class="person">&mdash; Ian, <?=$siteName?> member since 2012</div>
					</div>
				</div>
			</div>
			
			<img src="/images/acme.jpg" class="bigimage" alt="" />
		</div>
	</div>
</div>


<div style="padding: 10px 5px 5px 5px; font-family: arial; font-size: 11pt; width:1050px; margin-left: auto; margin-right:auto;" id="printme">

<style>
.subhead2
{
  font-size: 14px;
  font-weight: bold;
  margin-top:15px;
}

.subheadcontent2
{
  font-size: 13px;
}
</style>

  <div style="font-family: tahoma; font-size: 20pt; color: #7f7f7f; padding-bottom:20px;">About Pages on <?=$siteName?></div>

  <div style="float:left; width:470px;">
		<div class="subhead2">Pages on <?=$siteName?></div>
    <div class="subheadcontent2">
      A self-serve content rich solution that allows maritime organizations and brands the ability to share their contact information, what they do, media, and news.  It is the most effective and recognized way to get out in front of the decision makers that matter most.
    </div>

  	<div class="subhead2">Page Locations</div>
    <div class="subheadcontent2">
      Listings are Social. They show up in user logbooks, on welcome pages and throughout the site.  The more people connected with your page, the more your page appears across <?=$siteName ?>.  You can also upgrade your page to the <a href="/directory.php"><?=$siteName ?> Business Directory</a>.
    </div>

    <div class="subhead2">
      <img src="images/Business page about.png" width="471" height="290" alt="" />
    </div>

    <div class="subhead2">Page Look and Feel</div>
    <div class="subheadcontent2">
      Your pages will appear across the <?=$siteName ?> network, will be seen in the companion positions and in columns at the right when companion positions are not available. <?=$siteName ?> members will also have the ability to see similar ads with similar targets.  This feature is available to <?=$siteName ?> members through the <?=$siteName ?> ad directory.
    </div>


    <div class="subhead2">Advantages of <?=$siteName?> Pages</div>
    <div class="subheadcontent2">
    <ul>
      <li>People who connect with your page will get updates in their activity logs</li>
      <li>Available to all visitors of <?=$siteName ?> (login not required)</li>
      <li>Indexed by Google, Yahoo, MSN and many others</li>
    </ul>
    </div>

    <div class="subhead2">
      <img src="images/Business page about.png" width="471" height="290" alt="" />
    </div>

    <div class="subheadcontent2">
    <ul>
      <li>Contact information & address</li>
      <li>Direct Link to Website youtube, facebook & twitter accounts</li>
      <li>Map showing your location</li>
      <li>175x300 company logo or desired image</li>
      <li>Description with unlimited characters</li>
    </ul>
    </div>

    <div class="subhead2">
      <img src="images/Business page log.png" width="471" height="290" alt="" />
    </div>

    <div class="subheadcontent2">
    <ul>
      <li>Manually post unlimited updates, news, photos and videos to your company logbook.</li>
      <li>Include your RSS feed for continued postings to your page</li>
      <li>Post once on <?=$siteName ?> and see it on facebook and twitter</li>
      <li>Interact and engage with your customers</li>
    </ul>
    </div>

    <div class="subhead2">
      <img src="images/Business page photo.png" width="471" height="290" alt="" />
    </div>

    <div class="subheadcontent2">
    <ul>
      <li>Catalog products photos and videos</li>
      <li>Post videos already uploaded to youtube. Just copy the youtube link and done.</li>
      <li>Share photos and video from corporate events, tradeshows and more</li>
      <li>Identify your products, services, employees and more with the ability to tag photos and videos</li>
    </ul>
    </div>

  </div>

  <div style="float:left; width:470px; margin-left:20px;">
    	<div class="subhead2">Go Ahead and Get Started</div>
       <div class="subheadcontent2">
All you need is an account to get started.<br /><br />
After you login or create your account, take the following steps.<br />

<ol>
  <li>Launch the <?=$siteName ?> page platform from your <a href="/settings">account</a> located at the top right of <?=$siteName ?>.</li>
  <li>Select "<a href="/pages/page_create.php">create a page</a>" and build your first page.</li>
  <li>Under "category type" select the appropriate category for your page.</li>
  <li>Select your primary sector and category for your business or service, this is included with your page.  Charges for additional categories and sectors apply and can be chosen after page creation.  <a href="/pages/page_create.php">Learn More</a></li>
  <li>Add your Page Name, Website and Privacy setting.</li>
  <li>Agree to the terms of Service and select "Next"<br /><br /><i>Your page has now been created. For best results, we strongly recommend completing the following items.</i></li>
  <li>Like and Tweet your page on facebook and twitter (absolutely do this)<br /><img src="images/FB_Twit_like.png" width="164" height="20" alt="" style="padding-top:8px; padding-bottom:2px;"/></li>
  <li>Upload your company logo for desired photo (recommended)</li>
  <li>Include descriptive text with unlimited characters</li>
  <li>Select the Products and services you offer</li>
  <li>Select the Industries you'd like to target</li>
  <li>Add your contact information and address</li>
  <li>List what you can be contacted for in order to show up in <?=$siteName ?> searches</li>
  <li>Add Photos, Videos, Updates or current events related to your company</li>
  <li>Add people you know to your page (recommended)</li>
  <li><a href="http://www.salthub.com/pages/claim_company.php">Verify your page</a> (recommended)</li>
</ol>
      </div>

    	<div class="subhead2">Costs</div>
      <div class="subheadcontent2">
        All pages on <?=$siteName ?> are Free and can be found through your connections on <?=$siteName ?>, or if the name of your Page is entered directly into the search field.
        <br /><br />
        <span style="color:rgb(215, 94, 28);">Note: Business Pages do not show up in <?=$siteName ?> search results. If you'd like your company to be easily discovered, add it to the <?=$siteName ?> Business Directory. See below for more information.</span>
      </div>

    	<div class="subhead2">The <?=$siteName ?> Business Directory</div>
      <div class="subheadcontent2">
        Spend as little as 81 cents a day to include your <?=$siteName ?> page in the <a href="/directory.php"><?=$siteName ?> Business Directory</a>.
      </div>

    	<div class="subhead2">Benefits of being listed in the Directory</div>
      <div class="subheadcontent2">
<ol>
  <li>The Business Directory has an advanced search feature for businesses and services.</li>
  <li>The Directory makes finding companies by industry, products and services easy and quick.</li>
  <li>Posted content, RSS Feeds, updates and news about your company or service to your Business Page will be shown in the <a href="/directory.php">Business News Feed</a>.</li>
  <li>Your news and updates will appear under each industry that you choose in the Business News Feed.</li>
  <li>The Business Directory is social. This allows members to see if their connections use your business.</li>
  <li>Your company is then also found through members who have connected with your page and accessible to all visitors of <?=$siteName ?> (login not required)</li>
</ol>
      </div>

    	<div class="subhead2">Industry Sectors and Categories</div>
      <div class="subheadcontent2">
        For business and services, your primary sector and category is included with your page. Charges for additional categories and sectors apply and can be chosen for page creation. This allows your company to be found easier using advanced search, only found in the <a href="/directory.php"><?=$siteName ?> Business Directory</a>.
      </div>

    	<div class="subhead2">Billing and Charges</div>
      <div class="subheadcontent2">
        A credit card is required to set up your account. When you add categories, industry sectors or list your page in the Business Directory and accrue charges on your account, your credit card will be charged on a periodic basis. The frequency that we charge your credit card may be weekly, monthly , annually and is determined by your selections and the time which you added your page to the Business Directory.
      </div>

    	<div class="subhead2">Credits</div>
      <div class="subheadcontent2">
        Credits may be applied to your account at any time. All you have to do is redeem the credit code and submit your credit card information. Once approved, your credits will be applied first and your card will be charged for any overages. Note: Your page will not be added to the Business Directory without a credit card on file.
      </div>

    	<div class="subhead2">Contracts &amp; Approval Process</div>
      <div class="subheadcontent2">
        The <?=$siteName ?> Page platform and Business Directory is a better way to promote your service or offering.  No contracts, no hassles; just fast and effective. Once you create, submit and verify your Page, it goes live and will show up in search results and in the Business Directory Neds Feed immediately. You can then refine your Sectors and Business Sectors at any time.
      </div>

    	<div class="subhead2">Questions &amp; Support</div>
      <div class="subheadcontent2">
        If we didn't cover what you were looking for, go ahead and contact us. A <?=$siteName ?> team member will be ready to assist you with getting your pages up and running.
        <br /><br />
        <a href="javascript:void(0);" onclick="javascript: openSendMessagePopup( '','','<?=$siteName ?>', 1187301, 0,0, 1 );">Send Email</a> <br />
        Snail Mail: 1323 SE 17th St., Suite 690, Ft. Lauderdale, FL. 33316<br />
        <?=$siteName?> <a href="/tos.php">Terms of Service</a><br />
      </div>

    	<div class="subhead2">Give it a Try</div>
      <div class="subheadcontent2">
Setting up your account is free and you can cancel at anytime. We suggest you go ahead and give it a try. We think you'll like what you find.
      </div>

    <div style="text-align:center; margin-top:20px;">
      <a href="javascript:void(0);" onclick="javascript:printContent('printme');"><img src="images/printer.png" width="16" height="16" alt="" /> Print this page first</a><br /><span style="font-size:8pt;">then</span>
    </div>
    <div style="text-align:center;">
    <input type="button" class="button" onclick="window.document.location='/pages/page_create.php';" value="Create a new Page" style="padding:20px; font-weight:bold;"/>
    <div style="font-size:8pt;">
    (Requires Login)
    </div>
    </div>
  </div>

</div>

<div style="clear: both;"></div>

<?php include "signup/newfooter.php"; ?>