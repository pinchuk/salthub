<?php
/*
This script is called from when someone replies to a message, photo, video, or wall post.  This script
adds a comment onto the discussion thread.

Change Log:
8/17/2011 - Added code to retrieve j.mp link if one does not exist for photos and videos. (This is only the case when the photo or video was imported)
9/23/2012 - Added support for rss feed comments.


*/

include "inc/inc.php";
include "inc/mod_comments.php";

if (!$API->isLoggedIn())
	die(json_encode(array("error" => "You must be logged in to comment.")));

ob_start();

$type = addslashes(substr($_POST['type'], 0, 1));
$link = intval($_POST['link']);
$gid = $_POST['gid'];

$postAsPage = quickQuery( "select postAsPage from page_members where admin=1 and gid=$gid and uid=" . $API->uid );

if( $gid > 0 && !$postAsPage )
  $gid = 0;

sql_query("insert into comments (type,link,comment,uid,gid) values ('$type',$link,'" . $_POST['comment'] . "'," . $API->uid . ",$gid)");

$comment['comment'] = $_POST['comment'];
$comment['gid'] = $gid;
$comment['id'] = mysql_insert_id();

if ($type == "M")
{
	$x = sql_query("select uid,uid_to from messages where mid=$link");
	$info = mysql_fetch_array($x, MYSQL_ASSOC);
	if ($API->uid == $info['uid']) // if is sender
		sql_query("update messages set unreadByRecv=1, delByRecv=0 where mid=$link");
	else
		sql_query("update messages set unreadBySend=1, delBySend=0 where mid=$link");
}


showComment($comment, null, true, intval($_POST['layout']), $type != "M");

if ($type == "M") //message - just notify user they have one
	$API->sendNotification(NOTIFY_MESSAGE, array("uid" => $info['uid_to'] == $API->uid ? $info['uid'] : $info['uid_to'], "mid" => $link));
else //not message
{
  $to = null;
  $from = null;
  if( $gid > 0 )
  {
    if( $postAsPage == 1 )
    {
      $to = -1; $from = -1;
    }
  }

	$API->feedAdd("C", $comment['id'], $to, $from);

  $nType = 0;

	if ($type == "P")
	{
    $fid = quickQuery( "select fid from feed where type='$type' and link='$link'" );
		$x = sql_query("select photos.uid,aid,jmp,title,descr from photos join albums on albums.id=photos.aid where photos.id=$link");
		$nType = NOTIFY_PHOTO_COMMENT;
    $table = "photos";
	}
	elseif ($type == "V")
	{
    $fid = quickQuery( "select fid from feed where type='$type' and link='$link'" );
		$x = sql_query("select uid,title,descr,jmp from videos where id=$link");
		$nType = NOTIFY_VIDEO_COMMENT;
    $table = "videos";
	}
  elseif( $type == "W" )
  {
    $fid = quickQuery( "select fid from feed where type='$type' and link='$link'" );
    $nType = NOTIFY_WALL_COMMENT;
    $x = sql_query( "select users.uid, name as original_poster, jmp, wallposts.post as title from users left join wallposts on wallposts.uid=users.uid where wallposts.sid='$link'" );
  }
  elseif( $type == "r" ) //rss feed
  {
    $fid = quickQuery( "select fid from feed where type='rss' and link='$link'" );
    $nType = NOTIFY_RSS_COMMENT;
    $x = sql_query( "select " . $API->uid . " as uid, gname as original_poster, jmp, title, preview as descr, pages.pid as pid from pages left join rss_feed on rss_feed.gid=pages.gid where rss_feed.id='$link'" );
  }

	if (isset($x) && mysql_num_rows($x) == 1)
		$info = mysql_fetch_array($x, MYSQL_ASSOC);
  else
  {
    $info = array();
    $info['uid'] = 0;
  }

  if( $info['jmp'] == "" && ($type == "V" || $type == "P") )
  {
    $jmp = shortenMediaURL($type, $link);
    sql_query( "update $table set jmp='$jmp' where id=$link" );
    $info['jmp'] = $jmp;
  }

	$info['type'] = $type;
	$info['id'] = $link;
  $info['fid'] = $fid;

	$info['notify'] = array("uid" => $info['uid'], "type" => $type, "id" => $comment['id'], "from_uid" => $API->uid );

  $fbtarget = null;
  if( isset( $_POST['fbtarget'] ) )
  {
    $fbtarget = $_POST['fbtarget'];
    $_SESSION['fbpages']['selection'] = $fbtarget;
    if( $fbtarget == 0 )
      $fbtarget = null;
  }

  $info['fbtarget'] = $fbtarget;

  if( $nType && $info['uid'] > 0 )
  {
    $API->sendNotification($nType, $info);
  }

	//to notify other users who have commented on this previously
	$uids = array();
	$x = sql_query("select distinct uid from comments where uid not in ('" . $API->uid . "','" . $info['uid'] . "') and gid=0 and type='$type' and link=$link");
  echo mysql_error();
	while ($y = mysql_fetch_array($x, MYSQL_ASSOC))
		$uids[] = $y['uid'];

	if (count($uids) > 0)
		foreach ($uids as $uid)
		{
			$info['uid'] = $uid;
			$info['notify']['uid'] = $uid;
//      echo "sent notification to $uid <br>";
			$API->sendNotification(NOTIFY_CMTSAME, $info);
		}
}

$html = ob_get_contents();
ob_end_clean();

echo json_encode(array("hash" => $hash, "html" => $html, "id" => $comment['id'] ));

?>