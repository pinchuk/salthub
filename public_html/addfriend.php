<?php
/*
This script is called when a attempts to connect with another user, or the user accepts a pending connection request.

*/

include_once "inc/inc.php";

if( empty( $_POST['s'] ) ) $_POST['s'] = $_GET['s'];
if( empty( $_POST['uid'] ) ) $_POST['uid'] = $_GET['uid'];

$uid = intval($_POST['uid']);

if ($_POST['s'] == 0) //adding friend
{
  if( quickQuery( "select count(*) from friends where (id1='" . $API->uid . "' and id2='" . $uid . "') OR (id2='" . $API->uid . "' and id1='" . $uid . "')" ) == 0 )
  	sql_query("insert into friends (id1,id2,status) values (" . $API->uid . ",$uid,0)");
  else
  {
    //In case they sent a request to us, we should confirm it.
  	sql_query("update friends set status=1 where id1=$uid and id2=" . $API->uid);
  }

	$API->sendNotification(NOTIFY_FRIEND_ADD, array("uid" => $uid));
}
elseif ($_POST['s'] == 1) //confirming friends
{
	sql_query("update friends set status=1 where id1=$uid and id2=" . $API->uid);

	if (mysql_affected_rows() > 0)
	{
		sql_query("insert into contacts (site,uid,eid,name) values (" . SITE_OURS . ",$uid," . $API->uid . ",'" . addslashes($API->name) . "')");
		sql_query("insert into contacts (site,uid,eid,name) values (" . SITE_OURS . "," . $API->uid . ",$uid,'" . addslashes(quickQuery("select name from users where uid=$uid")) . "')");

		$fid = quickQuery("select fid from friends where id1=$uid and id2={$API->uid}");

		sql_query("insert into notifications (type,id,uid) values ('F',$fid,$uid)");
		sql_query("insert into notifications (type,id,uid) values ('F',$fid,{$API->uid})");
		$API->feedAdd("F", $fid, $uid, $API->uid);

    $API->sendNotification(NOTIFY_FRIEND_ACCEPT, array("uid" => $uid));
	}
}
else //removing friends
{
	sql_query("delete from friends where (id1=$uid and id2=" . $API->uid . ") or (id2=$uid and id1=" . $API->uid . ")");

	if (mysql_affected_rows() > 0)
	{
		sql_query("delete from contacts where uid=$uid and site=" . SITE_OURS . " and eid=" . $API->uid);
		sql_query("delete from contacts where uid=" . $API->uid . " and site=" . SITE_OURS . " and eid=$uid");
	}
  else //If this friend didn't exist, hide them!
  {
    sql_query( "insert into blocked_users (uid,blocked_uid) values ('" . $API->uid . "', '$uid')" );
  }
}

sql_query("delete from friend_suggestions where (id1=$uid and id2=" . $API->uid . ") or (id2=$uid and id1=" . $API->uid . ")");

$API->getFriendsOfFriends(true);

//mysql_close();

?>