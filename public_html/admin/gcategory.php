<?php

include "header.php";

$ctype = $_GET['ctype'];
if( empty( $ctype ) ) $ctype = $_POST['ctype'];
if( empty( $ctype ) ) $ctype = "G";
$catid = 0;


switch( $ctype )
{
  case "T": $ctype_desc = "Page Types"; $parent = Array( 0 ); break;
  case "G": $ctype_desc = "Second Tier"; $parent = Array( PAGE_TYPE_BUSINESS, PAGE_TYPE_VESSEL, PAGE_TYPE_ENTERTAINMENT, PAGE_TYPE_PROFESSIONAL ); break;
  case "C": $ctype_desc = "Company"; $parent = Array( PAGE_TYPE_BUSINESS ); break;
  case "B": $ctype_desc = "Vessel"; $parent = Array( PAGE_TYPE_VESSEL ); break;
  case "L": $ctype_desc = "Licenses and Certificates"; $parent = Array( LIC_AND_END_GRP ); break;
}


if (isset($_GET['del'])) //delete category
{
	$cat = intval($_GET['del']);
	if ($_GET['hash'] != $API->generateDeleteCommentHash("cat" . $cat)) die("Sorry");

	mysql_query("update videos set cat=1 where cat=$cat");
	mysql_query("update photos set cat=1 where cat=$cat");
	mysql_query("delete from categories where cat=$cat");
}
elseif (isset($_POST['add'])) //add category
{
  $industry = $_POST['industry'];
	mysql_query("insert into categories (catname,industry) values ('" . $_POST['catname'] . "', '$industry')");
}
elseif (isset($_POST['update'])) //update category
{
  $industry = $parent[0];
  if( isset( $_POST['industry'] ) ) $industry = $_POST['industry'];
	mysql_query("update categories set catname='" . $_POST['catname'] . "', industry='" . $industry . "' where cat=" . intval($_POST['cat']));
}

$q = sql_query( "select catname, cat from categories where cattype='T'" );
while( $r = mysql_fetch_array( $q ) )
  $industries[] = array( $r['catname'], $r['cat'] );

$x = mysql_query("select * from categories where industry IN (" . implode( ",", $parent ) . ") and cattype!='M' order by cat,catname");

echo '<h1>' . $ctype_desc . ' Categories</h1>';

echo '<table border=1 cellpadding=5 cellspacing=0><tr style="font-weight: bold;"><td>ID</td><td>Name</td>';

if( $ctype == "G" ) {
  echo '<td>Page Type</td><td colspan="2"></td>';
}

echo '</tr>';

while ($y = mysql_fetch_array($x, MYSQL_ASSOC))
{
	echo '<form action="gcategory.php" method="post"><input type="hidden" name="cat" value="' . $y['cat'] . '" /><input type="hidden" name="ctype" value="' . $ctype . '" /><tr><td>' . $y['cat'] . '</td><td><input type="text" style="width: 440px;" value="' . $y['catname'] . '" name="catname" /></td>';

  if( $ctype == 'G' )
  {
?>
  <td>
    <select name="industry">
      <option value="0">-</option>
      <?
      for( $i = 0; $i < sizeof( $industries ); $i++ )
      {
      ?>
        <option value="<? echo $industries[$i][1]; ?>"<? if( $industries[$i][1] == $y['industry'] ) echo " SELECTED";?>><? echo $industries[$i][0]; ?></option>
      <?
      }
      ?>
    </select>
  </td>
<?
  }

  echo '<td><input type="submit" name="update" class="button" value="Update" /></td><td>';

  echo '<input type="button" class="button" onclick="javascript:if (confirm(\'Are you sure?\')) location=\'gcategory.php?del=' . $y['cat'] . '&ctype=' . $ctype . '&hash=' . $API->generateDeleteCommentHash("cat" . $y['cat']) . '\';" value="Remove"';
	if ($y['cat'] <= 115) echo ' DISABLED';
  echo '/>';

	echo '</td></tr></form>';
}
echo '</table>';

?>

<p />

<form action="gcategory.php" method="post">
<input type="hidden" name="ctype" value="<? echo $ctype ?>" />
<input type="hidden" name="industry" value="<?= $parent[0] ?>" />
<input type="text" name="catname" style="width: 200px;" />
<input class="button" type="submit" name="add" value="Add Category" />
</form>

<?php include "footer.php"; ?>