<?php

include "header.php";


$titles = array( "Schools", "Employers" );
$tables = array( "education", "work" );
$fields = array( "school", "employer" );
$items_per_page = 40;
/*
if( isset( $_GET['cat'] ) && $_GET['cat'] != "" )
{
  $cat = intval($_GET['cat']);
  $sql = "select distinct " . $fields[$cat] . ", pages.* from pages inner join " . $tables[$cat] . " on pages.gid=" . $fields[$cat];
}
else
{
  $cat = "";
  $sql = "select * from pages";
}
*/

$sql = "select * from pages where 1=1";

if( isset( $_GET['cat'] ) && $_GET['cat'] != "" )
{
  $cat = intval($_GET['cat']);
  $sql .= " AND pages.cat=" . $cat;
}


if( isset( $_GET['q'] ) && $_GET['q'] != "" )
{
  $sql .= " AND pages.gname like '%" . $_GET['q'] . "%' order by gname";
}

$q = mysql_query( $sql );
$pages = ceil( mysql_num_rows( $q ) / $items_per_page );

if( isset( $_GET['p'] ) )
{
  $page = intval($_GET['p']);
  $sql .= " limit " . ($page * $items_per_page) . ",$items_per_page";
}
else
{
  $page = 0;
  $sql .= " limit $items_per_page";
}


$q = mysql_query( $sql );
echo mysql_error();

?>

<h1>Page Management</h1>

<style>
td
{
border: 1px solid black; border-left: 0; padding: 10px 4px;
}
</style>

<div>
  <div style="float:left;">
    <form method="get" action="pages.php">
    <input type="text" name="q" value="<?=$_GET['q']?>" style="width: 200px;" />
    <input type="submit" class="button" value="Search" />
    </form>
  </div>

	<div class="toplink" style="float:right; background-color:#FFFFBF; padding:3px;">
    <form action="newpage.php" method="GET" name="newgrp" onsubmit="if( this.gname.value == '' || this.gname.value=='(Page Name)' ) { alert('Invalid page name'); return false; }">
    Create New Page: <input name="gname" type="text" size="25" value="(Page Name)" onclick="if( this.value == '(Page Name)' ) this.value='';" />
    <input type="submit" name="" value="Create Page"/>
    <!--<img src="/images/page_add.png" /><a href="javascript:void(0);" onclick="javascript:showCreatePage();">Create a Page</a>-->
    </form>
  </div>

  <div style="clear:both;"></div>
  <table border="0" style="font-size: 9pt; border-left: 1px solid black; margin-top: 10px; width:780px; font-size:11px; float:left;" cellpadding="0" cellspacing="0">
  <tr style="background: black; color: white;">
    <td>ID</td>
    <td>Page Name/WWW/Email/Address</td>
    <td>Desc</td>
    <td>Admins</td>
    <td align="center">Pop<br />U<br />Lar</td>
    <td colspan="2"></td>
  </tr>
<?
  while( $r = mysql_fetch_array( $q ) )
  {
	$odd = !$odd;
	echo "<tr style=\"background: #" . ($odd ? "ddd" : "eee") . ";\">";
?>
    <td><? echo $r['gid']; ?></td>
    <td><a href="<? echo $API->getPageUrl( $r['gid'] ); ?>/about?edit" title="Edit Page"><? echo $r['gname']; ?></a><br /><? echo $r['www']; ?><br /><? echo $r['email']; ?><br /><? echo nl2br($r['address']); ?></td>
    <td><? echo nl2br($r['descr']); ?></td>
    <td>
				<?
        $gid = $r['gid'];
				$x = mysql_query("select u.uid,username,name from page_members m inner join users u on m.uid=u.uid where m.gid=$gid and m.admin=1 limit 3");
				while ($user = mysql_fetch_array($x, MYSQL_ASSOC))
				{
					echo '<a href="' . $API->getProfileURL($user['uid'], $user['username']) . '">' . $user['name'] . '</a><br />';
				}
        ?>
    </td>
    <td>
      <input type="checkbox" name="popular<? echo $r['gid']; ?>" onclick="javascript: checkPopular(<? echo $r['gid'] ?>, this.checked);"<? if( $r['popular'] ) echo " CHECKED"; ?>/>
    </td>
    <td align="center">
    <? if( $r['active'] == 1 ) { ?>
      <a href="pages_setactive.php?gid=<? echo $r['gid'] ?>&p=<? echo $_GET['p'] ?>&cat=<? echo $cat ?>&q=<? echo $_GET['q'] ?>&active=0"><img src="/images/lock.png" alt="" title="Deactivate Page" /></a>
    <? } else { ?>
      <a href="pages_setactive.php?gid=<? echo $r['gid'] ?>&p=<? echo $_GET['p'] ?>&cat=<? echo $cat ?>&q=<? echo $_GET['q'] ?>&active=1"><img src="/images/keys.png" alt="" title="Reactivate Page" /></a>
    <? } ?>
    </td>
    <td align="center">
      <? if( $r['gname'] != $siteName ) { ?>
      <a href="deletepage.php?gid=<? echo $r['gid']; ?>" onclick="return confirm('Are you sure you want to delete this page?');"><img src="../images/x.png" width="16" height="16" alt="" /></a>
      <? } ?>
    </td>
	</tr>
<?
  }
?>
  </table>

</div>

<div style="text-align: center; margin-top: 10px; font-size: 9pt; width:730px;">
<?php
for ($i = 0; $i < $pages; $i++)
	if ($i == $page)
		echo "&nbsp;" . ($i + 1);
	else
		echo ' <a href="pages.php?q=' . $_GET['q'] . '&p=' . $i . '&cat=' . $cat . '">' . ($i + 1) . '</a>';
?>
</div>

<script>

function banUser(uname, uid, active)
{
	if (confirm("Are you sure you want to " + (active == 1 ? "un" : "") + "ban " + uname + "?"))
		postAjax("/settings/setactive.php", "uid=" + uid + "&active=" + active, "banUserHandler");
}

function banUserHandler(data)
{
	location.reload(true);
}

function checkPopular(gid,checked)
{
  if( checked )
    popular = 1;
  else
    popular = 0;

  postAjax("/admin/page_setpopular.php", "gid=" + gid + "&popular=" + popular, "checkPopularHandler");
}

function checkPopularHandler(data)
{
//  alert( data );
}


</script>

<?php include "footer.php"; ?>