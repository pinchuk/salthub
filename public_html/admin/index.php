<?php
include "header.php";

include_once( "../employment/employment_functions.php" );

echo "<h1>Stuff to do</h1>";

$items = array();

$q = sql_query( "select created, id  from jobs where approved=0 and complete=2" );
while( $r = mysql_fetch_array( $q ) )
  $items[] = array( "date" => strtotime( $r['created'] ), "type" => 'J', "id" => $r['id'] );

$q = mysql_query("select ads2.start, ads2.id from ads2 inner join ad_campaigns on ads2.campaign=ad_campaigns.id where ads2.complete>=1 and ad_campaigns.status=1 and ads2.approved=0 order by ads2.approved, ads2.id desc");
//$q = sql_query( "select start, id  from ads2 where approved=0 and complete=2" );
while( $r = mysql_fetch_array( $q ) )
  $items[] = array( "date" => strtotime( $r['start'] ), "type" => 'A', "id" => $r['id'] );

$q = sql_query( "select feedback_time, comments, id from feedback where uid!=832 and uid!=2497 and done=0" );
while( $r = mysql_fetch_array( $q ) )
  $items[] = array( "date" => strtotime( $r['feedback_time'] ), "type" => 'F', "id" => $r['id'] );

$q = sql_query( "select ts, id, type from reported" );
while( $r = mysql_fetch_array( $q ) )
  $items[] = array( "date" => strtotime( $r['ts'] ), "type" => 'R' . $r['type'], "id" => $r['id'] );

$q = sql_query( "select sent, id from incoming_email where hide=0" );
while( $r = mysql_fetch_array( $q ) )
  $items[] = array( "date" => strtotime( $r['sent'] ), "type" => 'E', "id" => $r['id'] );


$sortedItems = array();

foreach($items as $item){
    foreach($item as $key=>$value){
        if(!isset($sortedItems[$key])){
            $sortedItems[$key] = array();
        }
        $sortedItems[$key][] = $value;
    }
}

$orderby = "date"; //change this to whatever key you want from the array
array_multisort($sortedItems[$orderby],SORT_DESC,$items);

foreach( $items as $item )
{
  $id = $item['id'];

  switch( substr( $item['type'], 0, 1 ) )
  {
    case 'A': $title = "Advertisement Pending Approval"; break;
    case 'J': $title = "Job Listing Pending Approval"; break;
    case 'E': $title = "Incoming Email"; break;
    case 'R':
      switch( substr( $item['type'], 1, 1 ) )
      {
        case 'P': $title = "Reported Photo"; break;
        case 'V': $title = "Reported Video"; break;
        case 'A': $title = "Reported Advertisement"; break;
        case 'J': $title = "Reported Job"; break;
        default:
          $title = "Reported Content";
        break;
      }
    break;
    case 'F': $title = "User Feedback"; break;
  }

?>
  <div style="padding-top:10px; color:#555; font-size:10pt;">
    <? echo $title . " " . date( "m/d/Y", $item['date'] ); ?>
  </div>
<?

  switch( substr( $item['type'], 0, 1 ) )
  {
    case 'A': //Advertisement
      $x = mysql_query("select * from ads2 where id='$id'");
      if( mysql_num_rows( $x ) == 0 ) continue;
      $r = mysql_fetch_array( $x );

      $hash = quickQuery( "select hash from photos where id='" . $r['pid'] . "'" );
      $url = $r['url'];

      $user = quickQuery( "select username from users where uid='" . $r['uid'] . "'");

      $imp = quickQuery( "select sum(impressions) from ad_log where ad='" . $r['id'] . "'" );
      $clk = quickQuery( "select sum(clicks) from ad_log where ad='" . $r['id'] . "'" );
    ?>
      <div class="ad" style="width:200px; cursor:pointer; overflow:hidden; height:295px; float:left; padding-left:20px;">
        <div class="title" id="adtitle"><? echo $r['title']; ?></div>
        <div class="image" id="adimg"><a href="<? echo $url ?>"><img src="/img/100x75/photos/<? echo $r['pid'] ?>/<? echo $hash ?>.jpg" width="100" height="75" /></a></div>
        <div class="body" id="adbody"><? echo $r['body'];?></div>

        <div style="font-size:8pt; color:#555; margin-top:10px;"><b>Link</b>: <a href="<? echo $url ?>"><? echo $url; ?></a></div>
        <div style="font-size:8pt; color:#555;"><b>User</b>: <a href="<? echo $API->getProfileUrl( $r['uid'] ); ?>"><? echo $user; ?></a></div>
        <div style="font-size:8pt; color:#555;"><b>Imp</b>: <? echo $imp ?></div>
        <div style="font-size:8pt; color:#555;"><b>Clk</b>: <? echo $clk ?></div>

        <div style="font-size:8pt; color:#555;">
        <? if( $r['approved'] == 0 ) { ?>
          <a href="internal_ads.php?approve=<? echo $r['id']; ?>">approve</a>
        <?
        } else echo 'approved';
        ?>
          &nbsp;&nbsp;&nbsp;&nbsp;
          <a href="internal_ads.php?del=<? echo $r['id']; ?>" onclick="return confirm('Are you sure you want to delete this ad?');">delete</a>
        </div>

      </div>
    <?
    break;

    case 'J': //Job posting
      $x = mysql_query("select * from jobs where id='$id'");
      if( mysql_num_rows( $x ) == 0 ) continue;
      $r = mysql_fetch_array( $x );

      $hash = quickQuery( "select hash from photos where id='" . $r['pid'] . "'" );
      $url = $r['url'];

      $user = quickQuery( "select username from users where uid='" . $r['uid'] . "'");
      ?>
      <div class="job_listing" style="padding:5px; float:left; overflow:hidden; width:535px;">
        <div style="width:430px; overflow:hidden; float:left; margin-top:15px;">
          <div class="image" id="img"><img src="/img/100x75/photos/<? echo $r['pid'] ?>/<? echo $hash ?>.jpg" width="100" height="75" /></div>
          <div class="title" id="title"><a href="/employment/jobs_detail.php?d=<? echo $r['id']; ?>"><? echo $r['title']; ?></a> - <span class="subtitle2"><? echo getSubTitle( $r ); ?></span></div>
          <div class="body" id="body"><? echo nl2br( cutOffText( $r['body'], 100 ) );?></div>
        </div>

        <div style="font-size:8pt; color:#555;"><b>User</b>: <a href="<? echo $API->getProfileUrl( $r['uid'] ); ?>"><? echo $user; ?></a></div>

        <div style="font-size:8pt; color:#555;">
        <? if( $r['approved'] == 0 ) { ?>
          <a href="jobs_admin.php?approve=<? echo $r['id']; ?>">approve</a>
        <?
        } else echo 'approved';
        ?>
          &nbsp;&nbsp;&nbsp;&nbsp;
          <a href="jobs_admin.php?del=<? echo $r['id']; ?>" onclick="return confirm('Are you sure you want to delete this ad?');">delete</a>
        </div>
      </div>
      <?
    break;

    case 'R': //Reported content
      $x = mysql_query("select rid,users.uid,username,name,type,reported.id,report.reason,ts from reported inner join report on report.id=reported.reason inner join users on reported.uid = users.uid where reported.id=$id");

      if( mysql_num_rows( $x ) == 0 ) continue;
      $r = mysql_fetch_array( $x, MYSQL_ASSOC );

    	$uid = $r['uid'];
    	$id = $r['rid'];
    	unset($r['rid']);

      $mediaid = $r['id'];
      $type = $r['type'];

      $title = "";
      if( $type == "P" || $type == "V" )
      {

        $title = current( $API->getMediaInfo( $type, $mediaid, ($type=="P" ? "ptitle" : "title") ) );

        if( $title == "" )
          $title = current( $API->getMediaInfo( $type, $mediaid, "title" ) );
      	$r['type'] = '<a href="' . $API->getMediaURL($r['type'], $r['id']) . '">' . $title . '</a>';
      }
      elseif( $type == 'A' )
      {
        $r['type'] = '<a href="internal_ads.php">(Ad; See Internal Ads)</a>';
      }
      elseif( $type == 'j' )
      {
        $r['type'] = '<a href="jobs_admin.php">(Job; See Internal Jobs)</a>';
      }
?>
      <div style="font-size:9pt; margin-top:5px;">
<?
    	if ($r['uid'] == 0)
    		$r['uid'] = "Anonymous";
    	else
    		$r['uid'] = '<a href="' . $API->getProfileURL($r['uid'], $r['uname']) . '">' . $r['name'] . '</a> (' . $r['uid'] . ')';

      echo "\"" . $r['type'] . "\", Reported By: " . $r['uid'] . ", Reason: " . $r['reason'] . "<br />";

    	echo '<a href="compose.php?uid=' . $uid . '"><img src="/images/message.png" width="24" height="24" alt="" /></a>';
    	echo '<a href="reported.php?r=' . $id . '&type=' . $type . '&mid=' . $mediaid . '"><img src="/images/redx.png" width="24" height="24" alt="" /></a>';
    	echo '<a href="reported.php?ok=' . $mediaid . '&type=' . $type . '&rid=' . $id. '"><img src="/images/check.png" width="24" height="24" alt="" /></a>';
?>
      </div>
<?
    break;

    case 'F':
      $x = mysql_query("select * from feedback where id='$id'");
      if( mysql_num_rows( $x ) == 0 ) continue;
      $r = mysql_fetch_array( $x );

      $r['name'] = quickQuery( "select name from users where uid='" . $r['uid'] . "'" );
      $pic = quickQuery( "select pic from users where uid='" . $r['uid'] . "'" );
?>
      <div style="font-size:8pt; margin-top:5px; width:520px;">
      <b>Feedback from <? echo '<a href="' . $API->getProfileURL($r['uid']) . '">' . $r['name'] . '</a> (' . $r['uid'] . ')'; ?>:</b><br />
      <? echo nl2br( $r['text'] ); ?><br />
      <a href="deletefeedback.php?id=<? echo $r['id']; ?>&action=done">Flag as done</a>&nbsp;&nbsp;&nbsp;&nbsp;
      <a href="deletefeedback.php?id=<? echo $r['id']; ?>&action=delete" onclick="return confirm('Are you sure you want to delete this feedback?');">Delete</a>&nbsp;&nbsp;&nbsp;&nbsp;      
      <a href="javascript:void(0);" onclick="openSendMessagePopup('','','<?=$r['name']?>',<?=$pic?>,<?=$r['uid']?>,0,0 );">Email User</a>
      </div>
<?
    break;

    case 'E':
      $x = mysql_query("select * from incoming_email where id='$id'");
      if( mysql_num_rows( $x ) == 0 ) continue;
      $r = mysql_fetch_array( $x );

?>
      <div style="font-size:8pt; margin-top:5px; width:520px;">
      <b>To:</b> <? echo $r['to_email']; ?><br />
      <b>From:</b> <? echo $r['from_email']; ?><br />
      <b>Subject:</b> <? echo $r['subject']; ?><br />
      <b>Message</b><br /> <? echo $r['message']; ?><br />
      <a href="deletefeedback.php?id=<? echo $r['id']; ?>&action=hideemail">Hide this email</a>
      <? if( $r['uid'] > 0 ) { ?> or
      <a href="compose.php?uid=<?=$r['uid']?>&manageusers=1">reply</a>
      <? } ?>
      </div>
<?
    break;

  }
?>
  <div style="clear:both; border-bottom:1px dotted #326798; width:535px; margin-top:10px;"></div>
<?
}
?>




<?php include "footer.php"; ?>