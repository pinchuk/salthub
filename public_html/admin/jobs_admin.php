<?php

include "header.php";

include_once( "../employment/employment_functions.php" );

?>
<h1>Job Listings</h1>
<?
if (isset($_GET['del']))
{
	mysql_query("delete from jobs where id=" . intval($_GET['del']) . " limit 1");
}

if (isset($_GET['approve']))
{
	mysql_query("update jobs set approved=1 where id=" . intval($_GET['approve']) . " limit 1");

  $gid = quickQuery( "select gid from jobs where id=". intval($_GET['approve']) );
  if( $gid > 0 )
    $API->sendNotificationToPage( NOTIFY_GROUP_JOB_POSTED, array( "from" => 0, "gid" => $gid, "id" => intval($_GET['approve']) ) );
}

if (isset($_GET['unapprove']))
{
	mysql_query("update jobs set approved=0 where id=" . intval($_GET['unapprove']) . " limit 1");
}

$reported = array();
$q = sql_query( "select id from reported where type='j'" );
while( $r = mysql_fetch_array( $q ) )
  $reported[] = $r['id'];

$x = mysql_query("select * from jobs where complete=2 or approved=1 order by id");
?>
<div style="width:600px;">
<?
while( $r = mysql_fetch_array( $x ) )
{
  $hash = quickQuery( "select hash from photos where id='" . $r['pid'] . "'" );
  $url = $r['url'];

  $user = quickQuery( "select username from users where uid='" . $r['uid'] . "'");

?>

  <div class="job_listing" style="padding:5px; float:left; overflow:hidden; border-bottom:1px dotted #326798; width:535px;">
    <div style="width:430px; overflow:hidden; float:left; margin-top:15px;">
      <div class="image" id="img"><img src="/img/100x75/photos/<? echo $r['pid'] ?>/<? echo $hash ?>.jpg" width="100" height="75" /></div>
      <div class="title" id="title"><a href="/employment/jobs_detail.php?d=<?=$r['id']; ?>"><? echo $r['title']; ?></a> - <span class="subtitle2"><? echo getSubTitle( $r ); ?></span></div>
      <div class="body" id="body"><? echo nl2br( cutOffText( $r['body'], 100 ) );?></div>
    </div>

    <div style="font-size:8pt; color:#555;"><b>User</b>: <a href="<? echo $API->getProfileUrl( $r['uid'] ); ?>"><? echo $user; ?></a></div>

    <div style="font-size:8pt; color:#555;">
    <? if( $r['approved'] == 0 ) { ?>
      <a href="jobs_admin.php?approve=<? echo $r['id']; ?>">approve</a>
    <?
    } else echo 'approved (<a href="jobs_admin.php?unapprove=' . $r['id'] . '">unapprove</a>)';
    ?>
      &nbsp;&nbsp;&nbsp;&nbsp;
      <a href="jobs_admin.php?del=<? echo $r['id']; ?>" onclick="return confirm('Are you sure you want to delete this ad?');">delete</a>
    </div>
    <?
    if( in_array( $r['id'], $reported ) )
    {
    ?>
      <div style="font-size:8pt; color:#FF6666; font-weight:bold;">Reported</div>
    <?
    }
    ?>

  </div>

  <div style="clear:both;"></div>
<?
}
?>
</div>
<?php include "footer.php"; ?>