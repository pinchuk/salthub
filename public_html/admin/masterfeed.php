<?php
$scripts[] = "/comments.js";
$scripts[] = "/profile/profile.js";

include "header.php";
include "../inc/mod_comments.php";

if( !isset( $_GET['f'] ) ) $_GET['f'] = "4";

?>
<div style="margin-bottom:10px;">
  <div style="float:left;">
    <h1>Master Feed</h1>
  </div>
  <div style="float:right; margin-top:10px; font-size:9pt;">
    Viewing:
    <select onchange="javascript:window.location='masterfeed.php?f=' + selectedValue( this );">
      <option value="0" <?if( $_GET['f'] == "0" ) echo " SELECTED"; ?>>All</option>
      <option value="4" <?if( $_GET['f'] == "4" ) echo " SELECTED"; ?>>No RSS Feeds &amp; Vessel Updates</option>
      <option value="1" <?if( $_GET['f'] == "1" ) echo " SELECTED"; ?>>RSS Feeds</option>
      <option value="2" <?if( $_GET['f'] == "2" ) echo " SELECTED"; ?>>New Users</option>
      <option value="3" <?if( $_GET['f'] == "3" ) echo " SELECTED"; ?>>Vessel Updates</option>
    </select>
  </div>
</div>

<div style="float: left; width: 441px;">
  <div class="subhead" style="margin-bottom: 5px;">
  	Log Entries
  </div>
  <?php
  include_once "../profile/getlogentries.php";
  //show log entries and return the last feedno (# of feed headings), last feed id, and the name of the next empty div
  echo '<div style="clear: both;" id="feeds">';

  switch( intval( $_GET['f'] ) )
  {
    case 1: $filter = -8; break;
    case 2: $filter = -9; break;
    case 3: $filter = -10; break;
    case 4: $filter = -11; break;
    default: $filter = -2;
  }

  $gleResult = getLogEntries(array("uid" => $filter, "gid" => $gid));
  echo '</div><div style="clear: both; height: 15px;"></div>';

  if ($gleResult['more']) // there are more feed entries left
  {
  ?>
  <div id="feedshowmore" class="feedshowmore">
  	<a href="javascript:void(0);" onclick="javascript:showMoreLogEntries();">show more &#0133;</a>
  </div>
  <?php } ?>
  </div>

<script language="javascript" type="text/javascript">
<!--
var fidold = <?=intval($gleResult['fidold'])?>;
var fidnew = <?=intval($gleResult['fidnew'])?>;
var fidlink = <?=intval($gleResult['fidlink'])?>;

function banUser(uname, uid, active)
{
	if (confirm("Are you sure you want to " + (active == 1 ? "un" : "") + "ban " + uname + "?"))
		postAjax("/settings/setactive.php", "uid=" + uid + "&active=" + active, "banUserHandler");
}

function showNewLogEntries()
{
	url = "/profile/getlogentries.php?fidlink=" + fidlink + "&gid=<?=$gid?>&uid=<?=$filter?>&newerthan=" + fidnew;

	getAjax(url, function(data)
	{
		eval("json = " + data + ";");

//		if (json.fidnew == null)
//			return; //nothing new

        console.log('before add feed: ' + fidnew);
        if (fidnew < json.fidnew)
		    addFeed(json.html, true);

        if (json.fidnew != null)
		    fidnew = json.fidnew;

        console.log('json.fidnew: '+json.fidnew);
        console.log('after add feed: ' + fidnew);
	});
}

function addFeed(html, top)
{
	newdiv = document.createElement("div");
	newdiv.innerHTML = html;
	newdiv.id = "newfeed" + new Date().getTime();

	e = document.getElementById("feeds");

	if (top)
		e.insertBefore(newdiv, e.firstChild);
	else
		e.appendChild(newdiv);

	h = getHeight(newdiv);
	slidedown(newdiv.id, h, true);
}

function showMoreLogEntries()
{
	url = "/profile/getlogentries.php?gid=<?=$gid?>&uid=<?=$filter?>&olderthan=" + fidold;
	log(url);

	$.ajax({
		type: 'GET',
		url: url,
		success: function(data)
		{
			json = json_decode(data);
			console.log(json);

			if (json.more == "0") //reached the end of the user's feed
				document.getElementById("feedshowmore").style.display = "none";
			else if (json.fidnew == null)
			{
				fidold -= 50;
				return; //didn't return any results - user probably should not have gotten here
			}

			addFeed(json.html, false);

			fidold = json.fidold;
		}
	});
}

function showTip() {}
function tipMouseOut() {}

setInterval("showNewLogEntries()", 10000);
//-->
</script>

<?php include "footer.php"; ?>