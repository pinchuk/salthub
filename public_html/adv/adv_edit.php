<?
/*
Tool to edit an advertisment
*/


$scripts[] = "/adv/photouploader.js";

$name_default = "My Advertisment";
$title_default = "Use " . $siteName;
$body_default = "A social network for professionals on the water";
$www_default = "http://www." . $siteName . ".com";
?>

    <div class="thead" style="width:100px;">Title:</div>
    <div class="tcontent" style="width:360px;">
      <input type="text" name="title" id="title" maxlength="25" value="<?if( isset( $r ) ) echo $r['title']; else echo $title_default?>" style="width: 330px;" onchange="javascript: refreshAd();"  onkeyup="javascript: refreshAd();" onclick="javascript: if( this.value=='<?=addslashes($title_default)?>' ) this.value='';"/>
    </div>

    <div class="thead" style="width:100px;">Image:</div>
    <div class="tcontent" style="width:360px;">
     	<div id="pccontainer" style="padding-left: 0px; float: left;"></div>
      <!--<input name="image" type="file" size="28"/>-->
    </div>

    <div class="thead" style="width:100px;">Body:</div>
    <div class="tcontent" style="width:360px;">
      <textarea id="body" name="body" style="width:150px; height:70px;" onchange="javascript: refreshAd();" onkeyup="javascript: refreshAd();" onclick="javascript: if( this.value=='<?=$body_default?>' ) this.value='';"><?if( isset( $r ) ) echo $r['body']; else echo $body_default; ?></textarea>
    </div>

    <div class="thead" style="width:100px;">URL:</div>
    <div class="tcontent" style="width:360px;">
      <input type="text" name="url" id="url" maxlength="255" style="width: 330px;" value="<?if( isset( $r ) ) echo $r['url']; else echo $www_default; ?>" onclick="javascript: if( this.value=='<?=$www_default?>' ) this.value='';"/>
    </div>

<? if( $r['complete'] > 0 ) { ?>
    <div class="thead" style="width:100px;">Status:</div>
    <div class="tcontent" style="width:360px;">
      <select name="complete">
        <option value="1"<? if( $r['complete'] == 1 ) echo " SELECTED";?>>Disabled</option>
        <option value="2"<? if( $r['complete'] == 2 ) echo " SELECTED";?>>Active</option>
      </select>
    </div>
<? } ?>
    <div style="clear:both;"></div>

    <div class="strong" style="color: #326798; margin-top:3px; margin-left:110px; font-size:13pt;">Advertisement Preview</div>

    <div style="background-color:#ffffff; border:1px solid #d8dfea; margin-left:110px; width:160px;">
      <div class="ad" style="padding:5px;">
        <div class="title" id="adtitle"><?if( isset( $r ) ) echo $r['title']; else echo $title_default?></div>
        <div class="image" id="adimg"><? if( isset( $r ) ) echo '<img src="/img/119x95/photos/' . $r['pid'] . '/' . $hash2 . '.jpg" width="119" height="95" />'; else echo '<img src="/images/salt_badge100x75.png" width="119" height="95" />'; ?></div>
        <div class="body" id="adbody"><? if( isset( $r ) ) echo $r['body']; else echo $body_default?></div>
      </div>
    </div>

