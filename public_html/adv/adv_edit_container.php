<?php
/*
Container frame for the edit advertisment tool. (used by the edit popup when editing a campaign)
*/

require "../inc/inc.php";

$popup = true;

if( isset($_GET['ID']) )
{
  $ID = intval( $_GET['ID'] );
  $r = queryArray( "select * from ads2 where id='$ID' and uid='" . $API->uid . "'" );
  $pid = $r['pid'];
  $campaign = $r['campaign'];
  $hash2 = quickQuery( "select hash from photos where id=" . $r['pid'] );
}
else
{
  $ID = 0;
  $campaign = intval($_GET['c']);
}
?>

<form action="adv_edit_save.php" method="POST">
<input type="hidden" name="ID" value="<? echo $ID ?>" />
<input type="hidden" name="campaign" value="<? echo $campaign ?>" />
<input type="hidden" id="pid" name="pid" value="0" />

<div class="contentborder"<? if( $popup ) echo ' style="width:525px;"'; ?>>
  <div class="strong" style="<? if( !$popup ) echo 'margin-top:20px;'; ?> font-size:13pt;">Ad settings</div>

  <div style="background-color:#f3f8fb; border:1px solid #d8dfea; margin-top:3px; width:500px; float:left; padding-bottom:9px; padding-top:6px;">
    <? include( "adv_edit.php" ); ?>

    <div style="clear:both;"></div>
  </div>

  <div style="clear:left; float:left;">
    <input style="margin-left:3px; margin-top:10px;" type="submit" class="button" name="save" value="Save &amp; Close" onclick="return checkEditForm();"/>
  </div>

  <div style="clear:both;"></div>
</div>

</form>