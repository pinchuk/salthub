<?php
/*
Step 3 of advertisment creation.
*/

include "../inc/inc.php";

$scripts[] = "/adv/advertising.js.php";

$ID = $_GET['ID'];

if( isset( $_GET['popup'] ) ) $popup = true;
else $popup = false;

$q = mysql_query( "select * from ad_campaigns where ID='$ID'" );

if( mysql_num_rows( $q ) == 0 )
  die( "invalid campaign!" );

$r = mysql_fetch_array( $q );

if( !$popup ) include "../header.php";

$start = strtotime( $r['start'] );
if( date("Y", $start) < 2010 ) $start = time();
$start = array( date( "Y", $start ), date( "m", $start ), date( "d", $start ) );

$end = strtotime( $r['end'] );
if( date("Y", $end) < 2010 ) $end = time() + 2592000;
$end = array( date( "Y", $end ), date( "m", $end ), date( "d", $end ) );

?>


<form action="create4.php" method="POST">
<input type="hidden" name="ID" value="<? echo $ID ?>" />
<input type="hidden" name="popup" value="<?= ($popup?"1":"0"); ?>"/>
<? if( !$popup ) include( "adv_top_menu.php" ); ?>

<div class="contentborder"<? if( $popup ) echo ' style="width:525px;"'; ?>>
  <div class="strong" style="<? if( !$popup ) echo 'margin-top:20px;'; ?> font-size:13pt;">Campaign Schedule</div>

  <div style="background-color:#f3f8fb; border:1px solid #d8dfea; margin-top:3px; width:500px; float:left; padding-bottom:9px; padding-top:6px;">
    <? //include( 'adv_create_menu.php' ); ?>

    <div style="color:#326798; font-size:9pt; font-weight:bold;  padding-left:5px; padding-top:5px;">Run Time</div>
    <div style="float:right; width:200px; font-size:8pt; color:#555; padding-right:5px; padding-top:12px;">Tip: Continuously is recommended and allows your campaign to run without interruption.</div>

    <div class="thead" style="width:100px; float:left;">Continuously:</div>

    <div class="tcontent" style="width:30px; padding-left:0px; margin-left:0px; margin-top:3px; text-align:left;">
      <input type="radio" style="width:20px;" name="runtime" value="0" <?if( $r['runtime'] == 0 ) echo 'checked="checked"'; ?> onchange="if( this.checked ) document.getElementById('select_run_time').style.display = 'none'; else document.getElementById('select_run_time').style.display = '';"/>
    </div>

    <div class="thead" style="width:100px;">End Time:</div>
    <div class="tcontent" style="width:30px; padding-left:0px; margin-top:3px; margin-left:0px; text-align:left;">
      <input type="radio" style="width:20px;" name="runtime" value="1" <?if( $r['runtime'] == 1 ) echo 'checked="checked"'; ?> onchange="if( this.checked ) document.getElementById('select_run_time').style.display = ''; else document.getElementById('select_run_time').style.display = 'none';"/>
    </div>

    <div id="select_run_time"  <?if( $r['runtime'] != 1 ) { echo 'style="display:none;"'; } ?>>
      <div class="thead" style="width:100px;">Start Date:</div>
      <div class="tcontent" style="width:250px; padding-left:0px; margin-left:0px; padding-top:5px;">
    		<select name="startm"><option></option>
    		<?php
    		for ($i = 1; $i <= 12; $i++)
    		echo "<option value=\"$i\" " . (intval($start[1]) == $i ? "selected" : "") . ">" . date("F", strtotime("2010-$i-1")) . "</option>";
    		?>
    		</select>
    		<select name="startd"><option></option>
    		<?php
    		for ($i = 1; $i <= 31; $i++)
    		echo "<option value=\"$i\" " . (intval($start[2]) == $i ? "selected" : "") . ">$i</option>";
    		?>
    		</select>
    		<select name="starty"><option></option>
    		<?php
    		for ($i = date("Y"); $i <= date("Y") + 3; $i++)
    		echo "<option value=\"$i\" " . (intval($start[0]) == $i ? "selected" : "") . ">$i</option>";
    		?>
    		</select>
      </div>

      <div class="thead" style="width:100px;">End Date:</div>
      <div class="tcontent" style="width:250px; padding-left:0px; margin-left:0px; padding-top:5px;">

  		<select name="endm"><option></option>
  		<?php
  		for ($i = 1; $i <= 12; $i++)
  		echo "<option value=\"$i\" " . (intval($end[1]) == $i ? "selected" : "") . ">" . date("F", strtotime("2010-$i-1")) . "</option>";
  		?>
  		</select>
  		<select name="endd"><option></option>
  		<?php
  		for ($i = 1; $i <= 31; $i++)
  		echo "<option value=\"$i\" " . (intval($end[2]) == $i ? "selected" : "") . ">$i</option>";
  		?>
  		</select>
  		<select name="endy"><option></option>
  		<?php
  		for ($i = date("Y"); $i <= date("Y") + 3; $i++)
  		echo "<option value=\"$i\" " . (intval($end[0]) == $i ? "selected" : "") . ">$i</option>";
  		?>
  		</select>

      </div>
    </div>

    <div style="padding-top:15px; clear:both;"></div>

    <div style="color:#326798; font-size:9pt; font-weight:bold;  padding-left:5px; padding-top:5px;">Impressions &amp; Budget</div>

    <div class="thead" style="width:100px;">Type:</div>
    <div style="float:left; width:300px; padding-left:0px; margin-left:0px; padding-top:8px; color:#555;">
      <div style="float:left;">
        <input type="radio" value="0" name="budget_type" CHECKED/>
      </div>
      <div style="float:left; font-size:8pt;">
        CPM = cost per 1,000 impressions<br />Current CPM Rate = $<? echo strip_tags( str_replace( "\n", "", quickQuery( "select content from static where id='cpm_rate'" ) ) ); ?>.00 (USD)
      </div>
    </div>

    <div style="clear:both; padding-top:5px;"></div>

    <div class="thead" style="width:100px;">Impressions:</div>
    <div style="float:left; width:300px; padding-left:0px; margin-left:0px; padding-top:8px; color:#555; font-size:8pt;">
      <select name="daily_budget">
      <option value="200"<? if( $r['daily_budget'] == 200 ) echo " SELECTED"; ?>>200</option>
      <option value="500"<? if( $r['daily_budget'] == 500 ) echo " SELECTED"; ?>>500</option>
      <? for( $c = 1; $c <= 15; $c++ ) { ?><option value="<? echo $c ?>000"<? if( $r['daily_budget'] == $c * 1000 ) echo " SELECTED"; ?>><? echo $c ?>,000</option><? } ?>
      </select>
      impressions daily
      <!--
      <select name="budget_time_period">
      <option value="1"<? if( $r['budget_time_period'] == 1 ) echo " SELECTED"; ?>>daily</option>
      <option value="7"<? if( $r['budget_time_period'] == 7 ) echo " SELECTED"; ?>>weekly</option>
      <option value="30"<? if( $r['budget_time_period'] == 30 ) echo " SELECTED"; ?>>monthly</option>
      </select>
      -->
    </div>

<? if( $r['status'] > 0 ) { ?>
    <div style="clear:both;"></div>
    <div class="thead" style="width:100px;">Status:</div>
    <div style="float:left; width:300px; padding-left:0px; margin-left:0px; padding-top:8px; color:#555; font-size:8pt;">
      <select name="status">
        <option value="1"<? if( $r['status'] == 1 ) echo ' SELECTED'; ?>>Active</option>
        <option value="2"<? if( $r['status'] == 2 ) echo ' SELECTED'; ?>>Disabled</option>
      </select>
    </div>
<? } ?>
    <div style="clear:both;"></div>

    </div>
    <div style="clear:both;"></div>


  </div>


  <? // include( "creatives.php" ); ?>

  <div style="clear:left; float:left;">
    <? if( !$popup ) { ?>
      <input style="margin-left:3px; margin-top:10px;" type="submit" class="button" name="next" value="Next - Billing &amp; Start Campaign &gt;"/>
      <input style="margin-left:3px; margin-top:10px;" type="submit" class="button" name="save" value="Save &amp; Complete Later"/>
    <? } else { ?>
      <input style="margin-left:3px; margin-top:10px;" type="submit" class="button" name="save" value="Save &amp; Close"/>
    <? } ?>

  </div>

  <div style="clear:both;"></div>

</form>

<script type="text/javascript">
<!--


-->
</script>
<?
if( !$popup ) include "../footer.php";
?>