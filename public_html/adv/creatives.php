<?
/*
List of all a user's advertisments. Also shows the status of their ads (i.e., approved, not approved.)
*/

include_once( "../inc/inc.php" );

if( $script == "adv/view_creatives" ) echo '<div style="margin-left:20px; width:100%; float:left;">';
else echo '<div style="margin-left:20px; margin-top:-35px; width:300px; float:right;">';
?>

    <div class="strong" style="margin-top:10px; font-size:13pt;">
		  Your Creatives
		</div>

<?
$q = sql_query( "select * from ads2 where uid='" . $API->uid . "'" );

while( $r = mysql_fetch_array( $q ) )
{
  $hash = quickQuery( "select hash from photos where id=" . $r['pid'] );

?>
    <div class="ad" style="padding:5px; float:left; height:200px;">
      <div class="title" id="adtitle"><? echo $r['title']; ?></div>
      <div class="image" id="adimg"><img src="/img/100x75/photos/<? echo $r['pid'] ?>/<? echo $hash ?>.jpg" width="100" height="75" /></div>
      <div class="body" id="adbody"><? echo $r['body'];?></div>
      <div style="font-size:8pt; margin-top:10px;"><b>Status:</b> <? echo getStatus( $r['complete'] ); ?></div>
      <div style="font-size:8pt; font-weight:bold;"><? if( $r['approved'] == 0 ) echo 'Pending Approval'; else echo 'Approved'; ?></div>
      <div style="font-size:8pt; margin-top:3px; margin-left:10px;"><a href="create.php?ID=<? echo $r['id']; ?>">edit</a>&nbsp;&nbsp;&nbsp;<a href="delete_ad.php?ID=<? echo $r['id']; ?>" onclick="return confirm('Are you sure you want to delete this ad?');">delete</a></div>
    </div>
<?
}
?>
  </div>
<?
function getStatus( $state )
{
  switch( $state )
  {
    case 0: return "incomplete";
    case 1: return "inactive";
    case 2: return "active";
  }
  return "unknown";
}
?>