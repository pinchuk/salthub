// Used in the ad creation/editing process to upload a photo for the ad.

var pcPhotos = [];
var pcPhotos = [];
var pcCurPage = 0;
var pcPerPage = 9;
var pcPhotoWidth = 119;
var pcPhotoHeight = 95;
var pcTween;
var pcMore = 1;
var pcCacheReady = false;
var pcswfu;

html  = '<div id="photochanger">';
html +=	'	<div>';
html += '		<div id="pcuploadlinkcontainer" style="float: right; padding-right: 20px;"><span id="pcuploadlink">wait &#0133;</span></div>';
html += '		<div id="pcprogresscontainer" class="progress">';
html +=	'			<div id="pcprogress" style="height: 3px; width: 0px; background: #C0D9EC; margin-top: 5px; overflow: hidden;">&nbsp;</div>';
html +=	'		</div>';
html += '	</div>';
html += '	<div style="clear: both;"></div>';


pcInitUpload();

function pcInitUpload()
{
  if( document.getElementById("pccontainer") == null )
    return;

  document.getElementById("pccontainer").innerHTML = html;

	delete(pcswfu);

	pcswfu = new SWFUpload({
	debug: false,
	file_upload_limit : 0,
	file_queue_limit : 1,

	file_types : "*.jpg; *.jpeg; *.png; *.gif",
	file_types_description: "Images",

	upload_url: "/upload/photo.php?pc=2&s=" + session_id,
	flash_url: '/upload/swfupload.swf',
	flash9_url: '/upload/swfupload_fp9.swf',
	button_text: '<span class="xyzzy">upload</span> <span style="color:#555;">a new photo (119x95 recommended)</span>',
	button_action: SWFUpload.BUTTON_ACTION.SELECT_FILE,
	button_width: 242,
	button_height: 20,
	file_size_limit: "7 MB",
	disableDuringUpload: true,

	// Button Settings
	button_placeholder_id : 'pcuploadlink',
	button_text_style : '.xyzzy { font-size: 11; font-family: arial; color: #326798; text-align: right; }',
	button_text_top_padding: 0,
	button_text_left_padding: 0,
	button_window_mode: SWFUpload.WINDOW_MODE.TRANSPARENT,
	button_cursor: SWFUpload.CURSOR.HAND,

	file_queued_handler: function (x) { toggleUpload(true, null); },
	file_queue_error_handler : function (x, y, msg) { showPopUp2("Queue Error", msg); },
	upload_error_handler : function (x, y, msg) { showPopUp2("Upload Errora", msg); toggleUpload(false, null); },
	upload_progress_handler : function (x, done, total)
	{
		document.getElementById("pcprogress").style.width = Math.round(done / total * 100) + "px";
	},
	upload_success_handler : function (x, json) {
    //alert( json );
		eval("data = " + json);
		toggleUpload(false, data);
	}
	});
}

function toggleUpload(yes, photo)
{
	if (yes)
	{
		pcswfu.startUpload();
		pcswfu.setButtonDisabled(true);
		document.getElementById("pcprogress").style.width =  0;
		document.getElementById("pcprogresscontainer").style.display = "inline";
	}
	else
	{
		document.getElementById("pcprogresscontainer").style.display = "none";

		pcAddPhoto(photo, true);
		pcswfu.setButtonDisabled(false);
	}
}

function pcAddPhoto(photo, begin)
{
	e = document.getElementById("adimg");

  if( e )
  {
    //alert( photo.img );
    e.innerHTML = '<img src="' + photo.img + '" width="' + pcPhotoWidth + '" height="' + pcPhotoHeight + '"/>';
  }

  pid = photo.id;

//	newpic.src = photo.thumb;
}



