<?php

include_once '../inc/inc.php';

header('Content-Type: application/json');

$limit = intval($_POST['limit']);

if (isset($_POST['items']))
	$items = json_decode(html_entity_decode($_POST['items']), true);
else
	$items = array(array('type' => $_POST['type'], 'link' => $_POST['link']));
	
$return = array();
foreach ($items as $item)
{
	$type = addslashes($item['type']);
	$link = intval($item['link']);

	$query = "select users.uid,likes.likes,users.name,users.company,replace(users.username, ' ', '.') as username,users.pic as pid,photos.hash as hash,container_url from likes
				inner join users on users.uid=likes.uid
				left join photos on users.pic=photos.id
				where type='$type' and link=$link";

	$x = sql_query($query);
	$nFeedback = mysql_num_rows($x);
	$nLikes = quickQuery("select count(*) from likes where type='$type' and link=$link and likes=1");
	$nDislikes = $nFeedback - $nLikes;

	if ($limit == 0)
		$limit = $nFeedback;

	$this_user_likes = -1; // does the logged in user like this?

	$dLikes = array();
	$dDislikes = array();

	$i = 0;
	if ($nFeedback > 0)
		while ($y = mysql_fetch_array($x, MYSQL_ASSOC))
		{
			if ($y['likes'] == '1')
				$dLikes[] = $y;
			else
				$dDislikes[] = $y;
			
			if ($API->isLoggedIn() && $y['uid'] == $API->uid)
				$this_user_likes = $y['likes'];
			
			if (++$i == $limit)
				break;
		}

	if ($this_user_likes == -1) // try one last time to see if this user has left feedback on this item
	{
		$like = quickQuery("select likes from likes where type='$type' and link=$link and uid={$API->uid}");
		if ($like != "")
			$this_user_likes = $like;
	}
	
	$return[] = array("type" => $type, "nLikes" => $nLikes, "nDislikes" => $nDislikes, "link" => $link, "this_user_likes" => $this_user_likes, "likes" => $dLikes, "dislikes" => $dDislikes);
}

echo json_encode($return);

?>