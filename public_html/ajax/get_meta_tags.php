<?php

header('Content-Type: application/json');

$url = trim($_GET['url']);

if (substr(strtolower($url), 0, 4) == 'www.')
	$url = "http://$url";

function file_get_contents_curl($url)
{
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

//    if (strpos($url, '?') == false)
//        $url.=$url.'?curl=1';
//    else
//        $url.=$url.'&curl=1';

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

    $data['html'] = curl_exec($ch);
	$data['response_code'] = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);

    return $data;
}

$data = file_get_contents_curl($url);

$doc = new DOMDocument();
@$doc->loadHTML($data['html']);

$nodes = $doc->getElementsByTagName('title');
$title = $nodes->item(0)->nodeValue;

//$xpath = new DOMXPath($doc);
//$query = '//*/meta[starts-with(@property, \'og:\')]';
//$metas = $xpath->query($query);
//
//foreach ($metas as $meta) {
//    $property = $meta->getAttribute('property');
//    $content = $meta->getAttribute('content');
//    $rmetas[$property] = $content;
//}


$metas = $doc->getElementsByTagName('meta');

for ($i = 0; $i < $metas->length; $i++)
{
    $meta = $metas->item($i);
    if ($meta->getAttribute('name') == 'description')
        $descr = $meta->getAttribute('content');
    //if ($meta->getAttribute('name') == 'keywords')
        //$keywords = $meta->getAttribute('content');
}



$metas = $doc->getElementsByTagName('link');

for ($i = 0; $i < $metas->length; $i++)
{
    $meta = $metas->item($i);
    if ($meta->getAttribute('rel') == 'image_src')
        $images[] = $meta->getAttribute('href');
}

$metas = $doc->getElementsByTagName('img');

for ($i = 0; $i < $metas->length; $i++)
{
	$meta = $metas->item($i);
	$images[] = $meta->getAttribute('src');
}

$i = strpos($url, '/', 9);
if ($i > 0)
	$domain = substr($url, 0, $i);
else
	$domain = $url;

for ($i = 0; $i < count($images); $i++)
{
	$image = $images[$i];
	
	if (substr($image, 0, 4) != 'http')
		$image = "$domain/$image";
	
	$images[$i] = $image;
}

$images[] = 'http://www.salthub.com/images/noimage.jpg';

$ret = array('response_code' => $data['response_code'], 'title' => clean($title), 'descr' => clean($descr), 'images' => $images);
echo json_encode($ret);

function clean($what)
{
	return trim(iconv("UTF-8", "ASCII//IGNORE", $what));
}

?>