<?php

include_once '../inc/inc.php';
header('Content-Type: application/json');

$show = false;
if (empty($_GET['t'])){
    $show = true;
	$_GET['t'] = 'UVPvp'; //get em all
}

$q = addslashes($_REQUEST['q']);
$n = intval($_REQUEST['n']);

$params = explode(' ', $q);

$searchStr = $q;

$guid = substr($_REQUEST['guid'], 0, 36);

$type_strings = array('U' => $siteName . ' Member', 'V' => 'Vessel', 'v' => 'Video', 'p' => 'Photo', 'P' => 'Page');

$blocked_uids = array();
$bu_q = mysql_query( "select blocked_uid from blocked_users where uid='" . $API->uid . "'" );
while( $r = mysql_fetch_array( $bu_q ) )
  $blocked_uids[] = $r['blocked_uid'];

$blocked_uids[] = 832; //Make sure that "SaltHub" is not added to the list.

if (!empty($q))
{
	if ($n < 1 || $n > 100)
		$n = 100;

	$select_pages = array();
	$types = array();
	$union = array();
	$join = array();
	$cats = array();
	$query = array();
	$include_pages = false;

	foreach (str_split($_GET['t']) as $t){
		switch ($t)
		{
            case 'U':
//                $query[] = "SELECT distinct 'U' AS TYPE,users.company,users.occupation,users.uid,REPLACE(users.username,' ','.') AS username,users.name AS title,users.container_url,HASH
//                                FROM users
//                                LEFT JOIN `work` ON `work`.uid = users.uid
//                                LEFT JOIN pages ON pages.gid = work.occupation
//                                LEFT JOIN photos ON IF(users.pic=0,pic0,IF(users.pic=1,pic1,pic))=photos.id
//                                WHERE (pages.gname LIKE '%{$q}%' OR users.name LIKE '%{$q}%' OR users.contactfor LIKE '%{$q}%' OR users.username LIKE '%{$q}%') AND users.active = 1";

//                $query[] = "select 'U' as type,users.company,users.occupation,users.uid,replace(users.username,' ','.') as username,users.name as title,users.container_url,hash from users
//								left join photos on if(users.pic=0,pic0,if(users.pic=1,pic1,pic))=photos.id
//								LEFT JOIN `work` ON `work`.uid = users.uid
//                                LEFT JOIN pages ON pages.gid = work.occupation
//								where (pages.gname LIKE '%{$q}%' OR users.name LIKE '%{$q}%' OR users.company LIKE '%{$q}%' OR users.occupation LIKE '%{$q}%' OR users.contactfor LIKE '%{$q}%' OR users.username LIKE '%{$q}%') and users.active=1 and users.verify=1 and users.dummy=0 and users.uid != $API->uid group by users.uid";

                $users = array();
                foreach ($params as $value){
                    $users[] = "(pages.gname like '%$value%' or users.name like '%$value%' or users.company like '%$value%' or users.occupation like '%$value%' or users.contactfor like '%$value%' or users.username like '%$value%')";
                }

                $query[] = "select 'U' as type,users.company,users.occupation,users.uid,replace(users.username,' ','.') as username,users.name as title,users.container_url,hash from users
								left join photos on if(users.pic=0,pic0,if(users.pic=1,pic1,pic))=photos.id
								LEFT JOIN `work` ON `work`.uid = users.uid
                                LEFT JOIN pages ON pages.gid = work.occupation
								where (".implode(' and', $users).") and users.active=1 and users.verify=1 and users.dummy=0 and users.uid != $API->uid group by users.uid";
		//AND users.uid NOT IN(".implode(',', $blocked_uids).")
                break;

			case 'V':
                $boats = array();
                foreach ($params as $value){
                    $boats[] = "(exnames like '%$value%' or name like '%$value%')";
                }

                $include_pages = true;
				$select_pages['exnames'] = "trim(exnames)";
				$select_pages['length_ft'] = "length";
				$select_pages['length_m'] = "(length*0.3048)";
				$types[] = PAGE_TYPE_VESSEL;
// Changed by AppDragon
//				$union[] = "select pages.gid from boats inner join pages on boats.id=pages.glink where name != '' and exnames like ' {$q}%'";
//				$join[] = "LEFT JOIN boats ON glink=boats.id";
				$union[] = "select pages.gid from boats as boats inner join pages on boats.id=pages.glink where name != '' and (".implode(' and', $boats).")";
				$join[] = "LEFT JOIN boats as boats ON glink=boats.id";
				break;

			case 'P': //all pages
				$include_pages = true;
				$types[] = 'type';
				break;

			case 'E':
				$include_pages = true;
				$types[] = PAGE_TYPE_BUSINESS;
				break;

			case 'S':
				$include_pages = true;
				$cats[] = CAT_SCHOOL;
				break;
//            case 'v':
//                $include_media = true;
//                $query[] = "select DISTINCT videos.cat,'v' as type,container_url,videos.hash,title,catname as cat_str,videos.id as media_id,replace(username,' ','.') as username,name,duration,descr,users.uid,photos.hash as user_thumb from videos
//								inner join users on users.uid=videos.uid
//								inner join photos on photos.id=users.pic
//								inner join categories on videos.cat=categories.cat
//								where ready=1 and videos.privacy=" . PRIVACY_EVERYONE . " and title like '%{$q}%'";
//                break;
//
//            case 'p':
//                $include_media = true;
//                $query[] = "select DISTINCT photos.cat,'p' as type,container_url,photos.hash,title as album_title,photos.ptitle as title,catname as cat_str,photos.aid,photos.id as media_id,replace(username,' ','.') as username,name,descr,users.uid,p2.hash as user_thumb from photos
//								left join users on users.uid=photos.uid
//								left join photos p2 on p2.id=users.pic
//								left join albums on photos.aid=albums.id
//								left join categories on photos.cat=categories.cat
//								where photos.privacy=" . PRIVACY_EVERYONE . " and photos.ptitle like '%{$q}%'";
//                break;
		}
    }

//    foreach (str_split($_GET['t']) as $t){
//        switch ($t)
//        {
//            case 'v':
//		echo "video2+";
//                $include_media = true;
//                $query[] = "select DISTINCT videos.cat,'v' as type,container_url,videos.hash,title,catname as cat_str,videos.id as media_id,replace(username,' ','.') as username,name,duration,descr,users.uid,photos.hash as user_thumb from videos
//								inner join users on users.uid=videos.uid
//								inner join photos on photos.id=users.pic
//								inner join categories on videos.cat=categories.cat
//								where ready=1 and videos.privacy=" . PRIVACY_EVERYONE . " and title like '%{$q}%'";
//                break;
//
//            case 'p':
//		echo "photo2+";
//                $include_media = true;
//                $query[] = "select DISTINCT photos.cat,'p' as type,container_url,photos.hash,title as album_title,photos.ptitle as title,catname as cat_str,photos.aid,photos.id as media_id,replace(username,' ','.') as username,name,descr,users.uid,p2.hash as user_thumb from photos
//								left join users on users.uid=photos.uid
//								left join photos p2 on p2.id=users.pic
//								left join albums on photos.aid=albums.id
//								left join categories on photos.cat=categories.cat
//								where photos.privacy=" . PRIVACY_EVERYONE . " and photos.ptitle like '%{$q}%'";
//                break;
//        }
//    }

	$sel_pages = '';
	
	if (count($select_pages) > 0)
		foreach ($select_pages as $k => $v)
			$sel_pages .= ",$v as $k";

	if ($include_pages)
	{
        if ($_GET['t'] == 'V' || $_GET['t'] == 'E' || $_GET['t'] == 'VE'){
            array_unshift($union, "SELECT gid from pages where gname LIKE '%{$q}%' AND pages.privacy=" . PRIVACY_EVERYONE . " and (" .
                                        (count($types) > 0 ? "type=" . implode(' or type=', $types) . (count($cats) > 0 ? ' or ' : '') : '') .
                                            (count($cats) > 0 ? " cat=" . implode(' or cat=', $cats) : "") . "
                                            )");

            $query[] = "SELECT pages.cat,pages.gid,type,users.container_url,hash,gname as title,catname as cat_str{$sel_pages}
                        FROM pages
                        natural join (
                            (" . implode(")\n\t\t\t\t\tunion (", $union) . ")
                        ) as gids
                        " . implode ("\n", $join) . "
                        LEFT JOIN photos ON photos.id=pid
                        LEFT JOIN users ON photos.uid=users.uid
                        inner join categories on categories.cat=pages.cat
                        group by pages.gid";

            
        }
        else{
            $pages = array();
            foreach ($params as $value){
                $pages[] = "(gname like '%$value%')";
            }

            $query[] = "SELECT pages.cat,
                        pages.gid,
                        type,
                        users.container_url,
                        hash,
                        gname AS title,
                        catname AS cat_str
                    FROM pages
                    LEFT JOIN photos ON photos.id=pid
                    LEFT JOIN users ON photos.uid=users.uid
                    INNER JOIN categories ON categories.cat=pages.cat
                    WHERE (".implode(' and', $pages).")
                    AND pages.privacy=0
                    AND TYPE != 1389
                    GROUP BY pages.gid";
        }
	}

	if (@$_GET['g'] == '0') // do not sort results (one query, limit $n)
	{
		$new_query = array();
		
		$i = 0;
		foreach ($query as $q)
		{
			$table = 'derived' . ++$i;
			
			$new_query[] = "($q) " . $table . ($i > 1 ? ' on true' : '');
		}

		$new_query_str = 'select * from ' . implode("\n\n\t\t\tleft join\n\n", $new_query);
		$queries = array($new_query_str);
	}
	else
		$queries = $query; // sort results by type (separate queries, limit $n * # of queries)

	$starttime = microtime(true);

	$results = array();
	//$queries = array_reverse($queries);
	$executed_queries = array();

    $b = 0;
	foreach ($queries as $q)
	{
        if ($b > 0){
            $executed_queries = "$q order by " . (empty($table) ? '' : "{$table}.") . "title asc";
        }
        else
            $executed_queries = "$q order by " . (empty($table) ? '' : "{$table}.") . "users.lastlogin desc";

		$x = mysql_query($executed_queries);
		if (mysql_num_rows($x) > 0)
		{
			while ($y = mysql_fetch_array($x, MYSQL_BOTH))
			{
				$i = 0;
				foreach ($y as $k => $v)
				{
					if ($i % 2 == 0)
					{
						$val = $v;
						unset($y[$k]);
					}
					else
						$y[$k] = $val;
					
					$i++;
				}

				$type = '';
				$type_str = '';
				
				switch ($y['type'])
				{
                    case 'U':
                        $y['order'] = '0';
                        break;

					case PAGE_TYPE_VESSEL:
						$type = 'V';
                        $y['order'] = '3';
						break;

                    case '1392':
                        $y['order'] = '1';
                        break;

					case PAGE_TYPE_BUSINESS:
						$type = 'E'; // (= employer)
                        $y['order'] = '2';
						break;

                    case 'p':
                        $y['order'] = '4';
                        break;

                    case 'v':
                        $y['order'] = '5';
                        break;

                    default:
                        $y['order'] = '6';
				}
				
				if (empty($type)) // type has not yet been assigned - maybe we can figure it out from the category
					switch ($y['cat'])
					{
						case CAT_SCHOOL:
							$type = 'S';
							$type_str = 'School';
							break;
					}
				
				if (empty($type_str))
					$type_str = get_type_string($y['type']);
				
				$y['type_str'] = $type_str;
				$y['type'] = empty($type) ? $y['type'] : $type;
				$results[] = array_filter($y, 'strlen');
			}
		}
        $b++;
	}
}

usort($results, 'compare');

$return = array('guid' => $guid, 'results' => $results, 'searchStr' => $searchStr, 'show' => $show == true ? 1 : 0);

$time = microtime(true) - $starttime;

if ($isDevServer)
	$return['time'] = $time;

if ($_GET['debug'] == 1 && $isDevServer)
{
	echo "Time: $time\n\n";
	echo "====================== QUERIES (" . count($executed_queries) . ") ======================\n";
	print_r($executed_queries);
	
	echo "\n\n====================== RESULTS (" . count($results) . ") ======================\n";
	print_r($return);
}
else
	echo json_encode($return);

function get_type_string($id)
{
	global $type_strings;
	
	if (empty($type_strings[$id]))
		$type_strings[$id] = quickQuery("select catname from categories where cat='{$id}'");
	
	return $type_strings[$id];
}

function compare($v1, $v2)
{
    if ($v1['order'] == $v2['order'])
        return strcmp($v1['title'], $v2['title']);
    return ($v1['order'] < $v2['order'])?-1:1;
}

?>