<?php

header('Content-Type: application/json');

include "../inc/inc.php";

if ($API->isLoggedIn())
{
	if ($isDevServer)
		$_POST = $_REQUEST;

	$uid = intval($_POST['uid']);

	$x = mysql_query("select fbid,twid,email from users where uid=$uid");

	if (mysql_num_rows($x) > 0)
	{
		$user = mysql_fetch_array($x, MYSQL_ASSOC);
		
		if (isset($_POST['from_uid']))
		{
			$from_uid = intval($_POST['from_uid']);
			if ($_POST['hash'] == md5($_COOKIE['PHPSESSID'] . "-sending_as_another_user-{$from_uid}"))
			{
				$real_uid = $API->uid;
				$API->uid = $from_uid;
			}
		}

		//Send messages from SplashVision user
		//$API->uid = 832;

		$API->sendMessage($uid, $_POST['subject'], nl2br($_POST['body']), $from_uid);

		if (!empty($user['email']))
		{
			$result['email'] = true;
			$API->emailUser($uid, $_POST['subject'], nl2br($_POST['body']));
		}
		else
		{
			if (!empty($user['fbid']))
				$result['facebook'] = $API->fbSendMessage($uid, $_POST['subject'], $_POST['body']);

			if (!empty($user['twid']))
				$result['twitter'] = $API->twSendMessage($uid, $_POST['subject'] . ":  " . $_POST['body']);
		}
	}
	else
		$result = array("error" => "User not found.");
	
	if ($real_uid)
		$API->uid = $real_uid;
}
else
	$result = array("error" => "Not logged in.");

echo json_encode($result);
?>