<?php

header('Content-Type: application/json');

include '../inc/inc.php';

$uid = intval($_REQUEST['uid']);
$x = mysql_query('select uid,email,name from users where uid=' . $uid);

if (mysql_num_rows($x) > 0)
	$user = mysql_fetch_array($x, MYSQL_ASSOC);

if (empty($user['email']))
	$ret = array('error' => 'No e-mail address found for this user.');
else
{
	$showNext = false;
	
	for ($i = 0; $i < strlen($user['email']); $i++)
	{
		$c = substr($user['email'], $i, 1);
		
		if ($i == 0)
			$email = $c;
		elseif ($c == '@')
		{
			$email .= '@';
			$showNext = true;
		}
		elseif ($showNext)
		{
			$showNext = false;
			$email .= $c;
		}
		else
			$email .= '*';
	}
	
	$tmp = explode('.', $user['email']);
	$ext = end($tmp);
	$email = strtolower(substr($email, 0, strlen($email) - 1 - strlen($ext)) . '.' . $ext);
	
	$API->uid = $user['uid'];
	emailAddress($user['email'], $siteName . " - Reset Password", $user['name'] . ', someone has initiated a password reset request.&nbsp; If this was you, you can reset your password by following <a href="http://' . $_SERVER['HTTP_HOST'] . '/signup/resetpw.php?uid=' . $user['uid'] . '&r=' . $API->generateDeleteCommentHash("resetpw", $user['uid']) . '">this link</a>.<p />If this was not you, please simply disregard this e-mail.', 'accounts');
	
	$ret = array('email' => $email);
}

echo json_encode($ret);

?>