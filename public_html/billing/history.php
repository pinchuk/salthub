<?php
/*
Displays a user's purchase history.
*/

include "../inc/inc.php";
include "../header.php";

include( "top_menu.php" );


if( !isset( $_GET['range'] ) ) $_GET['range'] = 5;

$activity = array();

$dates = dateRangeToRealDates( $_GET['range'] );
$startdate = $dates[0];
$enddate = $dates[1];

$q = mysql_query( "select * from billing_purchase_history where uid='" . $API->uid . "' and day BETWEEN '$startdate' AND '$enddate'" );
while( $r = mysql_fetch_array( $q ) )
{
  $offset = strtotime( $r['day'] ); while( isset( $activity[$offset] ) ) { $offset++; }
  $activity[$offset] = array( $r['id'], strtotime( $r['day'] ), "Purchase", $r['credits_purchased'], $r['total'] + $r['credits_used'], 0 );
}

$q = mysql_query( "select * from ad_log left join ad_campaigns on ad_campaigns.id=ad_log.campaign where ad_campaigns.uid='" . $API->uid . "' and ad_log.day BETWEEN '$startdate' AND '$enddate'" );
while( $r = mysql_fetch_array( $q ) )
{
  $offset = strtotime( $r['day'] ); while( isset( $activity[$offset] ) ) { $offset++; }
  $name = quickQuery( "select title from ads2 where id='" . $r['ad'] . "'" );
  $campaign = quickQuery( "select campaign from ads2 where id='" . $r['ad'] . "'" );
  $activity[ $offset ] = array( 0, strtotime( $r['day'] ), "Daily Impressions " . date( "M d, Y", strtotime( $r['day'] ) ) . " for \"<a href=\"/adv/campaign-details.php?ID=$campaign\">" . $name . "</a>\"", 0, $r['cost'], 0 );
}

$q = mysql_query( "select * from billing_promo_redemptions inner join billing_promos on billing_promos.id=billing_promo_redemptions.promo where billing_promo_redemptions.uid='" . $API->uid . "' and billing_promo_redemptions.day BETWEEN '$startdate' AND '$enddate'" );
while( $r = mysql_fetch_array( $q ) )
{
  $offset = strtotime( $r['day'] ); while( isset( $activity[$offset] ) ) { $offset++; }
  $activity[$offset] = array( 0, strtotime( $r['day'] ), "Coupon / Gift Card Redemption", $r['value'], 0, 0 );
}

krsort( $activity );
?>


<div class="contentborder">
<div style="clear:both; padding-top:30px;"></div>


  <div style="clear:both; float:left; width:300px; font-weight:bold; color:rgb(85,85,85); font-size:13pt;">Account History</div>

  <div style="float:right; font-size:9pt;">
    View History for:
    <select id="range" onchange="javascript: window.location='history.php?range=' + selectedValue( this );">
      <option value="0"<? if( $_GET['range'] == 0 ) echo " SELECTED"; ?>>Today</option>
      <option value="1"<? if( $_GET['range'] == 1 ) echo " SELECTED"; ?>>Yesterday</option>
      <option value="2"<? if( $_GET['range'] == 2 ) echo " SELECTED"; ?>>Last 7 Days</option>
      <option value="3"<? if( $_GET['range'] == 3 ) echo " SELECTED"; ?>>Last 15 Days</option>
      <option value="4"<? if( $_GET['range'] == 4 ) echo " SELECTED"; ?>>Last 30 Days</option>
      <option value="5"<? if( $_GET['range'] == 5 ) echo " SELECTED"; ?>>This Month</option>
      <option value="6"<? if( $_GET['range'] == 6 ) echo " SELECTED"; ?>>Previous Month</option>
      <option value="7"<? if( $_GET['range'] == 7 ) echo " SELECTED"; ?>>All</option>
    </select>
  </div>

  <div style="clear:both;"></div>

  <? if( sizeof($activity) == 0 ) { ?>
  <div>
    You have not applied any credits or made any payments for the selected time period.
  </div>
  <? } else {  ?>

  <table class="campaign_table" cellspacing="1" cellpadding="4">
  <tr>
    <th>Date</th>
    <th>Description</th>
    <th>Credits / Payments</th>
    <th>Amount Spent</th>
    <th>Amount Due</th>
  </tr>
<?

$totals = array(0,0,0);

foreach( $activity as $act )
{
  $totals[0] += $act[3];
  $totals[1] += $act[4];
  $totals[2] += $act[5];
?>
  <tr>
    <td><?=date( "M d, Y", $act[1] ); ?></td>
    <td>
      <?=$act[2];?>
      <? if( $act[0] > 0 ) { ?>
      (<a href="receipt.php?p=<?=$act[0]?>" target="_new">view receipt</a>)
      <? } ?>
    </td>
    <td align="right"><?if( $act[3] > 0 ) { ?>$<?=number_format($act[3],2);?><? } ?></td>
    <td align="right"><?if( $act[4] > 0 ) { ?>$<?=number_format($act[4],2);?><? } ?></td>
    <td align="right"><?if( $act[5] > 0 ) { ?>$<?=number_format($act[5],2);?><? } ?></td>
  </tr>
<?
}

?>
  <tr style="font-weight:bold;">
    <td></td>
    <td><b>Available Credits</b></td>
    <td colspan="2"></td>
    <td align="right"><?=number_format( quickQuery( "select ad_credits from users where uid='" . $API->uid . "'" ), 2 ); ?></td>
  </tr>
  </table>

  <? } // end mysql_num_rows == 0?>
  <div style="clear:both;"></div>
</div>


<?
include "../footer.php";
?>