<?php

include "../inc/inc.php";
$gid = intval($_GET['gid']);

$members = $API->getPageMembers($gid, "gm.admin desc,name");
$page = $API->getPageInfo($gid);

if( isset( $members ) )
foreach ($members as $member)
{
	$count[$member['admin']]++;
	
	if ($member['uid'] == $API->uid)
		$isAdmin = ($member['admin'] == 1);
}

$isAdmin = ($isAdmin || $API->admin);

if ($page['uid'] == $API->uid || $API->admin)
	$isCreator = true;


?>

<div style="float: left; padding-right: 7px;">
	<a href="<?=$page['url']?>"><img src="<?=$API->getThumbURL(1, 48, 48, $API->getPageImage($gid) )?>" alt="" /></a>
</div>

<div style="overflow: hidden; font-size: 9pt; padding-top: 3px;">
	<span style="font-weight: bold;">Admins and members of <a href="<?=$page['url']?>"><?=$page['gname']?></a></span>
	<div style="border-top: 2px solid #d8dfea; padding-top: 5px; margin-top: 5px; font-size: 8pt;">
		<span style="font-weight: bold; font-size: 9pt;">View:&nbsp;</span>
		<a href="javascript:void(0);" onclick="javascript:setScrollTop('gmems', 0);">admins (<?=$count[1]?>)&nbsp;|&nbsp;
		<a href="javascript:void(0);" onclick="javascript:setScrollTop('gmems', <?=58 * $count[1] + 19?>);">members (<?=$count[0]?>)</a>
	</div>
</div>

<div style="clear: both; height: 5px;"></div>

<div style="overflow: auto; width: 448px; padding-right: 5px; height: 350px;" id="gmems">
	<?php
	$admin = null;
  if( isset( $members ) )
	foreach ($members as $member)
	{
		if ($member['admin'] != $admin)
		{
			echo '<div class="subhead" style="clear: both;">' . ($member['admin'] == 1 ? "Administrators" : "Members") . '</div>';
			$admin = $member['admin'];
		}
		
		$isThisUserCreator = $page['uid'] == $member['uid'];
		$isThisUserAdmin = ($member['admin'] == 1  );
		$profileURL = $API->getProfileURL($member['uid'], $member['username']);
		
		?>
		<div class="mediapreviewsmall">
			<div class="pic" style="width: auto; padding-right: 7px;">
				<a href="<?=$profileURL?>"><img src="<?=$API->getThumbURL(1, 48, 48, $API->getUserPic($member['uid'], $member['pic']))?>" alt="" /></a>
			</div>
			<div class="info">
				<div class="title">
					<a href="<?=$profileURL?>"><?=$member['name'] . ($isThisUserAdmin ? " &ndash; " . ($isThisUserCreator ? "creator" : "admin") : "")?></a>
					<div class="addlink">
						<?php if ($API->uid != $member['uid']) { ?>
						<a href="javascript:void(0);" onclick="javascript:showSendMessage(<?=$member['uid']?>, '<?=addslashes($member['name'])?>', '<?=$API->getThumbURL(1, 48, 48, $API->getUserPic($member['uid'], $member['pic']))?>')">send message</a>
						<?php } ?>
					</div>
				</div>


				<div class="descr">
          <?
          $occupation = quickQuery( "select occupation from users where uid='" . $member['uid'] . "'" );
          if( $occupation != "" )
          {
            echo $occupation;
            $co = quickQuery( "select company from users where uid='" . $member['uid'] . "'" );
            if( $co != "" ) echo ", " . $co;
          } else { echo "Member of " . $siteName; }


          ?>

					<?php if ( ($isAdmin && !$isThisUserCreator && $API->uid != $member['uid']) || $API->admin ) { ?>
					<div class="addlink" id="gmlinks-<?=$member['uid']?>">
						<?php if ($isCreator || $API->admin ) { ?>
						<span id="gmadminlink-<?=$member['uid']?>"><a href="javascript:void(0);" onclick="javascript:modifyPageMember(<?=$page['gid']?>, <?=$member['uid']?>, '<?=$isThisUserAdmin ? "de" : "pro"?>mote');"><?=$isThisUserAdmin ? "remove as" : "make"?> admin</a></span> |
						<?php } ?>
						<a href="javascript:void(0);" onclick="javascript:modifyPageMember(<?=$page['gid']?>, <?=$member['uid']?>, 'remove');">remove from page</a>
						<?php if ($isCreator || $isThisUserAdmin == 0) { ?>
						| <a href="javascript:void(0);" onclick="javascript:modifyPageMember(<?=$page['gid']?>, <?=$member['uid']?>, 'block');">block</a>
						<?php } ?>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
		<?php
	}
	?>
</div>