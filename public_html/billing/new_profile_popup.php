<?
/*
Popup for creating a new payment profile.
*/

include_once( "../inc/inc.php" );
?>

<style>

.billing_header { color: #326798; margin-top:3px; padding-left:5px; font-size:10pt; font-weight:bold; }
.billing_itemhead { margin-top:15px; }
.billing_item {}
.hr { width:100%; height: 1px; color:#d8dfea; background-color:#d8dfea; margin-bottom:5px;}

.order_itemhead { clear:both; padding-left:10px; margin-top:5px; width:300px; float:left; font-size:9pt; font-weight:bold; }
.order_item { padding-right:10px; margin-top:5px; width:100px; float:right; font-size:9pt; font-weight:bold; }

</style>
<form action="new_profile_action.php" method="POST">

<div class="contentborder" style="width:450px;">
  <div class="strong" style="font-size:13pt;">New Payment Method</div>

  <div style="background-color:#f3f8fb; border:1px solid #d8dfea; width:425px; float:left; padding:10px;">
    <div style="padding-left:5px; font-size:8pt;">
      <div class="billing_itemhead">Payment Profile Description:</div>
      <div class="billing_item"><input type="text" name="descr" maxlength="50" style="width: 240px;"/></div>
    </div>

    <div style="float:left; padding-left:5px; font-size:8pt;">
      <div class="billing_itemhead">Cardholder First Name:</div>
      <div class="billing_item"><input type="text" name="firstname" maxlength="50" style="width: 180px;"/></div>

      <div class="billing_itemhead">Cardholder Last Name:</div>
      <div class="billing_item"><input type="text" name="lastname" maxlength="50" style="width: 180px;"/></div>

      <div class="billing_itemhead">Card Number:</div>
      <div class="billing_item"><input type="text" name="cc_number" maxlength="20" style="width: 180px;"/></div>

      <div class="billing_itemhead">Security Code:</div>
      <div class="billing_item"><input type="text" name="cc_code" maxlength="5" style="width: 50px;"/></div>

      <div class="billing_itemhead">Expiration Date:</div>
      <div class="billing_item">
      <select name="expmo">
        <? for( $c = 1; $c <= 12; $c++ ) echo '<option value="'. sprintf("%02d", $c) . '">' . $c . '</option>'; ?>
      </select>
      <select name="expyr">
        <? for( $c = intval(date("Y")); $c < intval(date("Y")+10); $c++ ) echo '<option value="'. $c . '">' . $c . '</option>'; ?>
      </select>
      </div>

      <div style="clear:both;"></div>


    </div>

    <div style="float:right; text-align:left; padding-right:5px; font-size:8pt; margin-left:20px;">
      <div class="billing_itemhead">Billing Address:</div>
      <div class="billing_item"><input type="text" name="address" maxlength="250" style="width: 200px;"/></div>

      <div class="billing_itemhead">City:</div>
      <div class="billing_item"><input type="text" name="city" maxlength="50" style="width: 200px;"/></div>

      <div class="billing_itemhead">Country:</div>
      <div class="billing_item">
      <select name="country" onchange="javascript: idx = this.selectedIndex; reloadStates(this.options[idx].value);" style="width:200px;">
<?
$q = sql_query( "select * from loc_country order by priority desc, country" );
while( $r = mysql_fetch_array( $q ) )
{
?>
      <option value="<? echo $r['id']; ?>"><? echo $r['country']; ?></option>
<?
}
?>
      </select>
      </div>

      <div class="billing_itemhead">State / Province:</div>
      <div class="billing_item">
      <select name="state" style="width:200px;" id="states">
<?
$q = sql_query( "select * from loc_state where country_id=220 order by region" );
while( $r = mysql_fetch_array( $q ) )
{
?>
      <option value="<? echo $r['region']; ?>"><? echo ucfirst($r['region']); ?></option>
<?
}
?>
      </select>
      </div>

      <div class="billing_itemhead">Postal Code:</div>
      <div class="billing_item"><input type="text" name="zip" maxlength="50" style="width: 70px;"/></div>

    </div>


    <div style="clear:both; padding-top:15px; padding-left:5px;">
      <input type="submit" class="button" value="Create Payment Method" id="create_payment_method"/>
    </div>
  </div>
  <div style="clear:both;"></div>
</div>

</form>

<?
mysql_close();
?>