<?php
/*
Displays a user's purchase receipt.
*/

include_once( "../inc/inc.php" );

$payment = intval( $_GET['p'] );

if( $API->uid != quickQuery( "select uid from billing_purchase_history where id='$payment'" ) )
{
  die( "Invalid purchase" );
}

$purchase_data = queryArray( "select day, total, credits_used, credits_purchased, profile from billing_purchase_history where id='$payment'" );
$profile_data = queryArray( "select * from billing_profiles where id='" . $purchase_data['profile'] . "'" );

$state = quickQuery( "select region from loc_state where id='" . $profile_data['state'] . "'" );
$country = quickQuery( "select country from loc_country where id='" . $profile_data['country'] . "'" );
?>
<? if( !isset( $email ) ) { ?>
<table width="734">
<tr>
  <td colspan="2"><img src="http://www.salthub.com/images/emailhead_s.png" width="734" height="88"/></td>
</tr>
</table>
<? } ?>

<table width="590" style="font-family:Arial; font-size:9pt; <?if( !isset( $email ) ) { ?>margin-left:75px;<? } ?>">
<tr>
  <td colspan="2" align="right" style="font-size:20pt; color:#555;">RECEIPT</td>
</tr>
<tr>
  <td width="50%" style="font-size:10pt;" valign="top">
    <?
    $user_data = queryArray( "select * from users where uid='" . $API->uid . "'" );

    echo "<b>" . $profile_data['firstname'] . " " . $profile_data['lastname'] . "</b><br/>";
    echo $profile_data['address'] . "<br/>";
    echo $profile_data['city'] . ", " . $state . ". " . $profile_data['zip'] . "<br/>";
    echo $country . "<br/><br/>";

    echo $user_data['email'];
    ?>

  </td>

  <td width="50%" valign="top">

  <table width="100%">
  <tr>
    <td style="background-color:#eceff2;">Receipt Number</td>
    <td><?=$payment; ?></td>
  </tr>
  <tr>
    <td style="background-color:#eceff2;">Date</td>
    <td align="center"><?=date( "M d, Y", strtotime( $purchase_data['day'] ) ); ?></td>
  </tr>
  </table>

  </td>
</tr>

<tr><td height="20"></td></tr>
<?
$q = mysql_query( "select * from billing_purchase_history_items where payment='$payment'" );
?>
<tr>
  <td colspan="2" valign="top">

  <table width="100%">
  <tr style="background-color:#eceff2;">
    <th>Description</th>
    <th>Quantity</th>
    <th>Unit Price</th>
    <th>Amount</th>
  </tr>
<?
while( $r = mysql_fetch_array( $q ) )
{
?>
  <tr>
    <td><?=quickQuery( "select `desc` from billing_purchase_types where id='" . $r['type'] . "'" ); ?></td>
    <td align="right"><?=$r['quantity'];?></td>
    <td align="right"><?=number_format($r['cost'],2);?></td>
    <td align="right"><?=number_format($r['cost'] * $r['quantity'],2);?></td>
  </tr>
<?
}
?>
  <tr>
    <td colspan="2"></td>
    <td align="right" style="font-weight:bold; background-color:#eceff2;">Total</td>
    <td align="right" style="font-weight:bold; background-color:#eceff2;"><?= number_format( $purchase_data['total'] + $purchase_data['credits_used'], 2 ); ?></td>
  </tr>
  </table>

  </td>
</tr>

<tr><td height="20"></td></tr>

<?
if( isset( $email ) )
{

$name = quickQuery("select name from users where uid='" . $API->uid . "'" );

switch( $profile_data['first_digit'] )
{
  case 3: $card_type = "American Express"; break;
  case 4: $card_type = "Visa"; break;
  case 5: $card_type = "Master"; break;
  case 6: $card_type = "Discover"; break;
}
?>

<tr>
  <td colspan="2" align="center">
    <div style="width:100%; border-bottom:3px dotted #eceff2;"></div>
    <?= $siteName ?> sent this message to <?=$name?>.<br />
    Your registered name is included to show this message originated from <?=$siteName?>.<br />
    Learn more: <a href="http://www.<?=$siteName?>.com/tos.php">http://www.<?=$siteName?>.com/tos.php</a>
    <div style="width:100%; border-bottom:3px dotted #eceff2; margin-bottom:20px;"></div>
  </td>
</tr>

<tr>
  <td colspan="2">

***This is an automatically generated email. Please do not reply.***<br /><br />

You have set up <?=$card_type?> as your automatic payment method.  Your purchases amount will be automatically deducted from your <?=$card_type?> account as they become due. The amount deducted may vary based on recent payments or credits.<br /><br />

To view your invoice:<br />
1. Go to <a href="http://www.<?=$siteName?>.com">http://www. <?=$siteName?>.com</a> and click "Account" at the top of most <?=$siteName?> pages. You will need to sign in.<br />
2. Click the "Billing" link under the "Account" heading at the top of the page.<br />
3. Select the "History" on the Billing page and select the invoice you wish to view.<br /><br />

Remember: <?=$siteName?> will not ask you for sensitive personal information (such as your password, credit card and bank account numbers, Social Security number, etc.) in an email.<br /><br />

Thank you for using  <?=$siteName?>.<br /><br />

- The  <?=$siteName?> Team
  </td>
</tr>
<? } else  { ?>
<tr><td colspan="2" align="center"><a href="javascript:void(0);" onclick="javascript:window.print();">Print this Receipt</a></td></tr>
<? } ?>
</table>