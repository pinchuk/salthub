var caTimer = [];
var caValue = [];
var caImgStyle =  "vertical-align: middle; height: 16px; width: 16px; float: none; margin: 0;";
var onSaveCallback = '';

function checkAvail(e, callback)
{
  if(!(typeof callback === "undefined") )
  {
    onSaveCallback = callback;
  }

	if (e.value.length == 0)
		return;

	if (caValue[e.id] != null)
		if (caValue[e.id] == e.value)
			return;

	if (typeof caTimer[e.id] == "number")
		clearTimeout(caTimer[e.id]);

	document.getElementById(e.id + "-avail").innerHTML = "<img src=\"/images/wait326798_l.gif\" style=\"" + caImgStyle + "\" alt=\"\" /> checking ...";// "";

	caTimer[e.id] = setTimeout("doCheckAvail('" + e.id + "', '" + escape(e.value) + "')", 750);
	caValue[e.id] = e.value;
}

function doCheckAvail(id, val)
{
	//e = document.getElementById(id + "-avail");
	//e.innerHTML = "<img src=\"/images/wait326798_l.gif\" style=\"" + caImgStyle + "\" alt=\"\" /> checking ...";
	getAjax("/checkavail.php?id=" + id + "&val=" + val, "doCheckAvailHandler");
}

function doCheckAvailHandler(data)
{
	x = data.split(":");
	e = document.getElementById(x[1] + "-avail");

  x[0] = parseInt( x[0] );

	if (x[0] == 0) //available
		e.innerHTML = "<img src=\"/images/check.png\" style=\"" + caImgStyle + "\" alt=\"\" /> <a href=\"javascript:void(0);\" onclick=\"javascript:saveAvail('" + x[1] + "');\">save</a>";
	else if (x[0] == 1) //not available
		e.innerHTML = "<img src=\"/images/x.png\" style=\"" + caImgStyle + "\" alt=\"\" /> already taken";
	else if(x[0]==2) //illegal chars
		e.innerHTML = "<img src=\"/images/x.png\" style=\"" + caImgStyle + "\" alt=\"\" /> invalid name";
	else if(x[0]==3) //illegal chars
		e.innerHTML = "<img src=\"/images/x.png\" style=\"" + caImgStyle + "\" alt=\"\" /> number required";
}

function saveAvail(id)
{
	e = document.getElementById(id + "-avail");
	e.innerHTML = "<img src=\"/images/wait326798_l.gif\" style=\"" + caImgStyle + "\" alt=\"\" /> working ...";

  sendConf = 0;
  if( document.getElementById("verifypage" ) ) sendConf = 1;

	postAjax("/saveavail.php", "id=" + id + "&val=" + escape(document.getElementById(id).value) + "&sendConf=" + sendConf, "doSaveAvailHandler");
}

function doSaveAvailHandler(data)
{
	x = data.split(":");
	e = document.getElementById(x[1] + "-avail");

	if (x[0] == "OK")
  {
    if( onSaveCallback != '' )
    {
      eval( onSaveCallback + "('" + document.getElementById(x[1]).value + "');" );
      onSaveCallback = '';
    }

    if( document.getElementById("verifypage" ) )
   		e.innerHTML = "<img src=\"/images/check.png\" style=\"" + caImgStyle + "\" alt=\"\" /> verification email sent";
    else
  		e.innerHTML = "<img src=\"/images/check.png\" style=\"" + caImgStyle + "\" alt=\"\" /> saved";


  }
	else //fail
		e.innerHTML = "<img src=\"/images/x.png\" style=\"" + caImgStyle + "\" alt=\"\" /> already taken";

  e = document.getElementById("userurl");
  username = document.getElementById( "username" );

  if( e && username )
  {
    var temp = username.value;
    e.innerHTML = temp.replace( " ", "." );
  }
}