var currentTL;
var defaultComment = "Reply to this ...";
var lastCommentId = {};
var firstCommentId = {};
var addingComment = false;

function commentKeypress(event, type, link, layout)
{
	kc = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
//	if (kc == 13)
//		submitComment(type, link, layout);
}

var viewNewCommentQueue = [];
var commentTimer = 0;

function Queue_viewNewComments(tl, layout)
{
	com = new Object();
	com.tl = tl;
	com.layout = layout;
	
	viewNewCommentQueue[viewNewCommentQueue.length] = com;
	
	if (commentTimer == 0)
		commentTimer = setTimeout('getNewComments()', 5000);
}

function getNewComments()
{
	for (i in viewNewCommentQueue)
		viewNewCommentQueue[i].firstComment = firstCommentId[viewNewCommentQueue[i].tl];
	
	$.ajax({
		url: '/getnewcomments.php',
		data: 'q=' + json_encode(viewNewCommentQueue),
		dataType: 'json'
	}).done(function (json)
	{
		for (i in json)
			for (j in json[i])
			{
				c = json[i][j];
				if (firstCommentId[c.layout + c.type + c.link] < c.id)
					firstCommentId[c.layout + c.type + c.link] = c.id;
				
				newdiv = $('<div class="newcomment"></div>');
				$(newdiv).html(c.html);
				$(newdiv).attr('id', "comment" + new Date().getTime());

				e = $("#commentscontainer-" + c.layout + c.type + c.link);
				if (e.length == 1)
				{
					$(e).append(newdiv);
					$(e).find('div.newcomment:last')
						.css('opacity', 0)
						.css('display', 'none')
						.slideDown('slow')
						.animate(
							{ opacity: 1 },
							{ queue: false, duration: 'slow' }
						);
				}
			}
		
		setTimeout('getNewComments()', 5000);
	});
}

function viewNewComments(tl, layout)
{
	//return;
	
  firstComment = firstCommentId[ tl ];

  if( addingComment ) return false;

	getAjax("/newcomments.php?mostRecent=" + firstComment + "&tl=" + tl + "&layout=" + layout, function (data)
	{
    if( data == "" || data == null ) return;

		eval("json = " + data);

    if( json.html != '' )
    {
      firstCommentId[tl] = json.id;

  		newdiv = document.createElement("div");
  		newdiv.innerHTML = json.html;
      newdiv.id = "comment" + new Date().getTime();

  		e = document.getElementById("commentscontainer-" + tl);
      if( e )
      {
    		e.appendChild(newdiv);

      	h = getHeight(newdiv);
      	slidedown(newdiv.id, h, true);
      }
    }
	});
}

function viewMoreComments(json)
{
	document.getElementById("viewmorecommentswait-" + json.tl).style.display = "";
	document.getElementById("viewmorecomments-" + json.tl).style.display = "none";

	getAjax("/morecomments.php?json_more=" + json_encode(json), function (data)
	{
    if( data == "" || data == null ) return;

		eval("json = " + data);

		lastCommentId[json.tl] = json.id;

		newdiv = document.createElement("div");
		newdiv.innerHTML = json.html;

		e = document.getElementById("commentsleft-" + json.tl);
		e.innerHTML = parseInt(e.innerHTML) - parseInt(json.numcomments);
		
		e = document.getElementById("commentsvisible-" + json.tl);
		e.innerHTML = parseInt(e.innerHTML) + parseInt(json.numcomments);

		e = document.getElementById("commentscontainer-" + json.tl);
		e.insertBefore(newdiv, e.firstChild);
		
		document.getElementById("viewmorecomments-" + json.tl).style.display = json.more == 0 ? "none" : "";
		document.getElementById("viewmorecommentswait-" + json.tl).style.display = "none";
	});
}

function txtCommentClick(e, click)
{
	if (click)
	{
		if (e.value == defaultComment)
			e.value = "";
	}
	else
	{
		if (e.value == "")
			e.value = defaultComment;
	}
}

function submitComment(type, link, layout, gid)
{
	if (isLoggedIn)
	{
    addingComment = true;

		currentTL = layout + type + link;

		comment = document.getElementById("txtcomment-" + currentTL).value;
		if (comment == defaultComment || comment == "") return; // user didn't enter anything

		document.getElementById("commentwrite-" + currentTL).style.display = "none";

		e = document.getElementById("commentintro-" + currentTL);
		if (e) e.innerHTML = "Replies &#0133;";

    //For posting to FB pages
    e = document.getElementById("cmt-target_id-" + currentTL);
    fbtarget = 0;
    if( e )
      fbtarget = e.value;

		postAjax("/addcomment.php", "layout=" + layout + "&type=" + type + "&link=" + link + "&comment=" + escape(comment) + "&gid=" + gid + "&fbtarget=" + fbtarget, function (data)
		{
      if( data == "" || data == null ) return;

			eval("json = " + data);

			if (json.error)
				showPopUp2("Error", json.error);
			else
			{
				newdiv = document.createElement("div");
				//newdiv.id = "nc-" + json.hash;
				newdiv.innerHTML = json.html;
				document.getElementById("commentscontainer-" + currentTL).appendChild(newdiv);

        firstCommentId[currentTL] = json.id;
			}
			
			e = document.getElementById("numcomments");
			if (e)
				e.innerHTML++;

      addingComment = false;
		});
	}
}

function deleteComment(div, id, hash)
{
	postAjax("/deletecomment.php", "id=" + id + "&hash=" + hash, "void");
  if( div )
  	div.style.display = "none";

	e = document.getElementById("numcomments");
	if (e)
  {
		e.innerHTML--;
  }
}

function itemLike(type, link, like, tl, layout)
{
	if (isLoggedIn)
	{
		postAjax("/likeitem.php", "type=" + type + "&link=" + link + "&like=" + like + "&layout=" + layout, "likeItemHandler");
		currentTL = tl;
	}
}

function likeItemHandler(data)
{
	x = data.split("|");

  x[0] = x[0].trim();
	if (x[0] == "-1")
		document.getElementById("youlikecontainer-" + currentTL).style.display = "none";
	else
	{
		document.getElementById("youlikecontainer-" + currentTL).style.display = "";



		if (x[0] == "1")
		{
			document.getElementById("youlike-" + currentTL).innerHTML = "You like this.";
			document.getElementById("youlikeimg-" + currentTL).src = "/images/thumb_up.png";
		}
		else
		{
			document.getElementById("youlike-" + currentTL).innerHTML = "You dislike this.";
			document.getElementById("youlikeimg-" + currentTL).src = "/images/thumb_down.png";
		}
	}
	document.getElementById("itemlike-" + currentTL).innerHTML = x[1];
}

