<?
    require_once 'inc/inc.php';

    switch ($_GET['type']){
        case 'blocked_emails':
            $data = mysql_query("select * from blocked_emails");
            $file = '/var/www/salthub.com/public_html/blocked_emails_table.csv';
            break;
        case 'unactive_users':
            $data = mysql_query("select * from users where active = 0");
            $file = '/var/www/salthub.com/public_html/inactive_users_emails.csv';
            break;
        case 'active_users':
            $data = mysql_query("select * from users where active = 1");
            $file = '/var/www/salthub.com/public_html/active_users_emails.csv';
            break;
        case 'blocked_users':
            $data = mysql_query("SELECT users.email FROM users INNER JOIN blocked_users ON blocked_users.blocked_uid = users.uid GROUP BY users.email");
            $file = '/var/www/salthub.com/public_html/blocked_users_emails.csv';
            break;
        default:
            $data = null;
            $file = null;
    }

    if (!empty($data) && !empty($file)){
        $result = array();
        while ($row = mysql_fetch_array($data)){
            $result[] = $row;
        }

        $str = "";
        fputcsv($file, $str);
        foreach ($result as $value){
            if ($value['email'] != '')
                $str .= $value['email']."\r\n";
        }

        file_put_contents($file, $str);

        if (file_exists($file)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename='.basename($file));
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            ob_clean();
            flush();
            readfile($file);
            exit;
        }
    }
?>