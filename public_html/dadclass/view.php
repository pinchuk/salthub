<?php

class View {

    public static function get($nameView, $param = null)
    {
        $nameFile = "./views/".$nameView.".php";
        foreach($param as $key=>$val)
        {
            ${$key}=$val;
        }
        if ( !file_exists( $nameFile ) ){
            return false;
        }
        ob_start();
        include $nameFile;
        $buffer = ob_get_contents();
        @ob_end_clean();
        return $buffer;
    }

}

?>