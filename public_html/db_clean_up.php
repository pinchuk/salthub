<?
include_once "inc/inc.php";
require( "header.php" );

if( $API->uid != 585 )
{
	if ($API->admin != 1)
	{
		header("Location: /");
		die();
	}
}

if( isset( $_GET['offset'] ) )
  $offset = intval( $_GET['offset'] );
else
  $offset = 0;

if( isset( $_GET['sort'] ) )
  $_SESSION['DBC_sort'] = intval( $_GET['sort'] );
if( isset( $_GET['show'] ) )
  $_SESSION['DBC_show'] = intval( $_GET['show'] );
if( isset( $_GET['type'] ) )
  $_SESSION['DBC_type'] = intval( $_GET['type'] );

$previd = intval( $_GET['id'] );

if( $_SESSION['DBC_sort'] == 0 && $_SESSION['DBC_show'] == 0 )
  $sql = "select pages.gid, cat from pages where type='" . PAGE_TYPE_VESSEL . "' order by gid";
else
{
  $sql = "select pages.gid, cat from pages inner join boats on boats.gid=pages.gid where pages.type='" . PAGE_TYPE_VESSEL . "'";

  switch( $_SESSION['DBC_show'] )
  {
    case 1:
      $sql .= " AND boats.IMO=0 AND boats.MMSI=0";
    break;
    case 2:
      $sql .= " AND 0 IN ( SELECT count(gid) from page_media where gid=pages.gid )";
    break;
    case 3:
      $sql .= " AND 0 IN ( SELECT count(gid) from page_media where gid=pages.gid )";
      $sql .= " AND boats.IMO=0 AND boats.MMSI=0";
    break;
  }

  if( $_SESSION['DBC_type'] > 0 )
  {
    $sql .= " AND pages.cat='" . $_SESSION['DBC_type'] . "'";
  }

  switch( $_SESSION['DBC_sort'] )
  {
    case 1:
      $sql .= " order by pages.cat, LTRIM(pages.gname)";
    break;
    case 2:
      $sql .= " order by pages.created desc";
    break;
    case 3:
      $sql .= " order by LTRIM(pages.gname)";
    break;
  }


}

$sql .= " limit $offset,105";

$q = mysql_query( $sql );

echo mysql_error();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <title>Hello!</title>
</head>

<body>

<script type="text/javascript">
<!--

function deletePage( gid, e )
{
  getAjax("/admin/deletepage.php?gid=" + gid, "void");

  if( e )
    e.innerHTML = "(Deleted)";
}

function changePageType( gid, cat )
{
  getAjax("/admin/changePageType.php?gid=" + gid + "&cat=" + cat, "void");
}

function updateMMSI( gid, mmsi )
{
  getAjax("/admin/update_boat_mmsi.php?gid=" + gid + "&mmsi=" + mmsi, "void");
}


-->
</script>

<div style="width:1000px;">
  <div style="float:left;">
    Show:
    <select name="show" onchange="document.location='/db_clean_up.php?show=' + selectedValue(this);">
    <option value="0">Creation Date</option>
    <option value="1"<?if( $_SESSION['DBC_show'] == 1 ) echo " SELECTED";?>>No IMO &amp; MMSI</option>
    <option value="2"<?if( $_SESSION['DBC_show'] == 2 ) echo " SELECTED";?>>No Images</option>
    <option value="3"<?if( $_SESSION['DBC_show'] == 3 ) echo " SELECTED";?>>No IMO/MMSI/Images</option>
    </select>
  </div>


  <div style="float:left;">
    &nbsp;Sort By:
    <select name="sort" onchange="document.location='/db_clean_up.php?sort=' + selectedValue(this);">
    <option value="0">No Sorting</option>
    <option value="2"<?if( $_SESSION['DBC_sort'] == 2 ) echo " SELECTED";?>>Creation Date</option>
    <option value="3"<?if( $_SESSION['DBC_sort'] == 3 ) echo " SELECTED";?>>Name</option>
    </select>
  </div>

  <div style="float:left;">
    &nbsp;Type:
    <select name="sort" onchange="document.location='/db_clean_up.php?type=' + selectedValue(this);">
    <option value="0">All</option>
<?
$q2 = mysql_query( "select * from categories where industry=1389" );
while( $r2 = mysql_fetch_array( $q2 ) )
{
?>
    <option value="<?=$r2['cat']?>"<? if( $_SESSION['DBC_type'] == $r2['cat'] ) echo " SELECTED";?>><?=$r2['catname'];?></option>
<?
}
?>
    </select>
  </div>

</div>

<div style="width:1000px; clear:both;">
<?
$cats = array();
$q2 = mysql_query( "select catname, cat from categories where industry='" . PAGE_TYPE_VESSEL . "'" );
while( $r = mysql_fetch_array( $q2 ) )
  $cats[] = $r;

while( $r = mysql_fetch_array( $q ) )
{
  $info = $API->getPageInfo( $r['gid'], true );

  $pageUrl = $API->getPageURL( $r['gid'] );
  $len = quickQuery( "select length from boats where gid='" . $r['gid'] . "'" );
  $imo = quickQuery( "select imo from boats where gid='" . $r['gid'] . "'" );
  $type = quickQuery( "select catname from categories where cat='" . $info['cat'] . "'" );
?>
  <div class="jobs-profile" style="padding-left:0px; padding-right:0px; width:125px; height:208px">
    <div style="left:auto; right:auto; height:188px; padding-left:3px;">
      <a href="<?=$info['url'];?>/editpage"><img border="0" src="/img/119x95/<?=$info['profile_pic']; ?>" width="119" height="95"/></a>
      <div><a href="<?=$info['url']; ?>"><? echo $info['gname']; ?></a></div>
      <? if( $imo > 0 ) { ?><div>IMO: <?=$imo; ?></div><? } ?>
<!--      <? if( $type != "" ) { ?><div>Type: <?=$type?></div><? } ?>-->
      <? if( $len > 0 ) { ?><div>LOA: <?=$len?> ft. / <?=number_format( $len * 0.3048 );?> m.</div><? } ?>
      <div>Page <a href="<?=$pageUrl?>/photos">Photos</a> <a href="<?=$pageUrl?>/videos">Videos</a></div>
      <div>
        <select onchange="changePageType(<?=$r['gid'];?>, selectedValue(this) );" style="width:115px;">
<?
        for( $c = 0; $c < sizeof( $cats ); $c++ )
        {
?>
        <option value="<?=$cats[$c]['cat'];?>" <? if($cats[$c]['cat'] == $r['cat']) echo " SELECTED"; ?> ><?=$cats[$c]['catname'];?></option>
<?
        }
?>
        </select>
      </div>
      <div>
        <? $mmsi = quickQuery( "select mmsi from boats where gid='" . $r['gid'] . "'" ); ?>
        MMSI <input name="" value="<?=$mmsi?>" onchange="javascript:updateMMSI(<?=$r['gid']?>, this.value);" size="12"/>
      </div>
    </div>

    <div style="background-color:#fff8cc; color:#555; text-align:center; height:22px; border-top:1px solid rgb(216,223,234); padding-top:4px; font-size:8.5pt; font-weight:bold;">
      <a href="javascript:void(0);" onclick="javascript:deletePage( <?=$r['gid'];?>, this );">Delete Page</a>
    </div>
  </div>

<?
  $last_id = $r['gid'];
}

?>
</div>

<div style="text-align:center;">
  <a href="db_clean_up.php?offset=<?=$offset+105;?>">Next Group of Pages</a>
</div>
</body>

</html>


<?
require( "footer.php" );
?>