<?php

include "inc/inc.php";

if (!$API->isLoggedIn())
	die("error 0");

include "inc/phpmailer.php";

if ($_POST['type'] == "P")
	$type = "P";
elseif($_POST['type'] == "V")
	$type = "V";
elseif($_POST['type'] == "S")
	$type = "S";
else
  $type = "J";

if ($type != "S")
{
	$id = intval($_POST['id']);
	if ($id == 0) die("error 1");
}

$mail->From = quickQuery("select email from users where uid=" . $API->uid);
if (!strpos($mail->From, ".") || !strpos($mail->From, "@"))
	$mail->From = "share@$siteName.com";

$mail->FromName = $API->name;

$tos = split("[,; \n]", $_POST['email-to']);
foreach ($tos as $to)
{
	if (strpos($to, "@") && strpos($to, "."))
	{
		$mail->ClearAddresses();
		$mail->AddAddress($to);

		if ($type == "J")
    {
			$mail->Subject = "Check out this job listing on $siteName";
			$mail->Body    = "http://" . $_SERVER['HTTP_HOST'] . "/employment/jobs.php?d=" . $_POST['id'] . "\n\n" . $_POST['email-body'];
    }
		elseif ($type == "S")
		{
			$mail->Subject = "Check out this page on $siteName";
			$mail->Body    = "http://" . $_SERVER['HTTP_HOST'] . "/" . $_POST['id'] . "\n\n" . $_POST['email-body'];
		}
		else
		{
			$mail->Subject = "Check out this " . ($type == "V" ? "video" : "photo") . " on $siteName";
			if ($site == "m")
				$mail->Body = "http://shb.me/" . quickQuery("select jmp from " . ($type == "V" ? "videos" : "photos") . " where id=$id");
			else
			{
				unset($newUser);
				unset($user);
				
				$x = sql_query("select uid,active from users where email='" . $to . "'");
				if (mysql_num_rows($x) > 0)
					$user = mysql_fetch_array($x, MYSQL_ASSOC);
				
				if (empty($user)) //no user exists
				{
					sql_query("insert into users (email,active) values ('$to', 2)");
					$uid = mysql_insert_id();
					$user['active'] == 2;
				}
				elseif ($user['active'] == 2) //pending join
					$uid = $user['uid'];
				else
					$uid = $user['uid'];
				
				if ($user['active'] == 2)
					$newUser = "/view/$uid/" . $API->generateDeleteCommentHash($uid, $uid);
				
				$API->feedAdd($type, $id, $uid, $API->uid);
				
				$mail->Body = "http://" . $_SERVER['HTTP_HOST'] . $newUser . $API->getMediaURL($type, $id);
			}
			
			$mail->Body .= "\n\n" . $_POST['email-body'];
		}
		
		$success = $mail->Send();
	}
}

if ($success)
	echo "OK";
else
	echo "FAIL";

?>