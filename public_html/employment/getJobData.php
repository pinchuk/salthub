<?
/*
Connects to a specified URL in an attempt to pull job listing information (making it easier for the user to duplicate job information across sites).
*/

$fromEmail = true;

include( "../inc/inc.php" );
include( "../upload/photo.php" );
include( "../upload/newalbum.php" );


function curl_get_contents( $url )
{
  $header[0] = "Accept: text/xml,application/xml,application/xhtml+xml,";
  $header[0] .= "text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5";
  $header[] = "Cache-Control: max-age=0";
  $header[] = "Connection: keep-alive";
  $header[] = "Keep-Alive: 300";
  $header[] = "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7";
  $header[] = "Accept-Language: en-us,en;q=0.5";
  $header[] = "Pragma: "; // browsers keep this blank.

  $ch = curl_init($url);
//  curl_setopt($ch, CURLOPT_COOKIEFILE, "/home/mediabirdy.com/cookies_t" );
//  curl_setopt($ch, CURLOPT_COOKIEJAR, "/home/mediabirdy.com/cookies_t" );
//  curl_setopt($ch, CURLOPT_HEADER, TRUE );
//  curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
//  curl_setopt($ch, CURLOPT_ENCODING, 'gzip,deflate');

  curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT'] );
  curl_setopt($ch, CURLOPT_REFERER, 'http://www.google.com');
  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
  curl_setopt($ch, CURLOPT_TIMEOUT, 10 );

  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_URL, $url);

  $response = curl_exec($ch);
  curl_close($ch);
  return $response;
}

function getBetween( $start, $end, $str )
{
  $s_pos = stripos( $str, $start );
  if( $s_pos === false ) return "";

  $s_pos += strlen( $start );

  $e_pos = stripos( $str, $end, $s_pos );
  if( $e_pos === false ) return "";

  return substr( $str, $s_pos, $e_pos - $s_pos );
}

$len = 0;
$text = "";
$invalid_nodes = array( "script", "head", "title", "#comment", "style" );
$acceptable_children = array( "br", "h1", "h2", "h3", "p", "span", "a", "b", "#text", "form");

function getTextFromNode($n)
{
  global $text, $invalid_nodes;

  if( $n->hasChildNodes() )
  {
    foreach( $n->childNodes as $c )
    {
      if( in_array($c->nodeName, $invalid_nodes ) )
        continue;

      if( $c->nodeType == XML_ELEMENT_NODE )
        getTextFromNode( $c );
      else
      {
        $text .= $c->textContent;
      }
    }
  }
  else
  {
    if( $n->nodeName == "br" )
      $text .= "\n";
    if( !in_array($n->nodeName, $invalid_nodes ) )
      $text .= $n->textContent;
  }
}

function hasChild($p) {
  global $acceptable_children;

  if ($p->hasChildNodes())
  {
    foreach ($p->childNodes as $c)
    {
      if( in_array( $c->nodeName, $acceptable_children ) )
        continue;
      if ($c->nodeType == XML_ELEMENT_NODE)
        return true;
    }
  }
  return false;
}


function getLargestTextBlock( $nodes )
{
  global $len,$text, $invalid_nodes;

  foreach( $nodes as $node )
  {
    if( hasChild( $node ) )
    {
      getLargestTextBlock( $node->childNodes );
    }
    else
    {
      if( strlen( $node->nodeValue ) > $len && !in_array( $node->nodeName, $invalid_nodes ) )
      {
//        echo "Largest block found: " . $node->nodeName . "<br>";
        $text = "";
        getTextFromNode( $node );
        $len = strlen( $node->nodeValue );
      }
    }
  }
}

function getImages( $nodes )
{
  $result = array();

  foreach( $nodes as $c )
  {
    if( $c->hasAttributes() )
    {
      if( $c->attributes->getNamedItem("src")->nodeValue != "" )
      {
        if( ($c->attributes->getNamedItem("width")->nodeValue != "" && intval( $c->attributes->getNamedItem("width")->nodeValue ) < 20) ||
            ($c->attributes->getNamedItem("height")->nodeValue != "" && intval( $c->attributes->getNamedItem("height")->nodeValue ) < 20) ) //We dont want tracking images or very small images.
        {
          continue;
        }
        $result[] = $c->attributes->getNamedItem("src")->nodeValue;
      }
    }
  }
  return $result;
}



$url = $_POST['url'];
//$url = "http://www.linkedin.com/jobs?viewJob=&jobId=2121310&srchIndex=0&trk=njsrch_hits&goback=.fjs_marine_*1_*1_I_us_*1_*1_1_R_true_*2_*2_*2_*2_*2_*2_*2_*2";
//$url = "http://www.postjobfree.com/job/ta9nnx/yacht-broker-fireman-sales-boat-baltimore-md-21228?ipjf=SimplyHired&utm_source=SimplyHired&utm_medium=cpc&utm_campaign=SimplyHired";
if( $url == "" ) $url = $_REQUEST['url'];
$url = str_replace( "&amp;", '&', $url );

$html = curl_get_contents( $url );

if( $html == "" )
{
  echo "Sorry, we couldn't reach that website!" . chr(1);
  exit;
}


$html = str_replace( "&nbsp;", " ", $html );
$html = str_replace( "&#xa0;", " ", $html );

$html = str_replace( "<meta property", "<meta name", $html ); //convert open graph tags to normal meta tags

file_put_contents( "/tmp/job_data", $html );
$tags = get_meta_tags( "/tmp/job_data" );

$title = getBetween( "<title>", "</title>", $html );



libxml_use_internal_errors(true);
$doc = new DOMDocument();
$doc->loadHTML( $html );


if( isset( $tags['og:description'] ) )
  $body = $tags['og:description'];
else
{
  getLargestTextBlock( $doc->childNodes );

  $body = $text;
}


$images = getImages( $doc->getElementsByTagName( "img" ) );
libxml_use_internal_errors(false);

$title = html_entity_decode( $title );
$body = html_entity_decode( $body );

echo trim($title) . chr(1) . trim($body) . chr(1) . (sizeof( $images ) ? implode( chr(2), $images ) : "" );

?>