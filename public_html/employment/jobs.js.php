// Common javascript used during job creation and adding jobs to a user's feed.

function saveJob( id )
{
  getAjax("/employment/save_job.php?id=" + id, "saveJobResponse");

  e = document.getElementById('s' + id);
  if( e )
    e.innerHTML = 'job saved';
}

function saveJobResponse( data )
{
  showPopUp('', '<br /><br />&nbsp;&nbsp;&nbsp;&nbsp;<b>Job saved! View you saved jobs in "my saved jobs".</b><br /><br /><br />');
}

function removeJob( id )
{
  getAjax("/employment/remove_job.php?id=" + id, "removeJobResponse");

  e = document.getElementById('s' + id);
  if( e )
    e.innerHTML = 'job removed';

  e = document.getElementById('d' + id);
  if( e )
    e.style.display='none';
}

function removeJobResponse( data )
{
  if( data == "empty" )
  {
    e = document.getElementById('searchResults');
    if( e )
    {
      e.innerHTML = '<div style="text-align:center; margin-top:30px; margin-bottom:30px;">Sorry!  It doesn\'t look like you have any saved jobs yet.<br />Before using this page, you must first save jobs from the <a href="jobs.php">jobs search page.</a></div>';
    }
  }
}


function changePageResponse( data )
{
  e = document.getElementById( "searchResults" );
  if( e )
  {
    e.innerHTML = data;
  }
}


function shareLink( id )
{
  getAjax("/employment/jobs_share.php?d=" + id, "shareLinkResponse");
}

function shareLinkResponse( data )
{
  showPopUp('', data );
}

function addJobToLog( id )
{
  getAjax("/employment/jobs_add2log.php?d=" + id, "add2logResponse");
}

function add2logResponse( data )
{
  e = document.getElementById('add2log');
  if( e )
    e.innerHTML = "added to log";
}

