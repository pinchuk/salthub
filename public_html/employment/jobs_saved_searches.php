<?
/*
Shows a list of saved job searches.  Searches are saved as cookies. 
*/

$header_shown = false;
$params = array( "q", "title", "company", "country", "state", "city" );
$srch_typ = 'emp_search';
$save_cnt = 10;

for( $c = 0; $c < $save_cnt; $c++ )
{
  $link_text = '';

  if( isset( $_COOKIE[$srch_typ . $c] ) )
  {
    $data = explode( "&", $_COOKIE[$srch_typ . $c] );

    if( !$header_shown )
    {
      echo '<div class="subhead" style="cursor:pointer; margin-top: 8px; margin-bottom:2px;">Saved Searches</div>';
      $header_shown = true;
    }

    for( $j = 0; $j < sizeof( $data ); $j++ )
    {
      $parts = explode( "=", $data[$j] );

      for( $k = 0; $k < sizeof( $params ); $k++ )
      {
        if( $parts[0] == $params[$k] && $parts[1] != "" && $parts[1] != "0" )
        {
          if( $parts[0] == "country" ) { $parts[1] = quickQuery( "select country from loc_country where id='" . $parts[1] . "'" ); if( strlen( $parts[1] > 3 ) ) $parts[1] = ucfirst( strtolower( $parts[1] ) ); }
          if( $parts[0] == "state" ) $parts[1] = ucfirst( strtolower( quickQuery( "select region from loc_state where id='" . $parts[1] . "'" ) ) );
          if( $parts[0] == "city" ) $parts[1] = ucfirst( strtolower( quickQuery( "select city from loc_city where id='" . $parts[1] . "'" ) ) );

          if( $parts[1] == "-" ) continue;

          if( $link_text != '' ) $link_text .= " + ";
          $link_text .= $parts[1];
        }
      }
    }

    echo '<a style="font-size:9pt;" href="jobs.php?' . $_COOKIE[$srch_typ . $c] . '">' . $link_text . '</a><div style="height:10px;"></div>';
  }
}
?>