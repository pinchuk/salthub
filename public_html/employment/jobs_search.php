<?
/*
Searches and displays job listings.  Searches are entered through the side bar (left side) of the jobs search page.

*/

$scripts[] = "http://s7.addthis.com/js/250/addthis_widget.js#username=mediabirdy";
$scripts[] = "/email.js";
$scripts[] = "http://api.simplyhired.com/c/jobs-api/js/xml-v2.js";

include_once( "../inc/inc.php" );
include_once( "employment_functions.php" );

$join = "";

$page = 0;
if( isset( $_GET['p'] ) )
  $page = $_GET['p'];

$results_per_page = 10;
$result_index = $page * $results_per_page;

if( isset($_GET['sj']) ) //Saved jobs; don't bother looking on Simply Hired
  $sql = "select * from jobs_saved js left join jobs on jobs.id=js.job where complete=2 and approved=1 and jobs.funded=1 and js.uid='" . $API->uid . "'";
else
{
  //Normal Query
  $sql = "select * from jobs where complete=2 and approved=1";

  if( isset( $_GET['company'] ) && $_GET['company'] != "" )
  {
    $query = addslashes($_GET['company']);
    $sql = "select jobs.* from jobs left join pages on pages.gid=jobs.gid where jobs.complete=2 and jobs.approved=1 AND jobs.funded=1 and pages.gname like '%$query%'";
  }
  elseif( isset( $_GET['q'] ) && $_GET['q'] != "" )
  {
    $query = addslashes($_GET['q']);
    $sql = "select jobs.* from jobs left join pages on pages.gid=jobs.gid where jobs.funded=1 AND jobs.complete=2 AND jobs.approved=1 AND (pages.gname like '%$query%' or jobs.title like '%$query%' or jobs.body like '%$query%')";
  }

  if( isset( $_GET['position'] ) && $_GET['position'] != "" )
  {
    $position = intval($_GET['position']);
    $sql .= " and jobs.position='$position'";
    if( $_GET['q'] == "" ) $_GET['q'] = quickQuery( "select gname from pages where gid='" . addslashes($_GET['position']) . "'" );
  }

  if( isset( $_GET['title'] ) && $_GET['title'] != "" )
  {
    $query = addslashes($_GET['title']);
    $sql .= " and jobs.title like '%$query%'";
  }
  if( $_GET['country'] > 0 )
  {
    $country = $_GET['country'];
    $sql .= " and jobs.country='$country'";
  }
  if( $_GET['state'] > 0 )
  {
    $st = $_GET['state'];
    $sql .= " and jobs.state='$st'";
  }
  if( $_GET['city'] > 0 )
  {
    $city = $_GET['city'];
    $sql .= " and jobs.city='$city'";
  }
}

$sql .= " order by created desc";


$q = mysql_query( $sql );

$num_results = mysql_num_rows( $q ); //Number of results from SaltHub

$num_results_shown = 0;

if( $result_index < $num_results ) //We are still within the range of SaltHub job search results
{
  $sql .= " limit $result_index, $results_per_page";
  $q = sql_query( $sql );
  $num_results_shown = mysql_num_rows( $q );

  while( $r = mysql_fetch_array( $q ) )
  {
    $hash = quickQuery( "select hash from photos where id=" . $r['pid'] );

    $url = "/employment/jobs_detail.php?d=" . $r['id'];

    $data = queryArray( "select name,pic from users where uid='" . $r['uid'] . "'"  );
    $name = $data['name'];
    $photo = $data['pic'];
    $hash2 = quickQuery( "select hash from photos where id=" . $photo );
  ?>
    <div class="job_listing" id="d<? echo $r['id'] ?>" style="padding:5px; float:left; overflow:hidden; border-bottom:1px dotted #326798; width:535px;" onmouseover="javascript:document.getElementById('addthis-<?=$r['id']?>').style.visibility='visible'; document.getElementById('text-<?=$r['id']?>').style.visibility='visible';" onmouseout="javascript:document.getElementById('addthis-<?=$r['id']?>').style.visibility='hidden'; document.getElementById('text-<?=$r['id']?>').style.visibility='hidden';">
      <div style="width:430px; overflow:hidden; float:left; margin-top:15px;">
        <div class="image" id="img"><img src="/img/100x75/photos/<? echo $r['pid'] ?>/<? echo $hash ?>.jpg" width="100" height="75" /></div>
        <div class="title" id="title"><a href="jobs_detail.php?d=<? echo $r['id']; ?>"><? echo $r['title']; ?></a> - <span class="subtitle2"><? echo getSubTitle( $r ); ?></span></div>
  <!--      <div class="subtitle" id="lsubtitle"><?if( isset( $r ) ) echo getSubTitle( $r ); ?></div>-->
        <div class="body" id="body"><? echo nl2br( cutOffText( $r['body'], 100 ) );?></div>
      </div>

  		<div style="float:right; margin-top:10px;" id="addthis-<?=$r['id']?>" class="addthis">
  			<div style="height: 22px;"><a class="addthis_button_facebook" addthis:url="http://<?=SERVER_HOST?><?=$url?>"></a></div>
  			<div style="height: 22px;"><a class="addthis_button_twitter" addthis:description="http://<?=SERVER_HOST?><?=$url?>" addthis:title="<?=$r['title']?>" addthis:url="http://<?=SERVER_HOST?><?=$url?>"></a></div>
  			<div><a onclick="javascript:showEmail('J', <?=$r['id']?>);" href="javascript:void(0);"><span class="at300bs at15t_email"></span></a></div>
  			<div style="padding-top:7px;"><a href="javascript:void(0);" onclick="javascript:showReport('j', <?=$r['id']?>);"><img src="/images/report.png" alt="" style="vertical-align: bottom;" /></a></div>
  		</div>

      <div style="clear:left; float:left; padding-left:108px; font-size:8pt; margin-top:2px; visibility:hidden;" id="text-<?=$r['id']?>">
        <div style="float:left;">Posted <? echo timeToHumanTime( strtotime($r['created']) );?> ago</div>
<? if( quickQuery( "select count(*) from jobs_saved where uid='" . $API->uid . "' and job='" . $r['id'] . "'" ) > 0 ) { ?>
        <div style="float:left; margin-left:20px;" id="s<? echo $r['id'] ?>"><a href="javascript:void(0);" onclick="javascript:removeJob(<?=$r['id'];?>);">remove saved job</a></div>
<? } else { ?>
        <div style="float:left; margin-left:20px;" id="s<? echo $r['id'] ?>"><a href="javascript:void(0);" onclick="javascript:saveJob(<?=$r['id'];?>);">save job</a></div>
<? } ?>
        <div style="float:left; margin-left:20px;"><a href="javascript:void(0);">similar jobs</a></div>

        <div style="float:left; margin-left:20px;"><a href="javascript:void(0);" onclick="javascript:showSendMessage(<?=$r['uid'];?>, '<?=$name?>', '/img/N85x128/photos/<? echo $photo ?>/<? echo $hash2 ?>.jpg');">send message</a></div>


      </div>

    </div>

    <div style="clear:both;"></div>
  <?
  }
}

if( isset( $_GET['q'] ) )   //Begin SimplyHired Query.  Only search SiHi if we have a real query.
{
  $sihi_page = 0;
  if( $page > 0 )
  {
    $sihi_page = $page+1; //sihi pages don't seem to be zero based.
    if( $num_results > 0 ) $sihi_page -= floor($num_results / $results_per_page);
  }

  $query = "";

  if( $_GET['q'] != "" )
  {
    $query_array = explode( " ", $_REQUEST['q'] );
    for( $c = 0; $c < sizeof( $query_array ); $c++ )
    {
      if( $c > 0 )
        $query .= " OR ";
      $query .= $query_array[$c];
    }
  }

  if( $_GET['company'] != "" )
  {
    if( strlen( $query ) > 0 ) $query .= " OR ";
    $query .= " " . $_GET['company'];
  }

  if( $_GET['title'] != "" )
  {
    if( strlen( $query ) > 0 ) $query .= " AND ";
    $query .= "title:(" . $_GET['title'] . ")";
  }
/*
  if( $_GET['company'] != "" )
  {
    if( strlen( $query ) > 0) $query .= " AND ";
    $query .= "company:(" . $_GET['company'] . ")";
  }
*/
  if( strlen( $query ) > 0 ) $query .= " AND ";
  $query .= "(onet:(17-*) OR onet:(53-*) OR onet:(47-*))";

  //Build Location
  $location = "";
  if( $_GET['city'] > 0 )
    $location .= quickQuery( "select city from loc_city where id=" . $_GET['city'] ) . ",";
  if( $_GET['state'] > 0 )
    $location .= quickQuery( "select region from loc_state where id=" . $_GET['state'] ) . ",";
//  if( $_GET['country'] > 0 )
//    $location .= quickQuery( "select country from loc_country where id=" . $_GET['country'] );

  $query = urlencode( $query );
  $location = urlencode( $location );

  $source = "http://api.simplyhired.com/a/jobs-api/xml-v2/q-$query/l-$location/pn-$sihi_page?pshid=37616&ssty=2&cflg=r&jbd=salthub.jobamatic.com&clip=" . $_SERVER['REMOTE_ADDR'];

  $xml_data = file_get_contents( $source );

  $validResult = true;

  if( strlen( $xml_data ) == 0 ) $validResult = false;

  try
  {
    $xml = new DOMDocument();
    $xml->preserveWhiteSpace = false;
    $xml->formatOutput = true;
    $xml->loadXML($xml_data);

    $rt = $xml->getElementsByTagName( "tr" )->item(0);

    if( $rt != NULL )
    {
      $num_results += intval( $rt->nodeValue ); //Add simply hired results to our total number of results.

      $rs = $xml->getElementsByTagName( "rs" )->item(0);
      $r_list = $rs->getElementsByTagName( "r" );
    }
    else
      $validResult = false;
  }
  catch (Exception $e)
  {
    echo "exception";
    $validResult = false;
  }

  if( $num_results_shown < $results_per_page && $validResult ) //We need to show results from SimplyHired
  {

    foreach( $r_list as $r )
    {
      foreach( $r->childNodes as $child )
      {
        switch( $child->nodeName )
        {
          case "jt": $title = $child->nodeValue; break;
          case "cn": $company = $child->nodeValue; break;
          case "src": $url = $child->attributes->getNamedItem("url")->nodeValue; break;
          case "loc":
            $city = $child->attributes->getNamedItem("cty")->nodeValue;
            $st = $child->attributes->getNamedItem("st")->nodeValue;
            if( $st == "" ) $st = $child->attributes->getNamedItem("region")->nodeValue;
            $country = $child->attributes->getNamedItem("country")->nodeValue;

            $loc = "";
            if( $city != "" ) $loc = $city . ", ";
            if( $st != "" ) $loc .= $st . ". ";
            if( $country != "" ) $loc .= $country;
          break;
          case "dp": $listed = $child->nodeValue; break;
          case "e": $body = $child->nodeValue; break;
        }
      }
  ?>
      <div class="job_listing" style="padding:5px; float:left; overflow:hidden; border-bottom:1px dotted #326798; width:535px;">
        <div style="width:430px; overflow:hidden; float:left; margin-top:15px; margin-bottom:15px;">
          <div class="title" id="title" style="width:100%; margin-left:0px;"><a href="<?=$url?>" onMouseDown="xml_sclk(this);"><? echo $title; ?></a> - <span class="subtitle2"><? echo $company . " - " . $loc ?></span></div>
          <div class="body" id="body" style="width:100%; margin-left:0px;"><? echo $body; ?></div>
        </div>
      </div>

      <div style="clear:both;"></div>
  <?
      $num_results_shown++;
    }
  }


}

$pages = floor($num_results / $results_per_page);

//Pagination
?>

<div class="pagination">
<?
if ($pages > 0)
{
	if ($page > 0)
		echo '<a href="javascript:void(0);" onclick="javascript: changePage(' . ($page - 1) . ');">&lt;</a>&nbsp; ';
	else
		echo "&lt;&nbsp; ";

	$i = $page - 3;
	if ($i < 0) $i = 0;
	$j = $i + 7;

	for (; $i < $pages && $i < $j; $i++)
		if ($i == $page)
			echo "<b>" . ($i + 1) . "</b>&nbsp; ";
		else
			echo '<a href="javascript:void(0);" onclick="javascript: changePage(' . $i . ');">' . ($i + 1) . '</a>&nbsp; ';

	if ($page < $pages - 1)
		echo '<a href="javascript:void(0);" onclick="javascript: changePage(' . ($page + 1) . ');">&gt;</a>&nbsp; ';
	else
		echo "&gt;&nbsp; ";
}
?>
</div>

<?
if( $num_results_shown == 0 )
{
  if( isset($_REQUEST['sj']) )
  {
  ?>
  <div style="text-align:center; margin-top:30px;">Sorry!  It doesn't look like you have any saved jobs yet.<br />Before using this page, you must first save jobs from the <a href="jobs.php">jobs search page.</a></div>
  <?
  }
  else
  {
  ?>
  <div style="text-align:center; margin-top:30px;">Sorry!  No jobs met your search criteria.<br />Please <a href="index.php">search again</a>.</div>
  <?
  }
}
else
{
?>
<div style="text-align: left; clear:both; font-size:8pt; color:#555;"><a STYLE="text-decoration:none" href="http://www.simplyhired.com/"><span style="color: #555;">Jobs</span></a> by <a STYLE="text-decoration:none" href="http://www.simplyhired.com/"><img src="/images/logo_sh_80x18.gif" width="80" height="18" alt="" /></a></div>
<? } ?>
