<?php
/*
This code dynamically fills in states (regions), countries and city combo boxes.  Used in the job listing creation process.
*/
include "../inc/inc.php";

$f = $_GET['f'];

if ($f == "state")
{
	$table = "state";
	$where = "country_id";
	$select = "region";
}
else
{
	$f = "city";
	$table = "city";
	$where = "region_id";
	$select = "city";
}

echo $f . '|';

if ($f == "state")
	echo '<select id="user-' . $table . '" name="' . $table . '" onchange="javascript:updateLocationSelect(\'city\', this.value);" style="width: 160px;">';
else
	echo '<select id="user-' . $table . '" name="' . $table . '" onchange="javascript:" style="width: 160px;">';

$x = mysql_query("select id,$select from loc_$table where $where=" . intval($_GET['v']) . " order by $select");
while ($y = mysql_fetch_array($x, MYSQL_ASSOC))
	echo '<option value="' . $y['id'] . '">' . addslashes(ucwords(strtolower($y[$select]))) . '</option>';

echo '</select>';

?>