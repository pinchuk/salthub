<?
/*
Displays a list of the jobs that the user has entered.
*/

include_once( "../inc/inc.php" );

$page = $_REQUEST['p'];
$p = $page * 5;

$q = sql_query( "select * from jobs where uid='" . $API->uid . "'" );
$pages = mysql_num_rows( $q )/5;

$q = sql_query( "select * from jobs where uid='" . $API->uid . "' limit $p,5" );

if( $script == "employment/view_my_jobs" ) echo '<div style="margin-left:15px; width:100%; float:left; margin-right:5px;">';
else echo '<div style="margin-left:5px; margin-top:-35px; width:430px; float:right;  margin-right:5px;">';
?>

    <div class="strong" style="margin-top:10px; font-size:13pt;">
		  Your Job Listings
		</div>


<?
while( $r = mysql_fetch_array( $q ) )
{
  $hash = quickQuery( "select hash from photos where id=" . $r['pid'] );

?>
    <div class="job_listing" style="padding:5px; float:left; overflow:hidden; border-bottom:1px solid #d8dfea;">
      <div style="height:90px; overflow:hidden;">
        <div class="title" id="title"><? echo $r['title']; ?></div>
        <div class="image" id="img"><img src="/img/100x75/photos/<? echo $r['pid'] ?>/<? echo $hash ?>.jpg" width="100" height="75" /></div>
        <div class="body" id="body"><? echo nl2br($r['body']);?></div>
      </div>

      <div style="clear:both; margin-top:4px;">
        <div style="float:left; font-size:8pt;"><b>Status:</b> <? echo getStatus( $r['complete'] ); ?></div>
        <div style="float:left; font-size:8pt; margin-left:10px; font-weight:bold;"><? if( $r['approved'] == 0 ) echo 'Pending Approval'; else echo 'Approved'; ?></div>
        <div style="float:left; font-size:8pt; margin-left:10px;"><a href="jobs_detail.php?d=<? echo $r['id']; ?>">view</a>&nbsp;&nbsp;&nbsp;<a href="emp_create.php?ID=<? echo $r['id']; ?>">edit</a>&nbsp;&nbsp;&nbsp;<a href="delete_listing.php?ID=<? echo $r['id']; ?>" onclick="return confirm('Are you sure you want to delete this job listing?');">delete</a></div>
        <div style="float:right; font-size:8pt;"><i><? echo date( "M d, Y", strtotime( $r['created'] ) ); ?></i></div>
      </div>
    </div>
<?
}
?>
  <div class="pagination" style="clear:both; padding-right:10px;">
  <?php

  if ($pages > 0)
  {
  	if ($page > 0) //  		echo "<a href=\"/$script.php?p=" . ($page - 1) . $query . "\">&lt;</a>&nbsp; ";
  		echo '<a href="javascript:void(0);" onclick="loadPage(' . ($page-1) . ');">&lt;</a>&nbsp; ';
  	else
  		echo "&lt;&nbsp; ";

  	$i = $page - 3;
  	if ($i < 0) $i = 0;
  	$j = $i + 7;

  	for (; $i < $pages && $i < $j; $i++)
  		if ($i == $page)
  			echo "<b>" . ($i + 1) . "</b>&nbsp; ";
  		else
  			echo '<a href="javascript:void(0);" onclick="loadPage(' . $i . ');">' . ($i + 1) . '</a>&nbsp; ';

  	if ($page < $pages - 1)
  		echo '<a href="javascript:void(0);" onclick="loadPage(' . ($page+1) . ');">&gt;</a>&nbsp; ';
  	else
  		echo "&gt;&nbsp; ";
  }
  ?>
  </div>

  </div>



<?
function getStatus( $state )
{
  switch( $state )
  {
    case 0: return "incomplete";
    case 1: return "inactive";
    case 2: return "active";
  }
  return "unknown";
}
?>