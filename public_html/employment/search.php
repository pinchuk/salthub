<?
/*
Search page for employers (users) to search for users that are seeking employment.  Users indicate that they are seeking employment on their profile page.
*/

include_once( "../inc/inc.php" );
include_once( "employment_functions.php" );
$num_conditions = 6;

for( $c = 0; $c <= $num_conditions; $c++ )
{
  if( $_POST['c' . $c] != "" && $_POST['c' . $c] != "0")
    $conds[$c] = explode( ",", $_POST['c' . $c] );
  else if( $_GET['c' . $c] != "" && $_GET['c' . $c] != "0")
    $conds[$c] = explode( ",", $_GET['c' . $c] );
}

//Page searches
$uids = "";
$gids = $_POST['c4'];
if( $_POST['c4'] != "" && $_POST['c5'] ) $gids .= ",";
$gids .= $_POST['c5'];
$gids = addslashes($gids); //This shouldn't do anything, but should prevent a SQL injection.

//Reconstruct Query String for sorting and changing pages.
$query = "";
foreach ($_POST as $k => $v)
	if ($k != "p")
		$query .= "&" . htmlentities("$k=$v");
foreach ($_GET as $k => $v)
	if ($k != "p")
		$query .= "&" . htmlentities("$k=$v");

 $page = 0;
if( isset( $_GET['p'] ) )
  $page = $_GET['p'];
$results_per_page = 16;
$result_index = $page * $results_per_page;

if( sizeof( $conds[4] ) > 0  || sizeof( $conds[5] ) > 0 )
{
  $uids = "0";
  $sql = "select distinct uid from personal where gid in ($gids)";
  $q = sql_query( $sql );

  while( $r = mysql_fetch_array( $q ) )
  {
    $uids .= "," . $r['uid'];
  }
}

$sql = "select uid, if(pic=0,pic0,if(pic=1,pic1,pic)) as pic, employment_position, employment_location, name from users where seeking_employment!=0";
if( strlen( $uids ) > 0 ) $sql .= " and uid in ($uids)";

for( $i = 0; $i < 4; $i++ )
{
  if( sizeof( $conds[$i] ) > 0 )
  {
    $sql .= " and (";
    for( $c = 0; $c < sizeof( $conds[$i] ); $c++ )
    {
      if( $c > 0 ) $sql .= " OR ";
      switch( $i )
      {
        case 0: $sql .= "employment_sector like '%," . $conds[$i][$c] . ",%'"; break;
        case 1: $sql .= "employment_position like '%," . $conds[$i][$c] . ",%'"; break;
        case 2: $sql .= "employment_location like '%," . $conds[$i][$c] . ",%'"; break;
        case 3: $sql .= "loc_country=" . $conds[$i][$c]; break;
      }

    }
    $sql .= ")";
  }
}

$sql .= " order by users.lastlogin desc";

$q = sql_query( $sql );
echo mysql_error();
$pages = mysql_num_rows( $q ) / $results_per_page;

$sql .= " limit $result_index, $results_per_page";

$q = sql_query( $sql );
echo mysql_error();

while( $r = mysql_fetch_array( $q ) )
  $results[] = $r;

?>

<div class="subhead" style="cursor:pointer; margin-top: 10px; margin-bottom:-10px; margin-right:8px;");">
  Employees
</div>

<div style="width:100%; margin-top:10px; clear:both;">
<?
$rowCount = 0;

if( mysql_num_rows( $q ) == 0 )
{
?>
  <div>Sorry, no results were found matching your criteria.</div>
<?
}
else
foreach( $results as $r )
{
  $rowCount++;
  if( $rowCount == 5 )
  {
    echo '<div style="clear:both"></div>';
    $rowCount = 1;
  }

  $positions = gidToName( $r['employment_position'], "", "no position selected", $r['uid'] );
  $locations = countryToName( $r['employment_location'], "", "no location selected", $r['uid'] );
  $profileUrl = $API->getProfileURL( $r['uid'] );

  if( $r['pic'] > 0 )
  {
    $picurl = '/img/119x95/photos/ ' . $r['pic'] .'/' . quickQuery( "select hash from photos where id='" . $r['pic'] . "'" ) . '.jpg';
  }
  else
  {
    $picurl = '/images/nouser.jpg';
  }
?>
  <div class="jobs-profile">
    <div style="left:auto; right:auto;">
      <a href="<?=$profileUrl; ?>"><img border="0" src="<? echo $picurl ?>" width="119" height="95"></a>
      <div><a href="<?=$profileUrl; ?>"><? echo $r['name']; ?></a></div>
      <div><b><? echo $positions; ?></b></div>
      <div><? echo $locations; ?></div>
      <div><a href="javascript:void(0);" onclick="javascript:showSendMessage(<? echo $r['uid']; ?>, '<?=$r['name'];?>', '/img/48x48/<? echo $picurl ?>');">send message</a></div>
    </div>
  </div>
<?
}
?>
</div>

<div class="pagination" style="clear:both;">
<?
if ($pages > 0)
{
	if ($page > 0)
		echo '<a href="javascript:void(0);" onclick="javascript: changePage(' . ($page - 1) . ', \'' . $query . '\');">&lt;</a>&nbsp; ';
	else
		echo "&lt;&nbsp; ";

	$i = $page - 3;
	if ($i < 0) $i = 0;
	$j = $i + 7;

	for (; $i < $pages && $i < $j; $i++)
		if ($i == $page)
			echo "<b>" . ($i + 1) . "</b>&nbsp; ";
		else
			echo '<a href="javascript:void(0);" onclick="javascript: changePage(' . $i . ', \'' . $query . '\');">' . ($i + 1) . '</a>&nbsp; ';

	if ($page < $pages - 1)
		echo '<a href="javascript:void(0);" onclick="javascript: changePage(' . ($page + 1) . ', \'' . $query . '\');">&gt;</a>&nbsp; ';
	else
		echo "&gt;&nbsp; ";
}
?>
</div>




