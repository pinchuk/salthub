<?
/*
Creates an XML feed of the jobs listed on SaltHub
*/

$noLogin = true;
require( "../inc/inc.php" );

header( 'Content-type: text/xml' );

function addField( &$doc, $branch, $value )
{
  $branch_obj = $doc->createElement( $branch );
  $branch_obj->appendChild( $doc->createTextNode( $value ) );
  return $branch_obj;
}



$document = DOMImplementation::createDocument(null, 'jobs' );
$jobs = $document->documentElement;

$q = mysql_query( "select * from jobs where approved=1" );
while( $r = mysql_fetch_array( $q  ) )
{
  $job = $document->createElement( 'job' );

  $job->appendChild( addField($document, 'title', $r['title']) );
  $job->appendChild( addField($document, 'job-code', $r['id']) );
  $job->appendChild( addField($document, 'posted-date', date( "m/d/Y", strtotime($r['created']) ) ) );
  $job->appendChild( addField($document, 'detail-url', "http://" . SERVER_HOST . "/employment/job_detail.php?d=" . $r['id']) );

  //Description
  $desc = $document->createElement('description');
  $desc->appendChild( addField($document, 'summary', $r['body']) );
  $job->appendChild( $desc );

  //Location
  $loc = $document->createElement('location');
  $loc->appendChild( addField($document, 'state', quickQuery( "select region from loc_state where id='" . $r['state'] . "'" ) ) );
  $loc->appendChild( addField($document, 'city', quickQuery( "select city from loc_city where id='" . $r['city'] . "'" ) ) );
  $loc->appendChild( addField($document, 'country', quickQuery( "select country from loc_country where id='" . $r['country'] . "'" ) ) );
  $job->appendChild( $loc );

  //Company
  $co = $document->createElement('company');
  if( $r['gid'] > 0 )
    $co->appendChild( addField($document, 'name', quickQuery( "select gname from pages where gid='" . $r['gid'] . "'" ) ) );
  else
    $co->appendChild( addField($document, 'name', quickQuery( "select name from users where uid='" . $r['uid'] . "'" ) ) );

  if( $r['sector'] > 0 )
    $co->appendChild( addField($document, 'industry', quickQuery( "select catname from categories where cat='" . $r['sector'] . "'" ) ) );
  $co->appendChild( addField($document, 'description', $r['about_the_company'] ) );
  $job->appendChild( $co );

  $jobs->appendChild($job);
}

echo $document->saveXML();
?>
