<?php
/**
 * User has successfully authenticated with Twitter. Access tokens saved to session and DB.

This script is called when a user signs in through Twitter.  It will create a new user for those
who are not currently users, merge Facebook and Twitter accounts, and perform the normal login
functions.

 */


/* Load required lib files. */
include "../inc/inc.php";
require_once('twitteroauth/twitteroauth.php');
//require_once('config.php');

/* If access tokens are not available redirect to connect page. */
if (empty($_SESSION['access_token']['oauth_token']) || empty($_SESSION['access_token']['oauth_token_secret'])) {
    header('Location: ./clearsessions.php');
}
/* Get user access tokens out of the session. */
$access_token = $_SESSION['access_token'];

if( empty( $access_token ) )
{
  exit;
}

/* Create a TwitterOauth object with consumer/user tokens. */
$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $access_token['oauth_token'], $access_token['oauth_token_secret']);

/* If method is set change API call made. Test is called by default. */
$content = $connection->get('account/verify_credentials');

if (empty($content->screen_name))
	header("Location: /");

$x = sql_query("select active,uid,name,admin" . ($site == "s" ? ",verify" : "") . " from users where twid=" . $content->id);

if (empty($API->uid) && mysql_num_rows($x) == 0)
{
	// create brand new user with tw info
	sql_query("insert into users (joined,ip,lastlogin,twusername,twid,pic,name,twtoken,twsecret) values (now()," . ip2long($_SERVER['REMOTE_ADDR']) . ",now(),'" . addslashes($content->screen_name) . "'," . $content->id . ",1,'" . addslashes($content->name) . "','" . $access_token['oauth_token'] . "','" . $access_token['oauth_token_secret'] . "')");
	$_SESSION['uid'] = mysql_insert_id();
	$_SESSION['name'] = $content->name;
	$_SESSION['pic'] = 1;
	$_SESSION['admin'] = 0;
	$API->uid = $_SESSION['uid'];
	spamWall();
	$API->feedAdd("J", SITE_TWITTER);
	$API->addReferral();
  $API->name = $content->name;

  $_GET['cat'] = 2; //Set the photo category
  unset($_GET['userpic']);
	$_SESSION['pic'] = $API->saveUserPicture(SITE_TWITTER, str_lreplace("_normal", "", $content->profile_image_url));

  sql_query("update users set pic={$_SESSION['pic']} where uid={$API->uid}");

	exec("/usr/local/bin/php " . $_SERVER['DOCUMENT_ROOT'] . "/../updatesocial.php " . $_SESSION['uid']);

  $API->twFollow( SITE_TWITTER_ID, SITE_TWITTER_SN );
  $API->prepWelcomeEmail();

  $uid = $API->uid;
  include( "../inc/create_you_have_connections_email.php" );

	if ($site == "m")
		header("Location: /");
	else
  {
		header("Location: /signup/invite-ps.php");
  }
}
else
{
	// existing user
	if (!$_SESSION['addacct'] || $_SESSION['twReturnToInvite'] )
	{
    if( mysql_num_rows( $x ) == 0 )
      $x = sql_query("select active,uid,name,admin" . ($site == "s" ? ",verify" : "") . " from users where uid=" . $API->uid);

    $info = mysql_fetch_array($x, MYSQL_ASSOC);
    if ($info['active'] == -1) //banned
    {
    	header("Location: /banned.php");
    	die();
    }

    $_SESSION['uid'] = $info['uid'];
    $_SESSION['name'] = $info['name'];
    $_SESSION['admin'] = $info['admin'];
    $_SESSION['verify'] = $info['verify'];
    $API->restoreSessionVariables($info['uid']);

	}
	elseif ($API->uid > 0) //wanting to activate user's twitter account
	{
		$twid = quickQuery("select count(*) from users where twid=" . $content->id);
	}
	else //somehow they got here
	{
        $query = "insert into sessions(`uid`, `url`, `date`) values(".$API->uid.", '".$_SERVER['REQUEST_URI']."', '".date('Y-m-d H:i:s')."')";
        mysql_query($query);

		header("Location: /logout.php");
		die();
	}

	if ($twid == 1) //user already signed in and tried to login with a twitter account - must be trying to merge accts
	{
		$_SESSION['merge'] = array("twid" => $content->id, "access_token" => $_SESSION['access_token'], "twusername" => addslashes($content->screen_name), "acct" => "t");
		unset($_SESSION['twid']);
		unset($_SESSION['access_token']);
		$API->saveUserPicture(SITE_TWITTER, str_lreplace("_normal", "", $content->profile_image_url));
		header("Location: /merge.php");
		die();
	}
	else
	{
		$_SESSION['pic'] = quickQuery("select pic from users where uid=" . $_SESSION['uid']);
		sql_query("update users set ip=" . ip2long($_SERVER['REMOTE_ADDR']) . ",active=1,lastlogin=now(),twid=" . $content->id . ",twusername='" . addslashes($content->screen_name) . "',twtoken='" . $access_token['oauth_token'] . "',twsecret='" . $access_token['oauth_token_secret'] . "' where uid=" . $_SESSION['uid']);
	}

//	$pic = $API->saveUserPicture(SITE_TWITTER, $content->profile_image_url );
	$pic = $API->saveUserPicture(SITE_TWITTER, str_lreplace("_normal", "", $content->profile_image_url));

	if( $pic >= 0 )
  {
    //New profile picture
    $_SESSION['pic'] = $pic;
    sql_query("update users set pic={$_SESSION['pic']} where uid={$API->uid}");
  }

  if( isset( $_SESSION['twReturnToInvite'] ) )
  {
    $url = urldecode($_SESSION['twReturnToInvite']);

    unset($_SESSION['twReturnToInvite']);
		unset($_SESSION['addacct']);

    $API->twFollow( SITE_TWITTER_ID, SITE_TWITTER_SN );

		spamWall();
		$API->feedAdd("J", SITE_TWITTER, $_SESSION['uid'], $_SESSION['uid']);

		if( isset( $_SESSION['joingid'] ) )
    {
  		$gid = intval($_SESSION['joingid']);
  		sql_query("insert into page_members (gid,uid) values ({$gid}," . $_SESSION['uid'] . ")");

      if( isset( $_SESSION['friend_uid'] ) )
      {
        sql_query( "insert into friends (id1, id2, status) values (" . $_SESSION['uid'] . "," . $_SESSION['friend_uid'] . ", 0)" );
    		$API->feedAdd("G", $gid, $_SESSION['uid'], $_SESSION['friend_uid'], $gid );
      }
      else
        $API->feedAdd("G", $gid, $_SESSION['uid'], $_SESSION['uid'], $gid );
    }

		header("Location: ../" . $url . ".php");
  } elseif ($_SESSION['addacct'])
	{
		unset($_SESSION['addacct']);
		spamWall();
		$API->feedAdd("J", SITE_TWITTER, $_SESSION['uid'], $_SESSION['uid']);

    $API->twFollow( SITE_TWITTER_ID, SITE_TWITTER_SN );

		if( isset( $_SESSION['joingid'] ) )
    {
  		$gid = intval($_SESSION['joingid']);
  		sql_query("insert into page_members (gid,uid) values ({$gid}," . $_SESSION['uid'] . ")");

      if( isset( $_SESSION['friend_uid'] ) )
      {
        sql_query( "insert into friends (id1, id2, status) values (" . $_SESSION['uid'] . "," . $_SESSION['friend_uid'] . ", 0)" );
    		$API->feedAdd("G", $gid, $_SESSION['uid'], $_SESSION['friend_uid'], $gid );
      }
      else
        $API->feedAdd("G", $gid, $_SESSION['uid'], $_SESSION['uid'], $gid );
    }

		header("Location: /");
	}
	elseif (isset($_SESSION['lastMediaViewed']))
		header("Location: " . $API->getMediaURL($_SESSION['lastMediaViewed']['type'], $_SESSION['lastMediaViewed']['id']));
	else
  {
    if( isset( $_SESSION['login_redirect'] ) )
      header( "Location: " . $_SESSION['login_redirect'] );
    else
    	header("Location: /welcome");
    $_SESSION['login_redirect'] = '';
  }
}


//tweet that the user just joined mb
function spamWall()
{
	global $connection;
  global $site;

  switch( $site )
  {
    case "s":
      $message = "Joined @" . SITE_TWITTER_SN . " A Career Management Utility and Business Marketing Platform for those in the Maritime Industry. Create your free account today to connect with professionals, businesses and explore new opportunities. http://j.mp/" . SERVER_JMP;
    break;

    default:
    	$message = "http://j.mp/" . SERVER_JMP . " Look at this! It lets us share our videos and photos on Facebook and Twitter with one upload, in real time!";
    break;
  }
	$connection->post("statuses/update", array("status" => $message));
}

/* Some example calls */
//$connection->get('users/show', array('screen_name' => 'abraham')));
//$connection->post('statuses/update', array('status' => date(DATE_RFC822)));
//$connection->post('statuses/destroy', array('id' => 5437877770));
//$connection->post('friendships/create', array('id' => 9436992)));
//$connection->post('friendships/destroy', array('id' => 9436992)));