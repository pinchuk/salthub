<?php

include "../inc/inc.php";

$my_url = "http://www.salthub.com/fbconnect/loggedin.php";

$code = $_REQUEST["code"];

if(empty($code)) {
  $_SESSION['state'] = md5(uniqid(rand(), TRUE)); // CSRF protection
  $dialog_url = "https://www.facebook.com/dialog/oauth?client_id="
   . $fbAppId . "&redirect_uri=" . urlencode($my_url) . "&state="
   . $_SESSION['state'] . "&scope=user_status,publish_stream,email,manage_pages";

  header( "Location: $dialog_url");
}

/*

$facebook = $API->startFBSession(null, "nothing");
$fbLoginUrl = $facebook->getLoginUrl(
	array(
		"req_perms" => "user_status",
		"redirect_uri" => "http://" . $_SERVER['HTTP_HOST'] . "/fbconnect/loggedin.php"
		)
	);

header("Location: $fbLoginUrl");
*/
?>