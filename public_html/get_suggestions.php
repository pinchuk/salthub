<?php
/*
Generates content for the home page "connect and engage" section, which is basically a list of
suggested contacts to connect to.

*/

ob_start();

//if (!isset($API)) //not from an include
	include_once "inc/inc.php";

global $API;

if (!$suggestions_quiet)
$num_results = doGetSuggestions();

function doGetSuggestions()
{
	global $API;
	global $suggestions_quiet;
//Load blocked uids
$blocked_uids = array();
$q = mysql_query( "select blocked_uid from blocked_users where uid='" . $API->uid . "'" );
while( $r = mysql_fetch_array( $q ) )
  $blocked_uids[] = $r['blocked_uid'];

if( empty( $limit ) )
{
  if( isset( $_GET['limit'] ) )
    $limit = intval( $_GET['limit'] );
  else
    $limit = 3;
}

$uids = array();
$friends = array();

$q = sql_query( "select if(id1=" . $API->uid . ",id2,id1) as uid,status from friends where (" . $API->uid . ") in (id1,id2)" );
while( $r = mysql_fetch_array( $q ) )
  if( $r['status'] == 1 )
    $friends[] = $r['uid'];
  else
    $uids[] = $r['uid'];

$q = sql_query( "select id2 from friend_suggestions where id1='" . $API->uid . "'" );
while( $r = mysql_fetch_array( $q ) )
  if( !in_array( $r['id2'], $friends ) )
    $uids[] = $r['id2'];


$q = sql_query( "select eid from contacts where site='2' and uid='" . $API->uid . "' and eid" );
while( $r = mysql_fetch_array( $q ) )
  if( !in_array( $r['eid'], $friends ) )
    $uids[] = $r['eid'];

$q = sql_query( "select users.uid from users inner join contacts on users.email=contacts.eid where contacts.uid='" . $API->uid . "' and contacts.site > 2" );
while( $r = mysql_fetch_array( $q ) )
  if( isset( $r['uid'] ) && !in_array( $r['uid'], $friends ) )
    $uids[] = $r['uid'];

//Look for people with the same email domain
//if( sizeof( $uids ) < 10 )
{


  $email = quickQuery( "select email from users where uid='" . $API->uid . "'" );

  if( $email != "" )
  {
    $data = explode( "@", $email );



    if( sizeof( $data ) > 1 )
    {
      $domain = strtolower( trim( $data[1] ) );

      $restricted_domains = array( "gmail.com", "yahoo.com", "aol.com", "hotmail.com", "msn.com" );

      if( !in_array( $domain, $restricted_domains ) )
      {
        $q = sql_query( "select users.uid from users where users.uid!='" . $API->uid . "' and users.email like '%$domain%'" );
        while( $r = mysql_fetch_array( $q ) )
          if( isset( $r['uid'] ) && !in_array( $r['uid'], $friends ) )
            $uids[] = $r['uid'];
      }
    }
  }
}


$friends = $API->getFriendsUids();

if( $limit == 1 )
{
  $blocked_uids = array_merge( $_SESSION['s_and_r_uids'], $blocked_uids );
}
else
{
  $_SESSION['s_and_r_uids'] = array();
}
//    if (isset($_GET['debug'])){
//        $res = mysql_query("select uid from users where uid between 2000 and 3000");
//        while ($row = mysql_fetch_assoc($res))
//            $uids[] = $row['uid'];
//
//    }
$uids = array_diff( $uids, $blocked_uids );
$uids = array_diff( $uids, $friends );

$uids = array_unique( $uids );


$count = 0;
if (isset($_GET['count']))
    $count = $_GET['count']*9;

$uids = array_slice($uids, $count);

$cond = getConditions( $uids );

    if (isset($_GET['debug']))
        $sql = "select uid,username,pic,name from users where uid != " . $API->uid . " and uid < 1000 and $cond limit 9";
    else
        $sql = "select uid,username,pic,name from users where active=1 and dummy=0 and uid != " . $API->uid . " and $cond limit 9"; //order by rand() limit $limit";

$x = sql_query($sql);

$c = 0;

$suggested_uids = array();
$num_results = mysql_num_rows( $x );

while ($user = mysql_fetch_array($x, MYSQL_ASSOC))
{
  $c++;
	$profileURL = $API->getProfileURL($user['uid'], $user['username']);

	$suggested_uids[] = $user['uid'];
	
  $div_name = "s_and_r-$c";
  if( $limit == 1 )
  {
    $div_name = $_GET['div'];
  }
  else
  {
    //We'll only show the div if we don't already have one.
    // this will be the case whenever limit > 1
	?>
	<div class="pymk" style="padding-top: 15px; height: 50px" id="<?=$div_name?>">
  <? } ?>
		<a href="<?=$profileURL?>" onmouseout="javascript:tipMouseOut();" onmouseover="javascript:showTip2(this,<?=$user['uid']?>,'U');">
			<img src="<?=$API->getThumbURL(1, 48, 48, $API->getUserPic($user['uid'], $user['pic']))?>" alt=""/>
		</a>
		<div class="nowrap">
			<a href="<?=$profileURL?>" class="userlink"><?=$user['name']?></a><br />
			<?=friendLink($user['uid'], null, null, true)?><br />
			<a href="javascript:void(0);" onclick="javascript:showSendMessage(<?=$user['uid']?>, '<?=addslashes($user['name'])?>', '<?=$API->getThumbURL(0, 85, 128, $API->getUserPic($user['uid'], $user['pic']))?>');">Send message</a><br />
			<span id="hide<?=$user['uid'];?>_s"><a href="javascript:void(0);" onclick="javascript:hideUser(<?=$user['uid']?>, '<?=$div_name?>', 's_and_r');">Hide</a></span>
		</div>
  <? if( $limit > 1 ) { ?>
	</div>
	<?
  }

  $_SESSION['s_and_r_uids'][] = $user['uid'];
}

$html = ob_get_contents();
ob_end_clean();

if (!$suggestions_quiet) //see weekly_profile_summary.php
{
	echo $html;
    $_GET['count'] = $c;
	return $num_results;
}
else{
	return $suggested_uids;
}

}

function getConditions( $uids )
{
  $i = 0;

  $cond = "(";
  foreach( $uids as $key => $value )
    if( $value != "" )
    {
      if( $i > 0 )
        $cond .= " OR ";
      $cond .= "uid=" . $value;
      $i++;
    }
  $cond .= ")";
  return $cond;
}

?>