<?php
/*
Generates the "Connections" popup, displayed throughout the site. The popup displays a list of connections that the
user has, as well as connection requests. Called from "common.js".
*/

include "inc/inc.php";

ob_start();

$blocked_uids = array();
$q = mysql_query( "select blocked_uid from blocked_users where uid='" . $API->uid . "'" );
while( $r = mysql_fetch_array( $q ) )
  $blocked_uids[] = $r['blocked_uid'];

$uid = intval($_GET['uid']);
if ($uid == 0) $uid = $API->uid;

$user = $API->getUserInfo($uid, "uid,pic,name");

if (empty($user['uid'])) die();

$numRequests = 0;
$numSuggests = 0;
$numFriends = 0;

$x = mysql_query("select if(id2=$uid and status=0,1,0) as canAccept,friends.status,uid,username,name,if(pic=0,pic0,if(pic=1,pic1,pic)) as pic from friends inner join users on if(id1=$uid,id2,id1)=users.uid where (id1=$uid or id2=$uid) and active=1 " . ($uid == $API->uid ? '' : 'and status=1') . " order by canAccept desc,status,name");

$friends = array();
while ($y = mysql_fetch_array($x, MYSQL_ASSOC))
	$friends[] = $y;
	
if ($API->uid == $uid)
{
	echo '<div id="friendspopup-r">';
	if ($friends[0]['status'] == "0")
	{
    $header = false;
		while ($friends[0]['status'] == "0" && $friends[0]['canAccept'] == 1 )
		{
      if( !$header ) echo '<div class="subhead" style="margin-bottom: 5px;">Connection Requests To You</div>';
      $header = true;
			showFriend(array_shift($friends));
			$numRequests++;
		}

    $header = false;
		while ($friends[0]['status'] == "0" && $friends[0]['canAccept'] == 0)
		{
      if( !$header ) echo '<div class="subhead" style="margin-bottom: 5px;">Connection Requests From You</div>';
      $header = true;
			showFriend(array_shift($friends));
			$numRequests++;
		}


		echo '<div style="clear: both; height: 10px;"></div>';
	}
	echo '</div>';

	echo '<div id="friendspopup-s">';

  $suggests = array();
  $suggest_uids = array();
  $friends2 = array();

	$x = mysql_query("select subj,body,u.uid,u.username,u.name,u.pic,v.uid as suid,v.username as susername,v.name as sname,-1 as status from friend_suggestions f inner join users u on u.uid=if(id1=$uid,id2,id1) inner join users v on v.uid=f.uid where (id1=$uid or id2=$uid) order by u.name");
  while ($y = mysql_fetch_array($x, MYSQL_ASSOC))
  {
    if( !in_array($y['uid'], $blocked_uids ) && !in_array($y['uid'], $suggest_uids ) )
    {
      $suggest_uids[] = $y['uid'];
    	$suggests[] = $y;
    }
  }

  $friends2 = $API->getFriendsUids();

  foreach( $friends2 as $friend )
  {
    $friends_of_friend = mysql_query( "select if(id1=" . $friend . ",id2,id1) as uid,u.username,u.name,u.pic, 0 as suid from friends inner join users u on u.uid=if(id1=" . $friend . ",id2,id1) where (" . $friend . ") in (id1,id2) and status=1 order by u.name" );

    while ($result = mysql_fetch_array($friends_of_friend, MYSQL_ASSOC))
    {
      if( $result['uid'] != $API->uid && !in_array($result['uid'], $friends2 ) && !in_array($result['uid'], $suggest_uids ) && !in_array($result['uid'], $blocked_uids ) )
      {
        $suggests[] = $result;
        $suggest_uids[] = $result['uid'];
      }
    }
  }

	if( sizeof($suggests) > 0 )
	{
  	echo '<div class="subhead" style="margin-bottom: 5px;">Connection Suggestions</div>';
		for( $c = 0; $c < sizeof( $suggests ); $c++ )
		{
			showFriend( $suggests[$c] );
			$numSuggests++;
		}
		echo '<div style="clear: both; height: 10px;"></div>';
	}
	echo '</div>';
}

echo '<div id="friendspopup-f">';
if ($friends[0]['status'] == "1" || $API->uid != $uid)
{
	echo '<div class="subhead" style="margin-bottom: 5px;">Connections</div>';
	while (count($friends) > 0)
	{
		showFriend(array_shift($friends), $uid == $API->uid);
		$numFriends++;
	}
}
echo '</div>';

$html = ob_get_contents();
ob_end_clean();

?>

<div style="float: left; width: 58px;">
	<a href="<?=$API->getProfileURL($user['uid'], $user['username'])?>"><img width="48" height="48" src="<?=$API->getThumbURL(1, 48, 48, $API->getUserPic($user['uid'], $user['pic']))?>" alt="" /></a>
</div>

<div style="float: right; width: 401px; font-size: 9pt; padding-top: 3px;">
	<span style="font-weight: bold;"><?=$uid == $API->uid ? "Your <a>Connections, Connection Requests, and Connection Suggestions</a>" : $user['name'] . "'s Connections"?></span>
	<div style="border-top: 2px solid #d8dfea; padding-top: 5px; margin-top: 5px; font-size: 8pt;">
		<span style="font-weight: bold; font-size: 9pt;">View:&nbsp; </span><a href="javascript:void(0);" onclick="javascript:showFriendsPopup(null, 'f');">connections (<?=$numFriends?>)</a><?php if ($API->uid == $uid) { ?> &nbsp;|&nbsp; <a href="javascript:void(0);" onclick="javascript:showFriendsPopup(null, 'r');">connection requests (<?=$numRequests?>)</a> &nbsp;|&nbsp; <a href="javascript:void(0);" onclick="javascript:showFriendsPopup(null, 's');">connection suggestions (<?=$numSuggests?>)</a><?php } ?>
	</div>
</div>

<div style="clear: both; height: 5px;"></div>

<?php

echo '<div style="height: 375px; overflow-y: scroll; padding-right: 5px;">' . $html . '</div>';

function showFriend($friend, $hideFriendLink = false)
{
	global $API, $uid, $siteName;

	$isSuggestion = $friend['suid'] != null;

	$profileURL = $API->getProfileURL($friend['uid'], $friend['username']);
	?>
	<div id="friendpopup-fr<?=$friend['uid']?>">
		<div style="float: left; width: 58px;">
			<a href="<?=$profileURL?>"><img width="48" height="48" src="<?=$API->getThumbURL(1, 48, 48, $API->getUserPic($friend['uid'], $friend['pic']))?>" alt="" /></a>
		</div>
		<div style="float: left; width: 375px; font-size: 8pt; float: left; border-bottom: 1px solid #c0c0c0; padding: 0 0 2px 2px;">
			<div style="float: left; width: 205px;">
				<a href="<?=$profileURL?>"><?=$friend['name']?></a><? if( $friend['uid'] != $API->uid ) echo "&ndash;" . plural(count($API->getMutualFriends($friend['uid'])), "mutual connection");?>
			</div>
			<div style="float: right; width: 168px; text-align: right; padding-right: 2px;">
				<?php if ($friend['uid'] != $API->uid) { ?>
				<a href="javascript:void(0);" onclick="javascript:showSendMessage(<?=$friend['uid']?>, '<?=$friend['name']?>', '<?=$API->getThumbURL(0, 85, 128, $API->getUserPic($friend['uid'], $friend['pic']))?>');">send message</a>
        <? $divId = "fr-" . $friend['uid'] . "-" . substr(md5(microtime()), 0, 10);

        $connectionRequestSent = quickQuery( "select count(*) from friends where (id1=" . $API->uid . " and id2=" . $friend['uid'] . ") and status=0" );
        if( $connectionRequestSent > 0 )
        {
          $connect = "send reminder";
          $pending = '<span id="' . $divId . '"><a href="javascript:void(0);" onclick="javascript:addFriend(' . $friend['uid'] . ', \'' . $divId . '\', \'request sent\');">send reminder</a></span>';
        }
        else
        {
          $connect = "connect";
          $pending = "pending";
        }
        ?>
				<?=$hideFriendLink ? "" : "| " . friendLink($friend['uid'], null, array( $connect, array( $pending, "accept"), "connected"))?>
				<?php } ?>
				<?php if ($uid == $API->uid) { ?>&nbsp; <a href="javascript:void(0);" onclick="javascript:if (confirm('Are you sure?')) deleteFriend(<?=$friend['uid']?>, 'friendpopup-fr<?=$friend['uid']?>');">X</a><?php } ?>
			</div>
		</div>
		<div style="float: left; width: 375px; padding: 2px 0 0 2px; font-size: 8pt;">
      <?
      $occupation = quickQuery( "select occupation from users where uid='" . $friend['uid'] . "'" );
      if( $occupation != "" )
      {
        echo $occupation;
        $co = quickQuery( "select company from users where uid='" . $friend['uid'] . "'" );
        if( $co != "" ) echo ", " . $co;
      } else { echo "Member of " . $siteName; }
      ?>
<!--
			<span style="font-size: 9pt; font-weight: bold;"><?=$friend['subj']?></span><br /><?=$friend['body']?>
			<?=$isSuggestion ? '<br />Suggested by:&nbsp; <a href="' . $API->getProfileURL($friend['suid'], $friend['susername']) . '">' . $friend['sname'] . '</a>' : ""?>
-->
		</div>
		<div style="clear: both; height: 5px;"></div>
	</div>
	<?php
}

?>