<?php
include "inc/inc.php";

$links = array();
$links[0] = array();
$links[1] = array();

$gid = $_GET['gid'];

$friends = $API->getFriendsUids( $API->uid );

$fof = $API->getFriendsOfFriends(false); //$fof includes friends

if( sizeof( $fof ) > 0 )
{
  $conn_q = sql_query( "SELECT page_members.uid FROM page_members WHERE page_members.gid='" . $gid . "' and page_members.uid IN (" . implode( ",", $fof ) . ")" );
  if( mysql_num_rows( $conn_q ) > 0 )
  {
    while( $conn_r = mysql_fetch_array( $conn_q ) )
    {
      if( in_array( $conn_r['uid'], $friends ) )
        $links[0][] = $conn_r['uid'];
      else if( in_array( $conn_r['uid'], $fof ) )
        $links[1][] = $conn_r['uid'];
    }
  }
}

$titles = array( 'Direct connections', 'Through your Connections' );

if (!$API->isLoggedIn()){
    echo '<div style="font-weight: bold">Login to see which of your connections use this business and/or service.</div>';
}
else{
?>

<div style="float: left; width: 58px;">
	<a href="<?=$API->getProfileURL($user['uid'], $user['username'])?>"><img width="48px" height="48px" src="<?=$API->getThumbURL(1, 48, 48, $API->getUserPic($user['uid']), $user['pic'])?>" alt="" /></a>
</div>

<div style="float: right; width: 401px; font-size: 9pt; padding-top: 3px;">
	<span style="font-weight: bold;">How you're Connected to this Page</span>
	<div style="border-top: 2px solid #d8dfea; padding-top: 5px; margin-top: 5px; font-size: 8pt;">
		<span style="font-weight: bold; font-size: 9pt;">View:&nbsp; </span><a href="javascript:void(0);" onclick="javascript: document.getElementById('links0').style.display=''; document.getElementById('links1').style.display='none';">direct connections (<?=sizeof($links[0]);?>)</a> &nbsp;|&nbsp; <a href="javascript:void(0);" onclick="javascript:document.getElementById('links1').style.display=''; document.getElementById('links0').style.display='none';">through connections (<?=sizeof($links[1]);?>)</a>
	</div>
</div>

<div style="clear: both; height: 5px;"></div>

<div style="height: 375px; overflow-y: scroll; padding-right: 5px;">
<?
for( $c = 0; $c <= 1; $c++ )
{
?>
  <div id="links<?=$c?>">
    <div class="subhead" style="margin-bottom: 5px;"><?=$titles[$c];?></div>
    <?for( $d = 0; $d < sizeof( $links[$c] ); $d++ ) showFriend( $links[$c][$d], ($c==0) ); ?>
    <div style="clear: both; height: 5px;"></div>
  </div>
<?
}
}
?>
</div>

<?php
function showFriend($uid, $hideFriendLink = false)
{
	global $API;

  $friend = queryArray( "select uid, name, pic, username from users where uid='" . $uid . "'" );

	$profileURL = $API->getProfileURL($friend['uid'], $friend['username']);
	?>
	<div id="friendpopup-fr<?=$friend['uid']?>">
		<div style="float: left; width: 58px;">
			<a href="<?=$profileURL?>"><img width="48px" height="48px" src="<?=$API->getThumbURL(1, 48, 48, $API->getUserPic($friend['uid'], $friend['pic']))?>" alt="" /></a>
		</div>
		<div style="float: left; width: 375px; font-size: 8pt; float: left; border-bottom: 1px solid #c0c0c0; padding: 0 0 2px 2px;">
			<div style="float: left; width: 205px;">
				<a href="<?=$profileURL?>"><?=$friend['name']?></a>

        <?
        if( !$hideFriendLink )
        {
          $mf = $API->getMutualFriends($friend['uid']);
          if( sizeof($mf) > 3 )
            echo ' &ndash; ' . plural(count($mf), "mutual connection");
          else
          {
            echo ' &ndash; via ';
            for( $c = 0; $c < sizeof( $mf ); $c++ )
            {

            	$profileURL = $API->getProfileURL($mf[$c]['uid'], $mf[$c]['username']);
              if( $c > 0 ) echo ", ";
              ?><a href="<?=$profileURL?>"><?=$mf[$c]['name']?></a><?
            }
          }
        }
        ?>

			</div>
			<div style="float: right; width: 168px; text-align: right; padding-right: 2px;">
				<?php if ($friend['uid'] != $API->uid) { ?>
				<a href="javascript:void(0);" onclick="javascript:showSendMessage(<?=$friend['uid']?>, '<?=$friend['name']?>', '<?=$API->getThumbURL(0, 85, 128, $API->getUserPic($friend['uid'], $friend['pic']))?>');">send message</a>
        <? $divId = "fr-" . $friend['uid'] . "-" . substr(md5(microtime()), 0, 10);
        $connectionRequestSent = 0;
        if (isset($API->uid)){
            $connectionRequestSent = quickQuery( "select count(*) from friends where (id1=" . $API->uid . " and id2=" . $friend['uid'] . ") and status=0" );
        }
        if( $connectionRequestSent > 0 )
        {
          $connect = "send reminder";
          $pending = '<span id="' . $divId . '"><a href="javascript:void(0);" onclick="javascript:addFriend(' . $friend['uid'] . ', \'' . $divId . '\', \'request sent\');">send reminder</a></span>';
        }
        else
        {
          $connect = "connect";
          $pending = "pending";
        }
        ?>
				<?=$hideFriendLink ? "" : "| " . friendLink($friend['uid'], null, array( $connect, array( $pending, "accept"), "connected"))?>
				<?php } ?>
				<?php if ($uid == $API->uid) { ?>&nbsp; <a href="javascript:void(0);" onclick="javascript:if (confirm('Are you sure?')) deleteFriend(<?=$friend['uid']?>, 'friendpopup-fr<?=$friend['uid']?>');">X</a><?php } ?>
			</div>
		</div>
		<div style="float: left; width: 375px; padding: 2px 0 0 2px; font-size: 8pt;">
      <?
      $occupation = quickQuery( "select occupation from users where uid='" . $friend['uid'] . "'" );
      if( $occupation != "" )
      {
        echo $occupation;
        $co = quickQuery( "select company from users where uid='" . $friend['uid'] . "'" );
        if( $co != "" ) echo ", " . $co;
      } else { echo "Member of " . $siteName; }
      ?>
			<!--<span style="font-size: 9pt; font-weight: bold;"><?=$friend['subj']?></span><br /><?=$friend['body']?>-->
		</div>
		<div style="clear: both; height: 5px;"></div>
	</div>
	<?php
}
?>