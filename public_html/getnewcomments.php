<?php

// comes before the includes so htmlentities is not executed on GET
$com = json_decode($_GET['q'], true);

include "inc/inc.php";
include "inc/mod_comments.php";

header('Content-type: application/json');

$comments = array();
$wheres = array();
$datas = array();

foreach ($com as $c)
{
	$data = array();
	
	$data['type'] = substr($c['tl'], 1, 1);
	$data['id'] = intval($c['firstComment']);
	//$data['id'] = 0;//for testing
	$data['link'] = intval(substr($c['tl'], 2));
	$data['layout'] = intval($c['layout']);
	
	$datas[] = $data;
	
	$where = "type='{$data['type']}' and link={$data['link']}";
	
	if ($data['id'] > 0)
		$where .= " and id > {$data['id']}";
	
	$wheres[] = "($where)";
}

$subquery = implode(' or ', $wheres);
$q = "select comments.*,twusername,fbid,name,pic,username from comments join users on comments.uid=users.uid where " . $subquery . " order by type,link,comments.id";

$x = mysql_query($q);

while ($y = mysql_fetch_array($x, MYSQL_ASSOC))
	$comments[] = $y;
	
$ret = array();

//print_r($com);die('ho');

//for ($i = count($comments) - 1; $i >= 0; $i--)
foreach ($comments as $c)
{
	$layout = determineLayout($c['type'], $c['link']);
	
	ob_start();
	showComment($c, rand(2000, 3000), false, $layout, true );
	$html = ob_get_contents();
	ob_end_clean();
	
	$ret[$c['type'] . $c['link']][] = array('type' => $c['type'], 'link' => $c['link'], 'layout' => $layout, 'id' => $comments[0]['id'], 'html' => $html);
}

/*
if( sizeof( $comments ) > 0 )
{
  echo json_encode( array("html" => $html, "id" => $comments[0]['id'] ));
}
else
  echo json_encode( array("html" => "") );*/

mysql_close();

echo json_encode($ret);

function determineLayout($type, $link)
{
	global $datas;
	
	$layout = 0;
	
	foreach ($datas as $c)
		if ($c['type'] == $type && $c['link'] == $link)
		{
			$layout = intval($c['layout']);
			break;
		}
	
	return $layout;
}

?>