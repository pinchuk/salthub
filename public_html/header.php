<?php
/*
Header file that is included by most pages, this file also includes header_s.php, which is the header bar
shown at the top of the page.

The file renders the meta data, java script includes.
*/

include_once "inc/inc.php";

if ($site == "s" && !$noLogin)
	$API->requireLogin();

unset($_SESSION['addacct']); //this is set to "true" on the settings page - for when a user wants to add a tw/fb acct to their mb acct

//if ($API->isLoggedIn())
//	mysql_query("update users set lastaction=now() where uid=" . $API->uid);

//$isIE = strpos($_SERVER['HTTP_USER_AGENT'], "MSIE");


if( $site == "s" && stristr( $script, "/" ) !== false && empty( $title ) && $descr == "" )
{
  $dir = current( explode( "/", $script ) );
  switch( $dir )
  {
    case "employment":
      $title = "Employment | SaltHub - Find Maritime Jobs - Post Maritime Jobs - Crew - Marine Professionals";
      $descr = "Find and list shore based and seagoing positions associated with maritime and water related sectors.";
    break;

    case "pages":
      $title = "Pages | $siteName - Be heard - Get discovered - Connect";
    break;
  }
}

if (empty($title))
{
  if ($site == "m")
  	$title = "$siteName - Upload to Facebook and Twitter";
  else
  	$title = $defaultPageTitle;
}

//Lookup to see if we have have new meta data set in from admin
    $meta_tags = queryArray( "select title, keywords, descr from meta_tags where script='$script'" );
if( $meta_tags != NULL )
{
  $title = $meta_tags['title'];
  if (isset($_GET['descr']))
    $descr = $_GET['descr'];
  else
    $descr = $meta_tags['descr'];
  $keywords = $meta_tags['keywords'];
}

if( $isDevServer )
  $title = "SH DEV [{$API->uid}] - " . $title;

?><!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<!--<meta http-equiv="X-UA-Compatible" content="IE=8" />-->
	<meta content="IE=edge" http-equiv="X-UA-Compatible">

    <? if (stripos($_SERVER['HTTP_HOST'], 'dev')) {?>
        <meta name="googlebot" content="noindex" />
        <meta name="robots" content="nofollow" />
    <? } ?>

<?


if( isset( $meta ) )
{
  for( $c = 0; $c < sizeof( $meta ); $c++ )
  {
    if( isset( $meta[$c]['property'] ) && isset( $meta[$c]['content'] ) )
    {
      $property = $meta[$c]['property'];
      $content = $meta[$c]['content'];
      echo "<meta property=\"$property\" content=\"$content\"/>
";
    }
  }
}
?>
	<?php if ($content_wide) { ?><link rel="stylesheet" href="/reset.css" type="text/css" /><?php } ?>
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" type="text/css" />
	<link rel="stylesheet" href="/style_<?=$site?>.css?ver=<?=filemtime($_SERVER['DOCUMENT_ROOT'] . "/style_$site.css")?>" type="text/css" />
	<link rel="stylesheet" href="/style.css?ver=<?=filemtime($_SERVER['DOCUMENT_ROOT'] . "/style.css")?>" type="text/css" />
    <script type="text/javascript" src="https://connect.facebook.net/en_US/all.js"></script>

<?
$css[] = "/jQuery.popeye/css/popeye/jquery.popeye.css";
$css[] = "/jQuery.popeye/css/popeye/jquery.popeye.style.css";

	if (is_array($css))
		foreach ($css as $url)
			echo "\t<link rel=\"stylesheet\" href=\"$url\" type=\"text/css\" />\n";
	?>
	<link rel="shortcut icon" href="/favicon_<?=$site?>.ico" />
    <? if (isset($_GET['title'])){ ?>
	    <meta name="title" content="<?=$_GET['title']?>" />
    <? }else {?>
        <meta name="title" content="<?=$title?>" />

<? }if (!isset($descr ) && $site == "s") $descr = $defaultMetaDescription; ?>
    <? if (isset($_GET['descr'])){ ?>
  <meta name="description" content="<?=str_replace("\n", " ", $API->convertChars($_GET['descr']))?>" />
    <? } else{ ?>
        <meta name="description" content="<?=str_replace("\n", " ", $API->convertChars($descr))?>" />

<?} if( isset( $keywords ) ) { ?>
  <meta name="keywords" content="<?=$keywords?>" />
<? } ?>

<?php if ($thumb) { ?>	<link rel="image_src" href="<?=$thumb?>" / >
<?php }

$scripts[] = "/jQuery.popeye/lib/popeye/jquery.popeye-2.1.js";
$scripts[] = "/media.js";
$scripts[] = "/tipmain.js";
$scripts[] = "/simpleajax.js";
$scripts[] = "/common.js";
$scripts[] = "/jquery/jquery.tabSlideOut.v1.3.js";
$scripts[] = "/jquery/jquery.bpopup-0.8.0.min.js";
$scripts[] = "/jquery/jquery.autosize-min.js";
$scripts[] = "/jquery/jquery.ui.widget.js";
$scripts[] = "/jquery/jquery.iframe-transport.js";
$scripts[] = "/jquery/jquery.fileupload.js";
$scripts[] = "/dropdown.js";
$scripts[] = "/report.js.php";
$scripts[] = "/tinybox/tinybox.js";
$scripts[] = "/upload/swfupload.js";
$scripts[] = "/upload/upload.js.php";
$scripts[] = "/upload/upload_photo.js";
$scripts[] = "/inc/tweetbox.js.php";
$scripts[] = "/crossbrowser.js";
$scripts[] = "/tween.js";
//$scripts[] = "/json.js";
$scripts[] = "https://static.ak.connect.facebook.com/js/api_lib/v0.4/FeatureLoader.js.php/en_US";
$scripts[] = "https://static.ak.fbcdn.net/connect.php/js/FB.Share";
$scripts[] = "https://connect.facebook.net/en_US/all.js#appId=" . $fbAppId . "&amp;xfbml=1";
$scripts[] = "https://platform.twitter.com/widgets.js";

$scripts[] = "https://apis.google.com/js/client.js?onload=OnLoadCallback";
$scripts[] = "/importgmail/chrome_ex_oauth.js";
$scripts[] = "/importgmail/onload.js";
$scripts[] = "/importgmail/chrome_ex_oauthsimple.j";

// jquery-ui
$scripts[] = "http://code.jquery.com/ui/1.10.3/jquery-ui.js";
$scripts[] = "/flowplayer/flowplayer-min.js";

if ($site == "s")
	$scripts[] = "/tags.js";

array_unshift( $scripts, "/jquery/jquery.min.js" );

   
loadJS($scripts);
unset($scripts);


?>
	<title><?=$title?></title>

	<script language="javascript" type="text/javascript">
		var isLoggedIn = <?=$API->isLoggedIn() ? $API->uid : "false"?>;
		var admin = <?=intval($API->admin)?>;
		var lastTime = <?=time()?>;
		var addthis_share = { templates: { twitter: '{{title}}: {{description}} via @<?=SITE_TWITTER_SN?>' } }
		var hostName = "<?=SERVER_HOST?>";
		var script = "<?=$script?>";
		var session_id = "<?=session_id()?>";
		var hash = "<?=$hash?>";
		var siteName = "<?=$siteName?>";
		var site = "<?=$site?>";
		<?php
		if ($isDevServer) echo 'var isDevServer = true;';
		if (is_array($jsVar))
			foreach ($jsVar as $k => $v)
				echo "\t\t$k = \"" . addslashes($v) . "\"";
		?>
<?php
if (isset($_SESSION['error']))
{
	echo 'showPopUp2("", "' . addslashes($_SESSION['error']) . '", 0);';
	unset($_SESSION['error']);
}
 ?>
	</script>
<? include_once( "inc/tracking_code.php" ); ?>
</head>
<!-- Wrapping .subhead content in span to decorate it - Added by Valery -->
<script>
jQuery(document).ready(function(){
    var create_ad = jQuery('div.create_ad');
    var view_all = jQuery('div.view_all');
    var view_all_2 = jQuery('div.view_all_2');
    var view_all_pages = jQuery('div.view_all_pages');
    create_ad.addClass('link');
    view_all.addClass('link');
    view_all_2.addClass('link');
    view_all_pages.addClass('link');
    create_ad.remove();
    view_all.remove();
    view_all_2.remove();
    view_all_pages.remove();
    jQuery('div.subhead').wrapInner('<span></span>');
    jQuery('div.parent_create_ad').append(create_ad[0]);    
    jQuery('div.parent_view_all').append(view_all[0]); 
    jQuery('div.parent_view_all_2').append(view_all_2[0]);
    jQuery('div.parent_view_all_pages').append(view_all_pages[0]);
});
</script>
<!-- END -->
<body onload="javascript:if (typeof initPage == 'function') initPage();">

<?php if ($isDevServer) { ?>
<div style="font-size: 24pt; color: red; position: absolute; top: 100px; left: 10px; font-weight: bold; z-index: 999;">
	DEV <?=$API->uid?>
</div>
<?php } ?>

<?php

//include "inc/loginoverlay.php";

include "header_$site.php";

if( $API->admin )
  $API->startTimer();
?>

<a name="top"></a>

<?php
$is_staging_server = $_SERVER['SERVER_ADDR'] == '23.253.66.22';

if ($is_staging_server)
	echo '<div style="top: 20px; left: 20px; padding: 10px; background: white; border: 5px solid red; font-weight: bold; font-size: 32px; color: red; position: absolute;">STAGING</div>';
?>

<div class="content<?=$content_wide ? ' content_wide' : ''?>"<?=$background ? " style=\"background: $background;\"" : ""?>>
	<div id="tiplayer" style="visibility: hidden; position: absolute; z-index: 1000;"></div>
