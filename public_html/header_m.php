<div class="clouds">
	 <div class="sitetitle">
		<div style="float: left;">
			<a href="/">mediaBirdy<span style="color: #ff8040;">.</span>com</a>
			<div class="sitesubtitle">
				Upload to Facebook and Twitter
			</div>
		</div>
		<?php if ($API->isLoggedIn()) { ?>
		<div style="float: right; padding-top: 28px; padding-right: 10px;">
			<div style="float: left;">
				<a href="<?=$API->getProfileURL()?>"><img src="<?=$API->getThumbURL(1, 48, 48, $API->getUserPic())?>" alt="" class="curuserpic" id="curuserpic" /></a>
			</div>
			<div class="curuserbox">
				<div class="curuseropts">
					<div class="smtitle">
						<a href="<?=$API->getProfileURL()?>"><?=$API->name?></a>
					</div>
					<div style="float: left;"><a href="/settings">settings</a></div>
					<div style="float: left; width: 107px; text-align: center;"><a href="javascript:void(0);" onclick="javascript:<?=$script == "index" ? "getStarted();" : "showTweetBox();"?>">upload</a></div>
					<div style="float: right;"><a href="/logout.php">logout</a></div>
					<div style="clear: both;"></div>
				</div>
			</div>
			<div style="clear: both;"></div>
		</div>
		<?php } ?>
		<div style="clear: both;"></div>
	 </div>
</div>

<div class="nav">
	<div class="navlinks">
		<a href="/">Home</a>
		<span class="separator">|</span>
		<a href="/invite.php">Invite Friends</a>
		<span class="separator">|</span>
		<a href="/search.php">Videos</a>
		<span class="separator">|</span>
		<a href="/search.php?t=P">Photos</a>
		<?php if ($API->isLoggedIn()) { ?>
		<?php } else { ?>
		<div style="float: right; padding-right: 3px;">
			<a href="#top" onclick="javascript:showLogin();">Login</a>
		</div>
		<?php } ?>
		<?php include "header_search.php"; ?>
	</div>
</div>