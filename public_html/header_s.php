<?php
$uri_segments = explode("/", $_SERVER['REQUEST_URI']);
$header_height = "";
if ($API->isLoggedIn())
{
	$unreadMsgs = $API->getUnreadMessageCount();

	$navLinks = array(
		"Home" => "/welcome",
		"Profile" => $API->getProfileURL(),
		"Connect" => array(
			array("Contact Manager", "/contacts.php", "vcard"),
			array("Connections", "javascript:showFriendsPopup(" . $API->uid . ");", "user"),
			array("Find Friends", "/findpeople.php", "find"),
			array("Invite Friends", "/invite.php", "email_notice")
				),
		"Inbox" . ($unreadMsgs > 0 ? " ($unreadMsgs)" : "") => array(
			array("Inbox" . ($unreadMsgs > 0 ? " ($unreadMsgs)" : ""), "/messaging/inbox.php", "email"),
			array("Connection Requests", "/messaging/inbox.php", "page_add"),
			array("Compose Message", "/messaging/compose.php", "email_add")
				),
		"Explore" => array(
			array("Popular Media", "/mediahome.php", "camera"),
			array("Videos", "/search_m.php?t=V", "television"),
			array("Photos", "/search_m.php?t=P", "images"),
			array("Pages", "/pages", "page"),
			array("Webs", "#", "world")
				),
		"Settings" => array(
			array("Account Settings", "/settings", "application_edit"),
			array("Manage My Photos", "/settings/photos.php", "images"),
			array("Manage My Videos", "/settings/videos.php", "television"),
			array("Billing", "/billing", "creditcards"),
			array("Advertising", "/adv", "sound_mega_phone"),
			array("Create a Page", "/pages/page_create.php", "page_add"),
			array("Post a job", "/employment/emp_create.php", "application_form_add")
				),
		"Logout" => "/logout.php"
		);
    $header_height = 'style="height: 113px;"';
}
else
{
	$navLinks = array(
		"Home" => "/",
		"Connect" => array(
			array("Contact Manager", "/", "vcard"),
			array("Connections", "/", "user"),
			array("Find Connections", "/", "find"),
			array("Invite Connections", "/", "email_add")
				),
		"Explore" => array(
			array("Popular Media", "/", "camera"),
			array("Videos", "/", "television"),
			array("Photos", "/", "images"),
			array("Pages", "/", "page"),
			array("Webs", "/", "world")
				),
		"Sign Up" => "/",
		"Log In" => "/"
		);
}

?>

<!--<div class="waterheader"></div>-->

<div class="nav2" <?=$header_height?>>
	<div class="nav2content">
		<div class="title" style="height: 60px;"><a href="/"><img src="/images/wavelogo.png" alt="" /></a></div>
<? if ($API->isLoggedIn() ) {?>
		<div class="search" id="header_search">
			<div style="float: left;">
        <div style="position: relative;">
  				<input class="txt" data-autocomplete="get_search" data-ac_min="4" data-ac_limit="50" type="text" id="txtsearch" data-default="Search for people, vessels, companies, and more ..." class="txtsearch">
                <div class="bino"><a href="javascript:void(0);" onclick="javascript:searchDo();"><img src="/images/find.png" alt="" /></a></div>
          <div id="suggestionBox" style="padding:0px; position: absolute; left:5px; top:27px; width:288px; height:465px; z-index:101; display:none;">
          </div>
        </div>

			</div>

		<?php
//		showNavDrop('<span class="moresearch">explore</span><img src="/images/arrow_down.png" alt="" />', array(
//			array("Business Directory", "/directory.php", "vcard"),
//			array("Vessel Directory", "/vessels", "anchor"),
//			array("Vessel Heat Map", "/vessels/vessel_live_tracking.php", "flames"),
//			array("Employment", "/employment", "person_tie"),
//			array("Pages", "/pages", "page"),
//			array("Popular Media", "/mediahome.php", "camera"),
//			array("Videos", "/search_m.php?t=V", "television"),
//			array("Photos", "/search_m.php?t=P", "images")
//				), "top: 22px; left: 0px;");
		?>

			<div style="clear: both;"></div>
		</div>

    <div style="width:75px; float:left;">&nbsp;</div>

    <div style="display: none;" class="links"<?=$hideHeaderLinks ? ' style="visibility: hidden;"' : ""?>>
	    <div style="float:left; padding-left:10px; padding-right:10px; padding-top:1px;"><a href="/">Home</a></div>
	    <div style="float:left; padding-top:1px; padding-left:10px; padding-right:15px;">
			      <?php
			      showNavDrop("Connect", array(
				      array("Contact Manager", "/contacts.php", "vcard"),
				      array("Connections", "javascript:showFriendsPopup(" . $API->uid . ");", "user"),
				      array("Find Connections", "/findpeople.php", "find"),
				      array("Invite Connections", "/invite.php", "email_add")
					      ), "top: 20px; left: -20px;");
			      ?>
	    </div>
    </div>
    
    <div style="width:7px; float:left;">&nbsp;</div>
    <div style="float: right; margin-right: 10px;">
        <div class="userpic" style="padding-top: 2px">
<!--          --><?// echo '<a href="' . $API->getProfileURL() . '" title="' . $API->name . '\'s Log Book"><img src="' . $API->getThumbURL(1, 24, 24, $API->getUserPic()) . '" alt="" /></a>';?>
            <div class="image" style="width: 30px; margin-left: 5px; line-height: 44px;">
                <div onmouseover="javascript:showDropDown('user_popup');" onmouseout="javascript:hideDropDown('user_popup');">
                    <img src="<?=$API->getThumbURL(1, 24, 24, $API->getUserPic())?>" alt="" />
                    <div id="user_popup" class="navdrop" style="top: 35px; left: -7px; display: none; width: 100px">
                        <div style="width: 100px" onmouseup="javascript:if (event.button == 0) actionClicked(this);" class="item">
                            <a href="<?=$API->getProfileURL()?>/about.php"><img style="border: none; width: 15px; height: 15px" src="/images/user.png" alt="">CV/Resume</a>
                        </div>
                        <div style="width: 100px" onmouseup="javascript:if (event.button == 0) actionClicked(this);" class="item">
                            <a href="<?=$API->getProfileURL()?>/logbook"><img style="border: none; width: 15px; height: 15px" src="/images/book_open.png" alt="">Logbook</a>
                        </div>
                        <div style="width: 100px" onmouseup="javascript:if (event.button == 0) actionClicked(this);" class="item">
                            <a href="<?=$API->getProfileURL()?>/photos"><img style="border: none; width: 15px; height: 15px" src="/images/images.png" alt="">Photos</a>
                        </div>
                        <div style="width: 100px" onmouseup="javascript:if (event.button == 0) actionClicked(this);" class="item">
                            <a href="<?=$API->getProfileURL()?>/videos"><img style="border: none; width: 15px; height: 15px" src="/images/television.png" alt="">Videos</a>
                        </div>
                    </div>
                </div>
                <div style=" display: none; padding-top:1px;">
                    <?
                    showNavDrop('Account <img src="/images/arrow_down_white.png"  width="11" height="9" alt="" />', array(
                        array("About", "/about.php", "application_edit"),
                        array("Logbook", "/logbook", "application_edit"),
                        array("Photos", "/photos", "images"),
                        array("Videos", "/videos", "television"),
                    ), "top: 20px; left: -85px;");
                    ?>
                </div>
            </div>
        </div>

        <div class="black" style="margin-right:10px;"></div>

              <div class="image" style="margin-right:5px;"><a href="javascript: void(0);" onclick="javascript:showTweetBox(); return false;" title="Upload"><img src="/images/upload.png" width="16" height="16" alt="" /></a></div>

              <div class="image"><a href="javascript: void(0);" onclick="javascript:loadShare('compose', 1); return false;" title="Compose Message"><img src="/images/compose.png" width="16" height="16" alt="" /></a></div>

                <div class="image" style="padding-left:5px;">
          <?
          if( $unreadMsgs > 0 )
            echo '<a href="/messaging/inbox.php" title="' . plural($unreadMsgs, "unread message") . '"><img src="/images/email_notice.png" alt="" /></a>';
          else
            echo '<a href="/messaging/inbox.php" title="Inbox"><img src="/images/email.png" alt="" /></a>';
          ?>
        </div>

                <div class="image">
                    <?php
                      $numNotifications = $API->getNotificationsCount();
          ?>
                    <a href="javascript:void(0);" onclick="javascript:showNotifications();" title="You have <?=plural($numNotifications, "notification")?>"><img src="/images/notice_<? if ($numNotifications > 0) echo "red"; else echo "grey"; ?>.png" width="16" height="16" alt="" /></a>
                    <div id="notifications_container">
                        <div id="headnotifications">
                            <div class="subhead">Notifications <span class="number">(<?=$numNotifications?>)</span></div>
                            <div style="padding: 20px; text-align: center;">
                                <img src="/images/wait.gif" alt="" />
                            </div>
                        </div>
                    </div>
                </div>

        <? $numNotifications += $unreadMsgs; ?>

        <div class="image"><a href="javascript:void(0);" title="Connections &amp; Management" onclick="javascript:showFriendsPopup(<? echo $API->uid; ?> );">
          <img src="/images/add_connection_<? echo ($API->getFriendRequestCount() > 0 ) ? "red" : "grey";?>.png" width="16" height="16" alt="" /></a>
        </div>
        <div class="image" style="width: 30px; margin-left: 5px; line-height: 44px;">
            <div onmouseover="javascript:showDropDown('dd-51ed98598213810597a105876e5876ce');" onmouseout="javascript:hideDropDown('dd-51ed98598213810597a105876e5876ce');">
                <img src="/images/settings_icon.png" width="20" height="20" alt="" />
                <div id="dd-51ed98598213810597a105876e5876ce" class="navdrop" style="top: 35px; left: -126px; display: none;">
                    <div onmouseup="javascript:if (event.button == 0) actionClicked(this);" class="item">
                        <a href="/settings"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAJRSURBVBgZpcHda81xHMDx9+d3fudYzuYw2RaZ5yTWolEiuZpCSjGJFEktUUr8A6ZxQZGHmDtqdrGUXHgoeZqSp1F2bLFWjtkOB8PZzvmd7+djv5XaBRfL6yVmxv+QjQeu7l25uuZYJmtxM0AVU8Wpw9RQU8w51AxzDqfKhFjwq6Mjdbj1RN0Zv2ZFzaloUdwrL2Is4r+y7hRwxs8G5mUzPxmrwcA8hvnmjIZtcxmr3Y09hHwzJZQvOAwwNZyCYqgaThVXMFzBCD7fJfv8MpHiKvaV3ePV2f07fMwIiSeIGeYJJoao4HmCiIeIQzPXifY+paJqO4lZi/nWPZ/krabjvlNHyANMBAQiBiqgakQMCunbxHJviM9bQeZdBzHJUzKhguLJlQnf1BghAmZ4gImAgAjk++8jP56QmL2GXG8zsfFCz8skA1mQXKbaU3X8ISIgQsgDcun7FL7cJjFnLUMfLyLRr0SLS4hbhiup5Szd19rpFYKAESKICCERoS95neyHmyTmbmAodQ4vGpAfmEn6YTtTahv4ODiRkGdOCUUAAUSE/uQNfqTaKFu4jvynJiIxIzcwg/SjF1RsOk9R+QJMlZCvqvwhQFdbM4XvrynIVHpfn2ZSWYyhzHS+PUtSueUC0cQ0QmpGyE9197TUnwzq1DnUKbXSxOb6S7xtPkjngzbGVVbzvS/FjaGt9DU8xlRRJdTCMDEzRjuyZ1FwaFe9j+d4eecaPd1dPxNTSlfWHm1v5y/EzBitblXp4JLZ5f6yBbOwaK5tsD+9c33jq/f8w2+mRSjOllPhkAAAAABJRU5ErkJggg==" alt="">Account Settings</a>
                    </div>
                    <div onmouseup="javascript:if (event.button == 0) actionClicked(this);" class="item">
                        <a href="/settings/photos.php"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAInSURBVDjLhZPda9NQHIbzVwlWryzthpWuIpWOieKYZXO2q1vC0KFr9aZM3Yr40QunspU2TVYmYhVRvNErwQtR3E0JTq3G2o80mc0Ql9dzTr/SYdnFA8k5yft78nLCjcxJNwKzsuoOiZoj2GKsi3NS1I7y4hIA7n9wgQvyz4KiWLphwNgyoRMq+jZ+MUyo1ToOR6Ra3wA6ua4b8F/2gL830WF8YRGB2VX4hBwOBEWrnxl3kGzQyXzyLJbfLuL+uwQevr+Jk7EsiBn2MmMBdbJ58UEEKx9vYfVDE89MBtTsTVjA53iiy/XbeD4XRaluwhWSNRZQIYmeay6cSsYxfCmFwfMpEGW4wjk4gxm4J7IECd6IhOW7z/AlkYRaawXQbyuTtCOJAQzPp/bU9gtrLOBHrUECJI3bP5bWypoJx7l9cE+tMO0TsTuIpl90uCq+xJnoEtP2hUV8Cp7G90orwMECGthQd5gynRxLPUWuoOOR8huPN//gyde/iMuvmLZvKgtlfBTFdsBgSNwslavQiOIACaCF0ofzRQv5bzsd6BrV9obSyI8EUCw34JwkAcd4aWFoWn5N00ihFi30+HwaM5LCmM4UGH5SLtX28uvMtlg2mwH2U9UuNHBlDUKu2ANdo9pDwjqqpNQSOwdyrSegXeih0Rh7wQ5da2lbdDI5RBqxT/Qa2ArdUK1ddLV7/gX7jb1QzdhGjVAl10262n0D7IXSSbtpa9vf+QeB6/JTIb6VuwAAAABJRU5ErkJggg==" alt="">Manage My Photos</a>
                    </div>
                    <div onmouseup="javascript:if (event.button == 0) actionClicked(this);" class="item">
                        <a href="/settings/videos.php"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAJKSURBVDjLdZJNSFRRFIC/82YazQJpkVgbQV9aU23cuQtatIxaREi4C9zmQvrBpEUGLtvnQkqIVi0jgmzjT5PWZJYwIiEG2iInnR/ffeeeFuNMjtaFwz1wz/fde+69Ymb03Z1Ine88uZxMSuP84lo4PtKb5x/j0rX7zafPtee2torlxWymY/rVWCRmBlAneZ/9Hk6M9tVJLl693dx5tiNXKBbLix9nOzJvnkUANQHAjTtPU+n248uYNc7MLIYvnwzkAS5cvtUcnjmVKxZK5a+fZzvm3z6PqkydAODKzceprs4TOfXx4Q/Tc2EUFelMd+UK26XSty+Z8NO7F9HeejEzBgcHHwD3qjIzo6WlJQGgqnjvWV9fVzPDzFBVCoXCw/Hx8eHkLjAUXn8k+y/NDNTAe8OXNLH221FSMODXWO8QMBwANDU1ScsRIZCDcKzGj7xjNe+IvZAQCADnnEAlx8xoTELrUSEZ/IXLkbK6GbEVeRIiJIIKEIigqtQEzjmcVsBjjYJIBf65HWOeXVgIEAIRAqMmSAJEUUTkgd2dU2LkywoIIkYAeKOSG3jZJ1BVnFaK1Hu2nKcpFeDUCAJQBcQQE6qPXieI45gdNcxDKTbUV/o8lDBiJ3VwNbz39S0UdgznoeSMWEHNUBNMKmf2tgfG6gUNDQ1svh5lZWWFkaUlBtracM6RTqdZmJuju7ubqakpenp6mJycJAzDWgtiZvT391trayuq+t/w3tdm7z3ZbJZMJiNJgI2NDRYWFmqL3nvM7EBe/crVvwPwB5ahnKoTKjg4AAAAAElFTkSuQmCC" alt="">Manage My Videos</a>
                    </div>
                    <div onmouseup="javascript:if (event.button == 0) actionClicked(this);" class="item"><a href="https://www.SaltHub.com/billing"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAJHSURBVDjLfZJPa1NREMV/972ksYmx0bbUpzS1ValpKVQohgguRLuxIBRBcKMbwS/g2l026jfQpWtTcCEFN+Kii0AoSFNpQUirlFprXmybNi/v3nHxYv6Q6oFhLsydc+aeO+rWs8UX08nYkx7bigOIAGIQEcQImCCLMRgjFEuVt+9fzt+jgdC10fjT00PnAQukdbkra0H7PhcOardpQwgBRIEECjSUxAiiTaCsWyQ9Fqc6CB5dP8P4+DCfVnYZONVDtabb66SG4ywWfjCfcQBYWVEddUtEANjYOeTVYql5/hurm3vklrZY3dwj8EjofEIDNyb7AYhGbKIRm+RgL1++7bOxc8h8xuHnb4/joIrFoqRSKQCWl5epVCpEo1Fs2z62QUSoVqu1Uqn0oVAoPA8dbb9DTrwBI5TLs6TTaUKhEEop/gXP8yKO44waYx6HRPvQcL+vr49wOIy3vo4sLCC1GlYqhT19EWKrUPsKGKzIBM7Q7MTIyMhl++Gd/rM7h87M1i8bFbvCoFKobBZrdxe7XMZaW4OPS+iMjSVV0DVU/Tth26dcG7JVu6uFQkEmNjYglwtW0hgwhr25S8SvHoAyIBrEx05k+Lw9idVlkueB1uD7zYjnivh1C0w9CF0PyNu/sUkwNobSuqmO1uynz3HSPgDjNxp9IFi4rgnCU1N4yWRrAq2JztyEiANiAAO9w6iBue4JXNelrjXRbBY5OkI8DxWPE2zE3dbyKIXnebiu20mQz+cfGGNeJxKJmGVZ/A+u65LP5+//AbkTRxnEop0TAAAAAElFTkSuQmCC" alt="">Billing</a>
                    </div>
                    <div onmouseup="javascript:if (event.button == 0) actionClicked(this);" class="item">
                        <a href="/adv"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAPCAMAAADarb8dAAAABGdBTUEAALGPC/xhBQAAAKhQTFRF7evhsqqLucPavLOTWlZO6OTX4t7Sy8nBbWdRlZCBdou3fXZeioRsT2qjaWZplZGBhH1kd3BZ6+jdsKeIqaWYZmBTqqGCi4Rqs6yRxMG59/bxraWHW1ZKubGR6OTYl5F+pJ1+tKuMnpd9tq6Os6uMo52I9/XxopuB5ODUycOu5eHS6ufb8e/nqKCCYFtTbmlaycKn3trNa2VPQ0BDYnqtO1mYxLua////YspnjgAAAINJREFUCNdjMEcGnEZ8DMh8ER4pYyQBdn5BCTWgAJcpEBgZGXEIqBsY6gAFTJmAQIgBCLS1NOWAArwmQGAGAcKs2ARMQQIqjECgqCzGimKokqyCBshaUWZmZhlTsLW6bGB3sBhym5tCHAYTEEd1OouQtCrcvSABPUOgHmQBSX1RebgAAF76JzooAJmWAAAAAElFTkSuQmCC" alt="">Advertising</a>
                    </div>
                    <div onmouseup="javascript:if (event.button == 0) actionClicked(this);" class="item">
                        <a href="/pages/page_create.php"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAIpSURBVDjLpZNPSFRRFMZ/749/Kt3IqFTSRoSMmrGIYTTbpEJtjBCCok1Em9JVG1dRC8FFEES5aGFEgRRZWq1iLKKxBiNqLDcltQgmHR9hY6LOu+feFm+YGVsZXbh8nHO53/nud8+xjDH8z3IB7r5avGgMZ8XoBq01okFpjYhGtEGJLtmCKINo/XbgVFPUBdDG9PVEq0P/UvnSvdlwQYFoHQIY/3obpRVKFL5W+OIXUVThrL91AN+XihKCwIeTu85sqPryqsJXUvRARAMwkshsiKB7fw25UgKVJwA40V7H/cl5jh+oL+RGk/P0xIqxl11dr8AXjTYG14HRNxkcx+ZhMoNlg52/ND6VAWMoc6F5+2Zy/l9PMIDrWByL1jI+tcDRaN06BaXxbDqLUnq9AqPBteHpuwUcJ0AIcgBXH93h+/wEyyuLrPk5cmv7gNY8gdIYYyhz4PDeWuIpj85IsS2ujQ2zJAk6DkZpqGnixcwYyU+PifUOX7Eh6DoAx7aIpzwA4imPeMrj+bTH+88PaNkZQWwhsrULsXxie9oAzgcESgUe2NAZCeE6AXZGQhwKh/Cyc5RZVXQ39wFwoeMmjXVhgMqiB8awe0cVP36u0Fi/iW9zvwuzkF3+xUz6Nal0gv6uWww+O02lUwGwmv8FM3l55EtLTvQWXwm+EkRpfNEoUZRXHCE5PUFbuJ0nH4cot1wSH14C3LA2Os6x3m2DwDmgGlgChpLX0/1/AIu8MA7WsWBMAAAAAElFTkSuQmCC" alt="">Create a Page</a></div><div onmouseup="javascript:if (event.button == 0) actionClicked(this);" class="item"><a href="/employment/emp_create.php"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAHiSURBVDjLpZPNaxNBGMb33/DWoxcPngr+B4pQggdL8WNbaxQbGtugkESE0IZEkiYhpC2GtJQatKW5pFYl2yrKCrZVrMYo2jbHfJElm/1IPJXHmdklbZGURBd+PPO++84zM+/ucAC4/4GzjC2PTKXzuvvZPlxPCcldOBd/4v7Cd9ybz8Exl8V4fAd3H3+GfeYjbLEtuOLvdTqPGXhT+YNopoxusUfeHTADuvK/YA28BTNwJffQbDa75obvtWHgfPILjUYDOkOHrutsLIpiWxqkhvcKpgFpmKqq6J/IttBITAvbPZqm4ZrnlWFAu11XFCh1glI3VTnRgL6/8vCFYUA/Va0mQ5Zlpgy5RrRGcoYeIht5wsCDVcPAkfgKSZJQNaFjqSr9de54KozxmauwhvrAPzoPi3PMMLDPfkKlUjnWAxofPcLzD0vwrAxiLTeNL0UB4fU7uBw+g96RU0FuNLaNUqlEKJtqcNRgKHAB6W8RpH9EWRzcuInwxi1q8JuzRTdRKBQJBRQJhRbFll50n8XLXOJYI1ez09QA9C4s3w6J7M8a9r/BoHcd1ycypMtrrFH97jTO2XoQEIbgE3g22ZfhD3fQyY0jhZOXgqcxJQyzlanSmPWg02tLiv0ElW7bVD/N/wGu4yJFlYuLaQAAAABJRU5ErkJggg==" alt="">Post a job</a>
                        </div>
                    <div onmouseup="javascript:if (event.button == 0) actionClicked(this);" class="item">
                        <a href="/logout.php"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAJCSURBVDjLlZO7a9RBFIW/+e1ms0kMmOCbRKKGaCBBUCsttNM/wU4UsRQUQQSblIKWFpGIiLVYWQgWsRIRxFc0PhOMhSjRDZFkZ+5jLFazWWx04HKq883cw5mQc+Z/z9T105fc7ayZLpb/x/j6xpl37jZYWb+JmdkpeouKrgDGxsayu/NnzGxFT4xkCpzKuk2s2TaIm5NnXiASWQGYGX19fQCEEFo055f07DsABOLPeUwiOTsiSrEakMlM1u+tmP+MmeFm1GufkaUFXBLZ7e8X3F++y0KqETqbZgDVhJtgmnBNQCC7k1K9CZjufcqWjZvpsbXc+jiBqaFimBpX+/eQVXFJmCbIDYDKb8CRK4eeD/QPMDo0irqya3An4oqYcPv2HeT3zSaRrHU2rv/K+6ykFCkfvnzw5sCWgdHRoRFq9RpLsoSYkFzoKq9B1RBJmCqWIt1dP+hdO09baZlFqVPcO/fg2JuPb6cePXtMEUq0l6pUyx1USx1ES6gYInVcIyaR2vcSs7PriKmtGeLkxYcjB8/vz8v1ZVSVDx9mMHVMDTcnpYir4BIxEeZjGdwRSc0Qt3/dyUx4S5FLnNt7oaUL+upaIwMVTCMhlHF3VFOzB6rK8eFTZMstHQghkCQ2zBJxSY0e5AagvBpQFAUndp9q6UAIAZHGCp09/bgKGpcgf8FMCePj43l6epq5ubmW/q/Wo9tn6erupr3aRaXaSVulncWfNT69efIt/Mt3nji5dYOZ7jCTYTMdcre+olw5ahIXfgHcTaP3d3vNvQAAAABJRU5ErkJggg==" alt="">Logout</a>
                    </div>	
                </div>
            </div>
            <div style=" display: none; padding-top:1px;">
            <?
                showNavDrop('Account <img src="/images/arrow_down_white.png"  width="11" height="9" alt="" />', array(
                    array("Account Settings", "/settings", "application_edit"),
                    array("Manage My Photos", "/settings/photos.php", "images"),
                    array("Manage My Videos", "/settings/videos.php", "television"),
                    array("Billing", ($isDevServer?"":("https://www." . $siteName . ".com")) . "/billing", "creditcards"),
                    array("Advertising", "/adv", "sound_mega_phone"),
                    array("Create a Page", "/pages/page_create.php", "page_add"),
                    array("Post a job", "/employment/emp_create.php", "application_form_add"),
                    array("Logout", "/logout.php", "door_out")
                        ), "top: 20px; left: -85px;");
                ?>
            </div>
        </div>
    </div>
    <div style="clear: both;"></div>
<? } else { ?>
<div style="background: #1B3E5D; padding-bottom: 15px; margin-left: 15px; height: 27px;">
  <div style="float:left; padding-right:20px; color:#ffffff; font-size:9pt;"><a style=" margin-left:20px; color:#fff;" href="http://<?=SERVER_HOST?>">Home</a></div>
  <div style="float:left; padding-right:20px; color:#ffffff; font-size:9pt;"><a style="color:#fff;" href="/about.php">What is <? echo $siteName ?>?</a></div>
  <div style="float:left; padding-right:20px; color:#ffffff; font-size:9pt;"><a style="color:#fff;" href="/start">Join Today!</a></div>
  <div style="float:left; padding-right:20px; color:#ffffff; font-size:9pt;"><a style="color:#fff;" href="/start">Sign In</a></div>
 </div>
<? }
?>

</div>
<? if ($API->isLoggedIn() ) {?>
    <div class="sub-header">
		<div style="width: 984px; margin: 0px auto;">
			<a style="width:55px" class="new_header_a <?php echo $uri_segments[1]=='welcome'?'active':''; ?> " href="/">Welcome</a>
            <a style="width:83px" class="new_header_a <?php echo $uri_segments[1]=='pages'?'active':''; ?> " href="<?= $API->getPageURL('15528').'/logbook' ?>">SaltHub News</a>
            <a style="width:74px" class="new_header_a <?php echo $uri_segments[2]=='employees.php'?'active':''; ?> " href="/employment/employees.php">Employment</a>
			<a style="width:112px" class="new_header_a <?php echo $uri_segments[1]=='directory.php'?'active':''; ?> " href="/directory">Business Directory</a>
            <a style="width:38px" class="new_header_a <?php echo $uri_segments[1]=='pages'?'active':''; ?> " href="/pages">Pages</a>
            <a style="width:83px" class="new_header_a <?php echo $uri_segments[1]=='mediahome.php'?'active':''; ?> " href="/mediahome.php">Popular Media</a>
            <a style="width:98px" class="new_header_a <?php echo ($uri_segments[1]=='vessels' && empty($uri_segments[2]))?'active':''; ?> " href="/vessels">Vessel Directory</a>
            <a style="width:94px" class="new_header_a <?php echo $uri_segments[2]=='vessel_live_tracking.php'?'active':''; ?> " href="/vessels/vessel_live_tracking.php">Vessel Tracking</a>

<!--			<a style="width:41px" class="new_header_a --><?php //echo $uri_segments[1]=='search_m.php?t=V'?'active':''; ?><!-- " href="/search_m.php?t=V">Videos</a>-->
<!--			<a style="width:42px" class="new_header_a --><?php //echo $uri_segments[1]=='search_m.php?t=P'?'active':''; ?><!-- " href="/search_m.php?t=P">Photos</a>-->
            <div class="sub-menu-sep"></div>
            <div style="font-size: 12px; margin: 0px 22px 0 21px; color: green; font-weight: bold; float: right; line-height: 49px;">
                <?php
                showNavDrop("Connect", array(
                    array("Contact Manager", "/contacts.php", "vcard"),
                    array("Connections", "javascript:showFriendsPopup(" . $API->uid . ");", "user"),
                    array("Find Connections", "/findpeople.php", "find"),
                    array("Invite Connections", "/invite.php", "email_add")
                        ), "top: 20px; left: -20px;");
                ?>
					<a href="/invite.php" style="color: green; margin-left: 15px;">
					<img width="16" height="16" alt="" style="position: relative; top: 4px; padding: 0px 5px 0px 0px;" src="/images/add_connection_green.png">
					Add
					</a>
            </div>
		</div>
    </div>
<?}?>
</div>

<div style="clear: both;"></div>

<script>
    $('#txtsearch').keypress(function(e){
        if (e.keyCode == '13')
            searchDo();
    });
</script>

<?php
/* old navbar below:


		showNavDrop('<span class="moresearch">more search</span><img src="/images/arrow_down.png" alt="" />', array(
			array("Account Settings", "/settings", "application_edit"),
			array("Manage My Photos", "/settings/photos.php", "images"),
			array("Manage My Videos", "/settings/videos.php", "television")
				), "top: 22px; left: 0px;");



<div class="nav">
	<div class="navlinks"<?=$hideHeaderLinks ? ' style="display: none;"' : ""?>>
		<a href="/invite.php" class="invite">Invite Friends</a>
		<?php
		$i = 0;
		foreach ($navLinks as $title => $item)
		{
			if ($i == count($navLinks) - 2)
			{
				echo '<div style="float: right;">';
				$isFloating = true;
			}
			else
				echo '<span class="separator"> | </span>';

			if (is_array($item)) //dropdown
			{
				$id = substr(md5($title), 0, 5);
				?>
				<span class="navdropcontain" onmouseover="javascript:showDropDown('dd-<?=$id?>');" onmouseout="javascript:hideDropDown('dd-<?=$id?>');">
					<?=$title?>
					<div id="dd-<?=$id?>" class="navdrop">
						<?php
						foreach ($item as $menuitem)
						{
							if (substr($menuitem[1], 0, 11) == "javascript:")
								$menuitem[1] = 'javascript:void(0);" onclick="' . $menuitem[1];
							echo '<div onmouseup="javascript:if (event.button == 0) actionClicked(this);" class="item"><a href="' . $menuitem[1] . '"><img src="/images/' . $menuitem[2] . '.png" alt="" />' . $menuitem[0] . '</a></div>';
						}
						?>
					</div>
				</span>
				<?php
			}
			else
				echo '<a ' . ($isFloating ? 'class="unbolded" ' : '') . 'href="' . $item . '">' . $title . '</a>';

			$i++;
		}
		unset($title);
		//close div from float right
		echo "</div>";?>
	</div>
</div>
*/
?>