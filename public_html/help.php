<?php
include_once "inc/inc.php";
include_once "inc/recaptchalib.php";

$title = "How to Use | $siteName";

$page_header = "How to Use $siteName";
  $sub_header = $siteName . ' is the leading social network for professionals and businesses who make a living on and around the water.';

$scripts[] = "/index.js";
$noheader = true;

if( $API->isLoggedIn() )
{
  include "header.php";
}
else
{
  include "signup/newheader.php";
  include "header_gradient.php";
}


?>

<div class="graygradient">
	<div class="headcontainer">
		<div class="entryslide">
			<?=$htmlSignIn?>
			<div class="slidecontent">
				<span class="title">How to Use</span>
			
				<div class="text">
We have a few rules. The terms of service below will provide you information about usig SaltHub and how to get the most out of our service.
				</div>
				
			</div>

			<img src="/images/TOS.jpg" class="bigimage" alt="" />
		</div>
	</div>
</div>

<div style="width: 1050px; margin: 0px auto;">
	
	<div class="imghelp imghelpr" style="background-image: url(/images/logbook.png);"><img src="/images/fadel.png" alt=""></div>
	<div class="txthelp">
		<div class="txthelp2">
			<div>
				<h1>Add to Log</h1>
				Let people know what you want to be contacted for to make yourself more visible in search results.
			</div>
		</div>
	</div>
	
	<div class="txthelp">
		<div class="txthelp2">
			<div>
				<h1>Contact For</h1>
				Let people know what you want to be contacted for to make yourself more visible in search results.
			</div>
		</div>
	</div>
	<div class="imghelp" style="background-image: url(/images/contactfor.png);"><img src="/images/fader.png" alt=""></div>
	
</div>

<?php include "signup/newfooter.php"; ?>