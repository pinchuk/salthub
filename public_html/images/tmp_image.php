<?php

// displays images in /tmp that might not have been uploaded to the cloud yet

$file = '/tmp/' . preg_replace("/[^A-Za-z0-9._ ]/", '', end(explode('/', $_GET['u'])));

if (file_exists($file))
{
	header('Content-type: image/jpeg');
	readfile($file);
}
else
	header('Location: ' . $_GET['u']);

?>