<?php

if (array_key_exists("logout", $_GET)) {
    session_start();
    unset($_SESSION['oauth_token']);
    unset($_SESSION['oauth_token_secret']);
    session_destroy();
    header("location: index.php");
}
?>