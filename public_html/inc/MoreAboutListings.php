<!-- More About SaltHub Listings Module -->
<div class="subhead">More about <?=$siteName?> Listings</div>

<div class="connect">
	<a href="/pages/claim_company.php"><img src="/images/vcard_add.png" alt="" />Get Listed <span style="font-size:8pt; color: #777; font-weight:normal;">(recommended)</span></a>
	<div class="desc">
  Creating a page has many advantages. It brings you closer to customers, peers and followers. It provides the ability to be heard, discovered and so much more.
    <div class="link">
    	<a href="/pages/claim_company.php">More about getting listed</a>&nbsp; <img src="/images/wrench.png" alt="" />
    </div>
	</div>
</div>

<div style="height: 0px; border-bottom: 1px solid #d8dfea; margin-top: 5px; margin-bottom: 5px;"></div>

<div class="connect">
	<a href="/pages/claim_company.php"><img src="/images/checkmark_sticker_16.png" alt="" />Claim your listing</a>
	<div class="desc">
  Many companies on <?=$siteName?> have been identified as cornerstones of Maritime Industries. Log in and see if your company is listed and claim it to receive leads + connect.
    <div class="link">
    	<a href="/pages/claim_company.php">More about claiming your listing</a>&nbsp; <img src="/images/wrench.png" alt="" />
    </div>
	</div>

</div>

<div style="height: 0px; border-bottom: 1px solid #d8dfea; margin-top: 5px; margin-bottom: 5px;"></div>

<div class="connect">
	<a href="/"><img src="/images/connected.png" alt="" />Your Connections</a>
	<div class="desc">
    <?=$siteName?> business listings are social. See how many of your connections use the businesses you're looking for. Learn and exchange information from others you're connected with.
    <div class="link">
    	<a href="/">Sign up to see who you're connected with</a>&nbsp; <img src="/images/wrench.png" alt="" />
    </div>
	</div>


</div>