<?php
/*
Contains advertisment functions, included in most pages on SaltHub.

3/11/2012 - Added get*Stats Function, changed ad code to support ad_campaigns

*/

//include "../inc/inc.php";

function showAd($w, $h = 0 )
{
	global $site;

  switch( $w )
  {
    case "companion": showSquareAd($h); break;
    case "skyscraper": showTallAd($h); break;
  }
}

function getAds()
{
  global $ads;
  
  $API = new API();
  
  $gid = intval($_GET['gid']);
  
  if( empty( $ads ) )
  {
    $ads = array();

    $sql = "select distinct ads2.id, ad_campaigns.name, ads2.title, ads2.url, ads2.body, ads2.pid, ad_campaigns.daily_budget, ads2.uid from ads2 inner join ad_campaigns on ads2.campaign=ad_campaigns.id left join ad_preferences ap on ap.ad=ad_campaigns.id where ads2.priority=1 AND ads2.approved=1 AND ads2.complete=2 AND ad_campaigns.status=1 AND (ad_campaigns.runtime=0 OR (ad_campaigns.start<=Now() AND ad_campaigns.end>=Now())) AND (ad_campaigns.locations like '%," . $_SESSION['country']  . ",%' OR ad_campaigns.locations like '%,0,%' OR ad_campaigns.locations=0) AND (";

    $condition_count = 0;
    
    if( sizeof( $_SESSION['occupations'] ) == 0 && sizeof( $_SESSION['page_types'] ) == 0 )
      $sql .= "1=1";
    else
    {
      $sql .= "(ap.type=3 and (ap.type_id=0";
      if( sizeof( $_SESSION['occupations'] ) > 0 )
      {
        for( $c = 0; $c < sizeof( $_SESSION['occupations'] ); $c++ )
        {
          $sql .= " OR ";
          $sql .= "ap.type_id=" . $_SESSION['occupations'][$c];
          $condition_count++;
        }
      }
      $sql .= "))";

      if( sizeof( $_SESSION['occupations'] ) > 0 )
        $sql .= " OR ";

      $sql .= "( ap.type=4 and (ap.type_id=0";

      if( sizeof( $_SESSION['page_types'] ) > 0 )
      {
        for( $c = 0; $c < sizeof( $_SESSION['page_types'] ); $c++ )
        {
          $sql .= " OR ";
          $sql .= "ap.type_id=" .$_SESSION['page_types'][$c];
          $condition_count++;
        }

      }
      

      $sql .= "))";
    }
    if ($gid > 0)
    {
      $page = $API->getPageInfo($gid);
      $curPageType = $page["type"];
      $sql .= " AND ap.type = 4 AND type_id = ".$curPageType;
      $condition_count++;
    }
    $sql .= ")";


    $q = sql_query( $sql );
//  echo mysql_error();
    if( $condition_count > 0 && $q && mysql_num_rows( $q ) > 0 )
    {
      while( $r = mysql_fetch_array( $q ) )
        $ads[] = $r;
    }
    
    $_SESSION['keywords'] = array_filter($_SESSION['keywords']);
    
    if( sizeof( $_SESSION['keywords'] ) > 0 )
    {
//      $sql = "select ads2.id, ads2.name, ads2.title, ads2.url, ads2.body, ads2.pid, ad_campaigns.daily_budget, ads2.uid from ads2 where ads2.priority=1 AND ads2.approved=1 AND ads2.complete=2 AND ads2.start<=Now() AND ads2.end>=Now() AND (locations like '%," . $_SESSION['country'] . ",%' OR locations like '%,0,%' OR locations=0) AND (";
      $sql = "select ads2.id, ad_campaigns.name, ads2.title, ads2.url, ads2.body, ads2.pid, ad_campaigns.daily_budget, ads2.uid from ads2 inner join ad_campaigns on ads2.campaign=ad_campaigns.id left join ad_preferences ap on ap.ad=ad_campaigns.id where ads2.priority=1 AND ads2.approved=1 AND ads2.complete=2 AND ad_campaigns.status=1 AND (ad_campaigns.runtime=0 OR (ad_campaigns.start<=Now() AND ad_campaigns.end>=Now())) AND (ad_campaigns.locations like '%," . $_SESSION['country']  . ",%' OR ad_campaigns.locations like '%,0,%' OR ad_campaigns.locations=0) AND (";

      $kw = 0;
      if(!empty($_SESSION['keywords'])){
	  for( $c = 0; $c < sizeof( $_SESSION['keywords'] ); $c++ )
	    {
	      if( strlen( trim( $_SESSION['keywords'][$c] ) ) > 0 )
	      {
		if( $kw > 0 ) $sql .= " OR ";
		$sql .= "ads2.keywords like '%" .$_SESSION['keywords'][$c] . "%'";
		$kw++;
	      }
	    }
      }
      else{
	  $sql .= "1=1";
      }
      

      $sql .= ")";

      if( $kw > 0 )
      {
        $q = sql_query( $sql );

        if( mysql_error() == "" )
        {
          while( $r = mysql_fetch_array( $q ) ) //This may have duplicates in it.
            $ads[] = $r;
        }
      }
    }
    
  }
  
  if( sizeof( $ads ) < 5  )
  {
    $q = sql_query( "select ads2.id, ad_campaigns.name, ads2.title, ads2.url, ads2.body, ads2.pid, ad_campaigns.daily_budget, ads2.uid from ads2 inner join ad_campaigns on ad_campaigns.id=ads2.campaign where ads2.priority=0" );
    while( $r = mysql_fetch_array( $q ) )
      $ads[] = $r;
  }
  //print_r($ads);
  shuffle( $ads );
}

function showSquareAd( $ad = 0 )
{
  global $ads;

  getAds();

  $adsDisplayed = 0;

	echo '<div style="z-index: 1;">';

  for( $c = 0; $adsDisplayed < 2 & $c < sizeof( $ads ); $c++ )
  {
    $daily_impressions = quickQuery( "select impressions from ad_log where ad='".$ads[$c]['id']."' and day=Date(Now())" );
    if( $daily_impressions >= $ads[$c]['daily_budget'] ) continue;

    $ad_credits = quickQuery( "select ad_credits from users where uid='" . $ads[$c]['uid'] . "'" );
    if( $ad_credits <= 0 ) continue;

    $hash = quickQuery( "select hash from photos where id='" . $ads[$c]['pid'] . "'" );

    $url = "/adv/clk.php?ID=" . $ads[$c]['id'];
?>
    <div class="ad" style="float:left; width:150px; cursor:pointer;">
      <div class="title" id="adtitle" onclick="javascript: location='<? echo $url ?>';"><? echo $ads[$c]['title']; ?></div>
      <div class="image" id="adimg" onclick="javascript: location='<? echo $url ?>';"><a href="<? echo $url ?>"><img src="/img/119x95/photos/<? echo $ads[$c]['pid'] ?>/<? echo $hash ?>.jpg" width="119" height="95" border="0"/></a></div>
      <div class="body" id="adbody" onclick="javascript: location='<? echo $url ?>';"><? echo $ads[$c]['body'];?></div>
      <div class="report"><a href="javascript:void(0);" onclick="javascript: showReport('A', <? echo $ads[$c]['id']; ?>);">report ad</a></div>
    </div>

<?
    incImpressions( $ads[$c]['id'], $ads[$c]['uid'] );

    $adsDisplayed++;
  }
  echo '<div style="clear:both;"></div>';

  if( $adsDisplayed == 0 )
  	echo quickQuery("select code from ads where id='companion'");

	echo '</div>';
}

function showTallAd( $adCount = 0 )
{
  global $ads;
  $adsDisplayed = 0;
  getAds();
  
  $url_arr = explode('/', $_SERVER['REQUEST_URI']);
	switch($url_arr[1]){
		case 'user' : $adCount = 4; break;
		default : $adCount = 4;//3;
	}

	echo '<div style="z-index: 1;">';

  for( $c = 0; $adsDisplayed < $adCount & $c < sizeof( $ads ); $c++ )
  {
    $daily_impressions = quickQuery( "select impressions from ad_log where ad='".$ads[$c]['id']."' and day=Date(Now())" );
    if( $daily_impressions >= $ads[$c]['daily_budget'] ) continue;

    $ad_credits = quickQuery( "select ad_credits from users where uid='" . $ads[$c]['uid'] . "'" );
    if( $ad_credits <= 0 ) continue;


    $hash = quickQuery( "select hash from photos where id='" . $ads[$c]['pid'] . "'" );
    $url = "/adv/clk.php?ID=" . $ads[$c]['id'];
?>
    <div class="ad" style="width:150px; cursor:pointer;">
      <div class="title" id="adtitle" onclick="javascript: location='<? echo $url ?>';"><? echo $ads[$c]['title']; ?></div>
      <div class="image" id="adimg" onclick="javascript: location='<? echo $url ?>';"><a href="<? echo $url ?>"><img src="/img/119x95/photos/<? echo $ads[$c]['pid'] ?>/<? echo $hash ?>.jpg" width="119" height="95" /></a></div>
      <div class="body" id="adbody" oncl`ick="javascript: location='<? echo $url ?>';"><? echo $ads[$c]['body'];?></div>
      <div class="report"><a href="javascript:void(0);" onclick="javascript: showReport('A', <? echo $ads[$c]['id']; ?>);">report ad</a></div>
    </div>
<?
    incImpressions( $ads[$c]['id'], $ads[$c]['uid'] );
    $adsDisplayed++;
  }

  if( $adsDisplayed == 0 )
  	echo quickQuery("select code from ads where id='skyscraper'");

	echo '</div>';
}

$adsShown = array();
function incImpressions( $ad, $uid )
{
  global $adsShown, $cpm_rate;
  if( !in_array( $ad, $adsShown ) )
  {
    if( !isset( $cpm_rate ) )
      $cpm_rate = floatval( quickQuery( "select content from static where id='cpm_rate'" ) / 1000 );

    $adsShown[] = $ad;
    sql_query( "update ad_log set impressions=impressions+1, cost=cost+$cpm_rate where ad='$ad' and day=Date(Now()) limit 1" );
    if( mysql_affected_rows() == 0 )
    {
      $campaign = quickQuery( "select campaign from ads2 where id='$ad'" );
      sql_query( "insert into ad_log (ad,day,impressions, cost, campaign) values ($ad, Now(), 1, $cpm_rate, $campaign)" );
    }

    if( $uid != 832 )
      sql_query( "update users set ad_credits=ad_credits-$cpm_rate where uid='$uid' limit 1" );
  }
}

function getAdStats( $ad, $range )
{
  $dates = dateRangeToRealDates( $range );
  $startdate = $dates[0];
  $enddate = $dates[1];
  return queryArray( "select SUM(impressions) as impressions, SUM(clicks) as clicks, SUM( shares ) as shares, SUM( cost ) as cost from ad_log where ad='$ad' AND day BETWEEN '$startdate' AND '$enddate'" );
}

function getCampaignStats( $campaign, $range )
{
  $dates = dateRangeToRealDates( $range );
  $startdate = $dates[0];
  $enddate = $dates[1];
  return queryArray( "select SUM(impressions) as impressions, SUM(clicks) as clicks, SUM( shares ) as shares, SUM( cost ) as cost from ad_log where campaign='$campaign' AND day BETWEEN '$startdate' AND '$enddate'" );
}


/*
Possible values for $range (below)
      <option value="0">Today</option>
      <option value="1">Yesterday</option>
      <option value="2">Last 7 Days</option>
      <option value="3">Last 15 Days</option>
      <option value="4">Last 30 Days</option>
      <option value="5">This Month</option>
      <option value="6">Previous Month</option>
      <option value="7">All</option>
*/

function dateRangeToRealDates( $range )
{
  switch( $range )
  {
    case 1:
      $startdate = date( "Y-m-d", time() - 86400 );
      $enddate = $startdate;
    break;

    case 2:
      $startdate = date ( "Y-m-d", time() - 604800 );
      $enddate = date ( "Y-m-d H:i" );
    break;

    case 3:
      $startdate = date ( "Y-m-d", time() - 1296000 );
      $enddate = date ( "Y-m-d H:i" );
    break;

    case 4:
      $startdate = date ( "Y-m-d", time() - 2592000 );
      $enddate = date ( "Y-m-d H:i" );
    break;

    case 5:
      $month = date( "m" ); //Get current month
      $startdate = date ( "Y" ) . "-" . $month . "-1";
      $enddate = date ( "Y" ) . "-" . $month . "-31";
    break;

    case 6:
      $month = date( "m", time() - 2419200 ); //Get previous month
      $startdate = date ( "Y" ) . "-" . $month . "-1";
      $enddate = date ( "Y" ) . "-" . $month . "-31";
    break;

    case 7:
      $startdate = date ( "Y-m-d", 0 ); //All data
      $enddate = date ( "Y-m-d H:i" );
    break;

    default:
      $startdate = date( "Y-m-d" );
      $enddate = $startdate;
    break;
  }

  return Array( $startdate, $enddate );
}
?>