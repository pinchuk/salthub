<?php
/*
This is the API class used throughout the site.  This class contains functions related to the current user and stores user session data.

8/9/2011 - Remove "via @Salt_Hub" from FB and emails; changed to "via SaltHub.com"
8/17/2011 - Updated Facebook notification messages
8/20/2011 - Updated Twitter notification messages, added "reported" field to videos and photos, so reported media doesn't show up.  Handled in GetPrivacyQuery
8/26/2011 - Added "todo" list tracking code
2/10/2012 - Added page notifications
2/17/2012 - Added timer functions startTimer, stopTimer for timing queries and page loads.
4/13/3012 - Added getMediaOwner() for tagging page.
5/5/2012  - Added Facebook error checking
7/28/2012 - Added support for vessel notifications
8/26/2012 - Added $API->requireLogin() before the header to fix redirect issues when a user leaves the browser open for extended periods (e.g., days)
9/9/2012  - Added function verifyPage( $gid )
9/23/2012 - Added support for RSS feed comments/notifications
9/28/2012 - Added support for JMP links to feed posts
10/26/2012 - Removed facebook sendEmail call (fbSendMessage).  Upgraded to latest Facebook API
10/28/2012 - Added fbFQLQuery function, uses Graph API
11/7/2012 - Added sendWelcomeMessage() for new users
*/
/**
 * Class API
 */
class API
{
		public $uid, $fbid, $name, $timer;

		function __construct()
		{
			global $facebook;

			$this->uid = $_SESSION['uid'];

      if( empty( $_SESSION['name'] ) ) $this->restoreSessionVariables( $this->uid );

			$this->admin = $_SESSION['admin'];
			$this->fbid = $_SESSION['fbid'];
			$this->twid = $_SESSION['access_token']['user_id'];
			$this->fbsess = $_SESSION['fbsess'];
			$this->name = $_SESSION['name'];
			$this->pic = $_SESSION['pic'];
			$this->username = $_SESSION['username'];
			$this->verify = $_SESSION['verify'];
			$this->needsVerified = $_SESSION['needsVerified'];
      $this->todoList = $_SESSION['todo'];
      $this->CloudConnection = false;

      //Advertisement settings
      if( isset( $_SESSION['adv'] ) )
      {
        $this->adv = $_SESSION['adv'];
      }
      else
      {
        $this->adv = intval( quickQuery( "select content from static where id='advert_on_off'" ) ); //Turn advertising on or off?
        $_SESSION['adv'] = $this->adv;
      }

      if( !isset( $_SESSION['country'] ) )
        $this->reloadAdData();
    }

    function restoreSessionVariables( $uid = null )
    {
      if( $uid == null )
        $uid = $this->uid;

      $q = sql_query( "select * from users where uid='$uid'" );

      if( $r = mysql_fetch_array( $q ) )
      {
			  $_SESSION['access_token']['user_id'] = $r['twid'];
        $_SESSION['fbid'] = $r['fbid'];
        $_SESSION['uid'] = $r['uid'];
        $_SESSION['todo'] = $r['todoList'];
        $_SESSION['admin'] = $r['admin'];
        $_SESSION['name'] = $r['name'];
        $_SESSION['pic'] = $r['pic'];
        $_SESSION['username'] = $r['username'];
        $_SESSION['needsVerified'] = ($r['verify'] == 0);
        $_SESSION['verify'] = $r['verify'];
      }
    }

		//checks to see if the user is logged in - if not, sends to home
		function requireLogin()
		{
			if (!$this->isLoggedIn())
    	{
        //$query = $_SERVER['QUERY_STRING'];
	$path = $_SERVER['REQUEST_URI'];
//        if( strlen( $query ) > 0 ) $query = '?' . $query;
//        $path = $_SERVER['PHP_SELF'] . $query;

        if( $path != "" && $path != "/404.php" )
          $_SESSION['login_redirect'] = $path;

				header("Location: /start?ia");
				exit;
			}
      else
      {
        global $script;

        $exceptions = array( "home", "signup/verify", "signup/newuser_updateProfile", "twitter/index", "fbconnect/loggedin", "facebook_error" );

        if( $this->verify == 0 && !in_array( $script, $exceptions ) && empty( $_SESSION['from_admin'] ) )
        {
          $this->verify = quickQuery( "select verify from users where uid='" . $this->uid . "'" );

          if( $this->verify == 0 )
          {
            header( "Location: /welcome" );
            //exit;
          }
        }
      }
		}

		//returns # of notifications for user
		function getNotificationsCount($uid = null)
		{
			if (empty($uid))
				$uid = $this->uid;

      $count = 0;

      $x = sql_query("select * from notifications where uid=" . $this->uid . " order by ts desc");

      while ($feed = mysql_fetch_array($x, MYSQL_ASSOC))
      {
      	$resolve = resolveFeed($feed);

        if( sizeof( $resolve ) > 0 ){
            switch ($feed['type'])
            {
                case "f":
                case "F":
                case "L":
                case "t":
                case "T":
                case "t2":
                case "T2":
                case "C":
                case "S":
                case "M":
                case "G":
                case "G":
                case "P":
                case "V":
                case "G1":
                case "G2":
                case "GV":
                case "GP":
                case "G3":
                case "G4":
                case "G5":
                case "G6":
                case "VP1":
                case "VP2":
                case "VP3":
                    $count++;
                    break;
                
                default:
                    break;
            }
        }
      }

			return $count;
		}

		//site = "m" or "s" - makes sure the user is on the right website (e.g. not trying to use a mb function for splash)
		function requireSite($s)
		{
			global $site;
			
			if ($site != $s)
			{
				header("Location: /");
				die();
			}
		}

		
		function getContainer($uid = null)
		{
			if ($uid == null)
				$uid = $this->uid;
				
			return quickQuery("select container_url from users where uid='$uid'");
		}
		
		//used for admin section, checks to see if user is an admin
		function requireAdmin()
		{
			global $isDevServer;
			
			if ($isDevServer)
				return;
			
			if ($this->admin != 1)
			{
				header("Location: /");
				die();
			}
		}
		
		function isAdmin()
		{
			return $this->admin == 1;
		}

		//called when user signs up, creates single uploads and profile photos albums if not already made
		function createSpecialAlbums()
		{
			global $site;


			if ($site == "s")
			{
        $found = array(0,0,0);

				$x = sql_query("select albType from albums where albType > 0 and uid={$this->uid}");

				while ($alb = mysql_fetch_array($x, MYSQL_ASSOC))
					$found[$alb['albType']] = true;
				
				if (!$found[1])
					sql_query("insert into albums (uid,title,descr,albtype) values (" . $this->uid . ",'Single Uploads','My miscellaneous photos',1)");

				if (!$found[2])
					sql_query("insert into albums (uid,title,descr,albtype) values (" . $this->uid . ",'Profile Photos','My profile photos',2)");
			}
		}

		//gives the current user credit for a successful referral
		function addReferral($uid_invited = null, $ref = null)
		{
			global $site;

			if (empty($uid_invited))
				$uid_invited = $_SESSION['uid'];

			if (empty($ref))
				$ref = $_SESSION['ref'];

      $email = quickQuery( "select email from users where uid=" . $this->uid );


      if( isset( $ref ) && $ref != "" )
      {
  			sql_query("insert into invites (uid,uid_invited) values ($ref,$uid_invited)");
  			//add referring user as a friend on splash
  			if ($site == "s")
  			{
  				sql_query("insert into friends (id1,id2,status) values ($uid_invited,$ref,0)");
  				$this->sendNotification(NOTIFY_FRIEND_ADD, array("uid" => $ref));
  			}
      }

      if( isset( $_SESSION['customcid'] ) )
      {
        $notifyType = null;
        $tid = null;

        //We need to convert the custom contact to a 'real' one.
        sql_query( "insert into contacts (eid, name, uid,site) values (" . $uid_invited . ", '" . $this->name . "', " . $ref . ", 2) " );
        $newcid = mysql_insert_id();

        //Clear out any left over contacts from the contact list.
        sql_query( "delete from contacts where uid='" . $ref . "' AND eid='" . $email . "'" );

        sql_query( "update photo_tags set cid='" . $newcid . "', custom=0 where custom='1' and uid='" . $ref . "' and cid='" . $_SESSION['customcid'] . "'" );
        if( mysql_affected_rows() > 0 )
        {
          $tid = quickQuery( "select tid from photo_tags where cid='$newcid' and uid='$ref' and custom='0'" );
          $notifyType = 't';
        }

        sql_query( "update video_tags set cid='" . $newcid . "', custom=0 where custom='1' and uid='" . $ref . "' and cid='" . $_SESSION['customcid'] . "'" );
        if( mysql_affected_rows() > 0 )
        {
          $tid = quickQuery( "select tid from video_tags where cid='$newcid' and uid='$ref' and custom='0'" );
          $notifyType = 'T';
        }

        //Add item on the new user's feed.
        if( isset( $notifyType ) )
          $this->feedAdd( $notifyType, $tid, $this->uid, $ref);
      }

      // Look for other people with the current user's email address in their contact list and send connection requests
      if( $email != "" )
      {
        $email = addslashes( $email );
        $q = mysql_query( "select uid from contacts where eid='$email'" );
        while( $r = mysql_fetch_array( $q ) )
        {
          $uid = $r['uid'];

          if( $uid != $ref )
          {
            sql_query( "insert into notifications (uid, type, from_uid) values (" . $this->uid . ", 'f', " . $uid . ")" );
            //$this->sendNotification(NOTIFY_FRIEND_ADD, array("uid" => $uid));
            mysql_query( "insert into friends (id1, id2, status) values ('" . $uid . "', '" . $this->uid . "', 0 )" );
          }
        }
      }
		}

		// the hash that will be used to determine whether or not the user has the privilege of deleting a comment
		function generateDeleteCommentHash($id, $uid = null)
		{
			if ($uid == null)
				$uid = $this->uid;

			return md5("_DeLeTe-" . chr(2) . (md5($id . ($uid * 8))) . "-mE^" . chr(8));
		}

		//returns javascript code that, when executed, will delete a feed
		function jsDeleteFeed($id)
		{
			return 'deleteFeed(\'' . $id . '\', \'' . $this->generateDeleteCommentHash($id) . '\');';
		}

		//checks to see if user is a member of a page
		function isUserMember($uid = null, $gid)
		{
			if ($uid == null)
				$uid = $this->uid;
			
			return quickQuery("select count(*) from page_members where gid=$gid and uid=$uid") == 1;
		}

		//counts media for a user id or page id
		function getMediaCount($uid = null, $isPage = false)
		{
			if ($isPage)
				$gid = $uid;
			else
			{
				if (empty($uid))
					$uid = $this->uid;
			}

			$pageFilter = ($gid == 0 ? "" : "inner join page_media gm on gm.id=media.id and gm.type=@type and gid=$gid");
      $pq = $this->getPrivacyQuery();

      if( $isPage )
      {
        $vidCount = quickQuery( "select count(*) from videos as media " . str_replace("@type", "'V'", $pageFilter)  );
        $picCount = quickQuery( "select count(*) from photos as media " . str_replace("@type", "'P'", $pageFilter)  );
        $albCount = 0;
//        $albCount = quickQuery( "select count(*) from albums where uid='" . $this->uid . "'" );
      }
      else
      {
        if( $this->uid == $uid )
        {
          $vidCount = quickQuery( "select count(*) from videos where uid='" . $uid . "' AND ready>0 and on_page = 0" );
          $picCount = quickQuery( "select count(*) from photos where uid='" . $uid . "' AND reported=0 AND aid!=884372 and on_page = 0" ); //Don't include advertisements in the count
          $picCount += quickQuery("SELECT count(*) FROM contacts INNER JOIN photo_tags ON photo_tags.cid = contacts.cid INNER JOIN photos ON photos.id = photo_tags.pid WHERE photos.privacy = 0 AND contacts.eid = ".$uid);
        }
        else
        {
          $vidCount = quickQuery( "select count(*) from videos as media where " . $this->getPrivacyQuery($uid, true, "", false) . " uid='" . $uid . "' AND ready>0 and on_page = 0" );
          $picCount = quickQuery( "select count(*) from photos as media left join albums on albums.id=media.aid where " . $this->getPrivacyQuery($uid) . " media.uid='" . $uid . "' AND reported=0 AND aid!=884372 and on_page = 0" ); //Don't include advertisements in the count
          $picCount += quickQuery("SELECT COUNT(*) FROM contacts INNER JOIN photo_tags ON photo_tags.cid = contacts.cid WHERE contacts.eid = ".$uid);
        }

        $albCount = quickQuery( "select count(*) from albums where uid='" . $uid . "'" );
      }

/*
			$vidCount = quickQuery("select @type:='V',count(*) from videos as media " . str_replace("@type", "'V'", $pageFilter) . " where " . ( $isPage ? "1=1 and " : $pq) . " ready=1 " . ($gid == 0 ? "and media.uid=$uid" : ""));
			$picCount = quickQuery("select @type:='P',count(*) from photos as media " . str_replace("@type", "'P'", $pageFilter) . " where " . ( $isPage ? "1=1 and " : $pq) . ($gid == 0 ? "media.uid=$uid" : "1=1"));
			$albCount = quickQuery("select @type:='P',count(*) from albums inner join photos as media on media.aid=albums.id " . str_replace("@type", "'P'", $pageFilter) . " where " . ( $isPage ? "1=1 and " : $pq) . ($gid == 0 ? "media.uid=$uid" : "1=1"));
*/
      global $site;
      if( $site == "s" )
      {
        $word = "photo";
        $type = "P";
//        $picCount += quickQuery( "select @type:='$type',count(*) from {$word}_tags left join contacts on contacts.cid={$word}_tags.cid and contacts.uid={$word}_tags.uid inner join {$word}s as media on media.id={$word}_tags.{$type}id " . ($type == "P" ? "inner join albums on media.aid=albums.id" : "") . " where " . $this->getPrivacyQuery() . " media.uid!=$uid and (({$word}_tags.uid=$uid and {$word}_tags.cid=0) or (eid=$uid))" );

        $word = "video";
        $type = "V";
  //      $vidCount += quickQuery( "select @type:='$type',count(*) from {$word}_tags left join contacts on contacts.cid={$word}_tags.cid and contacts.uid={$word}_tags.uid inner join {$word}s as media on media.id={$word}_tags.{$type}id " . ($type == "P" ? "inner join albums on media.aid=albums.id" : "") . " where " . $this->getPrivacyQuery() . " media.uid!=$uid and (({$word}_tags.uid=$uid and {$word}_tags.cid=0) or (eid=$uid))" );
      }
			return array($vidCount, $picCount, $albCount);
		}

		//get info about a page id
		function getPageInfo($gid, $addExName = false)
		{
//" . $this->getPrivacyQuery() . "  Privacy is broken
      if( intval( $gid ) == 0 ) return array();
      if( !isset( $this->uid ) ) $this->uid = 0;

      $x = sql_query("select g.type, g.contactfor, g.verified, g.fbid, g.ytid, g.twid, g.linkedin_url, g.address,g.email,g.www,g.phone,g.cell,g.subcat,g.products,g.contact_person, g.services, g.fax,g.created,m.admin,if(ifnull(m.gid,0)=0,0,1) as member,g.gid,gname,owner.uid,username,name,pic,pid,hash,g.cat,catname,g.privacy,g.descr,glink from pages g left join categories on categories.cat=g.cat natural left join (select uid from page_members where gid=$gid and admin=1 order by mid limit 1) as owner left join users on owner.uid=users.uid left join photos as media on pid=media.id left join page_members m on m.gid=$gid and m.uid='{$this->uid}' where g.gid=$gid") or die(mysql_error());

      echo mysql_error();

			if (mysql_num_rows($x) == 0)
      {
				return null;
      }
			else
			{
                $page = mysql_fetch_array($x, MYSQL_ASSOC);
                $page['gname'] = iconv('windows-1251', 'UTF-8', $page['gname']);
                $page['thumb'] = $this->getThumbURL(1, 48, 48, "/photos/{$page['pid']}/{$page['hash']}.jpg");
				$page['profile_pic'] = "/photos/{$page['pid']}/{$page['hash']}.jpg";
				$page['url'] = $this->getPageURL($page['gid'], $page['gname']);
        if( $page['type'] > 0 ) $page['pagetype'] = quickQuery( "select catname from categories where cat='" . $page['type'] . "'" );
        if( $page['subcat'] > 0 ) $page['subcatname'] = quickQuery( "select catname from categories where cat='" . $page['subcat'] . "'" );
        $page['gtype'] = $this->pageCatToGtype( $page['cat'] );

				switch ($page['type'])
				{
					case PAGE_TYPE_VESSEL: //boat
						$x = sql_query("select * from boats left join categories on cattype='B' and cat=shiptype where gid={$page['gid']}");
            if( $x )
            {
  						$page['boat'] = mysql_fetch_array($x, MYSQL_ASSOC);
              if( $addExName && $page['boat']['exnames'] != "" ) $page['gname'] .= " (ex " . $page['boat']['exnames'] . ")";
/*
		  				$x = sql_query("select ex from boats_ex where id={$page['glink']}");
	  					while ($ex = mysql_fetch_array($x, MYSQL_ASSOC))
  							$page['boat']['ex'][] = current($ex);
*/
            }
            if( $page['pid'] == 0 )
              $page['profile_pic'] = "/images/no_vessel.png";

					break;

          case PAGE_TYPE_BUSINESS:
            if( $page['pid'] == 0 )
              $page['profile_pic'] = "/images/company_default.png";
          break;

          default:
            switch( $page['cat'] )
            {
              case 1297: //Degree
              case 113: if( $page['pid'] == 0 ) $page['profile_pic'] = "/images/degree.jpg"; break;
              case 101: if( $page['pid'] == 0 ) $page['profile_pic'] = "/images/cert.png"; break;
              case 106: if( $page['pid'] == 0 ) $page['profile_pic'] = "/images/occupation.jpg"; break;
            }
          break;
				}

       $page['profilepic'] = $page['profile_pic'];

				return $page;
			}
		}

    /**
     * @param typeOfFedd $type  //C for comment, P for photo, etc.
     * @param $id               //id of the item defined by "type"
     * @param null $uid_to      //the uid for user whose feed is being posted to
     * @param null $uid_from    //the uid of the user posting on the feed
     * @param int $gid          //gid if action took place on a page
     * @return int
     */
    function feedAdd($type, $id, $uid_to = null, $uid_from = null, $gid = 0, $last_update = null)
	{
        global $site;

        if ($site == "m") return;

        if (empty($uid_to))
            $uid_to = $this->uid;

        if (empty($uid_from))
            $uid_from = $this->uid;

        $feed = array(
            "type"      => $type,
            "link"      => $id,
            "uid"       => $uid_to,
            "uid_by"    => $uid_from,
            "gid"       => $gid,
            "last_update" => $last_update
        );

        //To prevent duplicate feed entries
        if($type != typeOfFeed::VP4)
        {
            if( $uid_to == -1 && $uid_from == -1 )
            {
                $count = quickQuery( "select Count(*) from feed where type='$type' and uid=0 and uid_by=0 and link='$id' and gid='$gid' and ts > DATE(NOW())" );
            }
            else
            {
                $count = quickQuery( "select Count(*) from feed where type='$type' and uid='$uid_to' and uid_by='$uid_from' and link='$id' and gid='$gid' and ts > DATE(NOW())" );
            }

            if( $count > 0 )
            {
                return;
                //echo $count;
            }
        }
        elseif($type == typeOfFeed::VP4){
            $feed_vp4 = quickQuery( 'select `type` from feed where type in("VP1", "VP2", "VP3", "VP4") and uid=0 and uid_by=0 and gid="'.$gid.'" order by ts desc' );
            if($feed_vp4 == typeOfFeed::VP4){
                return;
            }
        }




			sql_query("insert into feed (" . implode(",", array_keys($feed)) . ") values ('" . implode("','", $feed) . "')");
			$feed['fid'] = mysql_insert_id();
      $result = $feed['fid'];
/*
			if ($feed['gid'] == 0)
			{
				//also see what pages certain types of media belong to and add to the page feeds
				$resolved = resolveFeed($feed);

        if( isset( $resolved[count($resolved) - 1]['type'] ) && isset( $resolved[count($resolved) - 1]['id'] ) )
        {
  				$x = sql_query("select gid from page_media where type='" . $resolved[count($resolved) - 1]['type'] . "' and id=" . $resolved[count($resolved) - 1]['id']);

  				unset($feed['fid']);

          if( mysql_num_rows( $x ) > 0 )
          {
    				while ($y = mysql_fetch_array($x, MYSQL_ASSOC))
    				{
    					//check to see if user is member of the page
    					if (quickQuery("select count(*) from page_members where uid={$this->uid} and gid={$y['gid']}") == 0)
    						continue;

    					$feed['gid'] = $y['gid'];
    					sql_query("insert into feed (" . implode(",", array_keys($feed)) . ") values ('" . implode("','", $feed) . "')");
    				}
          }
        }
			}
      */

        return $result;
    }
		
		//send notifications per settings/notifications page
		function sendNotification($nType, $info)
		{
			global $siteName;
      $err = "OK";
/*
			$l = fopen("/tmp/notify", "a");
			fwrite($l, "\n\n\n$nType: " . var_export($info, true));
			fclose($l);
*/

      //Skippable notification types
      $skippableTypes = array(
        NOTIFY_CMTSAME,
        NOTIFY_PHOTO_COMMENT,
        NOTIFY_VIDEO_COMMENT,
        NOTIFY_WALL_COMMENT,
        NOTIFY_RSS_COMMENT,
        NOTIFY_VIDEO_UPLOADED,
        NOTIFY_ALBUM_UPLOADED );

      //The descision to post to twitter will be made at a lower level (in $this->twUpdateStatus)
      $skiptw = false;

      //If this is a skippable type of message, then try to post to secondary twitter accounts
      $postToSecondaryTwitterAccounts = in_array( $nType, $skippableTypes );

      //Default to post to Facebook
      $skipfb = false;

      if( in_array( $nType, $skippableTypes ) )
      {
        //If the user has turned off facebook....
        if( isset( $_SESSION['postas'][SM_FACEBOOK][0] )
              && $_SESSION['postas'][SM_FACEBOOK][0] == 0 )
        {
          $skipfb = true;
        }
      }

			$notificationsEnabled = quickQuery("select notifications from users where uid='" . $this->uid . "'"); //for this user

			if ($info['uid'])
				$notificationsEnabledOwner = quickQuery("select notifications from users where uid='" . $info['uid'] . "'"); //for the media owner
			else
				$notificationsEnabledOwner = "";

      if( isset( $info['descr'] ) && ( $info['descr'] == "My miscellaneous photos" || $info['descr'] == "My profile photos" ) )
      {
        $userName = quickQuery( "select name from users where uid='" . $info['uid'] . "'" );
        $info['descr'] = str_replace( "My", $userName . "'s", $info['descr'] );
      }

			switch ($nType)
			{
				case NOTIFY_ITAG:
				    $word = typeToWord($info['type']);
                    $numNames = sizeof( $info['namesTagged'] );
                    if( $numNames <= 3 )
                        $names = implode(", ", $info['namesTagged']);
                    else
                        $names = $info['namesTagged'][0] . ", " . $info['namesTagged'][1] . " and " . ($numNames - 2) . " others";
                    //post to the user's fb/tw profiles
                    $twmsg = "tagged the $word '" . $info['title'] . "' http://shb.me/" . $info['jmp'] . " via @" . SITE_VIA . " Click link to view, reply or see more $word" . "s";
                    $fbmsg = "Tagged " . $names . " in a $word via " . $siteName . ".com http://shb.me/" . $info['jmp'] . " Click on the image to comment, share or view other great " . $word . "s.";

				if ($this->isNotificationEnabled(null, NOTIFY_PREF_FB_ITAG, $notificationsEnabled) && !$skipfb)
					if( !$this->fbStreamPublish(null, $fbmsg, $info) )
            $err = "FB";

				if ($this->isNotificationEnabled(null, NOTIFY_PREF_TW_ITAG, $notificationsEnabled) && !$skiptw)
					$this->twUpdateStatus(null, $twmsg, $postToSecondaryTwitterAccounts);

				break;

				case NOTIFY_ADDTAG_SOC:
				$word = typeToWord($info['type']);

				//post to the user's fb/tw profiles
				$twmsg = $this->name . " tagged users in my $word. http://shb.me/" . $info['jmp'] . " via @" . SITE_VIA;
				$fbmsg = $this->name . " tagged " . implode(", ", $info['namesTagged']) . " in my $word. http://shb.me/" . $info['jmp'] . " via " . $siteName . ".com";

				if ($this->isNotificationEnabled($info['uid'], NOTIFY_PREF_FB_ADDTAG, $notificationsEnabled))
					if( !$this->fbStreamPublish($info['uid'], $fbmsg) )
            $err = "FB";

				if ($this->isNotificationEnabled($info['uid'], NOTIFY_PREF_TW_ADDTAG, $notificationsEnabled))
					$this->twUpdateStatus($info['uid'], $twmsg, $postToSecondaryTwitterAccounts);//double check publish target!!

				break;

				case NOTIFY_ADDTAG:
					$word = typeToWord($info['type']);

					$subj = $this->name . " added a tag to your $word on " . $siteName;
					$msg = $subj . ".  To view the $word, please visit http://" . $_SERVER['HTTP_HOST'] . $this->getMediaURL($info['type'], $info['id'], $info['title']);

					if ($this->isNotificationEnabled($info['uid'], NOTIFY_PREF_MB_ADDTAG, $notificationsEnabledOwner))
					{
						if ($this->isNotificationEnabled($info['uid'], NOTIFY_PREF_FB, $notificationsEnabledOwner) && !$skipfb)
							if( !$this->fbSendMessage($info['uid'], $subj, $msg)  )
                $err = "FB";

						if ($this->isNotificationEnabled($info['uid'], NOTIFY_PREF_TW, $notificationsEnabledOwner) && !$skiptw)
							$this->twSendMessage($info['uid'], $msg);

						if ($this->isNotificationEnabled($info['uid'], NOTIFY_PREF_EMAIL, $notificationsEnabledOwner))
							$this->emailUser($info['uid'], $subj, $msg);
					}

					$info['notify'] = array($info['uid'], $info['type'] == "P" ? "t2" : "T2", $info['tid'],0);
				break;


				case NOTIFY_CMTSAME:
					$word = typeToWord($info['type']);
          $url ="http://" . $_SERVER['HTTP_HOST'] . $this->getMediaURL($info['type'], $info['id'], $info['title']);
          if( isset( $info['fid'] ) )
          {
            $url = getFeedJMPLink( $info['fid'] );
          }

					$subj = $this->name . " also commented on a $word on " . $siteName;
					$msg = $subj . ".  To view the $word, please visit $url";

					if ($this->isNotificationEnabled($info['uid'], NOTIFY_PREF_MB_CMTSAME, $notificationsEnabledOwner))
					{
						if ($this->isNotificationEnabled($info['uid'], NOTIFY_PREF_FB, $notificationsEnabledOwner) && !$skipfb)
							if( !$this->fbSendMessage($info['uid'], $subj, $msg) )
                $err = "FB";

						if ($this->isNotificationEnabled($info['uid'], NOTIFY_PREF_TW, $notificationsEnabledOwner) && !$skiptw)
							$this->twSendMessage($info['uid'], $msg);

						if ($this->isNotificationEnabled($info['uid'], NOTIFY_PREF_EMAIL, $notificationsEnabledOwner))
							$this->emailUser($info['uid'], $subj, $msg);
					}
				break;

				case NOTIFY_MESSAGE:
					$info['type'] = "C";
          $msg_subject = quickQuery( "select subj from messages where mid='" . $info['mid'] . "'" );
          $msg_body = quickQuery( "select comment from comments where type='M' and link='" . $info['mid'] . "' ORDER BY id desc" );

					$subj = $this->name . " sent you a message on " . $siteName;

          $targetName = quickQuery( "select name from users where uid='" . $info['uid'] . "'" );

					$msg = "Hello " . getFirstName( $targetName ) . ",<br /><br>";
          $msg .= $this->name . " sent you a message titled '" . $msg_subject . "'. <br /><br /><TABLE WIDTH=\"80%\" ALIGN=\"CENTER\" STYLE=\"background-color:#eceff2; padding:10px;\"><TR><TD><b>Message Body:</b><br />" . $msg_body . "</TD></TR></TABLE><br />To view and reply to the message, please visit http://" . $_SERVER['HTTP_HOST'] . "/messaging/read.php?mid=" . $info['mid'];
          $msg .= "<br><br>Thank you,<br />The $siteName Team";

          $twitter_message = $subj . ". To view and reply to the message, please visit http://" . $_SERVER['HTTP_HOST'] . "/messaging/read.php?mid=" . $info['mid'];

					if ($this->isNotificationEnabled($info['uid'], NOTIFY_PREF_MB_MESSAGE, $notificationsEnabledOwner))
					{
						if ($this->isNotificationEnabled($info['uid'], NOTIFY_PREF_FB, $notificationsEnabledOwner) && !$skipfb)
							if( !$this->fbSendMessage($info['uid'], $subj, $msg) )
                $err = "FB";

						if ($this->isNotificationEnabled($info['uid'], NOTIFY_PREF_TW, $notificationsEnabledOwner) && !$skiptw)
							$this->twSendMessage($info['uid'], $twitter_message);

						if ($this->isNotificationEnabled($info['uid'], NOTIFY_PREF_EMAIL, $notificationsEnabledOwner))
							$this->emailUser($info['uid'], $subj, $msg);
					}

					$info['notify'] = array($info['uid'], "M", $info['mid'],0);
				break;

        case NOTIFY_FRIEND_ACCEPT:
          $static_id = "connect_accept";
          //Fall through
				case NOTIFY_FRIEND_ADD:
          if( empty( $static_id ) )
            $static_id = "connect_request";

          $subj = "Connect with " . $this->name;
		  
		  sql_query( "insert into notifications (uid, type, from_uid) values (" . $info['uid'] . ", 'f', " . $this->uid . ")" );

          $msg = quickQuery( "select content from static where id='" . $static_id . "'" );
          $msg = str_replace( "{NAME_FROM}", $this->name, $msg );
          $msg = str_replace( "{PROFILE_URL}", "http://www." . $siteName . ".com" . $this->getProfileURL(), $msg );
          $msg = str_replace( "{ACCEPT_URL}", "http://www." . $siteName . ".com/notif_remind.php?a=1&amp;nid=" . mysql_insert_id(), $msg );
          $msg = str_replace( "{SITENAME}", $siteName, $msg );

          $msg = nl2br( $msg );


					if ($this->isNotificationEnabled($info['uid'], NOTIFY_PREF_MB_ADDFRIEND, $notificationsEnabledOwner))
					{
						if ($this->isNotificationEnabled($info['uid'], NOTIFY_PREF_FB, $notificationsEnabledOwner) && !$skipfb)
							if( !$this->fbSendMessage($info['uid'], $subj, $msg) )
                $err = "FB";

						if ($this->isNotificationEnabled($info['uid'], NOTIFY_PREF_TW, $notificationsEnabledOwner) && !$skiptw)
							$this->twSendMessage($info['uid'], $msg);

						if ($this->isNotificationEnabled($info['uid'], NOTIFY_PREF_EMAIL, $notificationsEnabledOwner))
            {
              $msg = findLinks(str_replace("  ", "&nbsp; ", $msg ));
							$this->emailUser($info['uid'], $subj, $msg);
            }
					}
				break;

        case NOTIFY_FRIEND_SUGGEST:
					$subj = $this->name . " suggested a connection on " . $siteName;

          $target_name = quickQuery( "select name from users where uid='" . $info['uid'] . "'" );

          $msg = quickQuery( "select content from static where id='friend_suggest'" );
          $msg = str_replace( "{NAME_FROM}", $this->name, $msg );
          $msg = str_replace( "{NAME}", $target_name, $msg );
          $msg = str_replace( "{SITENAME}", $siteName, $msg );

          $msg = nl2br( $msg );

					if ($this->isNotificationEnabled($info['uid'], NOTIFY_PREF_MB_ADDFRIEND, $notificationsEnabledOwner))
					{
						if ($this->isNotificationEnabled($info['uid'], NOTIFY_PREF_FB, $notificationsEnabledOwner) && !$skipfb)
							if( !$this->fbSendMessage($info['uid'], $subj, $msg) )
                $err = "FB";

						if ($this->isNotificationEnabled($info['uid'], NOTIFY_PREF_TW, $notificationsEnabledOwner) && !$skiptw)
							$this->twSendMessage($info['uid'], $msg);

						if ($this->isNotificationEnabled($info['uid'], NOTIFY_PREF_EMAIL, $notificationsEnabledOwner))
            {
              $msg = findLinks(str_replace("  ", "&nbsp; ", $msg ));
							$this->emailUser($info['uid'], $subj, $msg);
            }
					}
        break;

				case NOTIFY_VIDEO_TAGGED:
				case NOTIFY_PHOTO_TAGGED:
					$media = typeToWord($info['type']);

          $numNames = sizeof( $info['namesTagged'] );
          if( $numNames <= 3 )
            $names = implode(", ", $info['namesTagged']);
          else
            $names = $info['namesTagged'][0] . ", " . $info['namesTagged'][1] . " and " . ($numNames - 2) . " others";

          $info['jmp'] = str_replace( "\n", "", $info['jmp'] );

  				$twmsg = "was tagged in a $word '" . $info['title'] . "' http://shb.me/" . $info['jmp'] . " via @" . SITE_VIA . " Click link to view, reply or see more $word" . "s";
//					$twmsg = "was tagged in a $media on $siteName. http://shb.me/". $info['jmp'] . " via @" . SITE_VIA;

					$emailsubj = $this->name . " tagged you in a $media on $siteName";

          $name_array = $this->getUserInfo($info['uid'], "name");
          if( empty( $name_array[0] ) )
            return;

					$emailbody = "Hi " . $name_array[0] . ",<p />" . $this->name . " tagged you in the $media &quot;" . $info['title'] . ".&quot;<p />To see the $media, follow the link below:<br /><a href=\"" . $info['url'] . "\">" . $info['url'] . "</a><p />Thanks,<br />The $siteName Team";

					if ($this->isNotificationEnabled($info['uid'], NOTIFY_PREF_TW_TAGGED, $notificationsEnabled) && !$skiptw)
						$this->twUpdateStatus($info['uid'], $twmsg, $postToSecondaryTwitterAccounts);

  				$socmsg = "Tagged " . $names . " in a $word via " . $siteName . ".com http://shb.me/" . $info['jmp'] . " Click on the image to comment, share or view other great " . $word . "s";

					if ($this->isNotificationEnabled($info['uid'], NOTIFY_PREF_FB_TAGGED, $notificationsEnabled) && !$skipfb)
						if( !$this->fbStreamPublish(null, $socmsg, $info) )
              $err = "FB";

					//email user
					if ($this->isNotificationEnabled($info['uid'], NOTIFY_PREF_MB_TAGGED, $notificationsEnabledOwner))
					{
						if ($this->isNotificationEnabled(null, NOTIFY_PREF_FB, $notificationsEnabledOwner))
							if( !$this->fbSendMessage($info['uid'], $emailsubj, $emailbody) )
                $err = "FB";

						if ($this->isNotificationEnabled(null, NOTIFY_PREF_TW, $notificationsEnabledOwner))
							$this->twSendMessage($info['uid'], $emailbody);

						if ($this->isNotificationEnabled($info['uid'], NOTIFY_PREF_EMAIL, $notificationsEnabledOwner))
							$this->emailUser($info['uid'], $emailsubj, $emailbody);
					}

					$info['notify'] = array($info['uid'], $info['type'] == "P" ? "t" : "T", $info['tid'],0);
				break;

				case NOTIFY_VIDEO_UPLOADED:
				case NOTIFY_ALBUM_UPLOADED:
					$twmsg = "Shared the " . ($info['type'] == "V" ? "video" : ($info['type'] == "A" ? "album" : "photo")) . " '" . $info['title'] . "' http://shb.me/" . $info['jmp'] . " via @" . SITE_VIA . " Click link to view, reply or see more " . ($info['type'] == "V" ? "videos" : ($info['type'] == "A" ? "albums" : "photos"));

//					$fbmsg = "Click on the " . ($info['type'] == "V" ? "video" : ($info['type'] == "A" ? "album" : "photo")) . " to comment, share or view other great " . ($info['type'] == "V" ? "videos" : ($info['type'] == "A" ? "albums" : "photos"));
					$fbmsg = "Shared this " . ($info['type'] == "V" ? "video" : ($info['type'] == "A" ? "album" : "photo"));
          if( $info['gid'] > 0 )
          {
            $page = $this->getPageInfo( $info['gid'] );
            if( sizeof( $page ) > 0 )
              $fbmsg .= " with the '" . $page['gname'] . "' page,";
          }

          $fbmsg .= " via $siteName" . ".com http://shb.me/" . $info['jmp'] . " Click on the image to comment, share or view other great " . ($info['type'] == "V" ? "videos." : ($info['type'] == "A" ? "albums." : "photos."));

//					if ($this->isNotificationEnabled(null, NOTIFY_PREF_FB_UPLOAD, $notificationsEnabled) && !$skipfb)
          if( !$skipfb ) // NOTIFY_PREF_FB_UPLOAD was removed from the notification settings page as the user can now choose when posting a comment or uploading
						if( !$this->fbStreamPublish(null, $fbmsg, $info, $info['fbtarget'] ) )
              $err = "FB";

//					if ($this->isNotificationEnabled(null, NOTIFY_PREF_TW_UPLOAD, $notificationsEnabled) && !$skiptw)
          if( !$skiptw )
						$this->twUpdateStatus(null, $twmsg, $postToSecondaryTwitterAccounts);

					if ($this->isNotificationEnabled(null, NOTIFY_PREF_MB_LIKE, $notificationsEnabled))
						$this->twUpdateStatus(null, $twmsg, $postToSecondaryTwitterAccounts);

				break;

        case NOTIFY_FEED_LIKE:

          $title = $info['title'];
          $media = "log book post";

          $info['descr'] = cutOffText( strip_tags( $info['descr'] ), 150 );

					//post to the user's fb/tw profiles
          $url = getFeedJMPLink( $info['id'] );

          if( $info['type'] == "rss" || $info['type'] == "r" )
          {
            $media = "rss feed";

            $info["href"] = "http://" . $_SERVER['HTTP_HOST'] . "/profile/feed.php?fid=" . $info['id'];
            $info['type'] = "P"; //For the facebook like.
            $info['oid'] = $info['id'];
            $info['id'] = $info['pid'];
          }

					$msg = "" . ($info['like'] == 0 ? "dis" : "") . "likes this $media. $url via @" . SITE_VIA;

					if ($this->isNotificationEnabled(null, NOTIFY_PREF_TW_LIKE, $notificationsEnabled))
						$this->twUpdateStatus(null, $msg, $postToSecondaryTwitterAccounts);

					$msg = "" . ($info['like'] == 0 ? "dis" : "") . "likes this $media $title $url via @" . $siteName . ".com";
					$fbmsg = "Likes this $media $title $url via $siteName.com Click on the link to comment, share or view other great $media" . "s";

					if ($this->isNotificationEnabled(null, NOTIFY_PREF_FB_LIKE, $notificationsEnabled) )
            if( !$this->fbStreamPublish(null, $fbmsg, $info) )
              $err = "FB";

          unset( $info['title'] );


					//notify the media owner
          if( $info['uid'] > 0 )
          {
  					$subj = $this->name . " " . ($info['like'] == 0 ? "dis" : "") . "likes your $media @" . $siteName . ".com";
  					$msg = $subj . "  Check it out at $url";
  					if ($this->isNotificationEnabled($info['uid'], NOTIFY_PREF_MB_LIKE, $notificationsEnabledOwner))
  					{
  						if ($this->isNotificationEnabled($info['uid'], NOTIFY_PREF_FB, $notificationsEnabledOwner))
              {
                if( !$this->fbSendMessage($info['uid'], $subj, $msg) )
                  $err = "FB";
              }

  						if ($this->isNotificationEnabled($info['uid'], NOTIFY_PREF_TW, $notificationsEnabledOwner))
  							$this->twSendMessage($info['uid'], $msg);

  						if ($this->isNotificationEnabled($info['uid'], NOTIFY_PREF_EMAIL, $notificationsEnabledOwner))
  							$this->emailUser($info['uid'], $subj, $msg);
  					}
  					$info['notify'] = array($info['uid'], $info['type'], $info['oid'],0);
          }
        break;

				case NOTIFY_VIDEO_LIKE:
					$media = "video";
				case NOTIFY_PHOTO_LIKE:
					if (empty($media))
						$media = "photo";

					//post to the user's fb/tw profiles
					$msg = "" . ($info['like'] == 0 ? "dis" : "") . "likes: " . substr($info['descr'], 0, 92) . ". http://shb.me/". $info['jmp'] . " via @" . SITE_VIA;

					if ($this->isNotificationEnabled(null, NOTIFY_PREF_TW_LIKE, $notificationsEnabled))
						$this->twUpdateStatus(null, $msg, $postToSecondaryTwitterAccounts);

					$msg = "" . ($info['like'] == 0 ? "dis" : "") . "likes: " . substr($info['title'], 0, 92) . ". http://shb.me/". $info['jmp'] . " via " . $siteName . ".com";
					$fbmsg = "Likes this $media via $siteName.com http://shb.me/". $info['jmp'] . " Click on the image to comment, share or view other great $media" . "s";

					if ($this->isNotificationEnabled(null, NOTIFY_PREF_FB_LIKE, $notificationsEnabled) )
            if( !$this->fbStreamPublish(null, $fbmsg, $info) )
              $err = "FB";

					//notify the media owner
					$subj = $this->name . " " . ($info['like'] == 0 ? "dis" : "") . "likes your $media @" . $siteName . ".com";
					$msg = $subj . "  Check it out at http://shb.me/". $info['jmp'];
					if ($this->isNotificationEnabled($info['uid'], NOTIFY_PREF_MB_LIKE, $notificationsEnabledOwner))
					{
						if ($this->isNotificationEnabled($info['uid'], NOTIFY_PREF_FB, $notificationsEnabledOwner))
            {
              if( !$this->fbSendMessage($info['uid'], $subj, $msg) )
                $err = "FB";
            }

						if ($this->isNotificationEnabled($info['uid'], NOTIFY_PREF_TW, $notificationsEnabledOwner))
							$this->twSendMessage($info['uid'], $msg);

						if ($this->isNotificationEnabled($info['uid'], NOTIFY_PREF_EMAIL, $notificationsEnabledOwner))
							$this->emailUser($info['uid'], $subj, $msg);
					}

					$info['notify'] = array($info['uid'], $info['type'], $info['id'],0);
				break;


        case NOTIFY_RSS_COMMENT:
          $fbinfo = array (
            "title" => $info['title'],
            "descr" => cutOffText( strip_tags( $info['descr'] ), 150 ),
            "type" => "P",
            "id" => $info['pid'],
            "href" => "http://" . $_SERVER['HTTP_HOST'] . "/profile/feed.php?fid=" . $info['fid']
          );

          $msg = "Replied to " . substr($info['title'], 0, 87) . " by " . substr($info['original_poster'], 0, 87) . " - http://shb.me/". $info['jmp'] . " via @" . SITE_VIA;

        case NOTIFY_WALL_COMMENT:
          $media = "post";
          $info['type'] = "W";
          $info['notify']['type'] = "C";

          $url = "http://shb.me/". $info['jmp'];
          if( isset( $info['fid'] ) )
          {
            $url = getFeedJMPLink( $info['fid'] );
          }

					//post to the user's fb/tw profiles
          if( empty( $msg ) )
  					$msg = "Replied to " . substr($info['original_poster'], 0, 87) . "'s Wall Post, " . cutOffText($info['title'], 87) . " $url via @" . SITE_VIA;

//					if ($this->isNotificationEnabled(null, NOTIFY_PREF_FB_COMMENT, $notificationsEnabled) && !$skipfb)
          if( !$skipfb )
						if( !$this->fbStreamPublish(null, $msg, $fbinfo, $info['fbtarget']) )
              $err = "FB";

//					if ($this->isNotificationEnabled(null, NOTIFY_PREF_TW_COMMENT, $notificationsEnabled) && !$skiptw)
          if( !$skiptw )
						$this->twUpdateStatus(null, $msg, $postToSecondaryTwitterAccounts);
/* untested code, copied from video comments
					//notify the media owner
					$subj = $this->name . " replied to your $media @" . SITE_VIA;
					$msg = $subj . "  Check it out at http://shb.me/". $info['jmp'];
					if ($this->isNotificationEnabled($info['uid'], NOTIFY_PREF_MB_COMMENT, $notificationsEnabledOwner))
					{
						if ($this->isNotificationEnabled($info['uid'], NOTIFY_PREF_FB, $notificationsEnabledOwner))
							$this->fbSendMessage($info['uid'], $subj, $msg);

						if ($this->isNotificationEnabled($info['uid'], NOTIFY_PREF_TW, $notificationsEnabledOwner))
							$this->twSendMessage($info['uid'], $msg);

						if ($this->isNotificationEnabled($info['uid'], NOTIFY_PREF_EMAIL, $notificationsEnabledOwner))
							$this->emailUser($info['uid'], $subj, $msg);
					}
*/
				break;

				case NOTIFY_VIDEO_COMMENT:
					if (empty($media))
					{
  					$media = "video";
  					$info['type'] = "V";
          }
				case NOTIFY_PHOTO_COMMENT:
					if (empty($media))
					{
						$media = "photo";
						$info['type'] = "P";
					}

          if( isset( $info['id'] ) ) $info['notify']['id'] = $info['id'];

					//post to the user's fb/tw profiles
					$msg = "Replied to: " . substr($info['descr'], 0, 87) . ". http://shb.me/". $info['jmp'] . " via @" . SITE_VIA;

					if ($this->isNotificationEnabled(null, NOTIFY_PREF_TW, $notificationsEnabled) && !$skiptw)
						$this->twUpdateStatus(null, $msg, $postToSecondaryTwitterAccounts);

					$msg = "Replied to this video via " . $siteName . ".com http://shb.me/". $info['jmp'] . " Click on the image to comment, share or view other great " . $media . "s";

					if ($this->isNotificationEnabled(null, NOTIFY_PREF_FB, $notificationsEnabled) && !$skipfb)
						if( !$this->fbStreamPublish(null, $msg, $info, $info['fbtarget']) )
              $err = "FB";

					//notify the media owner
					$subj = $this->name . " replied to your $media @" . SITE_VIA;
					$msg = $subj . "  Check it out at http://shb.me/". $info['jmp'];
					if ($this->isNotificationEnabled($info['uid'], NOTIFY_PREF_MB_COMMENT, $notificationsEnabledOwner))
					{
//						if ($this->isNotificationEnabled($info['uid'], NOTIFY_PREF_FB, $notificationsEnabledOwner))
//							$this->fbSendMessage($info['uid'], $subj, $msg);

						if ($this->isNotificationEnabled($info['uid'], NOTIFY_PREF_TW, $notificationsEnabledOwner))
							$this->twSendMessage($info['uid'], $msg);

						if ($this->isNotificationEnabled($info['uid'], NOTIFY_PREF_EMAIL, $notificationsEnabledOwner))
							$this->emailUser($info['uid'], $subj, $msg);
					}

				break;

        case NOTIFY_GROUP_ADD:
          if( $this->isNotificationEnabled( null, NOTIFY_PREF_MB_PAGE_UPDATE ) )
            sql_query( "insert into notifications (uid, type, id, from_uid) values (" . $info['uid'] . ", 'G', " . $info['gid'] . ", " . $info['from'] . ")" );
        break;

        case NOTIFY_GROUP_COMMENT:
          if( $this->isNotificationEnabled( null, NOTIFY_PREF_MB_PAGE_UPDATE ) )
            sql_query( "insert into notifications (uid, type, id, from_uid) values (" . $info['uid'] . ", 'G1', " . $info['gid'] . ", " . $info['from'] . ")" );
        break;

        case NOTIFY_GROUP_NEW_CONNECT:
          if( $this->isNotificationEnabled( null, NOTIFY_PREF_MB_PAGE_UPDATE ) )
            sql_query( "insert into notifications (uid, type, id, from_uid) values (" . $info['uid'] . ", 'G2', " . $info['gid'] . ", " . $info['from'] . ")" );
        break;

        case NOTIFY_GROUP_NEW_VID:
          if( $this->isNotificationEnabled( null, NOTIFY_PREF_MB_PAGE_UPDATE ) )
            sql_query( "insert into notifications (uid, type, id, id2, from_uid) values (" . $info['uid'] . ", 'GV', " . $info['gid'] . ", " . $info['id'] . ", " .$info['from'] . ")" );
        break;

        case NOTIFY_GROUP_NEW_PHOTO:
          if( $this->isNotificationEnabled( null, NOTIFY_PREF_MB_PAGE_UPDATE ) )
            sql_query( "insert into notifications (uid, type, id, id2, from_uid) values (" . $info['uid'] . ", 'GP', " . $info['gid'] . ", " . $info['id'] . ", " .$info['from'] . ")" );
        break;

        case NOTIFY_GROUP_ADMIN:
					$subj = "Page Notification on SaltHub";

          $target_name = quickQuery( "select name from users where uid='" . $info['uid'] . "'" );
          $page_name = quickQuery( "select gname from pages where gid='" . $info['gid'] . "'" );
          $url = "http://" . $siteName . $this->getPageURL( $info['gid'] );

          $msg = quickQuery( "select content from static where id='page_admin'" );
          $msg = str_replace( "{PAGE_URL}", "<a href=\"" . $url . "\">" . $url . "</a>", $msg );
          $msg = str_replace( "{PAGE}", $page_name, $msg );
          $msg = str_replace( "{NAME}", $target_name, $msg );
          $msg = str_replace( "{SITENAME}", $siteName, $msg );

          $msg = nl2br( $msg );

          $this->emailUser($info['uid'], $subj, $msg);

          if( $this->isNotificationEnabled( null, NOTIFY_PREF_MB_PAGE_UPDATE ) )
            sql_query( "insert into notifications (uid, type, id, from_uid) values (" . $info['uid'] . ", 'G3', " . $info['gid'] . ", " .$info['from'] . ")" );
        break;

        case NOTIFY_GROUP_CLAIMED:
          if( $this->isNotificationEnabled( null, NOTIFY_PREF_MB_PAGE_UPDATE ) )
            sql_query( "insert into notifications (uid, type, id, from_uid) values (" . $info['uid'] . ", 'G4', " . $info['gid'] . ", " .$info['from'] . ")" );
        break;

        case NOTIFY_GROUP_JOB_POSTED:
          if( $this->isNotificationEnabled( null, NOTIFY_PREF_MB_PAGE_UPDATE ) )
            sql_query( "insert into notifications (uid, type, id, id2, from_uid) values (" . $info['uid'] . ", 'G5', " . $info['gid'] . ", " . $info['id'] . ", " . $info['from'] . ")" );
        break;

        case NOTIFY_VESSEL_NAVSTAT_CHANGE:
          if( $this->isNotificationEnabled( null, NOTIFY_PREF_MB_PAGE_UPDATE ) )
            sql_query( "insert into notifications (uid, type, id, id2, from_uid) values (" . $info['uid'] . ", 'VP1', " . $info['gid'] . ", 0, " .$info['from'] . ")" );
        break;

        case NOTIFY_VESSEL_NAME_CHANGE:
          if( $this->isNotificationEnabled( null, NOTIFY_PREF_MB_PAGE_UPDATE ) )
            sql_query( "insert into notifications (uid, type, id, id2, from_uid) values (" . $info['uid'] . ", 'VP2', " . $info['gid'] . ", 0, " .$info['from'] . ")" );
        break;

        case NOTIFY_VESSEL_SPEC_CHANGE:
          if( $this->isNotificationEnabled( null, NOTIFY_PREF_MB_PAGE_UPDATE ) )
            sql_query( "insert into notifications (uid, type, id, id2, from_uid) values (" . $info['uid'] . ", 'VP3', " . $info['gid'] . ", 0, " .$info['from'] . ")" );
        break;

        case NOTIFY_VESSEL_OUT_OF_RANGE:
          if( $this->isNotificationEnabled( null, NOTIFY_PREF_MB_PAGE_UPDATE ) )
            sql_query( "insert into notifications (uid, type, id, id2, from_uid) values (" . $info['uid'] . ", 'VP4', " . $info['gid'] . ", 0, " .$info['from'] . ")" );
        break;

			}

			if (isset($info['notify']) && $info['notify']['uid'] != $this->uid)
      {
        if( sizeof( $info['notify'] ) == 3 )
  				sql_query("insert into notifications (uid,type,id) values ('" . implode("','", $info['notify']) . "')");
        else
  				sql_query("insert into notifications (uid,type,id,from_uid) values ('" . implode("','", $info['notify']) . "')");
      }

      return $err;
		}

		//sends email to a user
		function emailUser($uid = null, $subj, $msg, $from = "notifications")
		{
			if ($uid == null)
				$uid = $this->uid;

			$email = quickQuery("select email from users where verify=1 and uid=$uid");

			if (!empty($email))
				emailAddress($email, $subj, $msg, $from);
		}

		//sends private message to a user
		function sendMessage($uid, $subj, $body, $from = null, $notify = true)
		{
			if( empty($from) ) $from = $this->uid;
			
			$real_uid = $this->uid;
			$this->uid = $from;

			sql_query("insert into messages (unreadByRecv,uid,uid_to,subj) values (" . implode(",", array(1, $from, $uid, "'" . $subj . "'")) . ")");
			$mid = mysql_insert_id();

			sql_query("insert into comments (type,link,comment,uid) values (" . implode(",", array("'M'", $mid, "'" . addslashes($body) . "'", $from)) . ")");
			
			

      if( $notify )
  			$this->sendNotification(NOTIFY_MESSAGE, array("uid" => $uid, "mid" => $mid));

			$this->uid = $real_uid;
			return $mid;
		}

		//sends an email thru twitter to a user
		function twSendMessage($uid, $message, $twuser = null)
		{
			if ($uid == null)
				$uid = $this->uid;

			$twitter = $this->startTWSession($uid);

			if (empty($twuser))
				$twuser = $twitter->user;

      if( strlen( $message ) > 140 ) $message = substr( $message, 0, 139 );

			if ($twitter) return $twitter->post("direct_messages/new", array("user" => $twuser, "text" => htmlToText($message) ));
      else return false;
		}

		//sends an email thru twitter to a user
		function twFollow($twid, $twname)
		{
			$twitter = $this->startTWSession($uid);

			if ($twitter) return $twitter->post("friendships/create", array("id" => $twid, "user_id" => $twid, "screen_name" => $twname ));
		}

		//sends an email thru fb to a user
		function fbSendMessage($uid, $subject, $message)
		{
			if ($uid == null)
				$uid = $this->uid;

      //This has been disabled as we shoud have all users email addresses; so they will get an email directly

//			$facebook = $this->startFBSession($uid);
//      $this->fbSafeCallMethod($uid, $facebook, "facebook.notifications.sendEmail", array("recipients" => array($facebook->getUser() ), "subject" => htmlToText($subject), "text" => htmlToText($message)));
			return true;
		}

    //Executes an FQL query
    function fbFQLQuery( $fql )
    {
      $facebook = $this->startFBSession($uid);
      if( $facebook )
      {
        $token = $facebook->getAccessToken();

        $fql_query_url = 'https://graph.facebook.com'
          . '/fql?q=' . urlencode( $fql )
          . '&access_token=' . $token;

        $result = file_get_contents($fql_query_url);

        return json_decode($result, true);
      }
      else
        return null;
    }

		//facebook call_method wrapper with error handling
		function fbSafeCallMethod($uid, $facebook, $method, $params)
		{
			$result = null;

      if( empty( $this->fbid ) || $this->fbid == "" )
        return; //Don't bother trying to post to FB if we don't have an FB account!

			if ($facebook)
				try
				{
					// $result = $facebook->api_client->call_method($method, $params);
					$params['method'] = $method;

          return $facebook->api($params);;
				}
				catch (Exception $e)
				{
          $result = "";

          if( $this->admin )
          {
            $result = $e->getMessage();
            var_dump($result);
          }

          return false;
				}
			else
				return false;

			return true;
		}

		//alias for fbSafeCallMethod
		function fbCallMethod($uid, $facebook, $method, $params)
		{
			return $this->fbSafeCallMethod($uid, $facebook, $method, $params);
		}

		//updates facebook status for a user
		function fbUpdateStatus($uid, $message)
		{
			if ($uid == null)
				$uid = $this->uid;

			$facebook = $this->startFBSession($uid);
			return $this->fbSafeCallMethod($uid, $facebook, "facebook.stream.publish", array("message" => $message));
		}
		
		//posts a tweet for a user
		function twUpdateStatus($uid, $message, $postToSecondary = false )
		{
			if ($uid == null)
				$uid = $this->uid;

      $skip = false;

      if( isset( $_SESSION['postas'][SM_TWITTER][0] )
            && $_SESSION['postas'][SM_TWITTER][0] == 0 )
        $skip = true;

      //Post to primary account
      if( !$skip )
      {
  			$twitter = $this->startTWSession($uid);
	  		if ($twitter) $twitter->post("statuses/update", array("status" => $message));
      }

      if( $postToSecondary )
      {
        //Look in secondary accounts
        $sql = "SELECT * FROM social_media_accounts WHERE uid='" . $this->uid . "' AND site=" . SM_TWITTER;
        $q = mysql_query( $sql );
        while( $r = mysql_fetch_array( $q ) )
        {
          $send = true;
          if( isset( $_SESSION['postas'][SM_TWITTER][$r['id']] ) && $_SESSION['postas'][SM_TWITTER][$r['id']] == 0 )
            $send = false;

          if( $send )
          {
            // Setup connection
            $twitter = $this->startTWSession($this->uid, $r['site_id'], $r['token'], $r['secret'] );

            // Post to account
            if ($twitter)
            {
              $twitter->post("statuses/update", array("status" => $message));
            }
          }
        }
      }
		}

		//facebook steam.publish wrapper
		function fbStreamPublish($uid, $message, $info = null, $tofbid = null)
		{
			global $siteName;

			if ($uid == null)
				$uid = $this->uid;

			$facebook = $this->startFBSession($uid);

      if( !$facebook ) return true; //This user doesn't have a facebook account, don't return a facebook error.

			if ($tofbid == null)
      {
				$tofbid = $this->fbid;
        if( is_null( $this->fbid ) )
          return true;
      }

      $fbuser = quickQuery( "select fbusername from users where uid='$uid'" );
      if( empty( $fbuser ) || $fbuser == "" ) return 0; //target user is not connected to facebook

			if ($facebook)
			{
				try
				{
					if (empty($info) && !is_array( $info ) )
					{
						$action_links[0]['text'] = "Visit " . $siteName;
						$action_links[0]['href'] = "http://" . $_SERVER['HTTP_HOST'];
						return $this->fbSafeCallMethod($uid, $facebook,
							'facebook.stream.publish',
							array('message' => $message,
								'action_links' => json_encode($action_links),
								'uid' => $tofbid
								  )
							);
					}
					else
					{

            if( empty( $info['href'] ) )
  						$url = "http://" . $_SERVER['HTTP_HOST'] . $this->getMediaURL($info['type'], $info['id'], $info['title']);
            else
              $url = $info['href'];

						$action_links[0]['text'] = "View media";
						$action_links[0]['href'] = $url;

						$attachment['media'][0]['type'] = "image";
						$attachment['media'][0]['src'] = "http://" . $_SERVER['HTTP_HOST'] . "/img/100x75" . $this->getMediaThumb($info['type'], $info['type'] == "A" ? $info['mainimage'] : $info['id']);
						$attachment['media'][0]['href'] = $url;
						$attachment['name'] = $info['title'];
						$attachment['href'] = $url;
						$attachment['caption'] = $info['descr'];
						return $this->fbSafeCallMethod($uid, $facebook,
							'facebook.stream.publish',
              array('message' => $message,
								  'uid' => $tofbid,
								  'attachment' => json_encode($attachment),
								  'action_links' => json_encode($action_links)
								  )
							);
					}
				}
				catch (Exception $e)
				{
					if ($this->admin == 1) die("Exception 2");
				}
			}
			else
				return "No fb";
		}

		//gets info about a uid
		function getUserInfo($uid = null, $fields = "*")
		{
			if (empty($uid))
				$uid = $this->uid;

			$x = sql_query("select $fields from users where uid=$uid limit 1");

      if( mysql_error() )
      {
        return null;
      }

			if (mysql_num_rows($x) > 0)
				while ($y = mysql_fetch_array($x, MYSQL_ASSOC))
					return $y;
		}

		//gets photos from an album
		function getAlbumPhotos($aid, $limit = 0)
		{
			$x = sql_query("select * from (select if(media.id=albums.mainimage,1,0) as isMain,width,height,media.id,hash,if(ifnull(ptitle,'')='',title,ptitle) as title,if(ifnull(pdescr,'')='',descr,pdescr) as descr from photos as media inner join albums on albums.id=media.aid where " . $this->getPrivacyQuery() . " aid='$aid'" . ($limit ? " limit $limit" : "") . ") as tmp order by isMain desc");

			while ($y = mysql_fetch_array($x, MYSQL_ASSOC))
				$photos[] = $y;
			
			return $photos;
		}

		//gets info about media
		function getMediaInfo($type, $id, $fields = null)
		{
      if( empty( $fields ) )
      {
        $fields = "title,id";
      }

  		if( empty($id) ) return;

			switch ($type)
			{
				case "V":
				$q = "select $fields from videos where id=$id limit 1";
				break;

				case "P":
				$q = "select $fields from photos left join albums on albums.id=photos.aid where photos.id=$id limit 1";
				break;

				case "A":
				$q = "select $fields from albums inner join photos on mainimage=photos.id where albums.id=$id limit 1";
				break;

        case "r":
        case "rss":
				$q = "select $fields from rss_feed where id='$id' limit 1";
        break;

        case "W":
				$q = "select post as title, sid as id from wallposts where sid='$id'";
        break;
			}
//      if( $this->uid == 9581 ) echo $q ;

			$x = sql_query($q);
			$rows = mysql_num_rows($x);

			if ($rows > 0)
				while ($y = mysql_fetch_array($x, MYSQL_ASSOC))
					return $y;
		}
		
		//gets the thumbnail url for a given media
		function getMediaThumb($type, $id, $hash = null)
		{
			switch ($type)
			{
				case "A":
				case "P":
				if ($hash == null) $hash = quickQuery("select hash from photos where id='$id'");
				return "/photos/$id/$hash.jpg";
				break;

				case "V":
				if ($hash == null) $hash = quickQuery("select hash from videos where id='$id'");
				return "/videos/$id/$hash.jpg";
				break;
			}
		}

		//checks to see if a user has a certain notification enabled as defined in const.php
		function isNotificationEnabled($uid = null, $nType, $notificationsEnabled = null)
		{
			if ($uid == null)
				$uid = $this->uid;

			if ($notificationsEnabled == null)
				$notificationsEnabled = quickQuery("select notifications from users where uid='$uid'");

			if ($notificationsEnabled == "*")
				return true;
			else
			{
				//first check to see if notifications are enabled per site
				if ($nType > NOTIFY_PREF_FB && $nType < NOTIFY_PREF_TW) //facebook
					if (strpos($notificationsEnabled, "," . NOTIFY_PREF_FB . ",") === false)
						return false;
				
				if ($nType > NOTIFY_PREF_TW && $nType < NOTIFY_PREF_MB) //twitter
					if (strpos($notificationsEnabled, "," . NOTIFY_PREF_TW . ",") === false)
						return false;

				if ($nType > NOTIFY_PREF_MB && $nType < NOTIFY_PREF_MY) //email
					if (strpos($notificationsEnabled, "," . NOTIFY_PREF_EMAIL . ",") === false)
						return false;

				return strpos($notificationsEnabled, ",$nType,") !== false;
			}
		}
		
		//starts a twitter session
		function startTWSession($uid = null, $twid = 0, $twtoken = null, $twsecret = null )
		{
			if ($uid == null)
				$uid = $this->uid;

      if( $twid > 0 )
      {
        $info = array( 'twid' => $twid, 'twtoken' => $twtoken, 'twsecret' => $twsecret );
      }
      else
      {
  			$x = sql_query("select twid,twtoken,twsecret from users where uid=$uid");
  			$info = mysql_fetch_array($x, MYSQL_ASSOC);
      }

			include_once $_SERVER["DOCUMENT_ROOT"] . "/twitter/twitteroauth/twitteroauth.php";

      if( is_null( $info['twtoken'] ) || is_null( $info['twsecret']) || $info['twtoken'] == "" || $info['twsecret'] == "" )
      {
        return false;
      }

			$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $info['twtoken'], $info['twsecret']);
			$connection->user = $info['twid'];

			return $connection;
		}

		//gets the twitter user info from a mediabirdy user that's displayed on the profile page
		function getTWUserInfo($uid = null, $forceRefresh = false)
		{
			if ($uid == null)
				$uid = $this->uid;
      if( $uid == null )
        return;
			$x = sql_query("select lastupdated,twfollowers as followers,twfollowing as following,twweb as web,twbio as bio,twloc as location from users where uid=$uid");
			$twinfo = mysql_fetch_array($x, MYSQL_ASSOC);
			
			$twinfo['lastupdated'] = strtotime($twinfo['lastupdated']);

			//returned cached information
			return $twinfo;
		}

		//starts a facebook session
		function startFBSession($uid = null, $token = null)
		{
      global $site, $facebook;

			if ($uid == null)
				$uid = $this->uid;

			include_once $_SERVER["DOCUMENT_ROOT"] . "/fbconnect/facebook.php";

			global $fbSecret, $fbAppId;

			$facebook = new Facebook(array(
			  'appId'  => $fbAppId,
			  'secret' => $fbSecret,
			));

      if( $token != null )
      {
        $facebook->setAccessToken( $token );
        $_SESSION['fbaccess_token'] = $token;
      }
      elseif( isset( $_SESSION['fbaccess_token'] ) )
      {
        $facebook->setAccessToken( $_SESSION['fbaccess_token'] );
      }
      elseif( isset( $uid ) )
      {
        $token = quickQuery( "select fbaccesstoken from users where uid='" . $uid . "'" );

        if( $token != "" )
        {
          $facebook->setAccessToken( $token );
          $_SESSION['fbaccess_token'] = $token;
        }
        else
        {
          return null;
        }
      }
      else
      {
        return null;
      }

			return $facebook;

		}

		//gets mutual friends between this user and another uid
		function getMutualFriends($uid)
		{
			$mutual = array();

      $myFriends = $this->getFriendsUids();
      $theirFriends = $this->getFriendsUids($uid);

      return array_intersect( $myFriends, $theirFriends );
/*
			$x = sql_query("select uid,username,name,pic from friends inner join users on users.uid=if(id1=$uid,id2,id1) where $uid in (id1,id2) and if(id1=$uid,id2,id1) in (select if(id1=" . $this->uid . ",id2,id1) from friends where " . $this->uid . " in (id1,id2))") or die(mysql_error());
			while ($y = mysql_fetch_array($x, MYSQL_ASSOC))
				$mutual[] = $y;

			return $mutual;
*/
		}

		function getMutualFriendsWithPage($uid, $gid)
		{
			$mutual = array();

			$x = sql_query("select uid,username,name,pic from friends inner join users on users.uid=if(id1=$uid,id2,id1) where $uid in (id1,id2) and if(id1=$uid,id2,id1) in (select uid from page_members where gid='" . $gid . "')") or die(mysql_error());
			while ($y = mysql_fetch_array($x, MYSQL_ASSOC))
				$mutual[] = $y;

			return $mutual;
		}

		//gets a list of friends
		function getFriends($uid = null, $order = "name", $limit = 0, $status = 1)
		{
      $friends = array();

      if ($uid == null)
				$uid = $this->uid;

			$x = sql_query("select if(id1=$uid,id2,id1) as uid,username,name,pic from friends inner join users on users.uid=if(id1=$uid,id2,id1) where (id1=$uid or id2=$uid) and status=$status and users.active=1 order by $order" . ($limit > 0 ? " limit $limit" : "") );

			while ($y = mysql_fetch_array($x, MYSQL_ASSOC))
				$friends[] = $y;

			return $friends;
		}

		//gets a list of friends
		function getFriendsUids($uid = null )
		{
			if ($uid == null)
				$uid = $this->uid;

      $friends = array();

			$x = sql_query("select if(id1=$uid,id2,id1) as uid from friends where (id1=$uid or id2=$uid) and status=1");

			while ($y = mysql_fetch_array($x, MYSQL_ASSOC))
				$friends[] = $y['uid'];

			return $friends;
		}

		//gets members of a page
		function getPageMembers($gid, $order = "name", $limit = null)
		{
			$x = sql_query("select u.uid,username,name,pic,gm.admin from page_members gm inner join users u on gm.uid=u.uid where gid=$gid and u.active=1 order by $order" . (empty($limit) ? "" : " limit $limit"));
			
			while ($y = mysql_fetch_array($x, MYSQL_ASSOC))
				$gm[] = $y;

			return $gm;
		}

		//counts the number of members in a page
		function getPageMembersCount($gid)
		{
			return quickQuery("select count(*) from page_members left join users on users.uid=page_members.uid where page_members.gid=$gid and users.active=1");
		}		

		//counts the number of friends for a user
		function getFriendsCount($uid = null)
		{
			if ($uid == null)
				$uid = $this->uid;

			return quickQuery("select count(*) from friends left join users on users.uid=if(id1=$uid,id2,id1) where (id1=$uid or id2=$uid) and status=1 and users.active=1");
		}

		//counts the number of friends for a user
		function getFriendRequestCount($uid = null)
		{
			if ($uid == null)
				$uid = $this->uid;

      $count = 0;
			$q = mysql_query("select id1 from friends where (id2=$uid) and status=0");
      while( $r = mysql_fetch_array( $q ) )
      {
        $id1 = $r['id1'];
        if( intval( quickQuery( "select count(*) from users where uid='$id1' and active=1" ) ) > 0 )
          $count++;
      }
      return $count;
		}

		//gets the fb user info from a mediabirdy user that's displayed on the profile page
		function getFBUserInfo($uid = null, $forceRefresh = false)
		{
			if ($uid == null)
				$uid = $this->uid;
      if( $uid == null )
        return;

			$x = sql_query("select lastupdated,fbloc as location,fbstatus as status,fbfriends as friends from users where uid=$uid");
			$fbinfo = mysql_fetch_array($x, MYSQL_ASSOC);
			
			$fbinfo['lastupdated'] = strtotime($fbinfo['lastupdated']);
			return $fbinfo;
		}

		//resizes an image on the fly - see /showimage.php
		//c = crop, boolean
		//w = width; h = height
		//url = relative url to image
		function getThumbURL($c, $w, $h, $url)
		{
			//return "/showimage.php?crop=$c&w=$w&h=$h&f=$url";
      if( stristr( $url, "/images/" ) ) return $url;
            if( $url == "" ) return "/images/noimage.jpg";
			if ($c === 1 || $c === "Y")
				$crop = "";
			else
				$crop = "N";
				
			return "/img/$crop{$w}x{$h}{$url}";
		}

		//gets the relative url for a media
		function getMediaURL($type, $id, $title = null, $gid = null)
		{
			if (empty($id)) return;

			if ($type == "V")
				return $this->getVideoURL($id, $title);
			elseif ($type == "P")
				return $this->getPhotoURL($id, $title);
      elseif( $type == "r" ) //RSS Feeds
      {
        $fid = quickQuery( "select fid from feed where link='$id' and type='rss'" );
        return "/profile/feed.php?fid=" . $fid;
      }
			elseif ($type == "W" )
      {
        if( isset( $gid ) && $gid > 0 )
          return $this->getPageURL( $gid );
        else
  				return $this->getProfileURL($id);
      }
			elseif ($type == "C") //get vid/photo URL for comment
			{
				$x = sql_query("select type,link from comments where id=$id");
				$y = mysql_fetch_array($x, MYSQL_ASSOC);
				return $this->getMediaURL($y['type'], $y['link']);
			}
			else
				return $this->getAlbumURL($id, $title);
		}

		//gets the relative url for a page
		function getPageURL($id, $title = null, $type = null)
		{
			if (empty($title) || empty($type))
				$data = queryArray( "select gname, type from pages where gid='$id'" );
			else
			{
				$data['gname'] = $title;
				$data['type'] = $type;
			}

      //echo mysql_error();

			if ($title == null)
				$title = $data['gname'];

			if ($title == "")
				$title = "Page not found";

      $dir = "page";

      if( $data['type'] == PAGE_TYPE_VESSEL )
        $dir = "vessel";

			return "/$dir/$id-" . preg_replace('/[\s\W]+/','-', trim(html_entity_decode($title)));//ereg_replace("[^A-Za-z0-9]", "-", html_entity_decode($title));
		}

		//gets the relative url for a video
		function getVideoURL($id, $title = null)
		{
			if ($title == null)
				$title = quickQuery("select title from videos where id=$id");
			
			if ($title == "")
				$title = "Video not found";

			return "/video/$id-" . preg_replace("/[^A-Za-z0-9]/", "_", trim(html_entity_decode($title)));
		}
		
		//gets the relative url for a photo
		function getPhotoURL($id, $title = null)
		{
			if ($title == null)
				$title = quickQuery("select title from photos left join albums on albums.id=aid where photos.id=$id");

			if ($title == "")
				$title = "Photo not found";

			return "/photo/$id-" . preg_replace("/[^A-Za-z0-9]/", "_", trim($title));
		}
		
		//gets the relative url for an album
		function getAlbumURL($id, $title = null)
		{
			if ($title == null)
				$title = quickQuery("select title from photos inner join albums on albums.id=aid where aid=$id");

			if ($title == "")
				$title = "Album not found";
			
			return "/album/$id-" . preg_replace("/[^A-Za-z0-9]/", "_", $title);
		}
		
		//gets the profile (log) url for a user
		function getProfileURL($uid = null, $username = null)
		{
			if ($uid == null)
			{
				$uid = $this->uid;
				$username = $this->username;
			}
			if (empty($uid))
				return "";
			
			if (empty($username)){
                $query = "select username from users where uid=$uid";
				$username = quickQuery($query);
            }

			if (empty($username)){ //user has no username
				return "/user/_$uid";
            }
			else //use the username
            {
                global $site;
                if( $site != "m" ){
                    $username = str_replace(" ", ".", $username);
                }
                return "/user/$username";
            }
		}
		
		function getUserWorks($uid = false){
		    if(!$uid){
			$uid = $this->uid;
		    }
		    $x = mysql_query(
			    "select * from
				    (select wid,unix_timestamp(start) as start,
					    unix_timestamp(stop) as stop,
					    if(stop=0,1,0) as present,
					    employer,
					    occupation,
					    w.www,g1.gname as employer_name,
					    g2.gname as occupation_name 
				    from work w
					    left join pages g1 on g1.gid=employer
					    left join pages g2 on g2.gid=occupation
				    where w.uid={$uid} AND g1.gid > 0)
			    as tmp order by present desc,start desc"
			    );

		    $userWorks = array();
		    while ($y = mysql_fetch_array($x, MYSQL_ASSOC)){
			    $userWorks[$y['wid']] = $y;
		    }
		    return $userWorks;
		}
		
		//checks to see if this user is logged in
		function isLoggedIn()
		{
			global $site;
			
//			if ($site == "s" && $this->needsVerified == 1) return false; //This is checked on /index.php

			if ($site == "m"){
				return (isset($_SESSION['linkedin_access_token']) || !empty($this->fbsess) || is_array($_SESSION['access_token']) || !empty($_SESSION['twtoken'])) && !empty($this->uid);
            }
			else{
				return !empty($this->uid);
            }
		}
		
		// upon user login, saves social site profile picture to our servers
		function saveUserPicture($ssite, $url)
		{
			global $site, $hash;


			if ($site == "m")
			{
				file_put_contents("../userpics_$site/" . $this->uid . "_$ssite.jpg", file_get_contents($url));
				return $ssite;
			}
			elseif ($site == "s") //store as part of profile pics album
			{
        try
        {
  				$contents = file_get_contents($url);
        }
        catch( Exception $e)
        {
//          echo "Can't retrieve: $url <br>";
          return -1; //can't retrieve user image
        }

        if( empty( $this->uid ) ) $this->uid = $_SESSION['uid'];

				$uid = $this->uid;

				//check to see if this was the last social site pic uploaded
        if( $ssite < SITE_OURS )
        {
          //Check to see if the photo is different from our current photo
  				sql_query("update users set md5pic$ssite='" . md5($contents) . "' where uid={$uid}");
	  			if (mysql_affected_rows() == 0)
          {
            //Before quitting, make sure the current photo exists in our database
            $currentPhoto = quickQuery( "select pic" . $ssite . " from users where uid={$uid}" );
            if( quickQuery( "select count(*) from photos where ID='" . $currentPhoto . "'" ) > 0 )
              return -1;
            //Otherwise, continue.
          }
        }

				$source = "/tmp/a$hash.jpg";
        if( file_exists( $source ) ) unlink( $source );

				file_put_contents($source, $contents);

				$_GET['pc'] = 1;
        global $fromEmail;
				$fromEmail = true;

   			ob_start();
				include_once $_SERVER['DOCUMENT_ROOT'] . "/upload/photo.php";
				processPhoto($source, true);
        $result = ob_get_contents();
				$json = json_decode( $result, true );
				ob_end_clean();

				mysql_query("update photos set privacy=" . PRIVACY_EVERYONE . ",aid='" . quickQuery("select id from albums where albType=2 and uid='{$uid}'") . "' where id=" . $json['id']);

        if($ssite < SITE_OURS)
        {
          sql_query( "update users set pic" . $ssite . "='" . $json['id'] . "' where uid='" . $uid . "'" );
        }

				return $json['id'];
			}
		}

		// saves an image from a url into a user's single uploads album
		function saveImageFromURL($url, $uploadAsAdmin = false)
		{
			global $hash, $API, $siteName;

			$contents = file_get_contents($url);

			$source = "/tmp/a$hash.jpg";

			if( !file_put_contents($source, $contents) )
        echo "Error writing file";

			$_GET['pc'] = 1;
			$fromEmail = true;

			if ($uploadAsAdmin)
			{
				$uid = $API->uid;
				$API->uid = quickQuery("select uid from users where username='$siteName'");
			}

			ob_start();
			include_once $_SERVER['DOCUMENT_ROOT'] . "/upload/photo.php";
			processPhoto($source, true);
			$json = json_decode(ob_get_contents(), true);
			ob_end_clean();
			
			sql_query("update photos set privacy=" . PRIVACY_EVERYONE . ",aid=" . quickQuery("select id from albums where albType=1 and uid={$this->uid}") . " where id=" . $json['id']);
			
			if ($uploadAsAdmin)
				$API->uid = $uid;
			
			return $json['id'];
		}

		//alias for getUserPic
		function getProfilePic($uid = null, $pic = null, $updatePicIfNotFound = true)
		{
			return $this->getUserPic($uid, $pic, $updatePicIfNotFound);
		}

		//gets the relative url for a user's profile picture
		function getUserPic($uid = null, $pic = null, $updatePicIfNotFound = true)
		{
			global $hash, $site;

			if (empty($uid))
				$uid = $this->uid;

      if( $pic < 2 && $site == "s" )
      {
        $pic_temp = quickQuery( "select pic$pic from users where uid='" . $uid . "'" );
        if( $pic_temp > 0 ) $pic = $pic_temp;
      }

			if (is_null($pic))
			{
				if ($uid == $this->uid)
					$pic = $this->pic;
				else
					$pic = intval(quickQuery("select pic from users where uid=$uid"));
			}

      if ( $site == "m")
			{
				$picpath = "/userpics_$site/$uid" . "_$pic.jpg";

				if (file_exists($_SERVER['DOCUMENT_ROOT'] . $picpath))
					return "$picpath?$hash";
				else
				{
					$l = fopen("/tmp/out", "w");
					fwrite($l, "Did not exist - " . ($_SERVER['DOCUMENT_ROOT'] . $picpath) . "\n");
					fclose($l);

//					if ($updatePicIfNotFound)
//						sql_query("update users set pic=2 where uid=$uid");
					
					if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/userpics_$site/$uid" . "_2.jpg"))
						return "/userpics_$site/$uid" . "_2.jpg";
					else
					{
						copy($_SERVER['DOCUMENT_ROOT'] . "/images/nouser.jpg", $_SERVER['DOCUMENT_ROOT'] . "/userpics_$site/$uid" . "_2.jpg");
						return "/userpics_$site/$uid" . "_2.jpg?$hash";
					}
				}
			}
			else
			{
				$hash = quickQuery("select hash from photos where id='$pic'");

				if (empty($hash))
					return "/images/nouser.jpg";
				else
					return "/photos/$pic/$hash.jpg";
			}
		}
		
		//gets the absolute url of a picture of a contact from an external site
		//contact = array of eid, site
		function getContactPic($contact)
		{
			if ($contact['site'] == SITE_FACEBOOK) //let's show the user's real pic
				return "http://graph.facebook.com/" . $contact['eid'] . "/picture";
			elseif ($contact['site'] == SITE_TWITTER)
				return 'http://api.twitter.com/1/users/profile_image/' . $contact['eid'];
			elseif ($contact['site'] == SITE_OURS)
				return $this->getThumbURL(1, 48, 48, $this->getUserPic($contact['eid']));
			else //or not
				return '/images/social/48x48/' . convertWordSite($contact['site']) . '.png"';
		}
		
		//sends confirmation email to user trying to verify email address
		function sendConfirmationEmail($email = null)
		{
			global $siteName;

      if( $site == "m" ) return; //We shouldn't be sending confirmation emails from the media birdy site

      $uid = $this->uid;

			if (empty($email))
				$email = quickQuery("select email from users where uid=" . $_SESSION['uid']);

			$confcode = $this->generateDeleteCommentHash($_SESSION['uid'] . chr(2) . $email);

			$msg = quickQuery("select content from static where id='newuser'");
			$msg = str_replace("{NAME}", strtok( $_SESSION['name'], " " ), $msg);
			$msg = str_replace("{SITENAME}", $siteName, $msg);
			$msg = str_replace("{URL}", "http://" . $_SERVER['HTTP_HOST'] . "/signup/verify.php?cc=$confcode&uid=$uid", $msg);
			$msg = textToHTML($msg);

			emailAddress($email, "Confirm your e-mail address on $siteName", $msg, "notifications");
			
			return $email;
		}
		
		//counts number of messages in a user's inbox
		//f = "u", "r", or ""
		function getMessageCount($uid = null, $f = "")
		{
			if ($uid == null)
				$uid = $this->uid;
			
			$q1 = "select mid,users.uid,username,name,pic,subj,comment,unreadBy" . ($sent == 1 ? "Send" : "Recv") . " as unread,created from messages inner join users on users.uid=messages.uid inner join comments on comments.link=mid where delBy" . ($sent == 1 ? "Send" : "Recv") . " = 0 and messages.uid" . ($sent == 1 ? "" : "_to") . "=" . $uid . " and comments.uid " . ($sent == 1 ? "" : "!") . "= " . $uid . ($f == "u" || $f == "r" ? " and unreadBy" . ($sent == 1 ? "Send" : "Recv") . ($f == "u" ? " > 0" : "= 0") : "");
			$q2 = "select mid,comments.uid,username,name,pic,subj,comment,unreadBy" . ($sent == 1 ? "Recv" : "Send") . " as unread,created from messages inner join comments on comments.link=mid inner join users on users.uid=comments.uid where delBy" . ($sent == 1 ? "Recv" : "Send") . " = 0 and messages.uid" . ($sent == 1 ? "_to" : "") . "=" . $uid . " and comments.uid " . ($sent == 1 ? "" : "!") . "= " . $uid . ($f == "u" || $f == "r" ? " and unreadBy" . ($sent == 1 ? "Recv" : "Send") . ($f == "u" ? " > 0" : "= 0") : "");

			$q = "from (select * from (select * from (($q1) union ($q2)) as t0) as t1 order by created desc) as t2 group by mid order by created desc";

			//$msgCount = intval(quickQuery("select sum(c) from (select count(*) as c $q) as xyz"));
            $msgCount = intval(quickQuery("select count(*) from (select unread as c $q) as xyz"));

			return $msgCount;
		}
		
		//counts number of unread messages
		function getUnreadMessageCount($uid = null)
		{
			return $this->getMessageCount($uid, "u");
		}
		
		//used to implement privacy into sql queries
		function getPrivacyQuery($uid = null, $showAnd = true, $table = "", $isPhoto = true) //put directly after where statement
		{
			global $site;
			
			if ($uid == null)
				$uid = $this->uid;

			if (empty($table))
				$table = "media";
			
			$privacy = array();

			if ($this->isLoggedIn())
			{
				$privacy[] = "uid=" . $this->uid; // takes care of PRIVACY_SELF and all media the user has uploaded
				
				if ($site == "s")
				{
					$privacy[] = "privacy=" . PRIVACY_FRIENDS . " and $table.uid in (select if(id1=" . $this->uid . ",id2,id1) from friends where " . $this->uid . " in (id1,id2))";
					$privacy[] = "privacy=" . PRIVACY_SELECTED . " and concat($table.id,@type) in (select concat($table.id,@type) from privacy where privacy.uid=" . $this->uid . " and privacy.type=@type and id=$table.id)";
				}
			}

			$privacy[] = "privacy=" . PRIVACY_EVERYONE;
			$privacy[] = "privacy is null";
			if($isPhoto){
			    $privacy[] = "privacy > 0 AND albums.albType = 2";
			}
			
      $q = "";
      if( $table == "photos" || $table == "videos" || $table == "media" )      //Make sure reported videos and photos don't show up
        $q .= " ($table.reported=0 OR $table.reported is NULL) and";
			$q .= " (($table." . implode(") or ($table.", $privacy) . ")) ";

			if ($showAnd)
				$q .= "and ";
			
			return $q;
		}

    function userHasAccess( $uid, $area )
    {
      global $site;

//      if( !$this->isLoggedIn() ) return false; 
      if( $site == "m" ) return true;
			if ($uid == $this->uid) return true; //we can view all our own stuff.
      if( empty( $area ) ) return false;

      $uid1 = $this->uid;
      $uid2 = $uid;
      $friends = quickQuery( "select count(*) from friends where ((id1='$uid1' AND id2='$uid2') OR (id2='$uid1' AND id1='$uid2')) and status=1" );
      if( $friends )
        return true;

      $privacy = quickQuery( "select privacy from users where uid='$uid'" );

      if( strpos($privacy, ",1,") !== false || strpos($privacy, ",$area,") !== false )
        return false;
      return true;
    }

    function fbGetPages()
    {
			$facebook = $this->startFBSession();

      if( $facebook != null )
      {
        try
        {
          $fql = "select page_id, name, page_url, type from page where page_id IN ( select page_id from page_admin where uid=me() ) AND type='WEBSITE'";
          $pages = $facebook->api(array('method' => 'fql.query', 'query' => $fql));
        }
        catch (Exception $e) {
//          echo "exception";
//          print_r( $e );
//          exit;

          unset( $_SESSION['fbpages'] );
        }

        if( sizeof($pages) > 0 )
          $_SESSION['fbpages'] = $pages;
        else
          unset( $_SESSION['fbpages'] );

        return $pages;
      }
      else
        return array();
    }

    function getToDoItem( $item )
    {
      if( empty( $this->todoList ) ) { $this->todoList = 0; }

      $bf = new Bitfield( (int)$this->todoList );
      return (int)$bf->get( $item );
    }

    function setToDoItem( $item, $reset = false )
    {
      if( empty( $this->todoList ) ) $this->todoList = 0;

      $bf = new Bitfield( (int)$this->todoList );

      if( $reset )
        $bf->reset( $item );
      else
        $bf->set( $item );

      $this->todoList = $bf->toNumber();
      $_SESSION['todo'] = $this->todoList;

      sql_query( "update users set todoList='" . $this->todoList . "' where uid='" . $this->uid . "' limit 1" );
    }

    function getToDoItemsCompleted()
    {
      if( !isset( $this->todoList ) || $this->todoList == 0 ) $this->todoList = 0;

      $bf = new Bitfield( (int)$this->todoList );
      $data = $bf->toArray();
      $count = 0;
      for( $c = 0; $c < sizeof( $data ); $c++ )
        if( $data[$c] == "1" ) $count++;
      return $count;
    }

    function getPageImage( $gid, $pid = 0, $cat = 0 )
    {
      if( $pid == 0 )
        $pid = quickQuery( "select pid from pages where gid='$gid'" );

      if( $pid == 0 )
      {
        $type = quickQuery( "select type from pages where gid='$gid'" );

				switch ($type )
				{
          case PAGE_TYPE_BUSINESS:
            if( $pid == 0 ) return "/images/company_default.png";
          break;

          case PAGE_TYPE_VESSEL:
            if( $pid == 0 ) return "/images/no_vessel.png";
          break;

          default:
            $cat = quickQuery( "select cat from pages where gid='$gid'" );
            switch( $cat )
            {
              case 113: return "/images/degree.jpg"; break;
              case 101: if( $page['pid'] == 0 ) return "/images/cert.png"; break;
              case 106: return "/images/occupation.jpg"; break;

            }
          break;
				}

      }
      else
      {
        $hash = quickQuery( "select hash from photos where id='$pid'" );
        return "/photos/$pid/$hash.jpg";
      }

    }

    function reloadAdData()
    {
      if( !$this->isLoggedIn() ) return;

      $uid = $this->uid;

      $q = sql_query( "select * from users where uid='$uid'" );

      if( $r = mysql_fetch_array( $q ) )
      {
        //Location
        $_SESSION['country'] = $r['loc_country'];
        $_SESSION['state'] = $r['loc_state'];

        //Occupations
        $occupation = array();
        $q2 = sql_query( "select occupation from work where uid='" . $this->uid . "'" );
        while( $r2 = mysql_fetch_array( $q2 ) )
          $occupation[] = $r2['occupation'];

        $q2 = sql_query( "select pages.gid from pages left join page_members on page_members.gid=pages.gid where page_members.uid='" . $this->uid . "' and cat='" . CAT_PROFESSIONS . "'" );
        while( $r2 = mysql_fetch_array( $q2 ) )
          $occupation[] = $r2['gid'];

        $_SESSION['occupations'] = $occupation;

        //Page Types
        $page_types = array();
        $q2 = sql_query( "select distinct cat from pages left join page_members on page_members.gid=pages.gid where page_members.uid='" . $this->uid . "'" );
        while( $r2 = mysql_fetch_array( $q2 ) )
          $page_types[] = $r2['cat'];

        $_SESSION['page_types'] = $page_types;

        $bio = quickQuery( "select txt from aboutme where uid='" . $this->uid . "'" );

        $keywords = explode( chr(2), $r['contactfor'] );
        $keywords = array_merge( $keywords, explode( " ", $bio ) );

        $_SESSION['keywords'] = $keywords;
      }
    }

    function prepWelcomeEmail()
    {
      global $isEmail, $siteName;
      $isEmail = true;

      ob_start();
      include( $_SERVER["DOCUMENT_ROOT"] . "/profile/user_todo_list.php" );
      $body = ob_get_contents();
      ob_end_clean();

      $subject = "Getting Connected with SaltHub";


      $body = addslashes( "Hi " . $this->name . ",<br />Welcome to $siteName. Here are a few things you can do to become more connected.<br />" . $body );

      sql_query( "insert into delayed_emails (to_uid, from_uid, send, subject, content) values (" . $this->uid . ", 832, DATE_ADD( Now(), INTERVAL 1 DAY), '$subject', '$body')");
      echo mysql_error();

      $this->sendWelcomeMessage();
    }

    function getFriendsOfFriends( $recalc = false )
    {
      if( $recalc || !isset( $_SESSION['fof'] ) )
      {
        $friends = array();
        $x = sql_query( "select if(id1=" . $this->uid . ",id2,id1) as uid from friends where (" . $this->uid . ") in (id1,id2) and status=1" );

        while ($friend = mysql_fetch_array($x, MYSQL_ASSOC))
        {
          if( $friend['uid'] != $this->uid )
            $friends[] = $friend['uid'];
        }

        foreach( $friends as $friend )
        {
          $friends_of_friend = sql_query( "select if(id1=" . $friend . ",id2,id1) as uid from friends where (" . $friend . ") in (id1,id2) and status=1" );

          while ($result = mysql_fetch_array($friends_of_friend, MYSQL_ASSOC))
          {
            if( $result['uid'] != $this->uid && !in_array($result['uid'], $friends ) )
              $friends[] = $result['uid'];
          }
        }

        $_SESSION['fof'] = $friends;
        return $_SESSION['fof'];
      }
      else
      {
        return $_SESSION['fof'];
      }
    }
    
    function getSuggestionsIds()
    {
	global $suggestions_quiet;
	//Load blocked uids
	$blocked_uids = array();
	$q = mysql_query( "select blocked_uid from blocked_users where uid='" . $this->uid . "'" );
	while( $r = mysql_fetch_array( $q ) )
	  $blocked_uids[] = $r['blocked_uid'];

	if( empty( $limit ) )
	{
	  if( isset( $_GET['limit'] ) )
	    $limit = intval( $_GET['limit'] );
	  else
	    $limit = 3;
	}

	$uids = array();
	$friends = array();

	$q = sql_query( "select if(id1=" . $this->uid . ",id2,id1) as uid,status from friends where (" . $this->uid . ") in (id1,id2)" );
	while( $r = mysql_fetch_array( $q ) )
	  if( $r['status'] == 1 )
	    $friends[] = $r['uid'];
	  else
	    $uids[] = $r['uid'];

	$q = sql_query( "select id2 from friend_suggestions where id1='" . $this->uid . "'" );
	while( $r = mysql_fetch_array( $q ) )
	  if( !in_array( $r['id2'], $friends ) )
	    $uids[] = $r['id2'];


	$q = sql_query( "select eid from contacts where site='2' and uid='" . $this->uid . "' and eid" );
	while( $r = mysql_fetch_array( $q ) )
	  if( !in_array( $r['eid'], $friends ) )
	    $uids[] = $r['eid'];

	$q = sql_query( "select users.uid from users inner join contacts on users.email=contacts.eid where contacts.uid='" . $this->uid . "' and contacts.site > 2" );
	while( $r = mysql_fetch_array( $q ) )
	  if( isset( $r['uid'] ) && !in_array( $r['uid'], $friends ) )
	    $uids[] = $r['uid'];
	{
	
	$email = quickQuery( "select email from users where uid='" . $this->uid . "'" );

	  if( $email != "" )
	  {
	    $data = explode( "@", $email );
	    if( sizeof( $data ) > 1 )
	    {
	      $domain = strtolower( trim( $data[1] ) );

	      $restricted_domains = array( "gmail.com", "yahoo.com", "aol.com", "hotmail.com", "msn.com" );

	      if( !in_array( $domain, $restricted_domains ) )
	      {
		$q = sql_query( "select users.uid from users where users.uid!='" . $this->uid . "' and users.email like '%$domain%'" );
		while( $r = mysql_fetch_array( $q ) )
		  if( isset( $r['uid'] ) && !in_array( $r['uid'], $friends ) )
		    $uids[] = $r['uid'];
	      }
	    }
	  }
	}
	
	$friends = $this->getFriendsUids();

	if( $limit == 1 )
	{
	  $blocked_uids = array_merge( $_SESSION['s_and_r_uids'], $blocked_uids );
	}
	else
	{
	  $_SESSION['s_and_r_uids'] = array();
	}

	$uids = array_diff( $uids, $blocked_uids );
	$uids = array_diff( $uids, $friends );

	$uids = array_unique( $uids );

	$cond = $this->getSuggConditions( $uids );

	$sql = "select uid,username,pic,name from users where active=1 and uid != " . $this->uid . " and $cond order by rand()"; //limit $limit";

	$x = sql_query($sql);

	$c = 0;

	$suggested_uids = array();
	$num_results = mysql_num_rows( $x );

	while ($user = mysql_fetch_array($x, MYSQL_ASSOC))
	{
	  $c++;
	  //$profileURL = $API->getProfileURL($user['uid'], $user['username']);
	  $suggested_uids[] = $user['uid'];
	  $_SESSION['s_and_r_uids'][] = $user['uid'];
	}
//	if (!$suggestions_quiet) //see weekly_profile_summary.php
//	{
//		return $num_results;
//	}
//	else
	return $suggested_uids;
    }
    
    function getSuggConditions( $uids )
    {
      $i = 0;

      $cond = "(";
      foreach( $uids as $key => $value )
	if( $value != "" )
	{
	  if( $i > 0 )
	    $cond .= " OR ";
	  $cond .= "uid=" . $value;
	  $i++;
	}
      $cond .= ")";
      return $cond;
    }

    function pageToGtype( $gid )
    {
      $cat = quickQuery( "select cat from pages where gid='$gid'" );
      return $this->pageCatToGtype( $cat );
    }

    function pageCatToGtype( $cat )
    {
      $data = queryArray( "select industry from categories where cat='$cat'" );
      if( $data['industry'] == PAGE_TYPE_BUSINESS )
        return 'C';
      if( $data['industry'] == PAGE_TYPE_VESSEL )
        return 'B';

      switch( $cat )
      {
        case 101: return 'L'; break;
        case 113: return 'S'; break;
        case 102:
        case 103:
        case 105:
        case CAT_NETWORKS: return 'N'; break;
        case 107:
          return 'P';
        break;
        case 106:
          return 'O';
        break;
      }
      return '';
    }

    function sendNotificationToPage( $nType, $info )
    {
      $q = mysql_query( "select uid from page_members where gid='" . $info['gid'] . "'" );
      while( $r = mysql_fetch_array( $q ) )
      {
        $info['uid'] = $r['uid'];
        $this->sendNotification( $nType, $info );
      }
      unset( $q );
    }

    function startTimer()
    {
      $time = microtime();
      $time = explode(" ", $time);
      $time = $time[1] + $time[0];
      $this->timer = $time;
    }

    function stopTimer()
    {
      $time = microtime();
      $time = explode(" ", $time);
      $time = $time[1] + $time[0];
      $time2 = $time;

      return ($time2 - $this->timer);
    }

    function uploadToCloud( $uid = null, $file, $dest_name )
    {
      include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/rackspace/cloudfiles.php" );

      if( $uid == null )
        $uid = $this->uid;

      if( !$this->CloudConnection )
      {
        $objRackspaceAuthentication = new CF_Authentication(RACKSPACE_USERNAME, RACKSPACE_KEY);
        $blAuthenticated = $objRackspaceAuthentication ->	authenticate();
        $objRackspaceConnection = new CF_Connection ($objRackspaceAuthentication);
        $objRackspaceConnection->setDebug (false);
        $objRackspaceConnection->ssl_use_cabundle();

        $this->CloudConnection = $objRackspaceConnection;
      }
      else
      {
        $objRackspaceConnection = $this->CloudConnection;
      }

      $createContainer = false;
      $containerName = "usr_" . $uid;

      if( strlen( quickQuery( "select container_url from users where uid='$uid'" ) ) > 0 )
      {
        try {
         	$container = $objRackspaceConnection->get_container($containerName);
        }
        catch( Exception $e )
        {
          $createContainer = true;
        }
      }
      else
        $createContainer = true;

      if( $createContainer )
      {
        $container = $objRackspaceConnection->create_container($containerName);
        $url = addslashes( $container->make_public() );
        mysql_query( "update users set container_url='$url' where uid='" . $uid . "'" );
      }

      if( !file_exists( $file ) )
      {
        if( $this->admin )
          echo "Unable to find file: $file\n";
        return false;
      }

      $remote_obj = $container->create_object( $dest_name );

      if( !$remote_obj )
      {
        if( $this->admin )
          echo "Unable to create remote file: $file\n";
        return false;
      }

      if( !( $remote_obj->write ( fopen($file, "r"),	filesize($file) ) ) )
      {
        if( $this->admin )
          echo "Unable to write file: $file\n";
        return false;
      }
	  
	  //var_dump($remote_obj);
	  
	  //echo "\n\ngood\n\n\n";
	  
      return true;
    }

    function deleteFromCloud( $uid = null, $dest_name )
    {
      include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/rackspace/cloudfiles.php" );

      if( $uid == null )
        $uid = $this->uid;

      if( !$this->CloudConnection )
      {
        $objRackspaceAuthentication = new CF_Authentication(RACKSPACE_USERNAME, RACKSPACE_KEY);
        $blAuthenticated = $objRackspaceAuthentication ->	authenticate();
        $objRackspaceConnection = new CF_Connection ($objRackspaceAuthentication);
        $objRackspaceConnection->setDebug (false);
        $objRackspaceConnection->ssl_use_cabundle();

        $this->CloudConnection = $objRackspaceConnection;
      }
      else
      {
        $objRackspaceConnection = $this->CloudConnection;
      }

      $containerName = "usr_" . $uid;

      try {
       	$container = $objRackspaceConnection->get_container($containerName);
      }
      catch( Exception $e )
      {
        $container = $objRackspaceConnection->create_container($containerName);
        $url = addslashes( $container->make_public() );
        mysql_query( "update users set container_url='$url' where uid='" . $r['uid'] . "'" );
        return;
      }
      try {
        return $container->delete_object( $dest_name, $containerName );
      }
      catch( Exception $e )
      {
        return false;
      }
    }

    function getMediaOwner( $ID, $type )
    {
      switch( $type )
      {
        case "P": return quickQuery( "select uid from photos where id='$ID'" );
        case "V": return quickQuery( "select uid from videos where id='$ID'" );
        default:
          return 0;
      }
    }

    function fbValidateSession() //Returns true is the session is OK or this user has no FB account, false is there is a session error.
    {

			$facebook = $this->startFBSession($this->uid);
      if( $facebook == null )
      {
        return true;
      }

      return $this->fbSafeCallMethod($this->uid, $facebook, "users.getInfo", array("uids" => array($facebook->getUser()), "fields" => array("birthday")));
    }

    function verifyPage( $gid )
    {
      global $siteName;

      sql_query( "update pages set verified=1 where gid='$gid'" );
      sql_query( "update page_members set admin=1 where gid='$gid' and uid='" . $this->uid . "'" );

      if( mysql_affected_rows() == 0 )
      { //if not in page, add the person to the page.
        sql_query( "insert into page_members (uid, gid, admin) values (" . $this->uid . ", $gid, 1)");
      }


      $this->feedAdd( "e", $gid, -1, -1, $gid );

      $email = quickQuery( "select email from users where uid='" . $this->uid . "'" );

      $msg = quickQuery("select content from static where id='page_verify'");
      $msg = str_replace("{NAME}", quickQuery("select name from users where uid='" . $this->uid . "'") , $msg);
      $msg = str_replace("{PAGE}", quickQuery( "select gname from pages where gid='" . $gid . "'"), $msg);
      $msg = str_replace("{PAGE_URL}", "http://www." . $siteName . ".com" . $this->getPageURL( $gid ), $msg);
      $msg = str_replace("{PROFILE_URL}", "http://www." . $siteName . ".com" . $this->getProfileURL( $this->uid ), $msg);
      $msg = str_replace("{SITENAME}", $siteName, $msg);
      $msg = textToHTML($msg);

      emailAddress($email, "Thanks for verifying the \"" . $page_data['gname'] . "\" page on $siteName!", $msg );

      //Look to see if there are any stored messages for this page.
      $q = mysql_query( "select * from page_messages where gid=$gid" );

      if( mysql_error() == "" )
        while( $r = mysql_fetch_array( $q ) )
          $this->sendMessage( $this->uid, $r['subject'], $r['message'], $r['from'] );

      $this->sendNotificationToPage( NOTIFY_GROUP_CLAIMED, array( "from" => $this->uid , "gid" => $gid) );
    }


    function sendWelcomeMessage( $uid = null )
    {
      if( $uid == null )
        $uid = $this->uid;

      $from = 832;
      $subj = "Welcome to SaltHub";

      $message = "Did you know that you can now send private messages that don�t show up in your Logbook feed? You can keep a conversation private between you and the people you send a message to.

Messages are a great way to:
- Ask someone a question privately
- Talk about confidential subjects like hiring or compensation
- Work on a project with a few select people

Go to My Messages and try sending a message now!

P.S. This message was automatically generated. If you reply, SaltHub isn�t able to reply back.";
      $message = addslashes( $message );

      mysql_query( "INSERT INTO messages (uid_to, uid, subj, unreadByRecv, unreadBySend) VALUES ('$uid', '$from', '$subj', 1, 1)" );
      $link = mysql_insert_id();

      mysql_query( "INSERT INTO comments (link, uid, type, comment, created ) VALUES ( $link, $from, 'M', '$message', Now() )" );
    }

    function convertCustomContactsToUserContacts( $uid = null )
    {
      if( $uid == null )
        $uid = $this->uid;

      $name = quickQuery( "select name from users where uid='$uid'" );
      $email = quickQuery( "select email from users where uid='$uid'" );

      if( $email == "" )
        return;

      sql_query( "update contacts set eid='$uid', site='" . SITE_OURS . "' where email='" . addslashes( $email ) . "' and site>2" );

      $q = mysql_query( "select * from custom_tags where email='" . addslashes( $email ) . "'" );

      while( $r = mysql_fetch_array( $q ) )
      {
        $q2 = mysql_query( "select * from photo_tags where cid='" . $r['id'] . "' and custom=1" );
        while( $r2 = mysql_fetch_array( $q2 ) )
        {
          $tagger = $r2['uid'];
          $cid = quickQuery( "select cid from contacts where site=2 and uid='$tagger' and eid='$uid'" );

          if( $cid == 0 )
          {
            mysql_query( "insert into contacts (uid, site, eid, name) VALUES ('$tagger', 2, $uid, '" . addslashes( $name ) . "')" );
            $cid = mysql_insert_id();
          }

          $tid = $r2['tid'];
          mysql_query( "update photo_tags set custom=0, cid='$cid' where tid='$tid'" );
        }

        $q2 = mysql_query( "select * from video_tags where cid='" . $r['id'] . "' and custom=1" );
        while( $r2 = mysql_fetch_array( $q2 ) )
        {
          $tagger = $r2['uid'];
          $cid = quickQuery( "select cid from contacts where site=2 and uid='$tagger' and eid='$uid'" );

          if( $cid == 0 )
          {
            mysql_query( "insert into contacts (uid, site, eid, name) VALUES ('$tagger', 2, $uid, '" . addslashes( $name ) . "')" );
            $cid = mysql_insert_id();
          }

          $tid = $r2['tid'];
          mysql_query( "update video_tags set custom=0, cid='$cid' where tid='$tid'" );
        }
      }
  }

  public function convertChars($text){
      $text = urldecode($text);

      $desc_validation_search = array("%u2019", "%u201C", "%u201D", "%u2013", "%u2014", "%uFB01", "%uFEFF", "%u2026");
      $desc_validation_replace = array("'", '"', '"', '-', ' - ', "fi", "", "...");
      $text = str_replace($desc_validation_search, $desc_validation_replace, $text);

      return $text;
  }
}

class typeOfFeed
{
    const A     = 'A';
    const C     = 'C';//add comment
    const e     = 'e';
    const F     = 'F';//add friend
    const G     = 'G';
    const J     = 'J';
    const L     = 'L';
    const o     = 'o';
    const P     = 'P'; //picture
    const PR0   = 'PR0';
    const PR2   = 'PR2';
    const PR3   = 'PR3';
    const PR5   = 'PR5';
    const PR6   = 'PR6';
    const PR7   = 'PR7';
    const PR8   = 'PR8';
    const PR9   = 'PR9';
    const r     = 'r';
    const rss   = 'rss';
    const S     = 'S';//sound
    const T     = 'T';
    const V     = 'V'; //video
    const VP1   = 'VP1';//change navstat
    const VP2   = 'VP2';//change name
    const VP3   = 'VP3';//change specifications
    const VP4   = 'VP4';//vessel out of range
    const W     = 'W';//wallpost

}
?>
