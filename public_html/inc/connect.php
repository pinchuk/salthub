<?
/*
Small module that is used in several places on the site.  Contains text that encourages users to connect with others.
*/
?>
<div class="subhead">Connect</div>

<div class="connect">
	<img src="/images/find.png" alt="" />Find People on <?=$siteName?>
	<div class="desc">
		<?=$siteName?> is used by friends, families, enthusiasts, businesses, and professionals around the globe.
		<div class="link">
			<a href="/findpeople.php">Use the tools to find <? echo ($site=="m") ? 'friends' : 'connections'; ?></a>&nbsp; <img src="/images/wrench.png" alt="" />
		</div>
	</div>
</div>

<div style="height: 0px; border-bottom: 1px solid #d8dfea; margin-top: 5px; margin-bottom: 5px;"></div>

<div class="connect">
	<a href="/invite.php"><img src="/images/email_add.png" alt="" />Share <?=$siteName?> with Friends</a>
	<div class="desc">
		<?=$siteName?> connects Maritime Businesses&nbsp; Support the movement, and invite someone today!
		<div class="link">
			<a href="/invite.php">Use the tools to find <? echo ($site=="m") ? 'friends' : 'connections'; ?></a>&nbsp; <img src="/images/wrench.png" alt="" />
		</div>
	</div>
</div>