<?php
/*
Contains common formatting/content used by the contact manager and the script that retrieves contact information from various user accounts (on other sites).
*/

function showContactsTop()
{
	global $API, $script, $siteName;
?>
	<script language="javascript" type="text/javascript">
	<!--
	<?php

	$twInviteMessage = str_replace("\r\n", "", trim(quickQuery("select content from static where id='tw_invite'")));
	$twInviteMessage = str_replace("{REF_LINK}", "http://" . $_SERVER['HTTP_HOST'] . "/?ref=" . $API->uid, strip_tags($twInviteMessage));

	echo "var inviteMessage = \"" . str_replace("%A0", "", urlencode($twInviteMessage)) . "\";\n\n";
	echo "var invitePreview = " . json_encode(emailTemplate(str_replace("{NAME_TO}", "there", generateInviteMsgTemplate()), "you")) . ";\n";

	$sitesInvited = array();
	$x = sql_query("select distinct site from contacts where uid={$API->uid} and site != " . SITE_OURS);
	while ($y = mysql_fetch_array($x, MYSQL_ASSOC))
		$sitesInvited[] = convertWordSite($y['site']);

	echo "var sitesInvited = ['" . implode("','", $sitesInvited) . "']\n";
	$facebook = $API->startFBSession();

  if( $facebook )
  {
  //see if session is valid
    $fbLoginUrl = "/fbconnect/login.php";

    try
    {
      $fbinfo = $facebook->api("/me");

      if( is_array( $fbinfo ) )
        $fbLoginUrl = "";
    }
    catch( Exception $e )
    {

    }

    if( $fbLoginUrl != "" )
    {
      $_SESSION['login_redirect'] = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    }
  }

  if( $API->startTwSession() != false )
    $twLoginUrl = "";
  else
  {
    $twLoginUrl = "/twitter/login.php?invite=" . urlencode($script);
  }

	echo "var fbLoginUrl = '$fbLoginUrl';\n";
	echo "var twLoginUrl = '$twLoginUrl';\n";

	echo "var doGetFBContacts = " . (!empty($_SESSION['fbid']) ? "true" : "false") . ";\n";

	?>
	//-->
	</script>
	<div id="fetchedcontactscontainer" style="display: none; position: relative;">
		<div id="fetchedcontactswait" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; display: none;">
			<table border=0 cellpadding=0 cellspacing=0 width=100% height=100%>
				<tr>
					<td style="color: #555555;">
						<center>
							<div style="font-size: 16pt; font-weight: bold;"><?=$script == "contacts" ? "Updating your contact list" : "Inviting your contacts"?> &#0133;</div>
							<img style="margin: 10px;" src="/images/wait<?=$script == "invite" ? "d8dfea_l" : "f3f8fb"?>.gif">
							<div style="font-size: 10pt;">This could take some time if you have a lot of contacts.</div>
						</center>
					</td>
				</tr>
			</table>
		</div>
		<div id="fetchedcontactschild">
			<!--
			<div class="subtitlepic" style="background-image: url(/images/page_add.png); margin-bottom: 5px; float: left;">
				<?=$script == "contacts" ? "Select the people you'd like to add to your contacts." : "Select the people you'd like to invite to $siteName."?>
			</div>
			<div style="float: right;">
				<img src="/images/bluex.png" style="cursor: pointer;" onclick="javascript:resetContacts();">
			</div>
			<div style="clear: both;"></div>-->
      <?
      $fetchedContactsHeight = 250;

      if( $script == "signup/invite-ps" )
        $fetchedContactsHeight = 405;
      ?>
			<div style="margin-top: 5px; background: white; border: 1px solid #D8DFEA; overflow-y: auto; overflow-x: hidden; height: <? echo $fetchedContactsHeight ?>px; width: 100%;" id="fetchedcontacts"></div>
			<div style="margin-top: 5px;">
				<table border=0 cellpadding=0 cellspacing=0 width=100%>
					<tr>
						<td width="150">
							<?php
							if ($script == "contacts")
								echo "&nbsp;";
							else
							{
							?>
							<div style="padding-left: 5px; font-size: 8pt; text-align: left;">
								<a href="javascript:void(0);" onclick="javascript:showInvitePreview();">view invite</a>
							</div>
							<?php
							}
							?>
						</td>
						<td style="text-align: center;">
							<input type="button" class="button" value="<?=$script == "contacts" ? "Add and Send Invites" : "Connect &amp; Send Invitations"?>" onclick="javascript:doAction();" <? if( $script != "contacts" ) { ?> id="InviteActionButton"<? } ?>/>
						</td>
						<td width=150 align="right" style="padding-right: 5px; font-size: 8pt;">
                <a href="javascript:void(0);" onclick="javascript:cancelInvite();">find other contacts</a>
            </td>
						<?php if (false && !empty($checkCaption)) { // we don't use this ?>
						<td style="padding-left: 75px;"><input type="checkbox" id="actioncheck" style="padding: 0; margin: 0;" checked></td>
						<td style="padding-left: 8px;"><?=$checkCaption?></td>
						<?php } ?>
					</tr>
				</table>
			</div>
		</div>
	</div>
	<div id="credentials" style="display: none; position: relative;">
		<div id="contacting" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; display: none;">
			<table border=0 cellpadding=0 cellspacing=0 width=100% height="100%">
				<tr>
					<td style="color: #555555;">
						<center>
							<div style="font-size: 16pt; font-weight: bold;">Contacting <span id="contactsite">XXXXXXX</span> ...</div>
							<img style="margin: 10px;" src="/images/wait<?=$script == "contacts" ? "f3f8fb" : "d8dfea_l"?>.gif">
							<div style="font-size: 10pt;">This could take some time if you have a lot of contacts.</div>
						</center>
					</td>
				</tr>
			</table>
		</div>

		<div id="credentialscontent" style="font-size: 9pt;">
			<div style="color: #555; float: left;">
				<?=$script != "invite" ? "Searching your e-mail address book and social networks is the fastest and most effective way to find your connections on " . $siteName . ".&nbsp; " : ""?>Select a social network or e-mail service provider below.
			</div>

      <table border="0" style="clear:both; width:100%;">
      <tr>
          <td>
          <div style="padding-top:5px;">
            <div style="float:left; padding-top:3px; margin-right:5px;">
    						Network:
            </div>
            <div style="float:left;">
      				<select style="border: 1px solid #7F9DB9; width: 120px; color: #555555; font-family: arial;" id="selnetwork" onchange="javascript:networkChanged(this.value);">
      					<option value="facebook">Facebook</option>
      					<option value="twitter">Twitter</option>
<!--      					<option value="hotmail">Hotmail</option>
      					<option value="msn">MSN</option>-->
      					<option value="yahoo">Yahoo</option>
      					<option value="gmail">Gmail</option>
      					<optpage>&nbsp;</optpage>
      					<option value="manual">Manual entry</option>
      				</select>
            </div>

            <div sytle="clear:both;" id="btnfbconnect">
      				<a href="javascript:void(0);" onclick="javascript:FB.Connect.requireSession(fbloggedin); return false;"><img src="/images/fbconnect.gif" alt="" /></a>
            </div>
          </div>

          </td>

          <td>
            <div style="clear:both; padding-top:5px;">
              <div style="float:left; padding-top:3px; margin-right:5px; width:60px;" id="fldlogin">
      						Login:
              </div>
              <div style="float:left;">
                  <input id="txtuser" value="" type="text" style="border: 1px solid #7F9DB9; width: 100px; color: #555555; font-family: arial;">
              </div>
            </div>
          </td>

          <td width="70">
    			  <input id="btncont" type="button" class="button" value="Continue" onclick="javascript:getSocialContacts();">
          </td>
      </tr>

      <tr>
        <td></td>

        <td>
          <div style="clear:both; padding-top:5px;">
            <div style="float:left; padding-top:3px; margin-right:5px; width:60px;" id="fldpassword">
    						Password:
            </div>
            <div style="float:left;">
    						<input id="txtpass" value="" type="password" style="border: 1px solid #7F9DB9; width: 100px; color: #555555; font-family: arial;">
    						<input id="txtname" value="" type="text" style="border: 1px solid #7F9DB9; width: 100px; color: #555555; font-family: arial; display: none;">
            </div>
          </div>
        </td>

        <td width="70">
          <div style="clear:both; text-align:right; padding-top:5px; text-align:center;">
    				<a href="javascript:void(0);" onclick="javascript:resetContacts();">cancel</a>
          </div>
        </td>
      </tr>
      </table>

		</div>
	</div>

	<div id="networkbuttons" class="networkbuttons">
		<div style="color: #555; font-size: 9pt; clear: both;">
			<?php if ($script == "contacts" || $title == "Invite Friends") { ?>
			Searching your e-mail address book and social networks is the fastest and most effective way to find your connections on <?=$siteName?>.&nbsp; Select a social network or e-mail service provider below.
			<?php } else { ?>

<!--			<div style="font-size: 8pt;">
				Go ahead and connect with your contacts already on <?=$siteName?>.
			</div>
-->
			<ul style="font-size: 8pt; color: #555; margin-bottom:-3px;">
				<li>Select a social network or e-mail service below.</li>
				<li>Log into your account and find contacts already on <?=$siteName?>.</li>
			</ul>
			<?php } ?>
		</div>

    <div style="width:100%;">
        <div style="float:left; width:100px; padding-top:3px;">
					<div class="subtitlepic" style="background-image: url(/images/facebook.png);" onclick="javascript:showSite(0);">
						Facebook
					</div>
 					<?php if ($script != "contacts" && $script != "home") { ?><span class="subtitlepic small" id="invited-facebook">searched</span><?php } ?>
				</div>

        <div style="float:left; width:100px; padding-top:3px;">
					<div class="subtitlepic" style="background-image: url(/images/twitter.png);" onclick="javascript:showSite(1);">
						Twitter
					</div>
					<?php if ($script != "contacts" && $script != "home") { ?><span class="subtitlepic small" id="invited-twitter">searched</span><?php } ?>
				</div>

<!--        <div style="float:left; width:100px; padding-top:3px;">
					<div class="subtitlepic" style="background-image: url(/images/hotmail.png);" onclick="javascript:showSite(2);">
						Hotmail
					</div>
					<?php if ($script != "contacts" && $script != "home") { ?><span class="subtitlepic small" id="invited-hotmail">searched</span><?php } ?>
				</div>
        <div style="float:left; width:100px; padding-top:3px;">
					<div class="subtitlepic" style="background-image: url(/images/msn.png);" onclick="javascript:showSite(3);">
						MSN
					</div>
					<?php if ($script != "contacts" && $script != "home") { ?><span class="subtitlepic small" id="invited-msn">searched</span><?php } ?>
				</div>-->
        <div style="float:left; width:100px; padding-top:3px;">
					<div class="subtitlepic" style="background-image: url(/images/yahoo.png);" onclick="javascript:showSite(2);">
						Yahoo
					</div>
					<?php if ($script != "contacts" && $script != "home") { ?><span class="subtitlepic small" id="invited-yahoo">searched</span><?php } ?>
				</div>
        <div style="float:left; width:100px; padding-top:3px;">
					<div class="subtitlepic" style="background-image: url(/images/gmail.png);" onclick="javascript:showSite(3);">
						GMail
					</div>
					<?php if ($script != "contacts" && $script != "home") { ?><span class="subtitlepic small" id="invited-gmail">searched</span><?php } ?>
				</div>

        <div style="float:left; width:100px; font-size:9pt; padding-top:5px;">
    			<a href="javascript:void(0);" onclick="javascript:showSite(document.getElementById('selnetwork').options.length - 1);">more ...</a>
        </div>

        <div style="clear:both;"></div>
    </div>

	</div>
<?php
}
?>