<?
/*
Creates a thumbnail image out of a full size image.
*/

function createThumb( $width, $height, $crop, $photos, $ID, $hash, $temp )
{
  $file = "/tmp/" . $hash . ".jpg";

  $ext = substr(strtolower($file), -3);
  $thumb = array( $width, $height );


  if ( strlen( $hash ) < 32) //youtube request
  {
  	$file = "http://i.ytimg.com/vi/" . substr($hash, 0, 11) . "/1.jpg";
    $ext = substr(strtolower($file), -3);
  	//die($file);
  }
  else
  {
    if (!file_exists($file))
    {
      echo "File not found for photo ID: " . $ID . "; $file\n";
      return "";
    }
  }

  if ($ext == "png")
  {
  	$img = imagecreatefrompng( $file );
  }
  elseif ($ext == "gif")
  {
  	$img = imagecreatefromgif($file);
  }
  else
  {
  	$img = @imagecreatefromjpeg($file);
  	if (!$img)
  		$img = @imagecreatefrompng( $file);
  	if (!$img)
  		$img = @imagecreatefromgif( $file);

  }

  $width = imageSX($img);
  $height = imageSY($img);

  if (!$width || !$height) {
  	echo "ERROR:Invalid width or height";
  	return "";
  }

  // Build the thumbnail
  $result = scaleImage($width, $height, $thumb[0], $thumb[1], $crop);

  $target_width = $result[0];
  $target_height = $result[1];
  $widthOffset = $result[2];
  $heightOffset = $result[3];

  $new_img = ImageCreateTrueColor($target_width, $target_height);

  if (!@imagecopyresampled($new_img, $img, 0, 0, round($widthOffset / 2), round($heightOffset / 2), $target_width, $target_height, $width - $widthOffset, $height - $heightOffset)) {
  	echo "ERROR:Could not resize image\n";
  	return "";
  }

  $quality = 90;

	imagejpeg($new_img, $temp, $quality);
  imagedestroy($new_img);
  imagedestroy($img);

  return $temp;

}
?>
