<?
/*
This script produces the "You have connections on SaltHub" email, which is sent to users 20 days
after signing up.
*/

if( empty( $uid ) )
{
  include_once( "inc.php" );
  $uid = intval( $_GET['uid'] );
}

if( $uid == 0 )
  exit;

$email = quickQuery( "select email from users where uid=$uid" );
$name = quickQuery( "select name from users where uid=$uid" );
$name = strtok( $name, " " );

$body = "Hi $name,<br /><br />";

$body .= "Connecting with people you already know on $siteName is a good idea.<br /><br />";

$body .= '<table width="600" cellpadding="0" cellspacing="0" style="border-bottom: 1px solid #d8dfea; margin-bottom:3px;">
	<tr>
		<td style="padding-top: 5px; color:#555; font-weight:bold;">
        People you already know
      </td>
	</tr>
</table>';

$body .= "Just like the real world, the more you connect and communicate with others, the more opportunities you will discover.  It's not a secret and it works.<br /><br />";

$body .= 'To find people you know, use the <a href="http://www.salthub.com/findpeople.php" style="text-decoration:none;">' . $siteName . ' contact manager</a>.<br /><br /><a href="http://www.salthub.com/findpeople.php"><img src="http://www.salthub.com/images/contact_manager.png" width="632" height="208"/></a><br /><br />';

$suggestions = getSuggestions( $uid );

if( sizeof( $suggestions ) > 0 )
{
  $body .= "Here are some people you might know on $siteName. <br /><br />";

  $body .= '
<table width="600" cellpadding="5" cellspacing="0">
<tr valign="top">
  <td>

<table width="600" cellpadding="0" cellspacing="0" style="border-bottom: 1px solid #d8dfea; margin-bottom:3px;">
	<tr>
		<td style="padding-top: 5px; color:#555; font-weight:bold;">
        Suggested Connections &nbsp;&nbsp;&nbsp; <a style="text-decoration:none; color:rgb(50,103,152);" href="http://www.salthub.com/findpeople.php">view more suggestions</a>
      </td>
	</tr>
</table>

  </td>
</tr>

<tr>
  <td>
    <table cellpadding="0" cellspacing="10">
    <tr>';

for( $c = 0; $c < sizeof( $suggestions ) && $c < 3; $c++ )
{
  $name = quickQuery( "select name from users where uid=" . $suggestions[$c] );
  if( $name != "" ) {
    $img = "http://www.salthub.com/img/48x48" . $API->getUserPic(  $suggestions[$c] );
    $purl = "http://www.salthub.com" . $API->getProfileUrl(  $suggestions[$c] );
    $body .= '<td align="center" style="font-size:9pt; "><a style="text-decoration: none; color: #326798;" href="' . $purl . '"><img src="' . $img . '" width="48" height="48"></a></td>';
    $body .= '<td width="100" style="font-size:9pt;color:#555;"><b>' . $name . '</b><br /><a style="text-decoration: none; color: #326798;" href="' . $purl . '">Connect<br />Send message</a></td>';
    if( $c == 2 ) $body .= "</tr><tr>";
  }
}

$body .= '
    </tr>
    </table>
  </td>
</td>
</table><br />';

}

$body .= "About SaltHub.com: A Career Management Utility and Business Marketing Platform for those in the Maritime Industry. Create your free account today to connect with professionals, businesses and explore new opportunities.<br /><br />Thanks,<br />The SaltHub Team<br /></div>";

mysql_query( "insert into delayed_emails (to_uid, from_uid, send, subject, content) values ($uid, 832, DATE_ADD( DATE(Now()), INTERVAL 2 DAY), 'You have connections on $siteName', '" . addslashes( $body ) . "')" );

mysql_query( "insert into delayed_emails (to_uid, from_uid, send, subject, content) values ($uid, 832, DATE_ADD( DATE(Now()), INTERVAL 20 DAY), 'You have connections on $siteName', '" . addslashes( $body ) . "')");
mysql_query( "insert into delayed_emails (to_uid, from_uid, send, subject, content) values ($uid, 832, DATE_ADD( DATE(Now()), INTERVAL 45 DAY), 'You have connections on $siteName', '" . addslashes( $body ) . "')");
mysql_query( "insert into delayed_emails (to_uid, from_uid, send, subject, content) values ($uid, 832, DATE_ADD( DATE(Now()), INTERVAL 90 DAY), 'You have connections on $siteName', '" . addslashes( $body ) . "')" );

//emailAddress( $email, "You have connections on $siteName", $body );

function getSuggestions( $uid )
{
  $limit = 3;

  $uids = Array();
  $friends = Array($uid);

  $q = sql_query( "select if(id1=" . $uid . ",id2,id1) as uid,status from friends where (" . $uid . ") in (id1,id2)" );
  while( $r = mysql_fetch_array( $q ) )
    if( $r['status'] == 1 )
      $friends[] = $r['uid'];
    else
      $uids[] = $r['uid'];

  $q = sql_query( "select eid from contacts where site='2' and uid='" . $uid . "' and eid" );
  while( $r = mysql_fetch_array( $q ) )
    if( !in_array( $r['eid'], $friends ) && !in_array( $r['eid'], $uids ) )
      $uids[] = $r['eid'];

  $q = sql_query( "select users.uid from contacts left join users on users.email=contacts.eid where contacts.uid='" . $uid . "' and contacts.site > 2" );
  while( $r = mysql_fetch_array( $q ) )
    if( isset( $r['uid'] ) && !in_array( $r['uid'], $friends ) && !in_array( $r['uid'], $uids ))
      $uids[] = $r['uid'];

  shuffle($uids);

  return $uids;
}

function tableHeader( $title, $tip = null )
{
  global $url;
  $body = '
    <table width="600" cellpadding="5" cellspacing="0" style="font-family:Arial;">
		<tr valign="top">
      <td>

  		<table width="600" cellpadding="0" cellspacing="0" style="border-bottom: 1px solid #d8dfea; font-size:9pt;">
  			<tr>
  				<td style="padding-top: 5px; color:#555; font-weight:bold; width:165px; font-size:11pt; font-family:Arial;">' . $title . '
          </td>';

  if( $tip != null )
    $body .= '<td style="padding-top: 5px; color:#555; font-size:8pt;">' . $tip . '</td>';
  else
    $body .= '<td style="padding-top: 5px; color:#555; font-weight:bold;"><a style="text-decoration: none; color: #326798;" href="' . $url . '">view all updates</a></td>';

  $body .= '
  			</tr>
  		</table>

      </td>
    </tr>
  ';
  return $body;
}

function tableFooter()
{
  return "</table><br />";
}


?>