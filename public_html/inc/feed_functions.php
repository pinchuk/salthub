<?
/*
Contains functions that assist in generating feed content.  Currently only being used by the vessels and connections weekly email.
*/

function feed_to_html( $feed )
{
  global $siteName;
  global $API;


  $html = "";

  $resolved = resolveFeed( $feed );

	if ($feed['gid'] > 0)
		$feed['page'] = $API->getPageInfo($feed['gid'],true);

  switch( $feed['type'] )
  {
		case "J": //joined
			if ($feed['link'] == 0)
				$via = " via Facebook";
			elseif ($feed['link'] == 1)
				$via = " via Twitter";
			else
				$via = "";

			$html = "joined $siteName$via";
			break;


    case "W":
      $html = $resolved[1]['descr'];
    break;

    case "C":
			$word = typeToWord($resolved[1]['type']);
      $murl = $API->getMediaURL( $resolved[1]['type'], $resolved[1]['link'] );
      $html = "Commented on a $word <a href=\"" . $murl . "\">" . $data['title'] . "</a>";
    break;

  	case "P":
  	case "V":
			$word = typeToWord($feed['type']);
 			$data = $API->getMediaInfo($feed['type'], $feed['link'], "'{$feed['type']}' as type,{$word}s.id,{$word}s.uid,{$word}s.created as ts,hash," . ($feed['type'] == "V" ? "title,descr" : "ifnull(ptitle,title) as title,ifnull(pdescr,descr) as descr"));
      $murl = $API->getMediaURL( $feed['type'], $feed['link'] );

      $title = $data['title'];
      if( $title == "" ) $title = $data['descr'];
      $html = "Posted a $word <a href=\"" . $murl . "\">" . $title . "</a>";
  	break;

		case "G": //joined page
			$html = "joined the page <a href=\"{$feed['page']['url']}\">{$feed['page']['gname']}</a>";
    break;

		case "T": //tags
		case "t":
      $target = quickQuery( "select name from users where uid='" . $feed['uid'] . "'" );
			$word   = typeToWord($resolved[1]['type']);
			$murl   = $API->getMediaURL($resolved[1]['type'], $resolved[1]['id']);
			$html   = "Tagged $target in a <a href=\"{$murl}\">$word</a>";
		break;

  }

  return $html;
}


?>