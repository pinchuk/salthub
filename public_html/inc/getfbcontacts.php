<?php
/*
Retrieves a list of a user's contacts on Facebook, used by /inc/contacts.php
*/

include "inc/inc.php";
//if (!$API->isLoggedIn()) die();

$facebook = $API->startFBSession();

$fql = "select uid,first_name,last_name,pic_square from user where uid in (select uid2 from friend where uid1 = " . $facebook->getUser() . ") order by last_name";
$tmp = $facebook->api(array('method' => 'fql.query', 'query' => $fql));

foreach ($tmp as $x)
{
	$pic = "http://graph.facebook.com/" . $x['uid'] . "/picture";
	$contact = array("uid" => $x['uid'], "name" => $x['first_name'] . " " . $x['last_name'], "img" => $pic);
	echo $contact['name'] . chr(1) . $contact['img'] . chr(1) . chr(1) . chr(1) . $contact['uid'] . chr(2);
	$contacts[$x['uid']] = $contact['name'];
}

$_SESSION['fetchedcontacts'] = $contacts;
$_SESSION['fetchedsite'] = "facebook";

?>