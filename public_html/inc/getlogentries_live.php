<?php

/*
getlogentries.php

7/22/2011 - Modified the getLogEntries() to enable posts by pages, instead of usernames.  This happens when 'uid'
            and 'uid_by' are both set to zero.  'gid' is then used as the poster.  Works with comments (replies) also.

*/

$users = array();

if (isset($API))
	$isAjax = false;
else
{
	include_once "../inc/inc.php";
	include_once "../inc/mod_comments.php";
	$isAjax = true;

	foreach ($_GET as $k => $v)
		$params[$k] = intval($v);

	ob_start();
	$return = getLogEntries($params);
	$return['html'] = ob_get_contents();
	ob_end_clean();

	echo json_encode($return);
}

function getLogEntries($params)
{
	global $API, $siteName, $gid;

	$uid = $params['uid'];
	$gid = $params['gid'];

	if (empty($uid))
		$uid = $API->uid;

	// if viewing profile feed, do not show these types found in pages
	$hidePageTypes = array("L", "C");

	if (isset($params['feedLimit']))
		$feedLimit = $params['feedLimit'];
	else
		$feedLimit = 20;

	if (isset($params['olderthan']))
	{
		$fids = "and fid < {$params['olderthan']}";
	}
	elseif (isset($params['newerthan']))
	{
		$fids = "and fid > {$params['newerthan']}";
		$feedLimit = 0; // no limit
	}

  $join = "";

	if ($gid == 0)
	{
		// find only log entries for this user and hide certain page types

		if ($uid == -1) // all users except this user
			$page = "f.uid != {$API->uid} and not (gid > 0 and f.type in ('" . implode("','", $hidePageTypes) . "'))";
		elseif ($uid == -2) // all users except this user
			$page = "not (gid > 0 and f.type in ('" . implode("','", $hidePageTypes) . "'))";
		elseif ($uid == -3) // only friends and the user
			$page = "(f.uid in (select if(id1=" . $API->uid . ",id2,id1) from friends where (" . $API->uid . ") in (id1,id2) and status=1) OR f.uid='" . $API->uid . "' OR gid in (select gid from page_members where uid='" . $API->uid . "'))";
		elseif ($uid == -4) // only photos
			$page = "(f.type='P' OR f.type='A' OR f.type='t')";
		elseif ($uid == -5) // only videos
			$page = "gid=0 and f.type='V'";
		elseif ($uid == -6) // only friends NOT the user
    {
			$page = "((f.uid in (select if(id1=" . $API->uid . ",id2,id1) from friends where (" . $API->uid . ") in (id1,id2) and status=1) OR gid in (select gid from page_members where uid='" . $API->uid . "')) and f.uid!='" . $API->uid . "')";
			$backup = "((f.uid in (select if(id1=" . $API->uid . ",id2,id1) from friends where (" . $API->uid . ") in (id1,id2) and status=1) OR gid in (select gid from page_members where uid='" . $API->uid . "')))";
    }
    elseif( $uid == -7 ) //Only groups in the specified category
    {
      $cat = $params['cat'];
      $page = "(f.uid=0 or f.type='G') and pages.gid IN ( select gid from pages where cat='$cat' )";
      $join = "inner join pages on pages.verified=1 and pages.gid=f.gid";
      $uid = 0;
      $gid = -1;
    }
    elseif( $uid == -8 ) //Only RSS feeds
    {
      $page = "f.type='rss'";
    }
    elseif( $uid == -9 ) //Only New User Notifications
    {
      $page = "f.type COLLATE latin1_general_cs LIKE 'J'";
    }
    else
			$page = "$uid in (f.uid,f.uid_by) and not (gid > 0 and f.type in ('" . implode("','", $hidePageTypes) . "'))";
	}
	else
	{
		// find only log entries for this page
		$page = "gid=$gid";
	}

	if ($params['mini'] == 1)
  {
		$page .= " and f.type not in ('V','P','A','g')";
    if( $backup != "" ) $backup .= " and f.type not in ('V','P','A','g')";
  }

  $hidden = "";

  if( $fids != "" || $page != "" )
    $hidden .= "AND ";
  $hidden .= " f.fid NOT IN ( select fid from feed_hidden where uid='" . $API->uid . "' )";

	if (isset($params['feeds']))
	{
		$rows = count($params['feeds']);
	}
	else
	{
//		$x = mysql_query("select f.fid,f.type,f.link,f.ts,f.uid,f.uid_by,f.gid,f.link as id from feed f left join photos on link=photos.id and type='P' left join videos on link=videos.id and type='V' where " . $API->getPrivacyQuery(null, true, "photos") . $API->getPrivacyQuery(null, true, "videos") . " $page $fids order by f.fid desc" . ($feedLimit ? " limit " . ($feedLimit + 1) : "")) or die(mysql_error());

//  echo "select f.fid,f.type,f.link,f.ts,f.uid,f.uid_by,f.gid,f.link as id from feed f $join where $page $fids $hidden order by f.fid desc";
		$x = mysql_query("select f.fid,f.type,f.link,f.ts,f.uid,f.uid_by,f.gid,f.link as id from feed f $join where $page $fids $hidden order by f.fid desc" . ($feedLimit ? " limit " . ($feedLimit  +30 ) : "")) or die(mysql_error());
		$rows = mysql_num_rows($x);

    if( $rows == 0 && $backup != "" )
    {
//  		$x = mysql_query("select f.fid,f.type,f.link,f.ts,f.uid,f.uid_by,f.gid,f.link as id from feed f left join photos on link=photos.id and type='P' left join videos on link=videos.id and type='V' where " . $API->getPrivacyQuery(null, true, "photos") . $API->getPrivacyQuery(null, true, "videos") . " $backup $fids order by f.fid desc" . ($feedLimit ? " limit " . ($feedLimit + 1) : "")) or die(mysql_error());
  		$x = mysql_query("select f.fid,f.type,f.link,f.ts,f.uid,f.uid_by,f.gid,f.link as id from feed f $join where $backup $fids $hidden order by f.fid desc" . ($feedLimit ? " limit " . ($feedLimit + 30) : "")) or die(mysql_error());
	  	$rows = mysql_num_rows($x);
    }
	}


	if ($rows > 0)
	{
		$i = 0;

		if (!isset($params['feeds']))
			while (($y = mysql_fetch_array($x, MYSQL_ASSOC)) && ($feedLimit == 0 || $i++ < ($feedLimit)))
      {
        $skip = false;
        switch( $y['type'] )
        {
          case "P":
            $temp = mysql_query( "select 'P' as type, id,uid,created as ts,hash,ptitle as title, pdescr as desr from photos where "  . $API->getPrivacyQuery(null, true, "photos") . " id=" . $y['link']);
            if( mysql_num_rows( $temp ) == 0 )
              $skip = true;
            else
              $y['result'] = mysql_fetch_array( $temp );
          break;

          case "V":
            $temp = mysql_query( "select 'V' as type, id,uid,created as ts,hash,title,descr from videos where "  . $API->getPrivacyQuery(null, true, "videos") . " id=" . $y['link'] );
            if( mysql_num_rows( $temp ) == 0 )
              $skip = true;
            else
              $y['result'] = mysql_fetch_array( $temp );
          break;

          default:
            $data = resolveFeed($y);
            if( sizeof( $data ) == 0 )
              $skip = true;
            else
              $y['result'] = array_pop( $data );
          break;

        }

        if( $skip )
        {
          //$rows--;
          $i--;
        }
        else
  				$params['feeds'][] = $y;
      }

		$results = array(array("dummy"));

		$i = -1;


    if( sizeof( $params['feeds'] ) == 0 ) return;

		foreach ($params['feeds'] as $feed)
		{
      $skip = false;

			$type = $feed['type'];
			$fid = $feed['id'];

			$isActor = $feed['uid_by'] == $API->uid;
			$isTarget = $feed['uid'] == $API->uid;
//			$isActor = $feed['uid_by'] == $uid;
//			$isTarget = $feed['uid'] == $uid;

      if( $feed['gid'] > 0 && $feed['uid'] == 0 && $feed['uid_by'] == 0 )
      {
        $isTarget = ($gid == $feed['gid']);
        $result['gid'] = $feed['gid'];
      }


      if( isset( $feed['result'] ) )
      {
        $resolved = $feed['result'];
        $result['resolved'] = $feed['result'];
			  $result['resolving'] = $feed['result'];
      }
      else
      {
  			$resolved = resolveFeed($feed);
        if( sizeof( $resolved ) > 0 )
        {
  		  	$result['resolved'] = array_pop($resolved);
  			  $result['resolving'] = $resolved;
        }
      }

			$result['feeds'][] = $feed;

      if( !$result['resolved'] ) { continue; }

      $feedCounter++;

			if ($feed['gid'])
				$result['page'] = $API->getPageInfo($feed['gid'],true);

			// if mini-feed in active voice, will probably have to set 'uid' and 'targets'

			// target = uid
			// actor = uid_by
      if( sizeof( $resolved ) > 0 )
			switch ($feed['type'])
			{
				case "J": //joined
					$result['img'] = "/images/anchor.png";

					if ($feed['link'] == 0)
						$via = " via Facebook";
					elseif ($feed['link'] == 1)
						$via = " via Twitter";
					else
						$via = "";

					$result['descr'] = "joined $siteName$via";
					break;

				case "W": //wallpost

					if ($result['page'] ) // && $gid == 0
					{
            if( $gid == 0 )
            {
  						$result['img'] = "/images/write.png";
						  $result['descr'] = "wrote on the log book for the {$result['page']['catname']} page, <a href=\"{$result['page']['url']}\">{$result['page']['gname']}</a>";
            }
            else
            {

              $result['title'] = "wrote";
              $result['descr'] = nl2br($result['resolved']['descr']);
            }

						$result['uid'] = $feed['uid_by'];
						$result['targets'][] = $feed['uid'];

					}
					elseif ($isTarget)
					{

						$result['uid'] = $feed['uid_by'];
						$result['title'] = "wrote";
						$result['descr'] = $result['resolved']['descr'];
					}
					else
					{
/*
						$result['img'] = "/images/write.png";
						$result['uid'] = $feed['uid_by'];
						$result['descr'] = "wrote on {TARGETS}'s log book";
						$result['targets'][] = $feed['uid'];
*/
//						$result['img'] = "/images/write.png";
						$result['uid'] = $feed['uid_by'];
						$result['title'] = "wrote on {TARGETS}'s log book";
						$result['targets'][] = $feed['uid'];
						$result['descr'] = $result['resolved']['descr'];
					}
					break;
				
				case "g":
					$result['img'] = $API->getMediaThumb("P", $result['page']['pid'], $result['page']['hash']);
					$result['title'] = "created the <a href=\"{$result['page']['url']}\">{$result['page']['gname']}</a> page";
					$result['subtitle'] = $result['page']['gname'];

          $names = "";
          $q = sql_query( "select users.* from page_members left join users on users.uid=page_members.uid where page_members.gid='" . $result['page']['gid'] . "'" );
          $total = mysql_num_rows( $q );
          $count = 0;
          while( $r = mysql_fetch_array( $q ) )
          {

            if( $count >= 3 )
            {

              $names .= " and " . ($total - $count) . " others";
              break;
            }

            if( $count > 0 ) $names .= ", ";
            $names .= '<a href="' . $API->getProfileUrl( $r['uid'] ) . '">' . $r['name'] . '</a>';

            $count++;
          }

					$result['descr'] = "In this page: " . $names;
					$result['url'] = $result['page']['url'];
					break;
				
				case "G": //joined page
					$result['img'] = "/images/page_add.png";
          $category = $result['page']['catname']; //quickQuery( "select catname from categories where cat='" . $result['cat'] . "'" );
          if( $category == "Unselected" )
            $category = $result['page']['pagetype'];

					if ($feed['uid'] == $feed['uid_by']) //joined himself
						$result['descr'] = "joined the $category page, <a href=\"{$result['page']['url']}\">{$result['page']['gname']}</a>";
					elseif ($isActor) //added a user
					{
						$result['uid'] = $feed['uid_by'];
						$result['descr'] = "added {TARGETS} to the $category page, <a href=\"{$result['page']['url']}\">{$result['page']['gname']}</a>";
						$result['targets'][] = $feed['uid'];
					}
					else //was forced into a page
					{
						$result['descr'] = "was added to the <a href=\"{$result['page']['url']}\">{$result['page']['gname']}</a> page by {ADDEDBY}";
						$result['addedby'] = $feed['uid_by'];
					}
					break;

				case "L": //likes
					$likes = $result['resolved']['likes'] == 1;
					$result['img'] = "/images/thumb_" . ($likes ? "up" : "down") . ".png";
					$word = typeToWord($result['resolved']['type']);
					$result['descr'] = ($likes ? "" : "dis") . "likes {OWNER}'s <a href=\"" . $API->getMediaURL($result['resolved']['type'], $result['resolved']['id']) . "\">$word";
/*
          if( $result['resolved']['descr'] != "" )
            $result['descr'] .= ", " . $result['resolved']['descr'];
          else */
          if( $result['resolved']['title'] != "" )
            $result['descr'] .= ", " . $result['resolved']['title'];

					// no break
				case "C": //commented
					if (!isset($result['descr']))
					{
						$word = typeToWord($result['resolved']['type']);
						$result['img'] = "/images/comment.png";
            if( $result['resolved']['type'] == 'W' )
  						$result['descr'] = "replied to {OWNER}'s <a href=\"" . $API->getMediaURL($result['resolved']['type'], $result['resolved']['uid'], null, $result['resolving'][count($result['resolving']) - 1]['gid'] ) . "\">$word</a>";
            else
  						$result['descr'] = "replied to {OWNER}'s <a href=\"" . $API->getMediaURL($result['resolved']['type'], $result['resolved']['id']) . "\">$word</a>";
					}


          if( $result['resolving'][count($result['resolving']) - 1]['gid'] > 0 )
  					$result['gid'] = $result['resolving'][count($result['resolving']) - 1]['gid'];
					$result['owner'] = quickQuery( "select uid from " . $word . "s where id='" . $result['resolved']['id'] . "'" );//$result['resolved']['uid'];
					break;

				case "T": //tags
				case "t":
					$word = typeToWord($result['resolved']['type']);
					$result['url'] = $API->getMediaURL($result['resolved']['type'], $result['resolved']['id']);
//          $result['url'] = $API->getMediaURL($feed['type'], $result['resolved']['id']);

					$result['targets'][] = $feed['uid'];
          $result['taggedby'] = $feed['uid_by'];

          //Removed minifeed as of 2/21/2012 by Todd's request.
          $isActor = true;//($uid == $feed['uid_by']);

					if ($isActor)
					{
            $result['uid'] = $feed['uid_by'];
            //Make this a mini post, instead of a full post.
		  			$result['title'] = "tagged a <a href=\"{$result['url']}\">$word</a>";
//		  			$result['title'] = "was tagged by {TAGGEDBY} in a <a href=\"{$result['url']}\">$word</a>";
            $result['img'] = "/{$word}s/{$result['resolved']['id']}/{$result['resolved']['hash']}.jpg";
  					$result['descr'] = " "; //Tagged {TARGETS} in a <a href=\"{$result['url']}\">$word</a>";

						$result['subtitle'] = $result['resolved']['title'];

            //$result['img'] = "/images/tag_add.png";
					}
					else
					{
            $result['uid'] = $feed['uid'];

						$result['img'] = "/images/tag_add.png";
						$result['descr'] = "was tagged in a <a href=\"{$result['url']}\">$word</a> by {TAGGEDBY}";
					}
					break;

				case "A": //album
					if ($feed['uid'] == $feed['uid_by']) //added it himself
						$result['title'] = "added an album";
					else //shared
					{
						$result['uid'] = $feed['uid_by'];
						$result['targets'][] = $feed['uid'];
						$result['title'] = "shared an album with {TARGETS}";
					}
					
					if ($result['page'])
						$result['title'] .= " to the <a href=\"{$result['page']['url']}\">{$result['page']['gname']}</a> page";
					
					$result['img'] = $API->getAlbumPhotos($result['resolved']['id']);

					$result['subtitle'] = $result['resolved']['title'];
					$result['descr'] = $result['resolved']['descr'];
					break;

				case "V": //video or photo
				case "P":


					$word = typeToWord($feed['type']);
					$result['url'] = $API->getMediaURL($feed['type'], $result['resolved']['id'], $result['resolved']['title']);

					if ($feed['uid'] == $feed['uid_by']) //added it himself
						$result['title'] = "added a <a href=\"{$result['url']}\">$word</a>";
					else //shared
					{
						$result['uid'] = $feed['uid_by'];
						$result['targets'][] = $feed['uid'];
						$result['title'] = "shared a <a href=\"{$result['url']}\">$word</a> with {TARGETS}";
					}

					if ($result['page'])
						$result['title'] .= " to the {$result['page']['catname']} page, <a href=\"{$result['page']['url']}\">{$result['page']['gname']}</a>";

					$result['subtitle'] = $result['resolved']['title'];
					$result['descr'] = $result['resolved']['descr'];
					$result['img'] = "/{$word}s/{$result['resolved']['id']}/{$result['resolved']['hash']}.jpg";
					break;

        case "R":
        case "r":
          if( $feed['type'] == "r" )
            $word = "photo";
          else
            $word = "video";

          $result['title'] = "reshared a <a href=\"{$result['url']}\">$word</a>";

					$result['subtitle'] = $result['resolved']['title'];
					$result['descr'] = $result['resolved']['descr'];
					$result['img'] = "/{$word}s/{$result['resolved']['id']}/{$result['resolved']['hash']}.jpg";
        break;

        case "j":
          if( $result['resolved']['approved'] == 1 )
          {
            $result['url'] = "/employment/jobs_detail.php?d=" . $result['resolved']['id'];
            $result['resolved']['hash'] = quickQuery( "select hash from photos where id='" . $result['resolved']['pid'] . "'" );
  					$result['img'] = "/photos/{$result['resolved']['pid']}/{$result['resolved']['hash']}.jpg";

            if( $feed['uid_by'] == 0 )
              $result['title'] = "reshared a <a href=\"/employment/jobs_detail.php?d=" . $result['resolved']['id'] . "\">job listing</a>";
            else
              $result['title'] = "posted a <a href=\"/employment/jobs_detail.php?d=" . $result['resolved']['id'] . "\">job listing</a>";
  					$result['subtitle'] = $result['resolved']['title'];
  					$result['descr'] = cutOffText( $result['resolved']['body'], 200 );
            $result['type'] = "j";
          }
          else
            $skip = true;

        break;

        case "o":
					$result['img'] = "/images/photos.png";
          $result['descr'] = 'updated his/her <a href="' . $API->getProfileUrl( $feed['uid_by'] ) . '">profile</a> photo';
        break;

        case "e":
					$result['img'] = "/images/checkmark_sticker_16.png";
          $result['descr'] = '<a href="' . $result['resolved']['link'] . '">' . $result['resolved']['name'] . '</a> is now verified. <a href="http://www.salthub.com/pages/claim_company.php">Claim yours</a>.';
        break;

				case "F": // friends
					if ($result['resolved']['id1'] == $uid)
					{
						$result['uid'] = $result['resolved']['id1'];
						$result['uid2'] = $result['resolved']['id2'];
					}
					else
					{
						// switch actor and targets so this profile owner is the actor
						$result['uid2'] = $result['resolved']['id1'];
						$result['uid'] = $result['resolved']['id2'];
					}

					// set uid_by to page friend feeds together if we can
					$result['feeds'][0]['uid_by'] = $result['feeds'][0]['uid'];
					$result['targets'] = array($result['uid2']);

					$result['img'] = "/images/user_add.png";
					$result['descr'] = "is now connected with {TARGETS}";
					break;

        case "rss":
          //$result['img'] = "/images/write.png";
          $result['title'] = "wrote";
          $result['descr'] = "<div><a target=\"_new\" style=\"font-weight:bold; margin:0px;\" href=\"" . $result['resolved']['link'] . "\">" . $result['resolved']['title'] . "</a></div><div>" . trim( strip_tags( $result['resolved']['preview'], "<img><br><a>" ) ) . "<br /><a target=\"_new\" href=\"" . $result['resolved']['link'] . "\">read more</a></div>";
        break;

				default:
					$result['descr'] = "doesn't know how to handle feed type {$feed['type']} (#{$feed['fid']})";
					break;
			}

			if (empty($result['uid'])) //set default actor
				$result['uid'] = $feed['uid'];

      if( !$skip )
      {
  			if ($i > -1)
  			{
          $i = count($results) - 1;
  				$lastResult = $results[$i];

  				if (is_array($result['targets']) &&
  					is_array($results[$i]['targets']) && // are there targets?
  					isset($lastResult['title']) == isset($result['title']) && // are feed styles the same (mini vs. full)?
  					$lastResult['feeds'][0]['type'] == $result['feeds'][0]['type'] && // are types the same?
  					$lastResult['feeds'][0]['uid_by'] == $result['feeds'][0]['uid_by'] && // are actors the same?
  					$lastResult['resolved']['id'] == $result['resolved']['id']) // are the final media types the same?
  				{
  					// multiple users are being acted upon (e.g. tags on same photo) - so, combine feeds
  					$results[$i]['targets'] = array_merge($results[$i]['targets'], $result['targets']);
            $results[$i]['targets'] = array_unique( $results[$i]['targets'] );
  					$results[$i]['feeds'] = array_merge($results[$i]['feeds'], $result['feeds']);
  				}
  				else
  				{
  					// do not combine feeds
  					$results[] = $result;
  				}
  			}
  			else
  				$results[] = $result;

  			$i = count($results) - 1;
      }

			unset($result);
		}

		array_shift($results);

		foreach ($results as $result)
		{
			$date = date("Y-m-d", strtotime($result['feeds'][0]['ts']));
			
			// link targets'/actors' profiles
			foreach (array("title", "descr") as $key)
			{
				preg_match_all("|{(.+?)}|", $result[$key], $replaces);
				
				foreach ($replaces[1] as $replace)
					if (strpos($result[$key], '{' . $replace . '}'))
					{
						$r = strtolower($replace);
						
						if (!is_array($result[$r]))
							$uids = array($result[$r]);
						else
							$uids = $result[$r];
						
						$html = array();

 					// get info about users
						foreach ($uids as $uidd)
						{
              if( $uidd > 0 )
                $info = getUserInfo($uidd);
              else
                $info = getPageInfo( $result['gid'] );

							$html[] = "<a href=\"{$info['profileurl']}\">{$info['name']}</a>";
						}
						
						$c = count($html);

						// add commas/and
						if ($c == 2)
							$html = array($html[0], "and", $html[1]);
						elseif ($c > 2)
						{
							for ($i = 0; $i < $c - 1 && $i < 3; $i++)
							{
								$html[$i] .= ",";
								if ($i == $c - 2)
									$html[$i] .= " and";
							}
							
							if ($c > 3)
							{
								$html = array_splice($html, 0, 3);
								$html[3] = "and " . plural($c - 3, "other");
							}
						}

						$result[$key] = str_replace('{' . $replace . '}', implode(" ", $html), $result[$key]);
					}
				}

      if( $result['uid'] > 0 )
        $result['actor_info'] = getUserInfo($result['uid']);
      else if( $result['gid'] > 0 )
        $result['actor_info'] = getPageInfo( $result['gid'] );

			$sorted[$date][$result['title'] ? "full" : "mini"][$result['uid']][] = $result;
		}

    if( empty( $sorted ) )
      return null;

		foreach ($sorted as $date => $styles)
		{
			foreach ($styles as $style => $actors)
			{
				foreach ($actors as $actor => $results)
				{
					if ($isEntryOpen && !$params['textonly'])
						endLogEntry();
					
					$isEntryOpen = false;

					foreach ($results as $result)
					{
            if( $result['uid'] > 0 & $result['actor_info']['active'] == 0 ) continue;

						if ($style == "mini")
						{
							if ($params['textonly'])
              {
								$text[] = $result['descr'];
								$uids2[] = $result['uid'];
              }
							else
							{
								if (!$isEntryOpen)
								{
									startLogEntry($result, false);
									$isEntryOpen = true;
								}
								showLogEntryMini($result, $API->uid == $result['uid'] || $API->admin, $params);
							}
						}
						else
						{
							if ($params['textonly'])
              {
								$text[] = $result['title'];
								$uids2[] = $result['uid'];
              }
							else
							{
								if ($isEntryOpen)
								{
									endLogEntry();
									$isEntryOpen = false;
								}

                if( $result['resolved'] )
                {
  								startLogEntry($result, $API->uid == $result['uid'] || $API->admin);

                  switch( $result['type'] )
                  {
                    case "j":
    	  							showLogEntryFull($result, $params);
                    break;

                    default:
    	  							showLogEntryFull($result, $params);
                      commentsLogEntry($result['resolved']);
                    break;
                  }

			  					endLogEntry();
                }
                else
                {
                  //if we get here, this indicates that the 'link' wasn't associated with anything that currently exists
                }
							}
						}
					}
				}

				if ($isEntryOpen && !$params['textonly'])
				{
					endLogEntry();
					$isEntryOpen = false;
				}
			}
		}
		
		if ($params['debug'] == "1")
		{
			echo "<pre style='font-size: 9pt;'>All feeds sorted:\n\n";
			print_r($sorted);
			echo "</pre>";
		}
	}
	
	$return['fidnew'] = $params['feeds'][0]['fid'];
	$return['fidold'] = $params['feeds'][count($params['feeds']) - 1]['fid'];
	$return['more'] = $rows > $feedLimit ? 1 : 0;
	$return['text'] = $text;
  $return['uids'] = $uids2;

	return $return;
}

function getUserInfo($uid)
{
	global $users, $API;

	if (!isset($users[$uid])) //info about user is not cached
	{
		$users[$uid] = $API->getUserInfo($uid, "username,name,pic,fbid,twusername,uid,active");
		$users[$uid]['profileurl'] = $API->getProfileURL($users[$uid]['uid'], $users[$uid]['username']);
		$users[$uid]['profilepic'] = $API->getUserPic($users[$uid]['uid'], $users[$uid]['pic']);
	}

	return $users[$uid];
}

function getPageInfo($gid)
{
	global $pages, $API;

	if (!isset($pages[$gid])) //info about user is not cached
	{
		$x = mysql_query("select gname as name, pid, twid as twusername, fbid from pages where gid=$gid" );
    $pages[$gid] = mysql_fetch_array( $x );
		$pages[$gid]['profileurl'] = $API->getPageUrl($gid, $pages[$gid]['gname'] );
		$pages[$gid]['profilepic'] = $API->getUserPic($gid, $pages[$gid]['pid']);
	}

	return $pages[$gid];
}


?>
