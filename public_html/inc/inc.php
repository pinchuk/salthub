<?php

/*
Includes all of the fundamental functions required for users to maintain their user sessions and login to the site.
Also provides security against unauthorized users logging in.

Starts the database connection.

Contains Twitter and Facebook app information.
*/

$host = explode(".", strtolower($_SERVER['HTTP_HOST']));

if ($host[0] == "mediabirdy" || $host[1] == "mediabirdy" || $site == "m")
{
	if ($host[1] != "mediabirdy" && isset($_SERVER['SERVER_ADDR'])) //check to see if they are on the www subdomain and it's not a shell script
	{
		header("Location: http://www.mediabirdy.com" . $_SERVER['REQUEST_URI']);
		die();
	}

	$site = "m";
	$siteName = "mediaBirdy";
	$fbAppId = "128549607164421";
	$fbAPIKey = "f0182cecbf86fe327794dba2fccd4b2c";
	$fbSecret = "50890b8862914158fe85425ff9cfb430";

	//Twitter Settings
	define('SITE_TWITTER_ID', '155620520');
	define('SITE_TWITTER_SN', 'media_Birdy');

	define('CONSUMER_KEY', 'CKYhru469wBH75Kze2u36g');
	define('CONSUMER_SECRET', 'Xy5AjChffbjEX6YLpqwKFYgU89ueuoWrQnlRrTZGKs');
	define('OAUTH_CALLBACK', 'http://' . $_SERVER['HTTP_HOST'] . '/twitter/loggedin.php');
}
else
{
	if ($host[1] != "salthub" && isset($_SERVER['SERVER_ADDR'])) //check to see if they are on the www subdomain and it's not a shell script
	{
		//header("Location: http://www.salthub.com" . $_SERVER['REQUEST_URI']);
		//die();
	}

	$site = "s";
	$siteName = "SaltHub";
	$fbAppId = "115146131880453";
	$fbAPIKey = "fa01c78aab6462c1750fcbdcc0933d80";
//  $fbAPIKey = "c84053c769c2472e637b32cd5aae44b5";

  $fbSecret = "d5fa60115241b96bf865ddc902d48052";

  $defaultPageTitle = "$siteName - Professional Maritime Network.";
  $defaultMetaDescription = "A Career Management Utility and Business Marketing Platform for those in the Maritime Industry. Create your free account today to connect with professionals, businesses and explore new opportunities.";

  //Rackspace Settings
	DEFINE ("RACKSPACE_USERNAME",		"tweider");
	DEFINE ("RACKSPACE_KEY",		"2eafbeef6d3c8e96728ebd33e031c533");

	//Twitter Settings
	define('SITE_TWITTER_ID', '222649263');
	define('SITE_TWITTER_SN', 'SaltHub');
	define('CONSUMER_KEY', 'jPUP1LSkIAYf3JXWFk6RRA');
	define('CONSUMER_SECRET', 'A7YyS9aK5ZbNYX9Z1BUvTYATZc5ZRFMeeuWDm4N2w');
	define('OAUTH_CALLBACK', 'http://' . $_SERVER['HTTP_HOST'] . '/twitter/loggedin.php');
}
unset($host);


$email_domain = $siteName;

$isDevServer = substr($_SERVER['HTTP_HOST'], 0, 3) == "dev";

if( !$isDevServer )
{
  $secureUrl = "https://www.salthub.com";
}
else
{
  $secureUrl = "";
}

session_set_cookie_params(60 * 60 * 24 * 7 * 4, '/', '.salthub.com', false, false); // 1 month
session_start();

$script = current(explode(".", substr($_SERVER["SCRIPT_NAME"], 1)));
//SERVER_HOST const is in misc.php

$hash = md5(microtime());

include $_SERVER["DOCUMENT_ROOT"] . "/inc/const.php";
include_once $_SERVER["DOCUMENT_ROOT"] . "/inc/sql.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/misc.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/ads.php";
include $_SERVER["DOCUMENT_ROOT"] . "/twitter/config.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/bitfield.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/api.php";




$API = new API();

//see if facebook sent us here with a new session

if (@$_GET['session'])
{
	$session = json_decode(stripslashes($_GET['session']), true);

	if (!empty($session['session_key']) )
	{
		$facebook = $API->startFBSession(null, $session);

    if( $facebook )
    {
  		try
  		{

  			$fbinfo = $facebook->api("/me");

  			// session is good

  			$_SESSION['fbid'] = $session['uid'];
  			$API->fbid = $_SESSION['fbid'];
        $API->fbsess = $session;

  			$_SESSION['fbsess'] = $session;


  		}
  		catch (Exception $e)
  		{
  			// session is bad
  			unset($session);
  			unset($fbinfo);
  		}
    }
    else
    {
			unset($session);
 			unset($fbinfo);
    }
	}
}

//protect our servers

foreach ($_POST as $k => $v)
	if (startsWith($k, "json_"))
		$_POST[$k] = stripslashes($v);
	else
		$_POST[$k] = htmlentities($v, ENT_COMPAT, "ISO-8859-1");

foreach ($_GET as $k => $v)
	if (startsWith($k, "json_"))
		$_GET[$k] = stripslashes($v);
	else
		$_GET[$k] = htmlentities($v, ENT_COMPAT, "ISO-8859-1");

if (isset($_GET['debug_session'])){
    echo '<pre>';
    print_r($_SESSION);
    echo '</pre>';
}

?>
