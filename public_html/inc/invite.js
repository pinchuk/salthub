var contacts;
var site;

function sendInvitation(cid, div)
{
	div.innerHTML = "sending &#0133;";
  invites = 1;
//  alert( cid );
	postAjax("/invite_action.php", "cid=" + cid, function (data)
	{
		if (data == "OK")
			div.innerHTML = "invitation sent";
		else
			div.innerHTML = "error sending";
	}
	);
}

function showInvitePreview()
{
	showPopUp({'content': invitePreview, 'animate': 'no'});
}

function doAction()
{
	try
	{
		if (typeof FB.Facebook.apiClient.get_session().session_key == "string")
			fbsess = FB.Facebook.apiClient.get_session().session_key;
		else
			fbsess = "";
	}
	catch (e)
	{
		fbsess = "";
	}

	add = "";

	for (i = 0; i < contacts.length - 1; i++)
  {
    if( document.getElementById("contact-" + i) )
  		if (document.getElementById("contact-" + i).checked)
	  		add += "," + i;

    if (document.getElementById("friend-" + i) )
      if (document.getElementById("friend-" + i).checked)
	      postAjax("/addfriend.php", "uid=" + document.getElementById("friend-" + i).value + "&s=0", null );
  }

	if (add == "")
		alert("You must select at least one contact to add.");
	else
	{
		document.getElementById("fetchedcontactschild").style.visibility = "hidden";
		document.getElementById("fetchedcontactswait").style.display = "";

			//if (document.getElementById("actioncheck").checked)
			//	otherAction = 1;
			//else
			otherAction = 0;

		if (script == "contacts")
			postAjax("/contacts_add.php", "add=" + add.substring(1), "actionHandlerPre");
		else
			postAjax("/invite_action.php", "add=" + add.substring(1) + "&fbsess=" + fbsess + "&otheraction=" + otherAction, "actionHandlerPre");
	}
}

function checkUserHasSite(site)
{
	if (site == "facebook")
	{
		if (hasSocialSites[0] == 0)
		{
			//showPopUp2("", "Please add your Facebook account on the <a href=\"/settings\">settings page</a><br />before adding your Facebook friends.");
			resetContacts();
			return false;
		}
	}
	else if (site == "twitter")
	{
		if (hasSocialSites[1] == 0)
		{
			showPopUp2("", "Please add your Twitter account on the <a href=\"/settings\">settings page</a><br />before adding your Twitter contacts.");
			resetContacts();
			return false;
		}
	}
	else //email
	{
		if (hasSocialSites[2] == 0)
		{
			showPopUp2("", "Please <a href=\"/signup/verify.php\">verify your e-mail address</a><br />before adding your e-mail contacts.");
			resetContacts();
			return false;
		}
	}
	
	return true;
}

function showSite(s)
{
	site = document.getElementById("selnetwork").options[s].value;

	//check to see if this is the contacts page and if the user has added their social accts
	if (script == "contacts")
	{
		if (!checkUserHasSite(site)) return;
	}
	
	document.getElementById("selnetwork").selectedIndex = s;
	document.getElementById("networkbuttons").style.display = "none";
	document.getElementById("credentialscontent").style.display = "";
	document.getElementById("credentialscontent").style.visibility = "visible";
	document.getElementById("contacting").style.display = "none";
	document.getElementById("credentials").style.display = "";
	
	networkChanged(site);
}

function networkChanged(net)
{
	found = false;
	
	if (net == null)
		net = 0;//document.getElementById("selnetwork").value;
	
	if (net == "manual")
	{
		document.getElementById("fldlogin").innerHTML = "E-mail:";
		document.getElementById("fldpassword").innerHTML = "Name:";
		document.getElementById("txtpass").style.display = "none";
		document.getElementById("txtname").style.display = "";
		document.getElementById("txtuser").style.display = "";
		document.getElementById("rightarrow").style.display = "";
		document.getElementById("btncont").style.display = "";
		document.getElementById("btnfbconnect").style.display = "none";
		
		//document.getElementById("tblsavepw").style.visibility = "hidden";
	}
	else if (net == "facebook")
	{
		//if (script == "contacts")
			//if (!checkUserHasSite(net)) return;
		
		document.getElementById("fldlogin").innerHTML = "<input type='text' style='visibility: hidden;'>";
		document.getElementById("fldpassword").innerHTML = "";
		document.getElementById("txtpass").style.display = "none";
		document.getElementById("txtname").style.display = "none";
		document.getElementById("txtuser").style.display = "none";
		document.getElementById("rightarrow").style.display = "none";
		document.getElementById("btncont").style.display = "none";
		//document.getElementById("btnfbconnect").style.display = "";
		
		if (fbLoginUrl == "") //logged in
			getFBContacts();
		else
			document.location = fbLoginUrl;
	}
	else if (net == "twitter")
	{
		if (script == "contacts")
		{
			if (!checkUserHasSite(net)) return;
			getSocialContacts();
		}
/*
    else if( script == "invite" )
    {
			if (!checkUserHasSite(net)) return;
			getSocialContacts();
			//location = "http://twitter.com/?status=" + inviteMessage;
    }
*/
		else
			location = "http://twitter.com/?status=" + inviteMessage;
	}
	else
	{
		//document.getElementById("chksavepw").checked = true;
		
		document.getElementById("fldlogin").innerHTML = "Login:";
		document.getElementById("fldpassword").innerHTML = "Password:";
		document.getElementById("txtpass").style.display = "";
		document.getElementById("txtname").style.display = "none";
		document.getElementById("txtuser").style.display = "";
		document.getElementById("rightarrow").style.display = "";
		document.getElementById("btncont").style.display = "";
		document.getElementById("btnfbconnect").style.display = "none";
		
		//document.getElementById("tblsavepw").style.visibility = "";
		
		/*for (i = 0; i < networks.length; i++)
			if (networks[i] == net && networkCred.length >= i)
			{
				if (typeof networkCred[i] == "object")
				{
					document.getElementById("txtuser").value = networkCred[i][0];
					document.getElementById("txtpass").value = networkCred[i][1];
					found = true;
				}
				break;
			}*/
	}

	if (!found)
		{
			document.getElementById("txtuser").value = "";
			document.getElementById("txtpass").value = "";
		}
}

function getFBContacts()
{
	e = document.getElementById("selnetwork");
	site = e.options[e.selectedIndex].value;
	//sk = FB.Facebook.apiClient.get_session().session_key;
	
	showSocialWait();

	getAjax("/getfbcontacts.php?", "getSocialContactsHandler");
}

function showSocialWait()
{
	document.getElementById("credentialscontent").style.visibility = "hidden";
	document.getElementById("contactsite").innerHTML = e.options[e.selectedIndex].text;
	document.getElementById("contacting").style.display = "";
}

function hideSocialWait()
{
	document.getElementById("credentialscontent").style.visibility = "visible";
	document.getElementById("contacting").style.display = "none";
}

function getSocialContacts()
{
	user = document.getElementById("txtuser").value;
	pass = document.getElementById("txtpass").value;
	
	e = document.getElementById("selnetwork");
	site = e.options[e.selectedIndex].value;

	if (site == "manual")
	{
		usrname = document.getElementById("txtname").value;
		if (usrname == "" || user == "")
			return(alert("Please enter a name and an e-mail address for this contact."));

			//if (document.getElementById("actioncheck").checked)
			//	otherAction = 1;
			//else
			otherAction = 0;

		showSocialWait();

		//for a manual contact entry, there cannot possibly be a fbsess relevant, so don't pass it
		if (script == "contacts")
			postAjax("/contacts_add.php", "name=" + escape(usrname) + "&email=" + escape(user), "actionHandlerPre");
		else
			postAjax("/invite_action.php", "name=" + escape(usrname) + "&email=" + escape(user) + "&otheraction=" + otherAction, "actionHandlerPre");

		//location = "/invite_action.php?name=" + escape(usrname) + "&email=" + escape(user) + "&otheraction=" + otherAction;
	}
	else if (site == "twitter")
	{
		showSocialWait();
		getAjax("/gettwcontacts.php?", "getSocialContactsHandler");
	}
	else
	{
		if (user == "" || pass == "")
			return(alert("Please enter a username and password for your account."));

		showSocialWait();

		data = "user=" + user + "&pass=" + pass + "&site=" + site;

		postAjax("/getsocialcontacts.php", data, "getSocialContactsHandler");
	}
}

function actionHandlerPre(data)
{
	//alert(data);
	document.getElementById("fetchedcontactscontainer").style.display = "none";
	document.getElementById("fetchedcontactschild").style.visibility = "visible";
	document.getElementById("fetchedcontactswait").style.display = "none";
	document.getElementById("networkbuttons").style.display = "";
	document.getElementById("credentialscontent").style.display = "none";
	document.getElementById("txtuser").value = "";

	hideSocialWait();

	actionHandler(data);
}

function cancelInvite()
{
	document.getElementById("fetchedcontactscontainer").style.display = "none";
	document.getElementById("fetchedcontactschild").style.visibility = "visible";
	document.getElementById("fetchedcontactswait").style.display = "none";
	document.getElementById("networkbuttons").style.display = "";
	document.getElementById("credentialscontent").style.display = "none";
	document.getElementById("txtuser").value = "";

	hideSocialWait();
}

function actionHandler(data)
{
	d = data.split("|");
	
	if (script == "contacts")
	{
		if (data == "OK")
		{
			msg = "Your contacts have been added successfully.";
			getUserContacts();
		}
		else if (admin == 1)
			msg = data;
		else
			msg = "There was an error updating your contacts list.&nbsp; Please try again later.";
	}
	else
	{
		if (d[0] == "OK" && d.length == 1)
			msg = "Your invitations have been sent successfully.";
		else if (admin == 1)
			msg = data;
		else if (d.length == 1)
			msg = "There was an error sending your invitations.&nbsp; Please try again later.";
		else
		{
			msg = "The following people did not receive invitations due to limits on Facebook sending:<br /><br />" + d[1];
		}
		
		e = document.getElementById("invited-" + site);
		if (e) e.style.visibility = "visible";
	}

	showPopUp("", '<div style="padding: 20px; font-weight: bold;">' + msg + '</div>', 400);
}

function getSocialContactsHandler(data)
{
	fail = false;
	if (data == "INVALID_CREDENTIALS")
	{
		alert("Invalid username and/or password.");
		fail = true;
	}
	else if (data == "UNKNOWN_ERROR")
	{
		alert("An unknown error occurred while trying to retrieve your contacts.  Please try again later.");
		fail = true;
	}

	document.getElementById("contacting").style.display = "none";
	
	if (fail)
	{
		document.getElementById("credentialscontent").style.visibility = "visible";
		return;
	}

	e = document.getElementById("invited-" + site);
	if (e) e.style.visibility = "visible";

	document.getElementById("fetchedcontactscontainer").style.display = "";
	document.getElementById("credentialscontent").style.display = "none";

	e = document.getElementById("fetchedcontacts");

	html = '<div style="padding: 3px;">';
	html += '	<table border=0 cellpadding=0 cellspacing=0>';
	html += '		<tr valign="top">';
	html += '			 <td width=1>';
	html += '				<input type="checkbox" style="margin: 0px; margin-right: 8px;" onclick="javascript:checkAllContacts(this.checked);">';
	html += '			</td>';
	html += '			<td style="color: #555555; font-size: 9pt; font-weight: bold;">';
	html += '				Select all';
	html += '			</td>';
	html += '		</tr>';
	html += '	</table>';
	html += '</div>';

	contacts = data.split(String.fromCharCode(2));
	for (i = 0; i < contacts.length - 1; i++)
	{
		user = contacts[i].split(String.fromCharCode(1));

		alreadyInvited = false;

		if (site == "facebook")
		{
			pic = user[1];
			if (script == "invite" && typeof fbInvited != "undefined")
				alreadyInvited = fbInvited.indexOf(user[2]) >= 0;
		}
		else if (site == "twitter" || user[1] != "" )
		{
      alreadyInvited = (user[5]>0);
			pic = user[1];
		}
		else
    {
      alreadyInvited = (user[5]>0);
			pic = "/images/social/48x48/" + site + ".png";
    }

		if (alreadyInvited)
			bg = "#FFFFCC";
		else if (i % 2 == 0)
			bg = "#F7F7F7";
		else
			bg = "white";

    uid = user[3];
    cid = user[4];

		html += '<div style="background: ' + bg + '; padding: 3px; border-top: 1px solid #D8DFEA;';
		if (i == contacts.length - 2)
		html += ' border-bottom: 1px solid #D8DFEA;';
		html += '">';
		html += '	<table border=0 cellpadding=0 cellspacing=0>';
		html += '		<tr valign="top">';
		html += '			 <td width=1>';
    if( user[3] > 0 )
    {
      if( user[6] == 0 )
    		html += '				<input id="friend-' + i + '" type="checkbox" style="margin: 0px; margin-right: 8px;" value="' + uid + '">';
      else
        html += '       <input type="checkbox" style="margin: 0px; margin-right: 8px;" disabled>';
    }
    else
  		html += '				<input id="contact-' + i + '" type="checkbox" style="margin: 0px; margin-right: 8px;">';
		html += '			</td>';
		html += '			<td width=1>';
		html += '				<img src="' + pic + '" height=48 width=48 style="margin-right: 8px;">';
		html += '			</td>';
		html += '			<td style="color: #555555; font-size: 9pt;"><div style="width: 375px; overflow: hidden;">';

		html += '				<b>' + user[0] + '</b>';

    var email1 = new String( user[0] );
    var email2 = new String( user[2] );
    email1 = email1.toLowerCase();
    email2 = email2.toLowerCase();

		if ((site == "hotmail" || site == "msn" || site == "yahoo" || site == "gmail") && email1 != email2)
   		html += '				<br/>' + user[2];

    if( user[3] > 0 )
    	html += '			<br><br />is a member';
    else if (alreadyInvited)
  		html += '				<br/>already invited';

		html += '			</div></td>';
    html += '     <td align="right" width="150">';
  	html += '		<div class="contactstatus" style="border-bottom:0px;"><div class="actions"><img src="/images/' + site + '.png" alt="" /><br /><br /><div class=link">';
    if( user[3] > 0 )
      if( user[6] > 0 )
        html += '			<a href="javascript:void(0);" onclick="javascript:showSendMessage(' + uid + ', \'' + user[0] + '\', \'' + user[1] + '\');">send message</a>';
      else
      	html += '			<a href="javascript:void(0);" onclick="javascript:addFriend(' + uid + ', this.parentNode, \'friend request pending\');">add as friend</a>';
    else if( alreadyInvited )
     	html += '			<a href="javascript:void(0);" onclick="javascript:sendInvitation(' + cid + ', this.parentNode);">send reminder invitation</a>';
    else
     	html += '			<a href="javascript:void(0);" onclick="javascript:sendInvitation(' + cid + ', this.parentNode);">invite</a>';

    html += '    </div></div></div></td>';
		html += '		</tr>';
		html += '	</table>';
		html += '</div>';
		//blah blah
	}

	e.innerHTML = html;
}

function resetContacts()
{
	document.getElementById('credentials').style.display = "none";
	document.getElementById('networkbuttons').style.display = "";
}

function checkAllContacts(on)
{

	for (i = 0; i < contacts.length - 1; i++)
  {
		if( document.getElementById("contact-" + i) )
    {
    	document.getElementById("contact-" + i).checked = on;
    }
		if( document.getElementById("friend-" + i) )
  		document.getElementById("friend-" + i).checked = on;
  }

}

for (var i in sitesInvited)
{
	e = document.getElementById("invited-" + sitesInvited[i]);
	if (e) e.style.visibility = "visible";
}

networkChanged(null);

if (doGetFBContacts)
	showSite(0);
