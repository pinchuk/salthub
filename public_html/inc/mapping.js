
	var map = 0;
	var geocoder;
	var infowindow;
	var centerOfMap;
  var markers = Array();

	var http = createRequestObject();

	function createRequestObject(){
		var request_o; //declare the variable to hold the object.
		var browser = navigator.appName; //find the browser name
		if(browser == "Microsoft Internet Explorer"){
			/* Create the object using MSIE's method */
			request_o = new ActiveXObject("Microsoft.XMLHTTP");
		}else{
			/* Create the object using other browser's method */
			request_o = new XMLHttpRequest();
		}
		return request_o; //return the object
	}

	/*
	geocodeAddress:  given an inputAddress, creates a marker and places it on the map.
	infoWindowDescription - this String is placed in the InfoWindow along with the address.
	If initializeMap is set to true, the address will be treated as the center of the map and the map will be initialized.
	*/
	function geocodeAddress(inputAddress, infoWindowDescription, initializeMap )
	{
		if (geocoder)
		{
			//Geocoder object has been initialized.

			geocoder.geocode( {'address':inputAddress}, function(results, status)
			{
				//The first argument is essentially a geocode request with address attribute set to the zip code.

				//Body of callback function for the geocoder.  Whatever code
				//is here is executed when the geocoder request is complete.
				marker_icon = "/images/forsale.png";

				if (status == google.maps.GeocoderStatus.OK)
				{
					if (initializeMap == true)
					{
						initializeMapWithCenter(results[0].geometry.location);
						markers.push( new google.maps.Marker( {map: map, title:  infoWindowDescription + ", Located at " + inputAddress, position: results[0].geometry.location}) );
					}
					else
					{
						if (centerOfMap != null)
						{
  						markers.push( new google.maps.Marker( {map: map, title:  infoWindowDescription + ", Located at " + inputAddress, position: results[0].geometry.location}) );
              map.setZoom(2);
						}
					}
				}
				else
				{
				//	alert("Geocode failed - " + status);
				}
			});
		}
	}

	function plotMarker( title, latitude, longitude )
	{
		if( geocoder )
		{
			marker_icon = "/images/sailing-ship-icon.png";

			LatLngObj = new google.maps.LatLng(latitude, longitude);

      initializeMapWithCenter(LatLngObj);

			if (centerOfMap != null)
			{

				//Only put the marker on the map if the distance is <= 20 mi.
				//if (calculateDistanceBetweenLocations(centerOfMap, LatLngObj ) <= 20)
				{
					var marker = new google.maps.Marker( {map: map, title: title, position: LatLngObj, icon: marker_icon } );
					//Add the event handler for clicking on a marker.
/*
					google.maps.event.addListener(marker, 'click', function()
					{
						top.document.location = 'some url';
					});
*/
				}
			}
		}
	}

	function addMarker( title, latitude, longitude, url )
	{
    if( !map )
    {
  		centerOfMap = new google.maps.LatLng(0, 0);

  		var myOptions = {
  			zoom: 2,
  			center: centerOfMap,
  			mapTypeId: google.maps.MapTypeId.ROADMAP,
        
  			};

  		map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
    }
	
	alert('here');
	
  	marker_icon = "/images/sailing-ship-icon-grey.png";
		LatLngObj = new google.maps.LatLng(latitude, longitude);
		var marker = new google.maps.Marker( {map: map, title: title, position: LatLngObj, icon: marker_icon } );

    if( url != undefined )
    {
    	google.maps.event.addListener(marker, 'click', function()
    	{
    		top.document.location = url;
    	});
    }
	}

	function initializeMapWithCenter(LatLngcenterLocation)
	{
		var myOptions = {
			zoom: 12,
			center: LatLngcenterLocation,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
      mapTypeControl: true,
			scaleControl : false,
			navigationControl : true,
			streetViewControl : false
			};

		centerOfMap = LatLngcenterLocation;

		map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
	}

	//Calculates the distance in miles between 2 locations (given as google.maps.LatLng objects).  Note that this is
	//direct distance (point-to-point distance).
	function calculateDistanceBetweenLocations(LatLngLocation1, LatLngLocation2)
	{
		var LatitudeDifference = Math.abs(LatLngLocation1.lat() - LatLngLocation2.lat()) * Math.PI / 180;
		var LongitudeDifference = Math.abs(LatLngLocation1.lng() - LatLngLocation2.lng()) * Math.PI / 180;

		RadiusOfEarth = 3963.1676;
		//Using s = r*theta, the difference between the latitude angles can be changed to a distance in miles.
		//r = radius of Earth, theta is in radians
		var LatDistance = LatitudeDifference * RadiusOfEarth;

		//Longitude is a little different.  The modified formula s = r cos(theta_lat) * theta_long should work.
		var LongDistance = LongitudeDifference * RadiusOfEarth * Math.cos(LatitudeDifference);

		return Math.sqrt(Math.pow(LatDistance,2) + Math.pow(LongDistance,2));
	}
