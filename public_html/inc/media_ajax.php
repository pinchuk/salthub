<?php
/*
11/20/2011 - Excluding the album ID 884372 from the photos listing because this is the album that holds the photos for jobs and advertisements.

*/

include_once "../inc/inc.php";

$gid = intval($_GET['gid']);
$uid = intval($_GET['uid']);
$p = intval($_GET['p']);
$tagged = $_GET['tagged'] == "1";
$type = $_GET['type'] == "V" ? "V" : "P";
$perPage = 12;

$word = typeToWord($type);

if ($gid == 0)
{
	$select = "@type:='$type',media.id,hash,title,descr";
}
else
{
	if ($type == "V")
		$select = "@type:='$type',media.id,hash,title,descr";
	elseif ($type == "P")
		$select = "@type:='$type',media.id,hash,ifnull(ptitle,title) as title,ifnull(pdescr,descr) as descr"; //set aid to -1 so it's not highlighted - we are showing single pictures anyway
}

if ($tagged)
	$q = "from {$word}_tags left join contacts on contacts.cid={$word}_tags.cid and contacts.uid={$word}_tags.uid inner join {$word}s as media on media.id={$word}_tags.{$type}id " . ($type == "P" ? "inner join albums on media.aid=albums.id" : "") . " where " . $API->getPrivacyQuery(null, true, "", ($type == "P" ? true : false)) . " (({$word}_tags.uid=$uid and {$word}_tags.cid=0) or (eid=$uid))" . ($gid == 0 ? "" : " and media.id in (select id from page_media gm where gm.gid=$gid and gm.type='$type')");
else
{
	if ($type == "V")
  {
    if( $gid == 0 )
  		$q = "from videos as media where " . $API->getPrivacyQuery(null, true, "", false) . " media.uid=$uid";
    else
      $q = "from videos as media inner join page_media gm on type='V' and media.id=gm.id where " . $API->getPrivacyQuery(null, true, "", false) . " gm.gid=" . $gid;
  }
	else
  { //Photos
    if ($gid == 0) //photos posted by a user
	  	$q = "from albums left join photos as media on media.id=albums.mainImage OR (albums.mainImage=0 and media.aid=albums.id) where " . $API->getPrivacyQuery() . " media.aid=albums.id and media.uid=$uid and media.aid!=884372";
	  else //photos in a page
		  $q = "from page_media gm inner join photos as media on type='P' and media.id=gm.id inner join albums on media.aid=albums.id where gid=$gid";
  }
}

$total = quickQuery("select @type:='$type',count(*) $q");
$pages = ceil($total / $perPage);

$q .=  " order by " . ($type == "P" && !$tagged ? "albType desc," : "") . ($type == "V" || $tagged ? "media" : "albums") . ".created desc limit " . ($p * $perPage) . ",$perPage";

$range = array($p * $perPage + 1, $p * $perPage + $perPage);

if ($total == 0)
{
	echo '<div class="nomedia">';
	if ($tagged)
		echo 'If you have any ' . $word . 's of ' . ($user['uid'] == $API->uid ? "yourself" : $user['name']) . ', tag<br />them for everybody to see.&nbsp; <a href="javascript:void(0);" onclick="javascript:showTweetBox();">Click here to upload</a>.';
	else
  {
		echo 'Does ' . ($gid == 0 ? $user['name'] : $page['gname']) . ' have a ' . $word . ' you would like<br />to see?&nbsp; Ask for it to be shared.&nbsp;';
    if( $gid == 0 ) echo ' <a href="javascript:void(0);" onclick="javascript:showSendMessage(' . $user['uid'] . ', \'' . $user['name'] . '\', \'' . $API->getThumbURL(0, 48, 48, $API->getUserPic($user['uid'], $user['pic'])) . '\');">Send a message</a>.';
  }
	echo '</div>';
}
else
{

?>

<div style="padding: 8px 13px 3px; font-size: 8pt;"><?=$range[0]?>-<?=$range[1] > $total ? $total : $range[1]?> of <?=plural($total, $tagged ? $word : ($type == "P" ? ($gid == 0 ? "album" : "photo") : $word))?></div>

<?php
$i = 0;

unset($medias);

$x = mysql_query("select $select" . ($type == "P" ? ",albType,aid" : "") . " $q") or die(mysql_error());

while ($y = mysql_fetch_array($x, MYSQL_ASSOC)) $medias[] = $y;

for ($i = 0; $i < count($medias); $i++)
{
	$media = $medias[$i];

	$url = $API->getMediaURL($type, $media['id'], $media['title']);

//	echo '<div class="usermedia_hspacer">&nbsp;</div>';

	if ($gid == 0 && $media['albType'] > 0 && $range[0] == 1 && $i == 0 && $type == "P" && !$tagged) //todd wants the special albums highlighted
	{
		$doHighlight = true;
		echo '<div class="usermedia_highlight-container">';
	}

	echo '<div style="margin-left:15px;" id="media-' . $media['id'] . '" class="usermedia' . ($doHighlight ? ' usermedia_highlight' : '') . '">';
	echo '	<a title="' . $media['descr'] . '" href="' . $url . '"><img src="' . $API->getThumbURL(1, 119, 95, '/' . $word . 's/' . $media['id'] . '/' . $media['hash'] . '.jpg') . '" /></a>';
	if (!$tagged || $type == "V")
	{
		echo '<div><a href="' . $url . '">' . $media['title'] . '</a>';
    if( isset( $gid ) && $gid > 0  && ($API->admin || $page['member'] ) ) {
      echo '<div style="text-align:center; width:100%;"><a href="javascript:void(0);" style="font-size:8pt; font-weight:300;" onclick="addPageExistingMedia( \'' . $type . '\', \'' . $media['id'] . '\', \'' . $gid . '\', true ); document.getElementById(\'media-' . $media['id'] . '\').style.display=\'none\';">remove from page</a></div>';
      echo '<div style="text-align:center; width:100%;"><a href="javascript:void(0);" style="font-size:8pt; font-weight:300;" onclick="changePagePrimaryPhoto( \'' . $media['id'] . '\', \'' . $gid . '\', \'' . $media['hash'] . '\' );">select as primary</a></div>';
    }
		if ($type == "P" && $gid == 0)
			echo '<br />' . quickQuery("select @type:='$type',count(*) from photos as media left join albums on albums.id=media.aid where " . $API->getPrivacyQuery() . " aid=" . $media['aid']) . ' photos';
		echo '</div>';
	}
	echo '</div>';

	if ($doHighlight)
	{
		if ($i == 1 || $medias[$i + 1]['albType'] < 1)
		{
			echo '<div style="clear: both;"></div></div>'; //end highlight
			$doHighlight = false;
		}
	}
	
	$vspaced = false;
	if (($i + 1) % 4 == 0)
	{
		$vspaced = true;
		echo '<div class="usermedia_vspacer">&nbsp;</div>';
	}
}

if (!$vspaced) echo '<div class="usermedia_vspacer">&nbsp;</div>';
?>

<div style="clear: both;"></div>

<div style="width: 535px; text-align: right; font-size: 9pt; clear: both;">
	<?php
	if ($p > 0)
		echo '<a href="javascript:void(0);" onclick="javascript:pageUserMedia(\'' . implode("','", array($uid, $type, $tagged ? 1 : 0, $p - 1, $gid)) . '\');">Prev</a> | ';

	for ($i = 0; $i < $pages; $i++)
	{
		if ($i == $p)
		 echo $i + 1;
		else //pageUserMedia(uid, t, tagged, p)
			echo '<a href="javascript:void(0);" onclick="javascript:pageUserMedia(\'' . implode("','", array($uid, $type, $tagged ? 1 : 0, $i, $gid)) . '\');">' . ($i + 1) . '</a>';
		if ($i < $pages - 1) echo ' | ';
	}
	
	if ($pages > $p + 1)
		echo '<a href="javascript:void(0);" onclick="javascript:pageUserMedia(\'' . implode("','", array($uid, $type, $tagged ? 1 : 0, $p + 1, $gid)) . '\');"> | Next</a>';
	?>
</div>

<?php
}
?>