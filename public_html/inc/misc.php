<?php
/*
Contains misc. functions used throughout the site:

email functions
resolveFeed (used by /profiles/getlogentries.php)
profile page creation
media utilities
.. and others

(This should probably be broken up into several files for clarity)

8/10/2011 - Added ability to not invlude 'target="_blank"' in the <a> tag in findLinks()
12/12/2011 - Changed from term "group" to "page"
12/18/2011 - Added showPagePreviewFull()
12/28/2011 - Added showSuggestPage()
9/28/2012 - Added getFeedJMPLink()
*/

function getStartTime()
{
	$mtime = microtime(); 
	$mtime = explode(" ",$mtime); 
	$mtime = $mtime[1] + $mtime[0]; 
	$starttime = $mtime;

	return $starttime;
}

function getEndTime($starttime)
{
	$mtime = microtime(); 
	$mtime = explode(" ",$mtime); 
	$mtime = $mtime[1] + $mtime[0]; 
	$endtime = $mtime;

	if (isset($starttime))
		return $endtime - $starttime;
	else
		return $endtime;
}

function mungeemail($address)
{
    $address = strtolower($address);
    $coded = "";
    $unmixedkey = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789.@";
    $inprogresskey = $unmixedkey;
    $mixedkey="";
    $unshuffled = strlen($unmixedkey);
    for ($i = 0; $i <= strlen($unmixedkey); $i++)
    {
	$ranpos = rand(0,$unshuffled-1);
	$nextchar = $inprogresskey{$ranpos};
	$mixedkey .= $nextchar;
	$before = substr($inprogresskey,0,$ranpos);
	$after = substr($inprogresskey,$ranpos+1,$unshuffled-($ranpos+1));
	$inprogresskey = $before.''.$after;
	$unshuffled -= 1;
    }
    $cipher = $mixedkey;

    $shift = strlen($address);

    $txt = "<script type=\"text/javascript\" language=\"javascript\"><!--\n";

    for ($j=0; $j<strlen($address); $j++)
    {
	if (strpos($cipher,$address{$j}) == -1 )
	{
		$chr = $address{$j};
		$coded .= $address{$j};
	}
	else
	{
		$chr = (strpos($cipher,$address{$j}) + $shift) % strlen($cipher);
		$coded .= $cipher{$chr};
	}
    }


    $txt .= "\n bro = \"" . $coded . "\"\n" .
	"  key = \"".$cipher."\"\n".
	"  shift=bro.length\n".
	"  dun=\"\"\n".
	"  for (i=0; i<bro.length; i++) {\n" .
	"    if (key.indexOf(bro.charAt(i))==-1) {\n" .
	"      bux = bro.charAt(i)\n" .
	"      dun += (bux)\n" .
	"    }\n" .
	"    else {     \n".
	"      bux = (key.indexOf(bro.charAt(i))-
shift+key.length) % key.length\n".
	"      dun += (key.charAt(bux))\n".
	"    }\n".
	"  }\n".
	"document.write(\"<a href='mailto:\"+dun+\"'>\"+dun+\"</a>\")\n" .
	"\n".
        "//-"."->\n" .
        "<" . "/script><noscript>N/A" .
	"<"."/noscript>";
    return $txt;
}

function sql_query($query, $l = null)
{
	$t = time();
	
	if (is_null($l))
		$x = mysql_query($query);
	else
		$x = mysql_query($query, $l);

	global $isDevServer;
	
	if ($isDevServer)
		file_put_contents("/tmp/queries", "\n\n" . $query . "\n>> " . (time() - $t) . " secs\n=================================================================", FILE_APPEND);

	return $x;
}

function plural($num, $word, $es = false)
{
	return "$num $word" . ($num == 1 ? "" : ($es ? "e" : "") . "s");
}

function plural2($num, $word, $es = false)
{
	return "$word" . ($num == 1 ? "" : ($es ? "e" : "") . "s");
}

function generateInviteMsgTemplate()
{
	global $API;

	$msgTemplate = quickQuery("select content from static where id='email_invite'");
	$msgTemplate = str_replace("{PROFILE_URL}", $_SERVER['HTTP_HOST'].$API->getProfileURL(), $msgTemplate);
	$msgTemplate = str_replace("{NAME_FROM}", $API->name, $msgTemplate);
	$msgTemplate = str_replace("{REF_LINK}", findLinks("http://" . $_SERVER['HTTP_HOST'] . "/?ref=" . $API->uid), $msgTemplate);
	$msgTemplate = str_replace("\n", "<br />", $msgTemplate);
	$msgTemplate = str_replace(".  ", ".&nbsp; ", $msgTemplate);

	return $msgTemplate;
}

function debug($what)
{
	$l = fopen("/tmp/debug", "a");
	fwrite($l, date("r") . ":  $what\n");
	fclose($l);
}

function getPathFromPhotoID($id)
{
	global $site;

  if( $site == "s" )
  {
  	$id = str_split(str_pad(intval($id), 8, '0', STR_PAD_LEFT));
  	return $site . "/" . implode("/", array_slice($id, 0, 4)) . "/" . implode(array_slice($id, 4));
  }
  else
  {
  	$id = str_pad(intval($id), 5, '0', STR_PAD_LEFT);
  	return $site . "/" . substr($id, 0, 1) . "/" . substr($id, -4);
  }
}

function getWikipediaInfo($s)
{
	$url = "http://en.wikipedia.org/w/api.php?action=opensearch&search=" . urlencode($s) . "&format=xml&limit=1";

	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_HTTPGET, TRUE);
	curl_setopt($ch, CURLOPT_POST, FALSE);
	curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_NOBODY, FALSE);
	curl_setopt($ch, CURLOPT_VERBOSE, FALSE);
	curl_setopt($ch, CURLOPT_REFERER, "");
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
	curl_setopt($ch, CURLOPT_MAXREDIRS, 4);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 6.1; he; rv:1.9.2.8) Gecko/20100722 Firefox/3.6.8");
	
	$page = curl_exec($ch);
	$xml = simplexml_load_string($page);
	
	if ($xml->Section->Item->Description)
	{
		$title = (string) $xml->Section->Item->Text;
		$descr = (string) $xml->Section->Item->Description;
		
		$url = "http://en.wikipedia.org/w/api.php?action=parse&prop=text&page=" . urlencode($title) . "&redirects&format=json";
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$json = json_decode(curl_exec($ch), true);
		curl_close($ch);

		$html = $json['parse']['text']['*'];

		preg_match_all('/<img[^>]+>/i', $html, $result); 
		if ($result[0][0])
		{
			foreach ($result[0] as $tag)
			{
				preg_match_all('/(src)=("[^"]*")/i', $tag, $src);
				$img = substr($src[2][0], 1, strlen($src[2][0]) - 2);
				
				// check to see if we want this image or not
				if (strpos($img, "/thumb/"))
				{
					$x = end(explode("/", $img));
					$i = strpos($x, "px");

					if ($i === false)
						continue;

					$px = substr($x, 0, $i);

					if ($px > 100) //we'll take an image over 100 pixels
						break;
					else
						$img = null;
				}
				else
					$img = null;
			}
		}

		return array("img" => $img, "title" => $title, "descr" => $descr);
	}
}

function showPageSmallPreview($page)
{
	global $API;
	$hash = md5($page['gid']) . microtime();
	$url = $API->getPageURL($page['gid'], $page['gname']);

  $img = $API->getPageImage($page['gid'], $page['pid'] );
  if( stristr( $img, "images" ) === false )
    $img = $API->getThumbURL(0, 24, 24, $img);
	?>
	<div class="page_smpreview">
		<div class="pic">
			<a href="<?=$url?>"><img height="24" width="24" src="<?=$img?>" alt="" /></a>
		</div>
		<div class="title">
			<a href="<?=$url?>"><?=$page['gname']?></a><br />
			<? if( isset( $page['friends'] ) ) { ?>Connections:&nbsp; <a href="#"><?=$page['friends']?></a><? } ?>
		</div>
<? if( $API->isLoggedIn() ) {
    if( quickQuery( "select count(*) from page_members where uid = " . $API->uid . " and gid=" . $page['gid'] ) == 0 )
    {
  ?>
		<div class="join" id="join-<?=$hash?>">
			<a href="javascript:void(0);" onclick="javascript:joinPage(<?=$page['gid']?>, '<?=$hash?>', '<?=$API->pageToGtype( $page['gid'] ); ?>')">join</a>
		</div>
<? } } ?>
	</div>
	<?php
}

function showPagePreview($page, $showJoin = true)
{
	global $API;

  if( !is_array( $page ) )
  {
    $page = $API->getPageInfo($page);
    if( sizeof( $page ) == 0 ) return;
    $page['friends'] = quickQuery( "select count(*) from page_members where gid='" . $page['gid'] . "'" );
  }

	$hash = md5($page['gid'] . microtime());
	$url = $API->getPageURL($page['gid'], $page['gname']);
  $gtype = $API->pageToGtype( $page['gid'] );
  $type = quickQuery( "select type from pages where gid='" . $page['gid'] . "'" );
  unset( $exnames );
  if( $type == PAGE_TYPE_VESSEL ) { $exnames = quickQuery( "select exnames from boats left join pages on pages.glink=boats.id where pages.gid='" . $page['gid'] . "'" ); }
	?>
	<div class="page_preview" id="gp-<?=$page['gid']?>">
		<div class="pic" style="width:53px;">
			<a href="<?=$url?>"><img width="48" height="48" src="<?=$API->getThumbURL(1, 48, 48, $API->getPageImage($page['gid'], $page['pid'] ) )?>" /></a>
		</div>
		<div class="info">
			<div class="title">
				<a href="<?=$url?>"><?=$page['gname']; if( isset( $exnames ) && $exnames != '' ) { echo ' <span style="font-weight:200;">ex. ' . $exnames . '</span>'; } ?></a>
				<?php
				if ($showJoin)
				{
					if (isset($page['isadded']))
					{
						echo '<div class="linkright"><a href="javascript:void(0);" onclick="javascript:addPageExistingMedia(\'' . $page['media']['type'] . '\', ' . $page['media']['id'] . ', ' . $page['gid'] . ');"><span id="addlink-' . $page['gid'] . $page['media']['type'] . $page['media']['id'] . '">';
						if ($page['isadded'])
							echo 'remove from page';
						else
							echo 'add to page';
						echo '</span></a></div>';
					}
					else
						echo '<div class="linkright" id="join-' . $hash . '"><a href="javascript:void(0);" onclick="javascript:joinPage(' . $page['gid'] . ', \'' . $hash . '\', \'' . $gtype . '\')">join</a></div>';
				}
				?>
			</div>
			<div class="cat">Category:&nbsp; <a href="#"><?=$page['catname']?></a></div>
			<div class="fr">Connections: <a href="#"><?=$page['friends']?></a></div>
		</div>
	</div>
	<?php
}


function showPagePreviewFull($page)
{
	global $API;
	$hash = md5($page['gid'] . microtime());
	$url = $API->getPageURL($page['gid'], $page['gname']);

  if( $page['gtype'] == "" )
    $gtype = $API->pageToGtype( $page['gid'] );
  else
    $gtype = $page['gtype'];

  unset( $exnames );
  if( $gtype == 'B' ) { $exnames = quickQuery( "select exnames from boats left join pages on pages.glink=boats.id where pages.gid='" . $page['gid'] . "'" ); }

  if( $page['catname'] == "" )
  {
    $catname = quickQuery( "select catname from categories where cat='" . $page['cat'] . "'" );
  }
  else
    $catname = $page['catname'];

  if( $page['subcatname'] == "" )
  {
    $subcatname = quickQuery( "select catname from categories where cat='" . $page['subcat'] . "'" );
  }
  else
    $subcatname = $page['catname'];

  if( $page['friends'] == "" )
  {
    $page['friends'] = quickQuery( "select COUNT(*) from page_members where gid='" . $page['gid'] . "' and admin=0" );
  }

  $img = $API->getPageImage($page['gid'], $page['pid'] );
  if( stristr( $img, "images" ) === false )
    $img = $API->getThumbURL(0, 48, 48, $img);
	?>
	<div class="page_preview" id="gp-<?=$page['gid']?>">
		<div class="pic">
			<a href="<?=$url?>"><img src="<?=$img?>" /></a>
		</div>
		<div class="info">
			<div class="title">
				<a href="<?=$url?>" style="font-size:10pt;"><?= cutOffText($page['gname'], 35); if( isset( $exnames ) && $exnames != '' ) { echo ' <span style="font-weight:200;">ex. ' . $exnames . '</span>'; } ?></a>

<?
if( $page['type'] == PAGE_TYPE_BUSINESS ) {

$ind = array();
$prod = array();
$catq = mysql_query( "select categories.cat, categories.catname, categories.industry from page_categories natural join categories where page_categories.gid='". $page['gid'] . "'" );
while( $catr = mysql_fetch_array( $catq ) )
{
  if( $catr['industry'] == PAGE_TYPE_BUSINESS )
    $ind[] = $catr;
  else
    $prod[] = $catr;
}
?>
        <div style="font-size:8pt; font-weight:200;">
          Industries:
<? for( $c = 0; $c < sizeof( $ind ); $c++ ) { if( $c > 0 ) echo ", "; ?><a href="/directory.php?search=&cat=<?=$ind[$c]['cat'];?>"><?=$ind[$c]['catname'];?></a><? } ?>
        </div>
        <div style="font-size:8pt; font-weight:200;">
          Products / Services:
<? for( $c = 0; $c < sizeof( $prod ); $c++ ) { if( $c > 0 ) echo ", "; ?><a href="/directory.php?search=&cat=<?=$prod[$c]['cat'];?>"><?=$prod[$c]['catname'];?></a><? } ?>
        </div>
<? } else { ?>
        <span style="font-size:8pt; font-weight:200;">
        - <a href="/directory.php?search=&cat=<?=$page['cat'];?>"><?=$catname?></a>
        <? if( $page['subcat'] > 0 ) { ?>- <a href="/directory.php?search=&subcat=<?=$page['subcat'];?>"><?=$subcatname?></a><? } ?>
  			<!--<div style="float:right;">Connections: <a href="<?=$url?>"><?=$page['friends']?></a></div>-->
        </span>
<? } ?>
			</div>
      <div style="color:#555;">
        <? echo cutOffText( $page['descr'], 140 ); ?>
      </div>
		</div>
	</div>
	<?php
}

function profileLink($user, $prefix = "", $you = "", $possession = false)
{
	global $API;
	
	if (empty($prefix))
		$prefix = "";
	else
		$prefix .= "_";
	
	if (!empty($you) && $user[$prefix . 'uid'] == $API->uid)
		$name = $you;
	else
	{
		$name = $user[$prefix . 'name'];
		if ($possession)
			$name .= "'s";
	}
	
	return '<a href="' . $API->getProfileURL($user[$prefix . 'uid'], $user[$prefix . 'username']) . '">' . $name . '</a>';
}

// can be called with params (pid, tid) or (type, name)
function createPageWithInfo($name, $tid = "")
{
	global $siteName;

  $cat = $tid;

  $type = quickQuery( "select industry from categories where cat='$cat'" );

  mysql_query("insert into pages (gname,type,cat) values ('" . addslashes($name) . "','$type',$cat)");

	$gid = mysql_insert_id();

	if ($gid)
	{
		// make the site admin the creator
		$uid_admin = quickQuery("select uid from users where username='$siteName'");
		mysql_query("insert into page_members (uid,gid,admin) values ($uid_admin,$gid,1)");
		
		// grab data for the page from sites
		forkHTTP("/pages/addpageinfo.php", array($gid, $tid));
	}
	
	return $gid;
}


function getGoogleBooksInfo($name)
{
	$xml = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", file_get_contents("http://books.google.com/books/feeds/volumes?q=" . urlencode($name)));
	$books = new SimpleXMLElement($xml, null, false);

	foreach ($books as $book) // loop through our books
	{
		if ($book->title)
		{
			$result['title'] = (string) $book->title;
			$result['descr'] = (string) $book->dcdescription;
			
			foreach ($book->link as $link)
			{
				$link = (array) $link;
				if (current(explode("/", $link['@attributes']['type'])) == "image")
				{
					$result['img'] = $link['@attributes']['href'];
					$result['img'] = replace_content_inside_delimiters("&zoom", "&", "=1", $result['img']);
					break;
				}
			}

			return $result;
		}
	}
}

function getIMDBInfo($m)
{
	include_once $_SERVER["DOCUMENT_ROOT"] . "/inc/imdb/imdb.class.php";
	include_once $_SERVER["DOCUMENT_ROOT"] . "/inc/imdb/imdbsearch.class.php";
	
	$search = new imdbsearch();
	$search->setsearchname($m);
	$results = $search->results();
	
	if ($results[0]->imdbID)
	{
		$imdb = new imdb($results[0]->imdbID);
		$movie['id'] = $results[0]->imdbID;
		$movie['title'] = str_replace("&#x27;", "'", $imdb->title());
		$movie['img'] = $imdb->photo(false);
		
		$plot = $imdb->plot();
		$i = strpos($plot[0], "<");
		$movie['descr'] = trim(substr($plot[0], 0, $i));
	}
	
	return $movie;
}

function max_key($array) {
foreach ($array as $key => $val) {
    if ($val == max($array)) return $key;
    }
}

function getTVDBInfo($s)
{
	$s = strtolower($s);
	$xml = simplexml_load_string(file_get_contents("http://www.thetvdb.com/api/GetSeries.php?seriesname=" . urlencode($s)));

	if ($xml->Series)
	{
		$words = count(explode(" ", $s));

		for ($i = 0; $i < count($xml->Series); $i++)
		{
			if (strpos(strtolower($xml->Series[$i]->SeriesName), $s) !== false)
				$match[$i] = true;//$words / count(explode(" ", $xml->Series[$i]->SeriesName));
		}

		if ($match)
		{
			//arsort($match);

			foreach (array_keys($match) as $max)
			{
				if ((string) $xml->Series[$max]->banner == "")
					continue;
				
				$title = (string) $xml->Series[$max]->SeriesName;
				$descr = str_replace(chr(226) . chr(128) . chr(153), "'", (string) $xml->Series[$max]->Overview);
				$banner = (string) $xml->Series[$max]->banner;
				$id = (string) $xml->Series[$max]->seriesid;
				
				$url = "http://www.thetvdb.com/data/series/$id/banners";
				$xml = simplexml_load_string(file_get_contents($url));
				
				//get dimensions of images
				for ($i = 0; $i < count($xml->Banner); $i++)
				{
					$dims = explode("x", (string) $xml->Banner[$i]->BannerType2);

					if (intval($dims[1]) == 0) continue;
					$fanart[$i] = $dims[0] / $dims[1];
				}

				//sort by ratio - pick a low ratio to get a tall image
				asort($fanart);
				
				foreach (array_keys($fanart) as $i)
				{
					if ((string) $xml->Banner[$i]->BannerPath == "")
						continue;
					
					$fanart = (string) $xml->Banner[$i]->BannerPath;
					break;
				}
				
				return array("id" => $id, "title" => $title, "descr" => $descr, "img" => "http://thetvdb.com/banners/" . ($fanart ? $fanart : $banner));
			}
		}
	}
}

function forkHTTP($uri, $args)
{
	global $site;

	foreach ($args as $arg)
		$args2[] = '"' . addslashes($arg) . '"';
	
	$cmd = "/usr/bin/php " . $_SERVER['DOCUMENT_ROOT'] . "$uri $site " . implode(" ", $args2) . " > /dev/null 2>&1 &";
	file_put_contents("/tmp/cmd", $cmd);
	exec("export LD_LIBRARY_PATH=/usr/local/lib; " . $cmd);
}

//about me page
function displayPersonalInfo($piType, &$info)
{
	global $API;

	$items = 0;

	if (is_array($info))
	foreach ($info as $item)
	{
        $item['name'] = iconv('windows-1251', 'UTF-8', $item['name']);
		if ($piType['tid'] != $item['tid']) continue;

		if ($items++ > 0)
      if( $piType['tid'] == LIC_TID || $piType['tid'] == DEGREE_TID )
        echo "<br/>";
//      else
//        echo ", ";

		if ($item['gid'])
			echo '<div><a href="' . $API->getPageURL($item['gid'], $item['name']) . '">' . $item['name'] . '</a></div>';
		else
			echo '<div>'.$item['name'].'</div>';
	}
	
	return $items;
}

function showSuggestFriends($uid)
{
	global $API;
	$pics = array();

	$x = mysql_query("select uid,pic from users where active=1 and uid != " . $API->uid . " and pic>0 order by rand() limit 2");
	while ($y = mysql_fetch_array($x, MYSQL_ASSOC))
		$pics[] = $API->getThumbURL(1, 48, 48, $API->getUserPic($y['uid'], $y['pic']));
?>
<div class="getconnected" style="padding-right: 0;" onclick="javascript:loadShare('suggest', 1, <?=$uid?>);">
	<div style="float: left; width: 55px;">
		<img height="48" width="48" src="<?=$pics[0]?>" alt="" />
		<img height="48" width="48" src="<?=$pics[1]?>" style="padding-top: 10px;" alt="" />
	</div>
	<div style="float: left; width: 113px;">
		<div style="font-weight: bold; font-size: 9pt; padding-bottom: 10px;">Suggest connections!</div>
		<span style="color: #555;">Do you know someone who may know or you think should know this user?</span>
		<div style="padding-top: 10px; text-decoration: underline;">Help them connect!</div>
	</div>
	<div style="clear: both;"></div>
</div>
<?php
}

function showSuggestPage($gid)
{
	global $API;
	$pics = array();

	$x = mysql_query("select uid,pic from users where active=1 and uid != " . $API->uid . " order by rand() limit 2");
	while ($y = mysql_fetch_array($x, MYSQL_ASSOC))
		$pics[] = $API->getThumbURL(1, 48, 48, $API->getUserPic($y['uid'], $y['pic']));
?>
<div class="getconnected" style="padding-right: 0;" onclick="javascript:loadShare('suggestpage', 1, <?=$gid?>);">
	<div style="float: left; width: 55px;">
		<img height="48" width="48" src="<?=$pics[0]?>" alt="" />
		<img height="48" width="48" src="<?=$pics[1]?>" style="padding-top: 10px;" alt="" />
	</div>
	<div style="float: left; width: 113px;">
		<div style="font-weight: bold; font-size: 9pt; padding-bottom: 10px;">Suggest connections!</div>
		<span style="color: #555;">Do you know someone who works / worked here or might want to connect with this page?</span>
		<div style="padding-top: 10px; text-decoration: underline;">Help them connect!</div>
	</div>
	<div style="clear: both;"></div>
</div>
<?php
}

function showNoAppRequired()
{
	?>
	<div class="getconnected" onclick="javascript:window.open('/email.php?1', 'Email','location=0,status=0,scrollbars=1,width=580,height=600');">
		<div style="float: left; width: 105px;">
			<div class="smtitle" style="padding-bottom: 10px;">No app required!</div>
			<span style="color: #555;">Share media on the go from your phone in real time, using e&#x2011;mail!</span>
			<div style="padding-top: 10px; text-decoration: underline; font-size: 10pt;">learn how &gt;&gt;</div>
		</div>
		<div style="float: right; width: 55px; padding-top: 5px; overflow: hidden;">
			<img src="/images/iphone.png" alt="" />
		</div>
		<div style="clear: both;"></div>
	</div>
	<?php
}

function showGetConnected()
{
	?>
	<div class="getconnected" onclick="javascript:location='/findpeople.php';">
		<div class="sites">
			<div><img src="/images/facebook.png" alt="" />Facebook</div>
			<div><img src="/images/twitter.png" alt="" />Twitter</div>
			<div><img src="/images/hotmail.png" alt="" />Hotmail</div>
			<div><img src="/images/msn.png" alt="" />MSN</div>
			<div><img src="/images/yahoo.png" alt="" />Yahoo</div>
			<div style="padding: 0px;"><img src="/images/gmail.png" alt="" />Gmail</div>
		</div>
		<div style="float: right; width: 90px; padding-top: 11px;">
			<div class="smtitle" style="padding-bottom: 3px;">Get Connected!</div>
			<span style="color: #555; line-height: 12pt;">Find people you know by using the <?=$siteName?> connection finder.</span>
			<div style="padding-top: 3px;"><u>Give it a try!</u></div>
		</div>
		<div style="clear: both;"></div>
	</div>
	<?php
}

function showCompleteProfileInformationWide()
{
	global $API, $siteName;
	?>
		<div style="margin-top: 3px; margin-bottom:5px; border: 1px solid #d8dfea; padding: 5px; font-size: 8pt; background: #F2FBFE; cursor: pointer;" onclick="javascript:location='<?=$API->getProfileURL()?>/about';">
			<div class="smtitle" style="padding-bottom: 10px;">Complete your profile information <span>(recommended)</span></div>
			<div style="float: left; width: 205px;">
			Filling out your profile information will help people find you on <?=$siteName?>.&nbsp; For example, by completing the interests section, you'll be able to connect with others who like what you like.
			</div>
			<div style="float: right; padding-right: 10px; text-align: center; font-size: 9pt; color: #326798; text-decoration: underline; line-height: 30px;">
				<img src="/images/profile_large.png" /><br />
				start here &gt;&gt;
			</div>
			<div style="clear: both;"></div>
		</div>
	<?php
}

function showCreatePageWide()
{
	global $API, $siteName;
	?>
		<div style="margin-top: 3px; border: 1px solid #d8dfea; padding: 5px; font-size: 8pt; background: #F2FBFE; cursor: pointer;" onclick="javascript:location='/pages/page_create.php';">
			<div class="smtitle" style="padding-bottom: 10px;">Set up your Page today <span>(recommended)</span></div>
			<div style="float: left; width: 190px;">
        Creating a page has many advantages. It brings you closer to customers, peers and followers. It provides the ability to be heard, discovered and so much more.
			</div>
			<div style="float: right; padding-right: 10px; text-align: center; font-size: 9pt; color: #326798; text-decoration: underline; line-height: 30px;">
				<img src="/images/profile_large.png" /><br />
				start here &gt;&gt;
			</div>
			<div style="clear: both;"></div>
		</div>
	<?php
}

function showCreatePageWide2()
{
	global $API, $siteName;
	?>
		<div style="margin-top: 3px; border: 1px solid #d8dfea; padding: 5px; font-size: 8pt; background: #F2FBFE; cursor: pointer;" onclick="javascript:location='/pages/page_create.php';">
			<div class="smtitle" style="padding-bottom: 10px;">About <?=$siteName?> Pages</div>
			<div style="float: left; width: 190px;">
        The Business Directory is made up of premium pages from <?=$siteName?>. It is the most effective and recognized way to get your company out in front of the decision makers that matter most.
			</div>
			<div style="float: right; padding-right: 10px; text-align: center; font-size: 9pt; color: #326798; text-decoration: underline; line-height: 30px;">
				<img src="/images/profile_large.png" /><br />
				start here &gt;&gt;
			</div>
			<div style="clear: both;"></div>
		</div>
	<?php
}


function showCompleteProfileInformation()
{
	global $API, $siteName;
?>
	<div class="getconnected" onclick="javascript:location='<?=$API->getProfileURL() . "/about"?>';">
		<div style="float: left; width: 115px;">
			<div class="smtitle">Complete your profile information</div>
			<span style="color: #555;">(recommended)</span>
		</div>
		<div style="float: right; padding: 5px 5px 0 0;">
			<img src="/images/profile_large.png" alt="" />
		</div>
		<div style="clear: both;"></div>

		<div style="padding-top: 10px; color: #555;">
			Filling in your profile information will help people find you on <?=$siteName?>.&nbsp; For example,
			by completing your work history, you'll be able to connect with others who do what you do.
		</div>
		
		<div style="padding-top: 10px; text-decoration: underline; font-size: 10pt;">start here &gt;&gt;</div>
	</div>
<?php
}

function showEditWork($work)
{
  $new = false;
	if ($work['start'] == null)
	{
		$work['start'] = -1;
		$work['stop'] = -1;
		$allowRemove = false;
    $new = true;

    if( $work['vessel'])
      $type = PAGE_TYPE_VESSEL;
    else
      $type = PAGE_TYPE_BUSINESS;
	}
	else
  {
		$allowRemove = true;
    $type = quickQuery( "select type from pages where gid='" . $work['employer'] . "'" );
  }

	?>
  <div id="work-container-<?=$work['wid']?>">
	<div style="float: left; font-size: 9pt; padding-right: 15px; height: 150px;">
    <?php
		for ($j = 0; $j < 2; $j++)
		{
			if ($j == 0)
				echo '<div class="smtitle2">Period:</div>';
			else
				 echo '<div style="padding: 5px; text-align: center;">to</div><div id="enddate-' . $work['wid'] . '" style="padding-bottom: 5px;' . ($work['present'] == "1" ? ' display: none;' : '') . '">';
			echo '<select id="month-' . $j . '-' . $work['wid'] . '" style="margin-right: 5px;"><option value=""></option>';
			for ($i = 1; $i <= 12; $i++)
				echo '<option ' . (date("n", $j == 0 ? $work['start'] : $work['stop']) == $i && $work['start'] >= 0 ? "selected" : "") . ' value="' . $i . '">' . date("F", strtotime($i . "/01/2010")) . '</option>';
			echo '</select>';

			echo '<select id="year-' . $j . '-' . $work['wid'] . '"><option value=""></option>';
			$y = date("Y");
			for ($i = 1970; $i <= $y + 1; $i++)
				echo '<option ' . (date("Y", $j == 0 ? $work['start'] : $work['stop']) == $i && $work['start'] >= 0 ? "selected" : "") . ' value="' . $i . '">' . $i . '</option>';
			echo '</select>';
		}
		echo '</div>'; //close enddate div
		?>
		<div>
			<input type="checkbox" <?=$work['present'] == "1" ? "checked" : ""?> id="present-<?=$work['wid']?>" onchange="javascript:document.getElementById('enddate-<?=$work['wid']?>').style.display = this.checked ? 'none' : 'inline';" /> I currently work here
		</div>
	</div>
	<div class="float: left; height: 150px;">
		<div style="float: left;">
			<div class="smtitle2">

      <? if( $type == PAGE_TYPE_VESSEL ) { echo "Vessel Name:"; $search_param = 2; } else { echo "Employer:"; $search_param = 1; }?>
      </div>

      <div style="position:relative;">
 			  <input type="text" class="autocomplete" data-autocomplete="<?=$type == PAGE_TYPE_VESSEL ? 'get_vessel' : 'get_business'?>" data-ac_func="get_employer_or_vessel" style="width: 181px;" id="employer-<?=$work['wid']?>" value="<?=$work['employer_name']?>" <?/*onkeyup="javascript:searchKeypress(event,this,'W<?=$work['wid']?>',<?=$search_param?>);" Xonblur="javascript: searchLostFocus(-1,'W<?=$work['wid']?>');"*/?>/>
        <div id="suggestionBoxW<?=$work['wid']?>" style="padding:0px; position: absolute; left:0px; top:22px; width:288px; height:205px; z-index:101; display:none;"></div>
      </div>
		</div>
		<div style="float: left; padding-left: 10px;">
			<div class="smtitle2">Occupation/Position:</div>
			<input type="text" style="width: 181px;" id="occupation-<?=$work['wid']?>" value="<?=$work['occupation_name']?>" />
		</div>
		<div style="float: left; padding-top: 5px; display: none;">
			<input type="checkbox" class="isvessel" <?=$type == PAGE_TYPE_VESSEL ? 'checked="checked"' : ''?> id="isvessel-<?=$work['wid']?>" /><span class="smtitle2" style="font-weight: normal;"> This is a vessel
		</div>
		<div style="float: left; padding-top: 5px;">
			<div class="smtitle2">Job Description:</div>
			<textarea id="descr-<?=$work['wid']?>" style="width: 374px;"><?=$work['descr']?></textarea>
		</div>
		<div style="float: left; padding-top: 5px;">
			<div class="smtitle2">Company Website:</div>
			<input type="text" style="width: 181px;" id="www-<?=$work['wid']?>" value="<?=$work['www']?>" />
		</div>

		<div style="float: right; padding-top: 22px; font-size: 9pt;<? if (!$allowRemove) echo " display:none;"; ?>" id="remove2-<?=$work['wid']?>">
			<input type="checkbox" id="remove-<?=$work['wid']?>"/> remove job
		</div>
		<div style="clear: both;"></div>
	</div>

	<div style="clear: both; height: 25px;"></div>
  </div>
	<?php
}

function showVideoPlayer($newWidth, $newHeight, $media, $place = false)
{
  $format = quickQuery( "select format from videos where id='" . $media['id'] . "'" );
  $containerUrl = quickQuery( "select container_url from users where uid='" . $media['uid'] . "'" );
  switch( $format )
  {
    case 1:
    ?>
      <video width="<?=$newWidth?>" height="<?=$newHeight?>" controls>
        <source src="<?= $containerUrl . '/' .  $media['hash']?>.mp4" type="video/mp4">
        <source src="<?= $containerUrl . '/' .  $media['hash']?>.ogv" type="video/ogg">
        <object id="flowplayer" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="<?=$newWidth?>" height="<?=$newHeight?>">
            <param name="movie" value="/flowplayer/flowplayer.swf" />
            <param name="flashvars" value='config={"clip":"<?= $containerUrl . '/' .  $media['hash']?>.flv"}'/>
        </object>
      </video>
    <?
    break;

    default:
    case 0:
  	?>
  	<div style="width: <?=$newWidth?>px; height: <?=$newHeight?>px;" id="player"></div>
  	<script language="javascript" type="text/javascript">
    <? if($place == "popup"){echo chr(2);} ?>
  	fplayer = flowplayer("player", {src: '/flowplayer/flowplayer.swf', wmode: 'transparent'}, {

  		plugins: {
  			pseudo: { url: 'flowplayer.pseudostreaming.swf' }
  		},

  		clip: {
  			autoBuffering: true,
  			provider: 'pseudo',
  			autoPlay: false,
  			url: '<?= $containerUrl . '/' .  $media['hash']?>.flv'
  		}
  	});
    <? if($place == "popup"){echo chr(2);} ?>
  	</script>
  	<?
    //exit("regular video. format:".$format."containerUrl:".$containerUrl);
    break;


  }
}

function isEmailAddressValid($email)
{
	return filter_var($email, FILTER_VALIDATE_EMAIL);
}

function sendEmail($from, $to, $subj, $body)
{
	emailAddress($to, $subj, $body, $from);
}

function str_lreplace($search, $replace, $subject)
{
	return substr_replace($subject, $replace, strrpos($subject, $search), strlen($search));
}

function scaleImage($width, $height, $target_width, $target_height, $crop = false)
{
	//$target_width = $thumb[0];//257;
	//$target_height = $thumb[1];//222;
	$target_ratio = $target_width / $target_height;

	$img_ratio = $width / $height;
    $widthOffset = 0;
    $heightOffset = 0;

	if ($crop)
	{
		if ($target_ratio > $img_ratio) {
			//too tall, we should crop the height......amplify the target dimensions to the width and take that height
			$newHeight = $target_height / $target_width * $width;
			$heightOffset = abs($newHeight - $height);
		} else {
			$newWidth = $target_width / $target_height * $height;
			$widthOffset = abs($newWidth - $width);
		}
	}
	else
	{
		if ($width > $target_width || $height > $target_height)
		{
			$tr = $target_width/$target_height;
			$ir = $width/$height;

			if ($ir > $tr) // too wide
				$target_height = $target_width / $ir;
			else // too tall
				$target_width = $target_height * $ir;
		}
		else
		{
			$target_width = $width;
			$target_height = $height;
		}
		$widthOffset = 0;
		$heightOffset = 0;
	}
	
	return array($target_width, $target_height, $widthOffset, $heightOffset);
}

function convertGender($x)
{
	if ($x == "M")
		return "Male";
	elseif ($x == "F")
		return "Female";
}

function convertRelationship($x)
{
	if ($x == "W")
		return "Work";
	elseif ($x == "N")
		return "Networking";
	elseif ($x == "F")
		return "Friends";
	elseif ($x == "D")
		return "Dating";
	elseif ($x == "R")
		return "Relationship";
}

function addhttp($url) {
    if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
        $url = "http://" . $url;
    }
    return $url;
}

function startsWith($x, $y)
{
	return substr($x, 0, strlen($y)) == $y;
}

function convertWordSite($x)
{
	global $site;
	
	if (!is_numeric($x))
	{
		switch ($x)
		{
			case "facebook":
			$ssite = SITE_FACEBOOK;
			break;
			
			case "twitter":
			$ssite = SITE_TWITTER;
			break;
			
			case $ssite;
			case "ours":
			$ssite = SITE_OURS;
			break;
			
			case "msn":
			case "hotmail":
			$ssite = SITE_HOTMAIL;
			break;
			
			case "yahoo":
			$ssite = SITE_YAHOO;
			break;
			
			case "gmail":
			$ssite = SITE_GMAIL;
			break;
			
			case "manual":
			$ssite = SITE_MANUAL;
			break;
		}
	}
	else
	{
		switch ($x)
		{
			case SITE_FACEBOOK:
			$ssite = "facebook";
			break;
			
			case SITE_TWITTER:
			$ssite = "twitter";
			break;
			
			case SITE_OURS:
			$ssite = $site;
			break;
			
			case SITE_HOTMAIL:
			$ssite = "hotmail";
			break;
			
			case SITE_YAHOO;
			$ssite = "yahoo";
			break;
			
			case SITE_GMAIL;
			$ssite = "gmail";
			break;

			case SITE_MANUAL;
			$ssite = "manual";
			break;
		}
	}
	
	return $ssite;
}

function loadJS($scripts)
{
	$sc = array_unique($scripts);
/*
	if (count($sc) < count($scripts))
	{
		echo "Tried to include js twice<!--";
		print_r($sc);
		print_r($scripts);
		echo "-->";
	}
*/
	foreach ($scripts as $js)
	{
		echo "\t";
		$filename = current(explode("?", $js));
		if (substr($filename, 0, 4) == "http")
			echo '<script src="' . $js . '" type="text/javascript"></script>';
		else
		{
			$m = filemtime($_SERVER['DOCUMENT_ROOT'] . $filename);
			//echo '<script src="' . $js . (strpos($js, "?") ? "&" : "?") . 'ver=' . $m . '" type="text/javascript"></script>';
echo '<script src="' . $js . '" type="text/javascript"></script>';
		}
		echo "\n";
	}
}

function showUserWithItems($user, $vidCount, $picCount = null, $showActionLinks = null)
{
	if (isset($picCount))
		showUserWithItems_old($user, $vidCount, $picCount, $showActionLinks); //found on profile, page
	else
		showUserWithItems_custom($user, $vidCount); //found elsewhere
}

function showNavDrop($title, $item, $style)
{
	$id = md5(microtime());
	?>
	<div class="navdropcontain" onmouseover="javascript:showDropDown('dd-<?=$id?>');" onmouseout="javascript:hideDropDown('dd-<?=$id?>');">
		<?=$title?>
		<div id="dd-<?=$id?>" class="navdrop" style="<?=$style?>">
			<?php
			foreach ($item as $menuitem)
			{
				if (substr($menuitem[1], 0, 11) == "javascript:")
					$menuitem[1] = 'javascript:void(0);" onclick="' . $menuitem[1];
				echo '<div onmouseup="javascript:if (event.button == 0) actionClicked(this);" class="item"><a href="' . $menuitem[1] . '">';
        if( $menuitem[2] != "" )
          echo '<img src="/images/' . $menuitem[2] . '.png" alt="" />';
        echo $menuitem[0] . '</a></div>';
			}
			?>
		</div>
	</div>
	<?php
}

function showUserWithItems_custom($user, $items)
{
	global $API, $site;
  //Width was 479, widened to 550
	?>
	<div style="width:560px; overflow: hidden; z-index:100;">
	<div class="phead_wrapper">
		<div class="phead">
			<div class="username">
				<a href="<?=$API->getProfileURL($user['uid'], $user['username'])?>"><?=$user['name']?></a>
			</div>
		<div style="height:28px; margin-right:0px; margin-left:-15px; background-image: url(/images/pix_d8dfea.png); background-position: 0 26px; background-repeat: repeat-x;"></div> <!--Removed: width:590px;  on 3/11/2012 for advertising popup (targeting)-->
		</div>
		<ul class="quicknav">
			<?php
			foreach ($items as $item)
			{
				if (substr($item[2], 0, 11) == "javascript:") //hackish but gets the job done
					$item[2] = "javascript:void(0);\" onclick=\"" . $item[2];
				
				echo '<li style="background-image: url(' . $item[0] . ');"><a href="' . $item[2] . '">' . $item[1] . '</a></li>';
			}
			?>
		</ul>
	</div>
	</div>
	<?php
}

function showMessagePreview($msg)
{
	global $API;

	$profileURL = $API->getProfileURL($msg['uid']);
	?>
	<div id="msg<?=$msg['mid']?>" class="msgpreview<?=$msg['unread'] == 0 ? "" : " msgpreview_on"?>">
		<div class="userpic">
			<a href="<?=$profileURL?>"><img width="48" height="48" src="<?=$API->getThumbURL(1, 48, 48, $API->getUserPic($msg['uid'] ))?>" alt="" /></a>
		</div>
		<div class="msgcontainer">
			<div class="border">
				<div class="date">
					<?=date("F j, Y", strtotime($msg['created']))?> <a href="<?=$profileURL?>"><?=quickQuery( "select name from users where uid='" . $msg['uid'] . "'" ); ?></a>
				</div>
				<div class="icons">
					<img src="/images/email<?=$msg['unread'] == 0 ? "_open" : ""?>.png" alt="" /><img src="/images/checkbox.png" id="chk<?=$msg['mid']?>" onclick="javascript:checkMsg(<?=$msg['mid']?>, 0, -1);" style="cursor: pointer;" alt="" /><a href="javascript:void(0);" onclick="javascript:deleteMsg(<?=$msg['mid']?>, 1);">X</a>
				</div>
			</div>
			<div class="msg" onclick="javascript:location='/messaging/read.php?mid=<?=$msg['mid']?>';">
				<span><?=$msg['subj']?></span><br />
				<div><?=nl2br($msg['comment']);?></div>
			</div>
		</div>
		<div style="clear: both;"></div>
	</div>
	<?php
}

function privacyToWord($p)
{
	switch ($p)
	{
		case PRIVACY_EVERYONE:
		return "The world";
		
		case PRIVACY_FRIENDS:
		return "Only my connections";

		case PRIVACY_SELECTED:
		return "Connections selected";

		case PRIVACY_SELF:
		return "Only me";
	}
}

function showUserWithItems_old($user, $vidCount, $picCount, $showActionLinks = null)
{
	global $API, $site;

	if (is_null($showActionLinks))
		$showActionLinks = $site == "s" && $API->isLoggedIn() && $user['uid'] != $API->uid && $user['gid'] == 0;

	if ($user['gid'] == 0)
		$url = $API->getProfileURL($user['uid'], $user['username']);
	else
  {
    $member = quickQuery( "select count(*) from page_members where gid='" . $user['gid'] . "' and uid='" . $API->uid . "'" );
		$url = $API->getPageURL($user['gid'], $user['gname']);
  }
	?>
	<div style="width: 750px; overflow-x: visible; position: relative; z-index:100;">    <!--563px-->
		<div class="phead">
			<div class="username">
				<div>
					<?php
					if ($user['gid'] == 0)
					{
						echo '<a href="' . $API->getProfileURL($user['uid'], $user['username']) . '"><h1 style="font-size:18pt;">' . $user['name'] . '</h1></a>';
					}
					else
					{
						echo '<div style="float:left;"><a style="' . (strlen($user['gname']) > 30 ? ' font-size:12pt;' : '' ) . '" href="' . $API->getPageURL($user['gid'], $user['gname']) . '"><h1 style="font-size:18pt;">' . $user['gname'] . '</h1></a></div>';

						if ($user['boat']['exnames'])
							echo "<div style=\"float:left;\"><span>(ex. " . $user['boat']['exnames'] . ")</span></div>";

						if ($user['boat']['length'])
							echo "<div style=\"float:left;\"><span>{$user['boat']['length']} ft. / " . round($user['boat']['length'] / 3.2808399, 2) . " m.</span></div>";

					}
					?>
				</div>
			</div>
<?
if( $user['gid'] > 0 ) {
//Check to see if this is a company, if so, add "claim this company" and "verified" in the header
  $q = mysql_query( "select type,verified from pages where (type=" . PAGE_TYPE_BUSINESS . " or verified=1) and gid='" . $user['gid'] . "'" );
  if( $r = mysql_fetch_array( $q ))
  {
    $data = queryArray( "select catname, industry from categories where cat=" . $r['type'] );

    $type = $data['catname'];
    if( $data['industry'] == 1 ) $type = "company";
    //$type = strtolower( $type );
    if( $r['verified'] ) echo '<div style="float:left; margin-left:15px; width:160px;"><div style="float:left; margin-top:-5px; margin-left:0px; padding-top:3px;"><img src="/images/checkmark_sticker_sm.png" width="21" height="21" alt="" /></div><div style="font-size:11px;"><i>Verified ' . $type . '</i></div></div>';
    else
    {
      $admin = quickQuery( "select admin from page_members where gid='" . $user['gid'] . "' and uid='" . $API->uid . "'" );
?>
    <div style="float:left; height:18px; background-color: #FFFFCC; font-size:11px; padding:1px; margin-left:20px;">
      <div style="float:left;">is this your <? echo $type ?>?
        <? /*
          if( quickQuery( "select www from pages where gid='" . $user['gid'] . "'" ) == "" ) { ?>
          <a href="javascript:void(0);" style="font-size:10px; color:#326798; text-decoration:underline; margin-left:11px; margin-right:10px;" onclick="javascript:if( !pageMember ) { alert('You must join this page before you can claim it.'); return false; } else claimThisPage(<? echo $user['gid'] ?>);">
        <? } else */ { ?>
          <a href="/pages/claim_company2.php?gid=<? echo $user['gid'] ?>" style="font-size:10px; color:#326798; text-decoration:underline; margin-left:11px; margin-right:10px;" onclick="javascript: if( !pageMember ) { alert('You must join this page before you can claim it.'); return false; }">
        <? } ?>
        <? if($admin) echo "verify"; else echo "claim"; ?> / edit page</a></div>
      <div style="float:left;"><a href="/pages/claim_company.php?gid=<? echo $user['gid'] ?>"><img src="/images/help.png" width="16" height="16" alt="" style="margin-top:1px;"/></a></div>
    </div>
<?
    }
  }
}
else
{
  //If this is not a company, add "My Pages" drop down here.
  if(quickQuery( "select count(*) from page_members where uid='" . $user['uid'] . "' and admin=1" ) > 0 || $user['uid'] == $API->uid )
  {
?>
    <div style="float:left; height:18px; font-size:9pt; padding:1px; margin-left:15px;">
      <div class="navdropcontain" onmouseover="javascript:showDropDown('dd-mypages');" onmouseout="javascript:hideDropDown('dd-mypages');" style="position:relative;">
    		<div id="dd-mypages" class="navdrop" style="width: 252px; top: 20px; left: 0px;">
          <? include "../profile/my_pages.php"; ?>
        </div>
        <div style="float:left"><img src="/images/page.png" width="16" height="16" alt="" /></div>
        <div style="float:left; padding-left:3px;"><?= current( explode( " ", $user['name'] ) ); ?>'s Pages</div>
        <div style="float:left; padding-left:3px; padding-top:7px;"><img src="/images/arrow_down.png" width="9" height="8" alt="" /></div>
      </div>
    </div>
<?
  }
}

?>
      <div style="height:28px; background-image: url(/images/pix_d8dfea.png); background-position: 0 26px; background-repeat: repeat-x; width:570px; margin-left:-15px;"></div>
		</div>

		<div style="padding-left: 10px; clear:both; padding-top:0px; margin-top:0px; width:550px;">
			<div onclick="javascript:document.location='<?=$url?>/about';" class="pheaditem" style="float: left; width: 125px;">
				<div class="caption" style="background-image: url(/images/user.png);"> <a href="<?=$url?>/about"><?=$user['gid'] == 0 ? "CV/Resume" : "about"?></a></div>
			</div>
      <? if( $API->isLoggedIn() || $user['gid'] == 15528 ) { ?>
  			<div onclick="javascript:document.location='<?=$url?>/logbook';" class="pheaditem" style="float: left; width: 125px;">
      <? } else { ?>
  			<div onclick="javascript: getStarted3(); return false;" class="pheaditem" style="float: left; width: 125px;">
      <? } ?>

			<?php if ($site == "s" && ($API->userHasAccess($user['uid'], PRIVACY_LOG ) || $user['gid'] > 0 ) ) { ?>
				<div class="caption" style="background-image: url(/images/book_open.png);"> <a href="<?=$url?>/logbook"><?=$user['gid'] == 0 ? "" : "page "?>log book</a>
        </div>
			<?php } ?>
			</div>

			<div <? if( ($user['gid'] == 0 || $member ) && $API->isLoggedIn() ) { ?>onclick="javascript:document.location='<?=$url?>/videos';" <? } ?> class="pheaditem">
      <?
      if( ($user['gid'] == 0 && $API->userHasAccess($user['uid'], PRIVACY_VIDEOS ) ) || $user['gid'] > 0 )
      {
        if( $API->isLoggedIn() ) { ?>
				<div class="caption" style="background-image: url(/images/television.png);"> <a href="<?=$url?>/videos" <? if( false && $user['gid'] > 0) { ?>onclick="javascript:if( !pageMember ) { joinHTML = '<div class=\'bluebutton\' style=\'cursor: default;\'>Joined</div>'; joinPage(<?=$user['gid']?>, 'side', '<?=$API->pageToGtype( $user['gid'] ); ?>'); return false; }"<? } ?>>videos</a></div><div class="number"><?=number_format($vidCount)?></div>
        <? } else { ?>
				<div class="caption" style="background-image: url(/images/television.png);"> <a href="javascript:void(0);" onclick="javascript: getStarted3(); return false;">videos</a></div><div class="number" style="height:25px;"></div>
        <?
        }
      }
      ?>
			</div>

			<div <? if( ($user['gid'] == 0 || $member) && $API->isLoggedIn() ) { ?>onclick="javascript:document.location='<?=$url?>/photos';" <? } ?>class="pheaditem">
      <?
      if( ($user['gid'] == 0 && $API->userHasAccess($user['uid'], PRIVACY_PHOTOS ) ) || $user['gid'] > 0 )
      {
        if( $API->isLoggedIn() ) { ?>
				<div class="caption" style="background-image: url(/images/images.png);"> <a href="<? echo $url . "/photos"; ?>" <? if( false && $user['gid'] > 0 ) { ?>onclick="javascript:if( !pageMember ) { joinHTML = '<div class=\'bluebutton\' style=\'cursor: default;\'>Joined</div>'; joinPage(<?=$user['gid']?>, 'side', '<?=$API->pageToGtype( $user['gid'] ); ?>'); return false; }"<? } ?>>photos</a></div><div class="number"><?=number_format($picCount)?></div>
        <? } else { ?>
				<div class="caption" style="background-image: url(/images/images.png);"> <a href="javascript:void(0);" onclick="javascript: getStarted3(); return false;">photos</a></div>
        <?
        }
      }
      ?>
			</div>
		</div>

		<?php /* if ($showActionLinks) { ?>
			<div style="position: absolute; top: 4px; left: 0; width: 560px;">
				<div style="float: right;">
					<div class="pheaditem" style="float: left; width: 175px;">
						<div class="caption" style="background-image: url(/images/user_add.png);"><?=friendLink($user['uid'])?></div>
					</div>
					<div class="caption" style="background-image: url(/images/email_add.png);"><a href="javascript:void(0);" onclick="javascript:showSendMessage(<?=$user['uid']?>, '<?=$user['name']?>', '<?=$API->getThumbURL(0, 85, 128, $API->getUserPic($user['uid'], $user['pic']))?>');">Send message</a></div>
				</div>
			</div>
		<?php } */?>
	</div>
  <div style="clear:both;"></div>
	<?php
}

function showProfileHead($user, $vidCount, $picCount, $showActionLinks = true)
{
	global $API;
	?>
		<div style="width: 53px; float: left;">
			<a href="<?=$API->getProfileURL($user['uid'], $user['username'])?>"><img src="<?=$API->getThumbURL(1, 48, 48, $API->getUserPic($user['uid'], $user['pic']))?>" alt="" style="height: 48px; width: 48px;" /></a>
		</div>
		<div style="float: left; ">
			<?php showUserWithItems($user, $vidCount, $picCount, $showActionLinks); ?>
		</div>
	<?php
}

function showDidYouKnow()
{
	global $siteName, $API;

	if ($API->isLoggedIn() || $_COOKIE['hide-nag-didyouknow'] == 1)
		return;
	?>
	<div id="nag-didyouknow" class="nagbox">
		<div style="width: 390px;">
			Did you know, if you have a Facebook or Twitter account, then you already have a <?=$siteName?> account?&nbsp; <a href="/?login" style="color: #0080ff;">Just click here &gt;&gt;</a>
		</div>
		<div>
			<span onclick="javascript:hideNag('didyouknow');">X</span>
		</div>
		<div style="clear: both; float: none;"></div>
	</div>
	<?php
}

function showHowAbout()
{
	global $siteName;
	?>
	<div style="background: #FFFFCC; padding: 15px 90px; color: #555; width: 300px; margin: 0 auto; font-size: 10pt;">
		<div style="height: 20px; font-size: 12pt; color: #0080ff; font-weight: bold;">
			How about an extra $100.00 a month?
		</div>
		Invite your friends to <?=$siteName?>, and we'll send you a $100.00 gift card!&nbsp; <a href="/invite.php" style="color: #0080ff;">Go to the invite page now&nbsp;&gt;&gt;</a>
	</div>
	<?php
}

function showFBBlurb($user, $forceRefresh = false) //displays info about user's fb profile
{
	global $API;
	$fbinfo = $API->getFBUserInfo($user['uid'], $forceRefresh);

	?>
		<div class="blurb">
			<div class="viewprofile"><a href="http://facebook.com/profile.php?id=<?=$user['fbid']?>" target="_new"><img src="/images/f.png" alt="" /> view profile</a></div>
			<?php if ($fbinfo['location']) { ?><span>Current City:</span>&nbsp; <?=$fbinfo['location']?><br /><?php } ?>
			<?php if ($fbinfo['status']) { ?><span>Status:</span>&nbsp; <?=findLinks($fbinfo['status'])?><br /><?php } ?>
			<div style="height: 7px;"></div>
			<?=$fbinfo['friends']?> <a href="http://www.facebook.com/friends/?id=<?=$user['fbid']?>" target="_new">friends</a>
		</div>
	<?php

	return $fbinfo;
}

function showTWBlurb($user, $forceRefresh = false) //displays info about user's tw profile
{
	global $API;
	$twinfo = $API->getTWUserInfo($user['uid'], $forceRefresh);
	
	?>
		<div class="blurb">
			<div class="viewprofile"><a href="http://twitter.com/<?=$user['twusername']?>" target="_new"><img src="/images/t.png" alt="" /> view profile</a></div>
			<?php if ($twinfo['location']) { ?><span>Location:</span>&nbsp; <?=$twinfo['location']?><br /><?php } ?>
			<?php if ($twinfo['web']) { ?><span>Web:</span>&nbsp; <a href="<?=$twinfo['web']?>" target="_new"><?=$twinfo['web']?></a><br /><?php } ?>
			<?php if ($twinfo['bio']) { ?><span>Bio:</span>&nbsp; <?=$twinfo['bio']?><br /><?php } ?>
			<div style="height: 7px;"></div>
			<?=$twinfo['following']?> <a href="http://twitter.com/<?=$user['twusername']?>/following">following</a><br />
			<?=$twinfo['followers']?> <a href="http://twitter.com/<?=$user['twusername']?>/followers">followers</a>
		</div>
	<?php

	return $twinfo;
}

function showUserResult($user, $padding = "5px 0 35px 0") //as seen in user search results, winners page
{
	global $API;
	
	$vidCount = quickQuery("select @type:='V',count(*) from videos as media where" . $API->getPrivacyQuery(null, true, "", false) . " ready=1 and uid=" . $user['uid']);
	$picCount = quickQuery("select @type:='P',count(*) from photos as media left join albums on albums.id=media.aid where" . $API->getPrivacyQuery() . " media.uid=" . $user['uid']);
//echo "select @type:='P',count(*) from photos as media left join albums on albums.id=media.aid where" . $API->getPrivacyQuery() . " media.uid=" . $user['uid'];
	?>
	<div>

		<div class="phead" style="margin-left:0px; padding-left:0px;">
  		<div style="width: 53px; float: left; padding-left:0px; margin-left:0px;">
  			<a href="<?=$API->getProfileURL($user['uid'], $user['username'])?>"><img src="<?=$API->getThumbURL(1, 48, 48, $API->getUserPic($user['uid'], $user['pic']))?>" alt="" style="height: 48px; width: 48px;" /></a>
  		</div>

			<div class="username" style="float:left; padding-top:30px;">
					<?php
          $sector = "";
          if( $user['sector'] > 0 )
            $sector = quickQuery( "select catname from categories where cat='" . $user['sector'] . "'" ) . " - ";

					echo '<a href="' . $API->getProfileURL($user['uid'], $user['username']) . '">' . $user['name'] . '</a>';
          echo '<span style="font-size:8pt;">' . $sector . $user['occupation'] . '</span>';
					?>
			</div>

      <div style="height:58px; background-image: url(/images/pix_d8dfea.png); background-position: 0 53px; background-repeat: repeat-x; width:570px; "></div>
		</div>

		<div style="clear: both; width: 562px; padding-top:5px;">
			<?php
      if( $user['contactfor'] != '' )
        echo '<span style="font-size:9pt;"><div style="float:left;"><b>Contact for: &nbsp;</b></div><div style="float:left">' . str_replace( chr(2), '</div><div style="float:left;"><img src=\'/images/bullet.png\' width="16" height="16" style="float:left">', substr( $user['contactfor'], 0, strlen( $user['contactfor']) - 1) ) . "</div></span>";
  //showBlurbs($user);
      ?>
		</div>
	</div>

  <div style="clear:both; padding-top:5px;"></div>
	<?php
}

function showBoatResult($boat, $hideBottomBorder = false) //as seen in search results
{
	global $API;
	
	$url = $API->getPageURL($boat['gid'], $boat['gtitle']);

	?>

	<div style="float: left; clear: left;">
		<a href="<?=$url?>"><img src="<?=$API->getThumbURL(1, 100, 75, $API->getPageImage($boat['gid'], $boat['pid'] ) )?>" style="float: left; padding: 5px 10px 0 0; width: 100px; height: 75px;" alt="" /></a>
	</div>
	<div style="float: left; width: 365px; color: #555;" class="mediapreview">
		<a href="<?=$url?>"><span style="font-size: 9pt; font-weight: bold;"><?=$boat['gname']?></span>
		<?php if ($boat['exnames']) { ?><span style="font-size: 8pt;"> (ex. <?=$boat['exnames']?>)</span><?php } ?></a>&nbsp;
		<span style="font-size: 8pt;"><?=" {$boat['length']} ft. / " . round($boat['length'] / 3.2808399, 2) . " m."?></span><br />
		<div style="font-size: 8pt; padding-top: 5px; height: 35px; overflow-y: hidden;">
			<?php
			if ($boat['imo'])
				echo "IMO Number:&nbsp; <span style=\"color: #000;\">{$boat['imo']}</span> &nbsp; ";

			if ($boat['country'])
				echo "Flag:&nbsp; <span style=\"color: #000;\">{$boat['country']}</span>";
			?>
		</div>
		<div class="stats">
			<div style="width: 65px;"><img src="/images/thumb_up.png" alt="" /><?=number_format($likes)?></div>
			<div style="width: 65px;"><img src="/images/thumb_down.png" alt="" /><?=number_format($dislikes)?></div>
		</div>
	</div>

	<div style="clear: both; <?=$hideBottomBorder ? "" : "border-bottom: 1px dotted #326798; "?>height: 1px; width: 535px; padding-top: 25px; margin-bottom: 20px;"></div>
	
	<?php
}

function showPageResult($page, $hideBottomBorder = false) //as seen in search results
{
	global $API;
  global $joined;

	$url = $API->getPageURL($page['gid'], $page['gtitle']);
	$hash = md5($page['gid'] . microtime());

  if( !isset( $joined ) )
  {
    $joined = array();
    $q = mysql_query( "select gid from page_members where uid='" . $API->uid . "'" );
    while( $r = mysql_fetch_array( $q ) )
      $joined[] = $r['gid'];
  }
	?>

	<div style="float: left; clear: left;">
		<a href="<?=$url?>"><img src="<?=$API->getThumbURL(1, 100, 75, $API->getPageImage($page['gid'], $page['pid'] ) )?>" style="float: left; padding: 5px 10px 0 0; width: 100px; height: 75px;" alt="" /></a>
	</div>
	<div style="float: left; width: 365px; color: #555;" class="mediapreview">
		<a href="<?=$url?>"><span style="font-size: 9pt; font-weight: bold;"><?=$page['gname']?></span></a><br />
		<div style="font-size: 8pt; height: 45px; overflow-y: hidden;">
			<?=$page['descr']?>
		</div>
		<?php if ($page['ismember'] == 0) { ?>
		<div class="joinresult" id="join-<?=$hash?>">
<? if( !in_array( $page['gid'], $joined ) ) { ?>
			<a href="javascript:joinPage(<?=$page['gid']?>, '<?=$hash?>', '<?=$API->pageToGtype( $page['gid'] ); ?>');"><img src="/images/door_in.png" alt="" /> join page</a>
<? } ?>
		</div>
		<?php } ?>
	</div>

	<div style="clear: both; <?=$hideBottomBorder ? "" : "border-bottom: 1px dotted #326798; "?>height: 1px; width: 535px; padding-top: 25px; margin-bottom: 20px;"></div>
	
	<?php
}

function showBlurbs($user)
{
	if ($user['twusername'])
		showTWBlurb($user);
	
	if ($user['fbid'] && $user['twusername'])
	{
	?>
		<div class="blurbdivider"></div>
	<?php
	}

	if ($user['fbid'])
		showFBBlurb($user);
}

function showMediaPreview($type, $media, $hideBottomBorder = false, $showAlbumPhotos = true) //as seen on profile page (mb), search page
{
	global $API, $site;

	if ($type == "V")
	{
		$likes = quickQuery("select count(*) from likes where likes=1 and type='V' and link='" . $media['id'] . "'");
		$dislikes = quickQuery("select count(*) from likes where likes=0 and type='V' and link='" . $media['id'] . "'");
		$views = quickQuery("select views from videos where id='" . $media['id'] . "'");
		$comments = quickQuery("select count(*) from comments where type='V' and link='" . $media['id'] . "'");

		$url = $API->getMediaURL("V", $media['id'], $media['title']);
	}
	else
	{
		if ($showAlbumPhotos)
		{
			$likes = quickQuery("select count(*) from likes where likes=1 and type='P' and link in (select id as link from photos where aid=" . $media['aid'] . ")");
			$dislikes = quickQuery("select count(*) from likes where likes=0 and type='P' and link in (select id as link from photos where aid=" . $media['aid'] . ")");
			$views = quickQuery("select sum(views) from photos where aid=" . $media['aid']);
			$comments = quickQuery("select count(*) from comments where type='P' and link in (select id as link from photos where aid=" . $media['aid'] . ")");
		}
		else
		{
			$likes = quickQuery("select count(*) from likes where likes=1 and type='P' and link=" . $media['id']);
			$dislikes = quickQuery("select count(*) from likes where likes=0 and type='P' and link=" . $media['id']);
			$views = quickQuery("select views from photos where id=" . $media['id']);
			$comments = quickQuery("select count(*) from comments where type='P' and link=" . $media['id']);
		}

//		if ($showAlbumPhotos)
//			$url = $API->getMediaURL("A", $media['aid'], $media['title']);
//		else
			$url = $API->getMediaURL("P", $media['id'], $media['ptitle'] ? $media['ptitle'] : $media['title']);

		$count = quickQuery("select count(*) from photos where aid=" . $media['aid']);
	}

  if( !isset( $media['cat'] ) && $type != 'A' ) $media['cat'] = quickQuery( "select cat from " . ($type == "V" ? "videos" : "photos") . " where id='" . $media['id'] . "'" );

	if ($media['name'])
	{
		$profileURL = $API->getProfileURL($media['uid'], $media['username']);
		$profilePic = $API->getUserPic($media['uid'], $media['pic']);
		$parts = array(rawurlencode($profileURL), rawurlencode($profilePic), rawurlencode($media['twusername']), rawurlencode($media['fbid']), rawurlencode($media['name']));
		$id = implode("-", $parts);
		$i = md5(microtime());
	}

	?>

	<div onmouseover="javascript:document.getElementById('addthis-<?=$media['id']?>').style.display='';" onmouseout="javascript:document.getElementById('addthis-<?=$media['id']?>').style.display='none';">
		<div style="float: left; clear: left;">
			<a href="<?=$url?>"><img src="<?=$API->getThumbURL(1, 100, 75, "/" . ($type == "V" ? "videos" : "photos") . "/" .  $media['id'] . "/" . $media['hash'] . ".jpg")?>"<?=empty($media['aid']) ? "" : "id=\"mainimage-" . $media['aid'] . "\""?> style="float: left; padding: 5px 10px 0 0; width: 100px; height: 75px;" alt="" /></a>
		</div>
		<div style="float: left; width: 365px; color: #555;" class="mediapreview">
			<span style="font-size: 9pt; font-weight: bold;"><a href="<?=$url?>"><?=$showAlbumPhotos ? $media['title'] : ($media['ptitle'] ? $media['ptitle'] : $media['title'])?></a></span>
			<span style="font-size: 8pt; color: #808080; font-style: italic;"> - <?=formatDate($media['created'])?><?php if ($media['name']) { ?> by <a id="<?=$i?>-<?=$id?>" onmouseover="javascript:showTip(4, this);" onmouseout="javascript:tipMouseOut();" href="<?=$profileURL?>"><?=$media['name']?></a><?php } ?></span><br />
			<div style="font-size: 9pt; height: 40px; overflow-y: hidden;"><?=$showAlbumPhotos ? $media['descr'] : ($media['pdescr'] ? $media['pdescr'] : $media['descr'])?></div>
			<div class="stats">
				<div style="width: 65px;"><img src="/images/thumb_up.png" alt="" /><?=number_format($likes)?></div>
				<div style="width: 65px;"><img src="/images/thumb_down.png" alt="" /><?=number_format($dislikes)?></div>
				<div style="width: 120px;"><img src="/images/comment.png" alt="" />Replies: <?=number_format($comments)?></div>
				<?php if ($site == "m") { ?><div style="width: 95px;"><img src="/images/view.png" alt="" />Views: <?=number_format($views)?></div><?php } ?>
			</div>
		</div>
		<div class="addthis" id="addthis-<?=$media['id']?>" style="display:none; width:16px;">    
			<div style="height: 22px;"><a href="javascript:void(0);" onclick="javascript: openPopupWindow( 'http://www.facebook.com/sharer/sharer.php?u=<?=urlencode("http://" . SERVER_HOST . $url );?>', 'Share', 550, 320 );"><img src="/images/f.png" width="16" height="16"></a></div>
			<div style="height: 22px;"><a class="addthis_button_twitter" addthis:description="<?=getMediaJMPLink( $media['id'], $type );?>" addthis:title="<?=$media['title']?>" addthis:url="<?=$media['jmp']?>"></a></div>
			<div><a onclick="javascript:showEmail('<?=$type?>', <?=$media['id']?>);" href="javascript:void(0);"><span class="at300bs at15t_email"></span></a></div>      
		</div>
		
		<?php
		if ($type == "P" && $count > 1 && $showAlbumPhotos) //show other photos in album
		{
			?>
			<div class="albumpreview">
				<?php
				$i = 0;
				$x = mysql_query("select @type:='P',media.id,hash,title from photos as media inner join albums on albums.id=aid where " . $API->getPrivacyQuery() . " aid=" . $media['aid']);
				while ($photo = mysql_fetch_array($x, MYSQL_ASSOC))
				{
          $i++;
					if ($i > 10)
					{
						$clear = " clear: left;";
						$i = 1;
					}
					else
						$clear = "";
					?>
					<div style="<?=$clear?>"><a href="<?=$API->getMediaURL("P", $photo['id'])?>"><img src="<?=$API->getThumbURL(1, 45, 35, "/photos/" . $photo['id'] . "/" . $photo['hash'] . ".jpg")?>" alt="" /></a></div>
					<?php
				}
				?>
        <div style="clear:both;"></div>
			</div>
			<?php
		}
		?>

<?
if( $API->admin && $type != 'A')
{
?>
    <div style="float:right; margin-right:105px; color:#AAAAAA;">
    <select id="mcat-<? echo $media['id'] ?>" onchange="updateMediaCategory(<? echo $media['id']; ?>,'<? echo $type?>',this.value,'<? echo $hash ?>');">
    <?
    $x = mysql_query( "select * from categories where cattype='M' order by industry, catname" );
    while( $r = mysql_fetch_array( $x ) )
    {
      $hash = md5($media['id']) . microtime();
    ?>
    <option value="<? echo $r['cat']; ?>"<? if( $r['cat'] == $media['cat'] ) { echo " SELECTED"; } ?>><? echo $r['catname']; ?></option>
    <?
    }
    ?>
    </select>
    </div>
<?
}
?>
		<div style="clear: both; <?=$hideBottomBorder ? "" : "border-bottom: 1px dotted #326798; "?>height: 1px; width: 535px; padding-top: 25px; margin-bottom: 20px;"></div>

	</div>
	<?php
}

//wraps the message in our template and sends it out
function emailAddress($email, $subj, $msg, $from = "notifications")
{
	global $email_domain, $API;

  if( quickQuery( "select count(*) from blocked_emails where email like '%" . addslashes( $email ) . "%'" ) > 0 )
    return false; //User's email address is blocked.

	include $_SERVER["DOCUMENT_ROOT"] . "/inc/phpmailer.php";

	if (strpos($from, "@"))
		$mail->SetFrom($from, $API->name);
	else
		$mail->SetFrom("$from@$email_domain.com", $email_domain);

	$mail->isHTML(true);
	
	$mail->AddAddress($email);
	$mail->Subject = $subj;
	$mail->Body = emailTemplate($msg, $email);
	$mail->Send();
	

  return true;
}

function emailTemplate($body, $email_to, $showSignUp = false)
{
	global $API, $site, $email_domain, $siteName, $hash;
	ob_start();
	?>
<?php
$host = "http://" . $_SERVER['HTTP_HOST'];

$tw = $API->getTWUserInfo();
$fb = $API->getFBUserInfo();


$mc = $API->getMediaCount();

      $vidCount = quickQuery( "select count(id) from videos where uid='" . $API->uid . "'" );
      $picCount = quickQuery( "select count(id) from photos where uid='" . $API->uid . "'" );

//$vidCount = quickQuery("select @type:='V',count(*) from videos as media where " . $API->getPrivacyQuery() . " ready=1 and uid=" . $API->uid);
//$picCount = quickQuery("select @type:='P',count(*) from photos as media inner join albums on media.aid=albums.id where " . $API->getPrivacyQuery() . " media.uid=" . $API->uid);

$profileURL = $host . $API->getProfileURL();

$body = str_replace("[ORANGEBOX]", '
			<table cellpadding="5" cellspacing="0" align="center" bgcolor="#FFF9D7" style="border: 1px solid #E2C822;" width="580">
				<tr>
					<td>
						<div style="padding: 9px 6px; font-weight: bold; font-size: 10pt; color: #000;">', $body);

$body = str_replace("[/ORANGEBOX]", '
						</div>
					</td>
				</tr>
			</table>', $body);

// thanks to the genius that is M$ outlook, tables are necessary
$url = $host;
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<body style="background: #EFF2F9; font-family: arial; color: #555;">
<div style="padding: 10px;">
	<table width="730" border="0" cellpadding="0" cellspacing="0" bgcolor="<?=$site == "m" ? "#ACD5EB" : "#fff"?>" style="border: 1px solid #ACD5EB;">
		<tr>
			<td>
				<div>
					<img src="<?=$host?>/images/emailhead_<?=$site?>.png" alt="" />
				</div>
				<div>
					<table width="730" cellpadding="5" cellspacing="0" bgcolor="#ffffff">
						<tr valign="top">
							<td width="57" valign="top">
								<a href="<?=$profileURL?>"><img width="48" height="48" src="<?=$host . $API->getThumbURL(1, 48, 48, $API->getUserPic())?>" style="margin-top:15px; border: 0;" alt="" /></a>
							</td>
							<td>

								<table width="660" cellpadding="0" cellspacing="0" style="border-bottom: 1px solid #d8dfea;">
									<tr>
										<td width="210" style="padding-top: 5px;">
											<a style="font-size: 18pt; font-weight: bold; color: #555; text-decoration: none;" href="<?=$profileURL?>"><?=$API->name?></a>
										</td>
										<td width="25">
											<img src="<?=$host?>/images/<?=$site == "m" ? "birdhead" : "s_badge16x16"?>.png" alt="" />
										</td>
										<td width="150" style="font-size: 10pt;">
											<?=number_format($vidCount)?> <a href="<?=$profileURL?>/videos" style="text-decoration: none; color: #326798;">videos</a>&nbsp; <?=number_format($picCount)?> <a href="<?=$profileURL?>/photos" style="text-decoration: none; color: #326798;">photos</a>
										</td>
										<td width="25">
											<img src="<?=$host?>/images/t.png" alt="" />
										</td>
										<td style="font-size: 10pt;">
											<?=$tw['following']?> <a href="<?=$profileURL?>" style="text-decoration: none; color: #326798;">following</a>&nbsp; <?=$tw['followers']?> <a href="<?=$profileURL?>" style="text-decoration: none; color: #326798;">followers</a>
										</td>
										<td width="25">
											<img src="<?=$host?>/images/f.png" alt="" />
										</td>
										<td width="70" style="font-size: 10pt;">
											<?=intval($fb['friends'])?> <a href="<?=$profileURL?>" style="text-decoration: none; color: #326798;"><? if( $site == "m") echo 'friends'; else echo 'friends'; ?></a>
										</td>
									</tr>
								</table>

								<div style="padding-left: 5px; padding-top: 7px; font-size: 10pt; color: #000; width: 625px; font-family: arial;"><?=$body?><br /><br /></div>
								<div style="clear: both;"></div>
							</td>
						</tr>
					</table>
				</div>
			</td>
		</tr>
	</table>

	<table border="0" cellpadding="7" cellspacing="0" width="730">
		<tr>
			<td>
				<div style="font-size: 8pt; color: #777;">
					<?php
					if ($showSignUp)
						echo "$email_to was invited to $siteName by {$API->name}.";
					else
						echo "This message was sent on behalf of {$API->name} and intended for $email_to.";

					// bs unsubscribe link - the user doesn't get unsubscribed because he wasn't subscribed to anything to begin with
					echo "&nbsp; If you do not want wish to receive this type of e-mail from $siteName in the future, please click <a href=\"$host/unsubscribe.php?e=$hash\">here</a> to unsubscribe.&nbsp; ";
					echo "$siteName, 1323 SE 17th St., Ft. Lauderdale, FL 33316";
					?>
				</div>
			</td>
		</tr>
	</table>
</div>
</body></html>
	<?php
	$html = str_replace("\n", "", str_replace("\t", "", ob_get_contents()));
	ob_end_clean();

	return $html;
}

function getFirstName( $full_name )
{
  return strtok( $full_name, " " );
}

function textToHTML($message)
{
	$message = findLinks($message);
	$message = str_replace("  ", "&nbsp; ", $message);
	$message = str_replace("\n\n", "<p />", $message);
	$message = str_replace("\n", "<br />", $message);
	
	return $message;
}


function htmlToText($message)
{
	$message = str_replace("<br>", "\n", $message);
	$message = str_replace("<br/>", "\n", $message);
	$message = str_replace("<br />", "\n", $message);
	$message = str_replace("<p>", "\n\n", $message);
	$message = str_replace("<p/>", "\n\n", $message);
	$message = str_replace("<p />", "\n\n", $message);
	$message = html_entity_decode(strip_tags($message));

	return $message;
}


function showMediaEdit($type, $media, $editable = true) //as seen on settings page
{
	global $API, $site;

  $deletable = true;

	if ($type == "V")
	{
		$likes = quickQuery("select count(*) from likes where likes=1 and type='V' and link='" . $media['id'] . "'");
		$dislikes = quickQuery("select count(*) from likes where likes=0 and type='V' and link='" . $media['id'] . "'");
		$views = quickQuery("select views from videos where id='" . $media['id'] . "'");
		$comments = quickQuery("select count(*) from comments where type='V' and link='" . $media['id'] . "'");

    if( $site != "m" )
      $cat = quickQuery("select cat from videos where id='" . $media['id'] . "'");
    $privacy = quickQuery("select privacy from videos where id='" . $media['id'] . "'");

		$url = $API->getMediaURL("V", $media['id'], $media['title']);
	}
	else
	{
		$likes = quickQuery("select count(*) from likes where likes=1 and type='P' and link in (select id as link from photos where aid=" . $media['aid'] . ")");
		$dislikes = quickQuery("select count(*) from likes where likes=0 and type='P' and link in (select id as link from photos where aid=" . $media['aid'] . ")");
		$views = quickQuery("select sum(views) from photos where aid=" . $media['aid']);
		$comments = quickQuery("select count(*) from comments where type='P' and link in (select id as link from photos where aid=" . $media['aid'] . ")");

		$url = $API->getMediaURL("A", $media['aid'], $media['title']);
		$count = quickQuery("select count(*) from photos where aid=" . $media['aid']);


    if( $site != "m" )
    {
      $album_type = quickQuery( "select albType from albums where id=" . $media['aid'] );
      $cat = quickQuery("select cat from photos where aid='" . $media['aid'] . "'");
    }

    $privacy = quickQuery("select privacy from photos where aid='" . $media['aid'] . "'");


    $deletable = ( $album_type == 0 );
	}

	?>
	<div class="mediaedit"<?=$editable ? "" : " style=\"width: 610px;\""?>>
		<div style="float: left; width: 20px;"><?php if ($editable && $deletable) { ?><input type="checkbox" name="chk<?=$type == "V" ? $media['id'] : $media['aid']?>" style="margin: 0; padding: 0;" /><?php } ?></div>
		<a href="<?=$url?>"><img height="80" width="100" src="<?=$API->getThumbURL(1, 119, 95, "/" . ($type == "V" ? "videos" : "photos") . "/" .  $media['id'] . "/" . $media['hash'] . ".jpg")?>"<?=empty($media['aid']) ? "" : "id=\"mainimage-" . $media['aid'] . "\""?> style="float: left; padding-right: 5px; width: 100px; height: 75px;" alt="" /></a>
		<div class="resultdata">
			<!--<form>-->
			<div class="smtitle" style="font-size: 8pt; float: left; width: 100px;"><?=formatDate($media['created'])?></div>
      <div style="float:left;"><a href="javascript:void(0);" onclick="javascript: openPopupWindow( 'http://www.facebook.com/sharer/sharer.php?u=<?=urlencode("http://" . SERVER_HOST . $url );?>', 'Share', 550, 320 );"><img src="/images/f.png"></a></div>
			<div style="float: left;"><a class="addthis_button_twitter" addthis:description="http://shb.me/<?=$media['jmp']?>" addthis:title="<?=$media['title']?>" addthis:url="http://shb.me/<?=$media['jmp']?>"></a></div>
			<div style="float: left;"><a onclick="javascript:showEmail('<?=$type?>', <?=$media['id']?>);" href="javascript:void(0);"><span class="at300bs at15t_email"></span></a></div>
			<?php if ($editable) { ?><div style="float: right; width: 70px; text-align: right;"><img src="/images/wait326798_l.gif" alt="" id="wait-<?=$type == "V" ? $media['id'] : $media['aid']?>" style="vertical-align: bottom; margin: 0; visibility: hidden;" /><span id="save-<?=$type == "V" ? $media['id'] : $media['aid']?>">&nbsp;&nbsp;<a href="javascript:void(0);" onclick="javascript:updateMedia(<?=$type == "V" ? $media['id'] : $media['aid']?>);">save</a></span></div><?php } ?>
			<div style="float: right;"><a href="javascript:void(0);" onclick="javascript:showEmbedMedia('<?=$url?>', '<?=$media['jmp']?>', '<?=$type?>', <?=$media['id']?>);"><img src="/images/embed.png" alt="" style="vertical-align: bottom;" />Embed</a></div>
			<div style="clear: both;"></div>
			<?php if ($editable) { ?>
			<input type="text" id="title-<?=$type == "V" ? $media['id'] : $media['aid']?>" value="<?=$media['title']?>" maxlength="50" style="width: 495px;" />
			<input type="text" id="descr-<?=$type == "V" ? $media['id'] : $media['aid']?>" value="<?=$media['descr']?>" onfocus="javascript:showCharsLeft(<?=$type == "V" ? $media['id'] : $media['aid']?>, true);" onblur="javascript:showCharsLeft(<?=$type == "V" ? $media['id'] : $media['aid']?>, false);" onkeyup="javascript:charsLeft(<?=$type == "V" ? $media['id'] : $media['aid']?>, this.value);" style="width: 495px;" />
			<div id="chars-<?=$type == "V" ? $media['id'] : $media['aid']?>" style="visibility: hidden;" class="charsleft"><?=104 - strlen($media['descr'])?> characters remaining</div>

      <div style="float:left;">
<? if( $site != "m" ) { ?>
        Category:
        <select id="cat-<?=$type == "V" ? $media['id'] : $media['aid']?>">
<?
$y = mysql_query( "select * from categories where cattype='M' order by catname" );
while( $r = mysql_fetch_array( $y ) )
{
?>
          <option value="<? echo $r['cat']; ?>"<? if( $r['cat'] == $cat ) echo " SELECTED"; ?>><? echo $r['catname'] ?></option>
<?
}
?>
        </select>
<? } ?>
      </div>

      <div style="float:right">
        Accessible by:
        <select id="privacy-<?=$type == "V" ? $media['id'] : $media['aid']?>">
        <option value="<? echo PRIVACY_EVERYONE; ?>"<? if( $privacy == PRIVACY_EVERYONE ) echo " SELECTED"; ?>>The world</option>
        <option value="<? echo PRIVACY_FRIENDS; ?>"<? if( $privacy == PRIVACY_FRIENDS ) echo " SELECTED"; ?>>Only my friends</option>
        <option value="<? echo PRIVACY_SELF; ?>"<? if( $privacy == PRIVACY_SELF ) echo " SELECTED"; ?>>Only me</option>
        </select>
      </div>

      <div style="clear:both; width:100%; text-align:center;">Category and privacy settings will affect all photos in this album.</div>
			<?php }	else { ?>
			<div class="resulttitle"><?=$media['title']?></div>
			<div class="resultdescr"><?=$media['descr']?><?=$media['descr']?></div><?php } ?>
			<div class="mediaeditstat"><img src="/images/thumb_up.png" alt="" /> Likes: <?=number_format($likes)?></div>
			<div class="mediaeditstat"><img src="/images/thumb_down.png" alt="" /> Dislikes: <?=number_format($dislikes)?></div>
			<div class="mediaeditstat"><img src="/images/comment.png" alt="" /> Comments: <?=number_format($comments)?></div>
			<div class="mediaeditstat" style="width: auto;"><img src="/images/view.png" alt="" /> Views: <?=number_format($views)?></div>
			<!--</form> -->
		</div>
		<?php if ($type == "P") { ?>
			<div id="albumcontainer-<?=$media['aid']?>" style="padding: 10px 0 0 <?=$editable ? "20px" : "0px"?>; clear: both;<?=$count == 1 ? " display: none;" : ""?>">
			<?php
			if (!$editable)
			{
                $count = 0;
				$x = mysql_query("select photos.id,hash,title from photos inner join albums on albums.id=aid where aid=" . $media['aid']);
				if (mysql_num_rows($x) > 0)
					while ($photo = mysql_fetch_array($x, MYSQL_ASSOC))
					{
                        $count++;
						?>
						<div class="phnewcontainer imgborder">
							<a href="<?=$API->getPhotoURL($photo['id'], $photo['title'])?>"><img height="50" width="75" src="<?=$API->getThumbURL(1, 119, 95, "/photos/" . $photo['id'] . "/" . $photo['hash'] . ".jpg")?>" alt="" /></a>
						</div>
						<?php
					}
			}
			?>
			</div>
		<?php } if($count > 14){ ?>
      <div id="albumshowmore-<?=$media['aid']?>" style="clear:both; text-align:center; display:block; font-size:10pt;"><a href="javascript:void(0);" onclick="javascript:showMoreMedia(<?=$media['aid']?>);">show more</a></div>
		<?}?><div style="clear: both; height: 10px;"></div>
	</div>
	<?php
}

function showSmallMediaPreview($media, $gid)
{
	global $API;
	?>
	<div class="mediapreviewsmall" id="mps-<?=$media['type'] . $media['id']?>">
		<div class="pic">
			<img height="75" width="75" src="<?=$API->getThumbURL(0, 119, 95, "/" . typeToWord($media['type']) . "s/{$media['id']}/{$media['hash']}.jpg")?>" alt="" />
		</div>
		<div class="info">
			<div class="title">
				<span><?=$media['title']?></span>
				<div class="addlink"><?='<a href="javascript:void(0);" onclick="javascript:addPageExistingMedia(\'' . implode("', '", array($media['type'], $media['id'], $gid)) . '\');"><span id="addlink-' . $gid . $media['type'] . $media['id'] . '">' . ($media['inpage'] == 1 ? "remove from page" : "add to page") . '</span></a>'?></div>
			</div>
			<div class="descr"><?=$media['descr']?></div>
		</div>
	</div>
	<?php
}

function friendLink($uid, $status = null, $arrCaptions = null, $showDecline = false)
{
	global $API;

	if ($uid == $API->uid) return;

	if (empty($arrCaptions))
		$arrCaptions = array("Connect", array("Pending connection", "Accept request"), "Already Connected");

	if (is_null($status))
	{
		$x = mysql_query("select * from friends where (id1='$uid' and id2='" . $API->uid . "') or (id2='$uid' and id1='" . $API->uid . "')");
		if ( mysql_num_rows($x) > 0)
		{
			$tmp = mysql_fetch_array($x, MYSQL_ASSOC);

			$status = $tmp['status'];
			
			if ($status == "0")
				if ($tmp['id1'] == $uid)
					$status = "0A"; //meaning, this user has the ability to accept
		}
		else
			$status = -1;
	}

	if ($status == "0" || $status == "1")
		return is_array($arrCaptions[$status + 1]) ? current($arrCaptions[$status + 1]) : $arrCaptions[$status + 1];

	if ($status == "0A")
		$status = "0";

	$divId = "fr-" . $uid . "-" . substr(md5(microtime()), 0, 10);

	if ($status == "-1")
		return '<span id="' . $divId . '"><a href="javascript:void(0);" onclick="javascript:addFriend(' . $uid . ', \'' . $divId . '\', \'' . $arrCaptions[1][0] . '\');">' . $arrCaptions[0] . '</a></span>';
	else
  {
    $response = '<span id="' . $divId . '"><a href="javascript:void(0);" onclick="javascript:confirmFriend(' . $uid . ', \'' . $divId . '\', \'' . $arrCaptions[2] . '\');">' . $arrCaptions[1][1] . '</a>';
    if( $showDecline )
      $response .= '<br /><a href="javascript:void(0);" onclick="javascript:deleteFriend(' . $uid . ', \'' . $divId . '\', \'Declined\');">' . 'Hide request' . '</a>';
    $response .= '</span>';
		return $response;
  }
}


function getCustomPicURL($uid)
{
	global $hash, $site;

	if (file_exists($_SERVER["DOCUMENT_ROOT"] . "/userpics_$site/$uid" . "_2.jpg"))
		return "/userpics_$site/$uid" . "_2.jpg?" . $hash;
	else
		return "";
}

function getFBPicURL($uid)
{
	if (empty($uid))
		return "";
	else
		return "http://graph.facebook.com/$uid/picture";
}

function getTWPicURL($uid)
{
	if (empty($uid))
		return "";
	else
		return "http://api.twitter.com/1/users/profile_image/$uid";
}

function shortenMediaURL($type, $id)
{
	switch(strtoupper($type))
  {
    case "V":
  		return shortenURL("http://" . SERVER_HOST . "/video.php?id=$id");
    break;

    case "P":
  		return shortenURL("http://" . SERVER_HOST . "/photo.php?id=$id");
    break;

    case "G":
      global $API;
      $url = $API->getPageUrl( $id );
  		return shortenURL("http://" . SERVER_HOST . $url);
    break;
  }
}

function shortenURL($url)
{
	$url = substr($url, 0, 100);
	
	$q = quickQuery('select id from shbme where url="' . addslashes($url) . '"');
	
	if (empty($q))
	{
		mysql_query('insert into shbme (url) values ("' . addslashes($url) . '")');
		
		$q = mysql_insert_id();
	}
	
	return str_pad(base_convert($q, 10, 36), 6, '0', STR_PAD_LEFT);
}

function formatDate($date)
{
	return date("F j, Y", strtotime($date));
}

function quickQuery($q)
{

  $success = false;

  while( !$success )
  {
  	$x = sql_query($q . " limit 1");

    if( !mysql_error() )
    {
      $success=true;
    }
    else
    {
      if( stristr( mysql_error(), "gone away" ) !== FALSE || stristr( mysql_error(), "lost connection" ) !== FALSE )
      {
        mysql_close();
        //reestablish connection
        include( "sql.php" );
      }
      else
      {
        echo "<pre style=\"font-size: 10pt;\">Failed on query:\n\n";
        global $API;
        if( $API->admin )
        {
          echo $q . " limit 1<br /><br />";
          echo mysql_error() . "<br /><br />";
          var_dump( debug_backtrace() );
        }
        exit;
      }
    }
  }
	if (mysql_num_rows($x) == 0)
		$ret = '';
	else
	{
		$ret = mysql_fetch_array($x, MYSQL_ASSOC);
		$ret = end($ret);
	}
	
	return $ret;
}

function xmysql_query($q)
{
	$l = fopen("/tmp/query", "a");
	fwrite($l, $q . "\n");
	fclose($l);
	
	return mysql_query($q);
}

function typeToWord($t)
{
	switch (substr($t, 0, 3))
	{
 		case "V":
		return "video";

		case "P":
		return "photo";

		case "A_":
		case "A":
		return "album";

		case "C":
		return "comment";
		
		case "W":
		return "log book post";

    case "r":
		case "rss":
		return "rss feed";

		case "G":
		case "g":
		return "page";
	}
}

function a_an($word)
{
	$x = strtolower(substr($word, 0, 1));

	if (in_array($x, array("a", "e", "i", "o", "u")))
		return "an";
	else
		return "a";
}

function deleteMediaFromDB($type, $id)
{
  global $API;

  switch( $type )
  {
    case "P":
      $hash = quickQuery( "select hash from photos where id='$id'" );
    break;
    case "V":
      $hash = quickQuery( "select hash from videos where id='$id'" );
    break;
  }

	$cid = mysql_query("select id from comments where type='$type' and link='$id'");
	mysql_query("delete from comments where id=$cid");
	mysql_query("delete from page_media where type='$type' and id='$id'");
	mysql_query("delete from feed where type='$type' and link='$id'");
	mysql_query("delete from feed where type='C' and link='$cid'");
	mysql_query("delete from views where type='$type' and link='$id'");
	mysql_query("delete from likes where type='$type' and link='$id'");
	mysql_query("delete from media_views where type='$type' and id='$id'");
	mysql_query("delete from reported where type='$type' and id='$id'");

  $tag_type = "";
  if( $type == "P" )
  {
    $tag_type = "t";
    $word = "photo_tags";

    $API->deleteFromCloud( null, $hash . ".jpg" );
    $API->deleteFromCloud( null, $hash . "_square.jpg" );
    $API->deleteFromCloud( null, $hash . "_tall.jpg" );
    $API->deleteFromCloud( null, $hash . "_wide.jpg" );
  }
  else if( $type == "V" )
  {
    $tag_type = "T";
    $word = "video_tags";
    $API->deleteFromCloud( null, $hash . ".jpg" );
    $API->deleteFromCloud( null, $hash . ".flv" );
  }

  if( strlen( $tag_type ) > 0 )
  {
    sql_query( "delete from feed where type='$tag_type' and link='$id'" );
    sql_query( "delete from $word where " . strtolower( $type ) . "id = '$id'" );
  }
}

function queryArray($q)
{
  $success = false;

  while( !$success )
  {
  	$x = mysql_query($q . " limit 1");

    if( !mysql_error() )
    {
      $r = mysql_fetch_array($x, MYSQL_ASSOC);
      $success=true;
    }
    else
    {
      if( stristr( mysql_error(), "gone away" ) !== FALSE || stristr( mysql_error(), "lost connection" ) !== FALSE )
      {
        mysql_close();
        //reestablish connection
        include( "sql.php" );
      }
      else
      {
        //Quit the loop
        return NULL;
      }
    }
  }

	return $r;
}

//returns all info about a feed to the actual thing it's feeding
function resolveFeed($feed)
{
	global $API;

	if ($feed['link'])
	{
		$feed['id'] = $feed['link'];
		unset($feed['link']);
	}

	$resolved = array($feed);
	$continue = true;

	do
	{
		$item = $resolved[count($resolved) - 1];

		switch ($item['type'])
		{
			case "_":
			$continue = false;
			break;

      case "t2":
      case "T2":
      case "t":
			case "T":
			if ( substr( $item['type'], 0, 1) == "t")
      {
				$word = "photo";
      }
			else
      {
				$word = "video";
      }

			$result = queryArray("
				SELECT '" . strtoupper(substr($word, 0, 1)) . "' as type,t.tid,t.cid,t.uid,t." . substr($word, 0, 1) . "id AS id,site, hash, IF(IFNULL(u.uid,0)>0,u.name, IF(IFNULL(c.cid,0)>0,c.name,ct.name)) AS contact_name,u.uid AS contact_uid,pic AS contact_pic
				FROM {$word}_tags t
				LEFT JOIN contacts c ON c.cid=t.cid AND custom=0
				LEFT JOIN custom_tags ct ON t.cid=ct.id AND custom=1
				LEFT JOIN users u ON u.uid=IF(t.cid=0,t.uid,c.eid)
        LEFT JOIN " . $word . "s ON " . $word . "s.id=t." . substr($word, 0, 1) . "id
				where t.tid={$item['id']}
					");

      $resolved[] = $result;

      $continue = false;
			break;

			case "g": //created page
			case "G": //joined page
			$resolved[] = $API->getPageInfo($item['id']);
			$continue = false;
			break;

			case "C":
			$resolved[] = queryArray("select id as cid,uid,gid,type,link as id,comment as descr,created as ts from comments where id={$item['id']}");
			break;

			case "L":
			$data = queryArray("select lid,uid,type,link as id,likes,ts,link from likes where lid={$item['id']}");

      switch( $data['type'] )
      {
        case "P":
          $data = array_merge( $data, queryArray("select pdescr from photos where id='" . $data['link'] . "'" ) );
        break;

        case "V":
          $data = array_merge( $data, queryArray("select title, descr from videos where id='" . $data['link'] . "'" ) );
        break;

        case "r":
        case "rss":
          $data = array_merge( $data, queryArray("select title, gid from rss_feed where id='" . $data['link'] . "'" ) );
        break;

        case "W":
          $data = array_merge( $data, queryArray("select post as title, uid from wallposts where sid='" . $data['link'] . "'" ) );
        break;

        default:
        break;
      }

      $resolved[] = $data;

      $continue = false;
			break;

			case "W":
			$resolved[] = queryArray("select 'W' as type,sid as id,uid,uid_to,post as descr,ts from wallposts where sid={$item['id']}");
			$continue = false;
			break;

			case "A":
			$resolved[] = $API->getMediaInfo("A", $item['id'], "'A' as type,albums.id,albums.created as ts,hash,mainimage,ifnull(ptitle,title) as title,ifnull(pdescr,descr) as descr,albums.uid");
			$continue = false;
			break;

			case "P":
			case "V":
			$word = typeToWord($item['type']);
			$resolved[] = $API->getMediaInfo($item['type'], $item['id'], "'{$item['type']}' as type,{$word}s.id,{$word}s.uid,{$word}s.created as ts,hash," . ($item['type'] == "V" ? "title,descr" : "ifnull(ptitle,title) as title,ifnull(pdescr,descr) as descr"));

//      if( $item['type'] == "P" ) echo "**" . $resolved[ sizeof( $resolved ) - 1 ]['descr'];
  			$continue = false;
			break;

      case "r":
      $item['type'] = "P";
			$word = typeToWord($item['type']);
			$resolved[] = $API->getMediaInfo($item['type'], $item['id'], "'{$item['type']}' as type,{$word}s.id,{$word}s.uid,{$word}s.created as ts,hash," . ($item['type'] == "V" ? "title,descr" : "ifnull(ptitle,title) as title,ifnull(pdescr,descr) as descr"));
			$continue = false;
      break;

      case "R":
      $item['type'] = "V";
			$word = typeToWord($item['type']);
			$resolved[] = $API->getMediaInfo($item['type'], $item['id'], "'{$item['type']}' as type,{$word}s.id,{$word}s.uid,{$word}s.created as ts,hash," . ($item['type'] == "V" ? "title,descr" : "ifnull(ptitle,title) as title,ifnull(pdescr,descr) as descr"));
			$continue = false;
      break;

			case "f":
			$resolved[] = $item;
			$continue = false;
			break;

			case "F":
			$resolved[] = queryArray("select 'F' as type,id1,id2,status,fid from friends where fid={$item['id']}");
			$continue = false;
			break;

			case "S":
			$resolved[] = queryArray("select 'S' as type,id1,id2,uid,subj,body from friend_suggestions where sid={$item['id']}");
			$continue = false;
			break;

			case "J":
			$resolved[] = $item;
			$continue = false;
			break;

			case "j":
			$resolved[] = queryArray( "select * from jobs where id={$item['id']}" );
			$continue = false;
			break;

      case "e":
			$resolved[] = array( "link"  => $API->getPageUrl( $item['gid'] ), "name" => quickQuery( "select gname from pages where gid='" . $item['gid'] . "'" ) );
      $continue = false;
      break;

      case "M":
			$resolved[] = queryArray("select 'M' as type, uid, uid_to, unreadByRecv, delByRecv, subj from messages where mid={$item['id']}");
			$continue = false;
      break;

      case "rss":
      $link = $feed['id'];
      $temp = queryArray("select rss_feed.*, 'rss' as type from rss_feed where id='$link'");
      $temp['jmp'] = str_replace( "http://shb.me/", "", getFeedJMPLink( $feed['fid'] ) );
			$resolved[] = $temp;
			$continue = false;
      break;

      case "G1":
      case "G2":
      case "G3":
      case "G4":
      case "G5":
      case "GV":
      case "GP":
      case "G6":
        $resolved[] = queryArray( "select gid, gname from pages where gid='" . $item['id'] . "'" );
        $continue = false;
      break;

      case "PR1": //Updated profile photo
			case "o":
	  		$resolved[] = array( "descr" => 'Updated his/her <a href="' . $API->getProfileUrl( $feed['uid_by'] ) . '">profile</a> photo' );
  			$continue = false;
			break;

      case "PR2": //Updated headline
        $resolved[] = array( "descr" => 'Updated headline: ' . quickQuery( "select occupation from users where uid='" . $feed['uid_by'] . "'" ) );
  			$continue = false;
      break;

      case "PR3": //Updated employment availability
        include_once( $_SERVER["DOCUMENT_ROOT"] . "/employment/employment_functions.php" );

        $duration = array( "not seeking", "seeking full Time", "seeking Part Time", "seeking Seasonal", "seeking Contract", "seeking Day Work", "seeking Delivery" );

        $user = queryArray( "select seeking_employment, employment_position, employment_location, employment_sector from users where uid='" . $feed['uid_by'] . "'" );

        $descr = "Updated employment availability: ";

        $descr .= "I'm " . strtolower( $duration[ $user['seeking_employment'] ] ) . " employment " . gidToName($user['employment_position'], "as an ", "in any position" ) . " in " . catToName($user['employment_sector'], "the ", "any") . " sector(s). I am currently located in " . countryToName( $user['employment_location'], "", "(not selected)" ) . ".";

        $resolved[] = array( "descr" => $descr );

  			$continue = false;
      break;

      case "PR5": //Updated work experience
        $descr = "Updated Work Experience: ";

        $q2 = mysql_query( "select employer from work where uid='" . $feed['uid_by'] . "' order by start" );
        $rows = mysql_num_rows( $q2 );
        $c = 0;
        while ( $r2 = mysql_fetch_array( $q2 ) )
        {
          $name = quickQuery( "select gname from pages where gid='" . $r2['employer'] . "'" );
          if( $c ) $descr .= ", ";
          $descr .= '<a href="' . $API->getPageURL( $r2['employer'], $name ) . '">' . $name . '</a>';

          $c++;
          if( $c >= 3 ) break;
        }

        if( $c < $rows )
        {
          $others = $rows - $c;
          $descr .= ' and <a href="' . $API->getProfileURL( $feed['uid'] ) . '">' . $others;
          if( $others == 1 ) $descr .= " other";
          else $descr .= " others";
          $descr .= "</a>";
        }

        if( $c )
          $resolved[] = array( "descr" => $descr );

  			$continue = false;
      break;

      case "PR6": //Updated licenses and certificates
        $descr = "Updated Licenses &amp; Certificates: ";

        $q2 = sql_query("select g.gid, g.gname from pages g left join personal on g.gid=personal.gid where g.cat='" . CAT_LICDEG . "' and uid=" . $feed['uid_by'] . " group by gid order by gid");

        $rows = mysql_num_rows( $q2 );
        $c = 0;
        while ( $r2 = mysql_fetch_array( $q2 ) )
        {
          $name = $r2['gname'];
          if( $c > 0 ) $descr .= ", ";
          $descr .= '<a href="' . $API->getPageURL( $r2['gid'], $name ) . '">' . $name . '</a>';

          $c++;
          if( $c >= 3 ) break;
        }

        if( $c < $rows )
        {
          $others = $rows - $c;
          $descr .= ' and <a href="' . $API->getProfileURL( $feed['uid'] ) . '">' . $others;
          if( $others == 1 ) $descr .= " other";
          else $descr .= " others";
          $descr .= "</a>";
        }

        if( $c )
          $resolved[] = array( "descr" => $descr );

  			$continue = false;
      break;

      case "PR7": //Updated education
        $descr = "Updated Education: ";

        $q2 = mysql_query( "select school from education where uid='" . $feed['uid_by'] . "' order by start" );
        $rows = mysql_num_rows( $q2 );
        $c = 0;
        while ( $r2 = mysql_fetch_array( $q2 ) )
        {
          $name = quickQuery( "select gname from pages where gid='" . $r2['school'] . "'" );
          if( $c > 0 ) $descr .= ", ";
          $descr .= '<a href="' . $API->getPageURL( $r2['school'], $name ) . '">' . $name . '</a>';

          $c++;
          if( $c >= 3 ) break;
        }

        if( $c < $rows )
        {
          $others = $rows - $c;
          $descr .= ' and <a href="' . $API->getProfileURL( $feed['uid'] ) . '">' . $others;
          if( $others == 1 ) $descr .= " other";
          else $descr .= " others";
          $descr .= "</a>";
        }

        if( $c )
          $resolved[] = array( "descr" => $descr );

  			$continue = false;
      break;

      case "PR8": //Updated clubs and associations
        $descr = "Updated Clubs &amp; Associations: ";

        $q2 = mysql_query("select pages.gid, pages.gname from page_members left join pages on page_members.gid=pages.gid where (pages.cat=" . CAT_CLUB_ASSOC . " OR pages.subcat=" . CAT_CLUB_ASSOC . ") and page_members.uid='" . $feed['uid_by'] . "'");
        $rows = mysql_num_rows( $q2 );
        $c = 0;
        while ( $r2 = mysql_fetch_array( $q2 ) )
        {
          $name = $r2['gname'];
          if( $c > 0 ) $descr .= ", ";
          $descr .= '<a href="' . $API->getPageURL( $r2['gid'], $name ) . '">' . $name . '</a>';

          $c++;
          if( $c >= 3 ) break;
        }

        if( $c < $rows )
        {
          $others = $rows - $c;
          $descr .= ' and <a href="' . $API->getProfileURL( $feed['uid'] ) . '">' . $others;
          if( $others == 1 ) $descr .= " other";
          else $descr .= " others";
          $descr .= "</a>";
        }

        if( $c )
          $resolved[] = array( "descr" => $descr );

  			$continue = false;
      break;


      case "PR9":
      {
        $descr = "Updated Likes:";

        $cats = array( 100,102,103,105,107 );

        $c = 0;
//      echo "select pages.gid from personal left join pages on pages.gid where personal.uid='" . $feed['uid'] . "' and pages.cat IN ('" . implode( $cats, "','" ) . "'";
//        $q2 = mysql_query( "select pages.gid from personal left join pages on pages.gid where personal.uid='" . $feed['uid'] . "' and pages.cat IN ('" . implode( $cats, "','" ) . "'" );

        while( $r2 = mysql_fetch_array( $q2 ) )
        {
          if( $c > 0 ) $descr .= ",";
          $info = $API->getPageInfo( $r2['gid'] );
          $descr .= " <a href=\"" . $info['url'] . "\">" . $info['gname'] . "</a>";
          $c++;
        }
        $continue=false;
      }
      break;

      case "PR0":
      {
        $resolved[] = array( "descr" => 'Updated Personal Information' );
  			$continue = false;
      }
      break;

      case "VP1": //Vessel updated their nav status
      {
        global $fromEmail;

        if( $feed['id'] == 0 ) //Initially feeds weren't storing the ID of the ais history; now it is ...
          $data = queryArray( "select navstat, sog, `dest` as destination, eta, lat, lon, last_update, cog from boats_ais_history where gid='" . $feed['gid'] . "' and last_update <= '" . $feed['ts'] . "' order by last_update desc" );
        else
          $data = queryArray( "select navstat, sog, `dest` as destination, eta, lat, lon, last_update, cog from boats_ais_history where id='" . $feed['id'] . "'" );


        $descr = "<TIME>";

        $title = "Time: " . date( "Hi", strtotime( $feed['ts'] ) ) . " UTC";

        $title = $descr;
        if( $data['lat'] > 0.0 )
        {
          if( $fromEmail || $feed['id'] == 0 )
            $descr .= "<br />Position: <a style=\"font-size:9pt; font-weight:300; color: #326798; text-decoration:none;\" href=\"" . $API->getPageURL($feed['gid']) . "\">Lat: " . $data['lat'] . " / Lon: " . $data['lon'] . "</a>";
          else
            $descr .= "<br />Position: <a style=\"font-size:9pt; font-weight:300; color: #326798; text-decoration:none;\" href=\"javascript:void(0);\" onclick=\"javascript:openVesselMap(" . $feed['gid'] . "," . $feed['id'] . ",1);\">Lat: " . $data['lat'] . " / Lon: " . $data['lon'] . "</a>";

        }

        switch( $data['navstat'] )
        {
          case 0:
          case 8:
            $img = "/images/sailing-ship-icon.png";

            $descr = "is now underway<br />" . $descr;

            if( $data['sog'] > 0.0 )
              $descr .= "<br />SOG: " . $data['sog'] . " knts";
            if( $data['cog'] > 0.0 )
              $descr .= " | COG: " . $data['cog'] . "&deg;";

            if( strlen( $data['destination'] ) > 0 )
              $descr .= "<br />Destination: " . $data['destination'];

            if( strlen( $data['eta'] ) > 0 )
              $descr .= "<br />ETA: " . $data['eta'];

          break;

          case 5:
            $img = "/images/map.png";
            $descr = "is now moored<br />" . $descr;
            $descr .= "<br />Status: Moored";
          break;

          default:
            $img = "/images/anchor.png";
            $descr = "is now anchored<br />" . $descr;
//            $descr .= "<br />Status: Anchored";
          break;
        }

        $email = str_replace( "<TIME>", "Time: " . date( "Hi", strtotime( $feed['ts'] ) ) . " UTC | " . date( "M j, Y", strtotime( $feed['ts'] ) ), $descr );
        $descr = str_replace( "<TIME>", "Time: " . date( "Hi", strtotime( $feed['ts'] ) ) . " UTC", $descr );

        //<div style="clear: both; border-bottom: 1px dotted #326798; height: 1px; width: 535px; padding-top: 25px; margin-bottom: 20px;"></div>

        $resolved[] = array( "descr" => $descr, "img" => $img, "title" => $title, "emaildesc" => $email );
        $continue = false;
      }
      break;

      case "VP2": //Vessel name change
        $img = "/images/sailing-ship-icon.png";
        $descr = "Had a name change";
        $continue = false;

        $resolved[] = array( "descr" => $descr, "img" => $img, "emaildesc" => $descr );
      break;

      case "VP3": //Upcated vessel specifications
        $img = "/images/sailing-ship-icon.png";
        $descr = "Updated Vessel Specifications";
        $continue = false;
        $resolved[] = array( "descr" => $descr, "img" => $img, "emaildesc" => $descr );
      break;

      case "VP4": //Vessel out of range
        $img = "/images/sailing-ship-icon.png";
        $descr = "Vessel out of range";
        $continue = false;
        $resolved[] = array( "descr" => $descr, "img" => $img, "emaildesc" => $descr );
      break;

			default:
      $continue = false;
			//die("<xmp>Resolution failed on item: " . var_export($item, true) . "\n\ndumping progress: " . var_export($resolved, true) . "</xmp>");
			break;
		}

    if( empty( $resolved[ sizeof( $resolved ) - 1 ] ) )
    {
      return null;
    }
	}
	while ($continue);

	return $resolved;
}

function startLogEntry_OLD($feed, $by, $media, $feedNo, $showDeleteLink = false)
{
	global $API;

	//did the owner of this media add it to his own feed, and are we viewing that feed now
	$isPostersFeed = $feed['uid'] == $feed['uid_by'];

	$profileURL = $API->getProfileURL($by['uid'], $by['username']);
	$profilePic = $API->getUserPic($by['uid'], $by['pic']);
	$parts = array(rawurlencode($profileURL), rawurlencode($profilePic), rawurlencode($by['twusername']), rawurlencode($by['fbid']), rawurlencode($by['name']));
	$id = implode("-", $parts);

	?>
	<div class="logentry"<?=( $showDeleteLink && $feed['fid']) ? ' id="feed-' . $feed['fid'] . '"' : ""?>>
		<div class="pic">
			<? 
      $loggedin = $API->isLoggedIn();
      if( $loggedin ) {
      ?>
      <a title="<?=$feed['fid']?>" id="11<?=$feedNo?>-<?=$id?>" onmouseover="javascript:showTip2(this,<?=$by['uid']?>,'U');" onmouseout="javascript:tipMouseOut();" href="<?=$profileURL?>">
      <? 
      }
      ?>
      <img src="<?=$API->getThumbURL(1, 48, 48, $profilePic)?>" alt="" />
      <?
      if( $loggedin ) {
      ?>      
      </a>
      <?
      }
      ?>
		</div>
		<div class="title_container">
			 <div class="title">
				<div class="border">
					<div class="alignbottom">
						<?=date("F j, Y", strtotime($feed['ts']))?>&nbsp; <a href="<?=$profileURL?>"><?=$by['name']?></a>
						<?php
						unset($action_by);

						switch ($media['type'])
						{
							case "g":
							$action = 'created a <a href="' . $media['url'] . '">page</a>';
							break;
							
							case "V":
							case "P":
							case "A":
							$word = typeToWord($media['type']);

							if ($isPostersFeed)
								if ($media['uid'] == $feed['uid'])
									$action = 'added';
								else
								{
									$info = $API->getUserInfo($media['uid'], "uid,username,name");
									$action = 'favorited';
									$action_by = 'by <a href="' . $API->getProfileURL($info['uid'], $info['username']) . '">' . $info['name'] . '</a>';
								}
							else
								$action = 'shared';

							if ($feed['gid'] > 0)
							{
								$page = $API->getPageInfo($feed['gid']);
								$toPage = " to the <a href=\"" . $API->getPageURL($feed['gid'], $feed['gname']) . "\">{$page['gname']}</a> page";
							}
							
							$action .= ' ' . a_an($word) . ' <a href="' . $API->getMediaURL($media['type'], $media['id']) . '">' . $word . '</a> ' . $toPage . $action_by;
						}
						echo $action;

						 if ($showDeleteLink)
							echo '<div class="delete"><a href="javascript:void(0);" onclick="javascript:' . $API->jsDeleteFeed($feed['fid']) . '">X</a></div>';
						 ?>
					</div>
				</div>
			 </div>
		</div>
		<div style="clear: both; padding-left: 55px;">
	<?php
}

function getLogEntrySearchQuery($q)
{
	return "
		(
		SELECT type,link,comment AS post,uid,created AS ts,username,name,id,null AS username_to,null AS uid_to,twusername,fbid,pic
		FROM comments
		NATURAL JOIN users
		WHERE COMMENT LIKE '%$q%' and type!='M' and users.active=1
		)
		UNION
		(
		SELECT 'W' AS type,NULL AS link,post,u.uid,ts,u.username,u.name,sid AS id,v.username AS username_to,uid_to,u.twusername,u.fbid,u.pic
		FROM wallposts w
		NATURAL JOIN users u
		INNER JOIN users v ON w.uid_to=v.uid
		WHERE post LIKE '%$q%' and u.active=1
		) order by ts desc
			";
}

function showLogEntrySearchResult($result, $q)
{
	global $API;

	$entry = array(
			"feeds" => array(
				array(
					"fid" => md5(microtime()),
					"ts" => $result['ts']
				)
			),
			"actor_info" => array(
				"name" => $result['name'],
				"profileurl" => $API->getProfileURL($result['uid'], $result['username']),
				"profilepic" => $API->getUserPic($result['uid'], $result['pic']),
				"twusername" => $result['twusername'],
				"fbid" => $result['fbid']
				)
		);

	if ($result['link']) //comment
	{
		switch ($result['type'])
		{
			case "W":
			$user = queryArray("select u.uid,u.username from wallposts inner join users u on u.uid=uid_to where sid={$result['link']}");
			$url = $API->getProfileURL($user['uid'], $user['username']) . '#' . $result['type'] . $result['link'];
			$word = "log entry";
			break;
			
			default:
			$url = $API->getMediaURL($result['type'], $result['link']);
			$word = typeToWord($result['type']);
		}
		
		$pretext = '<a href="' . $url . '"><img src="/images/comment.png" alt="" />go to ' . $word . ' comment</a>';
	}
	else //log
	{
		if ($result['uid_to'])
			$url = $API->getProfileURL($result['uid_to'], $result['username_to']);
		else
			$url = $entry['actor_info']['profileurl'];

    $fid = quickQuery( "select fid from feed where type='" . $result['type'] . "' and link='" . $result['id'] . "'" );
		$pretext = '<a href="/profile/feed.php?fid=' . $fid . '"><img src="/images/page_white_edit.png" alt="" />go to log entry</a>';
	}
	
	startLogEntry($entry, false, $pretext);

	$post = $result['post'];

	if (empty($q))
		echo $post;
	else
	{
		while (($i = stripos($post, $q)) !== false)
		{
			$html .= substr($post, 0, $i) . "<span class=\"match\">";
			$html .= substr($post, $i, strlen($q));
			$html .= "</span>";
			$post = substr($post, $i + strlen($q));
		}
		echo $html . $post;
	}

	endLogEntry();
}

function startLogEntry($result, $showDeleteLink = false, $pretext = null)
{
	global $API;
  global $script;

  
	//did the owner of this media add it to his own feed, and are we viewing that feed now
	$parts = array(rawurlencode(str_replace( "-", "$", $result['actor_info']['profileurl']) ), rawurlencode($result['actor_info']['profilepic']), rawurlencode( cutOffText( $result['actor_info']['occupation'], 50 ) ), rawurlencode( $result['actor_info']['uid'] ),  rawurlencode($result['actor_info']['name']));
	$id = implode("-", $parts);

	foreach ($result['feeds'] as $feed)
		$fids[] = $feed['fid'];

	echo '<a name="' . $result['resolved']['type'] . $result['resolved']['id'] . '"></a>';

	$fids = implode("-", $fids);

  $popup_type = 'U';
  $actor_id = $result['actor_info']['uid'];

  if( empty( $result['actor_info']['uid'] ) )
  {
    $popup_type = 'P';
    $actor_id = $result['actor_info']['gid'];
  }

	?>

	<div class="logentry" <?=( $showDeleteLink) ? ' id="feed-' . $fids . '"' : ""?> data-actor="<?=htmlentities(json_encode($result['actor_info']))?>">
		<div class="pic">			
			<? 
      $loggedin = $API->isLoggedIn();
      if( $loggedin ) {
      ?>
      <a id="<?=md5(microtime())?>" onmouseover="javascript:showTip2(this,<?=$actor_id?>,'<?=$popup_type?>');" onmouseout="javascript:tipMouseOut();" href="<?=$result['actor_info']['profileurl']?>">
      <? 
      }
      ?>
      <img src="<?=$API->getThumbURL(1, 48, 48, $result['actor_info']['profilepic'])?>" alt="" />
      <?
      if( $loggedin ) {
      ?>      
      </a>
      <?
      }
      ?>      
		</div>
		<div class="title_container">
			 <div class="title">
				<div class="border">
					<div class="alignbottom">
						<?php
						if ($pretext)
							echo "<div class=\"searchmore\">$pretext</div>";

						echo '<div class="text">';
						echo date("F j, Y", strtotime($feed['ts'])) . "&nbsp; <a href=\"{$result['actor_info']['profileurl']}\">{$result['actor_info']['name']}</a> {$result['title']}";

            if( $API->admin && ($script == "admin/masterfeed" || $script == "profile/getlogentries"))
            {
               echo '<a href="javascript:void(0);" onclick="javascript:banUser(\'' . $result['actor_info']['name'] . '\', \'' . $result['actor_info']['uid'] . '\', -1);"><img src="/images/lock.png" alt="" title="Ban User" /></a>';

               echo '<a href="javascript:void(0);" onclick="javascript:compose_from_log($(this).parents(\'.logentry:first\'), {from_uid: 832, hash: \'' . md5($_COOKIE['PHPSESSID'] . "-sending_as_another_user-832") . '\'});"><img src="/images/email.png" alt="" title="Email User" /></a>';
            }

						echo '</div>';
						
						if ($showDeleteLink)
							echo '<div class="delete"><a href="javascript:void(0);" onclick="javascript:' . $API->jsDeleteFeed($fids) . '">X</a></div>';
						?>
					</div>
				</div>
			 </div>
		</div>
		<div class="content">  <? //Changing the width of this screws up the alignment of the delete X's in the log?>

        <?php
    }

    function showLogEntryFull($result)
{
    /*echo '<xmp>';
    print_r($result);
    echo '</xmp>';*/

    global $API;
    echo '	<div class="media">';

    if (!empty($result['post']))
    echo '<div class="logpost">' . findLinks($result['post']) . '</div>';

    if (in_array($result['resolved']['type'], array("A", "P", "V"))) //is this media?
    {       ob_start();
            showTags($result['resolved'], $result['resolved']['type'], false, chr(1));
            $html = ob_get_contents();
            ob_end_clean();
            if (strpos($html, chr(1)) === false)
            echo '<div style="clear: both; padding: 3px 0;">' . $html . '</div>';
        }

    if (count($result['img']) > 1)
    {
            for ($i = 0; $i < count($result['img']); $i++)
            {
                //see what the width of this image would be if we scale it to max dimensions of 55x75
                $widthTotal += 5 + current(scaleImage($result['img'][$i]['width'], $result['img'][$i]['height'], 55, 75, false));
                if ($widthTotal > 387) break;
                showAlbumLogImage($result['img'][$i]);
            }

            echo '<div style="clear: both; height: 5px;"></div>';
        }
elseif (isset($result['img']))
    {
            echo '	<div class="mleft">';
            if( isset( $result['media_popup'] ) )
            {
          echo "<a href=\"javascript:void(0);\" onclick=\"javascript: showMediaPopup(" . $result['media_popup']['id'] . ", '" . $result['media_popup']['type'] . "');\">";
      }
        else
            {
          echo '<a href="' . $result['url'] . '" target="' . (startsWith($result['url'], 'http') ? '_new' : '_self') . '">';
      }

            if (substr($result['img'], 0, 4) == 'http') // external image
            {
            if (strpos($result['img'], 'rackcdn.com/wp_') && quickQuery("select TIME_TO_SEC(TIMEDIFF(now(), '{$result['resolved']['ts']}'))") < 300)
            {
                // this is a new log post and the image is _possibly_ not uploaded to the cloud yet
                $tmp_file = end(explode('/', $result['img']));
                if (file_exists('/tmp/' . $tmp_file))
                {
                    exit($result['img']);
                    $result['img'] = '/images/tmp_image.php?u=' . urlencode($result['img']);
                    //file_put_contents('/tmp/test', '3', FILE_APPEND);
//                    exit($result['img']);
                }
                //echo '<img src="' . $result['img'] . '" alt>';
            }
            $size = getimagesize($result['img']);
            if ($size[0] >= 365)
            echo '<img src="' . $result['img'] . '" width="370px" alt>';
        else
            echo '<img src="' . $result['img'] . '" width="119px" alt>';

//            echo '<img src="' . $result['img'] . '" width="385px" alt>';
        }
        else{
            // if youtube video
            if ($result['resolved']['type'] == 'V' && strlen($result['resolved']['hash']) < 32){
                $id = $result['resolved']['hash'];
                $img = "http://img.youtube.com/vi/".$id."/0.jpg";
            }
        else{
                $img = 'http://'.$_SERVER['HTTP_HOST'].$API->getThumbURL(0, 120, 95, $result['img']);
            }
//            $size = getimagesize('http://'.$_SERVER['HTTP_HOST'].$API->getThumbURL(0, 120, 95, $result['img']));
            $size = getimagesize($img);
//            if ($size[0] >= 385)
//			    echo '<img src="' . $API->getThumbURL(0, 120, 95, $result['img']) . '" width="385px" alt>';
//            else
//                echo '<img src="' . $API->getThumbURL(0, 120, 95, $result['img']) . '" width="119px" alt>';
            if ($size[0] >= 365)
            echo '<img src="' . $img . '" width="370px" alt>';
        else
            echo '<img src="' . $img . '" width="119px" alt>';
        }

            echo '				</a>
					</div>';
        }

    // if we are showing a larger thumbnail or there is no image, make the
    // text go all the way across - else, make the text column "skinny"
    // and put it to the right of the thumbnail
    echo '		<div class="minfo' . (substr($result['img'], -6) == '_l.jpg' || empty($result['img']) ? '' : ' minfo_skinny') . '">';

    if (isset($result['subtitle']))
    echo '			<span class="mtitle" style="width: 385px"><a href="' . $result['url'] . '" target="' . (startsWith($result['url'], 'http') ? '_new' : '_self') . '">' . $result['subtitle'] . '</a></span>';

    if (isset($result['url']) && $result['is_link']){
            echo '			<br><span class="murl"><a href="' . $result['url'] . '" target="' . (startsWith($result['url'], 'http') ? '_new' : '_self') . '">' . $result['url'] . '</a></span>';
        }
    $descr = $result['descr'];
    $len = 200; //Only show 150 characters of the description.

    if(preg_match('/(myloglink)/', $descr)>0)
    {
        $descr = str_replace('\\','',$descr);
    }
elseif( strlen( $descr ) > $len && $result['resolved']['type'] != 'rss' )
    {
        $tmp = json_decode($descr, true);
        if (!empty($tmp['descr']))
            $descr = $tmp['descr'];

        $original_descr = $descr;
        $descr = substr( $descr, 0, $len );

        $last_space = strlen( $descr ) - 1;

        while( substr( $descr, $last_space, 1 ) != " "  && $last_space > 0)
        $last_space--;

        $last_part = substr( $original_descr, $last_space + 1);
        $descr = substr( $descr, 0, $last_space);

        $descr = findLinks(str_replace("  ", "&nbsp; ", $descr ));
        $last_part = findLinks(str_replace("  ", "&nbsp; ", $last_part ));

        $span_id = "showmore-" . $result['resolved']['id'];
        $span_id2 = $span_id . "txt";
        $descr .= " <span id=\"$span_id\"><a href=\"javascript:none;\" onclick=\"javascript:swapElementDisplay('$span_id', '$span_id2');\">... read more</a></span> <span id=\"$span_id2\" style=\"display:none;\">$last_part <a href=\"javascript:none;\" onclick=\"javascript:swapElementDisplay2('$span_id', '$span_id2');\">&nbsp;&nbsp;&nbsp; show less</a></span>";
    }
else
    {
        $descr = findLinks(str_replace("  ", "&nbsp; ", $result['descr'] ));
    }

    echo '			<div class="mdescr" style="' . (isset($result['subtitle']) ? '' : 'font-size: 8pt;') . '; width: 385px">
							' . nl2br($descr) . '
						</div>
					</div>
			</div>';
}

function showLogEntryMini($result, $showDeleteLink = false)
{
	global $API;
	
	foreach ($result['feeds'] as $feed)
		$fids[] = $feed['fid'];
	
	$fids = implode("-", $fids);

	echo '<div class="mini" id="feed-' . $fids . '"><a><img src="' . $result['img'] . '" alt="" /></a> ' . $result['descr'];
	echo '</div>';

	if ($showDeleteLink)
		echo '<div class="delete" id="deletefeed-' . $fids . '"><a href="javascript:void(0);" onclick="javascript:' . $API->jsDeleteFeed($fids) . '">X</a></div>';
}

function miniLogEntry_OLD($mini, $showDeleteLink = false)
{
	global $API;
	echo '<div class="mini" id="feed-' . $mini['fid'] . '"><a title="' . $mini['fid'] . '"><img src="' . $mini['img'] . '" alt="" /></a> ' . $mini['html'];
	if ($showDeleteLink)
		echo '<div class="delete"><a href="javascript:void(0);" onclick="javascript:' . $API->jsDeleteFeed($mini['fid']) . '">X</a></div>';
	echo '</div>';
}

function commentsLogEntry($media, $fid)
{
	global $API;
  ?>
  <div style="clear: both; padding-top: 5px; width:350px;">
		<?php
		    showCommentsArea($media['type'], $media['id'], $media['jmp'], $API->uid == $media['uid'] && $gid == 0, 1, $fid);
		?>
	</div>
	<?php
}

function wallPostLogEntry($wallpost)
{
	echo '<div class="wallpost">' . findLinks($wallpost['post']) . '</div>';
}

function showAlbumLogImage($img)
{
	global $API;
	?>
	<div class="logalbum">
		<a href="javascript:void(0);" onclick="javascript:showMediaPopup(<?=$img['id']?>,'P');"><img src="<?=$API->getThumbURL(0, 55, 75, "/photos/" . $img['id'] . "/" . $img['hash'] . ".jpg")?>" alt="" /></a>
	</div>
	<?php
}

//sees if the user viewing the media is the one tagged and fills in gaps in information
function isUserTagged($tag)
{
	global $API;

	if ($tag['cid'] == $API->uid && $tag['site'] == SITE_OURS)
	{
		$tag['username'] = $API->username;
		$tag['name'] = $API->name;
		$tag['self'] = true;
	}

	return $tag;
}

function showTags($media, $type, $showRemove = true, $nobody = "Nobody has been tagged yet.")
{
	global $API;
	
	if ($type == "A")
	{
		$type = "P";
		$isAlbum = true;
	}
	else
		$isAlbum = false;
	
	$wordType = typeToWord($type);
	?>
		<b>In this <?=$isAlbum ? "album" : $wordType?>:</b>&nbsp;
		<?php
		$i = 0;

		$q1 = "select -1 as eid,tid,contacts.cid,{$wordType}_tags.uid," . ($type == "P" ? "x,y," : "") . "name,contacts.site,null as username, 0 as gid from {$wordType}_tags " . ($isAlbum ? "inner join photos on photo_tags.pid=photos.id inner join albums on photos.aid=albums.id " : "") . "inner join contacts on {$wordType}_tags.uid=contacts.uid and {$wordType}_tags.cid=contacts.cid where contacts.site not in (" . SITE_OURS . "," . SITE_CUSTOM . ") and " . ($isAlbum ? "a" : $type) . "id=" . $media['id'];
		$q2 = "select eid,tid,contacts.cid,{$wordType}_tags.uid," . ($type == "P" ? "x,y," : "") . "users.name,contacts.site,username, 0 as gid from {$wordType}_tags " . ($isAlbum ? "inner join photos on photo_tags.pid=photos.id inner join albums on photos.aid=albums.id " : "") . "inner join contacts on {$wordType}_tags.cid=contacts.cid inner join users on users.uid=eid where contacts.cid > 0 and contacts.site=" . SITE_OURS . " and " . ($isAlbum ? "a" : $type) . "id=" . $media['id'];
		$q3 = "select {$wordType}_tags.uid as eid,tid,concat('X',{$wordType}_tags.uid) as cid,{$wordType}_tags.uid," . ($type == "P" ? "x,y," : "") . "users.name," . SITE_OURS . " as site,username, 0 as gid from {$wordType}_tags " . ($isAlbum ? "inner join photos on photo_tags.pid=photos.id inner join albums on photos.aid=albums.id " : "") . "inner join users on users.uid={$wordType}_tags.uid where cid = 0 and " . ($isAlbum ? "a" : $type) . "id=" . $media['id'];
		$q4 = "select -1 as eid,tid,cid,{$wordType}_tags.uid," . ($type == "P" ? "x,y," : "") . "name," . SITE_CUSTOM . " as site,null as username,gid from {$wordType}_tags " . ($isAlbum ? "inner join photos on photo_tags.pid=photos.id inner join albums on photos.aid=albums.id " : "") . "inner join custom_tags on {$wordType}_tags.cid=custom_tags.id where custom=1 and " . ($isAlbum ? "a" : $type) . "id=" . $media['id'];
		$q = "select * from (($q1) union ($q2) union ($q3) union ($q4)) as tmp group by cid order by " . ($type == "V" ? "name" : "x");

		$x = mysql_query($q) or die(mysql_error());

		if ($type == "P")
		{
			while ($tag = mysql_fetch_array($x, MYSQL_ASSOC))
			{
				$tag = isUserTagged($tag);
				echo '<span id="tagged-' . $tag['tid'] . '">';
				if ($i > 0) echo ", ";

        $href = 'javascript:void(0);';
        if($tag['site'] == SITE_OURS ){
          $href = $API->getProfileURL($tag['eid'], $tag['username']);
            $blue = true;
        }
        else if($tag['site'] == SITE_CUSTOM && $tag['gid']>0){
          $blue = true;
          $tag['name'] = quickQuery("select gname from pages where gid = ".$tag['gid']);
          $href = $API->getPageURL($tag['gid']);
        }
        else{
            $res = mysql_query("SELECT uid FROM users INNER JOIN custom_tags ON custom_tags.email = users.email
            WHERE custom_tags.id = ".$tag['cid']);

            if (mysql_num_rows($res) > 0){
                $r = array();
                while ($row = mysql_fetch_assoc($res))
                    $r['id'] = $row['uid'];
                $href = $API->getProfileURL($r['id']);
                $blue = true;
            }
        }

                if ($href == 'javascript:void(0);')
                    $blue = false;


//                if (!$blue)
//                    $blue = ($tag['site'] == SITE_OURS || $tag['gid'] > 0 );

				echo '<a style="' . ($blue  ? '' : 'cursor: default; color: #555;' ) . '" href="' . $href . '" onmouseout="javascript:placeTagBox(-1, -1, \'\', \'\');" onmouseover="javascript:placeTagBox(' . $tag['x'] . ', ' . $tag['y'] . ', false, \'' . $tag['name'] . '\');">' . $tag['name'] . '</a>';
				if ($showRemove && ($tag['self'] || $tag['uid'] == $API->uid || $media['uid'] == $API->uid))
					echo ' (<a href="javascript:void(0);" onclick="javascript:deleteTag(\'P\',' . $tag['tid'] . ', \'' . $API->generateDeleteCommentHash($tag['tid']) . '\');">remove</a>)';
				$tagJs[] = "tags[$i] = [" . implode(", ", array($tag['x'], $tag['y'], '"' . addslashes($tag['name']) . '"')) . "];";
				echo "</span>";
				$i++;
			}
		}
		else
		{
			while ($tag = mysql_fetch_array($x, MYSQL_ASSOC))
			{
				$tag = isUserTagged($tag);
				echo '<span id="tagged-' . $tag['tid'] . '">';
				if ($i > 0) echo ", ";

        $href = 'javascript:void(0);';
        if($tag['site'] == SITE_OURS )
          $href = $API->getProfileURL($tag['eid'], $tag['username']);
        else if($tag['site'] == SITE_CUSTOM && $tag['gid']>0)
          $href = $API->getPageURL($tag['gid']);

        $blue = ($tag['site'] == SITE_OURS || $tag['gid'] > 0 );

				echo '<a style="' . ($blue ? '' : 'cursor: default; color: #555;') . '" href="' . $href . '">' . $tag['name'] . '</a>';
				if ($showRemove && ($tag['self'] || $tag['uid'] == $API->uid || $media['uid'] == $API->uid))
        {
          $tag['cid'] = quickQuery( "select cid from video_tags where tid='" . $tag['tid'] . "'" );
 					echo ' (<a href="javascript:void(0);" onclick="javascript:deleteTag(\'V\', ' . $tag['tid'] . ', \'' . $API->generateDeleteCommentHash($tag['tid']) . '\', \'' . $tag['cid'] . '\');">remove</a>)';
        }
				echo "</span>";
				$i++;
			}
		}
		
		if (mysql_num_rows($x) == 0)
			echo $nobody;
		
		return $tagJs;
}

function mediaLogEntry($media)
{
	global $API;
	
	// find if this is actual media or a page
	$isMedia = in_array($media['type'], array("P", "A", "V"));
	?>
	<div class="media">
		<?php
		if ($isMedia)
		{
			ob_start();
			showTags($media, $media['type'], false, chr(1));
			$html = ob_get_contents();
			ob_end_clean();
			if (strpos($html, chr(1)) === false)
				echo '<div style="clear: both; padding: 3px 0;">' . $html . '</div>';
		}

		if ($media['type'] == "A")
		{
			$x = mysql_query("select hash,photos.id,title,descr,mainimage,height,width from photos inner join albums on albums.id=photos.aid where aid=" . $media['aid']);
			$i = 0;
			while ($img = mysql_fetch_array($x, MYSQL_ASSOC))
			{
				if ($img['id'] == $img['mainimage'])
					$imgs[0] = $img; //show the main image first
				else
					$imgs[++$i] = $img;
			}
			
			for ($i = 0; $i < count($imgs); $i++)
			{
				//see what the width of this image would be if we scale it to max dimensions of 55x75
				$widthTotal += 5 + current(scaleImage($imgs[$i]['width'], $imgs[$i]['height'], 55, 75, false));
				if ($widthTotal > 441) break;
				showAlbumLogImage($imgs[$i]);
			}
			
			$mainURL = $API->getPhotoURL($imgs[0]['id'], $imgs[0]['title']);
		}
		elseif ($media['type'] == "g")
		{
			$mainURL = $media['url'];
			$media['title'] = $media['gname'];
			$media['descr'] = $media['gdescr'];
			?>
			<div style="float: left;">
				<a href="<?=$mainURL?>">
					<img src="<?=$API->getThumbURL(0, 130, 195, $API->getMediaThumb("P", $media['pid'], $media['hash']))?>" alt="" />
				</a>
			</div>
			<?php
		}
		else
		{
			$mainURL = $API->getMediaURL($media['type'], $media['id'], $media['title']);
			?>
			<div style="float: left;">
				<a href="<?=$mainURL?>">
					<img src="<?=$API->getThumbURL(0, 130, 195, $API->getMediaThumb($media['type'], $media['id'], $media['hash']))?>" alt="" />
				</a>
			</div>
			<?php
		}
		?>
		<div class="minfo" style="<?=$media['type'] == "A" ? "clear: both; padding: 10px 0 0 0;" : ""?>">
			<span class="mtitle"><a href="<?=$mainURL?>"><?=$media['title']?></a></span>
			<div class="mdescr">
				<?=$media['descr']?>
			</div>
		</div>
	</div>
	<?php
}

function endLogEntry()
{
	global $API;
	echo '</div></div><div style="clear: both; height: 10px;"></div>';
}

//////// i didn't write these:


function findLinks($text, $blank = true )
{
# this functions deserves credit to the fine folks at phpbb.com 

$text = preg_replace('#(script|about|applet|activex|chrome):#is', "\\1:", $text); 

// pad it with a space so we can match things at the start of the 1st line. 
$ret = ' ' . $text; 

// matches an "xxxx://yyyy" URL at the start of a line, or after a space. 
// xxxx can only be alpha characters. 
// yyyy is anything up to the first space, newline, comma, double quote or <
$ret = preg_replace("#(^|[\n ])([\w]+?://[\w\#$%&~/.\-;:=,?@\[\]+]*)#is", "\\1<a href=\"http://" . SERVER_HOST . "/extlink.php?\\2\" " . ($blank ? "target=\"_blank\"" : "" ) . ">\\2</a>", $ret);

// matches a "www|ftp.xxxx.yyyy[/zzzz]" kinda lazy URL thing
// Must contain at least 2 dots. xxxx contains either alphanum, or "-" 
// zzzz is optional.. will contain everything up to the first space, newline, 
// comma, double quote or <. 
$ret = preg_replace("#(^|[\n ])((www|ftp)\.[\w\#$%&~/.\-;:=,?@\[\]+]*)#is", "\\1<a href=\"http://" . SERVER_HOST . "/extlink.php?http://\\2\" " . ($blank ? "target=\"_blank\"" : "" ) . ">\\2</a>", $ret);

// matches an email@domain type address at the start of a line, or after a space. 
// Note: Only the followed chars are valid; alphanums, "-", "_" and or ".". 
$ret = preg_replace("#(^|[\n ])([a-z0-9&\-_.]+?)@([\w\-]+\.([\w\-\.]+\.)*[\w]+)#i", "\\1<a href=\"mailto:\\2@\\3\">\\2@\\3</a>", $ret); 

// Remove our padding..
$ret = substr($ret, 1); 
return $ret; 
} 

function TimeAgo($datefrom)
{
	return "<span id=\"timeago-$datefrom\"></span>";
}

function getTimeAgo($datefrom,$dateto=-1,$long=false)
{
// Defaults and assume if 0 is passed in that
// its an error rather than the epoch

if($datefrom<=0) { return "A long time ago"; }
if($dateto==-1) { $dateto = time(); }

// Calculate the difference in seconds betweeen
// the two timestamps

$difference = $dateto+14400 - $datefrom;

// If difference is less than 60 seconds,
// seconds is a good interval of choice

if($difference < 60)
{
$interval = "s";
}

// If difference is between 60 seconds and
// 60 minutes, minutes is a good interval
elseif($difference >= 60 && $difference<60*60)
{
$interval = "n";
}

// If difference is between 1 hour and 24 hours
// hours is a good interval
elseif($difference >= 60*60 && $difference<60*60*24)
{
$interval = "h";
}

// If difference is between 1 day and 7 days
// days is a good interval
elseif($difference >= 60*60*24 && $difference<60*60*24*7)
{
$interval = "d";
}

// If difference is between 1 week and 30 days
// weeks is a good interval
elseif($difference >= 60*60*24*7 && $difference <
60*60*24*30)
{
$interval = "ww";
}

// If difference is between 30 days and 365 days
// months is a good interval, again, the same thing
// applies, if the 29th February happens to exist
// between your 2 dates, the function will return
// the 'incorrect' value for a day
elseif($difference >= 60*60*24*30 && $difference <
60*60*24*365)
{
$interval = "m";
}

// If difference is greater than or equal to 365
// days, return year. This will be incorrect if
// for example, you call the function on the 28th April
// 2008 passing in 29th April 2007. It will return
// 1 year ago when in actual fact (yawn!) not quite
// a year has gone by
elseif($difference >= 60*60*24*365)
{
$interval = "y";
}

// Based on the interval, determine the
// number of units between the two dates
// From this point on, you would be hard
// pushed telling the difference between
// this function and DateDiff. If the $datediff
// returned is 1, be sure to return the singular
// of the unit, e.g. 'day' rather 'days'

switch($interval)
{
case "m":
$months_difference = floor($difference / 60 / 60 / 24 /
29);
while (mktime(date("H", $datefrom), date("i", $datefrom),
date("s", $datefrom), date("n", $datefrom)+($months_difference),
date("j", $dateto), date("Y", $datefrom)) < $dateto)
{
$months_difference++;
}
$datediff = $months_difference;

// We need this in here because it is possible
// to have an 'm' interval and a months
// difference of 12 because we are using 29 days
// in a month

if($datediff==12)
{
$datediff--;
}

$res = ($datediff==1) ? "$datediff mo" . ($long ? "nth" : "") . " ago" : "$datediff mo" . ($long ? "nth" : "") . "s ago";
break;

case "y":
$datediff = floor($difference / 60 / 60 / 24 / 365);
$res = ($datediff==1) ? "$datediff y" . ($long ? "ea" : "") . "r ago" : "$datediff y" . ($long ? "ear" : "") . "rs ago";
break;

case "d":
$datediff = floor($difference / 60 / 60 / 24);
$res = ($datediff==1) ? "$datediff day ago" : "$datediff days ago";
break;

case "ww":
$datediff = floor($difference / 60 / 60 / 24 / 7);
$res = ($datediff==1) ? "$datediff w" . ($long ? "ee" : "") . "k ago" : "$datediff w" . ($long ? "eek" : "") . "s ago";
break;

case "h":
$datediff = floor($difference / 60 / 60);
$mins = floor($difference / 60) - ($datediff * 60 );

$res = ($datediff==1) ? "$datediff h" . ($long ? "ou" : "") . "r" : "$datediff h" . ($long ? "ou" : "") . "rs";
$res .= " " . $mins . " m ago";
break;

case "n":
$datediff = floor($difference / 60);
$res = ($datediff==1) ? "$datediff min" . ($long ? "ute" : "") . " ago" : "$datediff min" . ($long ? "ute" : "") . "s ago";
break;

case "s":
$datediff = $difference;
$res = ($datediff==1) ? "$datediff sec" . ($long ? "ond" : "") . " ago" : "$datediff sec" . ($long ? "ond" : "") . "s ago";
break;
}
return $res;
}

function replace_content_inside_delimiters($start, $end, $new, $source) {
return preg_replace('#('.preg_quote($start).')(.*)('.preg_quote($end).')#si', '$1'.$new.'$3', $source);
}

function between($string, $start, $end)
{
	$string = " " . $string;
	$ini = strpos($string, $start);
	if ($ini == 0) return "";
	$ini += strlen($start);
	$len = strpos($string, $end, $ini) - $ini;
	return substr($string, $ini, $len);
}

function cutOffText( $text, $len, $addDots = true )
{
  if( strlen( $text ) < $len ) return $text;

  $text = substr( $text, 0, $len );

  while( substr( $text, strlen( $text ) - 1, 1 ) != " "  && strlen($text) > 0)
    $text = substr( $text, 0, strlen( $text ) - 1 );
   
  if( $addDots )
  {
    $text = $text . "...";
  }
    
  return $text;
}

function timeToHumanTime($time)
{
    $time = time() - $time; // to get the time since that moment

    $tokens = array (
        31536000 => 'year',
        2592000 => 'month',
        604800 => 'week',
        86400 => 'day',
        3600 => 'hour',
        60 => 'minute',
        1 => 'second'
    );

    foreach ($tokens as $unit => $text) {
        if ($time < $unit) continue;
        $numberOfUnits = floor($time / $unit);
        return $numberOfUnits.' '.$text.(($numberOfUnits>1)?'s':'');
    }
}


function code2utf($num){
  if($num<128)
    return chr($num);
  if($num<1024)
    return chr(($num>>6)+192).chr(($num&63)+128);
  if($num<32768)
    return chr(($num>>12)+224).chr((($num>>6)&63)+128)
          .chr(($num&63)+128);
  if($num<2097152)
    return chr(($num>>18)+240).chr((($num>>12)&63)+128)
          .chr((($num>>6)&63)+128).chr(($num&63)+128);
  return '';
}

function unescape($strIn, $iconv_to = 'UTF-8') {
  $strOut = '';
  $iPos = 0;
  $len = strlen ($strIn);
  while ($iPos < $len) {
    $charAt = substr ($strIn, $iPos, 1);
    if ($charAt == '%') {
      $iPos++;
      $charAt = substr ($strIn, $iPos, 1);
      if ($charAt == 'u') {
        // Unicode character
        $iPos++;
        $unicodeHexVal = substr ($strIn, $iPos, 4);
        $unicode = hexdec ($unicodeHexVal);
        $strOut .= code2utf($unicode);
        $iPos += 4;
      }
      else {
        // Escaped ascii character
        $hexVal = substr ($strIn, $iPos, 2);
        if (hexdec($hexVal) > 127) {
          // Convert to Unicode
          $strOut .= code2utf(hexdec ($hexVal));
        }
        else {
          $strOut .= chr (hexdec ($hexVal));
        }
        $iPos += 2;
      }
    }
    else {
      $strOut .= $charAt;
      $iPos++;
    }
  }
  if ($iconv_to != "UTF-8") {
    $strOut = iconv("UTF-8", $iconv_to, $strOut);
  }
  return $strOut;
}

//Function found at: http://stackoverflow.com/questions/2915864/php-how-to-find-the-time-elapsed-since-a-date-time
function timeToHumanTime2($time)
{
  $ret = "";

  $years = floor( $time / 31536000 );
  if( $years > 0 )
    $ret = $years . " years";

  $time -= $years * 31536000;

  $months = floor( $time / 2592000 );
  if( $months > 0 )
    $ret .= " " . $months . " months";
  else
    if( $years == 0 )
      $ret = "Less than a month";

  return trim( $ret );
}

function xml2array($url, $get_attributes = 1, $priority = 'tag')
{
    $contents = "";
    if (!function_exists('xml_parser_create'))
    {
        return array ();
    }
    $parser = xml_parser_create('');
    if (!($fp = @ fopen($url, 'rb')))
    {
        return array ();
    }
    while (!feof($fp))
    {
        $contents .= fread($fp, 8192);
    }
    fclose($fp);
    xml_parser_set_option($parser, XML_OPTION_TARGET_ENCODING, "UTF-8");
    xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
    xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
    xml_parse_into_struct($parser, trim($contents), $xml_values);
    xml_parser_free($parser);
    if (!$xml_values)
        return; //Hmm...
    $xml_array = array ();
    $parents = array ();
    $opened_tags = array ();
    $arr = array ();
    $current = & $xml_array;
    $repeated_tag_index = array ();
    foreach ($xml_values as $data)
    {
        unset ($attributes, $value);
        extract($data);
        $result = array ();
        $attributes_data = array ();
        if (isset ($value))
        {
            if ($priority == 'tag')
                $result = $value;
            else
                $result['value'] = $value;
        }
        if (isset ($attributes) and $get_attributes)
        {
            foreach ($attributes as $attr => $val)
            {
                if ($priority == 'tag')
                    $attributes_data[$attr] = $val;
                else
                    $result['attr'][$attr] = $val; //Set all the attributes in a array called 'attr'
            }
        }
        if ($type == "open")
        {
            $parent[$level -1] = & $current;
            if (!is_array($current) or (!in_array($tag, array_keys($current))))
            {
                $current[$tag] = $result;
                if ($attributes_data)
                    $current[$tag . '_attr'] = $attributes_data;
                $repeated_tag_index[$tag . '_' . $level] = 1;
                $current = & $current[$tag];
            }
            else
            {
                if (isset ($current[$tag][0]))
                {
                    $current[$tag][$repeated_tag_index[$tag . '_' . $level]] = $result;
                    $repeated_tag_index[$tag . '_' . $level]++;
                }
                else
                {
                    $current[$tag] = array (
                        $current[$tag],
                        $result
                    );
                    $repeated_tag_index[$tag . '_' . $level] = 2;
                    if (isset ($current[$tag . '_attr']))
                    {
                        $current[$tag]['0_attr'] = $current[$tag . '_attr'];
                        unset ($current[$tag . '_attr']);
                    }
                }
                $last_item_index = $repeated_tag_index[$tag . '_' . $level] - 1;
                $current = & $current[$tag][$last_item_index];
            }
        }
        elseif ($type == "complete")
        {
            if (!isset ($current[$tag]))
            {
                $current[$tag] = $result;
                $repeated_tag_index[$tag . '_' . $level] = 1;
                if ($priority == 'tag' and $attributes_data)
                    $current[$tag . '_attr'] = $attributes_data;
            }
            else
            {
                if (isset ($current[$tag][0]) and is_array($current[$tag]))
                {
                    $current[$tag][$repeated_tag_index[$tag . '_' . $level]] = $result;
                    if ($priority == 'tag' and $get_attributes and $attributes_data)
                    {
                        $current[$tag][$repeated_tag_index[$tag . '_' . $level] . '_attr'] = $attributes_data;
                    }
                    $repeated_tag_index[$tag . '_' . $level]++;
                }
                else
                {
                    $current[$tag] = array (
                        $current[$tag],
                        $result
                    );
                    $repeated_tag_index[$tag . '_' . $level] = 1;
                    if ($priority == 'tag' and $get_attributes)
                    {
                        if (isset ($current[$tag . '_attr']))
                        {
                            $current[$tag]['0_attr'] = $current[$tag . '_attr'];
                            unset ($current[$tag . '_attr']);
                        }
                        if ($attributes_data)
                        {
                            $current[$tag][$repeated_tag_index[$tag . '_' . $level] . '_attr'] = $attributes_data;
                        }
                    }
                    $repeated_tag_index[$tag . '_' . $level]++; //0 and 1 index is already taken
                }
            }
        }
        elseif ($type == 'close')
        {
            $current = & $parent[$level -1];
        }
    }
    return ($xml_array);
}

function isSSL(){
  if($_SERVER['https'] == 1) /* Apache */ {
    return TRUE;
  } elseif ($_SERVER['https'] == 'on') /* IIS */ {
    return TRUE;
  } elseif ($_SERVER['SERVER_PORT'] == 443) /* others */ {
    return TRUE;
  } else {
    return FALSE; /* just using http */
  }
}

function transitionToSSL()
{
  global $secureUrl, $isDevServer;
  if( $isDevServer ) return;
  $request = $_SERVER['REQUEST_URI'];
  header( "Location: " . $secureUrl . $request );
  exit;
}

function getFeedJMPLink( $fid )
{
  $jmp = quickQuery( "select jmp from feed where fid='$fid'" );

  if( $jmp == "" )
  {
    $url = "http://" . SERVER_HOST . "/profile/feed.php?fid=$fid";
    $jmp = shortenURL( $url );

    mysql_query( "update feed set jmp='$jmp' where fid='$fid' limit 1" );
  }

  return "http://shb.me/" . trim($jmp);
}

function getMediaJMPLink( $id, $type )
{
  $table = "";
  $idname = "id";

  switch( $type )
  {
    case "P":
      $table = "photos";
    break;

    case "V":
      $table = "videos";
    break;

    case "G":
      $table = "pages";
      $idname = "gid"; //Need to change "id" to "gid" for this table only...
    break;

    default:
      return null;
    break;
  }

  $jmp = quickQuery( "select jmp from $table where $idname='$id'" );

  if( $jmp == "" )
  {
    $jmp = shortenMediaURL($type, $id);
    $jmp = str_replace( "http://shb.me/", "", $jmp );
    mysql_query( "update $table set jmp='$jmp' where $idname='$id' limit 1" );
  }

  return "http://shb.me/" . trim($jmp);
}



function showSignUpModule( $name )
{
  global $siteName;
?>
		<div style="margin-top: 3px; border: 1px solid #d8dfea; padding: 5px; font-size: 8pt; background: #F2FBFE; width:168px;">
			<div class="smtitle" style="padding-bottom: 10px;"><?=$name;?> is listed on <?=$siteName?></div>
      <div>Sign up for <? echo $siteName ?> to see present and past employees, photos, videos and which of your connections like this page.</div>
      <div>
        <div style="float:left; padding-top:2px;"><input type="button" style="height: 23px; width: 62px; background: #FFF8CC; border: 1px solid #FF9A66; color: #3B5998; font-weight: bold; font-size: 9pt; font-family: arial; cursor:pointer;" value="Sign Up" onclick="getStarted2();"></div>
        <div style="float: left; text-align:center; color: #808080; padding-top:6px; margin-left:2px;">or login with</div>
        <span style="float:left; padding-top:4px; margin-left:2px;"><a href="/fbconnect/login.php"><img src="/images/facebook16.png" /></a> <a href="/twitter/login.php"><img src="/images/twitter16.png" /></a></span>
      </div>
			<div style="clear: both;"></div>
		</div>
<?
}

?>
