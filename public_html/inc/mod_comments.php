<?php
/*
Displays user comments area for wall posts and media. (also allows users to post new comments)

8/10/2011 - Removed target="_blank" from links in replies (aka, comments)
8/18/2012 - Added automatic updating/refresh of comments from other users without reloading the page.
1/6/2013 - Added support for layout type 4 in showCommentsArea()
*/

function showCommentsArea($type, $link, $jmp, $isUserMediaOwner = false, $layout = 0, $fid)
{


	//layout = 0 is found on media players
	//layout = 1 is found on log
	//layout = 2 is the same as layout 1 but without like/dislike, found in messaging
  //layout = 3 is for the messaging area
  //layout = 4 is designed for 290px wide spaces.

	global $API, $gid, $previous_comments;

  $type = addslashes(substr($type, 0, 1));

	$tl = $layout . $type . $link;

	$origLayout = $layout;

  if( empty( $link ) ) return;

  //Check to see if the comments area for this item has been shown already on the page.
  if( empty( $previous_comments ) )
    $previous_comments = array();

  if( isset( $previous_comments[$tl] ) ) return;

  $previous_comments[$tl] = 1;

//  echo "select comments.*,twusername,fbid,name,pic,username from comments join users on comments.uid=users.uid where type='$type' and link=$link order by comments.id desc";
	$x = sql_query("select comments.*,twusername,fbid,name,pic,username from comments join users on comments.uid=users.uid where type='$type' and link='$link' order by comments.id desc");
	$numComments = mysql_num_rows($x);
	$i = 0;

	if ($numComments > 0)
		while ($i++ < 3 && $y = mysql_fetch_array($x, MYSQL_ASSOC))
			$comments[] = $y;

	?>

	<div class="mod_comments">
	<div style="margin-left:3px; margin-right:3px;">
<!--		<div>
			<?php
				echo '<div style="float: left; padding-top: 10px; padding-right: 15px;" id="commentintro-' . $tl . '">Replies &#0133;</div>';
			?>
		</div>-->
		<?php 
    $hide_icons = array( 
      "facebook share" => false,
      "twitter share" => false,
      "like" => false,
      "email" => false,
      "link" => false,
      "share in page" => false,
      "add to my log" => false,
      "report" => false 
      );
    $reply_button_width = 70; //width of the reply button in pixels;
    
    switch( $layout )
    {
      case 2:
      case 3:
        //We're going to hide everything.
        foreach ($hide_icons as &$value)
        {
          $value = true;
        }
      break;
      
      case 4:
        //$hide_icons['email'] = true;
        $hide_icons['link'] = true;
        $hide_icons['report'] = true;
        //$hide_icons['add to my log'] = true;
        $reply_button_width = 45;
      break;
      
      default:
        break;
    }
    

      if( $type == "A" )
      {
        //$type = "P";
        //$link = quickQuery( "select mainImage from albums where id='$link'" );
      }

      if( $type != "P" && $type != "V" )
      {
        $url = "/profile/feed.php?fid=$fid";
        $jmp = getFeedJMPLink( $fid );
      }
      else
      {
        $url = $API->getMediaURL($type, $link, $media['title']);
        $jmp = getMediaJMPLink( $link, $type );
      }


      $jmp = str_replace( "http://shb.me/", "", $jmp );


      if( $type == "P" )
        $media = $API->getMediaInfo( $type, $link, "ptitle as title, hash, id" );
      else
        $media = $API->getMediaInfo( $type, $link );

      if( $media['title'] == "" && $type == "P" )
        $media = $API->getMediaInfo( $type, $link, "albums.title, hash, id" );
                

		?>
		<div style="padding-top: 10px; font-size: 9pt; float: left; font-weight:300;">
      <?
      if( !$hide_icons['facebook share'] && !$hide_icons['all'] )
      {
      ?>
      <div style="float:left;">
          <a type="button_count" href="javascript:void(0);" onclick="javascript: openPopupWindow( 'http://www.facebook.com/sharer/sharer.php?u=<?= urlencode( "http://" . SERVER_HOST . $url ); ?>&t=<?=urlencode($media['title']);?>', 'Share', 550, 320 );">
<!--          <a type="button_count" href="javascript:void(0);" onclick="Share.facebook()">-->
          <img src="/images/f.png" width="16" height="16" alt="" title="Share on Facebook" />
        </a>
      </div>
      <? 
      } 
      
      if( !$hide_icons['twitter share'] && !$hide_icons['all'] ) 
      {
      ?>
      <div style="float:left; margin-left:20px;">
        <a type="button_count" href="javascript:void(0);" onclick="javascript: openPopupWindow( 'https://twitter.com/intent/tweet?original_referer=<?= urlencode( "http://shb.me/" . $jmp ); ?>&source=tweetbutton&text=<?=urlencode($media['title']);?>&url=<?= urlencode( "http://shb.me/" . $jmp ); ?>&via=<?=SITE_VIA?>', 'Share', 550, 320 );">
          <img src="/images/t.png" width="16" height="16" alt="" title="Share on Twitter" />
        </a>
      </div>
      <? 
      } 
      
      if( !$hide_icons['like'] && !$hide_icons['all'] ) 
      {
      ?>
			<div class="like_buttons" id="itemlike-<?=$tl?>" <?php if (!$API->isLoggedIn()) { ?>onclick="javascript:showLogin();"<?php } ?>>
				<?php showLikeLinks($type, $link, $layout); ?>
			</div>
      <? 
      } 
      
      if( !$hide_icons['email'] && !$hide_icons['all'] ) 
      {
      ?>
			<div style="float:left; margin-left:20px;">
				<a href="javascript:void(0);" onclick="javascript:<?php if ($site == "m") echo "if (showEmail('$type', " . $link . "))"; else echo "loadShare('$type', 1, " . $link . ");";?> addShare();">
          <img src="/images/email_go.png" width="16" height="16" title="e-mail"/>
        </a>
			</div>
      <? 
      } 
      
      if( !$hide_icons['link'] && !$hide_icons['all'] ) 
      {
      ?>
			<div style="float:left; margin-left:20px;">
				<a href="javascript:void(0);" onclick="javascript:showEmbedMedia('<?=$url;?>', '<?=$jmp?>', '<?=$type?>', <?=$link?>);" style="font-weight:300;">
          <img src="/images/link_add.png" alt="" style="vertical-align: bottom;" title="Link"/>
        </a>
			</div>
      <? 
      } 
      
      if( !$hide_icons['share in page'] && !$hide_icons['all'] )
      {
      ?>
			<div style="float:left; margin-left:20px;">
				<a href="javascript:void(0);" onclick="javascript:shareInPage('<?=$type?>', <?=$link?>, '<?=$media['hash']?>');">
          <img src="/images/page_add.png" width="16" height="16" title="Share in page"/>
        </a>
			</div>
      <? 
      } 
      
      if( !$hide_icons['add to my log'] && !$hide_icons['all'] ) 
      {
      ?>
			<div style="float:left; margin-left:20px;">
				<a href="javascript:void(0);" onclick="javascript:addToLog('<?=$type?>', <?=$link?>, '<?=typeToWord($type)?> added to log', this);">
          <img src="/images/layout_add.png" width="16" height="16" title="Add to my log"/>
        </a>
			</div>

      <? 
      } 
      
      if( !$hide_icons['report'] && !$hide_icons['all'] ) 
      {
      ?>
      <div style="float:left; height:16px; border-left: 1px solid #000000; margin-left:20px;"></div>

			<div style="float:left; margin-left:20px;">
				<a href="javascript:void(0);" onclick="javascript:showReport('<?=$type?>', <?=$link?>);" style="font-weight:300;"><img src="/images/report.png" alt="" style="vertical-align: bottom;" title="Report Content"/>&nbsp; Report</a>
			</div>
      <?
      }
      ?>
		</div>
		<div style="clear: both;"></div>
	</div>

	<?php
	if ($numComments > 3)
	{
		?>
		<div class="morecomments<?=$layout?>" id="viewmorecommentswait-<?=$tl?>" style="display: none;">
			<div class="morecomments">
				&nbsp;<img src="/images/barwait.gif" alt="" />&nbsp;
			</div>
		</div>

		<div class="morecomments<?=$layout?>" id="viewmorecomments-<?=$tl?>">
			<div class="morecomments" style="text-align: left;">
				<div style="float: left; width: auto;">
					<a href="javascript:void(0);" onclick="javascript:viewMoreComments({'tl': '<?=$tl?>', 'isowner': '<?=$isUserMediaOwner ? 1 : 0?>', 'layout': '<?=$layout?>', 'reportx': '<?=$origLayout != 2 ? 1 : 0?>', 'type': '<?=$type?>', 'link': <?=$link?>, 'id': lastCommentId['<?=$tl?>']});"><span id="commentsleft-<?=$tl?>"><?=$numComments - 3?></span> more replies &#0133;</a>
				</div>
				<div style="float: right; width: auto;">
					<span id="commentsvisible-<?=$tl?>">3</span> of <?=$numComments?>
				</div>
			</div>
		</div>
		<?php
	}
	?>

	<div class="users_that_like" data-type="<?=$type?>" data-link="<?=$link?>"></div>
	
	<div style="margin: 3px;" id="commentscontainer-<?=$tl?>">
		<?php
		$i = 500; //start i = 500 because we will be showing on the same page as the also-viewed-by tooltips - we need unique ID's for each
		if ($numComments > 0)
		{
			for ($i = 2 > $numComments ? $numComments - 1 : 2; $i > -1; $i--)
        if( !empty($comments[$i]['id']) )
  				showComment($comments[$i], $i, $isUserMediaOwner, $layout, $origLayout != 2 && $origLayout != 3);
		}
		?>
	</div>

	<div style="clear: both;<?=$hideLike ? " height: 5px;" : ""?>"></div>

	<?php /*
		<div class="likes" style="<?=$nLikes == 0 ? 'display: none;' : ''?>"><?=$like == 1 ? "You like this." : ""?></div>
		<div class="dislikes" style="<?=$nDislikes == 0 ? 'display: none;' : ''?>"><?=$like == 0 ? "You dislike this." : ""?></div>
		<div class="remove"><a href="javascript:void(0);" onclick="javascript:itemLike('<?=$type?>',<?=$link?>, -1, '<?=$tl?>', <?=$layout?>);">remove</a></div>
		<div style="clear: both;"></div>
		*/ ?>

	<div class="commentwrite<?=$layout?>" id="commentwrite-<?=$tl?>" <?php if (!$API->isLoggedIn()) { ?>onclick="javascript:showLogin();"<?php } ?>>
    <div>
  		<div style="float:left;"><textarea type="text" style="height:20px;" class="txtwrite<?=$layout?>" id="txtcomment-<?=$tl?>" onkeypress="javascript:commentKeypress(event, '<?=$type?>', <?=$link?>, <?=$layout?>); FitToContent( this, 150 ); " onclick="javascript:txtCommentClick(this, true); document.getElementById('cmt-mediaselection-<?=$tl?>').style.display='';" onblur="javascript:txtCommentClick(this, false);" />Reply to this ...</textarea></div>
		  <div style="float:right;"><input type="text" id="btncomment-<?=$tl?>" style="width: <?=$reply_button_width?>px; height:20px; color: #326798; text-align: center; cursor: pointer;" value="reply" onfocus="javascript:this.blur();" onclick="javascript:this.blur(); submitComment('<?=$type?>', <?=$link?>, <?=$layout?>, <? if( empty($gid) || $gid == 0) echo '0'; else echo $gid ?>);" /></div>
      <div style="clear:both;"></div>
    </div>
<? if( $type != "M" ) { ?>
		<div style="width:100%; clear:both; margin-top:3px; display:none;" id="cmt-mediaselection-<?=$tl?>">
      <? include( "tweetbox_bottom.php" ); ?>

      <div id="postas2" style="float:left; font-size:11px; margin:0px; margin-top:3px; padding:0px;">
        Posting as <span id="targetname-<?=$tl?>">
      <?
        if( empty( $_SESSION['fbpages']['selection'] ) || $_SESSION['fbpages']['selection'] == '0' )
        {
          echo $API->name;
        }
        else
        {
          for( $i = 0; $i < sizeof( $_SESSION['fbpages'] ); $i++ )
            if( $_SESSION['fbpages']['selection'] == $_SESSION['fbpages'][$i]['page_id'] )
            {
              echo $_SESSION['fbpages'][ $i ]['name'];
              break;
            }
        }
      ?>
        (<a href="javascript:void(0);" onclick="javascript:document.getElementById('targetname-<?=$tl?>').style.display='none'; document.getElementById('targetselection-<?=$tl?>').style.display='';">Change</a>)</span>
        <span style="display:none;" id="targetselection-<?=$tl?>">
        <select id="cmt-target_id-<?=$tl?>" name="cmt-target_id-<?=$tl?>" style="width:95px;">
        <option value="0"><? echo $API->name; ?></option>
    <?
        for( $i = 0; $i <  sizeof( $_SESSION['fbpages'] ); $i++ ) { if($_SESSION['fbpages'][$i]['page_id'] == "" ) continue; ?>
        <option value="<? echo $_SESSION['fbpages'][$i]['page_id'] ?>"<? if( $_SESSION['fbpages']['selection'] == $_SESSION['fbpages'][$i]['page_id'] ) echo " SELECTED"; ?>><? echo $_SESSION['fbpages'][$i]['name']; ?></option>
    <? } ?>
      </select></span>

      </div>
      <div style="clear:both;"></div>
		</div>
<? } ?>
	</div>
</div>
	<script language="javascript" type="text/javascript">
	<!--
	firstCommentId['<?=$tl?>'] = <?=intval($comments[0]['id'])?>;
	lastCommentId['<?=$tl?>'] = <?=intval($comments[count($comments) - 1]['id'])?>;
	numComments = <?=$numComments?>;
	document.getElementById("txtcomment-<?=$tl?>").value = defaultComment;
	Queue_viewNewComments('<?=$tl?>', <?=$origLayout?>);
	//-->
	</script>
	<?php
}

function showLikeStats($type, $link)
{
	$dislike = quickQuery("select count(*) from likes where type='$type' and link=$link and likes=0");
	$like = quickQuery("select count(*) from likes where type='$type' and link=$link and likes=1");
	?>
	<span style="color: #326798;">
		<img src="/images/thumb_up.png" alt="" style="vertical-align: bottom;" />&nbsp; <?=$like?> <?=$like != 1 ? "people" : "person"?> &nbsp; &nbsp; &nbsp;
		<img src="/images/thumb_down.png" alt="" style="vertical-align: bottom;" />&nbsp; <?=$dislike?> <?=$dislike != 1 ? "people" : "person"?>
	</span>
	<?php
}

function showLikeLinks($type, $link, $layout)
{
  global $siteName;
	$tl = $layout . $type . $link;
	?>
	<a href="javascript:void(0);" onclick="javascript:setLike('<?=$type?>', <?=$link?>, 1);" style="font-weight: 300;"><img src="/images/thumb_up.png" alt="" style="vertical-align: bottom;" title="Like on <?=$siteName?>"/></a> &nbsp; &nbsp; &nbsp;
	<?php
}

function showComment($comment, $i = 0, $isUserMediaOwner, $layout = 0, $showReportX = true)
{
	global $API;

	if (empty($comment['uid']))
		$comment['uid'] = $API->uid;

	if (is_null($comment['pic']))
		$comment['pic'] = $API->pic;

	if (empty($comment['created']))
		$comment['created'] = date("r");
	
	if (empty($comment['name']))
		$comment['name'] = $API->name;

  $url = $API->getProfileURL($comment['uid'], $comment['username']);

  if( $comment['gid'] > 0 )
  {
		$x = sql_query("select gname as name, pid from pages where gid=" . $comment['gid'] );
    $r = mysql_fetch_array( $x );

    $comment['name'] = $r['gname'];
    $comment['pic'] = $r['pid'];
    $comment['uid'] = $comment['gid'];
    $url = $API->getPageUrl( $comment['gid'] );
  }

	?>
	<div class="comment<?=$layout?>" id="comment-<?=$layout . $comment['id'];?>">
		<?php
		$profileURL = $API->getProfileURL($comment['uid'], $comment['username']);
		$profilePic = $API->getUserPic($comment['uid'], $comment['pic']);
		$parts = array(rawurlencode($profileURL), rawurlencode($profilePic), rawurlencode($comment['twusername']), rawurlencode($comment['fbid']), rawurlencode($comment['name']));
		$id = implode("-", $parts);
    
	if ($API->isLoggedIn())
		echo '<a id="' . $i . '-' . $id . '" onmouseover="javascript:showTip(3, this);" onmouseout="javascript:tipMouseOut();" href="' . $profileURL . '">';
	
	echo '<img width="48" height="48" src="' . $API->getThumbURL(1, 48, 48, $profilePic) . '" alt="" />';
	
	if ($API->isLoggedIn())
		echo '</a>';
	
    ?>

		<div style="float: right;">
			<div class="commenthead">
				<div style="float: left; width: 75%; white-space: nowrap;">
					<?=formatDate($comment['created'])?> <a href="<?=$url?>"><?=$comment['name']?></a> wrote:
				</div>
				<?php if ($API->isLoggedIn() && $showReportX) { ?>
				<div style="float: right; width: 25%; text-align: right;">
					<a href="javascript:void(0);" onclick="javascript:showReport('C', <?=$comment['id']?>)">report</a> <?php if ($comment['uid'] == $API->uid || $isUserMediaOwner) { ?>&nbsp;|&nbsp; <a href="javascript:void(0);" onclick="javascript:deleteComment( document.getElementById('comment-<?=$layout . $comment['id'];?>'), '<?=$comment['id']?>', '<?=$API->generateDeleteCommentHash($comment['id'])?>');">X</a><?php } ?>&nbsp;
				</div>
				<?php } ?>
				<div style="clear: both;"></div>
			</div>
			<?
            $s = "�";
            $text = str_replace("  ", "&nbsp; ", nl2br($comment['comment']));
            $text = str_replace($s, "'", $text);
//      echo findLinks(str_replace("  ", "&nbsp; ", nl2br($comment['comment']) ), false);
      echo findLinks($text, false);

      ?>
		</div>
		<div style="clear: both;"></div>
	</div>
	<?php
}
?>