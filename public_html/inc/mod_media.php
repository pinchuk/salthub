<?php

function showMediaModule()
{
	global $API;
	?>
	<div style="font-size: 9pt; color: #000;" id="mediamodulecontainer">
		<div class="contentlinks">
			<?php if (!$API->isLoggedIn()) {?><div style="display: none;"><?php } ?><a href="javascript:void(0);" onclick="javascript:showMedia('ive', '', '', 0);"><span id="mmive">Media I've Viewed</span></a> |<?php if (!$API->isLoggedIn()) {?></div><?php } ?>
			<a href="javascript:void(0);" onclick="javascript:showMedia('browse', 'new', '', 0);"><span id="mmbrowse">Browse Media</span></a>
		</div>
		<div style="float: left; font-size: 9pt; position: relative;">
			<div style="cursor: default;" onmouseover="javascript:if (ddTimer) clearTimeout(ddTimer); document.getElementById('media-dd').style.display = '';" onmouseout="javascript:ddTimer = setTimeout('document.getElementById(\'media-dd\').style.display=\'none\';', 200);">
				<div style="width: 40px; padding-left: 3px; float: left;"><span id="media-chosen">Videos</span></div> &nbsp;<img src="/images/down.png" alt="" />
				<div id="media-dd" style="display: none;" class="dropdown" onclick="javascript:switchMediaShown(); showMedia('browse', 'new', '');">
					Photos
				</div>
			</div>
		</div>
		<div style="float: left; padding-left: 5px; font-size: 9pt;" id="mediamodulelinks"></div>
		<div style="clear: both;"></div>
		<div id="mediamodule">
			<div style="padding: 20px 0 450px 0; text-align: center;"><img src="/images/wait.gif" style="width: 50px; height: 50px;" alt="" /></div>
		</div>
	</div>
	<?php
}

function showMedia($media, $type)
{
	global $API;

	if ($type == "V")
  {
		$dir = "videos";
    if( $media['title'] == "" ) $media['title'] = quickQuery("select title from videos where id='" . $media['id'] . "'" );
    if( $media['descr'] == "" ) $media['descr'] = quickQuery("select descr from videos where id='" . $media['id'] . "'" );
  }
	else
  {
		$dir = "photos";
    if( $media['ptitle'] == "" ) $media['title'] = quickQuery("select title from albums where id='" . $media['aid'] . "'" );
    if ($media['title'] == 'Profile Photos')
        if( $media['pdescr'] == "" ) $media['descr'] = quickQuery("select name from users inner join albums ON users.uid = albums.uid where id='" . $media['aid'] . "'" );
    else
        if( $media['pdescr'] == "" ) $media['descr'] = quickQuery("select descr from albums where id='" . $media['aid'] . "'" );
  }


	$url = $API->getMediaURL($type, $media['id'], $media['ptitle'] ? $media['ptitle'] : $media['title']);
	?>
	<div class="modmediaresult">
		<div style="float: left;">
			<a href="<?=$url?>"><img src="<?=$API->getThumbURL(1, 100, 75, "/$dir/" . $media['id'] . "/" . $media['hash'] . ".jpg")?>" style="width: 100px; height: 75px;" alt="" /></a>
		</div>
		<div style="float: left; padding-left: 5px; width: 193px;">
			<div style="line-height: 15px; height: 15px; overflow: hidden;">
				<a href="<?=$url?>"><?=$media['ptitle'] ? $media['ptitle'] : $media['title']?></a>
			</div>
			<div style="line-height: 15px; height: 45px; overflow: hidden;">
				<?=$media['pdescr'] ? $media['pdescr'] : $media['descr'] ?>
			</div>
			<div style="clear: left; float: left; color: #777;">
				<!--Views: <?=number_format($media['views'])?> -->
			</div>
			<!--
      <div style="float: right; text-align: right;">
				<a href="<?=$API->getProfileURL($media['uid'], $media['username'])?>"><?=$media['name']?></a>
			</div>
      -->
		</div>
		<div style="clear: both;"></div>
	</div>
	<?php
}

?>