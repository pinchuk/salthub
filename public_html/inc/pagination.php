<div class="pagination">
<?php
/*
This is a tool to place page selections (like "Choose a page 1 2 3 4") on a page.
*/
$query = "";

foreach ($_GET as $k => $v)
	if ($k != "p")
		$query .= "&" . htmlentities("$k=$v");

if ($pages > 0)
{
	if ($page > 0)
		echo "<a href=\"/$script.php?p=" . ($page - 1) . $query . "\">&lt;</a>&nbsp; ";
	else
		echo "&lt;&nbsp; ";

	$i = $page - 3;
	if ($i < 0) $i = 0;
	$j = $i + 7;
	
	for (; $i < $pages && $i < $j; $i++)
		if ($i == $page)
			echo "<b>" . ($i + 1) . "</b>&nbsp; ";
		else
			echo "<a href=\"/$script.php?p=$i$query\">" . ($i + 1) . "</a>&nbsp; ";

	if ($page < $pages - 1)
		echo "<a href=\"/$script.php?p=" . ($page + 1) . "$query\">&gt;</a>&nbsp; ";
	else
		echo "&gt;&nbsp; ";
}
?>
</div>