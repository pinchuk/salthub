<?
/*
A search tool that enables users to find users who have work at a spefied company. Used on the welcome page.
*/

if( empty( $num_results ) )
{
  $num_results = 6;
}

?>

<div style="font-size:9pt; margin-bottom:3px;">
  Enter a company or vessel name below to find past and present colleagues.
</div>

<div style="position:relative;">
  <input type="text" class="txtsearch" id="search" name="kw" style="width: 417px; height: 20px; font-size: 9pt; background-position-y: 3px;" data-autocomplete="get_employer_or_vessel" data-ac_func="get_search" data-ac_click_func="people_where_you_work_click" data-default="Search by company or vessel name">
  <div id="suggestionBoxPWYW" style="padding:0px; position: absolute; left:0px; top:23px; width:417px; height:205px; z-index:101; display:none;"></div>
</div>

<div id="pwywresults">


</div>

<script language="javascript" type="text/javascript">
<!--

var PWYWgid = 0;
var PWYWFirstLoad = true;

function searchClick( e )
{
  if( e.value == "search company or vessel name, IMO #, MMSI #" )
    e.value = "";
}

function searchPWYW( gid )
{
  PWYWgid = gid;
  PWYWFirstLoad = true;

	getAjax("/inc/people_where_you_work_search.php?results=<?=$num_results?>&row=<?=$results_per_row?>&gid=" + gid, searchPWYW_Handler);
}

function searchPWYW_Handler( data )
{
  e = document.getElementById( "pwywresults" );

  if( e )
  {
    e.innerHTML = data;
  }

  if( PWYWFirstLoad )
  {
    e = document.getElementById( "joinForms" );
    if( e )
    {
      e.style.display = 'none';
    }

    setTimeout( slideDownJoinForms, 3000 );
  }
  else
  {
    joinPageChangeTab(1);
  }
}

function shufflePWYW()
{
  PWYWFirstLoad = false;
  if( PWYWgid > 0 )
  {
    getAjax("/inc/people_where_you_work_search.php?results=<?=$num_results?>&row=<?=$results_per_row?>&fl=1&gid=" + PWYWgid, searchPWYW_Handler);
  }
}

function slideDownJoinForms()
{
  joinPageChangeTab(1);

  e = document.getElementById( 'joinForms' );

  if( e )
  {
    slidedown( 'joinForms', null, false );
  }
}

function JoinHandlerPWYW( data )
{
  join = document.getElementById( 'joinForm' );
  joined = document.getElementById( 'joinedForm' );
  if( join ) join.style.display='none';
  if( joined ) joined.style.display='';
}
-->
</script>

<?


?>