<?
/*
Provides the search functionality for the people where you work module (used on the welcome page)
*/

require_once(  $_SERVER["DOCUMENT_ROOT"] . "/inc/inc.php" );

$gid = intval( $_GET['gid'] );
$results_per_row = intval( $_GET['row'] );
$limit = intval( $_GET['results'] );

$page = $API->getPageInfo( $gid );

?>


<div style="border: 1px solid #d8dfea; padding-left: 5px; margin-top: 10px; font-size:9pt; background-color:#e1edf4;">

<?

$results = array();
$q = mysql_query( "select uid from page_members where gid='" . $gid . "' and uid!='" . $API->uid. "' order by RAND() " );
while( $r = mysql_fetch_array( $q ) )
  $results[] = $r['uid'];

 if( sizeof( $results ) > 0 ) { ?>
  <div style="font-size:10pt; font-weight:bold;">Past &amp; Present Colleagues</div>
<?
}
else{ ?>
    <div style="font-size:10pt; font-weight:bold;">This page does not have members</div>
<? }

foreach( $results as $uid )
{
//  if( intval( $uid ) == 0 || $uid == $API->uid )
//    continue;
//  else
//    $c++;


  $user = $API->getUserInfo( $uid, "uid, username, pic, name" );

  if( sizeof( $user ) == 0 )
  {
    $c--;
    continue;
  }

	$profileURL = $API->getProfileURL($user['uid'], $user['username']);
	?>
	<div class="pymk">
		<a href="<?=$profileURL?>">
			<img src="<?=$API->getThumbURL(1, 48, 48, $API->getUserPic($user['uid'], $user['pic']))?>" alt="" />
		</a>
		<div class="nowrap">
			<a href="<?=$profileURL?>" class="userlink"><?=$user['name']?></a><br />
			<?=friendLink($user['uid'])?><br />
			<a href="javascript:void(0);" onclick="javascript:showSendMessage(<?=$user['uid']?>, '<?=addslashes($user['name'])?>', '<?=$API->getThumbURL(0, 85, 128, $API->getUserPic($user['uid'], $user['pic']))?>');">Send message</a><br />
			<span id="hide<?=$user['uid'];?>_p"><a href="javascript:void(0);" onclick="javascript:hideUser(<?=$user['uid']?>, 'hide<?=$user['uid'];?>_p');">Hide</a></span>
		</div>
	</div>
	<?php
  //if( $c >= $limit ) break;
}
?>


	<div style="clear: both; padding: 10px; text-align: center;">
<? if( sizeof( $results ) > $limit ) { ?>
		<a href="javascript:void(0);" onclick="javascript:shufflePWYW();" style="font-size: 9pt; text-decoration: none;">show more &hellip;</a>
<? } ?>
	</div>



<? if( !$page['member'] ) { ?>
  <div id="joinForms" style="<? if( sizeof( $results ) > 0 && empty( $_GET['fl'] ) ) { ?>display:none; overflow:hidden; opacity:0; filter:alpha(opacity = 0);<? } ?> height:180px;">

<? if( sizeof( $results ) > 0 ) { ?>
    <div style="width:420px; margin-left:5px; border-bottom: 1px solid rgb(85,85,85);"></div>
    <div style="padding-bottom:8px;"></div>
<? } ?>


  <div id="joinForm">
  <?
  $joinhandler = "JoinHandlerPWYW";
  include(  $_SERVER["DOCUMENT_ROOT"] . "/pages/join_page_popup.php" );
  ?>
  </div>

  <div id="joinedForm" style="display:none; font-size:9pt; margin-top:5px;">
  <b>Thank you for connecting, visit <a href="<?=$page['url']; ?>"><?=$page['gname']?></a> now!</b>
  </div>

  <div style="padding-bottom:5px;"></div>
  </div>
<? } else { ?>
<?
$type = $page['subcatname'];
if( $page['type'] == PAGE_TYPE_BUSINESS || $type == "" ) $type = $page['pagetype'];
?>
<? if( sizeof( $results ) > 0 ) { ?>
    <div style="width:420px; margin-left:5px; border-bottom: 1px solid #d8dfea;"></div>
    <div style="padding-bottom:8px;"></div>
<? } ?>

  <div style="padding-bottom:3px;">
    Viewing results for the <?=$type?> page <a href="<?=$page['url'];?>" style="font-weight:bold;"><?=$page['gname'];?></a>
  </div>
<? } ?>
  <div style="clear:both;"></div>
</div>
