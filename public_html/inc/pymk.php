<!-- People you may know module -->
<div class="subhead" style="margin-top: 10px;">People You May Know</div>

<div class="connect">
	<div class="desc">
		The results below are provided by comparing your profile data and using the social graph to find matches.&nbsp; For better results, we strongly recommend <a href="<?=$API->getProfileURL()?>/about">completing your profile information</a>.
		<div id="pymk_container0">
			<?php $limit = 8; $_GET['i'] = 0; $_GET['pymk_on_search'] = 1; include $_SERVER['DOCUMENT_ROOT'] . "/getpymk.php"; ?>
		</div>
	</div>
	<div style="clear: both; padding-top: 10px; text-align: right;">
		<a href="javascript:void(0);" onclick="javascript:pymkShowMore(this, 0, 8, true, 'pymk_on_search');" style="font-size: 9pt; text-decoration: none;"><img src="/images/arrow_rotate_anticlockwise.png" alt="" /> Shuffle matches</a>
	</div>
</div>