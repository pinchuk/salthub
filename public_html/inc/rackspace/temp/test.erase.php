<?php
	require_once ("./configuration.php");
	require_once ("./rackspace_classes/cloudfiles.php");
	
	$intMemberID = 1313;
	$strContainerName = "member_uploads_" . $intMemberID;
	$intRemovedCount = 0;

	$objRackspaceAuthentication = new CF_Authentication(RACKSPACE_USERNAME, RACKSPACE_KEY);
	
	$blAuthenticated = $objRackspaceAuthentication ->	authenticate();
	
	$objRackspaceConnection = new CF_Connection ($objRackspaceAuthentication);
			
	$objRackspaceConnection ->	setDebug (false);
	$objRackspaceConnection ->	ssl_use_cabundle(); 
 	$DESTINATION_CONTAINER = 	$objRackspaceConnection -> get_container($strContainerName);
 		
	if (DEBUG_MODE) echo ("Trying Delete For Member ID " . $intMemberID . "<br />");
	
		try {
			if (DEBUG_MODE) echo ("Switching Record Number $intMemberID to Container Name " . $strContainerName . "<br />");
			
			if (DEBUG_MODE) echo ("Got File Container For Record Number" . $intMemberID . "<br />");
			
			try {
				/* cloudfiles needs the object folder and the object id in order to delete it */ 
				if ($DESTINATION_CONTAINER -> delete_object ("downloaded_file_test__oAtD7G.png", $strContainerName)) {
					if (DEBUG_MODE) echo ("Deleted Object<br />");
					$intRemovedCount++;
				}
			} catch (Exception $e) {
				if (DEBUG_MODE) throw new Exception ($e);
			}
		} catch (Exception $e) {
			if (DEBUG_MODE) throw new Exception ($e);
		}

	echo ("Removed File Count = " . $intRemovedCount . "<br />");
?>