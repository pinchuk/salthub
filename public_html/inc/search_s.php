<?php

/*
Change Log

8/24/2011 - Removed the search of tags from the video and photo searches to improve speed.  Old code is commented out.


*/

include "header.php";

$params['q'] = $_GET['q'];
$params['t'] = substr($_GET['t'], 0, 1);

?>

<div class="bigtext2" style="clear: both; padding: 0 0 5px 5px;">
	Results for &quot;<?=$params['q']?>&quot;
</div>

<div class="contentborder">
	<div style="float: left; width: 634px;">
		<?php

		foreach (array("V", "P") as $type)
		{
      continue;

			?>
			<div class="searchhead">
				<a href="/search_m.php?<?=getParams(array("t" => $type))?>"><?=ucwords(typeToWord($type))?>s for &quot;<?=$params['q']?>&quot;</a>
			</div>

				<?php
/*
				if ($type == "V")
					$q = "select id,title,descr,hash from videos as media where " . $API->getPrivacyQuery() . " (title like '%{$params['q']}%' or descr like '%{$params['q']}%' or id IN (select distinct videos.id from videos left join video_tags on video_tags.vid=videos.id left join contacts on contacts.cid=video_tags.cid left join users on IF(video_tags.cid=0,video_tags.uid,contacts.uid) = users.uid where users.name like '%{$params['q']}%'))";
				else
					$q = "select media.id,ifnull(ptitle,title) as title,ifnull(pdescr,descr) as descr,hash from photos as media inner join albums on albums.id=media.aid where " . $API->getPrivacyQuery() . "
          (ptitle like '%{$params['q']}%' OR title like '%{$params['q']}%'
          OR descr like '%{$params['q']}%' OR
          media.id IN (select distinct photos.id from photos
             left join photo_tags on photo_tags.pid=photos.id
             left join contacts on contacts.cid=photo_tags.cid
             left join users on IF(photo_tags.cid=0,photo_tags.uid,contacts.uid) = users.uid
             where users.name like '%{$params['q']}%'))";
  */
				if ($type == "V")
					$q = "select id,title,descr,hash from videos as media where privacy=0 and reported=0 and (title like '%{$params['q']}%' or descr like '%{$params['q']}%')";
				else
					$q = "select media.id,ifnull(ptitle,title) as title,ifnull(pdescr,descr) as descr,hash from photos as media inner join albums on albums.id=media.aid where privacy=0 and reported=0 and
          (ptitle like '%{$params['q']}%' OR title like '%{$params['q']}%' OR descr like '%{$params['q']}%')";
				$q .= " order by media.created desc limit 5";
        //echo $q;

				$x = sql_query($q);

				if (mysql_num_rows($x) == 0)
					echo '<div class="noresults2">No results found.</div>';
				else
				{
					echo '<div class="searchimages">';
					while ($media = mysql_fetch_array($x, MYSQL_ASSOC))
					{
						echo '<div>';
						echo '<a href="' . $API->getMediaURL($type, $media['id'], $media['title']) . '">';
						echo '<img src="' . $API->getThumbURL(1, 100, 75, "/" . typeToWord($type) . "s/{$media['id']}/{$media['hash']}.jpg") . '" alt="" /><br />';
						echo "<span>{$media['title']}</span>";
						echo '</a>';
						echo '</div>';
						echo '</a>';
					}
					echo '<div style="clear: both;"></div></div>';
				}
				?>

			<?php if (mysql_num_rows($x) > 0) { ?><div class="searchmore"><a href="/search_m.php?<?=getParams(array("t" => $type))?>"><img src="/images/add.png" alt="" />show more <?=typeToWord($type)?>s</a></div><?php } ?>
			<div class="searchdiv"></div>
			<?php
		}
		?>

		<div class="searchhead">
			<a href="/search_m.php?<?=getParams(array("t" => "U"))?>">Connections For &quot;<?=$params['q']?>&quot;</a>
		</div>
		<div style="margin-top: -10px;">
			<?php
			$limit = 8;
			$like = $params['q'];
			ob_start();
			include "getpymk.php";
			$html = ob_get_contents();
			ob_end_clean();

			if (mysql_num_rows($x) > 0)
				echo $html;

			echo '<div style="clear: both; height: 5px;"></div>';
			
			if (mysql_num_rows($x) == 0)
				echo '<div class="noresults2" style="margin-top: 3px;">No results found.</div>';
			?>
		</div>

		<?php if (mysql_num_rows($x) > 0) { ?><div class="searchmore"><a href="/search_m.php?<?=getParams(array("t" => "U"))?>"><img src="/images/add.png" alt="" />show more connections</a></div><?php } ?>
		<div class="searchdiv"></div>
		
		<div class="searchhead">
			<a href="/search_m.php?<?=getParams(array("t" => "B"))?>">Vessels for &quot;<?=$params['q']?>&quot;</a>
		</div>
		<?php
		$fields = array("shiptype", "flag", "callsign", "imo", "name", "descr", "gname", "exnames");
		foreach ($fields as $f)
			$wheres[] = "$f like '%{$params['q']}%'";
                                                                                                                                                                                                     //Was gtype='B instead of category=112
		$q = "select gid,gname,g.pid,hash,length,exnames from groups g left join boats_ex ex on ex.id=glink left join photos on photos.id=g.pid inner join boats on boats.id=glink where g.active=1 and g.cat=112 and (" . implode(" or ", $wheres) . ") group by glink order by gname limit 5";
		$x = sql_query($q);
    echo mysql_error();

		if (mysql_num_rows($x) > 0)
		{
		?>
		<div class="searchimages">
			<?php
			while ($group = mysql_fetch_array($x, MYSQL_ASSOC))
			{
				echo '<div>';
				echo '<a href="' . $API->getGroupURL($group['gid'], $group['gname']) . '">';
				echo '<img src="' . $API->getThumbURL(1, 100, 75, "/photos/{$group['pid']}/{$group['hash']}.jpg") . '" alt="" /><br />';
				echo "<span><b>{$group['gname']}</b><br />{$group['length']} ft. / " . round($group['length'] / 3.2808399, 2) . " m.</span>";
        if( $group['exnames'] != '' )
  				echo "<br /><span>(ex. " . $group['exnames'] . ")</span>";
				echo '</a>';
				echo '</div>';
				echo '</a>';
			}

			?>
			<div style="clear: both;"></div>
		</div>
		<?php
		}
		else
			echo '<div class="noresults2">No results found.</div>';
		?>
		
		<?php if (mysql_num_rows($x) > 0) { ?><div class="searchmore"><a href="/search_m.php?<?=getParams(array("t" => "B"))?>"><img src="/images/add.png" alt="" />show more vessels</a></div><?php } ?>
		<div class="searchdiv"></div>

		<div class="searchhead">
			<a href="/search_m.php?<?=getParams(array("t" => "C"))?>">Comments and Log Entries for &quot;<?=$params['q']?>&quot;</a>
		</div>

		<?php
		$x = sql_query(getLogEntrySearchQuery($params['q']) . "limit 4");
		
		if (mysql_num_rows($x) > 0)
			while ($result = mysql_fetch_array($x, MYSQL_ASSOC))
				showLogEntrySearchResult($result, $params['q']);
		else
			echo '<div class="noresults2">No results found.</div>';
		?>
		
		<?php if (mysql_num_rows($x) > 0) { ?><div class="searchmore"><a href="/search_m.php?<?=getParams(array("t" => "C"))?>"><img src="/images/add.png" alt="" />show more comments and log entries</a></div><?php } ?>
		<div class="searchdiv"></div>

		<div class="searchhead">
			<a href="/search_m.php?<?=getParams(array("t" => "c"))?>">Companies for &quot;<?=$params['q']?>&quot;</a>
		</div>
		<?php                                                                                                                                     // gtype = 'C' // used to be this, changed to category=110
		$q = "select gid,gname,g.pid,hash from groups g left join photos on photos.id=g.pid left join categories on categories.cat=g.subcat where g.active=1 and g.privacy=" . PRIVACY_EVERYONE . " and g.cat=111 and (gname like '%{$params['q']}%' or products like '%{$params['q']}%' or contact_person like '%{$params['q']}%' or catname like '%{$params['q']}%') order by gname limit 5";
		$x = sql_query($q);
    echo mysql_error();

		if (mysql_num_rows($x) > 0)
		{
		?>
		<div class="searchimages">
			<?php
			while ($group = mysql_fetch_array($x, MYSQL_ASSOC))
			{
				echo '<div>';
				echo '<a href="' . $API->getGroupURL($group['gid'], $group['gname']) . '">';
				echo '<img src="' . $API->getThumbURL(1, 100, 75, $API->getGroupImage($group['gid'], $group['pid'] ) ) . '" alt="" /><br />';
				echo "<span><b>{$group['gname']}</b></span>";
				echo '</a>';
				echo '</div>';
				echo '</a>';
			}

			?>
			<div style="clear: both;"></div>
		</div>
		<?php
		}
		else
			echo '<div class="noresults2">No results found.</div>';
		?>

		<?php if (mysql_num_rows($x) > 0) { ?><div class="searchmore"><a href="/search_m.php?<?=getParams(array("t" => "c"))?>"><img src="/images/add.png" alt="" />show more work groups</a></div><?php } ?>
		<div class="searchdiv"></div>

		<div class="searchhead">
			<a href="/search_m.php?<?=getParams(array("t" => "g"))?>">Groups for &quot;<?=$params['q']?>&quot;</a>
		</div>
		<?php                                                                                                                                     // gtype = 'C' // used to be this, changed to category=110
		$q = "select gid,gname,g.pid,hash from groups g left join photos on photos.id=g.pid where g.active=1 and g.privacy=" . PRIVACY_EVERYONE . " and g.cat!=111 and g.cat!=112 and (gname like '%{$params['q']}%' or descr like '%{$params['q']}%') order by gname limit 5";
		$x = sql_query($q);

		if (mysql_num_rows($x) > 0)
		{
		?>
		<div class="searchimages">
			<?php
			while ($group = mysql_fetch_array($x, MYSQL_ASSOC))
			{
				echo '<div>';
				echo '<a href="' . $API->getGroupURL($group['gid'], $group['gname']) . '">';
				echo '<img src="' . $API->getThumbURL(1, 100, 75, $API->getGroupImage($group['gid'], $group['pid'] ) ) . '" alt="" /><br />';
				echo "<span><b>{$group['gname']}</b></span>";
				echo '</a>';
				echo '</div>';
				echo '</a>';
			}

			?>
			<div style="clear: both;"></div>
		</div>
		<?php
		}
		else
			echo '<div class="noresults2">No results found.</div>';
		?>

		<?php if (mysql_num_rows($x) > 0) { ?><div class="searchmore"><a href="/search_m.php?<?=getParams(array("t" => "g"))?>"><img src="/images/add.png" alt="" />show more groups</a></div><?php } ?>
		<div class="searchdiv"></div>

	</div>



	<div style="float: right; width: 300px;">
		<?php showCompleteProfileInformationWide() ?>
<? if( $API->adv ) { ?>
		<div class="subhead" style="margin-top: 10px;">Sponsors</div>
		<div style="margin: 5px 0 10px;"><?php showAd("companion"); ?></div>
<? } ?>
		<?php include "inc/connect.php"; ?>
<? if( $API->adv ) { ?>
		<div class="subhead">Sponsors</div>
		<div style="margin: 5px 0 0;"><?php showAd("companion"); ?></div>
<? } ?>
		<?php include "inc/pymk.php"; ?>
	</div>
	<div style="clear: both;"></div>
</div>

<?php

function getParams($change)
{
	global $params;
	
	$params2 = $params;
	
	foreach ($change as $k => $v)
		$params2[$k] = $v;
	
	foreach ($params2 as $k => $v)
		$res .= "&$k=$v";
	
	return substr($res, 1);
}

include "footer.php";

?>