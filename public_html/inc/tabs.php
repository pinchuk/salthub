<?php
class Tabs
{
	function __construct($width = 950)
	{
		?>
		<div style="position: relative; height: 23px; width: <?=$width?>px;">
			<div style="position: absolute; top: 0; left: 0; height: 23px; border-bottom: 1px solid #D8DFEA; width: <?=$width?>px;"></div>
			<div style="position: absolute; top: 0; left: 10px;">
		<?php
	}
	
	function add($caption, $link = "", $active = false)
	{
		?>
		<div class="htabs <?=$active ? "htabfront" : "htabback"?>"><a href="<?=$link?>"><?=$caption?></a></div>
		<?php
	}
	
	function finish()
	{
		?>
			</div>
		</div>
		<?php
	}
}
?>