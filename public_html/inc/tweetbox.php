	<div style="padding-top: 10px; background: #EDEFF4; width: 441px;">
		<div style="padding: 0 5px;">
			<div class="action" style="background-image: url(/images/add.png); font-weight: bold; margin-bottom: 10px; cursor: default;" id="tbtitle"></div>
			<div style="float: right; font-size: 8pt; color: #777; display: none; padding: 2px 2px 0 0;" id="wpcharsremain">XX characters remaining</div>
			<div id="tbattach" style="clear: both;">
				<div class="action" style="background-image: url(/images/comments.png);" onclick="javascript:resetUpload(true);">Comment</div>
				<div class="action" style="background-image: url(/images/television_add.png);" onclick="javascript:showUpload(new Array('vidadd','vidchoices','mediaselection'));">Videos</div>
				<div class="action" style="background-image: url(/images/image_add.png);" onclick="javascript:showUpload(new Array('phadd','phchoices','mediaselection'));">Photos</div>
			</div>
		</div>
		<div class="tbstatus" id="tbstatus">
			<div class="logbubble" style="margin: 0;">&nbsp;</div>
			<div class="txtstatus test2">
				<textarea id="txtstatus" onkeyup="javascript:txtStatusChanged(this,event);" onchange="javascript:txtStatusChanged(this,event);" onkeypress="javascript:return txtStatusChanged(this,event);" onfocus="javascript:txtStatusFocused(this, true);" onblur="javascript:txtStatusFocused(this, false);" onclick="document.getElementById('mediaselection').style.display='';"></textarea>
			</div>
		</div>
		<div style="float: right; padding: 16px 3px 0 0;" id="btnprocess">
			<input type="button" class="button" value="&nbsp;Process&nbsp;" onclick="javascript:wallPost();" />
		</div>

		<div style="clear: both;"></div>
		<div class="logbubble" id="logbubble" style="display: none; margin-top: 5px;">&nbsp;</div>
		<div class="uploadcontent" id="phadd" style="display: none;">
			<img src="/images/bluex.png" style="float: right; cursor: pointer;" alt="" onclick="javascript:resetUpload(true);" />
			<div class="smtitle">Add Photos</div>
			<div style="margin-top: 7px; border-top: 2px solid #D8DFEA;"></div>
			<div id="phchoices" style="display: none;">
				<div class="addchoice" style="background-image: url(/images/computer.png);" onclick="javascript:showUpload(new Array('phadd','phcomputer','mediaselection'));">From my computer</div>
				<div style="float: left; width: 4%;"><div class="addsep"></div></div>
				<div class="addchoice">&nbsp;</div>
				<div style="float: left; width: 4.5%;"><div class="addsep"></div></div>
				<div class="addchoice" style="background-image: url(/images/phone.png);" onclick="javascript:showPhonePopup();">From my phone</div>
				<div style="clear: both;"></div>
			</div>
			<div id="phcomputer" style="display: none; padding-top: 10px;">
				<table border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td width="150" style="text-align: center;">
							<div style="font-size: 9pt; font-weight: bold; visibility: hidden; margin-bottom: 2px;" id="phmainimagetitle">
								Main Image
							</div>
							<img src="/images/albumempty.png" id="mainimage-0" style="border: 2px solid #A8BDC6;" alt="" />
						</td>
						<td width="150" style="text-align: center;">
							<div style="font-size: 9pt; font-weight: bold; visibility: hidden; margin-bottom: 2px;" id="mainimagetitlepl">
								&nbsp;
							</div>
							<div id="phbrowse" style="overflow-y: hidden;">
								<div>
									<span id="placeholderPhotos"></span>
								</div>
								<div style="font-size: 7pt; margin-top: 5px;">
									Hold down the &quot;Ctrl&quot; key and select multiple images for easy uploading.
								</div>
							</div>
							<div id="phprogressbarcontainer" style="display: none;">
								<div class="progressbaroff" style="width: 175px;">
									<div id="phprogressbar" class="progressbaron">&nbsp;</div>
								</div>
								<div style="font-size: 8pt;" id="divuploadstatusph">
									Uploading 0 of 0 (0%)
								</div>
							</div>
						</td>
					</tr>
				</table>
				<div id="phuploadedcontainer" style="margin-top: 5px; display: none; color: #555; height: 57px;">
					<div style="font-weight: bold; margin-bottom: 2px;">
						Manage Your Images
					</div>
					<div style="font-size: 8pt;">
						Set your main image by selecting a photo, or delete an image by placing your cursor on it and selecting the &quot;X&quot;.
					</div>
					<div id="phuploaded" style="margin-top: 5px; width:300px;">
					</div>
          <div style="clear: both;"></div>
				</div>
				<div style="clear: both;"></div>
				<div style="color: #808080; font-size: 8pt; line-height: 11pt;" id="phdisclaimer">
					<ul>
						<li style="list-style-type: disc;">Upload your image.&nbsp; The main image will identify your album, or you can upload only one image if you like.&nbsp; You can upload GIF, JPEG, and PNG images.</li>
						<li style="list-style-type: disc;">You can upload up to 20 pictures at a time.</li>
						<li style="list-style-type: disc;">Maximum file size: 7 MB.</li>
						<li style="list-style-type: disc;">Do not upload copyrighted material which you do not have permission to share.</li>
						<li style="list-style-type: disc;">Do not ruin the experience of others with obscene media or any media which violates the <a onclick="javascript:showTOSPopUp();" href="javascript:void(0);">Terms of Use</a>.</li>
					</ul>
				</div>
				<div id="phinfo" style="display: none;">
					<div style="border-top: 2px solid #edeff4; padding-top: 10px; margin-top: 10px; text-align: center;" class="smtitle">
						Complete the following while your photos upload
					</div>
					<form name="phform" action="" onsubmit="return false;">
					<table border="0" cellpadding="7" style="margin-bottom: 15px;">
						<tr valign="top">
							<td style="text-align: right; color: #555;" class="smtitle">Title:</td>
							<td><input style="color: #555; font-size: 9pt; width: 320px; border: 1px solid #555;" name="phtitle" /></td>
						</tr>
						<tr valign="top" id="phdesc_container">
							<td style="text-align: right; color: #555;" class="smtitle">Description:</td>
							<td>
								<textarea onfocus="javascript:showCharsLeft(0, true);" onblur="javascript:showCharsLeft(0, false);" onkeyup="javascript:charsLeft(0, this.value);" class="uploaddesc" name="phdesc" rows="0" cols="0"></textarea>
								<div id="chars-0" style="visibility: hidden;" class="charsleft">XX characters remaining</div>
							</td>
						</tr>
						<tr valign="top" id="phcategory">
							<td style="text-align: right; color: #555;" class="smtitle">Category:</td>
							<td class="smtitle2">
								<a href="javascript:void(0);" onclick="javascript:showCategorySelect();"><img src="/images/arrow_down.png" alt="" style="vertical-align: middle;" />&nbsp; <span id="phcategoryselected"></span></a>
							</td>
						</tr>
					</table>
					</form>
					<div style="text-align: center; display: none;" id="phwait">
						<img src="/images/wait.gif" alt="" />
					</div>
					<div style="text-align: center; display: none;" id="phcontinue">
						<input type="button" class="button" value="&nbsp;&nbsp;Next &gt;&gt;&nbsp;&nbsp;" onclick="javascript:savePh();" />
					</div>
				</div>
			</div>
		</div>
		<div class="uploadcontent" id="vidadd" style="display: none;">
			<img src="/images/bluex.png" style="float: right; cursor: pointer;" alt="" onclick="javascript:resetUpload(true);" />
			<div class="smtitle">Add Videos</div>
			<div style="margin-top: 7px; border-top: 2px solid #D8DFEA;"></div>
			<div id="vidchoices" style="display: none;">
				<div class="addchoice" style="background-image: url(/images/computer.png);" onclick="javascript:showUpload(new Array('vidadd','vidcomputer','mediaselection'));">From my computer</div>
				<div style="float: left; width: 4%;"><div class="addsep"></div></div>
				<div class="addchoice" style="background-image: url(/images/youtube.png);" onclick="javascript:showUpload(new Array('vidadd','vidyt','mediaselection'));">From &nbsp; YouTube</div>
				<div style="float: left; width: 4.5%;"><div class="addsep"></div></div>
				<div class="addchoice" style="background-image: url(/images/phone.png);" onclick="javascript:showPhonePopup();">From my phone</div>
				<div style="clear: both;"></div>
			</div>
			<div id="vidyt" style="display: none; padding-top: 10px;">
				<div id="vidytthumb" style="margin-top: 10px; display: none; text-align: center;"></div>
				<div style="color: #808080; font-size: 8pt; line-height: 11pt;" id="viddisclaimer">
					<ul>
						<li style="list-style-type: disc;">Do not upload copyrighted material which you do not have permission to share.</li>
						<li style="list-style-type: disc;">Do not ruin the experience of others with obscene media or any media which violates the <a onclick="javascript:showTOSPopUp();" href="javascript:void(0);">Terms of Use</a>.</li>
					</ul>
				</div>
				<div id="vidyterror" style="display: none;" class="uploaderror"></div>
				<form name="vidytform" action="" onsubmit="return false;">
				<div id="vidytaddress">
					<div style="border-top: 2px solid #edeff4; padding: 10px 0 10px 0; margin-top: 10px; text-align: center;" class="smtitle">
						Enter the website address of the YouTube video below
					</div>
					<table border="0" cellpadding="7" style="margin-bottom: 61px;">
						<tr valign="top">
							<td style="text-align: right; color: #555;" class="smtitle">Address:</td>
							<td><input style="color: #555; font-size: 9pt; width: 320px; border: 1px solid #555;" name="vidyturl" /></td>
						</tr>
					</table>
				</div>
				<table border="0" cellpadding="7" style="display: none;" id="vidytinput">
					<tr valign="top">
						<td style="text-align: right; color: #555;" class="smtitle">Title:</td>
						<td><input style="color: #555; font-size: 9pt; width: 320px; border: 1px solid #555;" name="vidyttitle" /></td>
					</tr>
					<tr valign="top" id="vidytdesc_container">
						<td style="text-align: right; color: #555;" class="smtitle">Description:</td>
						<td>
							<textarea onfocus="javascript:showCharsLeft(1, true);" onblur="javascript:showCharsLeft(1, false);" onkeyup="javascript:charsLeft(1, this.value);" class="uploaddesc" name="vidytdesc" rows="0" cols="0"></textarea>
							<div id="chars-1" style="visibility: hidden;" class="charsleft">XX characters remaining</div>
						</td>
					</tr>
					<tr valign="top" id="vidytcategory">
						<td style="text-align: right; color: #555;" class="smtitle">Category:</td>
						<td class="smtitle2">
							<a href="javascript:void(0);" onclick="javascript:showCategorySelect();"><img src="/images/arrow_down.png" alt="" style="vertical-align: middle;" />&nbsp; <span id="vidytcategoryselected"></span></a>
						</td>
					</tr>
				</table>
				</form>
				<div style="text-align: center; height: 50px;">
					<input type="button" id="btnsaveyt" class="button" value="&nbsp;&nbsp;Next &gt;&gt;&nbsp;&nbsp;" onclick="javascript:saveYTVid();" />
					<img src="/images/wait.gif" alt="" id="vidytwait" style="display: none;" />
				</div>
			</div>
			<div id="vidcomputer" style="display: none; padding-top: 10px;">
				<table border="0" cellpadding="0" cellspacing="0" align="center" id="vidbrowse">
					<tr>
						<td>
							<div style="width: 230px; height: 20px; line-height: 20px; border: 1px solid #7F9DB9; padding-left: 5px; overflow: hidden;" id="vidfile_file"></div>
						</td>
						<td>
							<div style="padding-left: 20px;" id="placeholderVidParent">
								<span id="placeholderVid"></span>
							</div>
						</td>
					</tr>
				</table>
				<table border="0" cellpadding="0" cellspacing="0" align="center">
					<tr>
						<td colspan="2">
							<div id="vidfile" style="padding-top: 10px;">
								<div class="progressbaroff" id="vidprogressbarcontainer" style="display: none;">
									<div id="vidprogressbar" class="progressbaron">&nbsp;</div>
								</div>
							</div>
							<div style="border: 2px solid #00c000; padding: 5px; text-align: center; display: none; font-weight: bold; color #555; width: 230px;" id="vidsuccess">
								Upload complete
							</div>
						</td>
					</tr>
				</table>
				<div id="vidthumb" style="margin-top: 10px; display: none; text-align: center;"></div>
				<div style="color: #808080; font-size: 8pt; line-height: 11pt;">
					<ul>
						<li style="list-style-type: disc;">Maximum file size: 100 MB</li>
						<li style="list-style-type: disc;">Do not upload copyrighted material which you do not have permission to share.</li>
						<li style="list-style-type: disc;">Do not ruin the experience of others with obscene media or any media which violates the <a onclick="javascript:showTOSPopUp();" href="javascript:void(0);">Terms of Use</a>.</li>
					</ul>
				</div>
				<div id="viderror" style="display: none;" class="uploaderror"></div>
				<div id="vidinfo" style="display: none;">
					<div style="border-top: 2px solid #edeff4; padding-top: 10px; margin-top: 10px; text-align: center;" class="smtitle">
						Complete the following while your video uploads
					</div>
					<form name="vidform" action="" onsubmit="return false;">
					<table border="0" cellpadding="7" style="margin-bottom: 15px;">
						<tr valign="top">
							<td style="text-align: right; color: #555;" class="smtitle">Title:</td>
							<td><input style="color: #555; font-size: 9pt; width: 320px; border: 1px solid #555;" name="vidtitle" /></td>
						</tr>
						<tr valign="top" id="viddesc_container">
							<td style="text-align: right; color: #555;" class="smtitle">Description:</td>
							<td>
								<textarea onfocus="javascript:showCharsLeft(2, true);" onblur="javascript:showCharsLeft(2, false);" onkeyup="javascript:charsLeft(2, this.value);" class="uploaddesc" name="viddesc" rows="0" cols="0"></textarea>
								<div id="chars-2" style="visibility: hidden;" class="charsleft">XX characters remaining</div>
							</td>
						</tr>
						<tr valign="top" id="vidcategory">
							<td style="text-align: right; color: #555;" class="smtitle">Category:</td>
							<td class="smtitle2">
								<a href="javascript:void(0);" onclick="javascript:showCategorySelect();"><img src="/images/arrow_down.png" alt="" style="vertical-align: middle;" />&nbsp; <span id="vidcategoryselected"></span></a>
							</td>
						</tr>
					</table>
					</form>
					<div style="text-align: center; display: none;" id="vidwait">
						<img src="/images/wait.gif" alt="" />
					</div>
					<div style="text-align: center; display: none;" id="vidcontinue">
						<input type="button" class="button" value="&nbsp;&nbsp;Next &gt;&gt;&nbsp;&nbsp;" onclick="javascript:saveVid();" />
					</div>
				</div>
			</div>
		</div>
		<div id="upload-category" style="display: none; background: #fff; border-bottom: 2px solid #d8defa; padding: 10px;"></div>

		<div style="width:100%; clear:both; padding-top:3px;" id="mediaselection">
	    <div style="float:left; margin-right:3px;"><input type="checkbox" id="posttw" value="1" CHECKED/></div>
 	    <div style="float:left; margin-top:1px;"><img src="/images/twitter.png" width="16" height="16" alt="" /></div>
      <div style="float:left; width:1px; height:17px; border-right:1px dotted; margin-left:5px; margin-right:5px;"></div>
	    <div style="float:left;"><input type="checkbox" id="postfb" value="1" CHECKED/></div>
 	    <div style="float:left; margin-top:1px; margin-right:10px;"><img src="/images/facebook.png" width="16" height="16" alt="" /></div>
      <div id="postas" style="float:left; font-size:10px; margin:0px; margin-top:3px; padding:0px;"></div>
      <div style="clear:both;"></div>
		</div>
	</div>
