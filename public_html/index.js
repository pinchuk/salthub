var mouseOverBanner = [false, false];
var currentBanner = [0, 0];
var currentSlide = 0;

var curDD = null;
var curDDTimer = null;

function entryHeadDrop(e, over)
{
	div = $(e).siblings().filter('div')[0];
	
	if (over) //mouseover
	{
		if (curDD != null)
			if (curDD != div)
			{
				stopDDRaiseTimer();
				entryHeadRaise();
			}
		stopDDRaiseTimer();
		$(div).fadeIn();
	}
	else //mouseout
	{
		curDD = div;
	}
}

function ddItemClicked(div)
{
	location = $(div).children().filter('a')[0];
}

function stopDDRaiseTimer()
{
	clearTimeout(curDDTimer);
}

function startDDRaiseTimer()
{
	curDDTimer = setTimeout("entryHeadRaise()", 500);
}

function entryHeadRaise()
{
	$(curDD).fadeOut();
}

function submitenter(field ,e)
{
var keycode;
if (window.event) keycode = window.event.keyCode;
else if (e) keycode = e.which;
else return true;

if (keycode == 13)
   {
   field.form.submit();
   return false;
   }
else
   return true;
}

function bannerOver(n, id)
{
	if (mouseOverBanner[n])
		return false;
	
	if (currentBanner[n] == id)
		return true;
		
	diff = currentBanner[n] - id;
	direction = diff < 0;
	diff = Math.abs(diff);
	
	for (var i in banners[n])
	{
		if (n == 1)
			w = 520;
		else if (n == 0)
			w = 470;

		//$('#banner' + n + '-' + i).stop();
		$('#banner' + n + '-' + i).animate({left: (direction ? '-' : '+') + '=' + (w * diff) + 'px'}, w * diff);
		
		s = document.getElementById("banner" + n + "-sq-" + i);
		if (i == id)
			s.src = "/images/square2.png";
		else
			s.src = "/images/square.png";
	}
	
	mouseOverBanner[n] = true;
	
	currentBanner[n] = id;
	
	return true;
}

function xbannerOver(id)
{
	if (mouseOverBanner)
		return false;
	
	for (var i in banners)
	{
		e = document.getElementById("banner-" + i);
		s = document.getElementById("banner-sq-" + i);
		
		if (i == id)
		{
			e.style.display = "";
			s.src = "/images/square2.png";
		}
		else
		{
			e.style.display = "none";
			s.src = "/images/square.png";
		}
	}
	
	mouseOverBanner = true;
	
	return true;
}

function bannerOut(n)
{
	mouseOverBanner[n] = false;
}

function nextBanner(n)
{
	getNext = false;
	
	for (var i in banners[n])
	{
		if (i == currentBanner[n])
			getNext = true;
		else if (getNext)
		{
			getNext = false;
			break;
		}
	}
	
	if (getNext) //start from beginning
	{
		for (var i in banners[n])
			break;
	}
	
	if (bannerOver(n, i))
	{
		currentBanner[n] = i;
		bannerOut(n);
	}
}

var currentTab = 0;

function switchTab(t)
{
	document.getElementById("tabcontent0A").style.display = "none";
	
	currentTab = t;
	
	for (i = 0; i <= 2; i++)
	{
		e = document.getElementById("tabcontent" + i);
		
		if (t == i)
		{
			cls = "on";
			e.style.display = "inline";
		}
		else
		{
			cls = "off";
			e.style.display = "none";
		}
		
		e = document.getElementById("tab" + i);
		if (e)
		{
			e.setAttribute("class", "tab tab" + cls); 
			e.setAttribute("className", "tab tab" + cls); 
		}
	}
}

function getStarted()
{
	if (isLoggedIn)
	{
		switchTab(0);
		document.getElementById("tabcontent0A").style.display = "inline";
		document.getElementById("tabcontent0").style.display = "none";
	}
	else
	{
		//document.getElementById("loginoverlay").style.display = "inline";
		showLogin();
	}
}

	var cycle = [6,6];
	
	var queuedMedia = new Array();
	queuedMedia[0] = new Array();
	queuedMedia[1] = new Array();
	
	var processIntervalId = -1;
	
	function processMediaQueue()
	{
		updated = false;
		
		for (t = 0; t < 2; t++)
		{
			if (queuedMedia[t].length == 0)
			{
				//nothing to do
				continue;
			}
			
			updated = true;
			
			if (t == 0)
				m = "V";
			else
				m = "P";
			
			if (cycle[t] == 6)
				replace = 0;
			else
				replace = cycle[t];
			
			currentMedia[t][replace] = queuedMedia[t][0];
			
			html  = '<a href="/' + (m == "V" ? "video" : "photo") + '/' + queuedMedia[t][0][1] + '-' + queuedMedia[t][0][6] + '"><img src="' + queuedMedia[t][0][2] + '" width="80" height="55" alt="" border="0" /></a><br />';
			html += '<a href="' + queuedMedia[t][0][3] + '" class="author">' + queuedMedia[t][0][4] + '</a><br />';
			html += '<span id="timeago-' + queuedMedia[t][0][5] + '-' + Math.floor(Math.random()*5000) + '"></span>';
									
			document.getElementById("latest" + m + replace).innerHTML = html;
			queuedMedia[t].shift();
			
			doPrettyDate();
			
			cycleMedia(m);
		}
		
		if (!updated)
		{
			clearInterval(processIntervalId);
			processIntervalId = -1;
		}
	}
	
	function getNewestMediaHandler(data)
	{
		y = data.split("\n");
		lastTime = y[0];
		
		for (i = 1; i < y.length; i++)
		{
			m = y[i].split(String.fromCharCode(2));
			
			if (m[0] == "V")
				t = 0;
			else
				t = 1;
			
			queuedMedia[t][queuedMedia[t].length] = m;
		}
		
		if (y.length > 1 && processIntervalId == -1)
			processIntervalId = setInterval("processMediaQueue()", 5000);
		else
			doPrettyDate();
	}
	
	function getNewestMedia()
	{
		getAjax("/getnewestmedia.php?t=" + lastTime, "getNewestMediaHandler");
	}
	
	function sendToEnd(t)
	{
		if (t == "V")
			c = 0;
		else
			c = 1;
		
		e = document.getElementById('latest' + t + cycle[c]);
		e.style.left = -460 + ((5-cycle[c]) * 92) + "px";
	}
	
	function cycleMedia(t)
	{
		if (t == "V")
			c = 0;
		else
			c = 1;
		
		if (cycle[c] < 6)
			sendToEnd(t);
		
		if (--cycle[c] == -1)
			cycle[c] = 5;
		
		for (i = 0; i < 6; i++)
		{
			e = document.getElementById('latest' + t + i);
			l = parseInt(e.style.left.substring(0, e.style.left.length-2));
			
			t1 = new Tween(e.style,'left',Tween.regularEaseOut,l,l+92,1,'px');
			t1.start();
			
			//non-animated
			//e.style.left = (l - 92) + "px";
			//if (i == 5) sendToEnd(t);
		}
	}

function entrySlideTo(id)
{
	numSlides = 5;
	
	if (currentSlide == id)
		return true;
		
	diff = currentSlide - id;
	direction = diff < 0;
	diff = Math.abs(diff);
	
	for (i = 0; i < numSlides; i++)
	{                                                               //1053
		$('#slide' + i).animate({left: (direction ? '-' : '+') + '=' + (1070 * diff) + 'px'}, 500 * diff);
	}
	
	currentSlide = id;
	
	return true;
}