<?php
/*
For SaltHub, this simply forwards to the /welcome page (for users who are logged in) or the /start page
(for users who are not logged in)
*/

include "inc/inc.php";
if ($site == "s")
{
    if( $isDevServer )
        $url = 'http://dev.' . $siteName . '.com';
    else
        $url = 'http://www.' . $siteName . '.com';


    if ($API->isLoggedIn())
	{
		$active = quickQuery("select verify from users where uid=" . $API->uid);
		if ($active == 0)
			header("Location: /signup/verify.php?a=1");
		else
        {
          foreach ($_GET as $k => $v)
          {
            if ($query_string == "" )
              $query_string = "?a";
                $query_string .= "&" . htmlentities("$k=$v");
          }

                header("Location: /welcome" . $query_string );
        }
	}
	else{
		header("Location: /start" . (isset($_GET['ref']) ? "?ref=" . intval($_GET['ref']) : "") . (isset($_GET['cid']) ? "&cid=" . intval($_GET['cid']) : "") . (isset($_GET['joingid']) ? "?joingid=" . intval($_GET['joingid']) . "&uid=" . intval($_GET['uid']) : ""));
    }
	die();
}

$scripts[] = "/prettydate.js";
$scripts[] = "/index.js";

unset($_SESSION['lastMediaViewed']);

if (!empty($_GET['ref']))
	$_SESSION['ref'] = intval($_GET['ref']);

include "header.php";

$banners = array();
$x = sql_query("select * from banners order by id");
while ($y = mysql_fetch_array($x, MYSQL_ASSOC))
{
	if (file_exists("images/banners/" . $y['id'] . ".png"))
		$banners[$y['id']] = $y['link'];
}

?>

<div class="tabcontent" style="display: inline;<?=isset($_GET['upload']) ? " visibility: hidden;" : ""?>;" id="tabcontent0">
	<?php
	$i = 0;
	foreach ($banners as $id => $link)
	{
		?>
		<div id="banner-<?=$i?>" style="text-align: center; padding: 5px 0 10px 0; height: 192px;<?=$i == 0 ? "" : " display: none;"?>">
			<a href="<?=$link?>"><img src="/images/banners/<?=$id?>.png" alt="" /></a>
		</div>
		<?php
		$i++;
	}
	?>
	
	<div style="text-align: center; padding: 0 0 5px 10px;">
		<?php
		$i = 0;
		foreach ($banners as $id => $link)
		{
		?><img id="banner-sq-<?=$i?>" src="/images/square<?=$i == 0 ? "2" : ""?>.png" onmouseout="javascript:bannerOut();" onmouseover="javascript:bannerOver(<?=$i?>);" style="padding-right: 10px;" alt="" /><?php
		$i++;
		}
		?>
	</div>
	
	<div style="border: solid 1px #fff; padding: 5px; position: relative;" id="marqueegrandparent">
		<?php
		for ($j = 0; $j < 2; $j++)
		{
			if ($j == 0)
				$type = "videos";
			else
				$type = "photos";
		?>
		<div class="smtitle" style="padding-bottom: 2px;">Latest <?=$type?> &#0133;</div>
		<div class="marqueecontainer">
			<div class="marqueeparent">
				<?php
				unset($results);
				$x = sql_query("select @type:='" . ($j == 0 ? "V" : "P") . "',media.id,media.created,media.uid,name,username,title,hash,descr from $type as media inner join users on users.uid=media.uid " . ($j == 0 ? "" : "inner join albums on media.aid=albums.id") . " where" . $API->getPrivacyQuery(null, true, "", ($j == 0 ? false : true)) . " active=1 " . ($j == 0 ? "and ready=1" : "") . " order by created desc limit 5");
				while ($y = mysql_fetch_array($x, MYSQL_ASSOC))
					$results[] = $y;
					
				for ($i = 0; $i < 6, $i <= count($results); $i++)
				{
					if ($i > 0)
						$media = $results[$i - 1];
					else
						$media = $results[0]; // dummy result, will be hidden and a "buffer" for the first new result
					
					$currentMedia[] = array($j == 0 ? "V" : "P", $media['id'], $API->getThumbURL(1, 80, 55, "/$type/" . $media['id'] . "/" . $media['hash'] . ".jpg"), $API->getProfileURL($media['uid'], $media['username']), addslashes($media['name']), strtotime($media['created']), str_replace("\r", " ", str_replace("\n", " ", addslashes($media['title']))), str_replace("\r", " ", /*str_replace("\n", " ", */addslashes($media['descr'])));
				?>
				<div class="marquee" onmouseover="javascript:showTip(1, this);" onmouseout="javascript:tipMouseOut();" style="position: relative; left: 0px; top: 0px;" id="latest<?=strtoupper(substr($type, 0, 1))?><?=$i?>">
					<a href="<?=$API->getMediaURL(strtoupper(substr($type, 0, 1)), $media['id'], $media['title'])?>"><img src="<?=$API->getThumbURL(1, 80, 55, "/" . $type . "/" . $media['id'] . "/" . $media['hash'] . ".jpg")?>" width="80" height="55" alt="" /></a><br />
					<div style="overflow: hidden; height: 14px; white-space: nowrap;"><a href="<?=$API->getProfileURL($media['uid'], $media['username'])?>"><?=$media['name']?></a></div>
					<span id="timeago-<?=strtotime($media['created'])?>-<?=md5(microtime())?>"></span>
				</div>
				<?php
				}
				?>
				<div style="clear: both;"></div>
			</div>
		</div>
		<?php
		}
		?>
	</div>
	<div class="getstarted bigtext" onclick="javascript:getStarted();">
		Get Started!
	</div>
</div>

<?php

include "howitworks.php";
include "whyitworks.php";

?>

<div class="tabcontent" id="tabcontent0A" style="padding-top: 12px;">
	<div id="prop" style="float: left; width: 8px; height: 467px;"></div>
	<div style="float: left;">
		<script language="javascript" type="text/javascript">
		<!--
		tbPlace();
		//-->
		</script>
	</div>
	<div style="clear: both; height: 1px; overflow: hidden;"></div>
</div>

<div style="float: left; border-top: 1px solid #ccc; margin-left: -1px;">
	<div id="tab0" style="background-image: url(/images/camera.png);" class="tab tabon" onclick="javascript:switchTab(0);">
		You, Me, &amp;<br/>Everybody!
	</div>
	<div id="tab1" style="background-image: url(/images/gear.png);" class="tab taboff" onclick="javascript:switchTab(1);">
		How it<br/>Works
	</div>
	<div id="tab2" style="background-image: url(/images/accept.png);" class="tab taboff" onclick="javascript:switchTab(2);">
		Why Should<br/>You Join?
	</div>
</div>

<div class="whoshere">
	<div class="attention" style="padding-bottom: 10px;">See who's here</div>
	<?php if ($API->isLoggedIn()) { ?><a href="<?=$API->getProfileURL()?>"><img src="<?=$API->getThumbURL(1, 24, 24, $API->getUserPic())?>" alt="" /></a>&nbsp;<span class="smtitle">You!</span><br /><?php } ?>
	<?php
	$i = 0;
	$x = sql_query("select pic,uid,username,name,twusername,fbid from users where active=1" . ($API->isLoggedIn() ? " and uid != " . $API->uid : "") . " order by lastlogin desc limit 20"); //and lastaction > adddate(now(), interval -30 minute) 
	while ($user = mysql_fetch_array($x, MYSQL_ASSOC))
	{
		$profileURL = $API->getProfileURL($user['uid'], $user['username']);
		$profilePic = $API->getUserPic($user['uid'], $user['pic']);
		$parts = array(rawurlencode($profileURL), rawurlencode($profilePic), rawurlencode($user['twusername']), rawurlencode($user['fbid']), rawurlencode($user['name']));
		$id = implode("-", $parts);
		?>
		<a id="<?=$i?>-<?=$id?>" onmouseover="javascript:showTip(0, this);" onmouseout="javascript:tipMouseOut();" href="<?=$profileURL?>"><img src="<?=$API->getThumbURL(1, 24, 24, $profilePic)?>" alt="" /></a>
		<?php
	}
	if (!$API->isLoggedIn()) {
	?>
	<div class="info" style="color: #555;">
		<img src="/images/f.png" alt="" /><img src="/images/t.png" alt="" /> If you have a Facebook or Twitter account, then you already have a <?=$siteName?> account.&nbsp; <a href="javascript:getStarted();">login here</a>
	</div>
	<?php } ?>
</div>

<div id="sideinfo" class="sideinfo">
	<div class="phones">
		<a href="/email.php"><img src="/images/phones.jpg" alt="" style="float: left;" /></a>
		<div style="float: left; width: 150px; padding-top: 5px;">
			<div class="attention" style="padding-bottom: 3px;">No app required!</div>
			Share media using your phone in real time, using e-mail!&nbsp; <a href="/email.php">learn how &gt;&gt;</a>
		</div>
		<div style="clear: both;"></div>
	</div>
	<div class="new" style="color: #555;">
		<span>Invite</span> your friends to <span><?=$siteName?><span style="color: #ff8040;">.</span>com</span>, and we'll send you a <span>$100</span> gift card!&nbsp; <a href="/invite.php">learn how &gt;&gt;</a>
	</div>
	<div class="follow"><a href="http://www.facebook.com/pages/mediaBirdy/125626647481052" target="_new"><img src="/images/follow_us_facebook.png" alt="" /></a><img src="/images/and.png" style="padding: 13px;" alt="" /><a href="http://twitter.com/media_birdy" target="_new"><img src="/images/follow_us_twitter.png" alt="" /></a></div>
</div>

<div style="clear: both;"></div>

<script language="javascript" type="text/javascript">
<!--

var banners = new Array();
<?php
$i = 0;
foreach ($banners as $id => $link)
{
	if ($i == 0)
		echo "var currentBanner = $i;\n";
	echo "banners[" . $i++ . "] = [$id, '$link'];\n";
}
?>

function initPage()
{
	setInterval("getNewestMedia()", 30000); 
}

setInterval("nextBanner()", 7000); 

doPrettyDate();

var currentMedia = new Array();
currentMedia[0] = new Array();
currentMedia[1] = new Array();

<?php
$i = 0;
$j = 0;
$switchedToPhotos = false;

foreach ($currentMedia as $media)
{
	if ($media[0] == "P" && !$switchedToPhotos)
	{
		$i = 1;
		$j = 0;
		$switchedToPhotos = true;
	}
	echo "currentMedia[$i][" . $j++ . "] = ['" . implode("','", $media) . "'];\n";
}

if (isset($_GET['upload']))
	echo 'getStarted(); document.getElementById("tabcontent0").style.visibility = "";';
//elseif (isset($_GET['login']))
	//echo 'getStarted();';
elseif (isset($_GET['how_it_works']))
	echo 'switchTab(1);';
elseif (isset($_GET['why_it_works']))
	echo 'switchTab(2);';

?>

//-->
</script>

<?php include "footer.php";
?>
