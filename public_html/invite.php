<?php

include_once "inc/inc.php";

if (!$API->isLoggedIn())
{
	$_SESSION['error'] = "You must be logged in to invite friends.";
	header("Location: /");
	die();
}

if( $site != "m")
  $title = "Invite contacts";
else
  $title = "Invite friends";

if ($site == "s")
$scripts[] = "/getcontactstatus.js.php";
include "header.php";

include "invite_$site.php";

$scripts[] = "/fbinvited.js.php";
$scripts[] = "/invite.js";
include "footer.php";

?>
