<div style="width: 554px; border: 3px solid #fff; background: #d8dfea; padding: 25px; margin: 15px 0 0 13px; float: left;">
	<div style="padding-left: 10px;">
		<div style="font-size: 18pt; color: #000; float: left; font-weight: bold;">
			Invite your friends to<br />
			<span style="color: #326798;"><?=$siteName?><span style="color: #FF7F27;">.</span>com</span>, and<br/>
			we'll send you a<br/>
			$100.00 Gift Card!
			<div style="font-size: 8pt; font-weight: normal; padding: 5px 0 0 18px;">
				<a href="/winners.php">(view current gift card recipients)</a>
			</div>
		</div>
		<div style="float: right;">
			<a href="/giftcard.php"><img src="/images/giftcard.png" width="241" height="166" alt="" /></a>
		</div>
		<div style="clear: both;"></div>
	</div>
	
	<ul>
		<li>Select a social network or e-mail service below.</li>
		<li>Log into your account, and select the users you'd like to invite.</li>
		<li><?=$siteName?> will send each person you select an invitation in your name asking them to join <?=$siteName?>.</li>
	</ul>
	
	<div class="subtitlepic" style="margin: 10px 0 30px 25px; background-image: url(/images/help.png);" onclick="javascript:showHelp();"><span style="font-size: 11pt; font-weight: bold;">more tips and recommendations &#0133; </span>click here</div>
	
	<?php
	include "inc/contacts.php";
	showContactsTop();
	?>
	
	<div style="margin-top: 30px; float: right;">
		<div style="float: left; font-size: 8pt; color: #808080; padding-right: 10px;">share this page</div>
		<div style="float: left;"><a class="addthis_button_facebook"></a></div>
		<div style="float: left;"><a addthis:description="http://<?=$_SERVER['HTTP_HOST']?>/invite.php" addthis:title="<?=$media['title']?>" class="addthis_button_twitter"></a></div>
		<div style="float: left;"><a onclick="javascript:showEmail('S', 'invite.php');" href="javascript:void(0);"><span class="at300bs at15t_email"></span></a></div>
		<div style="clear: both;"></div>
	</div>	
</div>

<div style="float: right; padding: 64px 2px 0 0;">
	<?php if( $API->adv ) { showAd("companion"); } ?>
</div>

<script language="javascript" type="text/javascript">
<!--

function showHelp()
{
	html  = '	<div style="font-size: 16pt; color: #000; font-weight: bold; padding-bottom: 10px;">';
	html += '		About the giveaway';
	html += '	</div>';
	html += '	<div style="font-size: 11pt;">';
	html += '		<div style="padding-bottom: 10px;">';
	html += '			Every person who clicks on your invite link and joins <?=$siteName?> will automatically add another entry under your <?=$siteName?> account in our monthly giveaway.&nbsp; The fun part is, the more people who join through your link, the more entries you will get.&nbsp; (We don\'t want to bug your friends, though, so keep in mind how often you invite them.)';
	html += '		</div>';
	html += '	<div style="font-size: 16pt; color: #000; font-weight: bold; padding-top: 5px;">Here\'s what you need to do</div>';
	html += '		<div class="gcoption">';
	html += '			Option 1 -';
	html += '		</div>';
	html += '		<div class="gctext">';
	html += '			The easiest way to enter is to invite your friends and followers by using the invite tools found on this page.&nbsp; Log into <?=$siteName?> and use the tools on this page to invite your friends from Facebook, followers on Twitter and people from your contact lists on Hotmail, MSN, Yahoo, Gmail or enter them manually.&nbsp; The <?=$siteName?> tools we have provided will take care of the rest by sending invites to your friends, fans and followers.&nbsp; It\'s also a good idea to follow up with them and talk about <?=$siteName?> on your favorite social sites and forums to make sure they join.';
	html += '		</div>';
	html += '		<div class="gcoption">';
	html += '			Option 2 -';
	html += '		</div>';
	html += '		<div class="gctext">';
	html += '			Log into <?=$siteName?>.com and go to your profile page <a href="<?=$API->getProfileURL()?>">located here</a>.&nbsp; Two thirds of the way down the page on the right side, you will see your "invite link".&nbsp; Copy this link and use it in your tweets, on Facebook, when blogging, on your website, in a press release, forums and any other place you think people may click on it.&nbsp; This link allows you to receive an entry for anybody who clicks on it and joins <?=$siteName?>.&nbsp; After you have used the link, go ahead and use it again and again and remember to continue talking about using the link and joining <?=$siteName?>.com.&nbsp; This will increase your chances of winning.';
	html += '		</div>';
	html += '		<div class="gcoption">';
	html += '			Tip -';
	html += '		</div>';
	html += '		<div class="gctext">';
	html += '			By using options 1 and 2 above, this should increase your chances of winning and is highly recommended.<p />We really appreciate everyone\'s participation and wish you all good luck!<p />- the <span style="font-weight: bold; color: #326798;"><?=$siteName?></span> team';
	html += '		</div>';
	html += '	</div><div style="clear: both;"></div>';	
	showPopUp("", html, 610, 8);
}

//-->
</script>

<?php
$scripts[] = "http://s7.addthis.com/js/250/addthis_widget.js#username=mediabirdy";
$scripts[] = "/email.js";
?>
