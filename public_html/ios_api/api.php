<?
require_once '../inc/sql.php';
require_once '../inc/api.php';

class Ios_api{
    public function getJobs(){	//340 402
        $query = "select gid as 'id', gname as 'title' from pages where cat = 106";
        $jobs = $this->getData($query);

        return json_encode(array('jobs'=>$jobs));
    }

    public function getWorks(){	//288746 6957rows 322200 7004rows
//         $query = "select gid as 'id', gname as 'title' from pages where gid IN (SELECT gid FROM `page_categories` WHERE `cat` = '113') or type = 1388";
        $query = "
            SELECT 
                pages.gid AS id, gname AS title
            FROM
            pages NATURAL 
            JOIN (
                (SELECT 
                    gid 
                FROM
                    pages 
                WHERE pages.privacy = 0 
                AND (TYPE = 1388))
                ) AS gids 
            LEFT JOIN photos 
                ON photos.id = pid 
            LEFT JOIN users 
                ON photos.uid = users.uid 
            INNER JOIN categories 
                ON categories.cat = pages.cat 
            GROUP BY pages.gid
        ";
        $works = $this->getData($query);

        return json_encode(array('works'=>$works));
    }

    public function getSectors(){   //269 271
        $query = "SELECT cat AS 'id', catname AS 'title' FROM categories WHERE cattype = 'G' AND industry = 1388 ORDER BY catname";
        $sectors = $this->getData($query);

        return json_encode(array('sectors'=>$sectors));
    }
    
    public function getVessels($offset=0) {
        
//         $query = "
//             SELECT 
//                 pages.gid, gname AS title
//             FROM
//             pages NATURAL 
//             JOIN (
//                 (SELECT 
//                     gid 
//                 FROM
//                     pages 
//                 WHERE pages.privacy = 0 
//                 AND (TYPE = 1389))
//                 ) AS gids 
//             LEFT JOIN photos 
//                 ON photos.id = pid 
//             LEFT JOIN users 
//                 ON photos.uid = users.uid 
//             INNER JOIN categories 
//                 ON categories.cat = pages.cat 
//             GROUP BY pages.gid
//         ";
        $query = "
            SELECT 
                pages.gid AS id, gname AS title
            FROM
            pages NATURAL 
            JOIN (
                (SELECT 
                    gid 
                FROM
                    pages 
                WHERE pages.privacy = 0 
                AND (TYPE = 1389))
                ) AS gids 
            
            INNER JOIN categories 
                ON categories.cat = pages.cat
        ";
        $query .= $offset > 0 ? "LIMIT $offset , 10000" : "LIMIT 10000" ;
        $vessels = $this->getData($query);

        return json_encode(array('vessels'=>$vessels));
        
    }
    public function getVesselsExtended($search_str) {
//         $query = "SELECT 
//                     pages.gname 
//                   FROM pages 
//                   WHERE 
//                     privacy = 0 
//                   AND 
//                     TYPE = 1389
//         ";
//         $query .= $offset > 0 ? "LIMIT $offset , 10000" : "LIMIT 10000" ;
//         $query .= $offset > 0 ? "LIMIT $offset , 30000" : "LIMIT 30000" ;
        
        $query = "
                SELECT 
                    pages.gid AS id, pages.gname AS title, boats.length AS length_ft, boats.length*0.3048 AS length_m
                FROM 
                    pages LEFT JOIN boats ON pages.gid = boats.gid
                WHERE 
                    pages.gname LIKE '%$search_str%' 
                AND 
                    pages.type = 1389
                AND 
                    pages.privacy = 0
        ";
        
        $vessels = $this->getData($query);
//         $vessels = array_unique($vessels);

        return json_encode(array('vessels' => $vessels));
        
    }
    public function getVesselsExtended1($search_str) {
                
                $q = $search_str;
//                 $cats = array();
//                 
//                 $include_pages = true;
//                 $select_pages['exnames'] = "trim(exnames)";
//                 $select_pages['length_ft'] = "length";
//                 $select_pages['length_m'] = "(length*0.3048)";
//                 $types[] = 1389;
//                 $union[] = "select pages.gid from boats as boats inner join pages on boats.id=pages.glink where name != '' and exnames like '%{$q}%'";
//                 $join[] = "LEFT JOIN boats as boats ON glink=boats.id";
//                 
//                 array_unshift($union, "SELECT gid from pages where gname LIKE '%{$q}%' AND pages.privacy=" . 0 . " and (" .
//                                         (count($types) > 0 ? "type=" . implode(' or type=', $types) . (count($cats) > 0 ? ' or ' : '') : '') .
//                                             (count($cats) > 0 ? " cat=" . implode(' or cat=', $cats) : "") . "
//                                             )");
//                 
//                 $sel_pages = '';
//                 if (count($select_pages) > 0)
//                     foreach ($select_pages as $k => $v)
//                         $sel_pages .= ",$v as $k";
//                 
//                 $query[] = "SELECT pages.cat,pages.gid,type,users.container_url,hash,gname as title,catname as cat_str{$sel_pages}
//                         FROM pages
//                         natural join (
//                             (" . implode(")\n\t\t\t\t\tunion (", $union) . ")
//                         ) as gids
//                         " . implode ("\n", $join) . "
//                         LEFT JOIN photos ON photos.id=pid
//                         LEFT JOIN users ON photos.uid=users.uid
//                         inner join categories on categories.cat=pages.cat
//                         group by pages.gid";
//                 
//                 $vessels = $this->getData($query[0]);
                
                
                $query = "
                    SELECT 
                        pages.gname 
                    FROM pages 
                    WHERE 
                        gname LIKE '%$search_str%' 
                    AND 
                        type = 1389
                    AND 
                        privacy = 0 
                    
                ";
                $query = "
                    SELECT 
                    pages.gid AS id, pages.gname AS title, boats.length AS length_ft, boats.length*0.3048 AS length_m
                    FROM pages LEFT JOIN boats ON pages.gid = boats.gid
                    WHERE 
                    pages.gname LIKE '%$search_str%' 
                    AND 
                    pages.type = 1389
                    AND 
                    pages.privacy = 0
                    
                ";
                $vessels = $this->getData($query);
                
                
        //         $vessels = array_unique($vessels);
//                 $str = '';
//                 foreach($vessels as $key => $value) {
//                     $str = $str . $value['gname'] . ',';
//                 }
//                 $str = rtrim($str, ",");

                return json_encode(array('vessels' => $vessels));
        
        
//         $ch = curl_init('http://dev.salthub.com/ajax/get_vessel.php');
//         curl_setopt($ch, CURLOPT_POSTFIELDS, 'q=GEORG+OTS&n=100&guid=f5443e2e-4073-50b0-9ec-7500e7114b6');
//         curl_exec($ch); // выполняем запрос curl - обращаемся к сервера php.su
//         curl_close($ch);
        
    }

    public function getUser($data){
        $data = json_decode($data, true);
        $id = $data[0]['id'];

        $query = "SELECT users.password as 'pass', users.uid, users.name as 'username', users.email, users.public_email, users.fax AS 'phone', users.phone AS 'home_phone', users.www AS 'web', categories.catname AS 'sector', users.occupation as 'job', users.company as 'work'
                      FROM users
                      LEFT JOIN categories ON categories.cat = users.sector
                      WHERE uid = ".$id;

        $result = $this->getData($query);

        if ($result == null)
            $result = null;
        else{
            $result[0]['photo'] = $this->getUserPhoto($result[0]['uid']);
            $result[0]['skills'] = $this->getSkills($result[0]['uid']);
        }

        return json_encode(array('user'=>$result[0]));
    }

    private function getUserInfo($id){
        $query = "SELECT users.uid, users.name as 'username', users.email, users.cell AS 'phone', users.phone AS 'home_phone', users.www AS 'web', categories.catname AS 'sector', users.occupation as 'job', users.company as 'work'
                      FROM users
                      LEFT JOIN categories ON categories.cat = users.sector
                      WHERE uid = ".$id;

        $result = $this->getData($query);

        if ($result == null)
            $result = null;
        else{
            $result[0]['photo'] = $this->getUserPhoto($result[0]['uid']);
            $result[0]['skills'] = $this->getSkills($result[0]['uid']);
        }

        return $result[0];
    }

    public function getFriends($data){
        $data = json_decode($data, true);
        $user = $data[0]['id'];

        $query = "SELECT uid as 'id' FROM friends INNER JOIN users ON IF(id1 = ".$user.", id2, id1) = users.uid  WHERE (id1 = ".$user." OR id2 = ".$user.") AND active = 1 AND STATUS = 1";
//        $query = "SELECT id2 as 'id' FROM friends WHERE id1 = ".$user." or id2 = ".$user."  GROUP BY id1";
        $result = $this->getData($query);

        $friends = array();
        if ($result != null){
            foreach ($result as $key => $value){
                $friends[] = $this->getUserInfo($value['id']);
            }
        }
        else
            $friends[0] = 'not found';

        return json_encode(array('friends'=>$friends));
    }

    public function checkUser($data){
        $data = json_decode($data, true);
        $query = "select uid from users where email = '".$data[0]['email']."'";

        $result = $this->getData($query);
        if ($result == null)
            $return = false;
        else
            $return = true;

        return json_encode(array('result'=>$return));
    }

    /*
    [{"login": "anarchy92@mail.ru","pass": "123123123"}]
     */
    public function loginUser($data){
        $data = json_decode($data, true);
        $login = $data[0]['login'];
        $pass = $data[0]['pass'];

        $query = "SELECT users.uid, users.name as 'username', users.email, users.public_email, users.cell AS 'phone', users.phone AS 'home_phone', users.www AS 'web', categories.catname AS 'sector', users.occupation as 'job', users.company as 'work'
                      FROM users
                      LEFT JOIN categories ON categories.cat = users.sector
                      WHERE email = '".$login."' and password = '".md5($pass)."'";

        $result = $this->getData($query);
        if ($result == null)
            return json_encode(0);
        else{
            $result[0]['photo'] = $this->getUserPhoto($result[0]['uid']);
            $result[0]['skills'] = $this->getSkills($result[0]['uid']);
        }

        return json_encode(array('user'=>$result));
    }

    /*
    [{"home_phone":"553627","sector":"1310","phone":"3355447","pass":"qq","web":"www","email":"seregsdfsdfa","job":"title","work":"place","username":"serega", "skills":{"0":"first", "1":"second", "2":"third"}}]

    [{"username": "john","job": "werwer", "work": "dfgdg", "sector": "111","phone": "10101010","email": "anarchy92@mail.ru","pass": "123123123"}]
    [{"0":"first", "1":"second", "2":"third"}]
    */
    public function regUser($data, $photo, $skills = null){
        $data = json_decode($data, true);

        $name = $data[0]['username'];
        $job = $data[0]['job'];
        $work = $data[0]['work'];
        $sector = $data[0]['sector'];
        $phone = $data[0]['phone'];
        $email = $data[0]['email'];
        $public_email = $data[0]['public_email'];
        $pass = $data[0]['pass'];

        if (isset($data[0]['skills']))
            $skills = $data[0]['skills'];

        if (isset($data[0]['home_phone']))
            $home_phone = $data[0]['home_phone'];
        else
            $home_phone = '';
        
        if (isset($data[0]['web']))
            $web = $data[0]['web'];
        else
            $web = '';

        $checkUser = "SELECT users.uid, users.name as 'username', users.email, users.public_email, users.cell AS 'phone', users.phone AS 'home_phone', users.www AS 'web', categories.catname AS 'sector', users.occupation as 'job', users.company as 'work'
                      FROM users
                      LEFT JOIN categories ON categories.cat = users.sector
                      WHERE email = '".$email."'";

        $result = $this->getData($checkUser);

        // if user is not exists
        if ($result == null){
            $query = "insert into users(name, username, password, email, public_email, cell, sector, active, verify, occupation, company, phone, www, joined) values('".$name."', '".$name."', '".md5($pass)."', '".$email."', '".$public_email."', '".$phone."', '".$sector."', 1, 1, '".$job."', '".$work."', '".$home_phone."', '".$web."', '".date('Y-m-d H:i:s')."')";
            $result = mysql_query($query);

            // if insert success
            if ($result == true){
                $id = mysql_insert_id();

                if ($photo != null)
                    $image = $this->setUserPhoto($id, $photo);

                if (isset($data[0]['skills']))
                    $this->setSkills($id, $skills);

                $result = $this->getData($checkUser);
            }
            else{
                return json_encode(array('error'=>'insert to users'));
            }
        }

        $result[0]['skills'] = $this->getSkills($result[0]['uid']);
        $result[0]['photo'] = $this->getUserPhoto(intval($result[0]['uid']));

        return json_encode(array('user'=>$result));
    }

    public function setSkills($id, $skills){
        ksort($skills);
        $data = implode(chr(2), $skills);
        $query = "update users set contactfor = '".$data."' where uid = ".$id;
        $result = mysql_query($query);

        return $result;
    }

    // [{"id":"12062", "username":"john", "pass":"123123123", "email":"anarchy92@mail.ru", "public_email":"anarchy99@mail.ru", "phone":"000000000", "home_phone":"1111111111", "sector":"1310", "web":"newsite.com", "job":"new job", "work":"new work", "skills":{"0":"new first skill", "1":"new second skill", "2":"new third skill", "3":"new fourth skill", "4":"new fifth skill"}}]
    public function update($data, $photo){
        $data = json_decode($data, true);

        $id = $data[0]['id'];
        $name = $data[0]['username'];
        $pass = $data[0]['pass'];
        $email = $data[0]['email'];
        $public_email = $data[0]['public_email'];
        $cell = $data[0]['phone'];
        $phone = $data[0]['home_phone'];
        $sector = $data[0]['sector'];
        $web = $data[0]['web'];
        $job = $data[0]['job'];
        $work = $data[0]['work'];
        $skills = $data[0]['skills'];

        $query = "update users set name = '$name', password = '".md5($pass)."', email = '$email', public_email = '$public_email', cell = '$cell', phone = '$phone', sector = $sector, www = '$web', occupation = '$job', company = '$work' where uid = $id";

        /*
        [{"home_phone":"4675457","sector":"1305","id":"11919","phone":"9075457","web":"www","skills":{"0":"first","1":"second","2":"third"},"email":"kserega@mail.ru","job":"Title","pass":"qqq","work":"place","username":"Serega Kovalenko"}]
         */

        $result = mysql_query($query);
        if (!$result)
            return json_encode(array('error'=>'update info'));

        $result = $this->setSkills($id, $skills);
        if (!$result)
            return json_encode(array('error'=>'update skills'));

        if (isset($photo)){
            $result = $this->setUserPhoto($id, $photo);
            if (!$result)
                return json_encode(array('error'=>'update photo'));
        }

        return json_encode(array('photo'=>$this->getUserPhoto($id)));
    }

    private function getData($query){
        $array = mysql_query($query);
        $result = array();

        if ($array == false)
            $result[] = null;
        else{
            while ($row = mysql_fetch_assoc($array)){
                $result[] = $row;
            }
        }
        return $result;
    }

    /*
     [{"user1": "11805", "user2": "11793"}]
     */
    public function setFriends($data){
        $data = json_decode($data, true);
        $user1 = $data[0]['user1'];
        $user2 = $data[0]['user2'];

        if ($user1 == $user2)
            $result = 'You can not connect to your account';
        else{
            if (!$this->isFriends($user1, $user2)){
                // add friend
                $query = "insert into friends(id1, id2, status) values(".$user1.", ".$user2.", 1)";
                $result = mysql_query($query);
                if (!$result)
                    $result = false;
                else{
                    // send notification
                    $query = "insert into notifications (uid, type, from_uid) values (" . $user1 . ", 'f', " . $user2 . ")";
                    mysql_query($query);
                    $query = "insert into notifications (uid, type, from_uid) values (" . $user2 . ", 'f', " . $user1 . ")";
                    mysql_query($query);

                    $query = "select fid from friends where id1 = ".$user1." and id2 = ".$user2;
                    $result = $this->getData($query);
                    $fid = $result[0]['fid'];

                    if (!empty($fid)){
                        $this->addFeed("F", 1, $fid, $user1, $user2);
                        $this->addFeed("F", 1, $fid, $user2, $user1);

                        $result = true;
                        //$this->sendNotificationToMail($user1, $user2);
                    }
                    else
                        $result = 'feed id is empty';
                }
            }
            else
                $result = 'already friends';
        }
        return json_encode(array("result"=>$result));
    }

    private function getProfileURL($uid)
    {
        $query = "select username from users where uid = $uid";
        $result = $this->getData($query);
        $username = $result[0]['username'];

        if (empty($username)){
            return "/user/_$uid";
        }
        else
        {
            $username = str_replace(" ", ".", $username);
            return "/user/$username";
        }
    }

    private function sendMail($uid, $name){
        include "../inc/phpmailer.php";

        $query = "select content from static where id='connect_accept'";
        $result = $this->getData($query);

        $subj = "Connect with " . $name;
        $from = "notifications";
        $email = $this->getEmail($uid);

        if (!empty($email)){
            $msg = $result[0]['content'];
            $msg = str_replace( "{NAME_FROM}", $name, $msg );
            $msg = str_replace( "{PROFILE_URL}", "http://www.salthub.com" . $this->getProfileURL($uid) , $msg );
            $msg = str_replace( "{SITENAME}", 'salthub.com', $msg );
            $msg = nl2br( $msg );

            $mail->SetFrom("$from@SaltHub.com", "SaltHub");
            $mail->isHTML(true);
            $mail->AddAddress($email);
            $mail->Subject = $subj;
            $mail->Body = template($msg, $email);
            $mail->Send();

            return true;
        }
        else
            return 'email is empty';
    }

    private function getEmail($uid){
        $query = "select email from users where uid = ".$uid;
        $result = $this->getData($query);

        return $result[0]['email'];
    }

    private function addFeed($type, $from_app, $id, $uid_to, $uid_from, $gid = 0, $last_update = null){
        $feed = array(
            "type"      => $type,
            "link"      => $id,
            "uid"       => $uid_to,
            "uid_by"    => $uid_from,
            "gid"       => $gid,
            "last_update" => $last_update,
            "from_app" => $from_app
        );

        mysql_query("insert into feed (" . implode(",", array_keys($feed)) . ") values ('" . implode("','", $feed) . "')");
        $feed['fid'] = mysql_insert_id();
        $result = $feed['fid'];

        return $result;
    }

    private function isFriends($user1, $user2){
        $query = "SELECT COUNT(*) as 'count' FROM friends WHERE (id1 = ".$user1." AND id2 = ".$user2.") or (id2 = ".$user1." AND id1 = ".$user2.")";
        $count = $this->getData($query);

        return $count[0]['count'] > 0 ? true : false;
    }

    private function getName($user){
        $query = "select name from users where uid = ".$user;
        $result = $this->getData($query);

        return $result[0]['name'];
    }

    public function sendNotificationToMail($user1, $user2){
        $user1_name = $this->getName($user1);
        $user2_name = $this->getName($user2);

        $this->sendMail($user1, $user1_name);
        $this->sendMail($user2, $user2_name);
    }

    private function getUserPhoto($uid){
        $query = "SELECT photos.hash FROM photos INNER JOIN users ON users.pic = photos.id WHERE users.uid = ".$uid;
        $photo = $this->getData($query);

        if ($photo == null)
            return null;

        $query = "select container_url as 'hash' from users where uid = ".$uid;
        $cont = $this->getData($query);

        $url = $cont[0]['hash'].'/'.$photo[0]['hash'].'_tall.jpg';

        return $url;
    }

    private function setUserPhoto($uid, $data){
        $tmp = $data['tmp_name'];

        $hash = md5(microtime());

        $result = $this->getData("select id from albums where uid = ".$uid." and title = 'profile photos'");
        if ($result == null)
            mysql_query("insert into albums (uid,title,descr,albtype) values (".$uid.",'Profile Photos','My profile photos',2)");

        $result = $this->getData("select id from albums where uid = ".$uid." and albType=2");
        $aid = $result[0]['id'];

        $query = "insert into photos(uid,aid,hash,width,height) values (".$uid.", ".$aid.", '$hash',178,266)";
        mysql_query($query);
        $id = mysql_insert_id();
        mysql_query("update albums set mainImage=".$id." where uid = ".$uid." and albType=2");


        include_once '../inc/misc.php';
        $jmp = shortenURL("http://dev.salthub.com/photo.php?id=$id");
        mysql_query("update photos set jmp='$jmp' where id=$id");

        $info = getimagesize($tmp);

        switch ($info['mime'])
        {
            case "image/jpeg":
                $img = @imagecreatefromjpeg($tmp);
                break;

            case "image/png":
                $img = @imagecreatefrompng($tmp);
                break;

            case "image/gif":
                $img = @imagecreatefromgif($tmp);
                break;
        }

        //$img = @imagecreatefrompng($tmp);
        $info = getimagesize($tmp);
        $new_img = ImageCreateTrueColor(178, 266);
        $target_file = "/tmp/$hash.jpg";

        imagecopyresampled($new_img, $img, 0, 0, 0, 0, 178, 266, $info[0], $info[1]);
        imagejpeg($new_img, $target_file, 90);



        $this->uploadToCloud( $uid, $target_file, $hash . ".jpg" );

        $temp_thumb = "/tmp/" . mysql_thread_id();
        include_once '../inc/createThumb.php';

        if( file_exists( $temp_thumb ) ) unlink( $temp_thumb );

        $file = createThumb( 119, 95, true, true, $id, $hash, $temp_thumb );
        $this->uploadToCloud( $uid, $file, $hash . "_wide.jpg" );

        if( file_exists( $temp_thumb ) ) unlink( $temp_thumb );
        $file = createThumb( 178, 266, false, true, $id, $hash, $temp_thumb );
        $this->uploadToCloud( $uid, $file, $hash . "_tall.jpg" );

        if( file_exists( $temp_thumb ) ) unlink( $temp_thumb );
        $file = createThumb( 48, 48, true, true, $id, $hash, $temp_thumb );
        $this->uploadToCloud( $uid, $file, $hash . "_square.jpg" );

        $result = mysql_query("update users set pic = ".$id." where uid = ".$uid);

        return $result;
    }

    private function uploadToCloud( $uid = null, $file, $dest_name )
    {
        include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/rackspace/cloudfiles.php" );

        if( $uid == null )
            $uid = 11810;

        $objRackspaceAuthentication = new CF_Authentication('tweider', '2eafbeef6d3c8e96728ebd33e031c533');
        $blAuthenticated = $objRackspaceAuthentication ->	authenticate();
        $objRackspaceConnection = new CF_Connection ($objRackspaceAuthentication);
        $objRackspaceConnection->setDebug (false);
        $objRackspaceConnection->ssl_use_cabundle();

        $this->CloudConnection = $objRackspaceConnection;

        $createContainer = false;
        $containerName = "usr_11810";

        if( strlen( quickQuery( "select container_url from users where uid='$uid'" ) ) > 0 )
        {
            try {
                $container = $objRackspaceConnection->get_container($containerName);
            }
            catch( Exception $e )
            {
                $createContainer = true;
            }
        }
        else
            $createContainer = true;

        if( $createContainer )
        {
            $container = $objRackspaceConnection->create_container($containerName);
            $url = addslashes( $container->make_public() );
            mysql_query( "update users set container_url='$url' where uid='" . $uid . "'" );
        }

        if( !file_exists( $file ) )
        {
            return false;
        }

        $remote_obj = $container->create_object( $dest_name );

        if( !$remote_obj )
        {
            return false;
        }

        if( !( $remote_obj->write ( fopen($file, "r"),	filesize($file) ) ) )
        {
            return false;
        }
        //var_dump($remote_obj);

        //echo "\n\ngood\n\n\n";

        return true;
    }

    /*
     * these methods are not used
     */

    private function getSkills($id){
        $query = "SELECT users.contactfor as 'skills' FROM users WHERE users.uid = ".$id;
        $result = $this->getData($query);

        if ($result == null)
            return null;
        else{
            $result = implode('', $result[0]);

            $data = explode(chr(2), $result);
            return $data;
        }
    }

    private function getJobByUser_old($id){
        $query = "SELECT pages.gname AS 'job' FROM pages LEFT JOIN `work` ON work.occupation = pages.gid WHERE work.uid = ".$id;
        return $this->getData($query);
    }

    private function getWorkByUser_old($id){
        $query = "SELECT pages.gname AS 'work' FROM pages LEFT JOIN `work` ON work.employer = pages.gid WHERE work.uid = ".$id;
        return $this->getData($query);
    }

    private function getJobByUser($id){
        $query = "SELECT users.occupation as 'job' FROM users WHERE users.uid = ".$id;
        return $this->getData($query);
    }

    private function getWorkByUser($id){
        $query = "SELECT users.company as 'work' FROM users WHERE users.uid = ".$id;
        return $this->getData($query);
    }




    // test photo
    public function test($params){
        $params = json_decode($params, true);
        ksort($params[0]);
        return $params;
    }
    
    
    
/*
**Added by Me
*/

    public function myTest($data){
        $data = json_decode($data, true);
        $login = $data[0]['login'];
        $pass = $data[0]['pass'];

        $sent = 0;
//         $f = "u";
        $f = "r";
        $q1 = "select mid," . ($sent == 1 ? "messages.uid_to as uid, messages.uid as uid2, comments.uid as sender" : "users.uid" ). ",username,name,pic,subj,comment,unreadBy" . ($sent == 1 ? "Send" : "Recv") . " as unread,created from messages inner join users on users.uid=messages.uid inner join comments on comments.link=mid where delBy" . ($sent == 1 ? "Send" : "Recv") . " = 0 and messages.uid" . ($sent == 1 ? "" : "_to") . "=" . 12080 . " and comments.uid " . ($sent == 1 ? "" : "!") . "= " . 12080 . ($f == "u" || $f == "r" ? " and unreadBy" . ($sent == 1 ? "Send" : "Recv") . ($f == "u" ? " > 0" : "= 0") : "");
        $q2 = "select mid," . ($sent == 1 ? "messages.uid_to as uid, messages.uid as uid2, comments.uid as sender" : "comments.uid"). ",username,name,pic,subj,comment,unreadBy" . ($sent == 1 ? "Recv" : "Send") . " as unread,created from messages inner join comments on comments.link=mid inner join users on users.uid=comments.uid where delBy" . ($sent == 1 ? "Recv" : "Send") . " = 0 and messages.uid" . ($sent == 1 ? "_to" : "") . "=" . 12080 . " and comments.uid " . ($sent == 1 ? "" : "!") . "= " . 12080 . ($f == "u" || $f == "r" ? " and unreadBy" . ($sent == 1 ? "Recv" : "Send") . ($f == "u" ? " > 0" : "= 0") : "");

        $q = "from (select * from (select * from (($q1) union ($q2)) as t0) as t1 order by created desc) as t2 group by mid order by created desc";

    //    $mcount[$f] = intval(quickQuery("select sum(c) from (select count(*) as c $q) as xyz"));
//         $mcount[$f] = intval($this->getData("select count(*) from (select unread as c $q) as xyz"));
        $mcount = $this->getData("select count(*) from (select unread as c $q) as xyz");
        
        
        $x = "select uid_to,uid,subj from messages where  (uid_to = 12080 or uid= 12080 )";
        
        print_r($this->getData($x)); exit;
        
        
        return json_encode(array('user'=>$result));
    }
    
    //sends private message to a user, (copied from API class)
//     function sendMessage($uid, $subj, $body, $from = null, $notify = true)
//  [{"uid"}:{""},{"uid_to"}:{""},{"subj"}:{""},{""}:{""}]
    function sendMessage($data) {
        $data = json_decode($data, true);
        $uid    = $data[0]['uid'];
        $uid_to = $data[0]['uid_to'];
        $subj   = $data[0]['subj'];
        $body   = $data[0]['body'];
        
//         if( empty($from) ) $from = $this->uid;
//         $real_uid = $this->uid;
//         $this->uid = $from;

        $query = "insert into messages (unreadByRecv,uid,uid_to,subj) values (" . implode(",", array(1, $uid, $uid_to, "'" . $subj . "'")) . ")";
        mysql_query($query);
        $mid = mysql_insert_id();
        $query = "insert into comments (type,link,comment,uid) values (" . implode(",", array("'M'", $mid, "'" . addslashes($body) . "'", $uid)) . ")";
        mysql_query($query);
        
       
        
//         if( $notify )
//             $this->sendNotification(NOTIFY_MESSAGE, array("uid" => $uid, "mid" => $mid));
// 
//         $this->uid = $real_uid;
//         return $mid;
        return json_encode(array("mid"=>"$mid"));
    }
    
    function updatePass($data) {
        $data = json_decode($data, true);
        $email = $data[0]['email'];
        $pass = $data[0]['pass'];
        $pass = md5($pass);
        $q = "UPDATE users SET password = '$pass' WHERE email = '$email' LIMIT 1";
        mysql_query($q);
    }
  
    
    
}










?>