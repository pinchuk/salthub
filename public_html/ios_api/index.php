<?
    header('Content-type: text/json');

    ini_set('display_errors', 1);
    error_reporting(E_ALL);

    date_default_timezone_set('UTC');

    require_once 'api.php';
    $api = new Ios_api();

    if (isset($_GET['method'])){
        switch ($_GET['method']){
            case 'getJobs':
                echo $api->getJobs();
                break;

            case 'getSectors':
                echo $api->getSectors();
                break;

            case 'getWorks':
                echo $api->getWorks();
                break;
        }
    }
    
    else if (isset($_POST['getJobsGzip'])){
        echo gzencode($api->getJobs());
    }
    else if (isset($_POST['getSectorsGzip'])){
        echo gzencode($api->getSectors());
    }
    else if (isset($_POST['getWorksGzip'])){
//+         print_r($api->getWorks()); exit;
        echo gzencode($api->getWorks());
    }
    else if (isset($_POST['getVesselsGzip'])){
        $offset = $_POST['getVesselsGzip'];
//         print_r($api->getVessels($offset)); exit;
        echo gzencode($api->getVessels($offset));
    }
    else if (isset($_POST['getVesselsGzipExtended'])){
        $search_str = $_POST['getVesselsGzipExtended'];
//         print_r($api->getVesselsExtended($search_str)); exit;
        echo gzencode($api->getVesselsExtended($search_str));
    }
    else if (isset($_POST['getVesselsGzipExtended1'])){
        $search_str = $_POST['getVesselsGzipExtended1'];
        print_r($api->getVesselsExtended1($search_str)); exit;
        echo gzencode($api->getVesselsExtended1($search_str));
    }
    
    else if (isset($_POST['loginUser'])){
        $data = $_POST['loginUser'];
        echo $api->loginUser($data);
    }
    else if (isset($_POST['regUser'])){
        $data = $_POST['regUser'];
        $photo = null;

        if (isset($_FILES['photo']))
            $photo = $_FILES['photo'];

        echo $api->regUser($data, $photo);
    }
    else if (isset($_POST['setFriends'])){
        $data = $_POST['setFriends'];
        echo $api->setFriends($data);
    }
    else if (isset($_POST['checkUser'])){
        $data = $_POST['checkUser'];
        echo $api->checkUser($data);
    }
    else if (isset($_POST['update'])){
        $data = $_POST['update'];
        $photo = null;

        if (isset($_FILES['photo']))
            $photo = $_FILES['photo'];

        echo $api->update($data, $photo);
    }
    else if (isset($_POST['getFriends'])){
        $data = $_POST['getFriends'];
        echo $api->getFriends($data);
    }
    else if (isset($_POST['getUser'])){
        $data = $_POST['getUser'];
        echo $api->getUser($data);
    }
    else if (isset($_POST['test'])){
        print_r($api->test($_POST['test']));
    }
    
    
    else if (isset($_POST['myTest'])){
        $data = $_POST['myTest'];
        echo $api->myTest($data);
    }
    
    else if (isset($_POST['sendMessage'])){
        $data = $_POST['sendMessage'];
        echo $api->sendMessage($data);
    }
    
    else if (isset($_POST['updatePass'])){
        $data = $_POST['updatePass'];
        echo $api->updatePass($data);
    }
    
    
    
    
    
    else echo "Да-да... ты (Я) опять не обновил index ! ! !";
?>