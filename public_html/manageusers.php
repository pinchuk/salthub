<?php

include_once( "../inc/inc.php" );

$perPage = 20;
$page = intval($_GET['p']);

if( isset( $_GET['action'] ) )
{
  $uid = $_GET['uid'];
  switch( $_GET['action'] )
  {
    case "resend":
    {
      $originaluid = $API->uid;
      $originalname = $_SESSION['name'];
      $API->uid = $uid;
      $_SESSION['uid'] = $uid;
      $_SESSION['name'] = quickQuery( "select name from users where uid='$uid'" );
      $API->name = $_SESSION['name'];
      $API->username = quickQuery( "select username from users where uid='$uid'" );
      $API->pic = quickQuery( "select pic from users where uid='$uid'" );
      $API->sendConfirmationEmail();

      $_SESSION['uid'] = $originaluid;
      $_SESSION['name'] = $originalname;
      $API->name = $_SESSION['name'];
      $API->username = quickQuery( "select username from users where uid='$uid'" );
      $API->pic = quickQuery( "select pic from users where uid='$uid'" );
    }
    break;

    case "confirm":
    {
      mysql_query( "update users set verified=1 where uid='$uid'" );
    }
    break;

    case "activate":
    {
      mysql_query( "update users set active=1 where uid='$uid'" );
    }
    break;

    case "unsubscribe":
    {
      $email = quickQuery( "select email from users where uid='$uid'" );
      header( "Location: /unsubscribe.php?e=" . urlencode( $email ) );
      exit;
    }
    break;

    case "delete":
    {
      mysql_query( "delete from page_members where uid='$uid'" );
      mysql_query( "delete from wallposts where uid='$uid'" );
      mysql_query( "delete from friends where id1='$uid' OR id2='$uid'" );
      mysql_query( "delete from friend_suggestions where id1='$uid' OR id2='$uid'" );
      mysql_query( "delete from comments where uid='$uid'" );
      mysql_query( "delete from contacts where uid='$uid'" );
      mysql_query( "delete from education where uid='$uid'" );
      mysql_query( "delete from fbinvites where uid='$uid'" );
      mysql_query( "delete from feed where uid='$uid'" );
      mysql_query( "delete from invites where uid='$uid'" );
      mysql_query( "delete from jobs where uid='$uid'" );
      mysql_query( "delete from jobs_saved where uid='$uid'" );
      mysql_query( "delete from likes where uid='$uid'" );
      mysql_query( "delete from messages where uid='$uid'" );
      mysql_query( "delete from network_users where uid='$uid'" );
      mysql_query( "delete from notifications where uid='$uid'" );
      mysql_query( "delete from page_blocked where uid='$uid'" );
      mysql_query( "delete from personal where uid='$uid'" );
      mysql_query( "delete from photo_tags where uid='$uid'" );
      mysql_query( "delete from video_tags where uid='$uid'" );
      mysql_query( "delete from photos where uid='$uid'" );
      mysql_query( "delete from videos where uid='$uid'" );
      mysql_query( "delete from views where uid='$uid'" );
      mysql_query( "delete from work where uid='$uid'" );
      mysql_query( "delete from users where uid='$uid' limit 1" );
    }
    break;
  }
}

include "header.php";

?>
<h1>Manage Users</h1>

<style>
td
{
border: 1px solid black; border-left: 0; padding: 10px 4px;
}
</style>

<form method="get" action="manageusers.php">
<input type="text" name="q" value="<?=$_GET['q']?>" style="width: 200px;" />
<input type="submit" class="button" value="Search" />
</form>

<table border="0" style="font-size: 9pt; border-left: 1px solid black; margin-top: 10px; width:780px; font-size:11px;" cellpadding="0" cellspacing="0">
	<tr style="background: black; color: white;">
		<td>Name</td>
		<td>E-mail</td>
		<td>Last<br />Login</td>
		<td style="text-align: center;">Administration</td>
	</tr>

<?php

$pages = ceil(quickQuery("select count(*) from users") / $perPage);

$x = mysql_query("select uid,username,name,email,phone,lastlogin,active,admin,ip,twusername,fbusername from users " . (empty($_GET['q']) ? "" : "where (name like '%" . $_GET['q'] . "%' OR email like '%" . $_GET['q'] . "%')") . " order by admin desc, joined desc, username limit " . ($page * $perPage) . ",$perPage");

$odd = false;
while ($user = mysql_fetch_array($x, MYSQL_ASSOC))
{
  if( $user['lastlogin'] != '0000-00-00 00:00:00' )
  	$user['lastlogin'] = date("m/d/y", strtotime($user['lastlogin']));
  else
    $user['lastlogin'] = '(None)';

//	$user['admin'] = $user['admin'] == "1" ? '<img src="/images/gavel.png" alt="" title="Admin" />' : "";


	if ($user['active'] == "-1") //banned
	{
		$user['active'] = '<img src="/images/x.png" alt="" title="Banned" />';
		$banHTML = '<a href="javascript:void(0);" onclick="javascript:banUser(\'' . $user['name'] . '\', \'' . $user['uid'] . '\', 1);"><img src="/images/keys.png" alt="" title="Unban User" /></a>';
	}
	else
	{
		$banHTML = '<a href="javascript:void(0);" onclick="javascript:banUser(\'' . $user['name'] . '\', \'' . $user['uid'] . '\', -1);"><img src="/images/lock.png" alt="" title="Ban User" /></a>';
		if ($user['active'] == "0")
			$user['active'] = '<img src="/images/inactive.png" alt="" title="Inactive" />';
		else
			$user['active'] = '<img src="/images/check.png" alt="" title="Active" />';
	}


	$user['ip'] = '<a href="javascript:void(0);" onclick="javascript:getLocation(\'' . long2ip($user['ip']) . '\');"><img src="/images/globe.png" alt="" title="Location for ' . long2ip($user['ip']) . '" /></a>';

	$odd = !$odd;

  $color = $odd ? "ddd" : "eee";
  if( $user['admin'] ) $color = "BBDD88";
	?>
  <tr style="background-color: #<?=$color;?>;">
    <td width="150">
      <a href="<? echo $API->getProfileURL($user['uid']); ?>"><?=$user['name']; ?></a>
      <div style="float:right">
      <?
      if( $user['fbusername'] != NULL ) echo ' <img src="/images/facebook.png" width="16" height="16" alt="" />';
      if( $user['twusername'] != NULL ) echo ' <img src="/images/twitter.png" width="16" height="16" alt="" />';
      ?>
      </div>
    </td>
    <td><a href="compose.php?uid=<?=$user['uid']?>&manageusers=1"><img src="../images/message.png" width="16" height="16" alt="" /></a> <a href="mailto:<?=$user['email']; ?>"><?=$user['email']; ?></a></td>
    <td align="center"><?=$user['lastlogin']?></td>

  	<td width="275">
        <div style="float:left;">
          <select name="action" onchange="javascript: if( selectedValue( this ) != '' && confirm( 'Are you sure?' ) ) window.location=selectedValue(this);" style="font-size:9pt;">
          <option value="">(Select Action)</option>
          <option value="manageusers.php?p=<?=$_GET['p'];?>&action=resend&uid=<? echo $user['uid']; ?>">Resend Confirmation Email</option>
          <option value="manageusers.php?p=<?=$_GET['p'];?>&action=confirm&uid=<? echo $user['uid']; ?>">Confirm User</option>
          <option value="manageusers.php?p=<?=$_GET['p'];?>&action=activate&uid=<? echo $user['uid']; ?>">Activate User</option>
          <option value="manageusers.php?p=<?=$_GET['p'];?>&action=delete&uid=<? echo $user['uid']; ?>">Delete User</option>
          <option value="manageusers.php?p=<?=$_GET['p'];?>&action=unsubscribe&uid=<? echo $user['uid']; ?>">Unsubscribe User</option>
          </select> &nbsp;
        </div>

        <div style="padding-top:3px; float:left;">
          <a href="masq.php?uid=<?=$user['uid']?>"><img src="/images/door_in.png" alt="" title="Login as this user" /></a>&nbsp;
          <?=$user['ip']; ?>&nbsp;
          <?=$banHTML?>&nbsp;
          <?=$user['active']; ?>
        </div>
    </td>
  </tr>
	<?
}

?>

</table>

<div style="text-align: center; margin-top: 10px; font-size: 9pt; width:730px;">
<?php
for ($i = 0; $i < $pages; $i++)
	if ($i == $page)
		echo "&nbsp;" . ($i + 1);
	else
		echo ' <a href="manageusers.php?q=' . $_GET['q'] . '&p=' . $i . '">' . ($i + 1) . '</a>';
?>
</div>

<script>
function getLocation(ip)
{
	getAjax("getlocation.php?ip=" + ip, "getLocationHandler");
}

function getLocationHandler(data)
{
	alert(data);
}

function banUser(uname, uid, active)
{
	if (confirm("Are you sure you want to " + (active == 1 ? "un" : "") + "ban " + uname + "?"))
		postAjax("/settings/setactive.php", "uid=" + uid + "&active=" + active, "banUserHandler");
}

function banUserHandler(data)
{
	location.reload(true);
}
</script>

<?php include "footer.php"; ?>