var current_image = 0;
var max_images = 0;
var album_images = new Array();
var current_type = 0;
var current_id = 0;
var previous_url = "";

function showMediaPopup( id, type )
{
  previous_url = new String(window.location);

  getAjax( "/media_popup.php?id=" + id + "&type=" + type, mediaPopupOnLoad );
  //showPopupUrl( "/media_popup.php?id=" + id + "&type=" + type, mediaPopupLoaded );
  current_id = id;
  current_type = type;
}

function mediaPopupOnLoad( response )
{
  data = response.split( String.fromCharCode(1) );
  var fplayerInit = response.split( String.fromCharCode(2) );
  
  max_images = parseInt( data[0] );  
  
  if( data[1] != '' && typeof data[1] != undefined ) 
  {        
    eval( data[1] );
  }
  
  showPopUp("", data[2], 0, 1, 0, mediaPopupClosed );
  mediaPopupLoaded();
  if(fplayerInit[1] != '' && typeof fplayerInit[1] != undefined){
      eval(fplayerInit[1]);
  }
}

function mediaPopupClosed()
{    
  history.replaceState({},"",previous_url);
}

function mediaPopupLoaded()
{
  switch( current_type )
  {
    case "P":
      load_bottom_and_right( album_images[ current_image ], 'P' );
      $('#ppy1').popeye();
    break;
    
    case "V":
      load_bottom_and_right( current_id, 'V' );
    break;
  }
}

function changeImage( increment, type )
{
  current_image = current_image + parseInt( increment );
  if( current_image >= max_images )
    current_image = 0;
  if( current_image < 0 )
    current_image = max_images - 1;

  load_bottom_and_right( album_images[ current_image ], type );
}

function load_bottom_and_right( ida, type )
{
  getAjax( "/media_tags_and_description.php?id=" + ida + "&type=" + type, bottom_loaded );
  getAjax( "/media_popup_right.php?id=" + ida + "&type=" + type, right_loaded );
}

function bottom_loaded( data )
{
  e = document.getElementById( "media_popup_bottom" );

  if( e )
    e.innerHTML = data;

	getAllLikes();
}

function right_loaded( data )
{
  e = document.getElementById( "media_popup_right" );

  if( e )
    e.innerHTML = data;
	
	getAllLikes();
}