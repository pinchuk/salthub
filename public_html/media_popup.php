<?
include_once( "inc/inc.php" );

if( !$API->isLoggedIn() )
{
  echo "0" . chr(1) . chr(1);
  include( "signup/login_redirect_popup.php" );
  exit;
}


//Maximum video dimensions
$maxWidth = 830;
$maxHeight = 530;

$id = intval( $_GET['id'] );
$type = $_GET['type'];

switch( $type )
{
  case "P":
    $sql = "select * from photos where id='$id'";
    $photo = queryArray( $sql );
    
    $album_id = $photo['aid'];
    $album_info = queryArray( "select * from albums where id='$album_id'" );

    $title = $photo['ptitle'];
    if( $title == "" ) $title = $album_info['title'];
          
    $sql = "select *, if( $id=id, 1, 0 ) as matched, width, height from photos where aid='$album_id' order by matched desc, id";
    $q = mysql_query( $sql );    
        
    $q2 = mysql_query( $sql );

    echo mysql_num_rows( $q2 ) . chr(1);
    echo 'album_images = new Array(';
    
    $c = 0;
    while( $r2 = mysql_fetch_array($q2) )
    {
      if( $c > 0 ) echo ",";
      echo '"' . $r2['id'] . '"';
      $c++;
    }
    
    echo '); history.replaceState({},\'\',\''. $API->getMediaURL($type, $id, $title) . '\');' . chr(1);
  break;
  
  case "V":
    $title = quickQuery( "select title from videos where id='$id'" );
    echo "0" . chr(1) . 'history.replaceState({},\'\',\'' . $API->getMediaURL($type, $id, $title) . '\');' . chr(1);
  break;
}  


?>
<div style="width:1200px; height:720px;">
<?
switch( $type ) 
{
  case "P":
?>
  <div style="float:left; width:830px; height:580px;">
    <div class="ppy" id="ppy1">

      <ul class="ppy-imglist">
<?
        while( $r = mysql_fetch_array( $q ) )
        {
          $photo_url = "/img/N635x655/photos/" . $r['id'] . "/" . $r['hash'] . ".jpg";
          $title = $r['ptitle'];
          if( $title == "" ) $title = $album_info['title'];
          
          $dimensions = "";
          
          if( $r['width'] > $r['height'] && $r['width'] > 830 )
          {
            $dimensions .= ' width="830"';
          }
          else if( $r['height'] > 580 )
          {
            $width = 580 * ($r['width'] / $r['height']);
            $dimensions .= ' height="580" width="' . $width . '"';
          }
       
?>
          <li><a href="<?=$API->getMediaURL($type, $r['id'], $title);?>"><img src="<?=$photo_url;?>" alt="<?=$title?>"<?=$dimensions?>/></a></li>
<?
        }
?>
      </ul>

      <div class="ppy-outer">
          <div class="ppy-stage">
              <div class="ppy-nav">
                  <div class="nav-wrap">
                      <a class="ppy-prev" title="Previous image" style="float:left;" onclick="javascript:changeImage(-1,'<?=$type?>');">Previous image</a>
                      <a class="ppy-next" title="Next image" style="float:right;" onclick="javascript:changeImage(1,'<?=$type?>');">Next image</a>
                  </div>
              </div>
          </div>
      </div>
    </div>
  </div>
  <?
  break;
  
  case "V":
    $sql = "select * from videos where id='$id'";
    $media = queryArray( $sql );
    
    $error = "";
    
    if( $media['width'] == "" || $media['title'] == "" )
    {
      $error = "The requested media has not finished processing yet";
    }
    
    $ratio = $media['width'] / $media['height'];
    
    if ($ratio > 1) //wider than tall
    {
      $newWidth = $maxWidth;
      $newHeight = ceil($newWidth / $ratio);
      
      if ($newHeight > $maxHeight)
      {
        $newHeight = $maxHeight;
        $newWidth = ceil($newHeight * $ratio);
      }
    }
    else //taller than wide or square
    {
      $newHeight = $maxHeight;
      $newWidth = ceil($newHeight * $ratio);
      
      if ($newWidth > $maxWidth)
      {
        $newWidth = $maxWidth;
        $newHeight = ceil($newWidth / $ratio);
      }
    }    
    
    $isYouTube = (strlen($media['hash']) < 32);
    
    if( $isYouTube )
    {
      $newWidth = 830;
      $newHeight = 500;
    }
?>
    <div style="float:left; width:830px; height:580px; background-color:rgb(237,239,244);">
      <div style="margin-left:auto; margin-right:auto; width:<?=$newWidth?>px; padding-top:<?=round( (580-$newHeight)/2)?>px;">
      <?
      if( $error != "" )
      {
        echo "This media has not finished processing yet.  Please check back in a few minutes.";
      }
      else
      {
        if (!$isYouTube) //Regular video
        {
          showVideoPlayer($newWidth, $newHeight, $media, "popup");
        }
        else //youtube video
        {
          //$size = array($maxWidth, round($maxWidth / (640 / 360)));
          $size = array( $newWidth, $newHeight );
        ?>
         <object width="<?=$size[0]?>" height="<?=$size[1]?>" style="z-index: 990;">
            <param name="movie" value="https://www.youtube.com/v/<?=$media['hash']?>&hl=en_US&fs=0&rel=0&"></param>
            <param name="allowFullScreen" value="true"></param>
            <param name="allowscriptaccess" value="always"></param>
            <param name="wmode" value="transparent" />
            <embed src="https://www.youtube.com/v/<?=$media['hash']?>&hl=en_US&fs=1&rel=0&" type="application/x-shockwave-flash" allowscriptaccess="always" wmode="transparent" allowfullscreen="true" width="<?=$size[0]?>" height="<?=$size[1]?>" style="z-index: 990;">
            </embed>
          </object>
        <?php
        }          
      }      
      ?>
      </div>
    </div>
<?
  break;
}
?>
  <div style="float:right; width:350px; padding-left:5px;" id="media_popup_right">

  </div>

  <div style="float:left; clear:left; width:830px; padding-top:2px;" id="media_popup_bottom">

  </div>


  <div style="clear:both;"></div>
</div>

