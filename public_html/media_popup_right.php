<?
/*
This script renders the information below a photo or video in media.php and media_popup.php.  This script
can be included in a file, or called via AJAX.
*/

include_once( "inc/inc.php" );
include_once( "inc/mod_comments.php" );

$id = $_GET['id'];
$type = $_GET['type'];

switch( $type )
{
  case "P":
    $sql = "select photos.uid, ifnull(pdescr, descr) as descr, ifnull(ptitle, title) as title, photos.created from photos inner join albums on albums.id=photos.aid where photos.id='$id'";
    $on_page = quickQuery("select on_page from photos where id = $id");
  break;
  
  case "V":
    $sql = "select uid, descr, title, created from videos where id='$id'";
    $on_page = quickQuery("select on_page from videos where id = $id");
  break;
}

$media = queryArray( $sql );
echo mysql_error();

$media['name'] = quickQuery( "select name from users where uid='" . $media['uid'] . "'" );
$profile_url = $API->getProfileURL($media['uid']);

if ($on_page > 0){
    $page_name = quickQuery("select gname from pages where gid = $on_page");
}
?>

<div style="width:370px;">

  <div style="float: left;">
	<div class="mediabox" style="width:340px; margin-bottom:5px;">
		<div style="float: left; width: 50px;">
            <? if (!isset($on_page) || $on_page == 0){ ?>
                <a href="<?=$profile_url; ?>"><img width="48" height="48" src="<?=$API->getThumbURL(1, 48, 48, $API->getUserPic($media['uid']));?>" alt="" /></a>
            <? } else { ?>
                <a href="<?=$API->getPageURL($on_page)?>"><img height="48" width="48" src="<?=$API->getThumbURL(1, 48, 48, $API->getPageImage($on_page))?>" alt="" /></a>
            <? } ?>
		</div>
		<div style="float: left; padding-left: 8px;">
			<div class="mediatitle" style="margin-left:5px; width:270px;">
				<?=$media['title']?>
			</div>
		</div>
    <div style="clear:both; width:340px; padding-top:5px;" class="mediasubtitle">
        <? if (!isset($page_name)) { ?>
            <span>by: <a href="<?=$profile_url;?>"><?=$media['name']?></a></span>
        <? } else { ?>
            <span>by: <a href="<?=$API->getPageURL($on_page)?>"><?=$page_name?></a><?php if ($site == "s" && $API->isLoggedIn() && $media['uid'] != $API->uid) { ?><?php echo " &nbsp;|&nbsp; " . friendLink($media['uid']); } ?></span>
        <? } ?>
      <span><?=formatDate($media['created'])?></span>
    </div>

    <div style="clear: both;"></div>
    
    <? 
    $comments_height = 400;
    
    $link = intval( $id );
    $p = 0; //Limit the number of people to view here.
    
    ob_start();    
    include( "alsoviewedby.php" ); 
    $html = ob_get_contents();
    ob_end_clean();
    
    if( trim( $html ) != "" )
    {
      $comments_height = 320;
    ?>
    <div style="width:90%; margin-left:auto; margin-right:auto; border-bottom: 1px solid rgb(204,204,204); margin-top:5px; margin-bottom:5px;">
    </div>    
        
    <div class="userpics alsoviewed" id="alsoviewedby" style="border-bottom:0px; padding: 5px 0px 0px 0px;">
      <? echo $html; ?>
    </div>
    <? } 
    $id = intval( $_GET['id'] );
    ?>
	</div>

  <div class="media_popup_comments" style="width:350px; height:<?=$comments_height;?>px; overflow-y:auto; overflow-x:hidden; margin-bottom:5px;">
  <?    
    showCommentsArea( $type, $id, "", false, 4 );
  ?>
  </div>    

  <?
  if( $API->adv )
  {
  ?>
    <div class="subhead" style="padding-top:0px; margin: 0px 0 5px;"><div style="float:left;">Sponsors</div><div style="float:right;"><a href="http://www.salthub.com/adv/create.php" style="font-size:8pt; font-weight:300; color:rgb(0, 64, 128);">create an ad</a>&nbsp;</div><div style="clear:both;"></div></div>
  <?
    showAd("companion");
  }
  ?>
</div>