<?php
$scripts[] = "/gettrending.js.php";
$scripts[] = "/mod_media.js";
$title = "Popular Media | SaltHub - Trending Photos and Videos";
include "header.php";
?>

<div class="bigtext2" style="clear: both; padding: 0 0 5px 5px;">Popular Media</div>

<div style="border: 1px solid #d8dfea; padding: 5px;">
	<div style="float: left; width: 642px;">
		<div style="width: 630px;">
			<div style="float: left;"><img src="/images/application_view_list.png" alt="" /></div>
			<div style="float: left; font-size: 9pt; position: relative; padding-right: 15px; margin-right: 10px; border-right: 1px solid #d8dfea;">
				<div style="cursor: default;" onmouseover="javascript:if (ddTimer) clearTimeout(ddTimer); document.getElementById('view-dd').style.display = '';" onmouseout="javascript:ddTimer = setTimeout('document.getElementById(\'view-dd\').style.display=\'none\';', 200);">
					<div style="width: 45px; padding-left: 3px; float: left;"><span id="view-chosen">View &#0133;</span></div> &nbsp;<img src="/images/down.png" alt="" />
					<div style="display: none;" class="dropdown viewdd" id="view-dd">
						<a href="/search_m.php?o=r">Most Recent</a><br />
						<a href="/search_m.php?o=v">Most Viewed</a><br />
						<a href="/search_m.php?o=of">Of Friends</a><br />
						<a href="/search_m.php?o=bf">By Friends</a>
					</div>
				</div>
			</div>
			<div class="toplink"><a href="javascript:void(0);" onclick="javascript:showTweetBox();"><img src="/images/television_add.png" alt="" />Add Videos</a></div>
			<div class="toplink"><a href="javascript:void(0);" onclick="javascript:showTweetBox();"><img src="/images/image_add.png" alt="" />Add Photos</a></div>
			<div class="toplink"><a href="<?=$API->getProfileURL()?>/videos"><img src="/images/television.png" alt="" />My Videos</a></div>
			<div class="toplink"><a href="<?=$API->getProfileURL()?>/photos"><img src="/images/image.png" alt="" />My Photos</a></div>

			<div style="clear: both; height: 8px;"></div>

			<div class="subhead">Trending and Active</div>
			<div style="padding: 10px 0; position: relative;">
				<div id="marquee-parent" style="position: relative; overflow: hidden; height: 120px; width: 636px;">
					<?php
					$floatWidth = 106;
					$left = -$floatWidth;
					for ($i = 0; $i < 7; $i++)
					{
						echo '<div class="marqueefloat" style="left: ' . $left . 'px; top: 0px;" id="marquee-' . $i . '">';
						echo '	<div id="marquee-img-' . $i . '"></div>';
						echo '	<div class="title" id="marquee-title-' . $i . '"></div>';
						echo '</div>';
						$left += $floatWidth;
					}
					?>
					<div style="clear: both;"></div>
				</div>
			</div>

			<div class="subhead">Recently Viewed Media by Category</div>
		</div>

<div class="mh_recent">
<?php

$query = "
select media.*,users.container_url from (

select @row_num := 0, @categ_num := 0,type,catname,last_viewed,title,categ_id,item_id,item_seq_num,uid,hash,industry from
(
SELECT
    'V' as `type`, last_viewed,
	catname, title,
	 ext_vw.categ_id,
    ext_vw.item_id,
    ext_vw.item_seq_num,uid,hash,industry
FROM
               (
                 SELECT last_viewed,title,
                              ilvw.categ_id,
                              ilvw.item_id,uid,hash,
                              @row_num :=  CASE WHEN @categ_num = ilvw.categ_id THEN  @row_num + 1
                                                                                                          ELSE 1
                                                                                           END AS item_seq_num,
                              @categ_num := ilvw.categ_id AS categ_num
                              FROM
                              (
                              SELECT max(media_views.ts) as last_viewed,
                                               c.cat   AS  categ_id,
                                               cr.id  AS  item_id,
                                               title,uid,hash,
                                               ts
                              FROM
                                              `categories` c,
                                              `videos` cr
										inner join media_views on media_views.id=cr.id
                              WHERE
                                               c.cat = cr.cat and cr.privacy = 0 and media_views.type='V'
                              group by media_views.id
                              ORDER BY
                                               c.catname ASC,
                                               max(media_views.ts) desc
                              ) ilvw
               ) ext_vw
               inner join categories on categ_id=categories.cat
               WHERE
                              item_seq_num <= 2
                              ) tmp1
union all (

select @row_num := 0, @categ_num := 0,type,catname,last_viewed,title,categ_id,item_id,item_seq_num,uid,hash,industry from
(
SELECT
    'P' as `type`, last_viewed,uid,hash,
	catname, title,
	 ext_vw.categ_id,
    ext_vw.item_id,
    ext_vw.item_seq_num,industry
FROM
               (
                 SELECT last_viewed,title,uid,hash,
                              ilvw.categ_id,
                              ilvw.item_id,
                              @row_num :=  CASE WHEN @categ_num = ilvw.categ_id THEN  @row_num + 1
                                                                                                          ELSE 1
                                                                                           END AS item_seq_num,
                              @categ_num := ilvw.categ_id AS categ_num
                              FROM
                              (
                              SELECT max(media_views.ts) as last_viewed,uid,
                                               c.cat   AS  categ_id,
                                               cr.id  AS  item_id,
                                               ptitle as title,hash,
                                               ts
                              FROM
                                              `categories` c,
                                              `photos` cr
										inner join media_views on media_views.id=cr.id
                              WHERE
                                               c.cat = cr.cat and cr.privacy = 0 and media_views.type='P'
                              group by media_views.id
                              ORDER BY
                                               c.catname ASC,
                                               max(media_views.ts) desc
                              ) ilvw
               ) ext_vw
               inner join categories on categ_id=categories.cat
               WHERE
                              item_seq_num <= 2
                              ) tmp2

) 

) media
inner join users on media.uid=users.uid
where industry > -1 and categ_id not in (1299,2)
order by industry,catname,last_viewed desc
";

$x = mysql_query($query);

$current_category = -1;
$current_industry = -1;
while ($media = mysql_fetch_array($x, MYSQL_ASSOC))
{	
	if ($current_category != $media['categ_id'])
	{
		if ($current_category > -1)
			echo '</ul></div>'; //end class="category" div
		
		if ($current_industry != $media['industry'])
		{
			if ($media['industry'] == 0)
				echo '<h2>General Categories</h2>';
			else
				echo '<h2>Vessel Categories</h2>';
			
			$current_industry = $media['industry'];
		}
		
		echo '<div class="category">';
		echo '<h3><a href="/search_m.php?cat=' . $media['categ_id'] . '">' . $media['catname'] . '</a></h3>';
		echo '<ul>';
		
		$current_category = $media['categ_id'];
		$items_in_category = 0;
	}
	
	if ($items_in_category++ < 2)
	{
		$url = $API->getMediaURL($media['type'], $media['item_id'], $media['title']);
		echo '<li><a href="' . $url . '">';
		echo '<img src="' . $media['container_url'] . '/' . $media['hash'] . ($media['type'] == 'P' ? '_wide' : '') . '.jpg" alt>';
		echo '<span>' . $media['title'] . '</span></a></li>';
	}
}

echo '</ul></div>'; //end class="category" div

?>
</div>
		
	</div>

	<div style="float: right; width: 300px;">
<!--		<form action="" onsubmit="return false;"><input type="text" style="width: 252px;" id="txtsearch1" onkeypress="javascript:searchKeypress(event);" onfocus="javascript:if (this.value=='Search ...') this.value='';" onblur="javascript:if (this.value=='') this.value='Search ...';" class="txtsearch" value="Search ..." /><input type="text" onclick="javascript:searchDo();" onfocus="document.getElementById('txtsearch1').focus();" name="q" class="btnsearch" value="" /></form>-->

		<div style="clear: both; height: 6px;"></div>

		<?php  if( $API->adv ) { showAd("greeninfo"); ?>

		<div class="subhead" style="margin: 10px 0 5px;"><div style="float:left;">Sponsors</div><div style="float:right;"><a href="http://www.salthub.com/adv/create.php" style="font-size:8pt; font-weight:300; color:rgb(0, 64, 128);">create an ad</a>&nbsp;</div><div style="clear:both;"></div></div><?php showAd("companion"); ?>
		<div style="clear: both; height: 10px;"></div>

    <? } ?>

    <div class="subhead" style="margin-top:20px; margin-bottom:10px;">Recent and New</div>
		<?php
		include "inc/mod_media.php";
		showMediaModule();
		?>

		<?php  if( $API->adv ) { ?>
		<div class="subhead" style="margin: 10px 0 5px;"><div style="float:left;">Sponsors</div><div style="float:right;"><a href="http://www.salthub.com/adv/create.php" style="font-size:8pt; font-weight:300; color:rgb(0, 64, 128);">create an ad</a>&nbsp;</div><div style="clear:both;"></div></div><?php showAd("companion"); ?>
    <? } ?>		
		<?php //include "inc/pymk.php"; ?>
	</div>

	<div style="clear: both;"></div>
</div>

<script language="javascript" type="text/javascript">
<!--

var floatOrder = [0,1,2,3,4,5,6];
var floatMedia = new Array();
var showingMedia = "";

for (i = 0; i < 6; i++)
{
	changeMarqueeItem(i + 1, mediaQueue[i]);
	showingMedia += "," + mediaQueue[i].type + mediaQueue[i].id;
}

mediaQueue = [];

function changeMarqueeItem(i, obj)
{
	floatMedia[i] = obj;
	document.getElementById("marquee-img-" + i).innerHTML =  '<a href="' + obj.url + '"><img src="' + obj.img + '" alt="" /></a>';
	document.getElementById("marquee-title-" + i).innerHTML = '<a href="' + obj.url + '">' + obj.title + '</a>';
	document.getElementById("marquee-" + i).style.visibility = "visible";
}

var checkedTimes = 0;
function checkForNewMedia()
{
	if (mediaQueue.length > 0)
	{
		checkedTimes = 8;
		obj = mediaQueue.pop();
		changeMarqueeItem(floatOrder[0], obj);
		scrollMarquee();
	}
	else if (checkedTimes < 8)
		checkedTimes++;
	else
	{
		//alert("LOading new");
		checkedTimes = 0;
		loadjscssfile("/gettrending.js.php?t=" + lastUpdated + "&notIn=" + showingMedia.substring(1), "js");
	}
}

function scrollMarquee()
{
	lastFloat = floatOrder.pop();
	showingMedia = "";

	for (i = 0; i < 7; i++)
	{
		e = document.getElementById("marquee-" + i);
		l = parseInt(e.style.left.substring(0, e.style.left.length - 2));
		t = new Tween(e.style, "left", Tween.regularEaseInOut, l, l + <?=$floatWidth?>, 1, "px");
		if (i == lastFloat)
			t.onMotionFinished = function() { document.getElementById("marquee-" + lastFloat).style.left = "-<?=$floatWidth?>px"; }
		else if (typeof floatMedia[i] != "undefined")
			showingMedia += "," + floatMedia[i].type + floatMedia[i].id;
		t.start();
	}

	floatOrder.unshift(lastFloat);
}

var mediaOwner = <?=$API->uid?>;
showMedia("browse", "new", "", 0);

setInterval("checkForNewMedia()", 2500);

//-->
</script>

<?php
include "footer.php";
?>