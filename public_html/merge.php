<?php

include "inc/inc.php";

if (!isset($_SESSION['merge']))
{
	header("Location: /");
	die();
}

$merge = $_SESSION['merge'];
$isFB = isset($merge['fbid']);

include "header.php";

//header unsets
$_SESSION['merge'] = $merge;

?>

<div style="padding: 50px; border: 1px solid #555; text-align: center; font-weight: bold;">
Your <?=$isFB ? "Facebook" : "Twitter"?> account is already tied to another <?=$siteName?> account.<p/>
<a href="/merge_do.php">Merge accounts</a>
<br /><br />
<a href="/">Cancel</a>
</div>

<?php include "footer.php"; ?>
