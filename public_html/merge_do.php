<?php

include "inc/inc.php";

if (!$API->isLoggedIn() || !isset($_SESSION['merge'])) mergeError();

$merge = $_SESSION['merge'];

if (isset($merge['fbid'])) //merge fb with user's tw acct (e.g. user is already signed in under twitter and wants to merge his facebook account with the twitter account)
{
	$uidOld = intval(quickQuery("select uid from users where fbid=" . $merge['fbid']));
	if ($uidOld == 0) mergeError();
	sql_query("update users set fbid=" . $merge['fbid'] . ",fbsess='" . $merge['fbsess'] . "',fbusername='" . $merge['fbusername'] . "' where uid=" . $API->uid);
  $API->feedAdd("J", SITE_FACEBOOK, $API->uid, $API->uid );
}
else //merge tw with user's fb acct
{
	$uidOld = quickQuery("select uid from users where twid=" . $merge['twid']);
	if ($uidOld == 0) mergeError();
	sql_query("update users set twid=" . $merge['twid'] . ",twusername='" . $merge['twusername'] . "',twtoken='" . $merge['access_token']['oauth_token'] . "',twsecret='" . $merge['access_token']['oauth_token_secret'] . "' where uid=" . $API->uid);
  $API->feedAdd("J", SITE_TWITTER, $API->uid, $API->uid );
}

//merge sql data
sql_query("update ignore likes set uid=" . $API->uid . " where uid=$uidOld");
sql_query("delete from likes where uid=$uidOld");
sql_query("update ignore views set uid=" . $API->uid . " where uid=$uidOld");
sql_query("delete from views where uid=$uidOld");
sql_query("update albums set uid=" . $API->uid . " where uid=$uidOld");
sql_query("update photos set uid=" . $API->uid . " where uid=$uidOld");
sql_query("update videos set uid=" . $API->uid . " where uid=$uidOld");
sql_query("update reported set uid=" . $API->uid . " where uid=$uidOld");

//Feeds
sql_query("update feed set uid=" . $API->uid . " where uid=$uidOld");
sql_query("update feed set uid_by=" . $API->uid . " where uid_by=$uidOld");

//wall posts
sql_query("update wallposts set uid=" . $API->uid . " where uid=$uidOld");
sql_query("update wallposts set uid_to=" . $API->uid . " where uid_to=$uidOld");

//Messages
sql_query("update messages set uid=" . $API->uid . " where uid=$uidOld");
sql_query("update messages set uid_to=" . $API->uid . " where uid_to=$uidOld");

//Contacts
sql_query("update contacts set uid=" . $API->uid . " where uid=$uidOld");

//Comments
sql_query("update comments set uid=" . $API->uid . " where uid=$uidOld");

//Friends
sql_query("update friends set id1=" . $API->uid . " where id1=$uidOld");
sql_query("update friends set id2=" . $API->uid . " where id2=$uidOld");
//Some of these might fail due to duplicates; the database key should prevent duplicates.
sql_query("delete from friends where id1='$uidOld'");
sql_query("delete from friends where id2='$uidOld'");

//Notifications
sql_query("update notifications set uid=" . $API->uid . " where uid=$uidOld");

//delete old user
sql_query("delete from users where uid=$uidOld");

//cross our fingers and hope it worked
header("Location: /settings?merged");

function mergeError()
{
	$_SESSION['error'] = "There was an error merging your accounts.&nbsp; Please try again later.";
	header("Location: /");
	die();
}

?>
