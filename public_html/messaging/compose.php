<?php
/*
Displays a list of user messages, brings up the compose message popup by default.
*/

include "../inc/inc.php";

$script = "inbox";
$showCompose = true;

include "inbox.php";

?>