var msgsChecked = "";

function checkMsg(id, force, off)
{
	img = document.getElementById("chk" + id);
	
	if (force)
		isChecked = off;
	else
		isChecked = img.src.endsWith("_on.png");
	
	if (isChecked)
	{
		img.src = "/images/checkbox.png";
		msgsChecked = msgsChecked.replace("," + id + ",", "");
	}
	else
	{
		img.src = "/images/checkbox_on.png";
		msgsChecked += "," + id + ",";
	}
}

function checkAll(img)
{
	isChecked = img.src.endsWith("_on.png");
	
	img.src = "/images/checkbox" + (isChecked ? "" : "_on") + ".png";
	
	arrMsgs = allMsgs.split(",");
	for (var i in arrMsgs)
		if (arrMsgs[i] != "")
			checkMsg(arrMsgs[i], true, isChecked);
}

function deleteMessages(type)
{
	arrMsgs = msgsChecked.split(",");
	for (var i in arrMsgs)
		if (arrMsgs[i] != "")
			document.getElementById("msg" + arrMsgs[i]).style.display = "none";

	postAjax("/messaging/delete.php", "mids=" + msgsChecked + "&type=" + type, "void");
}

function markUnread(mid)
{
	postAjax("/messaging/markunread.php", "mid=" + mid, "void");
	
	e = document.getElementById("markunread-" + mid);
	
	if (e){
        e.innerHTML = "Marked as unread";
    }
}

function deleteMsg(mid, type)
{
	e = document.getElementById("msg" + mid);
	
	if (e)
		e.style.display = "none";
    console.log(type);
	postAjax("/messaging/delete.php", "mids=" + mid + "&type=" + type, e ? "void" : "location='/messaging/inbox.php';void");
}