<?php
/*
Displays a list of read and unread messages.  This is for private user-to-user messages.
*/

include_once "../inc/inc.php";
$API->requireLogin();

$scripts[] = "/tipmain.js";
$scripts[] = "/messaging/inbox.js";
include "../header.php";
include "header.php";

$sent = intval($_GET['s']) + intval($_GET['sent']);

$fs = array("all" => "a", "unread" => "u", "read" => "r");
if (!in_array($_GET['f'], $fs))
	$f = "a";
else
	$f = $_GET['f'];

if ($_GET['type'] == 'message' && $_GET['s'] == 1){
    $query = "select * from compose where uid = ".$API->uid;
    $result = mysql_query($query);

    $data = array();
    $mids = array();
    while ($row = mysql_fetch_assoc($result)){
        $data[] = $row;
        $mids[] = $row['id'];
    }

    ?>
    <style>
        .arrow_box {
            position: relative;
            background: #88b7d5;
            border: 3px solid #c2e1f5;
            margin-top: 10px;
        }
        .arrow_box:after, .arrow_box:before {
            bottom: 100%;
            border: solid transparent;
            content: " ";
            height: 0;
            width: 0;
            position: absolute;
            pointer-events: none;
        }

        .arrow_box:after {
            border-color: rgba(136, 183, 213, 0);
            border-bottom-color: #88b7d5;
            border-width: 10px;
            left: 50%;
            margin-left: -10px;
        }
        .arrow_box:before {
            border-color: rgba(194, 225, 245, 0);
            border-bottom-color: #c2e1f5;
            border-width: 16px;
            left: 50%;
            margin-left: -16px;
        }
    </style>
        <!-- title -->
        <div class="subhead"> Sent Messages </div>

        <div id="messages">
            <a href="inbox.php?s=1" style="font-size: 14px">Internal messages </a>|
            <a href="inbox.php?type=message&s=1" style="font-size: 14px; font-weight: bold">External messages</a>
        </div>

        <div style="font-size: 8pt; padding-top: 5px;">
            <div style="float: left; padding-left: 5px;">
                <?= 'all ('.intval(quickQuery("select count(id) from compose where uid = ".$API->uid)).')' ?>
            </div>
            <div style="float: right; padding-right: 9px;">
                <a href="javascript:void(0);" onclick="javascript:deleteMessages(0);">delete selected messages</a> | select all/none<img src="/images/checkbox.png" onclick="javascript:checkAll(this);" alt="" style="padding: 0 4px; vertical-align: middle; cursor: pointer;" /><span style="font-size: 9pt; color: #fff;">X</span>
            </div>
            <div style="clear: both;"></div>
        </div>

        <? if (count($data) > 0){
            foreach ($data as $value){
                if (strlen($value['body']) > 100)
                    $value['body'] = substr($value['body'], 0, 100).'... <a>read more<a/>';
        ?>
        <!-- body -->
        <div class="msgpreview" id="msg<?=$value['id']?>">
            <div class="userpic">
                <img width="48" height="48" src="../images/social/48x48/manual.png" alt="" />
            </div>

            <div class="msgcontainer">
                <div class="border">
                    <div class="date" onmouseover="javascript:showDropDown('mails-<?=$value['id']?>')" onmouseout="hideDropDown('mails-<?=$value['id']?>')">
                        <?
                            $first = explode(',', $value['email']);
                            $more = '';
                            if (count($first) > 1)
                                $more = ' and '.(count($first)-1).' more';
                        ?>
                        <div style="float: left">
                            <?= date("F j, Y", strtotime($value['created'])) ?>
                        </div>
                        <div style="float: left; margin-left: 5px">
                            <? echo '<a href="mailto:'.$first[0].'"'.$first[0].'>'.$first[0].'</a>'.$more; if (isset($first[1])){ ?>
                            <div id="mails-<?=$value['id']?>" style="background: #DDDDDD; position: absolute; display: none; padding: 3px" class="arrow_box">
                                <?
                                for ($i = 1; $i < count($first); $i++)
                                    echo '<a href="mailto:'.$first[$i].'">'.$first[$i].'<br/>';
                                ?>
                            </div>
                        <? } ?>
                        </div>
                    </div>
                    <div class="icons">
                        <img src="/images/email_open.png" alt="" style="margin-bottom: 2px" />
                        <img src="/images/checkbox.png" id="chk<?=$value['id']?>" onclick="javascript:checkMsg(<?=$value['id']?>, 0, -1);" style="cursor: pointer;" alt="" />
                        <a href="javascript:void(0);" onclick="javascript:deleteMsg(<?=$value['id']?>, 0);">X</a>
                    </div>
                </div>
                <div class="msg" style="cursor: pointer" onclick="javascript:location='/messaging/read.php?mid=<?=$value['id']?>&type=0';">
                    <span style="float: left; font-weight: 700; font-size: 9pt; text-decoration: none"><?=$value['subj']?></span><br />
                    <div style="height: auto"><?=str_replace("�", "'", $value['body'])?></div>
                </div>
            </div>
            <div style="clear: both;"></div>
        </div>
    <?
            }
    }
}
else{
    $q1 = "select mid," . ($sent == 1 ? "messages.uid_to as uid, messages.uid as uid2, comments.uid as sender" : "users.uid" ). ",username,name,pic,subj,comment,unreadBy" . ($sent == 1 ? "Send" : "Recv") . " as unread,created from messages inner join users on users.uid=messages.uid inner join comments on comments.link=mid where delBy" . ($sent == 1 ? "Send" : "Recv") . " = 0 and messages.uid" . ($sent == 1 ? "" : "_to") . "=" . $API->uid . " and comments.uid " . ($sent == 1 ? "" : "!") . "= " . $API->uid . ($f == "u" || $f == "r" ? " and unreadBy" . ($sent == 1 ? "Send" : "Recv") . ($f == "u" ? " > 0" : "= 0") : "");
    $q2 = "select mid," . ($sent == 1 ? "messages.uid_to as uid, messages.uid as uid2, comments.uid as sender" : "comments.uid"). ",username,name,pic,subj,comment,unreadBy" . ($sent == 1 ? "Recv" : "Send") . " as unread,created from messages inner join comments on comments.link=mid inner join users on users.uid=comments.uid where delBy" . ($sent == 1 ? "Recv" : "Send") . " = 0 and messages.uid" . ($sent == 1 ? "_to" : "") . "=" . $API->uid . " and comments.uid " . ($sent == 1 ? "" : "!") . "= " . $API->uid . ($f == "u" || $f == "r" ? " and unreadBy" . ($sent == 1 ? "Recv" : "Send") . ($f == "u" ? " > 0" : "= 0") : "");

    $q = "from (select * from (select * from (($q1) union ($q2)) as t0) as t1 order by created desc) as t2 group by mid order by created desc";

//    $mcount[$f] = intval(quickQuery("select sum(c) from (select count(*) as c $q) as xyz"));
    $mcount[$f] = intval(quickQuery("select count(*) from (select unread as c $q) as xyz"));


foreach ($fs as $x)
	if (!isset($mcount[$x]))
		$mcount[$x] = $mcount[$f]; //$API->getMessageCount(null, $x);
?>
	<div class="subhead">
		<?=$sent == 1 ? "Sent" : "Inbox"?> Messages
	</div>

    <? if ($_GET['s'] == 1){
        ?>
        <div id="messages">
            <a href="inbox.php?s=1" style="font-size: 14px; font-weight: bold">Internal messages </a>|
            <a href="inbox.php?type=message&s=1" style="font-size: 14px">External messages</a>
        </div>
        <?
    }?>

    <!-- old -->
	<div style="font-size: 8pt; padding-top: 5px;">
		<div style="float: left; padding-left: 5px;">
			<?php
			foreach ($fs as $k => $v)
			{
				if ($f != $v)
					echo '<a href="/messaging/inbox.php?s=' . $sent . '&f=' . $v . '">';
				echo $k . ' (' . $mcount[$v] . ')';
				if ($f != $v)
					echo '</a>';
				echo '&nbsp; ';

				if ($sent == 1) break;
			}
			?>
		</div>
		<div style="float: right; padding-right: 6px;">
			<a href="javascript:void(0);" onclick="javascript:deleteMessages(1);">delete selected messages</a> | select all/none<img src="/images/checkbox.png" onclick="javascript:checkAll(this);" alt="" style="padding: 0 4px; vertical-align: middle; cursor: pointer;" /><span style="font-size: 9pt; color: #fff;">X</span>
		</div>
		<div style="clear: both;"></div>
	</div>

	<?php

	$x = mysql_query("select * $q");

	$mids = array();
	while ($msg = mysql_fetch_array($x, MYSQL_ASSOC))
	{
    if( $sent == 1 && $msg['uid'] == $msg['sender'] ) { $msg['uid'] = $msg['uid2']; }
		$mids[] = $msg['mid'];
		showMessagePreview($msg);
	}
}
	?>
<script language="javascript" type="text/javascript">
<!--
var allMsgs = ",<?=implode(",", $mids)?>,";
<?php if ($showCompose) echo "loadShare('compose', 1);\n"; ?>
//-->
</script>

<?php
include "footer.php";
include "../footer.php";
?>