<?php
/*
Displays the list of notifications to the user.
*/

include_once "../inc/inc.php";

//style = 0 is narrow
//style = 1 is wide
//style = 2 is the same as 0 except the more link goes to inbox (used in header)

if (isset($_GET['style']))
	$style = intval($_GET['style']);
elseif (isset($API))
	$style = 0;
else
	$style = 1;

$x = mysql_query("select * from notifications where uid=" . $API->uid . " order by ts desc");
?>



<div class="subhead" style="margin-top:5px; clear: both;"><span>Notifications (<span id="notifications-number"><?=$API->getNotificationsCount()?></span>)</span>
  <? if( $style == 2 ) { ?>
  <div style="float:right"><a href="" onclick="document.getElementById('notifications_container').style.display='none'; return false;"><img src="/images/bluex.png" width="18" height="18" alt="" /></a></div>
  <? } ?>
</div>

<?php if ($style == 1 || $style==3) { ?><div style="width: 250px; height: 280px; padding-right: 5px; margin-top: 5px; overflow: auto;"><?php } ?>

<?php

$i = 0;
while ($feed = mysql_fetch_array($x, MYSQL_ASSOC))
{
	$resolve = resolveFeed($feed);

  if( sizeof( $resolve ) > 0 )
  {
  	$resolved = end($resolve);

    $show = true;
  	switch ($feed['type'])
  	{
      case "f":
  			$result['uid'] = $feed['from_uid'];
  			$result['descr'] = "has requested to connect with you";
        break;

  		case "F":
  			$result['uid'] = $resolved['id1'] == $API->uid ? $resolved['id2'] : $resolved['id1'];
  			$result['descr'] = "and you are now connected";
  			break;

  		case "L":
  			$result['uid'] = $resolve[1]['uid'];
  			$result['descr'] = ($resolve[1]['likes'] == 0 ? "dis" : "") . "likes your <a href=\"" . $API->getMediaURL($resolved['type'], $resolved['id'], $resolved['title']) . "\">" . typeToWord($resolved['type']) . "</a>";
  			break;

  		case "t":
  		case "T":
  			$result['uid'] = $resolve[1]['uid'];
  			$result['descr'] = "tagged you in a <a href=\"" . $API->getMediaURL($resolved['type'], $resolved['id'], $resolved['title']) . "\">" . typeToWord($resolved['type']) . "</a>";
  			break;

      case "t2";
      case "T2";
  			$result['uid'] = $resolve[1]['uid'];
  			$result['descr'] = "tagged your <a href=\"" . $API->getMediaURL($resolved['type'], $resolved['id'], $resolved['title']) . "\">" . typeToWord($resolved['type']) . "</a>";
        break;

  		case "C":
  			$result['uid'] = $resolve[1]['uid'];
  			$also = $resolved['uid'] != $API->uid;
  			$result['descr'] = ($also ? "also" : "") . " replied to " . ($also ? "a" : "your") . " <a href=\"" . $API->getMediaURL($resolved['type'], $resolved['id'], $resolved['title']) . "\">" . typeToWord($resolved['type']) . "</a>";
  			break;
  			//echo "<xmp>";print_r($resolve);die();

      case "S":
        $result['uid'] = $resolved['uid'];
  			$result['descr'] = "suggested a <a href=\"javascript:void(0);\" onclick=\"javascript:showFriendsPopup(" . $API->uid . ");\">connection</a>";
      break;

      case "M":
        $result['uid'] = $resolved['uid'];
        if( $resolved['uid'] == $API->uid ) $result['uid'] = $resolved['uid_to'];
  			$result['descr'] = 'sent you a <a href="/messaging/read.php?mid=' . $feed['id'] . '">message</a>';
      break;

      case "G":
        $result['uid'] = $feed['from_uid'];
  			$result['descr'] = "added you to the page <a href=\"" . $API->getPageURL( $resolved['gid'] ) . "\">" . $resolved['gname'] . "</a>";
      break;

      case "G":
        $result['uid'] = $feed['from_uid'];
  			$result['descr'] = "added you to the page <a href=\"" . $API->getPageURL( $resolved['gid'] ) . "\">" . $resolved['gname'] . "</a>";
      break;

      case "P":
      case "V":
        $result['uid'] = $feed['from_uid'];
        $word = typeToWord( $feed['type'] );
        $result['descr'] = "replied to the $word <a href=\"" . $API->getMediaUrl( $resolved['type'], $resolved['id'], $resolved['title'] ) . "\">" . $resolved['title'] . "</a>";

        if( $resolved['title'] == "" ) $show = false;
      break;

      case "G1":
        $result['uid'] = $feed['from_uid'];
        $result['gid'] = $resolved['gid'];
  			$result['descr'] = "added a comment to the page <a href=\"" . $API->getPageURL( $resolved['gid'] ) . "\">" . $resolved['gname'] . "</a>";
      break;

      case "G2":
        $result['uid'] = $feed['from_uid'];
  			$result['descr'] = "joined the page <a href=\"" . $API->getPageURL( $resolved['gid'] ) . "\">" . $resolved['gname'] . "</a>";
      break;

      case "GV":
        $result['uid'] = $feed['from_uid'];
        $result['gid'] = $resolved['gid'];
  			$result['descr'] = "added a <a href=\"" . $API->getMediaURL('V', $feed['id2'] ) . "\">video</a> to the page <a href=\"" . $API->getPageURL( $resolved['gid'] ) . "\">" . $resolved['gname'] . "</a>";
      break;

      case "GP":
        $result['uid'] = $feed['from_uid'];
        $result['gid'] = $resolved['gid'];
  			$result['descr'] = "added a <a href=\"" . $API->getMediaURL('P', $feed['id2'] ) . "\">photo</a> to the page <a href=\"" . $API->getPageURL( $resolved['gid'] ) . "\">" . $resolved['gname'] . "</a>";
      break;

      case "G3":
        $result['uid'] = $feed['from_uid'];
  			$result['descr'] = "made you an admin of the page <a href=\"" . $API->getPageURL( $resolved['gid'] ) . "\">" . $resolved['gname'] . "</a>";
      break;

      case "G4":
        $result['uid'] = $feed['from_uid'];
  			$result['descr'] = "claimed the page <a href=\"" . $API->getPageURL( $resolved['gid'] ) . "\">" . $resolved['gname'] . "</a>";
      break;

      case "G5":
        $result['uid'] = 0;
  			$result['descr'] = "posted a new <a href=\"/employment/jobs_detail.php?d=" . $feed['id2'] . "\">job opening</a>.";
      break;

      case "G6":
        $page = $API->getPageInfo( $resolved['gid'] );
        $result['uid'] = $feed['from_uid'];
  			$result['descr'] = "added you to the page <a href=\"" . $API->getPageURL( $resolved['gid'] ) . "\">" . $resolved['gname'] . '</a>, <a href="javascript:void(0);" id="join-a" onclick="javascript:joinPage(' . $resolved['gid'] . ',\'a\', \'' . $page['gtype'] . '\' );" >connect</a> to confirm.';
      break;

      case "VP1":
      case "VP2":
      case "VP3":
        $result['gid'] = $feed['id'];
        $result['uid'] = 0;
        $result['descr'] = $resolved['descr'];
      break;

  		default:
        $show = false;
  			$result['descr'] = "can't handle type '{$feed['type']}'";
  			break;
  	}

    if( $result['uid'] > 0 ) {
    	$result['uid_info'] = getUserInfo($result['uid']);
    	$result['descr'] = "<a href=\"{$result['uid_info']['profileurl']}\">{$result['uid_info']['name']}</a> " . $result['descr'];
    }
    else
    {
    	$result['uid_info'] = $API->getPageInfo($result['gid']);
      $result['uid_info']['profilepic'] = $result['uid_info']['profile_pic'];
      $result['uid_info']['profileurl'] = $result['uid_info']['url'];
      $result['uid_info']['name'] = $result['uid_info']['gname'];
      $result['descr'] = "<a href=\"{$result['uid_info']['profileurl']}\">{$result['uid_info']['name']}</a> " . $result['descr'];
    }

    if( $show ) {
  	?>

  	<div class="notification<?=$style == 1 ? "2" : ""?>" id="notification-<?=$feed['nid']?>" style="<?=$style == 1 && $i == 0 ? "border-top: 0;" : ""?>">
  		<div class="pic">
  			<a href="<?=$result['uid_info']['profileurl']?>">
  				<img src="<?=$API->getThumbURL(1, $style == 1 ? 48 : 24, $style == 1 ? 48 : 24, $result['uid_info']['profilepic'])?>" alt="" />
  			</a>
  		</div>
  		<div class="text">
  			<?=$result['descr']?>
  			<?php if ($style == 1) echo "<br />" . getTimeAgo(strtotime($feed['ts'])); ?>
  		</div>
  		<div style="float: right; text-align: right;">
  			<a href="javascript:void(0);" onclick="javascript:deleteNotification(<?=$feed['nid']?>);">X</a>
  		</div>
  		<div class="clear"></div>
  	</div>

  	<?php
      $i++;
      if ($i == 8 && ($style == 2 | $style==NULL) ) break;
    }
  }

}

if ( ($numNotifications > 8 && $style == 2))
{
	if ($style == 2)
		echo '<div style="text-align: center; font-size: 8pt; clear: both; padding-top: 5px;"><a href="/messaging/inbox.php">more &#0133;</a></div>';
	elseif ($style != 1)
		echo '<div style="text-align: center; font-size: 8pt; clear: both; padding-top: 5px;"><a href="javascript:void(0);" onclick="javascript:showNotifications();">more &#0133;</a></div>';
}

if ($style == 1)
	echo '</div>';


function getUserInfo($uid)
{
	global $users, $API;

	if (!isset($users[$uid])) //info about user is not cached
	{
		$users[$uid] = $API->getUserInfo($uid, "username,name,pic,fbid,twusername,uid");
		$users[$uid]['profileurl'] = $API->getProfileURL($users[$uid]['uid'], $users[$uid]['username']);
		$users[$uid]['profilepic'] = $API->getUserPic($users[$uid]['uid'], $users[$uid]['pic']);
	}

	return $users[$uid];
}

?>

