<?php
/*
Displays a user-to-user private message.
*/

include "../inc/inc.php";
$API->requireLogin();

$scripts[] = "/tipmain.js";
$scripts[] = "/comments.js";
$scripts[] = "/messaging/inbox.js";
include "../header.php";
include "header.php";

$mid = intval($_GET['mid']);

if (isset($_GET['type'])){
    $x = mysql_query("select * from compose where id = $mid and uid = ".$API->uid);
}
else{
    $x = mysql_query("select uid_to,uid,subj from messages where mid=$mid and (uid_to=" . $API->uid . " or uid=" . $API->uid . ")");
}

if (mysql_num_rows($x) < 1)
    die("Invalid message");

$msg = mysql_fetch_array($x, MYSQL_ASSOC);

if ($msg['uid'] == $API->uid && !isset($_GET['type'])) //user viewing is the original sender
{
	$uid = $msg['uid_to'];
	mysql_query("update messages set unreadBySend=0 where mid=$mid");
}
else if ($msg['uid'] != $API->uid && !isset($_GET['type'])) //user viewing is the original recipient
{
	$uid = $msg['uid'];
	mysql_query("update messages set unreadByRecv=0 where mid=$mid");
}

$user = $API->getUserInfo($uid, "uid,username,name");
?>
	<div class="subhead">
		Message
	</div>

    <? if (!isset($_GET['type'])){ ?>
        <div style="font-size: 8pt; padding-top: 5px;">
            Comments between <a href="<?=$API->getProfileURL($user['uid'], $user['username'])?>"><?=$user['name']?></a> and <a href="<?=$API->getProfileURL()?>">you</a> about <span class="smtitle" style="color: #555;"><?=$msg['subj']?></span>
        </div>
    <? } ?>
	
	<div style="font-size: 8pt;">
	<?php
    if (isset($_GET['type'])){
        ?>
        <div class="comment3">
            <img width="48" height="48" src="../images/social/48x48/manual.png" alt="" />
            <div style="float: right;">
                <div class="commenthead">
                    <div style="float: left; width: 75%; white-space: nowrap;">
                        <?= date("F j, Y", strtotime($msg['created'])) ?>
                        <a href="<?=$API->getProfileURL($user['uid'])?>"><?=$user['username']?></a> wrote:
                    </div>
                    <div style="clear: both;"></div>
                </div>
            <?=str_replace("�", "'", $msg['body'])?>
            </div>
            <div style="clear: both;"></div>
	    </div>
        <?
    }
    else{
        include "../inc/mod_comments.php";
        showCommentsArea("M", $mid, "", false, 3);
    }
	?>
		<div style="clear: both; padding-top: 3px; text-align: right; width: 342px;">
			<a href="javascript:void(0);" onclick="javascript:showReport('M', <?=$mid?>);">Report</a> | <span id="markunread-<?=$mid?>"><a href="javascript:void(0);" onclick="javascript:markUnread(<?=$mid?>);">Mark as unread</a></span> | <a href="javascript:void(0);" onclick="javascript:deleteMsg(<?=$mid?>);">Delete message</a>
		</div>
	</div>
	
<?php
include "footer.php";
include "../footer.php";
?>