<?
/*
Multipurpose popup that is capable of sending an email or a message.
*/

$noLogin = true;

include_once( "../inc/inc.php" );

$gid = intval( $_GET['gid'] );
$uid = intval( $_GET['uid'] );
$em = $_GET['em'];

$srcem = $_GET['srcem'];
$name = $_GET['name'];
$subj = $_GET['subj'];
$msg = $_GET['msg'];
$pic = intval( $_GET['pic'] );


?>

<html>
  <head>
    <title><?= $siteName ?> | Send Message</title>
    <link rel="stylesheet" type="text/css" href="/style_s.css" />
    <link rel="stylesheet" type="text/css" href="/style.css" />
  </head>

<script language="javascript">
<!--
<? if( isset( $_GET['invalid'] ) ) { ?>
  alert( "The validation code that you entered was incorrect.  Please try again." );
<? } ?>

function new_freecap()
{
	// loads new freeCap image
	if(document.getElementById)
	{
		// extract image name from image source (i.e. cut off ?randomness)
		thesrc = document.getElementById("freecap").src;
		thesrc = thesrc.substring(0,thesrc.lastIndexOf(".")+4);
		// add ?(random) to prevent browser/isp caching
		document.getElementById("freecap").src = thesrc+"?"+Math.round(Math.random()*100000);
	} else {
		alert("Sorry, cannot autoreload freeCap image\nSubmit the form and a new freeCap will be loaded");
	}
}

function checkForm()
{
  e = document.getElementById( "email" );
  if( e )
  {
    if( e.value == "" )
    {
      alert( "Please enter an email address before submitting." );
      return false;
    }
  }
  
  return true;
}
//-->
</script>


  <body style="background-color:#d8dfea;">
    <div style="width:470px; margin: 20px; padding:10px; background-color:#fff; font-family:Arial; font-size:9pt;">

    <form action="send_message_verify.php" onsubmit="return checkForm();" method="POST">
    <input type="hidden" name="em" value="<?=$em?>" />
    <input type="hidden" name="uid" value="<?=$uid?>" />
    <input type="hidden" name="gid" value="<?=$gid?>" />
    <input type="hidden" name="pic" value="<?=$pic?>" />
    <input type="hidden" name="name" value="<?=$name?>" />
<?
if ($uid == 0)
	$pic = '/images/salt_badge100.png';
elseif( $pic == 0 ) $pic = "/images/nouser.jpg";
else
{
  $hash = quickQuery( "select hash from photos where id='" . $pic . "'" );
  $pic = "/img/48x48/photos/" . $pic . "/" . $hash . ".jpg";
}
?>
    <div style="font-size: 8pt; color: #555;">
      <div style="float: left; width: 85px;"><img width="48" height="48" src="<?=$pic?>" alt="" style="padding-bottom: 2px;" /><br /><?=$name?></div>
      <div style="float: left; padding-left: 10px; font-weight: bold;">
        <? if( !$API->isLoggedIn() ) { ?>
        <div style="height: 12pt;">Please enter your email address:</div><input type="text" id="email" name="srcem" maxlength="50" style="width: 285px;" value="<?=$srcem?>"/>
        <? } ?>
        <div style="height: 12pt;">Subject:</div><input type="text" id="msgsubj" maxlength="50" style="width: 285px;" value="<?=$subj?>" name="subj"/>

        <div style="padding-top: 5px;">
        	<div style="height: 12pt;">Message:</div><textarea id="msgbody" style="width: 285px; height: 95px;" name="msg"><?=$msg?></textarea>
        </div>

        <div style="padding-top: 5px;">
          <div style="width: 300px;">Validation:<br /><span style="font-weight:300;">Enter the verification code (at the right) in the box below.</span></div>

          <div style="clear: both; padding-top:10px;">
            <div style="float:left;">
              <input name="word" size="10" value=""/>
            </div>

            <div style=" float:left; margin-left:10px;">
              <img src="/inc/freecap/freecap.php" id="freecap" width="200">
              <br />
              Can't read this?  <a href="javascript:void(0);" onclick="javascript: this.blur(); new_freecap();">Get a new image</a>.
            </div>
          </div>
        </div>

        <div style="clear:both;"></div>

        <div style="padding: 5px 0px; margin-left:90px; height: 16px;" id="msgbuttons">
        	<input type="submit" class="button" value="Send" />
        	<input type="button" class="button" value="Cancel" onclick="javascript:window.close();" />
        </div>
      </div>
      <div style="clear: both;"></div>
    </div>

    </form>

    </div>
  </body>
</html>


