<?
/*
send_message_popup.php submits to this page.

Verifies that the "captcha" is correct, and then sends the message or email.
*/

require( "../inc/inc.php" );

//Check captcha
if(!empty($_SESSION['freecap_word_hash']) && !empty($_POST['word']))
{
	// all freeCap words are lowercase.
	// font #4 looks uppercase, but trust me, it's not...
	if($_SESSION['hash_func'](strtolower($_POST['word']))==$_SESSION['freecap_word_hash'])
	{
		// reset freeCap session vars
		// cannot stress enough how important it is to do this
		// defeats re-use of known image with spoofed session id
		$_SESSION['freecap_attempts'] = 0;
		$_SESSION['freecap_word_hash'] = false;


		// now process form


		// now go somewhere else
		// header("Location: somewhere.php");
		$word_ok = true;
	} else {
		$word_ok = false;
	}
} else {
	$word_ok = false;
}

if( !$word_ok )
{
  $query = "";
  
  foreach ($_POST as $k => $v)
  {
  	if ($k != "p")
    {
  		$query .= "&" . htmlentities("$k=$v");
    }
  }
  
  header( "Location: send_message_popup.php?invalid=1&" . $query );
  exit;
}
else
{
  if( !$API->isLoggedIn() )
  {
    $email = $_POST['srcem'];
    $API->uid = 832;
    $_POST['body'] = "This message is being sent on behalf of " . $email . "\n\n" . $_POST['body'];
  }

  if( intval( $_POST['gid'] ) > 0 || intval( $_POST['uid'] ) > 0 )
  {    
    $_POST['email'] = $_POST['srcem'];
    $_POST['body'] = $_POST['msg'];
    require( "sendmessage.php" );
  }
  else
  {

    //Email is going to an email address
    $dest = "cr";
    switch( intval( $_POST['em'] ) )
    {
      case 1: $dest = "advertising"; break;
      case 2: $dest = "feed_back"; break;
      case 3: $dest = "signup"; break;
    }

    $_POST['to'] = $dest . '@' . $siteName . ".com";

    if( $API->isLoggedIn )
      $_POST['from'] = quickQuery( "select email from users where uid='" . $API->uid . "'" );
    else
      $_POST['from'] = $_POST['srcem'];

    $_POST['subject'] = $_POST['subj'];
    $_POST['message'] = $_POST['msg'];
    $_POST['send'] = 1;

    ob_start();
    include( "../send_email.php" );
    ob_end_clean();

  }
}
?>

<html>
<head>
  <title>Message Sent</title>
</head>
<body>
  <div style="text-align:center;">
  Thank you, your message has been sent.
  </div>
<script language="javascript">
<!--
window.close();
-->
</script>
</body>
</html>