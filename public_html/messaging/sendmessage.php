<?php
/*
Sends an internal message to another user.
*/

include_once "../inc/inc.php";

if( !$API->isLoggedIn() )
{
  $email = $_POST['email'];
  $API->uid = 832;
  $_POST['body'] = "This message is being sent on behalf of " . $email . "\n\n" . $_POST['body'];
}


if( isset( $_POST['gid'] ) && $_POST['gid'] > 0 )
{
  //This is a message sent to a page.  It will be sent to the admins of the site, or stored until someone claims the page.
  $gid = $_POST['gid'];

  $verified = quickQuery( "select verified from pages where gid='$gid'" );
  if( $verified )
  {
    //The page is verified, send the message to the page admins.
    $q = mysql_query( "select uid from page_members where gid='$gid' and admin=1" );
    while( $r = mysql_fetch_array( $q ) )
    {
      $uid = $r['uid'];
      if( $uid != $API->uid )
      {
        $API->sendMessage($uid, $_POST['subj'], $_POST['body'], null, false);
      }
    }

    $template = quickQuery("select content from static where id='page_msg_verified'" );
  }
  else
    $template = quickQuery("select content from static where id='page_msg_nonverified'" );


  $uid = $API->uid;
  //Need to store message for later.... whoever claims the page
  $to = quickQuery( "select contact_person from pages where gid='$gid'" );
  $email = quickQuery( "select email from pages where gid='$gid'" );
  $from = quickQuery("select name from users where uid=" . $uid);
  $gname = quickQuery( "select gname from pages where gid='$gid'" );

  $email_body = str_replace( "{NAME_FROM}", $from, $template );
  $email_body = str_replace( "{NAME}", $to, $email_body );
  $email_body = str_replace( "{PAGE}", $gname, $email_body );
  $email_body = str_replace( "{PAGE_URL}", "http://" . $siteName . $API->getPageURL( $gid ), $email_body );
  $email_body = str_replace( "{SITENAME}", $siteName, $email_body );

  $email_body = nl2br( $email_body );

  if (isEmailAddressValid($email))
  	emailAddress($email, "New Lead from $siteName", $email_body );

  $msg = addslashes( $_POST['body'] );
  $subj = addslashes( $_POST['subj'] );
  $email = addslashes( $_POST['email'] );
  mysql_query( "insert into page_message (`from`, `gid`, `message`, `subject`, `email`, `verified_email`) values ($uid,$gid, '$msg', '$subj', '$email', 0)" );
  echo mysql_error();
}
else
{  
  if (substr($_POST['uid'], 0, 1) == "G") //message page
  {
  	$gid = intval(substr($_POST['uid'], 1));

  	$x = mysql_query("select uid,admin from page_members where gid=$gid");
  	while ($y = mysql_fetch_array($x))
  	{
  		if (($y['uid'] == $API->uid && $y['admin'] == 1 ) || $API->admin ) // check to see if sending user is admin
  			$isAdmin = true;

  		if ($y['uid'] != $API->uid) // do not message self
  			$uids[] = $y['uid'];
  	}

  	if (!$isAdmin) die();
  }
  else
  	$uids = array(intval($_POST['uid'])); //regular message


  if (count($uids) > 0)
  	foreach ($uids as $uid)
    {      
  		$API->sendMessage($uid, $_POST['subj'], $_POST['body']);
    }
}
?>OK