<?php
/*
Step 1 in the claim a page process.
*/

$noLogin=true;

require( "../inc/inc.php" );

$title = "Pages | $siteName";
$descr = "Pages bring you closer to customers, peers, and followers.  It provides the ability to be heard, discoverd, and so much more. Claim or create yours.";


include_once "../inc/recaptchalib.php";
$publickey = "6LciL78SAAAAANVcbxyOQtkOkEyJN1YaGs_HUe4M";
$privatekey = "6LciL78SAAAAAKjsEQNYz2kD7Dm__mk4jshxleAk";

$scripts[] = "/pages/pages.js";

if ($site == "s" && !$API->isLoggedIn())
{
/*  $title = "Pages | SaltHub - Be heard - Get discovered - Connect";
  $descr = "SaltHub is the leading social network for professionals and businesses who make a living on and around the water. Connect, access knowledge, insights and be discovered.";
  $page_header = $siteName . " Pages";
  $sub_header = $siteName . ' is the leading social network for professionals and businesses who make a living on and around the water. Claim or create your page.';*/
	$scripts[] = "/index.js";
	$noheader = true;
	include "../signup/newheader.php";
	include "../header_gradient.php";
}
else
{
  $background = "url('/images/bggraygrad2.png'); background-repeat:repeat-x; margin-top:-5px; width:100%;";
  include "../header.php";
}

$gid = $_GET['gid'];

if( isset( $gid ) )
{
  $type = quickQuery( "select catname from pages left join categories on categories.cat=pages.cat where pages.gid='$gid'" );
  $type = strtolower( $type );

  if( $type != "School" )
    $type = "Company";
}
else
  $type = "company or School";

?>

<style>
.subhead2
{
  font-size: 14px;
  font-weight: bold;
  margin-top:15px;
}

.subheadcontent2
{
  font-size: 13px;
}

</style>

<div class="graygradient">
	<div class="headcontainer">
		<div class="entryslide">
			<?=$htmlSignIn?>
			<div class="slidecontent">
				<span class="title">Claim Your Page</span>
			
				<div class="text">
          Claiming your page is used to establish authenticity of company identities on <?=$siteName?>. The verified page badge helps users discover reliable sources of information and trust that a legitimate source is providing the information you are relying on.  Claim your page below.
				</div>

				<div class="quotecontainer">
					<div class="quote">
						<div class="open"></div>
						<div class="text" style="width: 430px;">Verifying our page on <?=$siteName?> allows users to know the information they are reading is official, which strengthens our customer relations.</div>
						<div class="close" style="margin-top:50px;"></div>
						<div class="person">&mdash; Jessica, <?=$siteName?> member since 2012</div>
					</div>
				</div>
			</div>
			
			<img src="/images/check.jpg" class="bigimage" alt="" />
		</div>
	</div>
</div>


<div style="padding: 0px 5px 5px 5px; font-family: arial; font-size: 11pt; width:1050px; margin-left: auto; margin-right:auto;">
  <div style="width:1000px; margin-left:15px; margin-top: 10px;">
  <div style="float:left; font-size:20pt; font-family:Tahoma; color:rgb(127, 127, 127); padding-top:12px;">
    Consumer Relations on <? echo $siteName ?>
  </div>
  <div style="float:left; margin-left:10px; padding-top:5px;"><img src="/images/checkmark_sticker.png" width="48" height="48" alt=""/></div>

  <div style="clear:both; width:100%; font-size:18px; padding-top:20px;">
    <div style="float:left; padding-top:5px;">Why Claim Your Page?</div>

    <div style="float:left; margin-left:20px; margin-top:3px; position:relative;">
      <!--<input type="text" id="searchbox" name="searchbox" value="Type the name of your company or school here" style="width:284px; font-size:10pt; height:20px; color:rgb(85,85,85);" onkeyup="javascript:searchKeypress(event,this,'1', 1);" onfocus="javascript:if( this.value=='Type the name of your company or school here' ) this.value='';" onblur="javascript:if (this.value=='') this.value='Type the name of your company or school here'; searchLostFocus(-1,'1');" />-->
      <input type="text" id="searchbox" name="searchbox" data-autocomplete="get_company_or_school" data-default="Type the name of your company or school here" style="width:284px; font-size:10pt; height:20px; color:rgb(85,85,85);" />
      <div id="suggestionBox1" style="padding:0px; position: absolute; left:0px; top:25px; width:288px; height:205px; z-index:101; display:none;"></div>
    </div>

    <div style="float:left; margin-left:20px; font-size:9pt; margin-top:8px;"><a href="/pages/page_create.php"><div style="float:left;"><img src="/images/page_add.png" width="16" height="16" alt="" /></div> <div style="float:left; margin-left:5px;">Create a Page</div></a></div>
    <div style="clear:both;"></div>
  </div>

  <div style="float:left; width:300px;">

    <div class="subhead2">Ensure Accurate Information</div>
    <div class="subheadcontent2">Verify and update your company description, contact information, media and more.</div>

    <div class="subhead2">Social Distribution</div>
    <div class="subheadcontent2">Send your <? echo strtolower($type) ?> press releases and consumer messages to multiple social networks simultaneously, by linking your <?=$siteName?> account to your Twitter, Facebook, LinkedIn and mediaBirdy pages.</div>

    <div class="subhead2">Targeted Audience</div>
    <div class="subheadcontent2">Get your message in front of the largest online network of professional mariners, water enthusiasts, industry experts and related business's and services.</div>

    <div class="subhead2">Special Offers</div>
    <div class="subheadcontent2"><?=$siteName?> will offer verified companies and pages, special promotions and opportunities not available to others.</div>

    <div class="subhead2">Connect</div>
    <div class="subheadcontent2">Connect with consumers and monitor what they have to say about your products and services. Manage posts and update your about page, photos and videos.</div>


  </div>

  <div style="float:left; width:300px; margin-left:150px;">
    <div class="subhead2">Verified Account</div>
    <div class="subheadcontent2">Give the community confidence they are consuming trusted official company news, information and feedback.</div>

    <div class="subhead2">Prominent Messaging</div>
    <div class="subheadcontent2">Based on user's interests and activities, verified companies and pages will have their posts shown in the activity log of the community page.</div>

    <div class="subhead2">Official Messaging</div>
    <div class="subheadcontent2"><? echo ucfirst($type) ?> posts will show as verified and will be highlighted with premium placement, when posted to the <? echo strtolower($type) ?> log book. Directly message members of your <? echo $type ?> page.</div>

    <div class="subhead2">Business News Feed</div>
    <div class="subheadcontent2">Posted content, updates and news about your company or service to your Business Page will be shown in the <a href="/directory.php">Business News Feed</a>.</div>

    <div class="subhead2">Support</div>
<? $email = "cr@" . strtolower( $siteName ) . ".com"; ?>
    <div class="subheadcontent2">Still have questions? Send us a message at <a href="javascript:void(0);" onclick="javascript:openSendMessagePopup('','','SaltHub',1187301,0,0,0 );"><? echo $email ?></a></div>

    <div class="subheadcontent2" style="text-align:center; margin-top:40px; margin-bottom:10px;">
<? if( isset( $_GET['gid'] ) ) { ?>
      <input type="button" class="button" value="Claim My <? echo ucfirst($type) ?>" style="font-size:20px;" onclick="javascript:location='claim_company2.php?gid=<? echo $_GET['gid']; ?>';" />
<? } ?>
    </div>
  </div>

</div>
</div>
<?php
if (isset($_GET['1']) || ($site == "s" && !$API->isLoggedIn()))
  include "../signup/newfooter.php";
else
	include "../footer.php";
?>