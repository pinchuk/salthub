<?php
/*
Saves the "About" information for a page.
*/

include "../inc/inc.php";

$gid = $_SESSION['gid'];

if (quickQuery("select admin from page_members where uid={$API->uid} and gid=$gid") == 0 && $API->admin == 0)
	die();


$type = quickQuery( "select type from pages where gid='$gid'" );

$cols = array('descr', 'cat', 'email0', 'www0', 'address0', 'privacy', 'phone0', 'cell0', 'fax0', 'contact_person0');

if( $API->admin )
{
  $cols[] = 'gname'; //Only the admin can edit the page name
  $cols[] = 'type';
  $type = $_POST['type'];
}

if( $type != PAGE_TYPE_BUSINESS )
{
  $cols[] = "cat";
  $cols[] = "subcat";
}

foreach ( $_POST as $col => $val)
{
  if( !in_array( $col, $cols ) ) continue;

  $val = urldecode( $val );
  $val = str_replace( "%u2019", "'", $val );
  $val = str_replace( "%u2013", "-", $val );
  $val = addslashes( unescape( $val ) );

  if( substr( $col, -1 ) == "0" )
    $col = substr( $col, 0, -1 );

  $update[] = "$col = \"" .$val . "\"";
}

mysql_query("SET character_set_results = 'utf8', character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'");

mysql_query( "delete from page_locations where gid='$gid'" );

$cols = array( 'location_name', 'phone', 'email', 'address', 'cell', 'fax', 'contact_person');
for( $l = 1; $l < 20; $l++ )
{
  if( isset( $_POST['location_name' . $l ] ) && $_POST['location_name' . $l ] != "" )
  {
    $sql = "insert into page_locations ( gid, location_name, phone,  email, address, cell, fax, contact ) values ( $gid";
    for( $c = 0; $c < sizeof( $cols ); $c++ )
    {
      $val = urldecode( $_POST[ $cols[$c] . $l ] );
      $val = str_replace( "%u2019", "'", $val );
      $val = str_replace( "%u2013", "-", $val );

      $sql .= ", '" . $val . "' ";
    }
    $sql .= ")";

    mysql_query( $sql );

    if( mysql_error() )
    {
      echo mysql_error();
      exit;
    }

  }
}

//$gtype is used in some older code.  Trying to remove the usage of this field!!  Included here for backward compatibility.
switch( $type )
{
  case PAGE_TYPE_BUSINESS:
  {
    $gtype = 'C';
    $API->setToDoItem( TODO_COMPANY_PAGE );
  }
  break;

  case PAGE_TYPE_VESSEL:
  {
    $bid = quickQuery( "select id from boats where gid='$gid'" );

    if( $bid == 0 )
    {
      sql_query( "insert into boats (name,gid) values ('" . addslashes( unescape( urldecode( $data['gname'] ) ) ) . "', $gid)" );
      $bid = mysql_insert_id();
      $update[] = "glink=\"$bid\"";
    }

    $gtype = 'B';

    $update2 = array();

    include( "about-vessel-sections.php" );
    $cols = array();

    foreach( $sections as $section => $val )
      foreach( $val as $field => $display )
        if( $field != "col" && $field != 'cat')
          $cols[] = $field;

//    $cols = array('exnames','year','shipyard','hullid','imo','mmsi','callsign','flag','navalarchitect','stylist','decorator','sistership','refit','length','breadth','draught','hullsuperstr','engine','speedmax','speedcruise','rangenm','fuelltr','waterltr', 'guests', 'crew' );

    foreach ($_POST as $col => $val)
    {
      if( !in_array( $col, $cols ) ) continue;
      $val = urldecode( $val );
      $val = str_replace( "%u2019", "'", $val );
      $val = str_replace( "%u2013", "-", $val );
      $val = addslashes( unescape( $val ) );
      $update2[] = "$col = \"" . addslashes(htmlentities($val)) . "\"";
    }

    sql_query("update boats set " . implode(",", $update2) . " where gid='$gid' limit 1");

    //echo "update boats set " . implode(",", $update2) . " where gid='$gid' limit 1";

    if( mysql_error() )
    {
      echo mysql_error();
      exit;
    }

  }
  break;

  default:
    switch( $_POST['cat'] )
    {
      case 101: $gtype = 'L'; break;
      case 113: $gtype = 'S'; break;
      case 102:
      case 103:
      case 105:
      case 107: $gtype = 'P';
      break;
      case 106:
        $gtype = 'O';
      break;
      default: $gtype = ''; break;
    }
  break;
}

/*
Update 5/4/2012 - Saving selection of categories and sectors here now.
*/
if( $type == PAGE_TYPE_BUSINESS )
  {
  $purchase = false;

  $cat_funded     = intval( quickQuery( "select categories_funded from pages where gid='" . $gid . "'" ) ) + 1;
  $sectors_funded = intval( quickQuery( "select sectors_funded from pages where gid='" . $gid . "'" ) ) + 1;

  $funded = $cat_funded;

  if( isset( $_SESSION['page_categories'] ) )
    $curr_selection = $_SESSION['page_categories'];
  else
  {
    $valid_categories = array();
    $sel_q = mysql_query( "select cat as type_id, catname as name, heading from categories where industry IN ( SELECT cat from categories where industry='" . PAGE_TYPE_BUSINESS . "') order by heading, catname" );
    while( $sel_r = mysql_fetch_array( $sel_q ) )
      $valid_categories[] = $sel_r['type_id'];

    $curr_selection = array();
    $sel_q = sql_query( "select cat as type_id from page_categories where gid='" . $gid . "'" );
    while( $sel_r = mysql_fetch_array( $sel_q ) )
      $curr_selection[] = $sel_r['type_id'];

    $curr_selection = array_intersect( $curr_selection, $valid_categories );
  }

  $prev_selection = array();

  $sel_q = sql_query( "select cat as type_id from page_categories where gid='" . $gid . "'" );
  while( $sel_r = mysql_fetch_array( $sel_q ) )
    $prev_selection[] = $sel_r['type_id'];  //This includes both sectors and categories

  $new_selection = array_diff( $curr_selection, $prev_selection );
  //$removed_selection = array_diff( $prev_selection, $curr_selection );

  $old_selections_remaining = array_intersect( $curr_selection, $prev_selection );
  $items = array();
  $_SESSION['unfunded_categories'][ $gid ] = array();

  for( $c = 0; $c < $funded && sizeof( $old_selections_remaining ) > 0; $c++ ) //Add the old items that we still want first
  {
    $items[] = array_shift( $old_selections_remaining );
  }

  if( sizeof( $old_selections_remaining ) > 0 )
  {
    $purchase = true;
    foreach( $old_selections_remaining as $val )
      $_SESSION['unfunded_categories'][$gid][] = $val; //Add to the list of things to purchase
  }

  $funded -= sizeof( $items );

  for( $c = 0; $c < $funded && sizeof( $new_selection ) > 0; $c++ ) //Now start adding the new items that we want, up to the point that we're still funded
  {
    $items[] = array_shift( $new_selection );
  }

  if( sizeof( $new_selection ) > 0 )
  {
    $purchase = true;
    foreach( $new_selection as $val )
      $_SESSION['unfunded_categories'][$gid][] = $val; //Add to the list of things to purchase
  }


  mysql_query( "delete from page_categories where cat IN ( SELECT distinct child from category_relationships inner join categories on categories.cat=category_relationships.parent where categories.industry='" . PAGE_TYPE_BUSINESS . "') and gid='$gid'" );
  mysql_query( "update pages set subcat='" . $items[0] . "' where gid='$gid' limit 1" );
  foreach( $items as $item )
    mysql_query( "insert into page_categories (gid,cat) values ($gid, $item)" );
  unset( $_SESSION['page_categories'] );

  $funded = $sectors_funded;

  if( isset( $_SESSION['page_sectors'] ) )
    $curr_selection = $_SESSION['page_sectors'];
  else
  {
    $valid_categories = array();
    $sel_q = mysql_query( "select cat as type_id, catname as name, 0 as heading from categories where industry=" . PAGE_TYPE_BUSINESS . " order by catname" );
    while( $sel_r = mysql_fetch_array( $sel_q ) )
      $valid_categories[] = $sel_r['type_id'];

    $curr_selection = array();
    $sel_q = sql_query( "select cat as type_id from page_categories where gid='" . $gid . "'" );
    while( $sel_r = mysql_fetch_array( $sel_q ) )
      $curr_selection[] = $sel_r['type_id'];

    $curr_selection = array_intersect( $curr_selection, $valid_categories );
  }

  $prev_selection = array();

  $sel_q = sql_query( "select cat as type_id from page_categories where gid='" . $gid . "'" );
  while( $sel_r = mysql_fetch_array( $sel_q ) )
    $prev_selection[] = $sel_r['type_id'];  //This includes both sectors and categories

  $new_selection = array_diff( $curr_selection, $prev_selection );
  //$removed_selection = array_diff( $prev_selection, $curr_selection );

  $old_selections_remaining = array_intersect( $curr_selection, $prev_selection );
  $items = array();
  $_SESSION['unfunded_sectors'][ $gid ] = array();

  for( $c = 0; $c < $funded && sizeof( $old_selections_remaining ) > 0; $c++ ) //Add the old items that we still want first
  {
    $items[] = array_shift( $old_selections_remaining );
  }

  if( sizeof( $old_selections_remaining ) > 0 )
  {
    $purchase = true;
    foreach( $old_selections_remaining as $val )
      $_SESSION['unfunded_sectors'][$gid][] = $val; //Add to the list of things to purchase
  }

  $funded -= sizeof( $items );

  for( $c = 0; $c < $funded && sizeof( $new_selection ) > 0; $c++ ) //Now start adding the new items that we want, up to the point that we're still funded
  {
    $items[] = array_shift( $new_selection );
  }

  if( sizeof( $new_selection ) > 0 )
  {
    $purchase = true;
    foreach( $new_selection as $val )
      $_SESSION['unfunded_sectors'][$gid][] = $val; //Add to the list of things to purchase
  }

  mysql_query( "delete from page_categories where cat IN ( select cat from categories where industry=" . PAGE_TYPE_BUSINESS . ") and gid='$gid'" );
  mysql_query( "update pages set cat='" . $items[0] . "' where gid='$gid' limit 1" );
  foreach( $items as $item )
    mysql_query( "insert into page_categories (gid,cat) values ($gid, $item)" );
  unset( $_SESSION['page_sectors'] );
}

/*
//For Debug
print_r( $_SESSION['unfunded_categories'] );
echo "<br /><br />";
print_r( $_SESSION['unfunded_sectors'] );

exit;
*/

/* End update for saving category and sectors */

mysql_query("update pages set " . implode(",", $update) . ",gtype='$gtype' where gid=$gid");

if( $_POST['dir_listing'] == "1" )
{
  $_SESSION['dir_listing'][$gid] = 1;
  $purchase = true;
/*
  $funded = quickQuery( "select dir_listing_funded from pages where gid='$gid'" );
  if( $funding != 1 )
  {
    include_once( "../billing/billing-functions.php" );
    billingAddItemToCart( 3, $gid );
    header( "Location: /billing" );
    exit;
  }
*/
}
else
  unset( $_SESSION['dir_listing'][$gid] );

header( "Location: " . $API->getPageURL( $gid ) . "/editpage?" . ($purchase?'checkout':'') );
?>




