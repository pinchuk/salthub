<?
/*
Enables the user to edit the about screen information for a Page.  The user must ethier be
a page or site admin to use this screen.
*/

$isAdmin = quickQuery( "select admin from page_members where gid='$gid' and uid='" . $API->uid . "'" );
$isAdmin = $isAdmin || $API->admin;

if( !$isAdmin ) die( "You do not have access to modify this page." );

$_SESSION['gid'] = $gid;

unset( $_SESSION['page_categories'] );
unset( $_SESSION['page_sectors'] );

?>

<style>
.smtitle2
{
  font-size:8pt;
  color:rgb(85,85,85);
  padding-top:3px;
  font-family:arial;
  font-weight:300;
}

</style>

<div id="pccontainer"></div>


<div style="font-size:13pt; margin-left:6px; margin-top:4px;">
  <div style="float:left; margin-right:3px;"><img src="/images/page.png" width="16" height="16" alt="" /></div>
  <div style="float:left; font-weight:700;">Welcome to your Page!</div>
</div>

<div style="clear:both; font-size:9pt; padding:3px; padding-bottom:5px; margin-left:6px;">
  Pages bring you closer to customers, peers and followers. We have also provided additional features to be heard, discovered and so much more. Learn more below.
</div>

<div style="font-size: 10pt; color: #555; padding: 5px 5px; line-height: 17px; margin: 0 4px 0 5px; background-color:#f2fbff; " class="pageabout">
  <div class="smtitle">
    <div style="float:left;"><img src="/images/comment_edit.png" width="16" height="16" alt="" /></div>
    <div style="float:left;">Add your 'about' info.</div>
  </div>

	<div style="padding-bottom: 3px; font-size: 8pt;	font-family: tahoma; clear:both;">
		<div>
  		<div class="left">About:</div>
			<textarea id="grpedit-descr" style="height:100px;" name="descr"><?=$page['descr']?></textarea>
		</div>
	</div>
<? if( $page['type'] == PAGE_TYPE_BUSINESS ) { ?>
  <div class="smtitle" style="padding-top:10px; margin-top:20px;">
    <div style="float:left;"><img src="/images/page_add.png" width="16" height="16" alt="" /></div>
    <div style="float:left;">Pages with more than one category or one industry get more exposure. Be Seen!</div>
  </div>

  <div class="smtitle2" style="clear:both; margin-top:10px;">
    Products and Services:
  </div>

  <div style="clear:both;">
    <div style="float:left; width:170px;"><div id="PandS" style="font-size:8pt;"></div></div>
    <div style="float:left;" class="smtitle2">Your first category is included, purchase more categories for $20.00 each.</div>
    <div style="clear:both;"></div>
  </div>


  <div class="smtitle2" style="clear:both; margin-top:10px;">
    Industries:
  </div>

  <div>
    <div style="float:left; width:170px;"><div id="ind"></div></div>
    <div style="float:left;" class="smtitle2">Your first industry is included, purchase more industries for $75.00 each.</div>
    <div style="clear:both;"></div>
  </div>

  <div class="smtitle" style="padding-top:10px; margin-top:20px;">
    <div style="float:left;"><img src="/images/vcard.png" width="16" height="16" alt="" /></div>
    <div style="float:left;">Be discovered on the worlds largest and only Social Maritime Directory. <span>(recommended)</span></div>
  </div>

  <div style="clear:both;">
    <ul style="font-size:8pt; color:rgb(85, 85, 85);">
      <li>Posted content, RSS Feeds, updates and news about your company or service to your Business Page will be shown in the <a href="/directory.php" target="_new">Business News Feed</a>.</li>
      <li>Your news and updates will appear under each industry that you choose in the Business News Feed.</li>
      <li>The Business Directory is social. This allows memebers to see if their connections use your business.</li>
      <li>Enable users to discover your business. The Business Directroy has an advanced search feature.</li>
      <li>There's an App for that. The only complete Maritime Directory available to mobile users.</li>
    </ul>
  </div>

<?
$dir_listing_funded = quickQuery( "select dir_listing_funded from pages where gid='$gid'" );
if( $dir_listing_funded == 0 )
{
?>
  <div style="clear:both; margin-top:5px; margin-left:20px;">
    <div style="float:left; padding-top:2px;">
      <input type="checkbox" name="dir_listing" value="1" />
    </div>
    <div class="smtitle2" style="float:left;">
      Include my Page in the <?=$siteName?> <a href="/directory.php" target="_new">Business Directory</a>. $299.00 for 12 months / 81 cents a day!
    </div>
  </div>
<? } //End if not listed in directory
else
{
?>
  <div style="clear:both;">
    <ul style="font-size:8pt; color:rgb(85, 85, 85);">
      <li>This page is listed in the <a href="/directory.php" target="_new">Business News Feed</a>.</li>
    </ul>
  </div>
<?
}

 } //End if( $page['type'] == PAGE_TYPE_BUSINESS ) ?>

<div style="clear: both; padding-top:5px;"></div>

<?php
if ($page['boat'])
{
  $edit = true;
  include( "about-vessel.php" );
}
?>

<div style="clear: both; height:10px;"></div>

  <div class="smtitle">
    <div style="float:left;"><img src="/images/application_edit.png" width="16" height="16" alt="" /></div>
    <div style="float:left;">Manage and administer your page.</div>
  </div>


	<div class="profiletop" style="clear:both;">
		<div class="summaryinfo" style="width: 165px; height:170px;" id="si1">
<? if( $API->admin == 1) { ?>
      <p />
			<div id="grpedit-gname0"><div class="left">Page Name:</div><br /><input type="text" id="grpedit-gname" value="<?=$page['gname']?>" name="gname" /></div>
<? } ?>
      <p/>
			<div class="left">Page Type:</div>
			<div class="value" id="grpedit-type0" style="margin-bottom:10px;">
        <?
        if( !$API->admin ) echo $page['pagetype'];
        else
        {
        ?>
				<select id="grpedit-type" name="type" onchange="javascript: refreshCategory(<? echo $page['gid']; ?>, selectedValue(this) );">
				<?php
				$x = sql_query("select cat,catname from categories where cattype='T' order by catname");
				while ($cat = mysql_fetch_array($x))
					echo '<option ' . ($cat['cat'] == $page['type'] ? 'selected' : '') . ' value="' . $cat['cat'] . '">' . $cat['catname'] . '</option>';
				?>
				</select>
        <? } ?>
			</div>

<? if( $page['type'] != PAGE_TYPE_BUSINESS && $page['type'] != PAGE_TYPE_VESSEL ) { ?>
      <p/>
      <div id="category">
			<div class="left">Category:</div>
			<div class="value" id="grpedit-cat0">
				<select id="grpedit-cat" name="cat" onchange="javascript: refreshSubcatType(<? echo $page['gid']; ?>, selectedValue(this) );">
				<?php
        $x = sql_query("select cat,catname from categories where industry='" . $page['type'] . "' order by catname");
				while ($cat = mysql_fetch_array($x))
					echo '<option ' . ($cat['cat'] == $page['cat'] ? 'selected' : '') . ' value="' . $cat['cat'] . '">' . $cat['catname'] . '</option>';
				?>
				</select>
			</div>
      </div>

			<p />
      <div id="subcat">
      <? include( "subcat_selection.php" ); ?>
      </div>
			<p />
<? } // End if( $page['type'] != PAGE_TYPE_BUSINESS ) ?>

			<div class="left">Accessible to:</div>
			<div class="value" id="grpedit-privacy0">
				<select id="grpedit-privacy" name="privacy">
					<?php
					$priv = array(
						PRIVACY_EVERYONE => "The world",
						PRIVACY_SELECTED => "Only members"
						);

					foreach ($priv as $k => $v)
						echo '<option value="' . $k . '" ' . ($k == $page['privacy'] ? 'selected' : '') . '>' . $v .'</option>';
					?>
				</select>
			</div>

    <div style="clear:both;"></div>

		</div>
		<div class="summaryinfo" style="width: 165px; height:170px;" id="si2">
			<p />
			Statistics:<br />
			<?=date("F j, Y", strtotime($page['created']))?><br />
			<?=plural($user['cfriends'], "member")?><br />
			<?=plural($vidCount, "video")?><br />
			<?=plural($picCount, "photo")?>
      <div style="clear:both;"></div>
		</div>
		<div class="summaryinfo" style="width: 171px; border-right: 0; margin-right: 0; height:170px;" id="si0">
				<p />
				<div class="left">Created by:</div>
				<div class="value" style="font-weight:300; color: #555;">
	  		<?php
  			if ( empty($page['uid']) || $page['uid'] == "" ) {
          $gid2 = quickQuery( "select gid from pages where gname='$siteName'" );
          echo '<a style="font-weight:300;" href="' . $API->getPageURL($gid2) . '">' . $siteName . '</a>';
         } else { ?>
          <a style="font-weight:300;" href="<?=$API->getProfileURL($page['uid'])?>"><?=$page['name']?></a><?=$page['uid'] == $API->uid ? '' : ' &nbsp;|&nbsp; ' . friendLink($page['uid'])?>
        <? } ?>
        </div>
				<p />



			Administrators: <? if( $isAdmin ) echo '<a style="font-weight:300; font-size:8pt;" href="javascript:void(0);" onclick="javascript:showPageMembers(' . $page['gid'] . ');">manage admins</a>';

  		$x = sql_query("select u.uid,username,name from page_members m inner join users u on m.uid=u.uid where m.gid=$gid and m.admin=1");

      if( mysql_num_rows( $x ) > 0 )
      {
				while ($user = mysql_fetch_array($x, MYSQL_ASSOC))
				{
					echo '<br /><a style="font-weight:300;" href="' . $API->getProfileURL($user['uid'], $user['username']) . '">' . $user['name'] . '</a>';
					if ($user['uid'] != $API->uid)
						echo " &nbsp;|&nbsp; " . friendLink($user['uid']);
				}


				if (isset($_GET['edit']) && $isAdmin)
				{
				?>
				<div style="padding: 20px; text-align:center;">
					<input type="button" class="button" value="Edit Members" onclick="javascript:showPageMembers(<?=$page['gid']?>);" style="width: auto;" />
				</div>
				<?php
				}
			}
			else
			{
        $gid2 = quickQuery( "select gid from pages where gname='$siteName'" );
        if( isset( $gid2 ) )
        {
          echo '<br /><a style="font-weight:300;" href="' . $API->getPageURL($gid2) . '">' . $siteName . '</a>';
        }
        else
          echo 'There are no administrators of this page.';
	    }

      if( isset( $isAdmin ) && $isAdmin == 1 )
      {
        $gid = $_SESSION['gid'];
          $postAsPage = quickQuery( "select postAsPage from page_members where uid='" . $API->uid . "' and admin='1' and gid='$gid'" );
        ?>
        <div><a href="javascript:void(0);" onclick="javascript:adminPostSettings('<?=$postAsPage?>');">Page Settings</a></div>
        <div style="margin-top:5px;"><a href="javascript:void(0);" onclick="javascript: showPopupUrl( '/pages/rss_feed_popup.php?gid=<?=$gid?>' );">Add your RSS Feed <img src="/images/feed_add.png" width="16" height="16" alt="" /></a></div>
        <?
      }
			?>

		</div>
	</div>

  <div style="clear:both;"></div>
</div>




<div style="clear: both; text-align: center; padding-top: 10px;" id="grpedit-save0">
<input type="submit" class="button" value="Save Changes" />
</div>

<? if( isset( $_GET['checkout'] ) ) { ?>
</form>

<div id="complete_order_popup">
<? $gid = $page['gid']; require( "complete_order_popup.php" ); ?>
</div>

<script language="javascript" type="text/javascript">
<!--
  showPopUp( '', '<div id="complete_order_parent"></div>', 0 );
  setTimeout('document.getElementById("complete_order_parent").appendChild(document.getElementById("complete_order_popup"));', 100);
-->
</script>

<? } ?>


<script language="javascript" type="text/javascript">
<!--
editPageAbout(true);

<?php
if (isset($_GET['pic']))
	echo 'showPhotoChanger();';
?>

newComboListBox( 1, "PandS" );
newComboListBox( 2, "ind" );

<?
$gid = $page['gid'];

$postAsPage = quickQuery( "select postAsPage from page_members where uid='" . $API->uid . "' and admin='1' and gid='$gid'" );
//if( empty( $postAsPage ) ) $postAsPage = 1;
//echo 'postAsPage = ' . !empty($postAsPage)?$postAsPage:-1 . ';';
?>
    
var postAsPage = <?=strlen($postAsPage) > 0?$postAsPage:-1?>;

function adminPostSettings(type)
{
    isPageAdmin = <?=strlen($postAsPage) > 0?$postAsPage:-1?>;
    if(isPageAdmin < 0){
        if(!confirm("You are not administrator of this page. This page settings are assotiated with specific page administrator. Are you realy sure to continue?")){
            return false;
        }
    }
    html = '<div style="height=200px; width:100%; padding:20px; margin-left:-20px;">';
	html += '	<div style="text-align:center;"><input type="checkbox" id="postAsPage" value="1"';
  if( postAsPage == '1' )
    html += ' CHECKED';
  html += '>Always comment and post on your page logbook as \'<?=addslashes($page['gname']);?>\',<br /> even when using <? echo $siteName ?> as \'<? echo $API->name ?>\'.<br /><br /><span style="font-size:8pt;">Note: if you deselct this, your posts will not appear in the Business News Feed<br /> located in the Business Directory.</span></div>';
 	html += ' <div style="text-align:center; clear:both; padding:10px; width:100%;">';
	html += '	<input type="button" class="button" onclick="javascript:UpdatePageSettings('+isPageAdmin+')" value="Update Settings">';
	html += '</div></div>';

	showPopUp("", html);
}

function UpdatePageSettings(checkPageAdmin)
{
    var pageAdmin = 1;
    if(checkPageAdmin < 0){
        pageAdmin = 0;
        if(!confirm("Are you realy sure to apply this settings for all page administrators?")){
            return false;
        }
    }
    console.log('update');
  e = document.getElementById( "postAsPage" );

  postAsPage = 0;
  if( e )
  {
    if( e.checked )
      postAsPage = 1;
  }

	postAjax("/pages/update_page_settings.php", "postAsPage=" + postAsPage + "&gid=<? echo $gid ?>&is_page_admin="+pageAdmin);

  closePopUp(true);
}
<? if( isset( $_REQUEST['join'] ) && quickQuery( "select count(*) from page_members where uid=" . $API->uid . " and gid=" . $gid ) == 0 ) {
?>
joinHTML = '<div class=\'bluebutton\' style=\'cursor: default;\'>Joined</div>'; joinPage(<?=$gid?>, 'side', '<?=$page['gtype']; ?>');
<? } ?>

//-->

</script>