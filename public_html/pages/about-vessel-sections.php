<?
  $anonymousFields = array();

  $anonymousFields[ 'General Information']['imo'] = 1;
  $anonymousFields[ 'General Information']['mmsi'] = 1;
  $anonymousFields[ 'General Information']['cat'] = 1;
  $anonymousFields[ 'General Information']['gross_tonnage'] = 1;
  $anonymousFields[ 'General Information']['deadweight'] = 1;
  $anonymousFields[ 'General Information']['deadweight_lightship'] = 1;
  $anonymousFields[ 'General Information']['build'] = 1;
  $anonymousFields[ 'General Information']['shipyard'] = 1;
  $anonymousFields[ 'General Information']['navalarchitect'] = 1;
  $anonymousFields[ 'General Information']['stylist'] = 1;
  $anonymousFields[ 'General Information']['decorator'] = 1;

  $anonymousFields[ 'General Information']['speedcruise'] = 1;
  $anonymousFields[ 'General Information']['speedmax'] = 1;
  $anonymousFields[ 'General Information']['flag'] = 1;
  $anonymousFields[ 'General Information']['last_known_flag'] = 1;

  $anonymousFields[ 'General Information']['home_port'] = 1;
  $anonymousFields[ 'General Information']['class_society'] = 1;
  $anonymousFields[ 'General Information']['guests'] = 1;
  $anonymousFields[ 'General Information']['crew'] = 1;

  $anonymousFields[ 'Contact Information']['callsign'] = 1;


  $sections = array();

	$sections['General Information'] = array(
		"imo" => "IMO Number",
		"mmsi" => "MMSI Number",
    "cat" => "Vessel Type",
    "gross_tonnage" => "Gross Tonnage",
    "deadweight" => "Summer DWT",
    "deadweight_lightship" => "Lightship DWT",
		"year" => "Build",
		"shipyard" => "Builder",
		"navalarchitect" => "Naval Architect",
		"stylist" => "Stylist",
		"decorator" => "Decorator",
    "col" => "col", //Indicates a new column
		"speedcruise" => "Max. Speed",
		"speedmax" => "Cruise Speed",
    "flag" => "Flag",
    "last_known_flag" => "Last Known Flag",
    "home_port" => "Home Port",
    "owner" => "Manager / Owner",
    "class_society" => "Class Society",
    "insurer" => "Insurer",
    "guests" => "Guests",
    "crew" => "Crew" );

  $sections['<a id="contact"></a>Contact Information'] = array(
		"callsign" => "Call Sign",
    "inmarsat_phone" => "Inmarsat Phone",
    "mobile_phone" => "Mobile Phone",
    "col" => "col",
    "satcom_id" => "SatCom ID",
    "ship_email" => "Ship Email",
    "Ship_fax" => "Ship Fax" );

  $sections['Dimensions'] = array(
    "breadth" => "Breadth Extreme",
    "draught" => "Draught",
    "height" => "Height",
    "keel_to_masthead" => "Keel To Masthead",
    "col" => "col",
    "length" => "Length Overall",
    "net_tonnage" => "Net Tonnage",
    "suez_net_tonnage" => "SUEZ Net Tonnage" );

  $sections['Engine'] = array(
    "engine_builder" => "Engine Builder",
    "engine_cylinders" => "Engine Cylinders",
    "engine_power" => "Engine Power",
    "engine_stroke" => "Engine Stroke",
    "col" => "col",
    "fuel_consumption" => "Fuel Consumption",
    "fuel_type" => "Fuel Type",
    "propeller" => "Propeller"  );

  $sections['Capacities'] = array(
    "ballast" => "Ballast",
    "ballast_segregated" => "Ballast (segregated)",
    "freshwater" => "Freshwater",
    "crude_capacity" => "Crude Capactity",
    "col" => "col",
    "liquid_oil" => "Liquid / Oil",
    "slops" => "Slops",
    "fuel" => "Fuel" );

  $sections['Historical Data'] = array(
    "exnames" => "Former Names",
    "yard_number" => "Yard Number",
    "date_of_order" => "Date of Order",
    "refit" => "Refit",
    "col" => "col",
    "keel_laid" => "Keel Laid",
    "build_end" => "Build End",
    "delivery_date" => "Delivery Date" );

  $sections['Additional Information'] = array(
    "class_assignment" => "Class Assignment",
    "speed_service" => "Speed (Service)",
    "hull_material" => "Hull Material",
    "hull_type" => "Hull Type",
    "col" => "col",
    "minimum_manning_officers" => "Minumum Manning (officers)",
    "minimum_manning_ratings" => "Minimum Manning (ratings)" );
/*
  $sections['Loadline'] = array(
    "deadweight_maximum_assigned" => "Deadweight (Max. Assigned)",
    "deadweight_normal_ballast" => "Deadweight (Normal Ballast)",
    "deadweight_segregated_ballast" => "Deadweight (Segregated Ballast)",
    "deadweight_tropical" => "Deadweight (Tropical)",
    "deadweight_winter" => "Deadweight (Winter)",
    "displacement_lightship" => "Displacement (Lightship)",
    "displacement_normal_ballast" => "Displacement (Normal Ballast)",
    "displacement_segregated_ballast" => "Displacement (Segregated Ballast)",
    "displacement_summer" => "Displacement (Summer)",
    "displacement_tropical" => "Displacement (Tropical)",
    "displacement_winter" => "Displacement (Winter)",
    "draft_lightship" => "Draft (Lightship)",
    "draft_normal_ballast" => "Draft (Normal Ballast)",
    "col" => "col",
    "draft_segregated_ballast" => "Draft (Segregated Ballast)",
    "draft_summer" => "Draft (Summer)",
    "draft_tropical" => "Draft (Tropical)",
    "draft_winter" => "Draft (Winter)",
    "draught_aft_normal_ballast" => "Draught Aft (Normal Ballast)",
    "draught_fore_normal_ballast" => "Draught Fore (Normal Ballast)",
    "freeboard_lightship" => "Draft (Lightship)",
    "freeboard_normal_ballast" => "Draft (Normal Ballast)",
    "freeboard_segregated_ballast" => "Draft (Segregated Ballast)",
    "freeboard_summer" => "Draft (Summer)",
    "freeboard_tropical" => "Draft (Tropical)",
    "freeboard_winter" => "Draft (Winter)",
    "fwa_summer_draft" => "FWA (Summer Draft)",
    "tpc_immersion_summer_draft" => "TPC Immersion (Summer Draft)" );
*/
?>