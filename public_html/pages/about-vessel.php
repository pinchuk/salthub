<?
  include( "about-vessel-sections.php" );
?>
  <style>
  .vessel_spec_item_edit {
    width:130px;
    margin-top:-3px;
  }
  .vessel_spec_item_edit select {
    width:135px;
  }
  </style>

  <div style="margin-left:0px;">
  <div class="subhead" style="margin-bottom:-5px;">Vessel Specifications</div>
<?

  $count = 0;
  foreach( $sections as $section => $val )
  {

    if( $section == "Dimensions" )
    {
      if( empty( $edit ) )
      {
  ?>
    <div style="text-align:center; clear:both; margin-top:10px; margin-bottom:10px;" id="more_vessel_details">
      <a href="javascript:void(0);" onclick="javascript: document.getElementById('more_vessel_details').style.display='none'; toggleSlide('vessel_details');">
        <span style="font-size:8pt;">more vessel details ...</span>
      </a>
    </div>
    <div id="vessel_details" style="display:none;">
  <?
      }
      else
      {
  ?>
    <div id="vessel_details">
  <?
      }
    }
  ?>

    <div class="vessel_spec_group" >

    <div class="vessel_spec_header"><?= $section ?></div>

    <div class="vessel_spec_col">
  <?
    $fields = $val;
    foreach( $fields as $field => $display )
    {
      if( $field == "col" )
      {
        echo '</div><div class="vessel_spec_col" style="margin-left:10px;">';
        continue;
      }

      echo '<div class="vessel_spec_item">';

      $narrow_data = ( $section == "Loadline" );
      if( $narrow_data )
      {
        echo '<div class="vessel_spec_item_title" style="width:200px;">' . $display . '</div>';
        echo '<div class="vessel_spec_item_value" style="width:40px;">';
      }
      else
      {
        echo '<div class="vessel_spec_item_title">' . $display . '</div>';
        echo '<div class="vessel_spec_item_value">';
      }

      switch( $field )
      {

        case "cat":
          if( $edit )
          {
            ?>
    				<select name="cat" class="vessel_spec_item_edit" style="width:135px;">
    				<?php
            $x = sql_query("select cat,catname from categories where industry='" . $page['type'] . "' order by catname");
    				while ($cat = mysql_fetch_array($x))
    					echo '<option ' . ($cat['cat'] == $page['cat'] ? 'selected' : '') . ' value="' . $cat['cat'] . '">' . $cat['catname'] . '</option>';
    				?>
    				</select>
            <?
          }
          else
          {
            echo $page['catname'];
          }
        break;

        default:
          if( $edit )
          {
            //We intend to edit the page
            ?>
            <input type="text" name="<?=$field?>" class="vessel_spec_item_edit" value="<?=$page['boat'][$field]?>" <? if($narrow_data) echo 'style="width:45px;"'; ?>>
            <?
          }
          else
          {
            if( $API->isLoggedIn() || isset( $anonymousFields[ $section ][ $field ] ) )
            {
              if( $page['boat'][$field] == "" )
                echo "-";
              else
                echo $page['boat'][$field];
            }
            else
            {
              echo '<a href="javascript:void(0);" onclick="javascript:getStarted3();" style="color:#ff8040;">Login to view</a>';
            }
          }
        break;
      }

      echo '</div>';
      echo '<div style="clear:both;"></div>';
      echo '</div>';

  ?>


  <?
    } // for each field
?>
    </div>
    <div style="clear:both;"></div>
    </div>
<?
  }  //For each section
  echo "</div></div>"; //Vessel Details Div
?>