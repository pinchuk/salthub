<?
/*
Displays the about screen for a Page.  Called from /profiles/profile.php

*/

      $isAdmin = quickQuery( "select admin from page_members where gid='$gid' and uid='" . $API->uid . "'" );
      $isAdmin = $isAdmin || $API->admin;

if( !$API->isLoggedIn() )
  include( $_SERVER["DOCUMENT_ROOT"] . "/signup/signup_popup.php" );

?>

<div id="pccontainer"></div>

<div style="font-size: 10pt; color: #555; padding: 5px 5px; line-height: 17px; margin: 0 4px 0 5px;" class="bgbluegrad pageabout" id="bluegrad">
	<div style="padding-bottom: 3px; font-size: 8pt;	font-family: tahoma;">
		<div id="descr-preview">
      <?=nl2br( cutoffText( $page['descr'], 300 ) );?>
      <div style="width:100%; text-align:center;">
        <a href="javascript:void(0);" onclick="javascript: document.getElementById('descr-preview').style.display='none'; document.getElementById('descr-full').style.display='';">show more ...</a>
      </div>
    </div>

		<div id="descr-full" style="display:none;">
      <?=nl2br($page['descr']);?>
      <div style="width:100%; text-align:center;">
        <a href="javascript:void(0);" onclick="javascript: document.getElementById('descr-preview').style.display=''; document.getElementById('descr-full').style.display='none';">show less ...</a>
      </div>
    </div>
		<!--<div style="text-align: right; font-size: 8pt;"><a href="<?=$page['url']?>">less&nbsp; <img src="/images/arrow_up.png" alt="" /></a></div>-->
	</div>

<? if( $page['type'] == PAGE_TYPE_BUSINESS ) { ?>
	<div style="padding-bottom: 3px; font-size: 8pt;	font-family: tahoma; margin-top:10px;">
		<div id="grpedit-products1">
<?
  $first = true;
  $q = mysql_query( "select catname from categories where cat IN (select cat as type_id from page_categories where gid='" . $gid . "') and industry !=" . PAGE_TYPE_BUSINESS . " order by catname" );
  if( mysql_num_rows( $q ) > 0 )
    echo 'Products and Services<br /><span style="color:rgb(50, 103, 152);">';

  while( $r = mysql_fetch_array( $q ) )
  {
    if( !$first ) echo ", ";

    echo $r['catname'];
    $first = false;
  }

  if( !$first ) echo "</span>";
?>
    </div>
	</div>

	<div style="padding-bottom: 3px; font-size: 8pt;	font-family: tahoma; margin-top:10px;">
		<div id="grpedit-services1">
<?
  $first = true;
  $q = mysql_query( "select catname from categories where cat IN (select cat as type_id from page_categories where gid='" . $gid . "') and industry =" . PAGE_TYPE_BUSINESS . " order by catname" );
  if( mysql_num_rows( $q ) > 0 )
    echo 'Industries<br /><span style="color:rgb(50, 103, 152);">';

  while( $r = mysql_fetch_array( $q ) )
  {
    if( !$first ) echo ", ";

    echo $r['catname'];
    $first = false;
  }

  if( !$first ) echo "</span>";
?>
    </div>
	</div>
<? } //End if pagetype = business ?>

<div style="clear: both;"></div>



<?
$map_available = false;

if( $page['type'] == PAGE_TYPE_BUSINESS && $page['address'] != ""  )
  $map_available = true;
else if( $page['type'] == PAGE_TYPE_VESSEL )
{
  if( $page['boat']['lat'] != 0 && $page['boat']['lon'] != 0 )
    $map_available = true;
//  if( quickQuery( "select count(*) from ais_data where imo='" . $page['boat']['imo'] . "' OR mmsi='" . $page['boat']['mmsi'] . "'" ) )
}

if( $map_available ) {
?>
  <a name="map"></a>
  <div class="subhead">Location</div><div style="clear: both; height: 5px;"></div>

<!--  <div style="color:rgb(85,85,85); font-size:8pt; margin-top:5px; font-family:tahoma;">
  Location
  </div>-->
  <div class="map_canvas" style="margin-bottom:5px;">
<? if( $page['type'] == PAGE_TYPE_VESSEL ) { ?>
   <iframe width="542" height="270" frameborder="0" scrolling="no" marginheight="0" marginwidth="2" id="map" class="map_canvas" src="/vessels/vessel_map.php?gid=<? echo $gid ?>&hideOthers=1&showTrack=0"></iframe>
<? } else { ?>
   <iframe width="542" height="270" frameborder="0" scrolling="no" marginheight="0" marginwidth="2" id="map" class="map_canvas" src="/pages/map.php?gid=<? echo $gid ?>"></iframe>
<? } ?>
  </div>

<? if( $page['type'] == PAGE_TYPE_VESSEL ) { ?>


  <div style="clear:both; font-size:9pt; padding-top:5px;">
    <div style="float:left; width:90px; text-align:left;"  >
      <a href="javascript:void(0);" onclick="javascript:<?if( !$API->isLoggedIn() ) { ?>getStarted3(); return false;<? } else { ?>openVesselMap( <?=$page['gid']?>, 0, 1 );<?}?>">Vessel's Track</a>
    </div>

    <div style="float:left; width:110px; text-align:center;">
      <a href="javascript:void(0);" onclick="javascript:<?if( !$API->isLoggedIn() ) { ?>getStarted3(); return false;<? } else { ?>openVesselMap( <?=$page['gid']?>, 0, 0 );<? } ?>">Live Chart</a>
    </div>

    <div style="float:left; width:100px; text-align:left;">
      <a href="<?=$API->getPageUrl($page['gid']);?>/logbook" <?if( !$API->isLoggedIn() ) { ?>onclick="javascript:getStarted3(); return false;"<?}?>>View Log Book</a>
    </div>

    <div style="float:right; font-size:9pt;">
        Last recorded position: <?= getTimeAgo( strtotime( $page['boat']['last_update'] ) ); ?>
    </div>
  </div>

  <div style="clear:both; padding-bottom:10px;"></div>

    <?
/*
      // Connect to Infobright instead of Standart DB Server
      mysql_close();
      $ibright = mysql_connect("10.179.38.20:5029", "salt_loader","xcjjsd3j892aAAsd");
      if($ibright){ 
          mysql_select_db("test_salthub_tracking_data", $ibright);      
      }else{ 
          include_once( "../inc/inc.php" );         
      } 
*/

    $sql = "select * from boats_ais_history where gid='" . $page['gid'] . "' order by last_update desc limit 5";
    $history_q = mysql_query( $sql );

    if( mysql_num_rows( $history_q ) > 0 ){
    ?>

    <div style="margin-bottom:3px;">
      <div class="subhead">Vessel History</div>
        <?$gid = $page['gid']; include( "vessel_history.php" );?>
    </div>
  <script language="javascript" type="text/javascript">
  <!--
  var last_history_id = '<?=urlencode($olderthan);?>';

  function showMoreVesselHistory(gid)
  {
      link = $('#vessel_history_show_more');

      if (!$(link).attr('data-wait') || $(link).attr('data-wait').length == 0)
      {
          $(link).attr('data-wait', $(link).html());
          $(link).html('<img src="/images/smwait.gif">');

          url = "/pages/vessel_history.php?gid=" + gid + "&olderthan=" + last_history_id + "&limit=10";

          $.ajax(url)
              .success(function (json)
              {
                  if (json.html.length == 0)
                  {
                      $('#vessel_history_show_more').hide()
                  }
                  else if (last_history_id > json.id)
                  {
                      last_history_id = json.id;

                      newdiv = $("<div></div>");
                      $(newdiv).html(json.html);
                      $(newdiv).hide();

                      $(newdiv).appendTo($('#vesselhistory')).slideDown(750, function ()
                          {
                              $(link).html($(link).attr('data-wait'));
                              $(link).attr('data-wait', '');
                          });
                  }
              });
      }
  }
  -->
  </script>
      <? if( $API->isLoggedIn() ) { ?>
        <div style="width:100%; text-align: center; font-size:8pt; height: 11px; line-height: 11px; padding: 10px;" id="vessel_history_show_more"><a href="javascript:void(0);" onclick="javascript:showMoreVesselHistory( <?=$gid?> );">show more &hellip;</a></div>
      <?
        }

    }
    if($ibright){
        mysql_close($ibright);
        include('../inc/sql.php');
   } 
  }
}


if ($page['type'] == PAGE_TYPE_VESSEL)
{
  include "about-vessel.php";
} //End if we're a boat.

if( $loggedIn ) { ?>

<div style="clear: both; height:10px;"></div>

	<div class="profiletop" style="background: transparent; border-top: 1px solid #d8dfea; padding-top: 0; margin: 0; padding-left:0px;">
		<div class="summaryinfo" style="width: 171px; height:190px;" id="si1">
<? /*
      <p/>
			<div class="left">Page Type:</div>
			<div class="value" id="grpedit-type0" style="display: none;">
				<select id="grpedit-type" onchange="javascript: refreshCategory(<? echo $page['gid']; ?>, selectedValue(this) );">
				<?php
				$x = sql_query("select cat,catname from categories where cattype='T' order by catname");
				while ($cat = mysql_fetch_array($x))
					echo '<option ' . ($cat['cat'] == $page['type'] ? 'selected' : '') . ' value="' . $cat['cat'] . '">' . $cat['catname'] . '</option>';
				?>
				</select>
			</div>
			<div class="value">
				<div class="value" id="grpedit-type1" style="color:#326798;"><?=$page['pagetype']?></div>
			</div>

*/
if( $page['type'] != PAGE_TYPE_BUSINESS  && $page['type'] != PAGE_TYPE_VESSEL ) { ?>

      <p/>
      <div id="category">
			<div class="left">Category:</div>
			<div class="value" id="grpedit-cat0" style="display: none;">
				<select id="grpedit-cat" onchange="javascript: refreshSubcatType(<? echo $page['gid']; ?>, selectedValue(this) );">
				<?php
        $x = sql_query("select cat,catname from categories where industry='" . $page['type'] . "' order by catname");
				while ($cat = mysql_fetch_array($x))
					echo '<option ' . ($cat['cat'] == $page['cat'] ? 'selected' : '') . ' value="' . $cat['cat'] . '">' . $cat['catname'] . '</option>';
				?>
				</select>
			</div>
			<div class="value">
				<div class="value" id="grpedit-cat1" style="color:#326798;"><?=$page['catname']?></div>
			</div>
      </div>

			<p />
<? if( $page['subcatname'] != "" ) { ?>
      <div id="subcat">
  			<div class="left">Sub category:</div>
  			<div class="value">
	  			<div class="value" style="color:#326798;"><?=$page['subcatname']?></div>
		  	</div>
      </div>
<? } } ?>

			<p />
			<div class="left">Accessible to:</div>
			<div class="value" id="grpedit-privacy0" style="display: none;">
				<select id="grpedit-privacy">
					<?php
					$priv = array(
						PRIVACY_EVERYONE => "The world",
						PRIVACY_SELECTED => "Only members"
						);

					foreach ($priv as $k => $v)
						echo '<option value="' . $k . '" ' . ($k == $page['privacy'] ? 'selected' : '') . '>' . $v .'</option>';
					?>
				</select>
			</div>
			<div class="value" id="grpedit-privacy1" style="color:#326798;">
				<?=$page['privacy'] == PRIVACY_EVERYONE ? "The world" : "Only members"?>
			</div>

    <div style="clear:both;"></div>

		</div>
		<div class="summaryinfo" style="width: 171px; height:190px;" id="si2">
<? if( $API->admin == 1) { ?>
      <p />
			<div id="grpedit-gname0" style="display: none;"><div class="left">Page Name:</div><br /><input type="text" id="grpedit-gname" value="<?=$page['gname']?>" /></div>
<? } ?>
			<p />
			Statistics:<br />
			<?=date("F j, Y", strtotime($page['created']))?><br />
			<?=plural($user['cfriends'], "member")?><br />
			<?=plural($vidCount, "video")?><br />
			<?=plural($picCount, "photo")?>
      <div style="clear:both;"></div>
		</div>
		<div class="summaryinfo" style="width: 171px; border-right: 0; margin-right: 0; height:190px;" id="si0">
				<p />
				<div class="left">Created by:</div>
				<div class="value" style="color: #555;">
	  		<?php
  			if ( empty($page['uid']) || $page['uid'] == "" ) {
          $gid = quickQuery( "select gid from pages where gname='$siteName'" );
          echo '<a style="font-weight:300;" href="' . $API->getPageURL($gid) . '">' . $siteName . '</a>';
         } else { ?>
          <a style="font-weight:300;" href="<?=$API->getProfileURL($page['uid'])?>"><?=$page['name']?></a><?=$page['uid'] == $API->uid ? '' : ' &nbsp;|&nbsp; ' . friendLink($page['uid'])?>
        <? } ?>
        </div>
				<p />



			Administrators: <? if( $isAdmin ) echo '<a style="font-weight:300; font-size:8pt;" href="javascript:void(0);" onclick="javascript:showPageMembers(' . $page['gid'] . ');">manage admins</a>';

  		$x = sql_query("select u.uid,username,name from page_members m inner join users u on m.uid=u.uid where m.gid=$gid and m.admin=1");

      if( mysql_num_rows( $x ) > 0 )
      {
				while ($user = mysql_fetch_array($x, MYSQL_ASSOC))
				{
					echo '<br /><a style="font-weight:300;" href="' . $API->getProfileURL($user['uid'], $user['username']) . '">' . $user['name'] . '</a>';
					if ($user['uid'] != $API->uid)
						echo " &nbsp;|&nbsp; " . friendLink($user['uid']);
				}


				if (isset($_GET['edit']) && $isAdmin)
				{
				?>
				<div style="padding: 20px; text-align:center;">
					<input type="button" class="button" value="Edit Members" onclick="javascript:showPageMembers(<?=$page['gid']?>);" style="width: auto;" />
				</div>
				<?php
				}
			}
			else
			{
        $gid2 = quickQuery( "select gid from pages where gname='$siteName'" );
        if( isset( $gid2 ) )
        {
          echo '<br /><a style="font-weight:300;" href="' . $API->getPageURL($gid2) . '">' . $siteName . '</a>';
        }
        else
          echo 'There are no administrators of this page.';
	    }

      if( isset( $isAdmin ) && $isAdmin == 1 )
      {
          $postAsPage = quickQuery( "select postAsPage from page_members where uid='" . $API->uid . "' and admin='1' and gid='$gid'" );
      ?>
        <div><a href="javascript:void(0);" onclick="javascript:adminPostSettings('<?=$postAsPage?>');">Page Settings</a></div>
        <div style="margin-top:5px;"><a href="javascript:void(0);" onclick="javascript: showPopupUrl( '/pages/rss_feed_popup.php?gid=<?=$page['gid']?>' );">Add your RSS Feed <img src="/images/feed_add.png" width="16" height="16" alt="" /></a></div>
      <?
      }
			?>

		</div>
	</div>
	<div style="clear: both; display: none; text-align: center; padding-top: 10px;" id="grpedit-save0">
		<input type="button" class="button" value="Save Changes" onclick="javascript:savePageAbout();" />
	</div>
	<div style="clear: both;" id="grpedit-save1"></div>

<?
}
else
{
  if( $page['type'] == PAGE_TYPE_VESSEL )
  {
?>
<div style="padding-top:10px; width:100%;">
    <?
    $view_type = "G";
    $link = $gid;
    include( "../pages/get_similar_vessels.php" ); ?>

</div>
<?
  }
}
?>

</div>
<?


if( !$isAdmin && $page['type'] == PAGE_TYPE_VESSEL && $API->isLoggedIn() ) { ?>
  <table cellpadding="5" cellspacing="0" align="center" bgcolor="#FFF9D7" style="border: 1px solid #E2C822; margin-top:10px;" width="550">
  <tr>
    <td><div style="padding: 9px 6px; font-weight: bold; font-size: 10pt; color: #000;">Suggestions: If you have something to add or see something which is incorrect, please submit suggestions using the feedback tool.</div>
    </td>
  </tr>
  </table>
<? }
?>



<script language="javascript" type="text/javascript">
<!--
<?
if (isset($_GET['pic']))
	echo 'showPhotoChanger();';

$gid = $page['gid'];

//$postAsPage = quickQuery( "select postAsPage from page_members where uid='" . $API->uid . "' and admin='1' and gid='$gid'" );
//if( empty( $postAsPage ) ) $postAsPage = 1;
//echo 'postAsPage = ' . $postAsPage . ';';
//var_dump();
//var_dump(empty($postAsPage));
?>
postAsPage = isPageAdmin = <?=strlen($postAsPage) > 0?$postAsPage:-1?>;
function adminPostSettings(type)
{
	if(isPageAdmin < 0){
        if(!confirm("You are not administrator of this page. This page settings are assotiated with specific page administrator. Are you realy sure to continue?")){
            return false;
        }
    }
    html = '<div style="height=200px; width:100%; padding:20px; margin-left:-20px;">';
	html += '	<div style="text-align:center;"><input type="checkbox" id="postAsPage" value="1"';
    if( postAsPage == 1 )
        html += ' CHECKED';
  html += '>Always comment and post on your page logbook as \'<?=addslashes($page['gname']);?>\',<br /> even when using <? echo $siteName ?> as \'<? echo $API->name ?>\'.<br /><br /><span style="font-size:8pt;">Note: if you deselct this, your posts will not appear in the Business News Feed<br /> located in the Business Directory.</span></div>';
 	html += ' <div style="text-align:center; clear:both; padding:10px; width:100%;">';
	html += '	<input type="button" class="button" onclick="javascript:UpdatePageSettings('+isPageAdmin+')" value="Update Settings">';
	html += '</div></div>';

	showPopUp("", html);
}

function UpdatePageSettings(checkPageAdmin)
{
    var pageAdmin = 1;
    if(checkPageAdmin < 0){
        pageAdmin = 0;
        if(!confirm("Are you realy sure to apply this settings for all page administrators?")){
            return false;
        }
    }
  e = document.getElementById( "postAsPage" );
    postAsPage = 0;
  if( e )
  {
    if( e.checked )
      postAsPage = 1;
  }
console.log('ajax');
	postAjax("/pages/update_page_settings.php", "postAsPage=" + postAsPage + "&gid=<? echo $gid ?>&is_page_admin="+pageAdmin);

  closePopUp(true);
}

<? if( strstr( $_SERVER['REQUEST_URI'], "LiveTrack" ) !== false  ) { ?>
  openVesselMap( <?=$gid?>, 0, 0 );
<? } else if( strstr( $_SERVER['REQUEST_URI'], "NavHistory" ) !== false  ) { ?>
  openVesselMap( <?=$gid?>, 0, 1 );
<? } ?>

<? if( isset( $_REQUEST['join'] ) && quickQuery( "select count(*) from page_members where uid=" . $API->uid . " and gid=" . $gid ) == 0 ) {
?>
joinHTML = '<div class=\'bluebutton\' style=\'cursor: default;\'>Joined</div>'; joinPage(<?=$gid?>, 'side', '<?=$page['gtype']; ?>');
<? } ?>

//-->
</script>
