<?php

/*
	this should never be called by a user - instead it is called when a user
	adds information to his about me section.  a pseudo-fork is created
	which adds the info to the page in the background
	
	at the point of calling the script, the page should already have been
	created.  this script just adds info from various sites to the page
*/

if (!isset($argv))
	die();

$_SERVER['DOCUMENT_ROOT'] = dirname(realpath( __FILE__ )) . "/..";

$site = $argv[1];
include $_SERVER['DOCUMENT_ROOT'] . "/inc/inc.php";

$gid = $argv[2];
$tid = $argv[3];

if ($gid > 0)
{
	$name = quickQuery("select gname from pages where gid=$gid");

	if (empty($name))
		die();
}
else
	$name = $tid;

switch ($tid)
{
	case PT_MOVIES:
		$info = getIMDBInfo($name);
		break;

	case PT_TV:
		$info = getTVDBInfo($name);
		break;

	case PT_BOOKS:
		$info = getGoogleBooksInfo($name);
		break;

	case PT_MUSIC:
    break;

	case PT_LIC:
	case PT_DEGREES:
    break;

	default:
		$info = getWikipediaInfo($name);
//		print_r($info);
		break;
}

if (!empty($info['img']))
{
	$API->uid = quickQuery("select uid from users where username='$siteName'");
	$photo_id = $API->saveImageFromURL($info['img']);
}

$query = "update pages set " . ($photo_id ? "pid=$photo_id," : "") . "descr='" . addslashes($info['descr']) . "'";
$query .= " where gid=$gid";

sql_query($query);

?>