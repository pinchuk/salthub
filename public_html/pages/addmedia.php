<?php
/*
Adds videos and photos to a Page.  The media is actually just a reference to a user's media; pages do not 'own'
the media directly. Uses the "page_media" table.
*/

include "../inc/inc.php";

$page = $API->getPageInfo(intval($_GET['gid']));

ob_start();

?>

<div style="clear: both; height: 10px;"></div>

<input type="text" id="txtgrpsearch" onkeyup="javascript:searchAddPageExistingMedia(this.value);" onfocus="javascript:if (this.value=='Search ...') this.value='';" onblur="javascript:if (this.value=='') this.value='Search ...';" class="txtsearch" style="border: 1px solid #d8dfea;" value="Search ..." />

<div style="clear: both; height: 5px;"></div>

<div style="overflow: auto; width: 480px; padding-right: 5px; height: 350px;">
<?php
foreach (array("V", "P") as $type)
{
	$count[$type] = 0;
	$word = typeToWord($type);
	?>
	<div id="agem-<?=$type?>head" class="subhead" style="margin-top: 5px;"><?=ucwords($word)?>s</div>
	<?php
	
	$q = "
			SELECT media.id,hash,'$type' as type,IF(gid={$page['gid']},1,0) AS inpage, " . ($type == "P" ? "IFNULL(ptitle,title) AS " : "") . "title, " . ($type == "P" ? "IFNULL(pdescr,descr) AS " : "") . " descr
			FROM {$word}s as media
			" . ($type =="P" ? "INNER JOIN albums ON media.aid=albums.id" : "") . "
			LEFT JOIN page_media gm ON media.id=gm.id AND gm.type='$type' and gid={$page['gid']}
			where media.uid={$API->uid}
		";
	
	$x = Xsql_query($q);

	while ($media = mysql_fetch_array($x, MYSQL_ASSOC))
	{
		$count[$type]++;
		if ($media['inpage'] == 1)
			$count['inpage']++;
		showSmallMediaPreview($media, $page['gid']);
		$medias[] = array("inpage" => $media['inpage'], "id" => $media['id'], "type" => $media['type'], "title" => $media['title']);
	}
}
?>
</div>

<?php
$html = ob_get_contents();
ob_end_clean();

ob_start();
?>

<div style="float: left; padding-right: 7px;">
	<a href="<?=$page['url']?>"><img src="<?=$page['thumb']?>" alt="" /></a>
</div>

<div style="overflow: hidden; font-size: 9pt; padding-top: 3px;">
	<span style="font-weight: bold;">Add media to <a href="<?=$page['url']?>"><?=$page['gname']?></a></span>
	<div style="border-top: 2px solid #d8dfea; padding-top: 5px; margin-top: 5px; font-size: 8pt;">
		<span style="font-weight: bold; font-size: 9pt;">View:&nbsp;</span>
		<a id="agem-a" href="javascript:void(0);" onclick="javascript:filterAddPageExistingMedia();" style="font-weight: bold;">all</a> &nbsp;|&nbsp;
		<a id="agem-v" href="javascript:void(0);" onclick="javascript:filterAddPageExistingMedia('V', null);">videos</a> &nbsp;|&nbsp;
		<a id="agem-p" href="javascript:void(0);" onclick="javascript:filterAddPageExistingMedia('P', null);">photos</a> &nbsp;|&nbsp;
		<a id="agem-i" href="javascript:void(0);" onclick="javascript:filterAddPageExistingMedia(null, 1);">in page</a> &nbsp;|&nbsp;
		<a id="agem-n" href="javascript:void(0);" onclick="javascript:filterAddPageExistingMedia(null, 0);">not in page</a>
	</div>
</div>

<?php
$html = ob_get_contents() . $html;
ob_end_clean();

echo json_encode(array("html" => /*str_replace("\n", "",*/ addslashes($html), "media" => $medias));
?>