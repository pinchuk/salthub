<?php
/*
Adds videos and photos to a Page.  The media is actually just a reference to a user's media; pages do not 'own'
the media directly. Send notifications out to page members.
*/

include "../inc/inc.php";

$type = $_POST['type'] == "V" ? "V" : "P";
$id = intval($_POST['id']);
$gid = intval($_POST['gid']);
$remove = $_POST['r'] == 1;


if( empty( $_POST['id'] ) )
{
  $type = $_GET['type'] == "V" ? "V" : "P";
  $id = intval($_GET['id']);
  $gid = intval($_GET['gid']);
  $remove = $_GET['r'] == 1;
}
//$words = count(explode(" ", $_POST['txt']));


if ($API->isUserMember(null, $gid) || $API->admin )
{
	if ($remove)
	{
		sql_query("delete from page_media where type='$type' and id=$id and gid=$gid");
		$link = "add to page";
	}
	else
	{
		sql_query("insert into page_media (type,id,gid,uid) values ('$type',$id,$gid,{$API->uid})");

    $to = null;
    $from = null;

    if( $type == "P" ) $notification = NOTIFY_GROUP_NEW_PHOTO;
    else $notification = NOTIFY_GROUP_NEW_VID;

    $postAsPage = quickQuery( "select postAsPage from page_members where admin=1 and gid=$gid and uid=" . $API->uid );

    if( $postAsPage == 1 )
    {
      $to = -1;
      $from = -1;
    }

    $API->sendNotificationToPage( $notification, array( "from" => ($from == -1 ? 0 : $API->uid), "gid" => $gid, "id" => $id ) );


		$API->feedAdd($type, $id, $to, $from, $gid);

    if( !$postAsPage )
    {
      //Add comment to the media page.
      $q = sql_query( "select gname, jmp from pages where gid='$gid'" );
      if( $r = mysql_fetch_array( $q ) )
      {
        $gname = $r['gname'];
        $jmp = $r['jmp'];

        if( empty($jmp ) || $jmp == "" )
        {
          $url = $API->getPageUrl( $gid, $gname );
          $jmp = shortenURL( "http://" . SERVER_HOST . $url );
          sql_query( "update pages set jmp='$jmp' where gid='$gid' limit 1" );
        }
        $jmp = str_replace( "\n", "", $jmp );

        if( $type == "V" )
        {
          $comment = "I shared this video with the '$gname' page. http://www.shb.me/" . $jmp . ". Click on the image to comment, share or view other great videos.";

          $x = mysql_query("select id,title,descr,uid,jmp,hash from videos where id=$id");
          $info = mysql_fetch_array($x, MYSQL_ASSOC);
        }
        else
        {
          $comment = "I shared this photo with the '$gname' page. http://www.shb.me/" . $jmp . ". Click on the image to comment, share or view other great photos.";

          $x = mysql_query("select id,ptitle as title,pdescr as descr,uid,jmp,hash,aid from photos where id=$id");
          $info = mysql_fetch_array($x, MYSQL_ASSOC);

          if( $info['title'] == NULL ) $info['title'] = quickQuery( "select title from albums where id='" . $info['aid'] . "'" );
          if( $info['descr'] == NULL ) $info['descr'] = quickQuery( "select descr from albums where id='" . $info['aid'] . "'" );
          unset( $info['aid'] );
        }
        $info['type'] = $type;

        sql_query("insert into comments (type,link,comment,uid) values ('$type',$id,'" . addslashes($comment) . "'," . $API->uid . ")");

        if ($API->isNotificationEnabled($API->uid, NOTIFY_PREF_FB_PAGE_MEDIA_SHARED))
          $API->fbStreamPublish($API->uid, $comment, $info);

    		if ($API->isNotificationEnabled($API->uid, NOTIFY_PREF_TW_PAGE_MEDIA_SHARED))
    			$API->twUpdateStatus($API->uid, $comment);

      }
    }

		$link = "remove from page";
	}
}

echo json_encode(array("id" => $gid . $type . $id, "text" => $link));

?>