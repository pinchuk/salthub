<?
/*
Displays a <select> box that loads the categories in a particular industry.  Used by about-edit.php.
*/

include_once( "../inc/inc.php" );

if( empty( $page ) )
{
  $gid = $_GET['gid'];
  $type = $_GET['type'];

  $page = queryArray( "select type,cat from pages where gid='$gid'" );

  if( $page['type'] != $type )
  {
    $page['type'] = $type;
    $page['cat'] = 0;
  }
}

$x = sql_query("select cat,catname from categories where industry='" . $page['type'] . "' order by catname");

if( mysql_num_rows( $x ) > 0 )
{
?>
	<div class="left">Category:</div>
	<div class="value" id="grpedit-cat0" style="display: none;">
		<select id="grpedit-cat" onchange="javascript: refreshSubcatType(<? echo $gid; ?>, selectedValue(this) );">
		<?php
		while ($cat = mysql_fetch_array($x))
			echo '<option ' . ($cat['cat'] == $page['cat'] ? 'selected' : '') . ' value="' . $cat['cat'] . '">' . $cat['catname'] . '</option>';
		?>
		</select>
	</div>
	<div class="value">
		<div class="value" id="grpedit-cat1" style="color:#326798;"><?=$page['catname']?></div>
	</div>
<?
}

?>