<?
/*
This is a popup that is presented to the user when there is no category selected for a particular page.
The purpose is to make sure the category selections are correct after new pages are created and accessed.
*/

include "../inc/inc.php";

$gid = $_REQUEST['gid'];
$cat = quickQuery( "select cat from pages where gid='$gid'" );
?>

<div style="width:100%">
  <div style="text-align:center;">
  <b>Please choose a category for this page</b>
  </div>

  <div style="margin-top:20px;">
     <select name="cat" id="cat" style="margin-left:20px; margin-right:10px; float:left;" onchange="document.getElementById('subcat111').style.display='none'; document.getElementById('subcat112').style.display='none'; if( this.value==111 || this.value==112 ) document.getElementById('subcat' + this.value).style.display='';">
<?
$q = sql_query( "select * from categories where cattype='G'" );
while( $r = mysql_fetch_array( $q ) )
{
?>
    <option value="<? echo $r['cat']; ?>"<? if( $cat == $r['cat'] ) echo " SELECTED"; ?>><? echo $r['catname']; ?></option>
<?
}
?>
    </select>

     <select name="subcat111" style="float:left; <? if( $cat != 111 ) { ?>display:none;<? } ?>" id="subcat111">
<?
$q = sql_query( "select * from categories where cattype='C'" );
while( $r = mysql_fetch_array( $q ) )
{
?>
    <option value="<? echo $r['cat']; ?>"><? echo $r['catname']; ?></option>
<?
}
?>
    </select>

     <select name="subcat112" style="float:left; <? if( $cat != 112 ) { ?>display:none;<? } ?>" id="subcat112">
<?
$q = sql_query( "select * from categories where cattype='B'" );
while( $r = mysql_fetch_array( $q ) )
{
?>
    <option value="<? echo $r['cat']; ?>"><? echo $r['catname']; ?></option>
<?
}
?>
    </select>

  </div>

  <div style="clear:both; padding-top: 20px;">
    <input type="button" name="submit" value="Save Category Selection" onclick="saveCatSelection();" style="margin-left:130px;"/>
  </div>

  <div style="clear:both;"></div>
</div>

