<?php
/*
Step 2 in the claim a page process - collects user information.
*/

include "../header.php";

$gid = $_GET['gid'];
$type = quickQuery( "select catname from pages left join categories on categories.cat=pages.cat where pages.gid='$gid'" );
$type = strtolower( $type );

if( $type != "School" )
  $type = "Company";

if( empty( $gid ) ) { echo "Invalid page;"; include "../footer.php"; die(); }

$website = quickQuery( "select www from pages where gid='$gid'" );

if( isset( $website ) && $website != "")
{
  $website = str_replace( "www.", "", $website );
  $website = str_replace( "http://", "", $website );
  $website = "@" . $website;
}
else
{
/*
  $website = "";
?>
  <div style="margin:20px;">
    <div style="margin:15px;">We're sorry, automated page verification is not available for<br /> this page. Please contact
    <a href="mailto:cr@salthub.com?subject=Claiming a page&body=Hello, I would like to claim this page: http://<?= SERVER_HOST . $API->getPageUrl($gid); ?>"><? echo $siteName ?></a> to claim this page.<div style="clear:both;"></div></div>
  </div>
<?
  include "../footer.php";
  exit;
*/
}

$company = quickQuery( "select gname from pages where gid='$gid'" );
$name = quickQuery( "select name from users where uid='" . $API->uid . "'" );
$name_array = explode( " " , $name );
$fname = $name_array[0];
$lname = $name_array[1];
?>


<form action="claim_company3.php" method="POST" onsubmit="return submitForApproval(this);">
<input type="hidden" name="gid" value="<? echo $gid ?>" />

<div style="width:90%; margin-left:150px;">
  <div style="float:left; font-size:35px; margin-top:5px;">
    Claim Your <? echo ucfirst( $type ); ?>
  </div>
  <div style="float:left; margin-left:20px;"><img src="/images/checkmark_sticker.png" width="48" height="48" alt=""/></div>

  <div style="clear:both; width:100%; height:1px; border:0px; border-top: 1px; border-style:solid; border-color:#d8dfea; margin:0px;"></div>

  <div style="width:60%; font-size:12px;">
<!--  Claiming your <? echo $type; ?> has many benefits, including the ability to build and manage your customer relations. Fill out the form below and your <? echo $type; ?> will soon be verified.-->
  Claiming your <? echo $type; ?> has many benefits including having it added to the business maritime directory, adding your RSS feed, including the ability to build and manage your customer relations and more.
  <br /><br />
  <i>* All fields required</i>
  </div>

  <div class="thead">Confirmation Email:</div>
  <div class="tcontent">
    <input type="text" name="email" value="" style="width: <? if( $website == "" ) echo "200"; else echo "100"; ?>px;" /><? echo $website ?>

    <div style="margin-top:10px;">
<? if( $website == "" ) { ?>
      We will contact you by phone and or send a confirmation notice to the email provided above. Upon verification that you are a company representative, you will become the administrator of your page. Please be sure you have access to you phone and email.
<? } else { ?>
      We will send a confirmation notice to the email provided above. This email will have a link in it which will verify you as the <? echo $type; ?> representative. Please be sure you have access to this email.
      <p />
      Please be sure to check your spam and junk folders to confirm your email address and to verify your page.
<? } ?>
    </div>
  </div>

  <div class="thead">First Name:</div>
  <div class="tcontent">
    <input type="text" name="fname" maxlength="25" value="<? echo $fname ?>" style="width: 200px;" readonly/>
  </div>

  <div class="thead">Last Name:</div>
  <div class="tcontent">
    <input type="text" name="lname" maxlength="25" value="<? echo $lname ?>" style="width: 200px;" readonly/>
  </div>

  <div class="thead">Position at Company:</div>
  <div class="tcontent">
    <input type="text" name="position" maxlength="25" value="" style="width: 200px;" />
  </div>

  <div class="thead">Phone:</div>
  <div class="tcontent">
    <input type="text" name="phone" maxlength="25" value="" style="width: 200px;" />
  </div>

  <div class="thead"><? echo ucfirst( $type ); ?> Name:</div>
  <div class="tcontent">
    <input type="text" name="company" maxlength="25" value="<? echo $company ?>" style="width: 200px;" />
  </div>

  <div class="thead">Address:</div>
  <div class="tcontent">
    <textarea name="address" id="address" rows="5" cols="25" style="width:200px;"><?= quickQuery( "select address from pages where gid='$gid'" ); ?></textarea>
  </div>

  <div class="thead"></div>
  <div class="tcontent">
    <?= $siteName ?> is for connecting with your coworkers at <?= str_replace("@", "", $website ); ?>. Add a few colleagues to grow your network. These coworkers will connect with others, allowing your company to be discovered through their connections. The more you add, the more success you will have.
    <p/>
    <span style="color:#000000">I work with:</span><br />
    <? for( $c = 1; $c <= 5; $c++ ) { ?>
    <input type="text" name="email<?= $c ?>" maxlength="25" value="" style="width: 200px;" /><? echo $website ?><br />
    <? } ?>
  </div>


  <div style="clear: both; padding-top:20px; margin-left:160px; margin-bottom:30px;">
    <div style="float:left;">
    	<input type="button" value="Cancel" class="button" onclick="javascript:location='<? echo $API->getPageUrl($gid); ?>';"/>
    	<input type="submit" value="Submit for Approval" class="button" style="margin-left:20px;"/>
    </div>
    <div class="tcontent" style="float:left; padding-left:20px;">* For help, use the feedback tool at the top left of this page.</div>
  </div>
</div>
</form>
<script language="javascript" type="text/javascript">
<!--

function submitForApproval(form)
{
  if( form.email.value == "" || form.fname.value == "" || form.lname.value=="" || form.position.value=="" || form.phone.value == "" || form.company.value=="" || form.address.value =="" )
  {
    alert('All fields must be completed before submitting this form.');
    return false;
  }
  return true;
}

//-->
</script>

<?php
include "../footer.php";
?>