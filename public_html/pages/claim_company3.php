<?php
/*
Final step in the claim a page process; sends out the emails in which the user must verify that
their email address is correct.  The email's link comes back to this page, which gives the user
ownership upon successful validation.

1/4/2012 - Added "page_verify" email, and support for adding people through the claim company page
5/2/2012 - Modified text sizing/spacing


*/


include "../inc/inc.php";


$API->setToDoItem( TODO_COMPANY_PAGE );

if( isset( $_GET['cc'] ) )
{
  $cc = $_GET['cc'];
  $id = $_GET['id'];

  $gid = quickQuery( "select gid from company_confirmations where ID='$id'" );

  if( empty( $gid ) ) { die("Invalid company"); }

  $hash = $API->generateDeleteCommentHash($API->uid . chr(2) . $gid . chr(2) . $id );

  if( $hash != $cc ) { die( "Invalid hash" ); }

  //If we get here, we're good.
  sql_query( "update company_confirmations set confirmed=1 where id='$id'" );

  $API->verifyPage( $gid );

  header( "Location: " . $API->getPageUrl( $gid ) );

}
else
{
  $gid = $_POST['gid'];

  if( empty( $gid ) ) { echo "Invalid page;"; include "../footer.php"; die(); }

  $website = quickQuery( "select www from pages where gid='$gid'" );

  if( isset( $website ) && $website != "" )
  {
    $website = str_replace( "www.", "", $website );
    $website = str_replace( "http://", "", $website );
    $website = "@" . $website;
  }
  else
    $website = "";

  $email = $_POST['email'] . $website;
  $lname = $_POST['lname'];
  $fname = $_POST['fname'];
  $company = $_POST['company'];
  $phone = $_POST['phone'];
  $position = $_POST['position'];
  $address = $_POST['address'];

  sql_query( "insert into company_confirmations (company, fname, lname, position, phone, address, gid, uid, email) values ('$company', '$fname', '$lname', '$position', '$phone', '$address', $gid, " . $API->uid . ", '$email')" );

  echo mysql_error();

  $id = mysql_insert_id();

  $uid = $API->uid;

  if( $website != "" )
  {
    $confcode = $API->generateDeleteCommentHash($API->uid . chr(2) . $gid . chr(2) . $id );

    $msg = quickQuery("select content from static where id='company_confirm'");
    $msg = str_replace("{NAME}", $_SESSION['name'], $msg);
    $msg = str_replace("{SITENAME}", $siteName, $msg);
    $msg = str_replace("{URL}", "http://" . $_SERVER['HTTP_HOST'] . "/pages/claim_company3.php?cc=$confcode&id=$id", $msg);
    $msg = textToHTML($msg);

    emailAddress($email, "Confirm your e-mail address on $siteName", $msg, "cr");


    //Send emails to other people
    $from = quickQuery("select name from users where uid=" . $API->uid);


    $template = quickQuery("select content from static where id='page_join'" );
    $page = $API->getPageInfo($gid);
    $gname = $page['gname'];
    $url = "http://www." . $siteName . ".com/?joingid=$gid&uid=" . $API->uid;

    $subj = "I thought you might like \"$gname\" on $siteName";

    $email_body = str_replace( "{NAME_FROM}", $from, nl2br($template) );
    $email_body = str_replace( "{PAGE}", $gname, $email_body );
    $email_body = str_replace( "{REF_LINK}", "<a href=\"$url\">$url</a>", $email_body );
    $email_body = str_replace( "{SITENAME}", $siteName, $email_body );

  	$userEmail = quickQuery("select email from users where uid=" . $API->uid);

    for( $c = 1; $c <= 5; $c++ )
  	{
      $email = $_POST['email' . $c];
      if( $email != '' )
      {
        $email .= $website;
    	  if (isEmailAddressValid($email))
  	  		emailAddress($email, $subj, $email_body, $userEmail);
      }
   	}
  }
  else
  {
    $url = "http://www." . $siteName . ".com" . $API->getPageURL( $gid );

    $msg = "A new company confirmation has been submitted:\n";
    $msg .= "Page Url: <a href=\"$url\">" . $url . "</a>\n\n";

    $msg .= "Name: " . $fname . " " . $lname . "\n";
    $msg .= "Email: " . $email . "\n";
    $msg .= "Phone: " . $phone . "\n";
    $msg .= "Position: " . $position . "\n";
    $msg .= "Company: " . $company . "\n";
    $msg .= "Address: " . $address . "\n";
    $msg .= "Work With 1: " . $_POST['email1'] . "\n";
    $msg .= "Work With 2: " . $_POST['email2'] . "\n";
    $msg .= "Work With 3: " . $_POST['email3'] . "\n";
    $msg .= "Work With 4: " . $_POST['email4'] . "\n";
    $msg .= "Work With 5: " . $_POST['email5'] . "\n\n";

    emailAddress("cr@salthub.com", "New Company Confirmation Submission", nl2br( $msg ), $email );

    mysql_query( "insert into feedback (text,uid,feedback_time) values ('" . addslashes( $msg ) . "', '" . $API->uid . "', Now() )" );
  }
}



include "../header.php";

$type = quickQuery( "select catname from pages left join categories on categories.cat=pages.cat where pages.gid='$gid'" );
$type = strtolower( $type );
if( $type != "School" )
  $type = "Company";
?>



<div style="width:70%; margin-left:150px;">
  <div style="float:left; font-size:35px; margin-top:5px;">
    Claim Your Company
  </div>
  <div style="float:left; margin-left:20px;"><img src="/images/checkmark_sticker.png" width="48" height="48" alt=""/></div>

  <div style="clear:both; width:100%; height:1px; border:0px; border-top: 1px; border-style:solid; border-color:#d8dfea; margin:0px;"></div>

  <div style="width:100%; font-size:24px; font-weight:bold; margin-bottom:5px; padding-top:5px;">
  Thank You
  </div>

  <div style="width:100%; font-size:12px;margin-bottom:20px; margin-left:5px;">
  A confirmation email was sent to the provided email address. This email will have a link in it which will verify you as the <? echo $type ?> representative. Please click the link to complete the confirmation process.
  <br /><br /><br />
  - The <?= $siteName ?> Team
  </div>

</div>

<?php
include "../footer.php";
?>