var frE, frC, addToLogPending, pagesToAdd;

function showHeaderNotifications()
{
	document.getElementById("notifications_container").style.display = "inline";
	
	getAjax("/messaging/notifications.php?style=2", function (html)
	{
		document.getElementById("headnotifications").innerHTML = html;
	});
}

function gymlShuffle()
{
	getAjax("/pages/previews.php?o=r&limit=3", function(js)
	{
		eval('data = ' + js);
		document.getElementById("gyml").innerHTML = data.html;
	}
	);
}

function logMouseOver(f, over)
{
	e = document.getElementById("user-" + f + "-edit");
	
	if (e)
		e.style.visibility = over ? "visible" : "hidden";
}

function showIfExists(id, show)
{
	e = document.getElementById(id);
	
	if (e)
		e.style.display = show ? "inline" : "none";
}

function shareInPage(type, id, hash)
{
	getAjax("/pages/share.php?type=" + type + "&id=" + id + "&hash=" + hash, shareInPageHandler);
}

var gids = null;
function shareInPageHandler(x)
{
  data = "";

  if( x != "" )
  {
    chunks = x.split(String.fromCharCode(1));
    html = chunks[0];
    gids = chunks[1].split(",");
  }

	showPopUp( '', html, [515, 487], 'no');
}

function searchShareInPage(value)
{
/*
	q = q.toLowerCase();

	for (var i in pagesToAdd)
	{
		e = document.getElementById("gp-" + pagesToAdd[i].gid);
		if (e)
			e.style.display = pagesToAdd[i].gname.toLowerCase().indexOf(q) == -1 ? "none" : "";
	}
*/
  if( !gids ) return;

  value = value.toUpperCase();

  for( i = 0; i < gids.length; i++ )
  {
    parent = document.getElementById( "media-" + gids[i] );
    searchData = document.getElementById( "media-search-" + gids[i] );

    if( parent && searchData )
    {
      var term = searchData.innerHTML.toUpperCase();
      if( value == null || term.indexOf( value ) >= 0 )
        parent.style.display='';
      else
        parent.style.display='none';
    }
  }
}
/*
function sortPagesToAdd(type, id, sort)
{
	getAjax("/pages/previews.php?o=m&type=" + type + "&id=" + id + "&sort=" + sort, sortPagesToAddHandler);
}

function sortPagesToAddHandler(x)
{
	eval('data = ' + x + ';');
	document.getElementById("pagestoadd").innerHTML = data.html;
	pagesToAdd = data.pages;
}
*/

function addPageExistingMedia(type, id, gid, remove)
{
//	e = document.getElementById("addlink-" + gid + type + id);

  if( remove ) remove = '1';
  else remove = '0';

	postAjax("/pages/addpagemedia.php", "r=" + remove + "&type=" + type + "&id=" + id + "&gid=" + gid, "addPageExistingMediaHandler");
}

function addPageExistingMediaHandler(x)
{
//  alert( x );
//	eval('data = ' + x + ';');
//	document.getElementById("addlink-" + data.id).innerHTML = data.text;

  if (typeof showNewLogEntries == "function")
  {
    showNewLogEntries();
  }
}

function addToLog(type, id, newCaption, e)
{
	if (e.innerHTML == newCaption) return;

	addToLogPending = [e, newCaption];

	postAjax("/profile/addtolog.php", "type=" + type + "&id=" + id, "addToLogHandler");
}

var joinPageTab = 0;

function joinPageChangeTab( tab )
{
  for( i = 0; i < 4; i++ )
  {
    e = document.getElementById('jg' + i );
    if( e )
      e.style.display='none';

    e = document.getElementById('jt' + i );
    if( e )
      e.style.fontWeight='';
  }

  e = document.getElementById('jg' + tab );
  if( e )
    e.style.display='';

  e = document.getElementById('jt' + tab );
  if( e )
    e.style.fontWeight='bold';

  joinPageTab = tab;
}

var occupation;
var employer;

function joinPage(gid, hash, type)
{
  html = '<div>';
  html += '  <div style="font-size:16px; font-weight:bold;">Tell us about your relationship to this page</div>';
  html += '  <div style="font-size:11px; margin-bottom:6px;">(select the one below that best fits and complete)</div>';

//Header
  html += '    <div style="float:left; font-weight:bold; font-size:13px;" id="jt0"><a href="javascript:void(0);" onclick="javascript:joinPageChangeTab(0);">I\'m a fan</a></div>';
if( type == 'S' || type == 'C' || type=='B')
  html += '    <div style="float:left; margin-left:25px; font-size:13px;" id="jt1"><a href="javascript:void(0);" onclick="javascript:joinPageChangeTab(1);">I work(ed) here</a></div>';
if( type == 'S' )
  html += '    <div style="float:left; margin-left:25px; font-size:13px;" id="jt2"><a href="javascript:void(0);" onclick="javascript:joinPageChangeTab(2);">I attend(ed) school here</a></div>';
if( type == 'O' )
  html += '    <div style="float:left; margin-left:25px; font-size:13px;" id="jt3"><a href="javascript:void(0);" onclick="javascript:joinPageChangeTab(3);">I work(ed) in this profession</a></div>';
if( type == 'L' )
  html += '    <div style="float:left; margin-left:25px; font-size:13px;" id="jt4"><a href="javascript:void(0);" onclick="javascript:joinPageChangeTab(4);">I have this license/degree</a></div>';

  html += '   <div style="clear:both; height:5px;"></div>';
  html += '    <div id="jg0" style="width:100%; height:130px;">';
  html += '      <div style="float:left; padding-top:4px; font-size:12px;">yes </div>';
  html += '      <div style="float:left; padding-top:1px;"><input type="checkbox" name="junk" CHECKED></div>';
  html += '      <div style="float:left; padding-top:4px;"><span style="font-size:11px;"> join as member</span></div>';
  html += '    </div>';

//Work
  html += '    <div id="jg1" style="display:none; width:100%; height:130px;">';

  html += '    <div style="float: left; font-size: 9pt;">';
  html += '      <div class="smtitle2">Period:</div>';
  html += '      <select id="month-0-X" style="margin-right: 5px;"><option value=""></option><option  value="1">January</option><option  value="2">February</option><option  value="3">March</option><option  value="4">April</option><option  value="5">May</option><option  value="6">June</option><option  value="7">July</option><option  value="8">August</option><option  value="9">September</option><option  value="10">October</option><option  value="11">November</option><option  value="12">December</option></select><select id="year-0-X"><option value=""></option><option  value="1970">1970</option><option  value="1971">1971</option><option  value="1972">1972</option><option  value="1973">1973</option><option  value="1974">1974</option><option  value="1975">1975</option><option  value="1976">1976</option><option  value="1977">1977</option><option  value="1978">1978</option><option  value="1979">1979</option><option  value="1980">1980</option><option  value="1981">1981</option><option  value="1982">1982</option><option  value="1983">1983</option><option  value="1984">1984</option><option  value="1985">1985</option><option  value="1986">1986</option><option  value="1987">1987</option><option  value="1988">1988</option><option  value="1989">1989</option><option  value="1990">1990</option><option  value="1991">1991</option><option  value="1992">1992</option><option  value="1993">1993</option><option  value="1994">1994</option><option  value="1995">1995</option><option  value="1996">1996</option><option  value="1997">1997</option><option  value="1998">1998</option><option  value="1999">1999</option><option  value="2000">2000</option><option  value="2001">2001</option><option  value="2002">2002</option><option  value="2003">2003</option><option  value="2004">2004</option><option  value="2005">2005</option><option  value="2006">2006</option><option  value="2007">2007</option><option  value="2008">2008</option><option  value="2009">2009</option><option  value="2010">2010</option><option  value="2011">2011</option><option  value="2012">2012</option></select>';
  html += '      <div style="padding: 5px; text-align: center;" id="enddate-X2">to</div>';
  html += '      <div id="enddate-X" style="padding-bottom: 5px;"><select id="month-1-X" style="margin-right: 5px;"><option value=""></option><option  value="1">January</option><option  value="2">February</option><option  value="3">March</option><option  value="4">April</option><option  value="5">May</option><option  value="6">June</option><option  value="7">July</option><option  value="8">August</option><option  value="9">September</option><option  value="10">October</option><option  value="11">November</option><option  value="12">December</option></select><select id="year-1-X"><option value=""></option><option  value="1970">1970</option><option  value="1971">1971</option><option  value="1972">1972</option><option  value="1973">1973</option><option  value="1974">1974</option><option  value="1975">1975</option><option  value="1976">1976</option><option  value="1977">1977</option><option  value="1978">1978</option><option  value="1979">1979</option><option  value="1980">1980</option><option  value="1981">1981</option><option  value="1982">1982</option><option  value="1983">1983</option><option  value="1984">1984</option><option  value="1985">1985</option><option  value="1986">1986</option><option  value="1987">1987</option><option  value="1988">1988</option><option  value="1989">1989</option><option  value="1990">1990</option><option  value="1991">1991</option><option  value="1992">1992</option><option  value="1993">1993</option><option  value="1994">1994</option><option  value="1995">1995</option><option  value="1996">1996</option><option  value="1997">1997</option><option  value="1998">1998</option><option  value="1999">1999</option><option  value="2000">2000</option><option  value="2001">2001</option><option  value="2002">2002</option><option  value="2003">2003</option><option  value="2004">2004</option><option  value="2005">2005</option><option  value="2006">2006</option><option  value="2007">2007</option><option  value="2008">2008</option><option  value="2009">2009</option><option  value="2010">2010</option><option  value="2011">2011</option><option  value="2012">2012</option></select></div>';
  html += '  	 </div>';

  html += '	  	 <div style="float: left; margin-left:20px; font-size:9pt;">';
  html += '			  <div class="smtitle2" style="margin-bottom:20px;">Position:<br /><input type="text" style="width: 181px;" id="occupation" value="" /></div>';
  html += '	  		<div style="clear:both; float:left;"><input type="checkbox"  id="present-X" onchange="javascript:document.getElementById(\'enddate-X\').style.display = this.checked ? \'none\' : \'\'; javascript:document.getElementById(\'enddate-X2\').style.display = this.checked ? \'none\' : \'\';" /></div>';
  html += '       <div style="float:left; padding-top:3px;"> I currently work here</div>';
  html += '		   </div>';


  html += '   </div>';

//Eduction
  html += '    <div id="jg2" style="display:none; width:100%; height:130px; font-size: 9pt;">';

  html += '      <div class="smtitle2">Period:</div>';
  html += '        <div style="float:left; margin-right:5px;"><select id="year-0-Y"><option value=""></option><option  value="1970">1970</option><option  value="1971">1971</option><option  value="1972">1972</option><option  value="1973">1973</option><option  value="1974">1974</option><option  value="1975">1975</option><option  value="1976">1976</option><option  value="1977">1977</option><option  value="1978">1978</option><option  value="1979">1979</option><option  value="1980">1980</option><option  value="1981">1981</option><option  value="1982">1982</option><option  value="1983">1983</option><option  value="1984">1984</option><option  value="1985">1985</option><option  value="1986">1986</option><option  value="1987">1987</option><option  value="1988">1988</option><option  value="1989">1989</option><option  value="1990">1990</option><option  value="1991">1991</option><option  value="1992">1992</option><option  value="1993">1993</option><option  value="1994">1994</option><option  value="1995">1995</option><option  value="1996">1996</option><option  value="1997">1997</option><option  value="1998">1998</option><option  value="1999">1999</option><option  value="2000">2000</option><option  value="2001">2001</option><option  value="2002">2002</option><option  value="2003">2003</option><option  value="2004">2004</option><option  value="2005">2005</option><option  value="2006">2006</option><option  value="2007">2007</option><option  value="2008">2008</option><option  value="2009">2009</option><option  value="2010">2010</option><option  value="2011">2011</option><option  value="2012">2012</option></select></div>';
  html += '        <div id="enddate-Y" style="float:left; margin-left:5px;";>to <select id="year-1-Y"><option value=""></option><option  value="1970">1970</option><option  value="1971">1971</option><option  value="1972">1972</option><option  value="1973">1973</option><option  value="1974">1974</option><option  value="1975">1975</option><option  value="1976">1976</option><option  value="1977">1977</option><option  value="1978">1978</option><option  value="1979">1979</option><option  value="1980">1980</option><option  value="1981">1981</option><option  value="1982">1982</option><option  value="1983">1983</option><option  value="1984">1984</option><option  value="1985">1985</option><option  value="1986">1986</option><option  value="1987">1987</option><option  value="1988">1988</option><option  value="1989">1989</option><option  value="1990">1990</option><option  value="1991">1991</option><option  value="1992">1992</option><option  value="1993">1993</option><option  value="1994">1994</option><option  value="1995">1995</option><option  value="1996">1996</option><option  value="1997">1997</option><option  value="1998">1998</option><option  value="1999">1999</option><option  value="2000">2000</option><option  value="2001">2001</option><option  value="2002">2002</option><option  value="2003">2003</option><option  value="2004">2004</option><option  value="2005">2005</option><option  value="2006">2006</option><option  value="2007">2007</option><option  value="2008">2008</option><option  value="2009">2009</option><option  value="2010">2010</option><option  value="2011">2011</option><option  value="2012">2012</option></select></div>';
  html += '	  		 <div style="clear:both; float:left;"><input type="checkbox" id="present-Y" onchange="javascript:document.getElementById(\'enddate-Y\').style.display = this.checked ? \'none\' : \'\';" /></div>';
  html += '        <div style="float:left;  padding-top:3px;"> I currently attend</div>';
  html += '      </div>';

  html += '    </div>';


//Employer
  html += '    <div id="jg3" style="display:none; width:100%; height:130px; font-size: 9pt;">';

  html += '    <div style="float: left; font-size: 9pt;">';
  html += '      <div class="smtitle2">Period:</div>';
  html += '      <select id="month-0-Z" style="margin-right: 5px;"><option value=""></option><option  value="1">January</option><option  value="2">February</option><option  value="3">March</option><option  value="4">April</option><option  value="5">May</option><option  value="6">June</option><option  value="7">July</option><option  value="8">August</option><option  value="9">September</option><option  value="10">October</option><option  value="11">November</option><option  value="12">December</option></select><select id="year-0-Z"><option value=""></option><option  value="1970">1970</option><option  value="1971">1971</option><option  value="1972">1972</option><option  value="1973">1973</option><option  value="1974">1974</option><option  value="1975">1975</option><option  value="1976">1976</option><option  value="1977">1977</option><option  value="1978">1978</option><option  value="1979">1979</option><option  value="1980">1980</option><option  value="1981">1981</option><option  value="1982">1982</option><option  value="1983">1983</option><option  value="1984">1984</option><option  value="1985">1985</option><option  value="1986">1986</option><option  value="1987">1987</option><option  value="1988">1988</option><option  value="1989">1989</option><option  value="1990">1990</option><option  value="1991">1991</option><option  value="1992">1992</option><option  value="1993">1993</option><option  value="1994">1994</option><option  value="1995">1995</option><option  value="1996">1996</option><option  value="1997">1997</option><option  value="1998">1998</option><option  value="1999">1999</option><option  value="2000">2000</option><option  value="2001">2001</option><option  value="2002">2002</option><option  value="2003">2003</option><option  value="2004">2004</option><option  value="2005">2005</option><option  value="2006">2006</option><option  value="2007">2007</option><option  value="2008">2008</option><option  value="2009">2009</option><option  value="2010">2010</option><option  value="2011">2011</option><option  value="2012">2012</option></select>';
  html += '      <div style="padding: 5px; text-align: center;" id="enddate-Z2">to</div>';
  html += '      <div id="enddate-Z" style="padding-bottom: 5px;"><select id="month-1-Z" style="margin-right: 5px;"><option value=""></option><option  value="1">January</option><option  value="2">February</option><option  value="3">March</option><option  value="4">April</option><option  value="5">May</option><option  value="6">June</option><option  value="7">July</option><option  value="8">August</option><option  value="9">September</option><option  value="10">October</option><option  value="11">November</option><option  value="12">December</option></select><select id="year-1-Z"><option value=""></option><option  value="1970">1970</option><option  value="1971">1971</option><option  value="1972">1972</option><option  value="1973">1973</option><option  value="1974">1974</option><option  value="1975">1975</option><option  value="1976">1976</option><option  value="1977">1977</option><option  value="1978">1978</option><option  value="1979">1979</option><option  value="1980">1980</option><option  value="1981">1981</option><option  value="1982">1982</option><option  value="1983">1983</option><option  value="1984">1984</option><option  value="1985">1985</option><option  value="1986">1986</option><option  value="1987">1987</option><option  value="1988">1988</option><option  value="1989">1989</option><option  value="1990">1990</option><option  value="1991">1991</option><option  value="1992">1992</option><option  value="1993">1993</option><option  value="1994">1994</option><option  value="1995">1995</option><option  value="1996">1996</option><option  value="1997">1997</option><option  value="1998">1998</option><option  value="1999">1999</option><option  value="2000">2000</option><option  value="2001">2001</option><option  value="2002">2002</option><option  value="2003">2003</option><option  value="2004">2004</option><option  value="2005">2005</option><option  value="2006">2006</option><option  value="2007">2007</option><option  value="2008">2008</option><option  value="2009">2009</option><option  value="2010">2010</option><option  value="2011">2011</option><option  value="2012">2012</option></select></div>';
  html += '  	 </div>';

  html += '	  	 <div style="float: left; margin-left:20px; font-size:9pt;">';
  html += '			  <div class="smtitle2" style="margin-bottom:20px;">Employer:<br /><input type="text" style="width: 181px;" id="employer" value="" /></div>';
  html += '	  		<div style="clear:both; float:left;"><input type="checkbox"  id="present-Z" onchange="javascript:document.getElementById(\'enddate-Z\').style.display = this.checked ? \'none\' : \'\'; javascript:document.getElementById(\'enddate-Z2\').style.display = this.checked ? \'none\' : \'\';" /></div>';
  html += '       <div style="float:left; padding-top:3px;"> I currently work here</div>';
  html += '		   </div>';

  html += '		   </div>';

//Degree
  html += '    <div id="jg4" style="display:none; width:100%; height:130px; font-size: 9pt;">';
  html += '      <div style="float:left; padding-top:4px; font-size:12px;">yes </div>';
  html += '      <div style="float:left; padding-top:1px;"><input type="checkbox" name="junk" CHECKED></div>';
  html += '      <div style="float:left; padding-top:4px; font-size:10px;">I have this license/degree </div>';
  html += '		 </div>';

  html += '    <div style=" width:100%; text-align:center; clear:both;">';
  html += '      <input type="button" class="button" name="" value="save &amp; connect" onclick="javascript:joinPage2(\'' + gid + '\', \'' + hash + '\');"/>';
  html += '    </div>';
  html += '  </div>';

	showPopUp("", html, 0);

  occupation = new actb(document.getElementById("occupation"), "occupation" );
  employer = new actb(document.getElementById("employer"), "employer" );
//  occupation.actb_val = '';

//	postAjax("/pages/join.php", "gid=" + gid + "&hash=" + hash, "eval");
}

function joinPage2(gid, hash)
{
  tab = joinPageTab;

  if( tab == 1 )
  {
    if( document.getElementById("month-0-X").value == "" ||
        document.getElementById("year-0-X").value == "" ||
        (document.getElementById("month-1-X").value == "" &&
        document.getElementById("year-1-X").value == "" && !document.getElementById("present-X").checked ) ||
        (occupation.actb_val == null && document.getElementById("occupation").value == "") )
    {
      alert( 'You must fill out all fields to continue.' );
      return;
    }

    items = ['month-0', 'month-1', 'year-0', 'year-1'];
    works = new Array();
    obj = {};

    obj.present = 0;

    e = document.getElementById("present-X");
    if( e ) obj.present = e.checked ? 1 : 0;

		for (var j in items)
			obj[items[j]] = document.getElementById(items[j] + "-X").value;

		obj['employer'] = {'val': gid};

		if (occupation.actb_val == null)
			obj['occupation'] = {'txt': document.getElementById("occupation").value};
		else
			obj['occupation'] = {'val': occupation.actb_val};

		works.push(obj);

  	postAjax("/profile/savework.php", "json_works=" + escape(json_encode(works)) + "&savePrev=1", function(data){} );
  }
  else if( tab == 2 )
  {
    if( document.getElementById("year-0-Y").value == "" ||
        document.getElementById("year-1-Y").value == "" )
    {
      alert( 'You must fill out all fields to continue.' );
      return;
    }


    var saveEdus = [];

  	obj = {'start': document.getElementById("year-0-Y").value };

    if( document.getElementById("present-Y").checked )
    {
      var theDate=new Date();
      obj['stop'] = theDate.getFullYear();
    }
    else
      obj['stop'] = document.getElementById("year-1-Y" ).value;

 		obj['school'] = {'val': gid};

  	saveEdus.push(obj);

    postAjax("/profile/saveabout.php", "json_edus=" + escape(json_encode(saveEdus)), function(data){} );
  }
  else if( tab == 3 )
  {
    if( document.getElementById("month-0-Z").value == "" ||
        document.getElementById("year-0-Z").value == "" ||
        (document.getElementById("month-1-Z").value == "" &&
        document.getElementById("year-1-Z").value == "" && !document.getElementById("present-Z").checked ) ||
        (employer.actb_val == null && document.getElementById("employer").value == "") )
    {
      alert( 'You must fill out all fields to continue.' );
      return;
    }

    items = ['month-0', 'month-1', 'year-0', 'year-1'];
    works = new Array();
    obj = {};

    obj.present = 0;

    e = document.getElementById("present-Z");
    if( e ) obj.present = e.checked ? 1 : 0;

		for (var j in items)
			obj[items[j]] = document.getElementById(items[j] + "-Z").value;

		obj['occupation'] = {'val': gid};

		if (employer.actb_val == null)
			obj['employer'] = {'txt': document.getElementById("employer").value};
		else
			obj['employer'] = {'val': employer.actb_val};

		works.push(obj);

  	postAjax("/profile/savework.php", "json_works=" + escape(json_encode(works)), function(data){} );
  }
  else if( tab == 4 )
  {
    postAjax("/profile/addlicense.php", "l=" + gid, function(data){} );
  }

  postAjax("/pages/join.php", "gid=" + gid + "&hash=" + hash, "joinPageHandler" );
}

function joinPageHandler( data )
{
  closePopUp();
  eval( data );
}

function deleteNotification(nid)
{
	postAjax("/messaging/deletenotification.php", "nid=" + nid, "void");
	document.getElementById("notification-" + nid).style.display = "none";

	e = document.getElementById("notifications-number");
	e.innerHTML = parseInt(e.innerHTML) - 1;
}

function showNotifications()
{

  if( document.getElementById( 'notifications' ) )
  {
		showPopUp("", '<div id="notifypopup" style="height: 280px; overflow: auto; padding-right:5px;"></div>', 311, "no", 0, notificationsReplace);
		setTimeout('document.getElementById("notifypopup").appendChild(document.getElementById("notifications"));', 100);
  }
  else
  	getAjax("/messaging/notifications.php?style=3", "showNotificationsHandler");
}

function notificationsReplace()
{
  if(document.getElementById("notificationscontainer"))
  {
  	document.getElementById("notificationscontainer").appendChild(document.getElementById("notifications"));
  }
}

function showNotificationsHandler(data)
{
	showPopUp("", data, 311);
}

function addToLogHandler(data)
{
	if (data == "OK")
		addToLogPending[0].innerHTML = addToLogPending[1];
	else
		addToLogPending[0].innerHTML = "Error";
	
	addToLogPending[0].style.cursor = "default";
}

function deleteFriend(uid, id, caption )
{
  if( typeof caption == "string" )
  	document.getElementById(id).innerHTML = caption;
  else
  	document.getElementById(id).style.display = "none";
	postAjax("/addfriend.php", "uid=" + uid + "&s=-1", "void");
}

function confirmFriend(uid, id, captionFriends)
{
	document.getElementById(id).innerHTML = "Wait &#0133;";
	postAjax("/addfriend.php", "uid=" + uid + "&s=1", "document.getElementById('" + id + "').innerHTML = '" + captionFriends + "'; void");

	e = document.getElementById("numfreqs");
	if (e)
		e.innerHTML = parseInt(e.innerHTML) - 1;
}

function addFriend(uid, id, captionPending)
{
	div = (typeof id == "string") ? div = document.getElementById(id) : id;
	
	div.innerHTML = "Wait &#0133;";
	
	postAjax("/addfriend.php", "uid=" + uid + "&s=0", function (data)
	{
		div.innerHTML = captionPending;
	});
}

var friendsPopupUid = null;
var friendsPopupQ = null;

function showFriendsPopup(uid, q)
{
	if (typeof q == "undefined")
		friendsPopupQ = "*";
	else
		friendsPopupQ = q;

	if (typeof uid == "undefined" || typeof uid == "object")
		uid = friendsPopupUid;
	else
		friendsPopupUid = uid;
	
	e = document.getElementById("friendspopup");

	if (e)
		showFriendsPopupHandler("");
	else
	{
		showPopUp('', '<div id="friendspopup"><div class="loginmessage"><img src="/images/barwait.gif" alt="" /></div>', [515, 487], 'no');
		getAjax("/getfriends.php?uid=" + uid, "showFriendsPopupHandler");
	}
}

function showFriendsPopupHandler(data)
{
	e = document.getElementById("friendspopup");

	if (e)
	{
		if (data != "")
			e.innerHTML = data;

		fs = ['f', 'r', 's'];

		for (var i in fs)
    {
			if( document.getElementById("friendspopup-" + fs[i]) )
        document.getElementById("friendspopup-" + fs[i]).style.display = (friendsPopupQ == fs[i] || friendsPopupQ == "*") ? "" : "none";
    }
	}
}

var shareLoaded = false;

function showCreatePage()
{
	loadShare("newpage", 1);
}

function loadShare(p, show, params)
{
	if (typeof params == "undefined")
		params = "";
	else if (typeof params == "object")
		params = "&json_params=" + escape(json_encode(params));

	if (typeof show == "undefined")
		show = 0;

	if (shareLoaded)
	{
		if (show == 1)
			showShare();
	}
	else
		loadjscssfile("/share.js.php?p=" + p + "&show=" + show + "&params=" + params, "js");

	shareLoaded = true;
}

function showPageMembers(gid)
{
	getAjax("/pages/members.php?gid=" + gid,
			function(html)
			{
				showPopUp({'content': '<div id="pagemembers"></div>', 'width': 509});
				document.getElementById("pagemembers").innerHTML = html;
			}
		);
}

function searchKeypress(event, obj, divName )
{
  dupeName = false;

	kc = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
	if (kc == 13 && divName=="")
		searchDo();

  if( obj.value.length > 2 )
  {
  	getAjax("/search_preview.php?q=" + obj.value + "&c=" + (divName.length > 0 ? 1 : 0),
      function( html )
      {
        if( html.indexOf( '<!--DUPE-->', 0 ) > -1 )
          dupeName = true;

        e = document.getElementById('suggestionBox' + divName);
        if( e )
        {
          if( html != "0" )
          {
            e.innerHTML = html;
            e.style.display = '';
          }
          else
            e.style.display = 'none';
        }
      }
    );
  }
}

var closeInterval = 0;
function searchLostFocus( interval, divName )
{
  if( interval == -1 )
  {
    closeInterval = window.setInterval( 'searchLostFocus(' + closeInterval + ', "' + divName + '" );', 150 );
  }
  else
  {
    e = document.getElementById('suggestionBox' + divName);
    if( e )
    {
      e.style.display='none';
    }
    window.clearInterval( closeInterval );
  }
}

function searchDo()
{
	if (typeof searchType != "undefined")
		suffix = "&t=" + searchType;
	else
		suffix = "";
	
	for (i = 0; i < 2; i++)
	{
		e = document.getElementById("txtsearch" + (i == 0 ? "" : i));
		if (!e) continue;
		
		q = e.value;
		if (q != "" && q != "Search ...") window.location='/search.php?q=' + escape(q) + suffix;
	}
}

function findPos(obj) {
	var curleft = curtop = 0;
	if (obj.offsetParent) {
			do {
					curleft += obj.offsetLeft;
					curtop += obj.offsetTop;
			} while (obj = obj.offsetParent);
	return [curleft,curtop];
	}
	}
	
function getScrollTop(){
    if(typeof pageYOffset!= 'undefined'){
        //most browsers
        return pageYOffset;
    }
    else{
        var B= document.body; //IE 'quirks'
        var D= document.documentElement; //IE with doctype
        D= (D.clientHeight)? D: B;
        return D.scrollTop;
    }
}

function setScrollTop(id, y)
{
	document.getElementById(id).scrollTop = y;
}
	
function getCursorPosition(e) {
    e = e || window.event;
    var cursor = {x:0, y:0};
    if (e.pageX || e.pageY) {
        cursor.x = e.pageX;
        cursor.y = e.pageY;
    } 
    else {
        cursor.x = e.clientX + 
            (document.documentElement.scrollLeft || 
            document.body.scrollLeft) - 
            document.documentElement.clientLeft;
        cursor.y = e.clientY + 
            (document.documentElement.scrollTop || 
            document.body.scrollTop) - 
            document.documentElement.clientTop;
    }
    return [cursor.x, cursor.y];
}

function hideNag(w)
{
	postAjax("/hidenag.php", "w=" + w, "void");
	document.getElementById("nag-" + w).style.display = "none";
}


function getHeight(e) {
    return Math.max(e.scrollHeight, e.offsetHeight, e.clientHeight);
}

String.prototype.reverse = function(){
splitext = this.split("");
revertext = splitext.reverse();
reversed = revertext.join("");
return reversed;
}

function getDocSize()
{
	var w = 0;
	var h = 0;

	//IE
	if(!window.innerWidth)
	{
		//strict mode
		if(!(document.documentElement.clientWidth == 0))
		{
			w = document.documentElement.clientWidth;
			h = document.documentElement.clientHeight;
		}
		//quirks mode
		else
		{
			w = document.body.clientWidth;
			h = document.body.clientHeight;
		}
	}
	//w3c
	else
	{
		w = window.innerWidth;
		h = window.innerHeight;
	}
	return [w,h];
}

function fireEvent(obj,evt){

	var fireOnThis = obj;
	if( document.createEvent ) {
	  var evObj = document.createEvent('MouseEvents');
	  evObj.initEvent( evt, true, false );
	  fireOnThis.dispatchEvent(evObj);
	} else if( document.createEventObject ) {
	  fireOnThis.fireEvent('on'+evt);
	}
}

function showSendMessage(uid, name, pic, subject, message )
{
  if( subject == undefined ) subject = "";
  if( message == undefined ) message = "";

	html  = '<div style="font-size: 8pt; color: #555;">';
	html += '	<div style="float: left; width: 85px;">';
	html += '		<img src="' + pic + '" alt="" style="padding-bottom: 2px;" /><br />';
	html += '		' + name;
	html += '	</div>';
	html += '	<div style="float: left; padding-left: 10px; font-weight: bold;">';
	html += '		<div style="height: 12pt;">Subject:</div><input type="text" id="msgsubj" maxlength="50" style="width: 285px;" value="' + subject + '"/>';
	html += '		<div style="padding-top: 5px;">';
	html += '			<div style="height: 12pt;">Message:</div><textarea id="msgbody" style="width: 285px; height: 95px;">' + message + '</textarea>';
	html += '		</div>';
	html += '		<div style="padding: 5px 0px; text-align: center; height: 16px;" id="msgbuttons">';
	html += '			<input type="button" class="button" value="Send" onclick="javascript:sendMessage(\'' + uid + '\');" /> &nbsp; &nbsp;';
	html += '			<input type="button" class="button" value="Cancel" onclick="javascript:closePopUp();" />';
	html += '		</div>';
	html += '	</div>';
	html += '	<div style="clear: both;"></div>';
	html += '</div>';
	
	showPopUp("", html, 0);
}

function sendMessage(uid)
{
	subj = escape(document.getElementById("msgsubj").value);
	body = escape(document.getElementById("msgbody").value);
	
	if (subj == "")
	{
		alert("Please enter a subject.");
		return;
	}
	
	if (body == "")
	{
		alert("Please type a message.");
		return;
	}
	
	document.getElementById("msgbuttons").innerHTML = "<img src='/images/tbpreload.gif' alt='' />";
	
	postAjax("/messaging/sendmessage.php", "uid=" + uid + "&subj=" + subj + "&body=" + body, function(data)
	{
		if (data == "OK")
			showPopUp2("", "Your message has been sent.");
		else
    {
      if( admin )
  			showPopUp2("", "Error: " + data );
      else
  			showPopUp2("", "There was an error sending your message.&nbsp; Please try again later.");
    }
	});
}

function fbLogin()
{
	FB.Connect.requireSession(fbloggedin);
	//FB.ensureInit( function(){ FB.Connect.requireSession(fbloggedin); });
	return false;
}

function showLogin(hideButton)
{
	if (typeof hideButton == "undefined")
		hideButton = -1;
	
	content  = '<div style="text-align: center;">';
	content += '	<div style="margin: 10px 0px;">Just click a button below to connect.</div>';
	content += '	<div style="height: 25px; padding-bottom: 10px;">';
	if (hideButton != 0)
	content += '		<span id="loginfb"><a href="/fbconnect/login.php"><img src="/images/fbconnect.gif" alt="" /></a></span>';
	if (hideButton != -1)
	content += '		<span id="loginspace">&nbsp; &nbsp;</span>';
	if (hideButton != 1)
	content += '		<span id="logintw"><a href="/twitter/login.php"><img src="/images/twitter.gif" alt="" /></a></span>';
	content += '	</div>';
	content += '</div>';
	
	showPopUp("Login with your favorite site", content, 0);
}

function actionClicked(e)
{
	x = e.getElementsByTagName('a');

	if (typeof x[0] == "object")
		fireEvent(x[0], "click");
}

function pymkShuffle(i)
{
	getAjax("/getpymk.php?i=" + i, "pymkShuffleHandler");
	document.getElementById("pymk_container" + i).style.visibility = "hidden";
}

function pymkSearch(i, q)
{
	getAjax("/getpymk.php?i=" + i + "&q=" + q, "pymkShuffleHandler");
	document.getElementById("pymk_container" + i).style.visibility = "hidden";
}

function pymkShuffleHandler(html)
{
	i = html.substring(0, 1);
	html = html.substring(1);
	document.getElementById("pymk_container" + i).innerHTML = html;
	document.getElementById("pymk_container" + i).style.visibility = "visible";
}

function suggestionsShuffle()
{
	getAjax("/get_suggestions.php?i=1", "suggestionsShuffleHandler");
	document.getElementById("suggestion_container").style.visibility = "hidden";
}

function suggestionsShuffleHandler(html)
{
	document.getElementById("suggestion_container").innerHTML = html;
	document.getElementById("suggestion_container").style.visibility = "visible";
}

function Shuffle( divname, url )
{
	getAjax("/" + url + "&divname=" + divname, "ShuffleHandler");
	document.getElementById(divname).style.visibility = "hidden";
}

function ShuffleHandler( data )
{
  data_array = data.split(String.fromCharCode(1));
	divname = data_array[0];
	html = data_array[1];

  e = document.getElementById(divname);
  if( e )
  {
  	e.innerHTML = html;
	  e.style.visibility = "visible";
  }
}

function showPhotoChanger(alturl)
{
	loadjscssfile("/photochanger.js", "js");
}

String.prototype.endsWith = function(str) 
{return (this.match(str+"$")==str)}

String.prototype.startsWith = function(str){
    return (this.indexOf(str) === 0);
}


function checkEmail(str) {

		var at="@"
		var dot="."
		var lat=str.indexOf(at)
		var lstr=str.length
		var ldot=str.indexOf(dot)
		if (str.indexOf(at)==-1){
		   return false
		}

		if (str.indexOf(at)==-1 || str.indexOf(at)==0 || str.indexOf(at)==lstr){
		   return false
		}

		if (str.indexOf(dot)==-1 || str.indexOf(dot)==0 || str.indexOf(dot)==lstr){
		    return false
		}

		 if (str.indexOf(at,(lat+1))!=-1){
		    return false
		 }

		 if (str.substring(lat-1,lat)==dot || str.substring(lat+1,lat+2)==dot){
		    return false
		 }

		 if (str.indexOf(dot,(lat+2))==-1){
		    return false
		 }
		
		 if (str.indexOf(" ")!=-1){
		    return false
		 }

 		 return true					
	}

function loadjscssfile(filename, filetype){
 if (filetype=="js"){ //if filename is a external JavaScript file
  var fileref=document.createElement('script')
  fileref.setAttribute("type","text/javascript")
  fileref.setAttribute("src", filename)
 }
 else if (filetype=="css"){ //if filename is an external CSS file
  var fileref=document.createElement("link")
  fileref.setAttribute("rel", "stylesheet")
  fileref.setAttribute("type", "text/css")
  fileref.setAttribute("href", filename)
 }
 if (typeof fileref!="undefined")
  document.getElementsByTagName("head")[0].appendChild(fileref)
}

function isArray(obj) {
   if (typeof obj == "undefined" || obj == null ) return false;
   if (obj.constructor.toString().indexOf("Array") == -1)
      return false;
   else
      return true;
}

function getFormVals(frm) {
        vals = "";
        for (i = 0; i < frm.elements.length; i++) {
                if (frm.elements[i].name == "") {
                        continue;
                }
                vals += "&" + frm.elements[i].name + "=";
                if (frm.elements[i].type == "hidden" ||
                        frm.elements[i].type == "text" ||
                        frm.elements[i].type == "textarea") {
                        vals += frm.elements[i].value;
                } else if (frm.elements[i].type == "checkbox") {
                        if (frm.elements[i].checked) {
                                vals += "on";
                        } else {
                                vals += "off";
                        }
                } else if (frm.elements[i].type == "select-one") {
                        vals += frm.elements[i].options[frm.elements[i].selectedIndex].value;
                }
        }
        vals = vals.substring(1);
        return vals;
}

function json_encode (mixed_val) {
    // Returns the JSON representation of a value
    //
    // version: 1009.2513
    // discuss at: http://phpjs.org/functions/json_encode
    // +      original by: Public Domain (http://www.json.org/json2.js)
    // + reimplemented by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +      improved by: Michael White
    // +      input by: felix
    // +      bugfixed by: Brett Zamir (http://brett-zamir.me)
    // *        example 1: json_encode(['e', {pluribus: 'unum'}]);
    // *        returns 1: '[\n    "e",\n    {\n    "pluribus": "unum"\n}\n]'
    /*
        http://www.JSON.org/json2.js
        2008-11-19
        Public Domain.
        NO WARRANTY EXPRESSED OR IMPLIED. USE AT YOUR OWN RISK.
        See http://www.JSON.org/js.html
    */
    var retVal, json = this.window.JSON;
    try {
        if (typeof json === 'object' && typeof json.stringify === 'function') {
            retVal = json.stringify(mixed_val); // Errors will not be caught here if our own equivalent to resource
                                                                            //  (an instance of PHPJS_Resource) is used

            if (retVal === undefined) {
                throw new SyntaxError('json_encode');
            }
            return retVal;
        }
      }
      catch(err)
      {}
    //If the first round doesn't work, fall through to the back up method
    try
    {
        var value = mixed_val;

        var quote = function (string) {
            var escapable = /[\\\"\u0000-\u001f\u007f-\u009f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g;
            var meta = {    // table of character substitutions
                '\b': '\\b',
                '\t': '\\t',
                '\n': '\\n',
                '\f': '\\f',
                '\r': '\\r',
                '"' : '\\"',
                '\\': '\\\\'
            };

            escapable.lastIndex = 0;
            return escapable.test(string) ?
                            '"' + string.replace(escapable, function (a) {
                                var c = meta[a];
                                return typeof c === 'string' ? c :
                                '\\u' + ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
                            }) + '"' :
                            '"' + string + '"';
        };


        var str = function (key, holder) {
            var gap = '';
            var indent = '    ';
            var i = 0;          // The loop counter.
            var k = '';          // The member key.
            var v = '';          // The member value.
            var length = 0;
            var mind = gap;
            var partial = [];
            var value = holder[key];

            // If the value has a toJSON method, call it to obtain a replacement value.
            if (value && typeof value === 'object' &&
                typeof value.toJSON === 'function') {
                value = value.toJSON(key);
            }

            // What happens next depends on the value's type.
            switch (typeof value) {
                case 'string':
                    return quote(value);

                case 'number':
                    // JSON numbers must be finite. Encode non-finite numbers as null.
                    return isFinite(value) ? String(value) : 'null';

                case 'boolean':
                case 'null':
                    // If the value is a boolean or null, convert it to a string. Note:
                    // typeof null does not produce 'null'. The case is included here in
                    // the remote chance that this gets fixed someday.

                    return String(value);

                case 'object':
                    // If the type is 'object', we might be dealing with an object or an array or
                    // null.
                    // Due to a specification blunder in ECMAScript, typeof null is 'object',
                    // so watch out for that case.
                    if (!value) {
                        return 'null';
                    }
                    if ((this.PHPJS_Resource && value instanceof this.PHPJS_Resource) ||
                        (window.PHPJS_Resource && value instanceof window.PHPJS_Resource)) {
                        throw new SyntaxError('json_encode');
                    }

                    // Make an array to hold the partial results of stringifying this object value.
                    gap += indent;
                    partial = [];

                    // Is the value an array?
                    if (Object.prototype.toString.apply(value) === '[object Array]') {
                        // The value is an array. Stringify every element. Use null as a placeholder
                        // for non-JSON values.

                        length = value.length;
                        for (i = 0; i < length; i += 1) {
                            partial[i] = str(i, value) || 'null';
                        }

                        // Join all of the elements together, separated with commas, and wrap them in
                        // brackets.
                        v = partial.length === 0 ? '[]' :
                                gap ? '[\n' + gap +
                                partial.join(',\n' + gap) + '\n' +
                                mind + ']' :
                                '[' + partial.join(',') + ']';
                        gap = mind;
                        return v;
                    }

                    // Iterate through all of the keys in the object.
                    for (k in value) {
                        if (Object.hasOwnProperty.call(value, k)) {
                            v = str(k, value);
                            if (v) {
                                partial.push(quote(k) + (gap ? ': ' : ':') + v);
                            }
                        }
                    }

                    // Join all of the member texts together, separated with commas,
                    // and wrap them in braces.
                    v = partial.length === 0 ? '{}' :
                            gap ? '{\n' + gap + partial.join(',\n' + gap) + '\n' +
                            mind + '}' : '{' + partial.join(',') + '}';
                    gap = mind;
                    return v;
                case 'undefined': // Fall-through
                case 'function': // Fall-through
                default:
                    throw new SyntaxError('json_encode');
            }
        };

        // Make a fake root object containing our value under the key of ''.
        // Return the result of stringifying the value.
        return str('', {
            '': value
        });

    } catch(err) { // Todo: ensure error handling above throws a SyntaxError in all cases where it could
                            // (i.e., when the JSON global is not available and there is an error)
        if (!(err instanceof SyntaxError)) {
            throw new Error('Unexpected error type in json_encode()');
        }
        this.php_js = this.php_js || {};
        this.php_js.last_error_json = 4; // usable by json_last_error()
        return null;
    }

}

function htmlentities(str) {
    return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
}



////////////////

var timerID = new Array();
var startTime = new Array();
var obj = new Array();
var endHeight = new Array();
var moving = new Array();
var dir = new Array();
var ddd = [];

function slidedown(objname, h, resetstyle){
  if(moving[objname])
          return;

  //if(document.getElementById(objname).style.display != "none")
          //return; // cannot slide down something that is already visible

  moving[objname] = true;
  dir[objname] = "down";
  startslide(objname, h, resetstyle);
}

function slideup(objname, h, resetstyle){
  if(moving[objname])
    return;

        //if(document.getElementById(objname).style.display == "none")
                //return; // cannot slide up something that is already hidden

  moving[objname] = true;
  dir[objname] = "up";
  startslide(objname, h, resetstyle);
}

function startslide(objname, h, resetstyle){
  obj[objname] = document.getElementById(objname);
  if (resetstyle)
  {
  	obj[objname].style.overflow = "hidden";
  	obj[objname].style.height = 0;
  }

  endHeight[objname] = parseInt(h ? h : obj[objname].style.height);
  obj[objname].style.display = "block";

  timerID[objname] = setInterval('slidetick("' + objname + '", ' + (resetstyle ? 'true' : 'false') + ');',2); //timerlen
  startTime[objname] = (new Date()).getTime();
}

function slidetick(objname, resetstyle){
	slideAniLen = 100;
  var elapsed = (new Date()).getTime() - startTime[objname];

  if (elapsed > slideAniLen)
    endSlide(objname, resetstyle)
  else {
    var d =Math.round(elapsed / slideAniLen * endHeight[objname]);
    var o =Math.round(elapsed / slideAniLen * 100);

    if(dir[objname] == "up")
    {
      o = 100 - o;
      d = endHeight[objname] - d;
    }

    obj[objname].style.height = d + "px";
    ddd[ddd.length] = o;
    obj[objname].style.opacity = o / 100;
    obj[objname].style.filter = 'alpha(opacity = ' + o + ')';
  }

  return;
}

function endSlide(objname, resetstyle){
  obj[objname] = document.getElementById(objname);
  clearInterval(timerID[objname]);

	o = 100;
  if(dir[objname] == "up")
  {
    obj[objname].style.display = "none";
		o = 0;
	}

		if (resetstyle)
		{
		obj[objname].style.height = "";
		obj[objname].style.overflow = "";
		}
		else
		{
        obj[objname].style.height = endHeight[objname] + "px";
		}
        obj[objname].style.opacity = o / 100;
        obj[objname].style.filter = 'alpha(opacity = ' + o + ')';

        delete(moving[objname]);
        delete(timerID[objname]);
        delete(startTime[objname]);
        delete(endHeight[objname]);
        delete(obj[objname]);
        delete(dir[objname]);
}

function toggleSlide( objname )
{
  obj = document.getElementById(objname);
  if( obj.style.display == "none" )
    slidedown( objname );
  else
    slideup( objname );
}

////////////////common functions from autocomplete//////////////////

/* Event Functions */

// Add an event to the obj given
// event_name refers to the event trigger, without the "on", like click or mouseover
// func_name refers to the function callback when event is triggered
function addEvent(obj,event_name,func_name){
	if (obj.attachEvent){
		obj.attachEvent("on"+event_name, func_name);
	}else if(obj.addEventListener){
		obj.addEventListener(event_name,func_name,true);
	}else{
		obj["on"+event_name] = func_name;
	}
}

// Removes an event from the object
function removeEvent(obj,event_name,func_name){
	if (obj.detachEvent){
		obj.detachEvent("on"+event_name,func_name);
	}else if(obj.removeEventListener){
		obj.removeEventListener(event_name,func_name,true);
	}else{
		obj["on"+event_name] = null;
	}
}

// Stop an event from bubbling up the event DOM
function stopEvent(evt){
	evt || window.event;
	if (evt.stopPropagation){
		evt.stopPropagation();
		evt.preventDefault();
	}else if(typeof evt.cancelBubble != "undefined"){
		evt.cancelBubble = true;
		evt.returnValue = false;
	}
	return false;
}

// Get the obj that starts the event
function getElement(evt){
	if (window.event){
		return window.event.srcElement;
	}else{
		return evt.currentTarget;
	}
}
// Get the obj that triggers off the event
function getTargetElement(evt){
	if (window.event){
		return window.event.srcElement;
	}else{
		return evt.target;
	}
}
// For IE only, stops the obj from being selected
function stopSelect(obj){
	if (typeof obj.onselectstart != 'undefined'){
		addEvent(obj,"selectstart",function(){ return false;});
	}
}

/*    Caret Functions     */

// Get the end position of the caret in the object. Note that the obj needs to be in focus first
function getCaretEnd(obj){
	if(typeof obj.selectionEnd != "undefined"){
		return obj.selectionEnd;
	}else if(document.selection&&document.selection.createRange){
		var M=document.selection.createRange();
		try{
			var Lp = M.duplicate();
			Lp.moveToElementText(obj);
		}catch(e){
			var Lp=obj.createTextRange();
		}
		Lp.setEndPoint("EndToEnd",M);
		var rb=Lp.text.length;
		if(rb>obj.value.length){
			return -1;
		}
		return rb;
	}
}
// Get the start position of the caret in the object
function getCaretStart(obj){
	if(typeof obj.selectionStart != "undefined"){
		return obj.selectionStart;
	}else if(document.selection&&document.selection.createRange){
		var M=document.selection.createRange();
		try{
			var Lp = M.duplicate();
			Lp.moveToElementText(obj);
		}catch(e){
			var Lp=obj.createTextRange();
		}
		Lp.setEndPoint("EndToStart",M);
		var rb=Lp.text.length;
		if(rb>obj.value.length){
			return -1;
		}
		return rb;
	}
}
// sets the caret position to l in the object
function setCaret(obj,l){
	obj.focus();
	if (obj.setSelectionRange){
		obj.setSelectionRange(l,l);
	}else if(obj.createTextRange){
		m = obj.createTextRange();		
		m.moveStart('character',l);
		m.collapse();
		m.select();
	}
}
// sets the caret selection from s to e in the object
function setSelection(obj,s,e){
	obj.focus();
	if (obj.setSelectionRange){
		obj.setSelectionRange(s,e);
	}else if(obj.createTextRange){
		m = obj.createTextRange();		
		m.moveStart('character',s);
		m.moveEnd('character',e);
		m.select();
	}
}

/*    Escape function   */
String.prototype.addslashes = function(){
	return this.replace(/(["\\\.\|\[\]\^\*\+\?\$\(\)])/g, '\\$1');
}
String.prototype.trim = function () {
    return this.replace(/^\s*(\S*(\s+\S+)*)\s*$/, "$1");
};
/* --- Escape --- */

/* Offset position from top of the screen */
function curTop(obj){
	toreturn = 0;
	while(obj){
		toreturn += obj.offsetTop;
		obj = obj.offsetParent;
	}
	return toreturn;
}
function curLeft(obj){
	toreturn = 0;
	while(obj){
		toreturn += obj.offsetLeft;
		obj = obj.offsetParent;
	}
	return toreturn;
}
/* ------ End of Offset function ------- */

/* Types Function */

// is a given input a number?
function isNumber(a) {
    return typeof a == 'number' && isFinite(a);
}

/* Object Functions */

function replaceHTML(obj,text){
	while(el = obj.childNodes[0]){
		obj.removeChild(el);
	};
	
	newspan = document.createElement("span");
	newspan.innerHTML = text;
	
	//obj.appendChild(document.createTextNode(text));
	obj.appendChild(newspan);
}

function updateMediaCategory( id, type, cat, hash )
{
  postAjax("/admin/update_media_category.php", "type=" + type + "&id=" + id + "&cat=" + cat + "&hash=" + hash, "updateMediaCategoryHandler" ); //, function( data ) {alert( data); } );
}

function updateMediaCategoryHandler( data )
{
}

function sendFeedback()
{
  if( document.getElementById('feedbackText') )
  {
    text = document.getElementById('feedbackText').value;
    postAjax("/send_feedback.php", "text=" + text + "&script=" + script + "&requesturi=" + encodeURIComponent(request_uri), "sendFeedbackHandler" );
  }
}

function sendFeedbackHandler( data )
{

  if( document.getElementById('feedbackText') )
  {
    document.getElementById('feedbackText').value = '';

  }

  if( document.getElementById('feedbackContainer') )
  {
    document.getElementById('feedbackContainer').innerHTML = '<br /><br /><br /><h3>Feedback Sent,<br /> Thank you!</h3>';
  }
}

function selectedValue(selBox) {
  return selBox.options[selBox.selectedIndex].value;
}


function loadGetListed()
{
	postAjax("/pages/page_create_popup.php", '', function (data)
	{
    showPopUp("", data, 0);
	});
}


function getFormVals2(frm) {
	vals = "";
	for (i = 0; i < frm.elements.length; i++) {
		if (frm.elements[i].name == "") {
			continue;
		}
		vals += "&" + frm.elements[i].name + "=";
		if (frm.elements[i].type == "hidden" || frm.elements[i].type == "text" || frm.elements[i].type == "textarea") {
      temp = frm.elements[i].value;
      temp = temp.replace( "&", "&amp;" );
			vals += temp;
		} else if (frm.elements[i].type == "checkbox") {
			if (frm.elements[i].checked) {
				vals += "on";
			} else {
				vals += "off";
			}
		} else if (frm.elements[i].type == "select-one") {
			vals += escape(frm.elements[i].options[frm.elements[i].selectedIndex].value);
		}
	}
	vals = vals.substring(1);
	return vals;
}

function createCookie(name,value,days) {
	if (days) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else var expires = "";
	document.cookie = name+"="+value+expires+"; path=/";
}

function sendEmail(email, message)
{
  post = 'email=' + email;
  if( message != "" )
    post += '&message=' + message;

	postAjax("/send_email.php", post, function (data)
	{
    showPopUp("Send Email", data, 0);
	});
}

function sendEmailAction()
{
  to = document.getElementById( 'to' ).value;
  from = document.getElementById( 'from' ).value;
  subject = document.getElementById( 'subject' ).value;
  message = document.getElementById( 'message' ).value;

	postAjax("/send_email.php", 'send=1&to=' + encodeURIComponent(to) + '&from=' + encodeURIComponent(from) + '&subject=' + encodeURIComponent(subject) + '&message=' + encodeURIComponent(message), function (data)
     { if( data != 'OK' ) alert( data ); closePopUp() } );
}

function showLinksToPage( gid )
{
	e = document.getElementById("linkspopup");

	if (e)
		showLinksToPageHandler("");
	else
	{
		showPopUp('', '<div id="linkspopup"><div class="loginmessage"><img src="/images/barwait.gif" alt="" /></div>', [515, 487], 'no');
		getAjax("/getlinkstopage.php?gid=" + gid, showLinksToPageHandler);
	}
}

function showLinksToPageHandler( data )
{
	e = document.getElementById("linkspopup");

	if (e)
	{
		if (data != "")
			e.innerHTML = data;
  }
}


var showPopupUrlOnLoad = null;
function showPopupUrl( url, onLoadEvent )
{
  if(typeof onLoadEvent == "undefined")
    showPopupUrlOnLoad = null;
  else
    showPopupUrlOnLoad = onLoadEvent;

  getAjax(url, "showPopupUrlHandler");
}

function showPopupUrlHandler(data)
{
	showPopUp("", data );

  if( showPopupUrlOnLoad != null )
    showPopupUrlOnLoad();
}