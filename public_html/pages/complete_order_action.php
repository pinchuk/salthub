<?
/*
This is for users who are adding more than one category to their business page.  Adds the
category to their shopping cart, and sends them to the billing page.
*/

require( "../inc/inc.php" );
require( "../billing/billing-functions.php" );

$gid = $_POST['gid'];

$unfunded_items = array();
$unfunded_items[0] = $_SESSION['unfunded_categories'][$gid];
$unfunded_items[1] = $_SESSION['unfunded_sectors'][$gid];

for( $c = 0; $c <= 1; $c++ )
{
  if( sizeof( $unfunded_items[$c] ) > 0 )
  {
    switch( $c )
    {
      case 0: $type = PURCHASE_TYPE_CAT_LISTING; break;
      case 1: $type = PURCHASE_TYPE_IND_LISTING; break;
    }

    $qty = sizeof( $unfunded_items[$c] );

    billingAddItemToCart( $type, $gid, $qty );
  }

}

if( $_SESSION['dir_listing'][$gid] == 1 || $_POST['dir_listing'] == 1 )
{
  billingAddItemToCart( PURCHASE_TYPE_DIR_LISTING, $gid );
}

header( "Location: /billing/" );
?>