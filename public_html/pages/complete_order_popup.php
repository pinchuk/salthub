<?
/*
This is a popup presented to users who have added "premium" items to their page.  If the user confirms
that the items shoudl be added to the page, then items are added to their cart, and they're forwarded
to the checkout page.
*/

include_once( "../inc/inc.php" );

//$gid = intval( $_GET['uid'] ); //This now comes from the parent page that includes this

?>

<form action="/pages/complete_order_action.php" method="POST">

<input type="hidden" name="gid" value="<?=$gid?>" />

<div style="width: 475px; font-size:9pt;">
  <div style="color:#326798; font-weight:bold;">
    Well done! Target more Decision Makers by completing your order
  </div>
    <div style="border-bottom: 2px solid #d8dfea; margin-top:3px; margin-bottom:5px;"></div>

  <div style="color:#555;">
    Your first category and industry are free.  Charges apply for additional selections and to be listed in the <?=$siteName?> Business Directory and News Feed, accessible on the web and on our app.
  </div>

  <div style="margin-top:10px; width:100%;">
    <div class="billing_header" style="padding-left:0px; font-size:10pt; margin-bottom:5px;">Order Information</div>

    <div style="border-bottom: 2px solid #d8dfea; margin-top:3px; margin-bottom:5px;"></div>

<?

$unfunded_items = array();
$unfunded_items[0] = $_SESSION['unfunded_categories'][$gid];
$unfunded_items[1] = $_SESSION['unfunded_sectors'][$gid];

for( $c = 0; $c <= 1; $c++ )
{
  $title = "";
  switch( $c )
  {
    case 0: $title = "Additional Product/Service Listing(s)"; $cost_per = quickQuery( "select cost from billing_purchase_types where id='" . PURCHASE_TYPE_CAT_LISTING . "'" );; break;
    case 1: $title = "Additional Industry Listing(s)"; $cost_per = quickQuery( "select cost from billing_purchase_types where id='" . PURCHASE_TYPE_IND_LISTING . "'" ); break;
  }

  $cost = $cost_per * sizeof( $unfunded_items[$c] );
  $total += $cost;

  if( sizeof( $unfunded_items[$c] ) > 0 )
  {
  ?>
  <div class="order_itemhead"><?=$title?></div>
  <div class="order_item" style="text-align:right;">$<?= number_format( $cost, 2 ); ?></div>
  <?
  for( $d = 0; $d < sizeof( $unfunded_items[$c] ); $d++ )
  {
  ?>
  <div style="clear:both; margin-left: 50px; color:rgb(50, 103, 152);">
     <?=quickQuery( "select catname from categories where cat='" . $unfunded_items[$c][$d] . "'" ); ?>
  </div>
  <?
  } }
}

$dir_listing_funded = quickQuery( "select dir_listing_funded from pages where gid='$gid'" );

if( !$dir_listing_funded )
{

$title = "Listed in the <?=$siteName?> Business Directory";
$cost_per = quickQuery( "select cost from billing_purchase_types where id='" . PURCHASE_TYPE_DIR_LISTING . "'" );
?>
  <div class="order_itemhead" style="width:350px;">
<?
if( $_SESSION['dir_listing'][$gid] != 1 )
{
?>
  <input type="checkbox" name="dir_listing" value="1" checked="checked" onchange="javascript: e=document.getElementById('total'); if(e) { f = document.getElementById('base_cost'); total = parseInt(f.value); if( this.checked) total+=<?=$cost_per?>; e.innerHTML = '$' + total.toFixed(2); }"/> <?=$title?> <span style="font-weight:300; font-size:8pt; color: rgb(50, 103, 152)">(recommended)</span>
<?
}
else echo $title;

?>
  </div>

<input type="hidden" name="base_cost" id="base_cost" value="<?=$total?>"/>
<?$total += $cost_per; ?>

  <div class="order_item" style="text-align:right;">$<?= number_format( $cost_per, 2 ); ?></div>


    <div style="clear:both; padding-top:5px;"></div>
  </div>
<?
}
?>
  <div style="border-bottom: 2px solid #d8dfea; margin-top:3px; margin-bottom:5px;"></div>

  <div style="clear:both; width:100%; padding-top:10px; font-weight:bold; color:#000;">
    <div style="float:left; font-size:9pt;">Total:</div>
    <div class="order_item" style=" text-align:right; font-size:9pt; font-weight:bold; float:right; font-weight:bold;" id="total">$<?= number_format( $total, 2 ); ?></div>
  </div>

  <div style="clear:both; border-bottom: 2px solid #d8dfea; margin-top:3px; margin-bottom:5px;"></div>

  <div style="padding-top:10px; margin-top:10px;">
    <div style="width: 225px; text-align:center; height:40px; float:left; padding-top:13px;">
      <a href="javascript: void();" onclick="javascript: closePopUp();">Cancel Order</a>
    </div>

    <div style="width: 225px; text-align:center; height:40px; float:left;">
      <input type="submit" class="button" style="padding:10px;" value="Submit Order"/>
    </div>
  </div>

  <div style="clear:both;"></div>
</div>
</form>