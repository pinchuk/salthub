<?php
//Allow the FB crawler to see this site without logging in.
if( stristr( $_SERVER['HTTP_USER_AGENT'], "facebookexternal" ) !== false )
{
  $facebookCrawler = true;
  $noLogin = true;
}

$title = "Maritime Business Directory | SaltHub - Suppliers - Services - Shipyards - Brokers";
$meta[] = array( "property" => "og:description", "content" => "SaltHub's business directory is a free resource for professionals who live, work, and engage in and around the water. Find and list shore based and seagoing businesses associated with maritime and water related sectors." );
$meta[] = array( "property" => "og:title", "content" => "$siteName Business Directory" );
$meta[] = array( "property" => "og:url", "content" => "http://" . SERVER_HOST . "/directory.php" );
$meta[] = array( "property" => "og:image", "content" => "http://www.salthub.com/images/salt_badge100.png" );
$meta[] = array( "property" => "og:site_name", "content" => $siteName );
$meta[] = array( "property" => "og:type", "content" => "website" );
$meta[] = array( "property" => "fb:app_id", "content" => $fbAppId );

$scripts[] = "/pages/pages.js.php";
$scripts[] = "/profile/profile.js";
$scripts[] = "/tipmain.js";
$scripts[] = "/comments.js";
$scripts[] = "/pages/multi_select.js.php";

include "header.php";

$API->setToDoItem( TODO_EXPLORE_GROUPS );

$page = 0;
if( isset( $_GET['p'] ) )
  $page = $_GET['p'];

$home = true;
$results_per_page = 15;
$result_index = $page * $results_per_page;

if( isset( $_GET['search'] ) )
{
  $conditions = 0;

  $sql = "select * from pages natural join categories where categories.industry=" . PAGE_TYPE_BUSINESS;

  if( $_GET['kw'] != "" )
  {
    $sql .= " AND";
    $kw = addslashes($_GET['kw']);
    $sql .= " (gname like '%$kw%' or contactfor like '%$kw%')";
//    $sql .= " gname like '%$kw%'";
    $conditions++;
  }

  if( $_GET['loc'] != "" & $_GET['loc'] != "Country, State, City" )
  {
    $sql .= " AND";
    $loc = addslashes($_GET['loc']);
    $sql .= " address like '%$loc%'";
    $conditions++;
  }

  if( $_GET['cat'] > 0 )
  {
    $sql .= " AND";

    $cat = intval($_GET['cat']);
    $sql .= " cat=" . $cat;
    $conditions++;
  }

  if( $_GET['subcat'] > 0 )
  {
    $sql .= " AND";

    $subcat = intval($_GET['subcat']);
    $sql .= " subcat=" . $subcat;
    $conditions++;
  }

  if( $conditions > 0 )
  {
    $sq = mysql_query( $sql );
    $num_results2 = mysql_num_rows( $sq );
    $sql .= " order by gname limit $result_index, $results_per_page";
    $sq = mysql_query( $sql );

    $home = false;
  }
}

?>

<div class="trickyborder">
	<div class="left">
		<div class="blinder">&nbsp;</div>
		<img src="/images/telephone_dir.JPG" alt="" />

    <table border="0" cellpadding="0" cellspacing="0" class="mediaactions" style="margin-top:5px">
    	<tr>
    		<td style="padding-right: 0px;">
          <iframe src="http://www.facebook.com/plugins/like.php?app_id=<? echo $fbAppId; ?>&amp;href=<? echo urlencode("http://" . SERVER_HOST . "/directory.php" ); ?>&amp;send=false&amp;layout=button_count&amp;width=85&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:85px; height:21px; margin-top:3px;" allowTransparency="true"></iframe>
        </td>
    		<td>
          <a href="http://twitter.com/share" class="twitter-share-button" data-url="http://shb.me/2" data-text="SaltHub's business directory is for professionals in the maritime & water related sectors. Find & list businesses." data-count="horizontal" data-via="<?=SITE_VIA?>">Tweet</a>
        </td>
    	</tr>
    </table>


		<div class="subhead">
			Directory Search
		</div>

    <div class="getconnected" style="color:#555;">
      <div class="smtitle" style="padding-bottom: 5px;">Find Businesses &amp; Services</div>
      Use the Directory Search tool below to find a business or service.  If you don't find what you want, refine your search.
    </div>

<form action="directory.php" method="GET" onsubmit="javascript: document.getElementById('subcat').value = selectedValue( document.getElementById('subcat_temp') );">
<input type="hidden" id="subcat" name="subcat" />
    <div style="color:#555; font-size:9pt; margin-top:5px;">Keywords:</div>
    <div><input name="kw" type="text" id="keywords" style="color:#555; width:155px;" value="<? echo $_GET['kw']; ?>"/></div>

    <div style="color:#555; font-size:9pt; margin-top:5px;">Sector:</div>
    <div>
      <select name="cat" style="color:#555; width:160px;" onchange="javascript: refreshSubcats( selectedValue( this ) );">
      <option value=""> - </option>
<?
$q = sql_query( "select * from categories where cattype='G' and industry='" . PAGE_TYPE_BUSINESS . "' order by catname desc" );
while( $r = mysql_fetch_array( $q ) )
{
?>
          <option value="<? echo $r['cat']; ?>"<? if( $r['cat'] == $_GET['cat'] ) echo " SELECTED"; ?>><? echo $r['catname']; ?></option>
<?
}
?>
      </select>
    </div>


    <div style="color:#555; font-size:9pt; margin-top:5px;">Category:</div>
    <div id="company_subcats">
      <select name="subcat_temp" id="subcat_temp" style="color:#555; width:160px;">
      <option value=""> - </option>
<?
if( $_GET['cat'] > 0 )
{
  $q = sql_query("select cat,catname from categories where cat IN (SELECT child FROM category_relationships where parent='" .  $_GET['cat'] . "') order by catname");

//  $q = sql_query( "select * from categories where industry='" . $_GET['cat'] . "' order by catname" );
  while( $r = mysql_fetch_array( $q ) )
  {
?>
          <option value="<? echo $r['cat']; ?>"<? if( $r['cat'] == $_GET['subcat'] ) echo " SELECTED"; ?>><? echo $r['catname']; ?></option>
<?
  }
}
?>

      </select>
    </div>

    <div style="color:#555; font-size:9pt; margin-top:5px;">Location:</div>
    <div><input name="loc" type="text" id="location" style="color:#555; width:155px;" value="<? if( isset( $_GET['loc'] ) ) echo $_GET['loc']; else echo 'Country, State, City'; ?>" onclick="javascript: if( this.value == 'Country, State, City' ) this.value='';"/></div>

    <div style="color:#555; font-size:9pt; margin-top:10px;">
      <input type="submit" class="button" name="search" value="Search" />
    </div>

</form>

<?php
    if( isset( $_SESSION['recentPages'] ) && strlen( $_SESSION['recentPages'] ) > 0 )
    {
?>
		<div class="subhead">
			Recently Viewed
		</div>
<?
/*
    $x = sql_query("
			SELECT distinct gid,gname,pid,hash
			FROM pages
			natural join categories JOIN photos ON photos.id=pid
			WHERE categories.industry=1 and pages.gid IN ( select gid from feed where gid != 0 and ts > DATE_SUB(NOW(),INTERVAL 40 DAY) )
      and pages.gid not in ( select gid from page_members where uid={$API->uid})
			GROUP BY gid order by rand() limit 10
		") or die(mysql_error());

		while ($page = mysql_fetch_array($x, MYSQL_ASSOC))
			showPageSmallPreview($page);
*/
      $recentPages = explode( ",", $_SESSION['recentPages'] );

      for( $c = 0; $c < 10 & $c < sizeof( $recentPages); $c++ )
      {
        if( $recentPages[$c] != "" )
        {
          $page = mysql_query( "select gid, gname, pid, hash from pages natural join categories left JOIN photos ON photos.id=pid where categories.industry=" . PAGE_TYPE_BUSINESS . " and gid='" . $recentPages[$c] . "'" );
          $page = mysql_fetch_array( $page );
          if( $page )
      			showPageSmallPreview($page);
        }
      }
    }
		?>
	</div>
	<div class="topborder">
		<div class="biggertext title" style="margin-top:0px; padding-top:0px; padding-bottom:4px;">
			Business Directory
		</div>
	</div>
	<div class="right">
		<!--<form action="" onsubmit="return false;"><div style="white-space: nowrap;"><input type="text" style="width: 251px;" id="txtsearch1" onkeypress="javascript:searchKeypress(event);" onfocus="javascript:if (this.value=='Search ...') this.value='';" onblur="javascript:if (this.value=='') this.value='Search ...';" class="txtsearch" value="Search ..." /><input type="text" onclick="javascript:searchDo();" onfocus="document.getElementById('txtsearch1').focus();" name="q" class="btnsearch" value="" /></div></form>-->
		<?php showCreatePageWide();
    if( $API->adv ) {
    ?>
		<div class="subhead" style="margin-bottom: 10px;"><div style="float:left;">Sponsors</div><div style="float:right;"><a href="http://www.salthub.com/adv/create.php" style="font-size:8pt; font-weight:300; color:rgb(0, 64, 128);">create an ad</a>&nbsp;</div><div style="clear:both;"></div></div>
		<?php showAd("companion"); } ?>
		<?php include "inc/connect.php";

    if( $API->adv ) {
    ?>
		<div class="subhead" style="margin-bottom: 10px;"><div style="float:left;">Sponsors</div><div style="float:right;"><a href="http://www.salthub.com/adv/create.php" style="font-size:8pt; font-weight:300; color:rgb(0, 64, 128);">create an ad</a>&nbsp;</div><div style="clear:both;"></div></div>
		<?php showAd("companion"); } ?>
		<?php include "inc/pymk.php"; ?>
	</div>
	<div class="links">
		<div class="toplink"><img src="/images/find.png" /><a href="/pages">Browse Pages</a></div>
		<div class="divider">&nbsp;</div>
		<div class="toplink"><img src="/images/vcard_add.png" /><a href="javascript:void(0);" onclick="javascript: showPopupUrl( '/pages/page_create_popup.php?a', createDropDowns );">Get Listed</a></div>
		<div class="toplink"><img src="/images/sound_mega_phone.PNG" /><a href="/adv/">Advertise</a></div>
		<div class="toplink"><img src="/images/checkmark_sticker_16.png" width="16" height="16"/><a href="/pages/claim_company.php">Claim your Listing</a></div>
	</div>
	<div class="middle">
<?
if( $home )
{
?>
		<div class="subhead">Business news by sector</div>

    <div style="margin-top:3px; margin-bottom:5px;">
  		<div style="float: left;"><img src="/images/application_view_list.png" alt="" /></div>
    	<div style="float: left; font-size: 9pt; position: relative; padding-right: 15px; margin-right: 10px;">
        <div style="cursor: default;" onmouseover="javascript:if (ddTimer) clearTimeout(ddTimer); document.getElementById('view-dd').style.display = '';" onmouseout="javascript:ddTimer = setTimeout('document.getElementById(\'view-dd\').style.display=\'none\';', 200);">
        	<div style="width: 100px; padding-left: 3px; float: left;"><span id="view-chosen" style="font-size:8pt; padding-top:1px;">View by sector&#0133;</span></div> &nbsp;<img src="/images/down.png" alt="" />
        	<div style="display: none; width:100px;" class="dropdown viewdd" id="view-dd">
  <?
  $q = mysql_query( "select * from categories where industry=" . PAGE_TYPE_BUSINESS . " and cattype='G'" );
  while( $r = mysql_fetch_array( $q ) )
  {
  ?>
          <a href="/directory.php?s=<? echo $r['cat']; ?>"><? echo $r['catname']; ?></a><br />
  <?
  }
  ?>
        	</div>
        </div>
      </div>
    </div>

    <div style="clear:both; height:5px;"></div>

    <? if( !isset( $_COOKIE['hideConnectWidth'] ) )
      { ?>
    <div class="getconnected" style="cursor:auto; color:#555; width:430px; margin-bottom:10px;" id="connectwith">
      <div class="smtitle" style="padding-bottom: 5px; float:left;">Connect with your peers and customers </div>
      <div style="float:left; color:#555; font-size: 8pt; padding-left:10px; ">(recommended)</div>
      <div style="float:right; color:#555; font-size:9pt; padding-right:3px;"><a href="javascript:void(0);" onclick="javascript: document.getElementById('connectwith').style.display='none'; createCookie( 'hideConnectWidth', '1', 15 );" style="color:#555;">X</a></div>

      <div style="clear:left; float:left; color:#555; width:325px; padding-bottom:5px;">
        Using the <? echo $siteName ?> Business Feed will help you connect with industry peers and customers.  Adding content, updates and news about your company or service to your <a href="/pages/#mypages">Business Page</a> will be shown in the feed below.  The feed categorizes news by sector and any <a href="/pages/claim_company.php">verified</a> company, big or small, can use it.
      </div>

      <div style="float:right; width:80px; text-align:center; font-size:9pt; margin-right:5px;">
        <img src="images/pages_48.png" width="40" height="50" alt="" style="padding-bottom:3px;"/><br />
        <a style="text-decoration:underline;" href="javascript:void(0);" onclick="javascript:  showPopupUrl( '/pages/page_create_popup.php?a', createDropDowns );">start here &gt;&gt;</a>
      </div>

      <div style="clear:both;"></div>
    </div>

    <div style="clear:both; height:10px;"></div>
    <? } ?>
   	<div id="feeds">
    <?
    $cat = 111;
    if( isset( $_GET['s'] ) ) $cat = intval( $_GET['s'] );

    include "inc/mod_comments.php";
    include "profile/getlogentries.php";

    $gleResult = getLogEntries(array("gid" => 0, "uid" => -7, "mini" => 0, "feedLimit" => 12, "cat" => $cat));
    ?>
    </div>
    <?
		if ($gleResult['more']) // there are more feed entries left
		{
		?>
		<div id="feedshowmore" class="feedshowmore">
			<a href="javascript:void(0);" onclick="javascript:showMoreLogEntries();">show more &#0133;</a>
		</div>
    <?
    }
}
else
{
?>
	<div class="subhead">
		Matches
	</div>
<?
  $friends = $API->getFriendsOfFriends(false);

  if( mysql_num_rows( $sq ) == 0 )
    echo '<div style="width:100%; text-align:center; padding-top:20px;">Sorry, no results were found!</div>';
  else
  {
    while( $r = mysql_fetch_array( $sq ) )
    {
      showPagePreviewFull( $r );

      if( $r['gid'] > 0 )
      {
        if( sizeof( $friends ) > 0 )
        {
          $conn_q = sql_query( "SELECT page_members.uid FROM page_members WHERE page_members.gid='" . $r['gid'] . "' and page_members.uid IN (" . implode( ",", $friends ) . ")" );
          if( mysql_num_rows( $conn_q ) > 0 )
          {
          //  echo '<div style="clear:both; margin-left:55px;"><a href="javascript:void(0);" onclick="javascript:showLinksToPage(' . $r['gid'] . ');" style="font-size:8pt;"><img src="images/connected.png" width="16" height="16" alt="" /> ' . plural( mysql_num_rows( $conn_q ), 'connection' ) . '</a></div>';
              $login = $API->isLoggedIn() ? 1 : 0;
              echo '<div style="clear:both; margin-left:55px;"><a href="javascript:void(0);" onclick="javascript:showLinksToPage(' . $r['gid'] . ', '.$login.');" style="font-size:8pt;"><img src="images/connected.png" width="16" height="16" alt="" /> ' . plural( mysql_num_rows( $conn_q ), 'connection' ) . '</a></div>';
          }
        }
      }



      echo '<div style="clear:both; height:5px;"></div>';
    }
  }

  $num_results = $num_results2;
  $page = $_GET['p'];
  $pages = floor($num_results / $results_per_page);

  include( "inc/pagination.php" );

}
?>

	</div>
	<div style="clear: both;"></div>
</div>

<script language="javascript" type="text/javascript">
var pagesShown = <?=json_encode($pagesShown)?>;
var fidold = <?=intval($gleResult['fidold'])?>;
var fidnew = <?=intval($gleResult['fidnew'])?>;

function refreshSubcats( cat )
{
	getAjax("/pages/page_create_subcats.php?compact=1&cat=" + cat, function (data)
	{
    e = document.getElementById( "company_subcats" );
    if( e  )
    {
      e.innerHTML = data;
    }
	});
}

function refreshSubcats2( cat )
{
	getAjax("/pages/page_create_subcats.php?cat=" + cat, function (data)
	{
    e = document.getElementById( "company_subcats2" );
    if( e  )
    {
      e.innerHTML = data;
    }
	});
}

function showNewLogEntries()
{
	url = "/profile/getlogentries.php?gid=<?=$gid?>&uid=-7&cat=<? echo $cat ?>&newerthan=" + fidnew;

	getAjax(url, function(data)
	{
		eval("json = " + data + ";");

		if (json.fidnew == null)
			return; //nothing new

		addFeed(json.html, true);

		fidnew = json.fidnew;
	});
}

function addFeed(html, top)
{
	newdiv = document.createElement("div");
	newdiv.innerHTML = html;
	newdiv.id = "newfeed" + new Date().getTime();

	e = document.getElementById("feeds");

	if (top)
		e.insertBefore(newdiv, e.firstChild);
	else
		e.appendChild(newdiv);

	h = getHeight(newdiv);
	slidedown(newdiv.id, h, true);
}

function showMoreLogEntries()
{
	url = "/profile/getlogentries.php?gid=<?=$gid?>&uid=-7&cat=<? echo $cat ?>&olderthan=" + fidold;

	getAjax(url, function(data)
	{
		eval("json = " + data + ";");

		if (json.more == "0") //reached the end of the user's feed
			document.getElementById("feedshowmore").style.display = "none";
		else if (json.fidnew == null)
			return; //didn't return any results - user probably should not have gotten here

		addFeed(json.html, false);

		fidold = json.fidold;
	});
}

function createDropDowns()
{

  newComboListBox( 3, "PandS" );
  newComboListBox( 4, "ind" );
}


function checkPageCreateForm()
{
  name = document.getElementById( 'l1_gname' ).value;

  if( name == 'Page Name' || name == '' )
  {
    alert( 'Please enter a page name before continuing.' );
    return false;
  }
  return true;
}
</script>

<?php

include "footer.php";

?>