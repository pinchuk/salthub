<?php
/*
Called from follow_page_full.php

Displays social media links and (optionally) saves changes.
*/

include_once "../inc/inc.php";

if( !isset( $gid ) )
  $gid = $_POST['gid'];

$sites = array();
$sites[] = array("fbid", "http://www.facebook.com/", "f.png");
$sites[] = array("twid", "http://twitter.com/", "t.png");
$sites[] = array("linkedin_url", "http://www.linkedin.com/", "linkedin.png");
$sites[] = array("ytid", "http://www.youtube.com/", "youtube.png");

if ($_POST['save'] == "1")
{
	foreach ($sites as $ssite)
		$q[] = $ssite[0] . "='" . $_POST[$ssite[0]] . "'";

	mysql_query("update pages set " . implode(",", $q) . " where gid=" . $gid );
}

foreach ($sites as $ssite)
	$fields[] = $ssite[0];

if (!isset($page))
	$page = $API->getPageInfo($gid);

$found = false;
foreach ($sites as $ssite)
	if ($page[$ssite[0]])
	{
		$found = true;
		echo '<a href="' . $ssite[1] . $page[$ssite[0]] . '" target="_blank"><img src="/images/' . $ssite[2] . '" alt="" /></a>';
	}

if (!$found)
{
  if( quickQuery( "select admin from page_members where uid='" . $API->uid . "' and gid='" . $gid . "'" ) )
		echo '<div style="padding: 10px; text-align: center; font-size: 8pt;">Add your website and other social profiles <a href="javascript:editFollowPage();">here</a>.</div>';
}
?>