<?php
/*
Popup that enables the user to choose which of their media to add to the page.
*/

include "../inc/inc.php";

$gid = intval( $_GET['gid'] );

$gname = quickQuery( "select gname from pages where gid='$gid'" );

$page = $API->getPageInfo($gid);

ob_start();

if( $page['pid'] == 0 )
  $picurl ="/images/no_vessel_119.png";
else
  $picurl = "/img/119x95/" . $page['profile_pic'];

?>


<div style="float: left; width: 58px;">
	<a href="<?=$API->getPageURL($gid)?>"><img width="48" src="<?=$picurl?>" alt="" /></a>
</div>

<div style="float: right; width: 401px; font-size: 9pt; padding-top: 3px;">


	<span style="font-weight: bold;">Add your media to the '<? echo $gname ?>' page</span>
	<div style="border-top: 2px solid #d8dfea; padding-top: 5px; font-size: 8pt;">
    <div style="border: 1px solid #d8dfea; width:250px; padding:0px; float:left;">
      <div style="float:left;"><img src="/images/magglass.png" width="16" height="16" alt="" style="padding-top:3px; padding-left:3px;"/></div>
      <div style="float:left; margin-top:3px;"><input id="mediaChooserSearch" style="border:0px; font-size:9pt; margin:0px; width:220px; height:12px;" name="search" size="25" value="Search ..." onclick="if( this.value == 'Search ...' ) this.value = '';" onblur="if( this.value=='') this.value = 'Search ...';" onkeyup="javascript:mediaSearch(this.value);"/></div>
      <div style="clear:both;"></div>
    </div>

      <div style="float:right; font-size:9pt; padding-top:8px;">

      <?php
			showNavDrop('Show <img src="/images/arrow_down.png"  width="11" height="9" alt="" />', array(
				array("Photos", "javascript:showMediaChooser(" . $gid . ",'P');", "images"),
				array("Videos", "javascript:showMediaChooser(" . $gid . ",'V');", "television"),
				array("All", "javascript:showMediaChooser(" . $gid . ",'*');", "find"),
					), "top: 20px; left: -102px;");
			?>

      </div>
	</div>
</div>

<div style="clear: both; height: 5px;"></div>

<div style="height:376px; overflow-x:hidden; overflow-y:scroll; width:455px;">

<?
$media = array();
$types = array( "P", "V" );

foreach( $types as $type )
{
  switch( $type )
  {
    case "P":
      $type_word = "photo";
     // $sql = "select id, ptitle as title, hash, pdescr as descr from photos where uid='" . $API->uid . "'";
    break;

    case "V":
      $type_word = "video";
     // $sql = "select id, title, hash, descr from videos where uid='" . $API->uid . "'";
    break;
  }


  $sql  = "	SELECT media.id,hash,'$type' as type,IF(gid={$gid},1,0) AS inpage, " . ($type == "P" ? "IFNULL(ptitle,title) AS " : "") . "title, " . ($type == "P" ? "IFNULL(pdescr,descr) AS " : "") . " descr
			FROM {$type_word}s as media
			" . ($type =="P" ? "INNER JOIN albums ON media.aid=albums.id" : "") . "
			LEFT JOIN page_media gm ON media.id=gm.id AND gm.type='$type' and gid={$gid}
			where media.uid={$API->uid}
		";

  $q = sql_query( $sql );
//  echo mysql_error();


?>
<div id="mediaChooser-<? echo $type ?>" style="margin-bottom:15px;">
  <div class="subhead" style="margin-bottom: 5px;">Your <? echo ucfirst( $type_word ) ?>s</div>
<?
if( mysql_num_rows( $q ) > 0 )
{
?>
  <div style="font-size:8pt;">Items will be added when checked</div>
<?
  while( $r = mysql_fetch_array( $q ) )
  {
    $media[] = $type . $r['id'];
    $url = $API->getMediaURL($type, $r['id'], $r['title']);
?>
  <div id="media-<? echo $type . $r['id']; ?>" style="margin-top:5px;">
    <div id="media-search-<? echo $type . $r['id']; ?>" style="display:none;"><? echo $r['title'] . " " . $r['descr']; ?></div>
    <div style="float:left"><input <? if( $r['inpage'] ) echo ' CHECKED '; ?>type="checkbox" id="media-selected-<? echo $type . $r['id'] ?>" value="<? echo $type . $r['id'] ?>" onchange="javascript:selectMedia('<? echo $r['id']; ?>', '<? echo $type ?>', <? echo $gid ?>, !this.checked );"/></div>
    <div style="float:left; width:105px; height:77px; margin-left: 5px;">
      <a title="<? echo $r['descr'] ?>" href="<? echo $url ?>"><img src="<? echo $API->getThumbURL(1, 100, 75, '/' . strtolower( $type_word ) . 's/' . $r['id'] . '/' . $r['hash'] . '.jpg') ?>" width="100" height="75"/></a>&nbsp;
    </div>
    <div style="float:left; margin-left: 5px; margin-top:-1px; width:295px;">
    	<div style="color:#004080; font-size:10pt;"><? echo $r['title']; ?></div>
    	<div style="border-top: 1px solid #d8dfea; padding-top: 2px; margin-top: 1px; font-size: 8pt;"><? echo $r['descr']; ?></div>
    </div>
  </div>
  <div style="clear:both;"></div>
<?
  }
}
else
{
?>
  <div style="font-size:9pt;">You don't have any <? echo $type_word ?>s uploaded to your account.</div>
<?
}
?>
</div>


<?
}
?>
</div>

<?
$html = ob_get_contents();
ob_end_clean();

$html = str_replace("\n", "", $html );
$html = str_replace("\r", "", $html );
$html = str_replace("\t", "", $html );


echo $html . chr(1) . implode(",", $media) ;
?>
