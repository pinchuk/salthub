<? include( '../inc/inc.php' ); ?>
function leavePage(gid)
{
	if (!confirm("Are you sure you want to leave this page?"))
		return;
	
	postAjax("/pages/leave.php", "gid=" + gid, "eval");
}

function addPageMedia(type)
{
	html  = '<div class="smtitle2" style="font-size: 10pt; padding-bottom: 3px; border-bottom: 2px solid #d8dfea;">';
	html += '	Add ' + (type == "V" ? "Videos" : "Photos") + ' to <a href="' + page.url + '">' + page.gname + '</a>';
	html += '</div>';
	html += '<div style="width: 200px; float: left; border-right: 1px solid #d8dfea;">';
	html += '	<div class="btnaddmedia" onclick="javascript:showMediaChooser(' + page.gid + ', \'' + type + '\');">Add Existing Media</div>';
	html += '</div>';
	html += '<div style="width: 200px; float: left;">';
	html += '	<div class="btnaddmedia" onclick="javascript:showTweetBox();">Upload New Media</div>';
	html += '</div>';
	html += '<div style="clear: both;"></div>';

	showPopUp("", html);
}

function updatePage(data)
{
	data.gid = page.gid;
	postAjax("/pages/update.php", "json_data=" + escape(json_encode(data)), "updatePageHandler");
}

function updatePageHandler( data )
{
//  alert( data );
}

var pageExistingMedia;
function showAddPageExistingMediaHandler(x)
{

	eval('data = \' + x + \';');
	showPopUp("", data.html, [541,494]);

	pageExistingMedia = data.media;
}

function showAddPageExistingMedia(type)
{
	getAjax("/pages/addmedia.php?gid=" + page.gid, "showAddPageExistingMediaHandler");
}

function modifyPageMember(gid, uid, action)
{
	postAjax("/pages/modifymember.php", "uid=" + uid + "&gid=" + gid + "&action=" + action, function(data)
		{
			eval(data);
		}
	);
}

var filterInPage = -1;
var filterType = "";

function filterAddPageExistingMedia(type, inpage)
{
	if (typeof type == "undefined") // all
	{
		filterInPage = -1;
		filterType = "";
	}
	else
	{
		if (type != null)
			filterType = type;
		
		if (inpage != null)
			filterInPage = inpage;
	}
	
	document.getElementById("agem-a").style.fontWeight = filterType == "" && filterInPage == -1 ? "bold" : "";
	document.getElementById("agem-v").style.fontWeight = filterType == "V" ? "bold" : "";
	document.getElementById("agem-Vhead").style.display = filterType == "V" || filterType == "" ? "" : "none";
	document.getElementById("agem-p").style.fontWeight = filterType == "P" ? "bold" : "";
	document.getElementById("agem-Phead").style.display = filterType == "P" || filterType == "" ? "" : "none";
	document.getElementById("agem-i").style.fontWeight = filterInPage == 1 ? "bold" : "";
	document.getElementById("agem-n").style.fontWeight = filterInPage == 0 ? "bold" : "";

	document.getElementById("agem-Vhead").innerHTML = "Videos " + (filterInPage == 0 ? "Not in Page" : (filterInPage == 1 ? "In Page" : ""));
	document.getElementById("agem-Phead").innerHTML = "Photos " + (filterInPage == 0 ? "Not in Page" : (filterInPage == 1 ? "In Page" : ""));

	searchAddPageExistingMedia();
}

function searchAddPageExistingMedia(q)
{
	if (q == null)
	{
		q = document.getElementById("txtgrpsearch").value;
		
		if (q == "Search ...")
			q = "";
	}
	
	q = q.toLowerCase();
	
	for (var i in pageExistingMedia)
	{
		e = document.getElementById("mps-" + pageExistingMedia[i].type + pageExistingMedia[i].id);
		
		if (pageExistingMedia[i].title.indexOf(q) == -1 || (filterInPage > -1 && pageExistingMedia[i].inpage != filterInPage) || (filterType != "" && pageExistingMedia[i].type != filterType))
			e.style.display = "none";
		else
			e.style.display = "";
	}
}

function editPageAbout(edit)
{
	if (edit)
		disp = ['', 'none', 230];
	else
		disp = ['none', '', 125];

	fields = ['descr', 'cat', 'email', 'www', 'address', 'privacy', 'phone', 'cell', 'subcat', 'fax', 'products', 'services', 'contact_person', 'save'<? if( $API->admin == 1 ) { ?>, 'gname'<? } ?>, 'exnames'];


	for (var i in fields)
		for (var j = 0; j < 2; j++)
    {
      e = document.getElementById("grpedit-" + fields[i] + j);
      if( e )
        e.style.display = disp[j];
//      else
//        alert( "Can't find grpedit-" + fields[i] + j );
    }

	for (var i = 0; i < 3; i++)
		document.getElementById("si" + i).style.height = disp[2] + "px";

	showBlueGrad();
}

function savePageAbout()
{
	fields = ['descr', 'cat', 'email', 'www', 'address', 'phone', 'cell', 'fax', 'subcat', 'products', 'services', 'contact_person', 'privacy'<? if( $API->admin == 1 ) { ?>, 'gname'<? } ?>, 'exnames','year','shipyard','hullid','imo','mmsi','callsign','flag','navalarchitect','stylist','decorator','sistership','refit','length','beam','draught','hullsuperstr','engine','speedmax','speedcruise','rangenm','fuelltr','waterltr', 'guests', 'crew' ];

	update = {'gid': page.gid};

	for (var i in fields)
	{
		e = document.getElementById("grpedit-" + fields[i]);
    if( e == null ) continue;

    if( typeof( e.value ) == "undefined" ) continue;

		val = escape(e.value);
		page[fields[i]] = val;
		update[fields[i]] = encodeURIComponent(val);

		if (fields[i] == "cat" || fields[i] == "privacy")
      if( document.getElementById("grpedit-" + fields[i] + "1") )
  			document.getElementById("grpedit-" + fields[i] + "1").innerHTML = e.options[e.selectedIndex].text;
		else
      if( document.getElementById("grpedit-" + fields[i] + "1") )
  			document.getElementById("grpedit-" + fields[i] + "1").innerHTML = htmlentities(val).replace(/\n/g, '<br>');
	}

	postAjax("/pages/update.php", "json_update=" + json_encode(update), "saveHandler");

	editPageAbout(false);
}

function saveHandler(data)
{
  //alert( data );

	location.reload(true);
}

function showMorePages(o)
{
	limit = 5;
	getAjax("/pages/previews.php?o=" + o + "&limit=" + limit + "&p=" + pagesShown[o], function(json)
	{
		eval('data = ' + json + ';');
		if (o == "r")
			document.getElementById("pages-r").innerHTML = data.html;
		else
		{
			newdiv = document.createElement("div");
			newdiv.innerHTML = data.html;
			p = document.getElementById("pages-" + o).appendChild(newdiv);

			if (data.pages.length < limit)
				document.getElementById("showmore-" + o).style.display = "none";
			else
				pagesShown[o] += data.pages.length;
		}
	});
}

function refreshSide(gid)
{
	postAjax("/profile/side.php", "gid=" + gid, "refreshHandler");
}

function refreshHandler( data )
{
  e = document.getElementById("sidediv");
  if( e )
  {
    e.innerHTML = data;
  }
}

function showCatSelection()
{
	getAjax("/pages/category_selection_popup.php?gid=" + page.gid, "showCatSelectionHandler");
}

function showCatSelectionHandler(html)
{
	showPopUp("", html, null, null, true );
}

function saveCatSelection()
{
  subcat = 0;
  cat = document.getElementById( 'cat' ).value;
  if( cat == 111 || cat == 112 )
  {
    subcat = document.getElementById( 'subcat' + cat ).value;
  }

  getAjax("/pages/save_category_selection.php?gid=" + page.gid + "&cat=" + cat + "&subcat=" + subcat, "saveCatSelectionHandler");

  closePopUp();
}

function saveCatSelectionHandler(data)
{

}

function refreshSubcatType( gid, cat )
{
  getAjax("/pages/subcat_selection.php?gid=" + gid + "&cat=" + cat, "refreshSubcatTypeHandler");
}

function refreshSubcatTypeHandler( data )
{
  e = document.getElementById( "subcat" );
  if( e )
  {
    e.innerHTML = data;
  }
  editPageAbout(true);
}