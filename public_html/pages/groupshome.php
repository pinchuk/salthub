<?php
$scripts[] = "/gettrending.js.php";
$scripts[] = "/mod_media.js";
$scripts[] = "/tween.js";
include "../header.php";
?>

<div class="bigtext2" style="clear: both; padding: 0 0 5px 5px;">Popular Media</div>

<div style="border: 1px solid #d8dfea; padding: 5px;">
	<div style="float: left; width: 642px;">
		<div style="width: 630px;">
			<div style="float: left;"><img src="/images/application_view_list.png" alt="" /></div>
			<div style="float: left; font-size: 9pt; position: relative; padding-right: 15px; margin-right: 10px; border-right: 1px solid #d8dfea;">
				<div style="cursor: default;" onmouseover="javascript:if (ddTimer) clearTimeout(ddTimer); document.getElementById('view-dd').style.display = '';" onmouseout="javascript:ddTimer = setTimeout('document.getElementById(\'view-dd\').style.display=\'none\';', 200);">
					<div style="width: 45px; padding-left: 3px; float: left;"><span id="view-chosen">View &#0133;</span></div> &nbsp;<img src="/images/down.png" alt="" />
					<div style="display: none;" class="dropdown viewdd" id="view-dd">
						<a href="/search.php?o=r">Most Recent</a><br />
						<a href="/search.php?o=v">Most Viewed</a><br />
						<a href="/search.php?o=of">Of Friends</a><br />
						<a href="/search.php?o=bf">By Friends</a>
					</div>
				</div>
			</div>		
			<div class="toplink"><a href="javascript:void(0);" onclick="javascript:showTweetBox();"><img src="/images/television_add.png" alt="" />Add Videos</a></div>
			<div class="toplink"><a href="javascript:void(0);" onclick="javascript:showTweetBox();"><img src="/images/image_add.png" alt="" />Add Photos</a></div>
			<div class="toplink"><a href="<?=$API->getProfileURL()?>/videos"><img src="/images/television.png" alt="" />My Videos</a></div>
			<div class="toplink"><a href="<?=$API->getProfileURL()?>/photos"><img src="/images/image.png" alt="" />My Photos</a></div>
			
			<div style="clear: both; height: 8px;"></div>

			<div class="subhead">Trending and Active</div>
			<div style="padding: 10px 0; position: relative;">
				<div id="marquee-parent" style="position: relative; overflow: hidden; height: 120px; width: 636px;">
					<?php
					$floatWidth = 106;
					$left = -$floatWidth;
					for ($i = 0; $i < 7; $i++)
					{
						echo '<div class="marqueefloat" style="left: ' . $left . 'px; top: 0px;" id="marquee-' . $i . '">';
						echo '	<div id="marquee-img-' . $i . '"></div>';
						echo '	<div class="title" id="marquee-title-' . $i . '"></div>';
						echo '</div>';
						$left += $floatWidth;
					}
					?>
					<div style="clear: both;"></div>
				</div>
			</div>
			
			<div class="subhead">Recently Viewed Media by Category</div>
		</div>
		<?php
		$x = sql_query("select * from categories order by catname");
		while ($cat = mysql_fetch_array($x, MYSQL_ASSOC))
		{
			// random
			$q1 = "select @type:='P' as type,media.id,ptitle,title,hash from photos as media inner join albums on media.aid=albums.id where " . $API->getPrivacyQuery() . " cat=" . $cat['cat'];
			$q2 = "select @type:='V' as type,media.id,null as ptitle,title,hash from videos as media where " . $API->getPrivacyQuery(null, true, "", false) . " cat=" . $cat['cat'];
			$xx = sql_query("select * from (($q1) union ($q2)) as tmp order by rand() desc limit 2") or die(mysql_error());
		?>
			<div class="category">
				<div class="title"><a href="/search.php?cat=<?=$cat['cat']?>"><?=$cat['catname']?></a></div>
				<?php
				while ($media = mysql_fetch_array($xx, MYSQL_ASSOC))
				{
					$title = $media['ptitle'] ? $media['ptitle'] : $media['title'];
					$url = $API->getMediaURL($media['type'], $media['id'], $title);
					echo '<div style="float: left; padding-top: 10px;">';
					echo '	<a href="' . $url . '"><img src="' . $API->getThumbURL(1, 100, 75, "/" . typeToWord($media['type']) . "s/" . $media['id'] . "/" . $media['hash'] . ".jpg") . '" alt="" /></a>';
					echo '</div>';
					echo '<div style="float: right; width: 82px; height: 75px; overflow: hidden; padding-top: 10px;">';
					echo '	<a href="' . $url . '">' . $title . '</a>';
					echo '</div>';
				}
				?>
			</div>
		<?php
		}
		?>
	</div>
	
	<div style="float: right; width: 300px;">
		<form action="" onsubmit="return false;"><input type="text" style="width: 252px;" id="txtsearch1" onkeypress="javascript:searchKeypress(event);" onfocus="javascript:if (this.value=='Search ...') this.value='';" onblur="javascript:if (this.value=='') this.value='Search ...';" class="txtsearch" value="Search ..." /><input type="text" onclick="javascript:searchDo();" onfocus="document.getElementById('txtsearch1').focus();" name="q" class="btnsearch" value="" /></form>
		
		<div style="clear: both; height: 6px;"></div>
		
		<?php if($API->adv ) { showAd("greeninfo"); ?>

		<div class="subhead" style="margin: 10px 0 5px;">Sponsors</div><?php showAd("companion"); ?>

		<div style="clear: both; height: 10px;"></div>
    <? }

		include "inc/mod_media.php";
		showMediaModule();

    if( $API->adv ) {
    ?>

		<div class="subhead" style="margin: 10px 0 5px;">Sponsors</div><?php showAd("companion"); ?>

		<?php } include "inc/pymk.php"; ?>
	</div>
	
	<div style="clear: both;"></div>
</div>

<script language="javascript" type="text/javascript">
<!--

var floatOrder = [0,1,2,3,4,5,6];
var floatMedia = new Array();
var showingMedia = "";

for (i = 0; i < 6; i++)
{
	changeMarqueeItem(i + 1, mediaQueue[i]);
	showingMedia += "," + mediaQueue[i].type + mediaQueue[i].id;
}

mediaQueue = [];

function changeMarqueeItem(i, obj)
{
	floatMedia[i] = obj;
	document.getElementById("marquee-img-" + i).innerHTML =  '<a href="' + obj.url + '"><img src="' + obj.img + '" alt="" /></a>';
	document.getElementById("marquee-title-" + i).innerHTML = '<a href="' + obj.url + '">' + obj.title + '</a>';
	document.getElementById("marquee-" + i).style.visibility = "visible";
}

var checkedTimes = 0;
function checkForNewMedia()
{
	if (mediaQueue.length > 0)
	{
		checkedTimes = 8;
		obj = mediaQueue.pop();
		changeMarqueeItem(floatOrder[0], obj);
		scrollMarquee();
	}
	else if (checkedTimes < 8)
		checkedTimes++;
	else
	{
		//alert("LOading new");
		checkedTimes = 0;
		loadjscssfile("/gettrending.js.php?t=" + lastUpdated + "&notIn=" + showingMedia.substring(1), "js");
	}
}

function scrollMarquee()
{
	lastFloat = floatOrder.pop();
	showingMedia = "";

	for (i = 0; i < 7; i++)
	{
		e = document.getElementById("marquee-" + i);
		l = parseInt(e.style.left.substring(0, e.style.left.length - 2));
		t = new Tween(e.style, "left", Tween.regularEaseInOut, l, l + <?=$floatWidth?>, 1, "px");
		if (i == lastFloat)
			t.onMotionFinished = function() { document.getElementById("marquee-" + lastFloat).style.left = "-<?=$floatWidth?>px"; }
		else if (typeof floatMedia[i] != "undefined")
			showingMedia += "," + floatMedia[i].type + floatMedia[i].id;
		t.start();
	}
	
	floatOrder.unshift(lastFloat);
}

var mediaOwner = <?=$API->uid?>;
showMedia("browse", "new", "", 0);

setInterval("checkForNewMedia()", 5000);

//-->
</script>

<?php
include "../footer.php";
?>