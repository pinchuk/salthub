<!-- Displays "Pages Your Might Like" Module -->
<div class="subhead" style="margin-top: 10px;">Pages You Might Like</div>

<div style="font-size: 8pt; padding: 5px 3px 0;">
	Pages are a great way to connect and share with others who have similar knowledge, expertise, and interests as you do.&nbsp;
	Give it a try by selecting one below or <a href="/pages">exploring pages</a>.
</div>

<div id="gyml" style="height:175px">
<?php

$limit = 4;
$o = "r";
//include "previews.php";
?>
</div>

<div style="clear: both; padding-top: 10px; text-align: right;">
	<a href="javascript:void(0);" onclick="javascript:gymlShuffle();" style="font-weight: bold; font-size: 9pt;"><img src="/images/arrow_rotate_anticlockwise.png" style="vertical-align: text-bottom;" alt="" /> Shuffle matches</a>
</div>

<script language="javascript" type="text/javascript">
<!--
gymlShuffle();
-->
</script>