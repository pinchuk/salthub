<?php
/*
Displays the content found at /pages.  Includes a list of the user's pages (where they are members), pages they might like, etc.
*/

//Allow the FB crawler to see this site without logging in.
if( stristr( $_SERVER['HTTP_USER_AGENT'], "facebookexternal" ) !== false )
{
  $facebookCrawler = true;
  $noLogin = true;
}

include( "../inc/inc.php" );

$meta[] = array( "property" => "og:description", "content" => "SaltHub's page directory is a free resource for professionals who live, work, and engage in and around the water. Find and list vessels as well as shore based and seagoing hobbies and organizations associated with maritime and water related sectors." );
$meta[] = array( "property" => "og:title", "content" => "$siteName Page Directory" );
$meta[] = array( "property" => "og:url", "content" => "http://" . SERVER_HOST . "/pages" );
$meta[] = array( "property" => "og:image", "content" => "http://www.salthub.com/images/pool.jpg" );
$meta[] = array( "property" => "og:site_name", "content" => $siteName );
$meta[] = array( "property" => "og:type", "content" => "website" );
$meta[] = array( "property" => "fb:app_id", "content" => $fbAppId );

if( $facebookCrawler )
{
  $API->uid = 91;
}

$scripts[] = "/pages/pages.js.php";
include "../header.php";

$API->setToDoItem( TODO_EXPLORE_GROUPS );

?>

<div class="trickyborder">
	<div class="left">
		<div class="blinder">&nbsp;</div>
		<img src="/images/pool.jpg" alt="" />
<? if( $_SERVER['REQUEST_URI'] == "/pages/" ) { ?>
    <table border="0" cellpadding="0" cellspacing="0" class="mediaactions" style="margin-top:5px">
    	<tr>
    		<td style="padding-right: 20px; padding-top:5px; padding-left:10px;">
          <a type="button_count" href="javascript:void(0);" onclick="javascript: openPopupWindow( 'http://www.facebook.com/sharer/sharer.php?u=<? echo urlencode("http://" . SERVER_HOST . "/pages" ); ?>', 'Share', 550, 320 );">
            <img src="/images/facebook_share.png" width="58" height="20" alt="" />
          </a>
        </td>
    		<td>
          <a href="http://twitter.com/share" class="twitter-share-button" data-url="http://shb.me/4" data-text="SaltHub's page directory is for professionals in the maritime & water related organizations. Find & list your page." data-count="horizontal" data-via="<?=SITE_VIA?>">Tweet</a>
        </td>
    	</tr>
    </table>
<? } ?>
		<div class="subhead">
			Active Pages
		</div>
		<?php
/*
    $x = sql_query("
			SELECT gid,gname,pid,hash, COUNT(m.uid) AS friends
			FROM page_members m
			NATURAL JOIN pages
			JOIN photos ON id=pid
			WHERE m.uid IN (
			SELECT IF(id1={$API->uid},id2,id1)
			FROM friends
			WHERE
			pages.privacy=" . PRIVACY_EVERYONE . " and pages.active=1 and
			{$API->uid} IN (id1,id2) AND STATUS=1) AND gid NOT IN (
			SELECT gid
			FROM page_members
			WHERE uid={$API->uid})
			GROUP BY gid order by rand()
		") or die(mysql_error());

		while ($page = mysql_fetch_array($x, MYSQL_ASSOC))
			showPageSmallPreview($page);
*/


/*
    $x = sql_query("
SELECT PAGE_DATA.gid, PAGE_DATA.pid, PAGE_DATA.gname, PHOTOS.hash from
    photos as PHOTOS
INNER JOIN
(
    SELECT distinct
        PAGES.gid, PAGES.pid, PAGES.gname
    FROM
        pages as PAGES
    INNER JOIN
    (
        select
            gid
        from
            feed
        where
            gid != 0
            and ts > DATE_SUB(NOW(), INTERVAL 20 DAY)
            and gid not in (select gid from page_members where uid = " . $API->uid . ")
    ) FEED on FEED.gid = PAGES.gid
    where PAGES.privacy=" . PRIVACY_EVERYONE . "
    GROUP BY gid
    order by rand()
    limit 20
) PAGE_DATA on PAGE_DATA.pid=PHOTOS.id
		") or die(mysql_error());
*/

    $x = sql_query("
SELECT SEARCH_RESULTS.gid, pages.pid, pages.gname from
(
 (
        select distinct
            gid
        from
            feed
        where LENGTH(type)>0 AND ts > DATE_SUB(NOW(), INTERVAL 2 DAY) AND
            gid IN (SELECT gid FROM `pages` WHERE pages.type!=1389) limit 50
 )

) SEARCH_RESULTS, pages WHERE pages.gid=SEARCH_RESULTS.gid
    AND pages.privacy=" . PRIVACY_EVERYONE . " order by rand() limit 20 " ) or die(mysql_error());
/*


            and feed.type != 'VP1'
            and feed.type != 'VP2'
            and feed.type != 'VP3'
*/
		while ($page = mysql_fetch_array($x, MYSQL_ASSOC))
			showPageSmallPreview($page);

		?>
	</div>
	<div class="topborder">
		<div class="biggertext title" style="margin-top:0px; padding-top:0px; padding-bottom:4px;">
			Pages
		</div>
	</div>
	<div class="right">
		<!--<form action="" onsubmit="return false;"><div style="white-space: nowrap;"><input type="text" style="width: 251px;" id="txtsearch1" onkeypress="javascript:searchKeypress(event);" onfocus="javascript:if (this.value=='Search ...') this.value='';" onblur="javascript:if (this.value=='') this.value='Search ...';" class="txtsearch" value="Search ..." /><input type="text" onclick="javascript:searchDo();" onfocus="document.getElementById('txtsearch1').focus();" name="q" class="btnsearch" value="" /></div></form>-->
		<?php showCreatePageWide();
    if( $API->adv ) {
    ?>

		<div class="subhead parent_create_ad" style="margin-bottom: 10px;">
            <div style="float:left;">Sponsors</div>
            <div class="create_ad link" style="float:right;">
                <a href="http://www.salthub.com/adv/create.php" style="font-size:8pt; font-weight:300; color:rgb(0, 64, 128);">create an ad</a>&nbsp;
            </div>
            <div style="clear:both;"></div>
        </div>

		<?php showAd("companion"); } ?>
		<?php include "../inc/connect.php";

    if( $API->adv ) {
    ?>
		<div class="subhead parent_create_ad" style="margin-bottom: 10px;"><div style="float:left;">Sponsors</div><div class="create_ad link" style="float:right;"><a href="http://www.salthub.com/adv/create.php" style="font-size:8pt; font-weight:300; color:rgb(0, 64, 128);">create an ad</a>&nbsp;</div><div style="clear:both;"></div></div>
		<?php showAd("companion"); } ?>
		<?php include "../inc/pymk.php"; ?>
	</div>
	<div class="links">
		<div class="toplink"><img src="/images/find.png" /><a href="#">Browse Pages</a></div>
		<div class="divider">&nbsp;</div>
		<div class="toplink"><img src="/images/page_add.png" /><a href="page_create.php">Create a Page</a></div>
		<div class="toplink"><img src="/images/page_go.png" /><a href="#imin">Pages I'm In</a></div>
		<div class="toplink"><img src="/images/page.png" /><a href="#mypages">My Pages</a></div>
	</div>
	<div class="middle">
		<div class="subhead">Pages You Might Like</div>
		<div id="pages-r">
			<?php
			$limit = 5;
			$o = "r";
			include "previews.php";
			?>
		</div>
		<div style="clear: both; padding: 5px; text-align: center; font-size: 8pt;">
			<a href="javascript:void(0);" onclick="javascript:showMorePages('r');"><img src="/images/arrow_rotate_anticlockwise.png" style="vertical-align: text-bottom;" alt="" />&nbsp; Shuffle pages</a>
		</div>

    <a name="imin"></a>
		<div class="subhead" name"imin">Pages I'm In</div>
		<div id="pages-m">
			<?php
			$o = "m";
			include "previews.php";
			$pagesShown[$o] = count($grps);
			?>
		</div>
		<div style="clear: both; height: 6px;"></div>
		<?php if (count($grps) == 5) { ?>
		<div class="showmore" id="showmore-m" onclick="javascript:showMorePages('m');">show more</div>
		<?php } ?>

    <a name="mypages"></a>
		<div class="subhead">My Pages</div>
		<div id="pages-a">
			<?php
			$o = "a";
			include "previews.php";
			$pagesShown[$o] = count($grps);
			?>
		</div>
		<div style="clear: both; height: 6px;"></div>
		<?php if (count($grps) == 5) { ?>
		<div class="showmore" id="showmore-a" onclick="javascript:showMorePages('a');">show more</div>
		<?php } ?>
		
	</div>
	<div style="clear: both;"></div>
</div>

<script language="javascript" type="text/javascript">
<!--
var pagesShown = <?=json_encode($pagesShown)?>;

<? if( isset( $_GET['private'] ) ) { ?>
showPopUp( 'Sorry!', '<div style="width:350px; padding:20px; text-align:center;">The page that you tried to access is only available to members.</div>' );
<? } ?>
//-->
</script>

<?php

include "../footer.php";

?>