<?php
/*
Called in response to a user joining a page.  Sends notifications of new users, adds the user to the list of members,
refreshes the left side menu of the user's browser.
*/

include "../inc/inc.php";

$gid = intval($_POST['gid']);

if (quickQuery("select count(*) from page_blocked where gid=$gid and uid={$API->uid}") == 1)
	echo 'joinHTML = "join blocked"; alert("You have been banned from this page.");';
else if (quickQuery("select count(*) from page_members where gid=$gid and uid={$API->uid}") == 0 )
{
  $API->sendNotificationToPage( NOTIFY_GROUP_NEW_CONNECT, array( "from" => $API->uid, "gid" => $gid ) );

	sql_query("insert into page_members (uid,gid) values ({$API->uid},$gid)");
	$API->feedAdd("G", $gid, null, null, $gid);
}
else
  sql_query( "update page_members set visited=1 where gid='$gid' and uid={$API->uid} limit 1" );


if ($_POST['hash'] == "side") // joined page from left side - show links
{
	echo 'document.getElementById("joinedlinks").style.display = "";';
	echo 'e = document.getElementById("mustjoin"); if (e) e.style.display = "none";';
  echo 'pageMember = true;'; 
}

echo 'if (typeof joinHTML == "undefined")
	joinHTML = "joined";

if( document.getElementById("join-' . $_POST['hash'] . '") )
  document.getElementById("join-' . $_POST['hash'] . '").innerHTML = joinHTML;
delete joinHTML;
refreshSide(' . $_POST['gid'] . ');
';
?>