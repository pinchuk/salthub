<?
/*
Popup displayed to the user when attempting to join a page.  Presents various options to the user, depending on
the page type and category selections (e.g., "I'm a fan" - for all pages, "I worked here" for businesses).
*/

require_once(  $_SERVER["DOCUMENT_ROOT"] . "/inc/inc.php" );

if( empty( $gid ) )
{
  $gid = intval( $_GET['gid'] );
}

$popup = empty( $gid );

$page = $API->getPageInfo( $gid );

?>

<div>
  <div style="font-size:<? if( $popup ) echo "16"; else echo "10"; ?>pt; font-weight:bold;">Tell us about your relationship to this page</div>
  <div style="font-size:11px; margin-bottom:6px;">(select the one below that best fits and complete)</div>

    <div style="float:left; font-weight:bold; font-size:13px;" id="jt0"><a href="javascript:void(0);" onclick="javascript:joinPageChangeTab(0);">I'm a fan</a></div>

<? if( $page['gtype'] == 'S' || $page['gtype'] == 'C' || $page['gtype']=='B') { ?>
    <div style="float:left; margin-left:25px; font-size:13px;" id="jt1"><a href="javascript:void(0);" onclick="javascript:joinPageChangeTab(1);">I work(ed) here</a></div>
<? } if( $page['gtype'] == 'S' ) { ?>
    <div style="float:left; margin-left:25px; font-size:13px;" id="jt2"><a href="javascript:void(0);" onclick="javascript:joinPageChangeTab(2);">I attend(ed) school here</a></div>
<? } if( $page['gtype'] == 'O' ) { ?>
    <div style="float:left; margin-left:25px; font-size:13px;" id="jt3"><a href="javascript:void(0);" onclick="javascript:joinPageChangeTab(3);">I work(ed) in this profession</a></div>
<? } if( $page['gtype'] == 'L' ) { ?>
    <div style="float:left; margin-left:25px; font-size:13px;" id="jt4"><a href="javascript:void(0);" onclick="javascript:joinPageChangeTab(4);">I have this license/degree</a></div>
<? } ?>

   <div style="clear:both; height:5px;"></div>
    <div id="jg0" style="width:100%; <? if( $popup ) { ?>height:130px;<? } ?>">
<!--      <div style="float:left; padding-top:4px; font-size:12px;">yes </div>-->
      <div style="float:left; padding-top:1px;"><input type="checkbox" name="junk" CHECKED></div>
      <div style="float:left"><span style="font-size:11px;">Yes connect with this page</span></div>
    </div>

    <div id="jg1" style="display:none; width:100%; <? if( $popup ) { ?>height:130px;<? } ?>">

    <div style="float: left; font-size: 9pt;">
      <div class="smtitle2">Period:</div>
      <select id="month-0-X" style="margin-right: 5px;"><option value=""></option><option  value="1">January</option><option  value="2">February</option><option  value="3">March</option><option  value="4">April</option><option  value="5">May</option><option  value="6">June</option><option  value="7">July</option><option  value="8">August</option><option  value="9">September</option><option  value="10">October</option><option  value="11">November</option><option  value="12">December</option></select><select id="year-0-X"><option value=""></option><option  value="1970">1970</option><option  value="1971">1971</option><option  value="1972">1972</option><option  value="1973">1973</option><option  value="1974">1974</option><option  value="1975">1975</option><option  value="1976">1976</option><option  value="1977">1977</option><option  value="1978">1978</option><option  value="1979">1979</option><option  value="1980">1980</option><option  value="1981">1981</option><option  value="1982">1982</option><option  value="1983">1983</option><option  value="1984">1984</option><option  value="1985">1985</option><option  value="1986">1986</option><option  value="1987">1987</option><option  value="1988">1988</option><option  value="1989">1989</option><option  value="1990">1990</option><option  value="1991">1991</option><option  value="1992">1992</option><option  value="1993">1993</option><option  value="1994">1994</option><option  value="1995">1995</option><option  value="1996">1996</option><option  value="1997">1997</option><option  value="1998">1998</option><option  value="1999">1999</option><option  value="2000">2000</option><option  value="2001">2001</option><option  value="2002">2002</option><option  value="2003">2003</option><option  value="2004">2004</option><option  value="2005">2005</option><option  value="2006">2006</option><option  value="2007">2007</option><option  value="2008">2008</option><option  value="2009">2009</option><option  value="2010">2010</option><option  value="2011">2011</option><option  value="2012">2012</option></select>
      <div style="padding: 5px; text-align: center;" id="enddate-X2">to</div>
      <div id="enddate-X" style="padding-bottom: 5px;"><select id="month-1-X" style="margin-right: 5px;"><option value=""></option><option  value="1">January</option><option  value="2">February</option><option  value="3">March</option><option  value="4">April</option><option  value="5">May</option><option  value="6">June</option><option  value="7">July</option><option  value="8">August</option><option  value="9">September</option><option  value="10">October</option><option  value="11">November</option><option  value="12">December</option></select><select id="year-1-X"><option value=""></option><option  value="1970">1970</option><option  value="1971">1971</option><option  value="1972">1972</option><option  value="1973">1973</option><option  value="1974">1974</option><option  value="1975">1975</option><option  value="1976">1976</option><option  value="1977">1977</option><option  value="1978">1978</option><option  value="1979">1979</option><option  value="1980">1980</option><option  value="1981">1981</option><option  value="1982">1982</option><option  value="1983">1983</option><option  value="1984">1984</option><option  value="1985">1985</option><option  value="1986">1986</option><option  value="1987">1987</option><option  value="1988">1988</option><option  value="1989">1989</option><option  value="1990">1990</option><option  value="1991">1991</option><option  value="1992">1992</option><option  value="1993">1993</option><option  value="1994">1994</option><option  value="1995">1995</option><option  value="1996">1996</option><option  value="1997">1997</option><option  value="1998">1998</option><option  value="1999">1999</option><option  value="2000">2000</option><option  value="2001">2001</option><option  value="2002">2002</option><option  value="2003">2003</option><option  value="2004">2004</option><option  value="2005">2005</option><option  value="2006">2006</option><option  value="2007">2007</option><option  value="2008">2008</option><option  value="2009">2009</option><option  value="2010">2010</option><option  value="2011">2011</option><option  value="2012">2012</option></select></div>
  	 </div>

	  	 <div style="float: left; margin-left:20px; font-size:9pt;">
			  <div class="smtitle2" style="margin-bottom:20px;">Position:<br /><input type="text" style="width: 181px;" id="occupation" value="" /></div>
	  		<div style="clear:both; float:left;"><input type="checkbox"  id="present-X" onchange="javascript:document.getElementById('enddate-X').style.display = this.checked ? 'none' : ''; javascript:document.getElementById('enddate-X2').style.display = this.checked ? 'none' : '';" /></div>
       <div style="float:left; padding-top:3px;"> I currently work here</div>
		   </div>
   </div>

    <div id="jg2" style="display:none; width:100%; <? if( $popup ) { ?>height:130px;<? } ?> font-size: 9pt;">

      <div class="smtitle2">Period:</div>
        <div style="float:left; margin-right:5px;"><select id="year-0-Y"><option value=""></option><option  value="1970">1970</option><option  value="1971">1971</option><option  value="1972">1972</option><option  value="1973">1973</option><option  value="1974">1974</option><option  value="1975">1975</option><option  value="1976">1976</option><option  value="1977">1977</option><option  value="1978">1978</option><option  value="1979">1979</option><option  value="1980">1980</option><option  value="1981">1981</option><option  value="1982">1982</option><option  value="1983">1983</option><option  value="1984">1984</option><option  value="1985">1985</option><option  value="1986">1986</option><option  value="1987">1987</option><option  value="1988">1988</option><option  value="1989">1989</option><option  value="1990">1990</option><option  value="1991">1991</option><option  value="1992">1992</option><option  value="1993">1993</option><option  value="1994">1994</option><option  value="1995">1995</option><option  value="1996">1996</option><option  value="1997">1997</option><option  value="1998">1998</option><option  value="1999">1999</option><option  value="2000">2000</option><option  value="2001">2001</option><option  value="2002">2002</option><option  value="2003">2003</option><option  value="2004">2004</option><option  value="2005">2005</option><option  value="2006">2006</option><option  value="2007">2007</option><option  value="2008">2008</option><option  value="2009">2009</option><option  value="2010">2010</option><option  value="2011">2011</option><option  value="2012">2012</option></select></div>
        <div id="enddate-Y" style="float:left; margin-left:5px;";>to <select id="year-1-Y"><option value=""></option><option  value="1970">1970</option><option  value="1971">1971</option><option  value="1972">1972</option><option  value="1973">1973</option><option  value="1974">1974</option><option  value="1975">1975</option><option  value="1976">1976</option><option  value="1977">1977</option><option  value="1978">1978</option><option  value="1979">1979</option><option  value="1980">1980</option><option  value="1981">1981</option><option  value="1982">1982</option><option  value="1983">1983</option><option  value="1984">1984</option><option  value="1985">1985</option><option  value="1986">1986</option><option  value="1987">1987</option><option  value="1988">1988</option><option  value="1989">1989</option><option  value="1990">1990</option><option  value="1991">1991</option><option  value="1992">1992</option><option  value="1993">1993</option><option  value="1994">1994</option><option  value="1995">1995</option><option  value="1996">1996</option><option  value="1997">1997</option><option  value="1998">1998</option><option  value="1999">1999</option><option  value="2000">2000</option><option  value="2001">2001</option><option  value="2002">2002</option><option  value="2003">2003</option><option  value="2004">2004</option><option  value="2005">2005</option><option  value="2006">2006</option><option  value="2007">2007</option><option  value="2008">2008</option><option  value="2009">2009</option><option  value="2010">2010</option><option  value="2011">2011</option><option  value="2012">2012</option></select></div>
	  		 <div style="clear:both; float:left;"><input type="checkbox" id="present-Y" onchange="javascript:document.getElementById('enddate-Y').style.display = this.checked ? 'none' : '';" /></div>
        <div style="float:left;  padding-top:3px;"> I currently attend</div>
    </div>


    <div id="jg3" style="display:none; width:100%; <? if( $popup ) { ?>height:130px;<? } ?> font-size: 9pt;">

    <div style="float: left; font-size: 9pt;">
      <div class="smtitle2">Period:</div>
      <select id="month-0-Z" style="margin-right: 5px;"><option value=""></option><option  value="1">January</option><option  value="2">February</option><option  value="3">March</option><option  value="4">April</option><option  value="5">May</option><option  value="6">June</option><option  value="7">July</option><option  value="8">August</option><option  value="9">September</option><option  value="10">October</option><option  value="11">November</option><option  value="12">December</option></select><select id="year-0-Z"><option value=""></option><option  value="1970">1970</option><option  value="1971">1971</option><option  value="1972">1972</option><option  value="1973">1973</option><option  value="1974">1974</option><option  value="1975">1975</option><option  value="1976">1976</option><option  value="1977">1977</option><option  value="1978">1978</option><option  value="1979">1979</option><option  value="1980">1980</option><option  value="1981">1981</option><option  value="1982">1982</option><option  value="1983">1983</option><option  value="1984">1984</option><option  value="1985">1985</option><option  value="1986">1986</option><option  value="1987">1987</option><option  value="1988">1988</option><option  value="1989">1989</option><option  value="1990">1990</option><option  value="1991">1991</option><option  value="1992">1992</option><option  value="1993">1993</option><option  value="1994">1994</option><option  value="1995">1995</option><option  value="1996">1996</option><option  value="1997">1997</option><option  value="1998">1998</option><option  value="1999">1999</option><option  value="2000">2000</option><option  value="2001">2001</option><option  value="2002">2002</option><option  value="2003">2003</option><option  value="2004">2004</option><option  value="2005">2005</option><option  value="2006">2006</option><option  value="2007">2007</option><option  value="2008">2008</option><option  value="2009">2009</option><option  value="2010">2010</option><option  value="2011">2011</option><option  value="2012">2012</option></select>
      <div style="padding: 5px; text-align: center;" id="enddate-Z2">to</div>
      <div id="enddate-Z" style="padding-bottom: 5px;"><select id="month-1-Z" style="margin-right: 5px;"><option value=""></option><option  value="1">January</option><option  value="2">February</option><option  value="3">March</option><option  value="4">April</option><option  value="5">May</option><option  value="6">June</option><option  value="7">July</option><option  value="8">August</option><option  value="9">September</option><option  value="10">October</option><option  value="11">November</option><option  value="12">December</option></select><select id="year-1-Z"><option value=""></option><option  value="1970">1970</option><option  value="1971">1971</option><option  value="1972">1972</option><option  value="1973">1973</option><option  value="1974">1974</option><option  value="1975">1975</option><option  value="1976">1976</option><option  value="1977">1977</option><option  value="1978">1978</option><option  value="1979">1979</option><option  value="1980">1980</option><option  value="1981">1981</option><option  value="1982">1982</option><option  value="1983">1983</option><option  value="1984">1984</option><option  value="1985">1985</option><option  value="1986">1986</option><option  value="1987">1987</option><option  value="1988">1988</option><option  value="1989">1989</option><option  value="1990">1990</option><option  value="1991">1991</option><option  value="1992">1992</option><option  value="1993">1993</option><option  value="1994">1994</option><option  value="1995">1995</option><option  value="1996">1996</option><option  value="1997">1997</option><option  value="1998">1998</option><option  value="1999">1999</option><option  value="2000">2000</option><option  value="2001">2001</option><option  value="2002">2002</option><option  value="2003">2003</option><option  value="2004">2004</option><option  value="2005">2005</option><option  value="2006">2006</option><option  value="2007">2007</option><option  value="2008">2008</option><option  value="2009">2009</option><option  value="2010">2010</option><option  value="2011">2011</option><option  value="2012">2012</option></select></div>
  	 </div>

	  	 <div style="float: left; margin-left:20px; font-size:9pt;">
			  <div class="smtitle2" style="margin-bottom:20px;">Employer:<br /><input type="text" style="width: 181px;" id="employer" value="" /></div>
	  		<div style="clear:both; float:left;"><input type="checkbox"  id="present-Z" onchange="javascript:document.getElementById('enddate-Z').style.display = this.checked ? 'none' : '' javascript:document.getElementById(\'enddate-Z2\').style.display = this.checked ? 'none' : ''" /></div>
       <div style="float:left; padding-top:3px;"> I currently work here</div>
		   </div>

		   </div>

    <div id="jg4" style="display:none; width:100%; <? if( $popup ) { ?>height:130px;<? } ?> font-size: 9pt;">
      <div style="float:left; padding-top:4px; font-size:12px;">yes </div>
      <div style="float:left; padding-top:1px;"><input type="checkbox" name="junk" CHECKED></div>
      <div style="float:left; padding-top:4px; font-size:10px;">I have this license/degree </div>
		 </div>

    <div style=" width:100%; text-align:center; clear:both;">
      <input type="button" class="button" name="" value="save &amp; connect" onclick="javascript:joinPage2( <?= $gid ?>, 'side' <? if( isset( $joinhandler ) ) { ?>, '<?=$joinhandler?>'<? } ?>);"/>
    </div>

</div>