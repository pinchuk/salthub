<?php header('Content-type: text/javascript'); ?>
// Code used for selecting media to be added/removed from the page.

var media = null;
var mediaChooserQ = null;
var mediaAdded = false;

function showMediaChooser(gid, q)
{
	if (typeof q == "undefined")
		mediaChooserQ = "*";
	else
		mediaChooserQ = q;

	e = document.getElementById("mediaChooser");

	if (e)
		showMediaChooserHandler("");
	else
	{
    mediaAdded = false;
		showPopUp('', '<div id="mediaChooser"><div class="loginmessage"><img src="/images/barwait.gif" alt="" /></div>', [515, 487], 'no', false, onShowMediaChooserClose );
		getAjax("/pages/getmediachooser.php?gid=" + gid, "showMediaChooserHandler");
	}
}

function showMediaChooserHandler( response )
{
  data = "";

  if( response != "" )
  {
    chunks = response.split(String.fromCharCode(1));
    data = chunks[0];
    media = chunks[1].split(",");
  }

  e = document.getElementById("mediaChooser");

	if (e)
	{

		if (data != "")
			e.innerHTML = data;

		fs = ['P', 'V'];

		for (var i in fs)
			document.getElementById("mediaChooser-" + fs[i]).style.display = (mediaChooserQ == fs[i] || mediaChooserQ == "*") ? "" : "none";
	}
}

function onShowMediaChooserClose()
{
  if( !mediaAdded ) return;

  if( window.location.href.substring( window.location.href.length - 7 ) != "logbook" )
  {
    window.location.href = "/page/" + page.gid + "-_r/logbook";
  }
  else
  {
    showNewLogEntries();
  }

}

var mediaType = null;
var mediaID = null;

function selectMedia( id, type, gid, remove )
{
  mediaID = id;
  mediaType = type;

  if( remove ) remove = '1';
  else
  {
    remove = '0';
    mediaAdded = true;
  }
  postAjax("/pages/addpagemedia.php", "gid=" + gid + "&id=" + id + "&type=" + type + "&r=" + remove, "selectMediaHandler" );
}

function selectMediaHandler( data )
{
//  e = document.getElementById( "media-selected-" + mediaType + mediaID );
//  if( e )
//    e.disabled = true;
//    e.style.display = 'none';

  //Update Page Log
  if (typeof showNewLogEntries == "function")
  {
  	showNewLogEntries();
  }
}

function mediaSearch( value )
{
  if( !media ) return;

  value = value.toUpperCase();


  for( i = 0; i < media.length; i++ )
  {
    parent = document.getElementById( "media-" + media[i] );
    searchData = document.getElementById( "media-search-" + media[i] );

    if( parent && searchData )
    {
      var term = searchData.innerHTML.toUpperCase();
      if( value == null || term.indexOf( value ) >= 0 )
        parent.style.display='';
      else
        parent.style.display='none';
    }
  }
}

