<?php
/*
Implements member changes by the page admin, called from members.php.
*/

include "../inc/inc.php";

$uid = intval($_POST['uid']);
$page = $API->getPageInfo(intval($_POST['gid']));
$isCreator = $page['uid'] == $API->uid || $API->admin;
$isAdmin = quickQuery("select admin from page_members where uid={$API->uid} and gid={$page['gid']}") == 1 || $API->admin;

if (!$isAdmin)
	die("alert('You are not an admin of this page.');");

if ($_POST['action'] == "remove")
{
	sql_query("delete from page_members where gid={$page['gid']} and uid=$uid" . ($isCreator ? "" : " and admin=0"));
	echo 'document.getElementById("gmlinks-' . $uid . '").innerHTML = "removed from page";';
}
elseif ($_POST['action'] == "block")
{
	if ($isCreator)
		$admin = 0; //creator can block admins
	else
		$admin = quickQuery("select admin from page_members where gid={$page['gid']} and uid=$uid");
	
	if ($admin == 1)
		die("alert('You are not the owner of this page.');");
	
	sql_query("delete from page_members where gid={$page['gid']} and uid=$uid");
	
	if (mysql_affected_rows() == 1)
	{
		sql_query("insert into page_blocked (gid,uid) values ({$page['gid']}, $uid)");		
		echo 'document.getElementById("gmlinks-' . $uid . '").innerHTML = "blocked from page";';
	}
}
elseif (!$isCreator)
	die("alert('You are not the owner of this page.');");
elseif ($_POST['action'] == "demote" || $_POST['action'] == "promote")
{
	sql_query("update page_members set admin=" . ($_POST['action'] == "demote" ? 0 : 1) . " where uid=$uid and gid={$page['gid']}");
	ob_start();
	?><a href="javascript:void(0);" onclick="javascript:modifyPageMember(<?=$page['gid']?>, <?=$uid?>, '<?=$_POST['action'] != "demote" ? "de" : "pro"?>mote');"><?=$_POST['action'] != "demote" ? "remove as" : "make"?> admin</a><?php
	$html = ob_get_contents();
	ob_end_clean();
	echo 'document.getElementById("gmadminlink-' . $uid . '").innerHTML = "' . addslashes(str_replace("\n", "", $html)) . '";';


  if( $_POST['action'] == "promote" )
  {
    $API->sendNotification( NOTIFY_GROUP_ADMIN, array( "uid" => $uid, "from" => $API->uid, "gid" => $page['gid'] ) );
  }
}

?>