<?php
/*
Contains the javascript used for the custom drop down control that enables the user to select
multiple items.  This particular code is used for selecting multiple categories and sectors that
a page is in.  This is currently only used by businesses.
*/

header("Content-type: text/javascript");
include "../inc/inc.php";

$load_page = "/pages/multi_select_load.js.php";
$save_page = "/pages/multi_select_save.php";

$gid = $_GET['gid'];
$cat_funded     = intval( quickQuery( "select categories_funded from pages where gid='" . $gid . "'" ) );
$sectors_funded = intval( quickQuery( "select sectors_funded from pages where gid='" . $gid . "'" ) );
?>

var updating;
var itemsChosen = Array();
var comboListBoxIndex = 0;
var tidSelLimits = Array( <?=$cat_funded + 1?>, <?=$sectors_funded + 1?> );

function newComboListBox(index, targetid ) //, title )
{
  tidEditing = index;
  width = 160;
  if( index > 2 )
    width = 190;

	html  = '<div style="width:' + width + 'px; padding-right: 10px; float: left;">';
	html += '	<div style="width: ' + width + 'px; float:left;">';
//	html += '	  <div class="smtitle2" style="padding-bottom: 3px;">' + title + '</div>';
	html += '	  <div style="width: ' + width + 'px; height: 18px; border: 1px solid #555; position: relative; float:left;">';
	html += '		  <input type="text" onkeyup="javascript:searchItems(' + tidEditing + ' );" id="itemsearch-' + tidEditing + '" style="margin: 0; padding: 0px 2px; border: 0; width: ' + (width - 21) + 'px; height: 18px;"><img src="/images/dropdown.png" onclick="javascript:toggleItemChooser(' + tidEditing + ');" style="cursor: pointer; vertical-align: top;" alt="" />';

if( index == 1 || index == 3 )
{
	html += '		  <div id="itemchooser-' + tidEditing + '" class="itemchooser" style="width:233px; height:402px;">';
	html += '			  <div id="chooser-' + tidEditing + '" class="chooser" style="width:227px; height:352px;"></div>';
}
else
{
	html += '		  <div id="itemchooser-' + tidEditing + '" class="itemchooser" style="width:233px;">';
	html += '			  <div id="chooser-' + tidEditing + '" class="chooser" style="width:227px;"></div>';
}
	html += '			  <div style="padding: 5px; text-align: center; background: #fff; border-top: 1px solid #555;">';
	html += '				  <input type="button" class="button" style="width:75px; margin:4px; height:30px; color:rgb(50, 103, 152);" value="Apply" onclick="javascript:saveItems(' + tidEditing + '); toggleItemChooser(' + tidEditing + ');" /> <input style="width:75px; margin:4px; height:30px; color:rgb(50, 103, 152);" type="button" class="button" value="Cancel" onclick="javascript:toggleItemChooser(' + tidEditing + '); loadItems(0,' + tidEditing + ' );" />';
	html += '			  </div>';
	html += '		  </div>';
	html += '	  </div>';
if( index <= 2 ) {
	html += '	  <div style="clear:both; padding-top: 5px; font-size: 8pt; color:rgb(50, 103, 152);">Your Selections</div>';
	html += '	  <div id="selections-' + tidEditing + '" style="font-size:8pt;"></div>';
}
	html += '	</div>';
  html += ' <div style="margin-bottom:10px;"></div>';
	html += '</div>';
	html += '<div style="clear: both;"></div>';

	e = document.getElementById(targetid);
	e.innerHTML = html;
	e.style.display = "inherit";

	loadItems(0, tidEditing);
}

function loadItems(i, tidEditing)
{
	loadjscssfile("<?=$load_page?>?i=" + i + "&tid=" + tidEditing + "&hash=" + hash, "js");
}

function refreshSelections( tidEditing )
{
  html = '';

  e = document.getElementById( "selections-" + tidEditing );
  if( e )
  {
  	for (var i in itemsChosen[tidEditing])
  		if (itemsChosen[tidEditing][i] != null)
	  		if (itemsChosen[tidEditing][i][1] != null)
      	{
          html += itemsChosen[tidEditing][i][1] + "<br />";
        }

    if( html == '' )
      html = 'use dropdown above to select';
    e.innerHTML = html;
  }
}

function saveItemsHandler( data ) { //alert( data )
 }
function saveItems( tidEditing )
{
	pids = "";

	for (var i in itemsChosen[tidEditing])
	{
		if (itemsChosen[tidEditing][i] != null)
			if (typeof itemsChosen[tidEditing][i][0] != "undefined")
				pids += "," + itemsChosen[tidEditing][i][0];
	}

  data = "tid=" + tidEditing + "&items=" + pids.substring(1);

	postAjax("<?=$save_page?>", data, "saveItemsHandler" );
}

function toggleItemChooser(tidEditing)
{
	e = document.getElementById("itemchooser-" + tidEditing);
	isShown = e.style.display == "inline";

	if (!isShown) // load item choices
	{
		document.getElementById("chooser-" + tidEditing).innerHTML = '&nbsp;';
		loadItems(1, tidEditing );
	}

	e.style.display = isShown ? "none" : "inline";
}

function searchItems(tidEditing)
{
  e = document.getElementById("itemsearch-" + tidEditing);

	q = e.value.toLowerCase();

	if (document.getElementById("itemchooser-" + tidEditing).style.display != "inline")
		toggleItemChooser(tidEditing);

	i = 0;
	
	while (e = document.getElementById("nameitem-" + i))
		document.getElementById("item-" + i++).style.display = e.innerHTML.toLowerCase().indexOf(q) == -1 ? "none" : "";
}

function itemChosen(i, pid, chosen, tidEditing)
{
  j = i;
	if (chosen)
	{
    count = 0;
    alreadyChosen = false;
    for (var i in itemsChosen[tidEditing])
    {
    	if (itemsChosen[tidEditing][i] != null)
      {
        if (itemsChosen[tidEditing][i][0] == pid)
          alreadyChosen = true;
        count++;
      }
    }

    if( !alreadyChosen ) count++;

    if( count > tidSelLimits[tidEditing-1] )
    {
      if( overSelectionLimit( tidEditing ) )
      {
        e = document.getElementById( 'cb-' + tidEditing + '-' + j );
        if( e ) e.checked = false;

        return;
      }
    }

		name = document.getElementById("nameitem-" + tidEditing + "-" + j).innerHTML;
		itemsChosen[tidEditing][itemsChosen[tidEditing].length] = [pid, name];
	}
	else
	{
		for (var i in itemsChosen[tidEditing])
		{
			if (itemsChosen[tidEditing][i] != null)
				if (itemsChosen[tidEditing][i][0] == pid)
				{
					itemsChosen[tidEditing][i] = null;
					break;
				}
		}
	}

  refreshSelections( tidEditing );
}

function overSelectionLimit( tid )
{
//  showPopupUrl('/pages/purchase_services_popup.php?gid=<?=$gid?>&ol=1' );
  return false;
}

function getSelectionCount( tid )
{
  count = 0;
  for (var i in itemsChosen[tidEditing])
  	if (itemsChosen[tidEditing][i] != null)
      count++;
  return count;
}
