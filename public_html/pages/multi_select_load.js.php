<?php
/*
Loads the category selections that are shown in the custom drop-down control on a business about page.
*/

include "../inc/inc.php";
header("Content-type: text/javascript");

$tid = intval($_GET['tid']);
$gid = $_SESSION['gid'];

$itemsChosen = array();
?>

var tidItem = <?=$tid?>;

html  = '';
<?php

$sel_items = array();


switch( $tid )
{
  case 1:
  case 3:
    $sql = "select cat as type_id, catname as name, heading from categories where industry IN ( SELECT cat from categories where industry='" . PAGE_TYPE_BUSINESS . "') order by heading, catname";
  break;

  case 2:
  case 4:
  case 5:
    $sql = "select cat as type_id, catname as name, 0 as heading from categories where industry=" . PAGE_TYPE_BUSINESS . " order by catname";
  break;
}


$x = sql_query( $sql );
echo mysql_error();


switch( $tid )
{
  case 1:
  {
    if( isset( $_SESSION['page_categories'] ) )
      $sel_items = $_SESSION['page_categories'];
    else
    {
      $sel_q = sql_query( "select cat as type_id from page_categories where gid='" . $gid . "'" );
      while( $sel_r = mysql_fetch_array( $sel_q ) )
        $sel_items[] = $sel_r['type_id'];
    }
  }
  break;

  case 2:
  {
    if( isset( $_SESSION['page_sectors'] ) )
      $sel_items = $_SESSION['page_sectors'];
    else
    {
      $sel_q = sql_query( "select cat as type_id from page_categories where gid='" . $gid . "'" );
      while( $sel_r = mysql_fetch_array( $sel_q ) )
        $sel_items[] = $sel_r['type_id'];
    }
  }
  break;

  case 3:
  {
    $sel_items = $_SESSION['page_create_categories'];
  }
  break;

  case 5:
  case 4:
  {
    $sel_items = $_SESSION['page_create_sectors'];
  }
  break;
}


$i = 0;

$currentHeader = 0;

while ($item = mysql_fetch_array($x, MYSQL_ASSOC))
{
  $has = false;
  if ( in_array($item['type_id'], $sel_items) ) //selected
  {
		$itemsChosen[] = $item;
    $has = true;
  }

  if( $item['heading'] > 0 && ($currentHeader != $item['heading'] ) )
  {
    echo "html += '<div style=\"font-size:9pt; font-weight:bold; margin-top:10px;\">";
    echo quickQuery( "select name from product_and_service_types where ID=" . $item['heading'] );
    echo "</div>';";
    $currentHeader = $item['heading'];
  }

	echo "html += '	<div  style=\"margin-top:5px; margin-bottom:5px; margin-left:20px; line-height:200%;\" class=\"item\" id=\"item-" . $i . "\"><input id=\"cb-$tid-$i\"style=\"width:22px;\" type=\"checkbox\" " . ($has ? "checked" : "") . " onchange=\"javascript:itemChosen(" . $i . ", " . $item['type_id'] . ", this.checked, " . $tid . ");\" /><span id=\"nameitem-" . $tid . "-" . $i++ . "\">" . addslashes($item['name']) . "</span></div>';\n";
}

if ($_GET['i'] == "1") { ?>

document.getElementById("chooser-<? echo $tid ?>").innerHTML = html;
searchItems(<? echo $tid ?>);

<?php } ?>

itemsChosen[<? echo $tid ?>] = [<?php
$i = 0;
foreach ($itemsChosen as $item)
{
	if ($i > 0) echo ", ";
	echo "[" . $item['type_id'] . ", '" . addslashes($item['name']) . "']";
	$i++;
}
?>];

refreshSelections(<? echo $tid ?> );
