<?
/*
Saves business page category selections.  Selections are saved as session variables because
the changes can not be made in the database immediately as they must be paid for first.
This presents a popup to the user which prompts them to add the categories to their
shopping cart.  After purchase, the changes are made in the database.
*/

include "../inc/inc.php";

$gid = $_SESSION['gid'];
$tid = $_POST['tid'];
$items = $_POST['items'];

$items = explode( ",", $items );

switch( $tid )
{
  case 1:
    //Note: 5/4/2012, changing selection scheme to use session variables.  Variables are saved after the user chooses "Save Changes"
    $_SESSION['page_categories'] = Array();

/*
    $sql = "delete from page_categories where cat IN ( SELECT distinct child from category_relationships inner join categories on categories.cat=category_relationships.parent where categories.industry='" . PAGE_TYPE_BUSINESS . "') and gid='$gid'";
    mysql_query( "update pages set subcat='" . $items[0] . "' where gid='$gid' limit 1" );
*/
  break;

  case 2:
    $_SESSION['page_sectors'] = Array();

/*
    $sql = "delete from page_categories where cat IN ( select cat from categories where industry=" . PAGE_TYPE_BUSINESS . ") and gid='$gid'";
    mysql_query( "update pages set cat='" . $items[0] . "' where gid='$gid' limit 1" );
*/
  break;

  case 3:
    $_SESSION['page_create_categories'] = Array();
  break;

  case 5:
  case 4:
    $_SESSION['page_create_sectors'] = Array();
  break;
}

mysql_query( $sql );

foreach( $items as $item )
{
  switch( $tid )
  {
    case 1:
      $_SESSION['page_categories'][] = $item;
    break;

    case 2:
      $_SESSION['page_sectors'][] = $item;
//      mysql_query( "insert into page_categories (gid,cat) values ($gid, $item)" );
    break;

    case 3:
      $_SESSION['page_create_categories'][] = $item;
    break;

    case 5:
    case 4:
      $_SESSION['page_create_sectors'][] = $item;
    break;
  }
}

?>OK