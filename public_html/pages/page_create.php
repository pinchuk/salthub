<?php
/*
Step 1 of a tool for users to create new pages.  Supports all page types.
*/

include "../inc/inc.php";

$_SESSION['page_create_sectors'] = Array();
$_SESSION['page_create_categories'] = Array();

$scripts[] = "/pages/pages.js";
$scripts[] = "/pages/multi_select.js.php";

include "../header.php";
include( "page_create_top_menu.php" ); ?>

<style>

.page-create-left-button
{
  background-color:#f3f8fb;
  border:1px solid #d8dfea;
  width:200px;
  margin-bottom:9px;
  padding-top:6px;
  cursor:pointer;
  font-size: 9pt;
  font-weight:bold;
  padding:6px;
  height:48px;
}

.page-create-left-button-chosen
{
  background-color:#fff9d7;
  border:1px solid #e2c822;
  width:200px;
  margin-bottom:9px;
  padding-top:6px;
  cursor:pointer;
  font-size: 9pt;
  font-weight:bold;
  padding:6px;
  height:48px;
}

.page-create-left-button:hover
{
  background-color:#fff9d7;
  border:1px solid #e2c822;
}

.page-create-content
{
  background-color:#f3f8fb;
  border:1px solid #d8dfea;
  width:490px;
  margin-bottom:9px;
  padding-top:6px;
  font-size:9pt;
}


.dentry input
{
  padding:5px;
  margin-left:0px;
  margin:5px;
  color:#555;
  width:170px;
  height:15px;
}

.dentry select
{
  height:28px;
  margin:5px;
  margin-left:10px;
  padding:5px;
  color:#555;
}

</style>

<? include( "single_select.js.php" ); ?>

<form action="page_create2.php" method="POST" onsubmit="javascript:return submitForm();" id="page_create_form" autocomplete="off">
<input type="hidden" name="sharedata" id="sharedata"/>
<input type="hidden" name="type_selection" id="type_selection"/>
<input type="hidden" name="l1_subcat" id="l1_subcat"/>

<div class="contentborder" style="width:740px; float:left; clear:left; padding-left:15px; margin-top:-22px; padding-top:30px;">
  <div style="clear:both;"></div>
  <div class="strong" style="margin-top:10px; font-size:13pt;">Create a page</div>

  <div style="height:1px; border-bottom:1px solid #d8dfea; width:720px; margin-bottom:5px;"></div>

  <div style="width:235px; float:left;">
    <div style="font-size:10pt; font-weight:bold; margin-bottom:5px;">Select a category type</div>

    <div class="page-create-left-button" id="lb1" onclick="javascript: changeCategory(1, this);" onmouseover="javascript: mouseIn(1);" onmouseout="javascript: mouseOut(1);">
      <div style="float:left; padding-right:10px;"><img src="/images/building_grey.jpg" width="48" height="48" alt="" id="lbi1"/></div>
      <div style="float:left; padding-top:10px; width:120px;">Business or Service</div>
    </div>

    <div class="page-create-left-button" id="lb2" onclick="javascript: changeCategory(2, this);" onmouseover="javascript: mouseIn( 2 );" onmouseout="javascript: mouseOut(2);">
      <div style="float:left; padding-right:10px;"><img src="/images/yacht_gray.png" width="48" height="48" alt="" id="lbi2"/></div>
      <div style="float:left; padding-top:10px; width:120px;">Vessel</div>
    </div>

    <div class="page-create-left-button" id="lb4" onclick="javascript: changeCategory(4, this);" onmouseover="javascript: mouseIn( 4 );" onmouseout="javascript: mouseOut(4);" >
      <div style="float:left; padding-right:10px;"><img src="/images/boarding.png" width="48" height="48" alt="" id="lbi4"/></div>
      <div style="float:left; padding-top:10px; width:120px;">Arts, Entertainment, Activities or Sports</div>
    </div>

<!--
    <div class="page-create-left-button" id="lb3" onclick="javascript: changeCategory(3, this);" onmouseover="javascript: mouseIn( 3);" onmouseout="javascript: mouseOut(3);" >
      <div style="float:left; padding-right:10px;"><img src="/images/teacher.png" width="48" height="48" alt="" id="lbi3"/></div>
      <div style="float:left; padding-top:10px; width:120px;">Professional</div>
    </div>
-->

    <div class="page-create-left-button" id="lb5" onclick="javascript: changeCategory(5, this);" onmouseover="javascript: mouseIn( 5);" onmouseout="javascript: mouseOut(5);" >
      <div style="float:left; padding-right:10px;"><img src="/images/puzzle.png" width="48" height="48" alt="" id="lbi5"/></div>
      <div style="float:left; padding-top:10px; width:120px;">Club / Association</div>
    </div>


    <div style="clear:both;"></div>

  </div>


  <div style="width:490px; float:left;">
    <div style="font-size:10pt; font-weight:bold; margin-bottom:5px;">About your page</div>

    <div class="page-create-content" style="height:370px;">
      <div id="default" style="padding:15px; width:490px;">
        <div style="float:left; margin-right:10px; width:130px;"><img src="/images/page_display.png" width="129" height="95" alt="" /></div>
        <div style="float:left; width:330px; font-size:12pt;">Creating a page has many advantages. It brings you closer to customers, peers and followers. It provides the ability to be heard, discovered and so much more.</div>
        <div style="clear:both; height:20px; margin-bottom:30px;"></div>

        <div style="float:left; padding:5px; width:30px;"><img src="/images/group_3.png" width="32" height="21" alt="" /></div>
        <div style="float:left; padding:5px; width:30px;"><img src="/images/chat-2.png" width="24" height="22" alt="" /></div>
        <div style="float:left; margin-left:15px; width:350px;">Connect and communicate with customers, peers and followers.</div>
        <div style="clear:both; height:10px;"></div>

        <div style="float:left; padding:5px; margin-top:3px; width:30px;"><img src="/images/bullhorn.png" width="24" height="17" alt="" /></div>
        <div style="float:left; padding:5px; width:30px;"><img src="/images/tags.png" width="24" height="25" alt="" /></div>
        <div style="float:left; margin-left:15px; width:350px;">Allow the <? echo $siteName ?> network to increase awareness of your page through the power of "social breadcrumbs".</div>
        <div style="clear:both; height:10px;"></div>

        <div style="float:left; padding:5px; width:30px;"><img src="/images/target.png" width="28" height="28" alt="" /></div>
        <div style="float:left; padding:5px; width:30px;"><img src="/images/radar.png" width="27" height="27" alt="" /></div>
        <div style="float:left; margin-left:15px; width:350px;">Get your message in front of the largest online network of professional mariners, water enthusiasts, industry experts, businesses and services.</div>
        <div style="clear:both; height:10px;"></div>
      </div>

      <div id="company" class="dentry" style="padding:10px; display:none;">
        <div style="font-size:14pt; font-weight:bold; padding-left:10px;">Business or Service</div>
        <div style="font-size:9pt; padding-left:10px; padding-top:5px;">Choose a sector and category</div>

        <div style="margin-left:10px; margin-top:5px;">
          <div style="float:left; font-size:9pt;">
            Sector(s): <!--<div id="ind"></div>-->
          <select name="l1_cat" style="width:130px;"<!-- onchange="javascript: refreshSubcats2( selectedValue( this ) );"-->>
<?
$q = sql_query( "select * from categories where industry=" . PAGE_TYPE_BUSINESS . " order by catname" );
while( $r = mysql_fetch_array( $q ) )
{
?>
          <option value="<? echo $r['cat']; ?>"><? echo $r['catname']; ?></option>
<?
}
?>
          </select>

          </div>

          <div style="float:left; margin-left:20px;  font-size:9pt;">
            <div style="float:left; padding-top:10px;">Categories:</div>
            <div style="margin-left:10px; width:150px; float:right; padding-top:5px;"><?showSingleSelectDropDown( "l1_subcat", 120, 25, 101 );?></div>
<!--
          <select name="l1_subcat" id="l1_subcat_temp" style="width:150px;">
<?
$q = sql_query( "select * from categories where industry IN ( SELECT cat from categories where industry='" . PAGE_TYPE_BUSINESS . "') order by heading, catname" );
while( $r = mysql_fetch_array( $q ) )
{
?>
          <option value="<? echo $r['cat']; ?>"><? echo $r['catname']; ?></option>
<?
}
?>
          </select>-->

          </div>
        </div>

        <div style="font-size:9pt; clear:both; margin: 10px; margin-bottom:5px; padding-top:10px; ">
          <!--Select 1 sector and 1 category for your business or service, this is included with your page. Charges for additional categories and sectors apply.-->
          Select your primary sector and category for your business or service, this is included with your page. Charges for additional categories and sectors apply and can be chosen after page creation.
        </div>

        <div style="margin-top:5px;">
          <div style="float:left; padding:5px; padding-top:10px; position:relative;">
            <input type="text" id="l1_gname" name="l1_gname" data-autocomplete="get_business" style="width:402px;" data-default="Page Name" />
            <div id="suggestionBox1" style="padding:0px; position: absolute; left:10px; top:42px; width:288px; height:205px; z-index:101; display:none;"></div>
          </div>
        </div>

        <div style="margin-top:5px;">
          <div style="float:left; padding:5px;"><input type="text" name="l1_website" style="width:402px;" data-default="Website (optional)"/></div>
        </div>

      </div>

      <div id="vessel" class="dentry" style="padding:10px; display:none;">
        <div style="font-size:14pt; font-weight:bold; padding-left:10px;">Vessel</div>
        <div style="font-size:9pt; padding-left:10px; padding-top:5px;">Choose a type of vessel</div>

        <div>
          <select name="l2_subcat" style="width:200px;">
<?
$q = mysql_query( "select * from categories where industry='" . PAGE_TYPE_VESSEL . "' order by catname" );
while( $r = mysql_fetch_array( $q ) )
{
?>
          <option value="<? echo $r['cat']; ?>"><? echo $r['catname']; ?></option>
<?
}
?>
          </select>
        </div>

        <div style="margin-top:5px;">
          <div style="float:left; padding:5px; position:relative;">
            <input type="text" id="l2_gname" name="l2_gname" data-default="Vessel Name" data-autocomplete="get_vessel">
            <div id="suggestionBox2" style="padding:0px; position: absolute; left:10px; top:42px; width:288px; height:205px; z-index:101; display:none;"></div>
          </div>
          <div style="float:left; padding:5px;"><input type="text" id="l2_exnames" name="l2_exnames" data-default="Ex Names"></div>
        </div>

        <div style="clear:both;"></div>

        <div>
          <div style="float:left; padding:5px;"><input type="text" id="l2_length" name="l2_length" data-default="Length (ft.)" onkeyup="checkNumeric(this);"/></div>
        </div>

      </div>


      <div id="product" class="dentry" style="padding:10px; display:none;">
        <div style="font-size:14pt; font-weight:bold; padding-left:10px;">Professional</div>
        <div style="font-size:9pt; padding-left:10px; padding-top:5px;">Choose a category</div>

        <div>
          <div style="float:left;">
          <select name="l3_cat" style="width:180px;">
<?
$q = mysql_query( "select * from categories where industry='" . PAGE_TYPE_PROFESSIONAL . "' order by catname" );
while( $r = mysql_fetch_array( $q ) )
{
?>
          <option value="<? echo $r['cat']; ?>"><? echo $r['catname']; ?></option>
<?
}
?>
          </select>
          </div>
        </div>

        <div style="margin-top:5px;">
          <div style="float:left; padding:5px; padding-top:10px; position:relative;">
            <input type="text" id="l3_gname" name="l3_gname" data-default="Page Name" style="width:402px;" data-autocomplete="get_pages">
            <div id="suggestionBox3" style="padding:0px; position: absolute; left:10px; top:42px; width:288px; height:205px; z-index:101; display:none;"></div>
          </div>
        </div>

        <div style="margin-top:5px;">
          <div style="float:left; padding:5px;"><input type="text" name="l3_website" value="Website (optional)" style="width:402px;" onclick="if( this.value=='Website (optional)' ) this.value='';"/></div>
        </div>
      </div>

      <div id="arts" class="dentry" style="padding:10px; display:none;">
        <div style="font-size:14pt; font-weight:bold; padding-left:10px;">Arts, Entertainment, Activities or Sports</div>
        <div style="font-size:9pt; padding-left:10px; padding-top:5px;">Choose a category</div>

        <div>
          <div style="float:left;">
          <select name="l4_cat" style="width:150px;">
<?
$q = mysql_query( "select * from categories where industry='" . PAGE_TYPE_ENTERTAINMENT . "' order by catname" );
while( $r = mysql_fetch_array( $q ) )
{
?>
          <option value="<? echo $r['cat']; ?>"><? echo $r['catname']; ?></option>
<?
}
?>

          </select>
          </div>
        </div>

<!--        <div style="margin-top:5px;">
          <div style="float:left; padding:5px; padding-top:10px;"><input type="text" id="l4_gname" name="l4_gname" value="Page Name" style="width:402px;" onclick="if( this.value=='Page Name' ) this.value='';"/></div>
        </div>

-->
        <div style="margin-top:5px;">
          <div style="float:left; padding:5px; padding-top:10px; position:relative;">
			<input type="text" id="l4_gname" name="l4_gname" data-default="Page Name" style="width:402px;" data-autocomplete="get_pages">
            <div id="suggestionBox4" style="padding:0px; position: absolute; left:10px; top:42px; width:288px; height:205px; z-index:101; display:none;"></div>
          </div>
        </div>


        <div style="margin-top:5px;">
          <div style="float:left; padding:5px;"><input type="text" name="l4_website" value="Website (optional)" style="width:402px;" onclick="if( this.value=='Website (optional)' ) this.value='';"/></div>
        </div>
      </div>

      <div id="clubs" class="dentry" style="padding:10px; display:none;">
        <div style="font-size:14pt; font-weight:bold; padding-left:10px;">Club / Association</div>

        <div style="margin-left:10px; margin-top:5px;">
          <div style="float:left; font-size:9pt;">
            Choose a Sector <!--<div id="ind2"></div>-->

          <select name="l5_cat" style="width:200px;"<!-- onchange="javascript: refreshSubcats2( selectedValue( this ) );"-->>
<?
$q = sql_query( "select * from categories where industry=" . PAGE_TYPE_BUSINESS . " order by catname" );
while( $r = mysql_fetch_array( $q ) )
{
?>
          <option value="<? echo $r['cat']; ?>"><? echo $r['catname']; ?></option>
<?
}
?>
          </select>

          </div>
        </div>

        <div style="font-size:9pt; clear:both; margin: 10px; margin-bottom:5px; padding-top:10px; ">
          <!--Select 1 sector and 1 category for your business or service, this is included with your page. Charges for additional categories and sectors apply.-->
          Select your primary sector and category for your business or service, this is included with your page. Charges for additional categories and sectors apply and can be chosen after page creation.
        </div>

        <div style="margin-top:5px;">
          <div style="float:left; padding:5px; padding-top:10px; position:relative;">
			<input type="text" id="l5_gname" name="l5_gname" data-default="Page Name" style="width:402px;" data-autocomplete="get_pages">
            <div id="suggestionBox5" style="padding:0px; position: absolute; left:10px; top:42px; width:288px; height:205px; z-index:101; display:none;"></div>
          </div>
        </div>

<!--        <div style="margin-top:5px;">
          <div style="float:left; padding:5px; padding-top:10px;"><input type="text" id="l5_gname" name="l5_gname" value="Page Name" style="width:402px;" onclick="if( this.value=='Page Name' ) this.value='';"/></div>
        </div>
-->
        <div style="margin-top:5px;">
          <div style="float:left; padding:5px;"><input type="text" name="l6_website" value="Website (optional)" style="width:402px;" onclick="if( this.value=='Website (optional)' ) this.value='';"/></div>
        </div>
      </div>

      <div style="clear:both;"></div>

      <div id="common" class="dentry" style="padding:10px; padding-top:5px; display:none;">
        <div>
          <div style="float:left;">
          <select name="privacy">
            <option value="0">Accessible to:</option>
            <option value="<? echo PRIVACY_EVERYONE ?>">Everyone</option>
            <option value="<? echo PRIVACY_FRIENDS ?>">Connections</option>
            <!--<option value="<? echo PRIVACY_SELF ?>">Myself</option>-->
          </select>
          </div>

          <div style="float:left; margin-top:10px; font-size:9pt;">
            Privacy
          </div>
        </div>

        <div style="clear:both;"></div>

        <div style="font-size:9pt; margin-left:10px; margin-top:10px;">
          <div style="float:left;"><input type="checkbox" name="agree" id="agree" checked="checked" style="padding:0px; margin:0px; width:20px;"/></div>
          <div style="float:left; padding-top:0px;">I agree to the <a href="/tos.php" target="_blank">Terms of Service</a> and I am authorized to create this page.</div>
        </div>

      <div style="clear:both;"></div>

        <div style="margin-top:15px; margin-left:10px;">
          <input type="button" value="Next" style="color:#326798; font-size:12pt; width:80px; margin-left:10px; margin:0px; height:33px;" class="button" onclick="javascript: if( submitForm() ) loadShare('newpage2',1);"/>
        </div>
      </div>
    </div>

    <div style="clear:both;"></div>
  </div>

  <div style="clear:left;"></div>

</div>

</form>

<div style="float:left; margin-left:3px;">
  <!-- Advertisements -->
  <div style="width: 135px; float: left; position: relative;">
    <? if( $API->adv ) { ?>
  	<div style="padding-left: 15px; margin-top:-25px;">
  		<?php showAd("skyscraper"); ?>
  	</div>
    <? } ?>
  </div>
</div>

<img src="/images/yacht_blue.png" style="display:none;" width="1" height="1" />
<img src="/images/building_blue.png" style="display:none;" width="1" height="1" />
<img src="/images/teacher_blue.png" style="display:none;" width="1" height="1" />
<img src="/images/boarding_blue.png" style="display:none;" width="1" height="1" />
<img src="/images/puzzle_blue.png" style="display:none;" width="1" height="1" />


<script type="text/javascript">
<!--
var leftSelect = 0;
var dupeName = false;

function checkNumeric(entry)
{
//  var numeric = /^[0-9]*$/;
  entry.value = entry.value.replace(/[^0-9]/g,"");
}

function mouseIn(sel)
{
  switch( sel )
  {
    case 1:
      e=document.getElementById('lbi1'); if( e ) e.src='/images/building_blue.png';
    break;

    case 2:
      e=document.getElementById('lbi2'); if( e ) e.src='/images/yacht_blue.png';
    break;

    case 3:
      e=document.getElementById('lbi3'); if( e ) e.src='/images/teacher_blue.png';
    break;

    case 4:
      e=document.getElementById('lbi4'); if( e ) e.src='/images/boarding_blue.png';
    break;

    case 5:
      e=document.getElementById('lbi5'); if( e ) e.src='/images/puzzle_blue.png';
    break;

  }
}

function mouseOut(sel)
{
  switch( sel )
  {
    case 1:
      e=document.getElementById('lbi1');
      if( e && leftSelect != 1) e.src='/images/building_grey.jpg';
    break;

    case 2:
      e=document.getElementById('lbi2');
      if( e && leftSelect != 2) e.src='/images/yacht_gray.png';
    break;

    case 3:
      e=document.getElementById('lbi3');
      if( e && leftSelect != 3) e.src='/images/teacher.png';
    break;

    case 4:
      e=document.getElementById('lbi4');
      if( e && leftSelect != 4) e.src='/images/boarding.png';
    break;

    case 5:
      e=document.getElementById('lbi5');
      if( e && leftSelect != 5) e.src='/images/puzzle.png';
    break;
  }
}

function changeCategory(selection, div)
{
  leftSelect = selection;

  for( c = 1; c <= 5; c++ )
  {
    if( c == selection ) continue;

    e = document.getElementById( "lb" + c );
    if( e )
      e.className = "page-create-left-button";

    mouseOut( c );
  }

  div.className = "page-create-left-button-chosen";
  mouseIn( selection );

  def = document.getElementById( "default" );
  vessel = document.getElementById( "vessel" );
  company = document.getElementById( "company" );
  common = document.getElementById( "common" );
  product = document.getElementById( "product" );
  arts = document.getElementById( "arts" );
  clubs = document.getElementById( "clubs" );

  def.style.display = 'none';
  vessel.style.display = 'none';
  company.style.display = 'none';
  product.style.display = 'none';
  arts.style.display = 'none';
  clubs.style.display = 'none';
  common.style.display = '';

  switch( selection )
  {
    case 1:
      company.style.display = '';
    break;

    case 2:
      vessel.style.display = '';
    break;

    case 3:
      product.style.display = '';
    break;

    case 4:
      arts.style.display = '';
    break;

    case 5:
      clubs.style.display = '';
    break;

  }
}

function submitForm()
{
  if( dupeName && leftSelect != 2 )
  {
    alert( 'This page name already exists, please choose another.' );
    return false;
  }

  switch( leftSelect )
  {
    case 2:
      length = document.getElementById( "l2_length" ).value;
      name = document.getElementById( "l2_gname" ).value;

      if( name == "Vessel Name" || name == "" )
      {
        alert( 'Please enter a vessel name before continuing.' );
        return false;
      }

      if( length == "Length in feet" || length == "" )
      {
        alert( 'Please enter a vessel length before continuing.' );
        return false;
      }

    break;

    case 1:
    case 3:
    case 4:
    case 5:
      name = document.getElementById( "l" + leftSelect + "_gname" ).value;

      if( name == "Page Name" || name == "" )
      {
        alert( 'Please enter a page name before continuing.' );
        return false;
      }
    break;


  }

  if( !document.getElementById( "agree" ).checked )
  {
    alert( 'You must agree to the Terms of Service before continuing.' );
    return false;
  }

  document.getElementById( "type_selection" ).value = leftSelect;

  return true;
}

function refreshSubcats( cat )
{
	getAjax("/pages/page_create_subcats.php?cat=" + cat, function (data)
	{
    e = document.getElementById( "company_subcats" );
    if( e  )
    {
      e.innerHTML = data;
    }
	});
}
/*
newComboListBox( 3, "PandS" );
newComboListBox( 4, "ind" );
newComboListBox( 5, "ind2" );
*/

<? if( isset( $_GET['sel'] ) ) {
  $s = intval( $_GET['sel'] );
?>
changeCategory( <?=$s?>, document.getElementById( 'lb<?=$s?>' ) );
<? } ?>

-->
</script>
<?
include "../footer.php";
?>