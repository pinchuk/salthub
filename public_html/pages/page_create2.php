<?
include "../inc/inc.php";

function getBetween( $start, $end, $str )
{
  $s_pos = strpos( $str, $start );
  if( $s_pos === false ) { return ""; }

  $s_pos += strlen( $start );

  $e_pos = strpos( $str, $end, $s_pos );
  if( $e_pos === false ) { echo "no end"; return ""; }

  return substr( $str, $s_pos, $e_pos - $s_pos );
}

$page_data = array();

$sel = intval( $_POST['type_selection'] );

if( $sel == 1 ) //Creating business pages supports multiple subcategory types
{
/*
  $_POST['l1_subcat'] = $_SESSION['page_create_categories'][0];
  $_POST['l1_cat'] = $_SESSION['page_create_sectors'][0];
*/
}

switch( $sel )
{
  case 1:
    $page_data['type'] = PAGE_TYPE_BUSINESS;
    $page_data['cat'] = $_POST['l1_cat'];
    $page_data['subcat'] = $_POST['l1_subcat'];
    $page_data['gname'] = $_POST['l1_gname'];

    if( $_POST['l1_website'] != "Website (optional)" )
      $page_data['www'] = $_POST['l1_website'];
  break;

  case 2:
    $page_data['type'] = PAGE_TYPE_VESSEL;
    $page_data['gname'] = $_POST['l2_gname'];

    $page_data['cat'] = $_POST['l2_subcat'];
  break;

  case 3:
    $page_data['type'] = PAGE_TYPE_PROFESSIONAL;
    $page_data['cat'] = $_POST['l' . $sel . '_cat'];
    $page_data['gname'] = $_POST['l' . $sel . '_gname'];

    if( $_POST['l' . $sel . '_website'] != "Website (optional)" )
      $page_data['www'] = $_POST['l' . $sel . '_website'];
  break;

  case 4:
    $page_data['type'] = PAGE_TYPE_ENTERTAINMENT;
    $page_data['cat'] = $_POST['l' . $sel . '_cat'];
    $page_data['gname'] = $_POST['l' . $sel . '_gname'];

    if( $_POST['l' . $sel . '_website'] != "Website (optional)" )
      $page_data['www'] = $_POST['l' . $sel . '_website'];
  break;

  case 5:
    $page_data['type'] = PAGE_TYPE_BUSINESS;
//    $page_data['cat'] = $_SESSION['page_create_sectors'][0];
    $page_data['cat'] = $_POST['l' . $sel . '_cat'];
    $page_data['subcat'] = CAT_CLUB_ASSOC;
    $page_data['gname'] = $_POST['l5_gname'];
    $_SESSION['page_create_categories'] = Array( CAT_CLUB_ASSOC );  //A club is just like a business, but only has one sub categoryf


    if( $_POST['l5_website'] != "Website (optional)" )
      $page_data['www'] = $_POST['l5_website'];
  break;

  default:
    header( "Location: page_create.php" );
    exit;
  break;
}

$fields = array();
$values = array();
foreach( $page_data as $key => $val )
{
  $fields[] = $key;
  $values[] = addslashes($val);
}

mysql_query( "insert into pages (" . implode( ",", $fields ) . ") values ('" . implode( "','", $values ) . "')" );
$gid = mysql_insert_id();

if( intval( $_POST['type_selection'] ) == 2 ) //For Vessel types
{
  mysql_query( "insert into boats (name, length,gid) values ('" . addslashes($page_data['gname']) . "', '" . intval($_POST['l2_length']) . "', $gid);" );
  mysql_query( "update pages set glink='$gid' where gid='$gid'" );
}

if( $page_data['type'] == PAGE_TYPE_BUSINESS )
{
  mysql_query( "insert into page_categories (gid, cat) values ($gid, " .   $page_data['cat'] . ")" );
  mysql_query( "insert into page_categories (gid, cat) values ($gid, " .   $page_data['subcat'] . ")" );


  //if( $_POST['from_popup'] == 1 )  //This is the method for both popups and the page_create.php
  if( $sel == 1 || $sel == 5 ) //If this is a business type page
  {
    for( $c = 1; $c < sizeof( $_SESSION['page_create_categories'] ); $c++ )
      mysql_query( "insert into page_categories (gid, cat) values ($gid, " . $_SESSION['page_create_categories'][$c] . ")" );

    for( $c = 1; $c < sizeof( $_SESSION['page_create_sectors'] ); $c++ )
      mysql_query( "insert into page_categories (gid, cat) values ($gid, " . $_SESSION['page_create_sectors'][$c] . ")" );

    $_SESSION['page_create_sectors'] = Array();
    $_SESSION['page_create_categories'] = Array();
  }
}


$email = quickQuery( "select email from users where uid='" . $API->uid . "'" );

$msg = quickQuery("select content from static where id='page_create'");
$msg = str_replace("{NAME}", quickQuery("select name from users where uid='" . $API->uid . "'") , $msg);
$msg = str_replace("{PAGE}", $page_data['gname'], $msg);
$msg = str_replace("{PAGE_URL}", "http://www." . $siteName . ".com" . $API->getPageURL( $gid ), $msg);
$msg = str_replace("{PROFILE_URL}", "http://www." . $siteName . ".com" . $API->getProfileURL( $API->uid ), $msg);
$msg = str_replace("{SITENAME}", $siteName, $msg);
$msg = textToHTML($msg);

emailAddress($email, "Thanks for creating the \"" . $page_data['gname'] . "\" page on $siteName!", $msg );

// Done creating the new page, need to handle the users chosen to be added to the group

$action0 = getBetween( "action0=", "&amp;priv=0", $_POST['sharedata'] );
$action0 = str_replace( ",,", ",", $action0 );
$cids = preg_split("/,/", $action0, -1, PREG_SPLIT_NO_EMPTY);

$uids = array();

foreach ($cids as $cid)
{
	$cid = intval($cid);

	$x = sql_query("select eid,site from contacts where uid=" . $API->uid . " and cid=$cid");
	$contact = mysql_fetch_array($x, MYSQL_ASSOC);

	switch ($contact['site'])
	{
		case SITE_FACEBOOK:
		$fbid = intval($contact['eid']);
		//check to see if user is a member of our site already
		$uid = quickQuery("select uid from users where fbid=$fbid");
		if (empty($uid)) //nope - so we create him
		{
			sql_query("insert into users (fbid,active) values ($fbid,0)");
			$uid = mysql_insert_id();
		}

    // We should be using fbSendMessage here, not fbStreamPublish
		//$API->fbStreamPublish(null, "I sent you a message on $siteName.  Log-in to see the message at http://" . SERVER_HOST . "/facebook", null, $fbid);
    //$API->fbSendMessage($info['uid'], $subj, $msg)
		$uids[] = $uid; //we will send the actual message to the new user below
		break;

		case SITE_TWITTER:
//I don't think this does anything.
//		$API->twSendMessage(null, "$subj: $body", $contact['eid']);
		break;

		case SITE_OURS:
		$uids[] = $contact['eid'];
		break;

		default: //email
		if (isEmailAddressValid($contact['eid']))
			$_POST['emails'] .= "," . $contact['eid'];
		break;
	}
}

$owner = null;

// our site - per user processing
if( !in_array( $API->uid, $uids ) )
  $uids[] = $API->uid;

if (count($uids) > 0)
{
  foreach ($uids as $uid)
	{
    if( $uid == $API->uid ) $admin = 1;
    else $admin = 0;

    sql_query("insert into page_members (gid,uid, visited, admin) values ($gid,$uid, 0, $admin)");
    $API->feedAdd("G", $gid, $uid, $API->uid, $gid);
    $API->sendNotification(NOTIFY_GROUP_ADD, array("uid" => $uid, "from" => $API->uid, "gid" => $gid ));
 	}
}

//emails
if ($API->verify == 1)
{
  $from = quickQuery("select name from users where uid=" . $API->uid);

  $template = quickQuery("select content from static where id='page_join'" );

  $page = $API->getPageInfo($gid);
  $gname = $page['gname'];

  $gname = quickQuery( "select gname from pages where gid='$gid'" );
  $url = "http://www." . $siteName . ".com/?joingid=$gid&uid=" . $API->uid;

  $email_body = str_replace( "{NAME_FROM}", $from, nl2br($template) );
  $email_body = str_replace( "{PAGE}", $gname, $email_body );
  $email_body = str_replace( "{REF_LINK}", "<a href=\"$url\">$url</a>", $email_body );
  $email_body = str_replace( "{SITENAME}", $siteName, $email_body );
  $email_body = str_replace( "{MESSAGE}", $body, $email_body );

	$userEmail = quickQuery("select email from users where uid=" . $API->uid);
	foreach (explode(",", $_POST['emails']) as $email)
	{
		if (isEmailAddressValid($email))
			emailAddress($email, $subj, $email_body, $userEmail);
	}
}

if( isset( $_POST['popup'] ) )
  echo $API->getPageURL( $gid, $page_data['gname'] ) . "/editpage";
else
  header( "Location: " . $API->getPageURL( $gid, $page_data['gname'] ) . "/editpage" );
?>