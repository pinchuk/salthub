function ac_get_company_or_school(data)
{
	log(data, 'autocomplete response received for ' + data.guid);
	
	ul = $('<ul class="autocomplete"></ul>');
	for (i in data.results)
	{
		li = $('<li></li>');
		img_url = (typeof data.results[i].container_url == 'string' ? data.results[i].container_url + '/' + data.results[i].hash + '_square.jpg' : '/images/company_default.png');
		html  = '<img src="' + img_url + '">';
		html += '<div><span class="name">' + data.results[i].title + '</span><br>' + data.results[i].type_str;
		html += '</div>';
		
		$(li).html(html);
		$(li).attr('data-json', json_encode(data.results[i]));
		$(ul).append($(li));
	}
	
	$('ul.autocomplete').remove();
	
	if (data.results.length > 0)
	{
		parent = getClosestRelativeParent($('input[data-guid="' + data.guid + '"]'));
		$(ul).appendTo(parent).css('width', $('input[data-autocomplete]').width() + 2);
		
		$('ul.autocomplete li').mousedown(
			function ()
			{
				json = json_decode($(this).attr('data-json'));
				location = '/page/' + json.gid + '-' + escape(json.title);
			}
		);
	}
}

function ac_get_pages(data)
{
	ac_get_business(data);
}

function ac_get_business(data)
{
	el = ac_get_search(data);
	$(el).css('margin', '-6px 0 0 5px').css('width', '414px');
	//$('input[data-autocomplete]').blur( function () { $('ul.autocomplete').remove(); });
}

function ac_get_vessel(data)
{
	el = ac_get_search(data);
	$(el).css('margin', '-6px 0 0 5px').css('width', '182px');
	$('input[data-autocomplete]').blur( function () { $('ul.autocomplete').remove(); });
}