<?
/*
Common javascript used by various pages in the /pages subdirectory.
*/

include( '../inc/inc.php' ); ?>
function leavePage(gid)
{
	if (!confirm("Are you sure you want to leave this page?"))
		return;
	
	postAjax("/pages/leave.php", "gid=" + gid, "eval");
}

function addPageMedia(type)
{
	html  = '<div class="smtitle2" style="font-size: 10pt; padding-bottom: 3px; border-bottom: 2px solid #d8dfea;">';
	html += '	Add ' + (type == "V" ? "Videos" : "Photos") + ' to <a href="' + page.url + '">' + page.gname + '</a>';
	html += '</div>';
	html += '<div style="width: 200px; float: left; border-right: 1px solid #d8dfea;">';
	html += '	<div class="btnaddmedia" onclick="javascript:showMediaChooser(' + page.gid + ', \'' + type + '\');">Add Existing Media</div>';
	html += '</div>';
	html += '<div style="width: 200px; float: left;">';
	html += '	<div class="btnaddmedia" onclick="javascript:showTweetBox();">Upload New Media</div>';
	html += '</div>';
	html += '<div style="clear: both;"></div>';

	showPopUp("", html);
}
/*
function updatePage(data)
{
	data.gid = page.gid;
	postAjax("/pages/update.php", "json_data=" + escape(json_encode(data)), "updatePageHandler");
}
*/
function updatePageHandler( data )
{
//  alert( data );
}

var pageExistingMedia;
function showAddPageExistingMediaHandler(x)
{

	eval('data = \' + x + \';');
	showPopUp("", data.html, [541,494]);

	pageExistingMedia = data.media;
}

function showAddPageExistingMedia(type)
{
	getAjax("/pages/addmedia.php?gid=" + page.gid, "showAddPageExistingMediaHandler");
}

function modifyPageMember(gid, uid, action)
{
	postAjax("/pages/modifymember.php", "uid=" + uid + "&gid=" + gid + "&action=" + action, function(data)
		{
			eval(data);
		}
	);
}

var filterInPage = -1;
var filterType = "";

function filterAddPageExistingMedia(type, inpage)
{
	if (typeof type == "undefined") // all
	{
		filterInPage = -1;
		filterType = "";
	}
	else
	{
		if (type != null)
			filterType = type;
		
		if (inpage != null)
			filterInPage = inpage;
	}

	document.getElementById("agem-a").style.fontWeight = filterType == "" && filterInPage == -1 ? "bold" : "";
	document.getElementById("agem-v").style.fontWeight = filterType == "V" ? "bold" : "";
	document.getElementById("agem-Vhead").style.display = filterType == "V" || filterType == "" ? "" : "none";
	document.getElementById("agem-p").style.fontWeight = filterType == "P" ? "bold" : "";
	document.getElementById("agem-Phead").style.display = filterType == "P" || filterType == "" ? "" : "none";
	document.getElementById("agem-i").style.fontWeight = filterInPage == 1 ? "bold" : "";
	document.getElementById("agem-n").style.fontWeight = filterInPage == 0 ? "bold" : "";

	document.getElementById("agem-Vhead").innerHTML = "Videos " + (filterInPage == 0 ? "Not in Page" : (filterInPage == 1 ? "In Page" : ""));
	document.getElementById("agem-Phead").innerHTML = "Photos " + (filterInPage == 0 ? "Not in Page" : (filterInPage == 1 ? "In Page" : ""));

	searchAddPageExistingMedia();
}

function searchAddPageExistingMedia(q)
{
	if (q == null)
	{
		q = document.getElementById("txtgrpsearch").value;
		
		if (q == "Search ...")
			q = "";
	}
	
	q = q.toLowerCase();

	for (var i in pageExistingMedia)
	{
		e = document.getElementById("mps-" + pageExistingMedia[i].type + pageExistingMedia[i].id);
		
		if (pageExistingMedia[i].title.indexOf(q) == -1 || (filterInPage > -1 && pageExistingMedia[i].inpage != filterInPage) || (filterType != "" && pageExistingMedia[i].type != filterType))
			e.style.display = "none";
		else
			e.style.display = "";
	}
}

function editPageAbout(edit)
{
  //show "add another location"
  e = document.getElementById("grpedit-addAnotherLocation" );
  if( e )
  {
    if( edit )
      e.style.display = '';
    else
      e.style.display = 'none';
  }

	if (edit)
  {
		disp = ['', 'none', 190];
  }
	else
  {
		disp = ['none', '', 125];
  }

	fields = ['type', 'descr', 'cat', 'email', 'www', 'address', 'privacy', 'phone', 'cell', 'subcat', 'fax', 'products', 'services', 'save'<? if( $API->admin == 1 ) { ?>, 'gname'<? } ?>, 'exnames'];


	for (var i in fields)
		for (var j = 0; j < 2; j++)
    {
      e = document.getElementById("grpedit-" + fields[i] + j);
      if( e )
        e.style.display = disp[j];
    }


	fields = [ 'location_name', 'contact_person', 'email', 'www', 'address', 'phone', 'cell', 'fax' ];

  for( var c = 0; c < num_locations; c++ )
  {
  	for (var i in fields)
    {
      if( i != 3 || (i == 3 && c == 0) ) //Only show www when location = 0
      {
    		for (var j = 0; j < 2; j++)
        {
          e = document.getElementById("grpedit-" + fields[i] + j + "-" + c );
          if( e )
            e.style.display = disp[j];
        }
      }
    }
  }

//	for (var i = 0; i < 3; i++)
//		document.getElementById("si" + i).style.height = disp[2] + "px";

	showBlueGrad();
}
/*
function savePageAbout()
{
	fields = ['type', 'descr', 'cat', 'email', 'www', 'address', 'phone', 'cell', 'fax', 'subcat', 'products', 'services', 'contact_person', 'privacy'<? if( $API->admin == 1 ) { ?>, 'gname'<? } ?>, 'exnames','year','shipyard','hullid','imo','mmsi','callsign','flag','navalarchitect','stylist','decorator','sistership','refit','length','beam','draught','hullsuperstr','engine','speedmax','speedcruise','rangenm','fuelltr','waterltr', 'guests', 'crew' ];

	update = {'gid': page.gid};

	for (var i in fields)
	{
		e = document.getElementById("grpedit-" + fields[i]);
    if( e == null ) continue;

    if( typeof( e.value ) == "undefined" ) continue;

		val = escape(e.value);
		page[fields[i]] = val;
		update[fields[i]] = encodeURIComponent(val);

		if (fields[i] == "cat" || fields[i] == "privacy")
      if( document.getElementById("grpedit-" + fields[i] + "1") )
  			document.getElementById("grpedit-" + fields[i] + "1").innerHTML = e.options[e.selectedIndex].text;
		else
      if( document.getElementById("grpedit-" + fields[i] + "1") )
  			document.getElementById("grpedit-" + fields[i] + "1").innerHTML = htmlentities(val).replace(/\n/g, '<br>');
	}

	postAjax("/pages/update.php", "json_update=" + json_encode(update), "saveHandler");

	editPageAbout(false);
}
*/
function saveHandler(data)
{
  //alert( data );

	location.reload(true);
}

function showMorePages(o)
{
	limit = 5;
	getAjax("/pages/previews.php?o=" + o + "&limit=" + limit + "&p=" + pagesShown[o], function(json)
	{
		eval('data = ' + json + ';');
		if (o == "r")
			document.getElementById("pages-r").innerHTML = data.html;
		else
		{
			newdiv = document.createElement("div");
			newdiv.innerHTML = data.html;
			p = document.getElementById("pages-" + o).appendChild(newdiv);

			if (data.pages.length < limit)
				document.getElementById("showmore-" + o).style.display = "none";
			else
				pagesShown[o] += data.pages.length;
		}
	});
}

function refreshSide(gid)
{
	postAjax("/profile/side.php", "gid=" + gid, "refreshHandler");
}

function refreshHandler( data )
{
  e = document.getElementById("sidediv");
  if( e )
  {
    e.innerHTML = data;
  }
}

function showCatSelection()
{
	getAjax("/pages/category_selection_popup.php?gid=" + page.gid, "showCatSelectionHandler");
}

function showCatSelectionHandler(html)
{
	showPopUp("", html, null, null, true );
}

function saveCatSelection()
{
  subcat = 0;
  cat = document.getElementById( 'cat' ).value;
  if( cat == 111 || cat == 112 )
  {
    subcat = document.getElementById( 'subcat' + cat ).value;
  }

  getAjax("/pages/save_category_selection.php?gid=" + page.gid + "&cat=" + cat + "&subcat=" + subcat, "saveCatSelectionHandler");

  closePopUp();
}

function saveCatSelectionHandler(data)
{

}
<?
$page = $_SESSION['pagedata'];
unset( $_SESSION['pagedata'] );

if( isset( $page ) ) {
?>

function refreshCategory( gid, type )
{
  getAjax("/pages/cat_selection.php?gid=" + gid + "&type=" + type, "refreshCategoryHandler");
}

function refreshSubcatType( gid, cat )
{
  getAjax("/pages/subcat_selection.php?gid=" + gid + "&cat=" + cat, "refreshSubcatTypeHandler");
}

function refreshCategoryHandler( data )
{
  e = document.getElementById( "category" );
  if( e )
  {
    e.innerHTML = data;
  }
  editPageAbout(true);

  e = document.getElementById( "grpedit-cat" );
  if( e )
    refreshSubcatType( <?= $page['gid'] ?>, selectedValue( e ) );
}

function refreshSubcatTypeHandler( data )
{
  e = document.getElementById( "subcat" );
  if( e )
  {
    e.innerHTML = data;
  }
  editPageAbout(true);
}
function editFollowPage()
{
	html  = '<div class="followmepopup">';
	html += '	<div class="site"><img src="/images/salt_badge_48x48.png" style="width: 48px;" alt="" />http://<?=SERVER_HOST . $API->getPageURL($page['gid'])?></div>';
	html += '	<div class="site"><div style="height: 2px; background: #c0c0c0; overflow: hidden;">&nbsp;</div></div>';
	html += '	<div class="site"><img src="/images/f.png" alt="" /><span>Facebook</span>http://www.facebook.com/<input type="text" id="follow-fbid" value="<?= $page['fbid'] ?>" /></div>';
	html += '	<div class="site"><img src="/images/t.png" alt="" /><span>Twitter</span>http://twitter.com/<input type="text" id="follow-twid" value="<?= $page['twid'];?>" /></div>';
	html += '	<div class="site"><img src="/images/linkedin.png" alt="" /><span>Linked In</span>http://www.linkedin.com/<input type="text" id="follow-linkedin_url" value="<?=$page['linkedin_url']?>" /></div>';
	html += '	<div class="site"><img src="/images/youtube.png" alt="" /><span>YouTube</span>http://www.youtube.com/<input type="text" id="follow-ytid" value="<?=$page['ytid']?>" /></div>';
	html += '	<div class="site" style="text-align: center;"><input type="button" class="button" value="Save" onclick="javascript:saveFollowPage();" /> &nbsp; &nbsp; <input type="button" class="button" value="Cancel" onclick="javascript:closePopUp();" /></div>';
	html += '</div>';

	showPopUp("Manage your website and social profile links", html);
}

function saveFollowPage()
{
	data  = "save=1";
	data += "&fbid=" + escape(document.getElementById("follow-fbid").value);
	data += "&twid=" + escape(document.getElementById("follow-twid").value);
	data += "&linkedin_url=" + escape(document.getElementById("follow-linkedin_url").value);
	data += "&ytid=" + escape(document.getElementById("follow-ytid").value);
  data += "&gid=<?= $page['gid'] ?>";

	postAjax("/pages/follow_pages.php", data, "saveFollowPageHandler");

	closePopUp();
}

function saveFollowPageHandler( data )
{
  location.reload(true);
}

<?
}
?>
function claimThisPage(gid)
{
  html = '<div style="margin:15px;">Automated page verification is not available for this page. Please <br />';
  html += '<a href="javascript:void(0);" onclick="javascript: sendEmail( \'CR@salthub.com\', \'Your corporate email Address: \\nWebsite:\\nPosition:\\nCompany address:\', \'Claiming http://<?= SERVER_HOST ?>/page/' + gid +'-pg\' );">send us a message</a> to claim this page. Please include your corporate<br /> email address, website, position and company address.<div style="clear:both;"></div></div>';

	showPopUp("Steps to Claim this page", html);
}

function updatePageInfo(x)
{
	updating = x;
	text = "";
	html = "";

  if (updating == "contactfor")
	{
		text = 'Update what this page should be contacted for<br /><div style="text-align:center;"><div style="float:left; margin-left:30px;"><img src="/images/exclamation2.png" width="16" height="16" alt=""/></div><div style="float:left; font-weight:300;">&nbsp;&nbsp;doing so will increase your chance of being found in search</div></div>';
<?
  $data = explode( chr(2), $page['contactfor'] );
?>
		html += '<div style="text-align: left;">';
<?
for( $c = 0; $c < 6; $c++ )
{
?>
		html += '	<div class="embedlabel"><div style="padding-top: 3px;"><? echo $c+1 ?>:</div></div>';
		html += '	<div class="embedinput" style="height: 16px; font-weight: normal;">';
		html += '		<input id="contactfor-<? echo $c ?>" value="<? if( $data[$c] != "" ) echo $data[$c]; ?>" type="text" size="35" maxlength="40">';
		html += '	</div>';
<? } ?>
    html += '</div>';
		html += '<div style="clear: both; height: 10px;"></div>';

	}

	if (text != "")
	{
		content = '<div style="font-size: 9pt; padding-bottom: 10px;">' + text + '<p />';

		if (html == "")
			content += '<input type="text" id="updateinfo" value="' + value + '" style="width: 250px;" />';
		else
			content += html;

		content += '</div><input type="button" class="button" value="Save" onclick="javascript:doUpdatePageInfo();" /> &nbsp; &nbsp; <input type="button" class="button" value="Cancel" onclick="javascript:closePopUp();" />';

		showPopUp2("", content);
		return;
	}
}

function doUpdatePageInfo()
{
		text = "";
    for( c = 0; c < 6; c++ )
    {
      if( document.getElementById("contactfor-" + c).value != '' )
        text += document.getElementById("contactfor-" + c).value + String.fromCharCode(2);
    }

		postAjax("/pages/updatecontactfor.php", "data=" + escape(text) + "&gid=<?=$page['gid']; ?>", "updatePageHandler");
	closePopUp();

}

function updatePageHandler(data)
{
//  window.location.reload();
  e = document.getElementById('contactfor');
  if( e )
  {
    e.innerHTML = data;
  }
}
function showSendMessageToPage( gid, name, pic, subject, message )
{
  if( subject == undefined ) subject = "";
  if( message == undefined ) message = "";

	showPopupUrl( '/messaging/send_message_popup.php?gid=' + escape(gid) + '&name=' + escape(name) + '&subj=' + escape(subject) + '&msg=' + escape(message) + '&pic=' + escape(pic) );
}

function sendMessageToPage(gid)
{

	subj = escape(document.getElementById("msgsubj").value);
	body = escape(document.getElementById("msgbody").value);
  email = "";



  if( document.getElementById("email") )
  {
    email = escape( document.getElementById("email").value );
    if( email == "" )
    {
      alert( "Please enter your email address" );
      return;
    }
  }

	if (subj == "")
	{
		alert("Please enter a subject.");
		return;
	}

	if (body == "")
	{
		alert("Please type a message.");
		return;
	}

	document.getElementById("msgbuttons").innerHTML = "<img src='/images/tbpreload.gif' alt='' />";


	postAjax("/messaging/sendmessage.php", "gid=" + gid + "&subj=" + subj + "&body=" + body + "&email=" + email, function(data)
	{
		if (data == "OK")
			showPopUp2("", "Your message has been sent.");
		else
    {
      if( admin )
  			showPopUp2("", "Error: " + data );
      else
  			showPopUp2("", "There was an error sending your message.&nbsp; Please try again later.");
    }
	});


}


function verifyBusinessPageCreateForm()
{
  if( document.getElementById( 'l1_gname' ).value == '' || document.getElementById( 'l1_gname' ).value == 'Page Name' )
  {
    alert( 'Please enter a page name before proceeding' );
    return false;
  }
/*
  if( getSelectionCount(3) == 0 || getSelectionCount(4) == 0 )
  {
    alert( 'Please choose at least 1 industry and 1 product/service before proceeding.' );
    return false;
  }
*/
  return true;
}

function verifyPageCreateForm()
{
  if( document.getElementById( 'l1_gname' ).value == '' || document.getElementById( 'l1_gname' ).value == 'Page Name' )
  {
    alert( 'Please enter a page name before proceeding' );
    return false;
  }
/*
  if( getSelectionCount(3) == 0 || getSelectionCount(4) == 0 )
  {
    alert( 'Please choose at least 1 industry and 1 product/service before proceeding.' );
    return false;
  }
*/
  return true;
}

function changePagePrimaryPhoto( id, gid, hash )
{
  e = document.getElementById( 'profilepic' );
  if( e )
    e.src = '/img/N178x266/photos/' + id + '/' + hash + '.jpg';

	getAjax("/pages/update_primary_photo.php?gid=" + gid + "&id=" + id, "void" );
}

function addAnotherLocation()
{
  e = document.getElementById( 'grpedit-NewLocations' );

  if( e )
  {
    getAjax("/pages/side_contactinfo_newLocation.php?l=" + num_locations, "addAnotherLocationResponse");
  }
}

function addAnotherLocationResponse(data)
{
  e = document.getElementById( 'grpedit-NewLocations' );
  if( e )
  {
		newdiv = document.createElement("div");
  	newdiv.innerHTML = data;
    e.appendChild(newdiv);
    num_locations++;
  }
}