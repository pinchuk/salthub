<?php
/*
This is used to generate the lists of pages shown on /pages/index.php (pages you might like, pages you're in, etc).
*/

if (empty($API))
{
	include "../inc/inc.php";
	
	$o = $_GET['o'];
	$p = intval($_GET['p']);
	$limit = intval($_GET['limit']);

	$ajax = true;
}

/*

when:
o = r, we're looking for pages of friends, and pages that are similar to the ones we're in.
o = m, we're looking for pages that I'm in.
o = a, we're looking for pages that the user is an admin of
*/


if ($limit > 100 || intval($limit) == 0)
	$limit = 5;

if( isset( $p ) )
{
  $limit = "$p, $limit";
}


/*
if (!$tempTableCreated)
{
	$q = "
	create temporary table pages_tmp
	select t3.gid,gname,member,cat,catname,pid,hash,ifnull(friends, 0) as friends from
	(
		select g.gid,g.gname,ifnull(t1.gid, 0) as member,c.cat,c.catname,pid,hash from pages g
		left join (select gm.gid from page_members gm where uid={$API->uid}) t1 on t1.gid=g.gid
		left join photos p on p.id=g.pid
		left join categories c on c.cat=g.cat
		order by rand()
	) t3
	left join
	(
		select gid,count(*) as friends from friends
		inner join page_members m on m.uid=if(id1={$API->uid},id2,id1)
		where {$API->uid} in (id1,id2)
		group by gid
	) t2 on t2.gid=t3.gid
	";



	//die($q);
	sql_query($q);

	$tempTableCreated = true;
}

if ($o == "a")
	$q = "select g.* from (select * from pages_tmp where member > 0) g left join (select gm.gid from page_members gm where uid={$API->uid} and admin=1) t1 on t1.gid=g.gid";
else
	$q = "select * from pages_tmp where member" . ($o == "m" ? "!" : "") . "=0";

*/


if( !function_exists( "altGYMLQuery" ) )
{

  function altGYMLQuery()
  {
    global $API;

    $q = sql_query( "select distinct subcat, cat, type from pages natural join page_members where page_members.uid={$API->uid}" );

    $cats = array();
    $subcats = array();
    while( $r = mysql_fetch_array( $q ) )
    {
      if( $r['subcat'] != 0 )
        $subcats[] = $r['subcat'];
      else if( $r['type'] != PAGE_TYPE_VESSEL && $r['type'] != PAGE_TYPE_BUSINESS ) //We don't want the top level categories for companies or boats; includes too many things!
        $cats[] = $r['cat'];
    }
    $subcat_imploded = implode( ",", $subcats );
    $cat_imploded = implode( ",", $cats );

    $current_pages = array();
    $q = sql_query("select gid from pages natural join page_members where page_members.uid='{$API->uid}'");
    while( $r = mysql_fetch_array( $q ) )
      $current_pages[] = $r['gid'];
    $cg_imploded = implode( ",", $current_pages );


    $q = "select distinct gid,gname,cat,pid from pages natural join page_members where (";
    $cond = 0;
    if( strlen( $subcat_imploded ) > 0 )
    {
      $q .= " pages.subcat in ($subcat_imploded) or";
      $cond++;
    }

    if( strlen( $cat_imploded ) > 0 )
    {
      $q .= " pages.cat in ($cat_imploded) or";
      $cond++;
    }

    if( strlen( $friends_implode ) > 0 )
    {
      $q .= " page_members.uid in ($friends_implode) or";
      $cond++;
    }

    if( $cond > 0 )
      $q = substr( $q, 0, strlen( $q ) - 2 );
    else
      $q .= "1=1";
    $q .= ")";

    if( strlen( $cg_imploded ) > 0 )
      $q .= " and gid not in ($cg_imploded)";

    $q .= " order by rand()";
  return $q;
  }
}


if( $friends_implode == "" )
{
  $q = sql_query( "select distinct if(id1={$API->uid},id2,id1) as uid from friends where ({$API->uid}=id1 or {$API->uid}=id2)" );
  $friends = array();
  while( $r = mysql_fetch_array( $q ) )
    $friends[] = $r['uid'];

  $friends_implode = implode( ",",$friends );
}

switch( $o )
{
  case "r":
    $friends = $API->getFriendsOfFriends();

    shuffle( $friends );

    if( sizeof( $friends ) == 0 )
      $q = altGYMLQuery();
    else
      $q = "select pages.* from pages natural join page_members where page_members.uid in (0," . implode( ",", $friends ) . ") and pages.gid NOT IN (select gid from page_members where uid = " . $API->uid . ") order by rand()";



/*


    $friends = array();
    $x = mysql_query( "select if(id1=" . $API->uid . ",id2,id1) as uid from friends where (" . $API->uid . ") in (id1,id2) order by rand() limit 8" );

    if( mysql_num_rows( $x ) == 0 )
    {
      $q = altGYMLQuery();
    }
    else
    {
      while ($friend = mysql_fetch_array($x, MYSQL_ASSOC))
      {
        $friends[] = $friend['uid'];
      }

      foreach( $friends as $friend )
      {
        $friends_of_friend = mysql_query( "select if(id1=" . $friend . ",id2,id1) as uid from friends where (" . $friend . ") in (id1,id2) and status=1" );

        while ($result = mysql_fetch_array($friends_of_friend, MYSQL_ASSOC))
        {
          if( $result['uid'] != $API->uid && !in_array($result['uid'], $friends ) )
            $results[] = $result['uid'];

          if( sizeof( $results ) > 10 )
            break;
        }

        if( sizeof( $results ) > 10 )
          break;
      }

      $friends = array_merge( $friends, $results );

      $q = "select * from pages natural join page_members where (";
      for( $c = 0; $c < sizeof( $friends ); $c++ )
      {
        if( $c > 0 ) $q .= " OR ";
        $q .= "page_members.uid='" . $friends[$c] . "'";
      }
      $q .= ") and pages.gid not in (0";

      $q2 = mysql_query( "select pages.gid from pages natural join page_members where page_members.uid='{$API->uid}'" );

      while( $r2 = mysql_fetch_array( $q2 ) )
      {
        $q .= "," . $r2['gid'];
      }


      $q .= ") order by rand()";
    }

*/

  break;

  case "m":
    $q = "select * from pages natural join page_members where page_members.uid='{$API->uid}' order by gname";
  break;

  case "a":
    $q = "select * from pages natural join page_members where page_members.uid='{$API->uid}' and page_members.admin=1 order by gname";
  break;
}

$q .= " limit $limit";



$x = sql_query($q);

if( mysql_num_rows( $x ) == 0 && $o == "r" )
  $x = sql_query( "select distinct gid,gname,cat,pid from pages order by rand() limit $limit" );

if ($ajax)
	ob_start();



$grps = array();
$gids = array();

while ($grp = mysql_fetch_array($x, MYSQL_ASSOC))
{
	$grps[] = array("gname" => $grp['gname'], "gid" => $grp['gid']);

  if( !in_array( $grp['gid'], $gids ) )
  {
    $gids[] = $grp['gid'];

    $grp['catname'] = quickQuery( "select catname from categories where cat='" . $grp['cat'] . "'" );

    if( $friends_implode == "" ) $friends_implode="0";

    $grp['friends'] = quickQuery( "select count(*) from page_members where gid='" . $grp['gid'] . "' and uid in ($friends_implode)" );
    $grp['hash'] = quickQuery( "select hash from photos where id='" . $grp['pid'] . "'" );
  	$grp['media'] = array("id" => $grp['pid'], "type" => "P");
  	showPagePreview($grp, $showJoin);
  }
}

if ($ajax)
{
	$html = ob_get_contents();
	ob_end_clean();
	echo json_encode(array("pages" => $grps, "html" => $html));
}



?>