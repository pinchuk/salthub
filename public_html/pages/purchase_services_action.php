<?
/*
Adds items to the user's cart when they choose to add a new Page service. Called from purchase_services_popup.php
*/

include "../inc/inc.php";

$gid = $_POST['gid'];

$cats = intval( $_POST['addlcategories'] );
$inds = intval( $_POST['addlindustries'] );

include_once( "../billing/billing-functions.php" );

if( $cats > 0 )
  billingAddItemToCart( 5, $ID, $cats );

if( $inds > 0 )
  billingAddItemToCart( 4, $ID, $inds );

if( $cats == 0 && $inds == 0 )
{
  header( "Location: " . $API->getPageURL( $gid ) );
}
else
{
  header( "Location: /billing" );
}
?>
