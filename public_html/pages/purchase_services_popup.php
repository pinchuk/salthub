<?
/*
Popup presented to the user when they add new services to their account after editing the about page.
The user is asked to confirm that they intend to purchase something.  Upon confirmation, the items are
added to their cart and they're taking to the billing page.
*/

include "../inc/inc.php";

$gid = $_GET['gid'];

?>
<form action="/pages/purchase_services_action.php" method="post">
<input type="hidden" name="gid" value="<?=$gid?>" />
<div style="width: 450px; font-size:9pt;">
  <div>
    <? if( intval( $_GET['ol'] ) == 1 ) { ?>
    One category and one industry is included with your page, additional indistries and categories must be purchased before selecting.<br /><br />
    <? } ?>
    Select the number of industries and categories that you'd like to purchase.
  </div>

  <div style="margin-left: 100px; margin-top:10px;">
    <div style="float:left;">
      <select name="addlcategories">
  <? for( $c = 0; $c <= 20; $c++ ) { ?>
      <option value="<?=$c?>"><?=$c?></option>
  <? } ?>
      </select>

    </div>

    <div style="float:left; margin-left:10px;">
      <b>Additional Categories ($20.00 each)</b><br />
      <?
      $funded = quickQuery( "select categories_funded from pages where gid='$gid'" );
      $funded++;
      echo "(" . $funded . " may be selected currently)";
      ?>
    </div>
  </div>

  <div style="margin-left: 100px; padding-top:10px; clear:both;">
    <div style="float:left;">
    <select name="addlindustries">
<? for( $c = 0; $c <= 20; $c++ ) { ?>
    <option value="<?=$c?>"><?=$c?></option>
<? } ?>
    </select>

    </div>

    <div style="float:left; margin-left:10px;">
      <b>Additional Industries ($75.00 each)</b><br />
      <?
      $funded = quickQuery( "select sectors_funded from pages where gid='$gid'" );
      $funded++;
      echo "(" . $funded . " may be selected currently)";
      ?>
    </div>
  </div>

  <div style="text-align:center; padding-top:10px; clear:both;">
    <input type="submit" name="" value="Proceed to Billing" />
  </div>
</div>
</form>