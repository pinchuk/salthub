<?
/*
Popup used to send a message to the page admins.  This is used by people who are currently not logged into the site.
*/

$noLogin = true;

include_once( "../inc/inc.php" );

$gid = $_GET['gid'];
$name = $_GET['name'];
$subj = $_GET['subj'];
$msg = $_GET['msg'];
$pic = $_GET['pic'];

if( trim( $pic ) == "" ) $pic = "/images/nouser.jpg";
?>

<div style="font-size: 8pt; color: #555;">
  <div style="float: left; width: 85px;"><img src="<?=$pic?>" alt="" style="padding-bottom: 2px;" /><br /><?=$name?></div>
  <div style="float: left; padding-left: 10px; font-weight: bold;">
  <? if( !$API->isLoggedIn() ) { ?>
  <div style="height: 12pt;">Please enter your email address:</div><input type="text" id="email" maxlength="50" style="width: 285px;" value=""/>
  <? } ?>
  <div style="height: 12pt;">Subject:</div><input type="text" id="msgsubj" maxlength="50" style="width: 285px;" value="<?=$subj?>"/>
  <div style="padding-top: 5px;">
  	<div style="height: 12pt;">Message:</div><textarea id="msgbody" style="width: 285px; height: 95px;"><?=$msg?></textarea>
  </div>
  <div style="padding: 5px 0px; text-align: center; height: 16px;" id="msgbuttons">
  	<input type="button" class="button" value="Send" onclick="javascript:sendMessageToPage(<?=$gid?>);" /> &nbsp; &nbsp;
  	<input type="button" class="button" value="Cancel" onclick="javascript:closePopUp();" />
  </div>
  </div>
  <div style="clear: both;"></div>
</div>