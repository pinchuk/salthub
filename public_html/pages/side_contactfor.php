<?
/*
Displays the "Contact For" module on the left side of a page - A list of reasons why a user should contact this
page/company/org.  This is called from /profile/side.php
*/

if( $page['admin'] == 1 || $API->admin || strlen( $page['contactfor'] ) > 0 )
{
?>
<div class="subhead" style="margin-top: 10px;">
	<div style="float: left;">Skills &amp; Experience</div>
	<div style="clear: both;"></div>
</div>

<div style="font-size: 8pt;	font-family: tahoma;  margin:0px; padding:2px; " onmouseover="javascript:document.getElementById('edit-contactfor').style.visibility = 'visible';" onmouseout="javascript:document.getElementById('edit-contactfor').style.visibility = 'hidden';">
				<?php if ($page['admin'] == 1 || $API->admin) { ?>
				<div id="edit-contactfor" style="visibility: hidden; float: right; padding-right: 5px;">
					<a href="javascript:showPopupUrl('/pages/update_contactfor_popup.php?gid=<?=$gid?>');">edit</a>
				</div>
				<?php } ?>
<?
$data = explode( chr(2), $page['contactfor'] );
if( strlen( $page['contactfor'] ) == 0  ) {
?>
<div style="height:2px"></div>
<div id="contactfor">

<div style="text-align:center; font-size:8pt; width:100%; clear:both;">
  Update what this page should be contacted for
</div>

</div>
<?
}
else
{
?>

<div style="height:2px"></div>
<div id="contactfor">
<? for( $c = 0; $c < sizeof( $data ) && $c < 6; $c++ ) if( $data[$c] != "" ) { ?>
  <div style=""><img src="/images/bullet.png" width="16" height="16" alt="" style="float:left"/> <?=$data[$c]?></div>
  <div style="height:5px"></div>
  <?
  }
?>
</div>
<?
}
?>
</div>
<? } ?>