<?
/*
Displays the contact information for a Page and enables page admins to edit the contact information.
*/

if( (($page['address'] != '' || $page['fax'] != '' || $page['phone'] != '' || $page['cell'] != '' || $page['email'] != '' || $page['www'] != '' ) && ($API->isUserMember($null, $page['gid'] ) || $page['type'] == PAGE_TYPE_BUSINESS ) ) || $action == "editpage" )
{
  $data = array();

  $primary = array( "location_name" => "Primary Location", "contact_person" => $page['contact_person'], "email" => $page['email'], "www" => $page['www'], "address" => $page['address'], "phone" => $page['phone'], "cell" => $page['cell'], "fax" => $page['fax'], "verified" => $page['verified'] );
  $data[] = $primary;

  $sql = "select * from page_locations where gid='" . $page['gid'] . "'";
  $q2 = mysql_query( $sql );
  while( $r2 = mysql_fetch_array( $q2 ) )
  {
    $secondary = array( "location_name" => $r2['location_name'], "contact_person" => $r2['contact'], "email" => $r2['email'], "www" => "", "address" => $r2['address'], "phone" => $r2['phone'], "cell" => $r2['cell'], "fax" => $r2['fax'] );
    $data[] = $secondary;
  }

?>

<div class="subhead" style="margin-top: 10px;">
	<div style="float: left;">Contact Info</div>
	<div style="clear: both;"></div>
</div>

<?
for( $idx = 0; $idx < sizeof( $data ); $idx++ )
{
?>
  <div style="font-size: 8pt;	font-family: tahoma;  margin:0px; padding:2px; ">

  <? if( $idx > 0 ) { ?>
  <div style="height:1px; padding-top:5px; border-top:1px dotted rgb(0,64,128); width:85%;"></div>
  <? } ?>

  <div style="height:5px"></div>

  <? if( $idx > 0 ) { ?>
  <div id="grpedit-location_name0-<?=$idx?>" style="display:none;">Location Name:<br /><input name="location_name<?=$idx?>" type="text" style="width:140px;" id="grpedit-location_name-<?=$idx?>" value="<?=$data[$idx]['location_name']?>" /></div>
  <div class="value" id="grpedit-location_name1-<?=$idx?>" style="font-weight:bold;"><?=$data[$idx]['location_name']?></div>
  <? } ?>

  <div id="grpedit-contact_person0-<?=$idx?>" style="display: none;">Contact Person:<br /><input name="contact_person<?=$idx?>" type="text" style="width:140px;" id="grpedit-contact_person-<?=$idx?>" value="<?=$data[$idx]['contact_person']?>" /></div>
  <div class="value" id="grpedit-contact_person1-<?=$idx?>" style="font-weight:bold;"><?=$data[$idx]['contact_person']?></div>
  <? if( isset( $data[$idx]['contact_person'] ) && $data[$idx]['contact_person'] != ""  ) { ?><div style="height:5px"></div><? } ?>

  <?
    $contact_uid = 0;

    if( $data[$idx]['contact_person'] != "" ) {
    $q2 = mysql_query( "select users.uid, users.name, users.pic from page_members inner join users on users.uid=page_members.uid where users.name='" . $data[$idx]['contact_person'] . "' limit 1" );
    if( $r2 = mysql_fetch_array( $q2 ) )
      $contact_uid = $r2['uid'];
    }

    if( $contact_uid > 0 || $data[$idx]['email'] != "" && $idx == 0 ) {
  ?>
  <div class="value" id="grpedit-email1-<?=$idx?>">
  <div style="width:170px; margin-left:-2px; background-color:#f7f7f7; margin-top:4px; padding:4px; border:1px; border-style:solid; border-color:#aaaaaa; margin-bottom:5px;">
  <?
  if( $contact_uid > 0 && $API->isLoggedIn() ) { ?>
    <div class="caption" style="font-size:8pt; margin-top:2px; margin-left:5px; height:16px; background-image: url(/images/add_connection_green.png);"><?=friendLink($r2['uid'])?></div>
    <div style="clear:both;"></div>
  <?
  }

  if( $data[$idx]['email'] != "" || $contact_uid > 0 && $idx == 0) {
    $name = $r2['name'];
    if( $name == "" ) $name = quickQuery( "select contact_person from pages where gid='$gid'" );

    if( $contact_uid > 0 && $API->isLoggedIn()) {
  ?>
      <div class="caption" style="font-size:8pt; margin-top:2px; margin-bottom:0px; margin-left:5px; background-image: url(/images/email_add.png); height:16px;"><a href="javascript:void(0);" onclick="javascript:openSendMessagePopup('','', '<?=$r2['name']?>', <?=$r2['pic'] ?>,0,<?=$page['gid']?>,'');">Send message</a></div>
  <? } else { ?>


      <div class="caption" style="font-size:8pt; margin-top:2px; margin-bottom:0px; margin-left:5px; background-image: url(/images/email_add.png); height:16px;"><a href="javascript:void(0);" onclick="javascript:openSendMessagePopup('','','<?=$name?>','', 0, <?=$page['gid']?>,'');">Send message</a></div>
  <? } ?>
    <div style="clear:both;"></div>
  <? } ?>
  </div>
  </div>
  <? }
  ?>

  <div id="grpedit-email0-<?=$idx?>" style="display: none;">E-mail:<br /><input name="email<?=$idx?>" type="text" style="width:140px;" id="grpedit-email-<?=$idx?>" value="<?=$data[$idx]['email']?>" /></div>
  <? /*
  <? if( isset( $data[$idx]['email'] ) && $data[$idx]['email'] != ""  ) { echo findLinks($data[$idx]['email']);  } ?></div>
  */
  ?>

  <? if( $idx == 0 ) { ?>
  <div id="grpedit-www0-<?=$idx?>" style="display: none; padding-top: 5px;">Website:<br /><input name="www<?=$idx?>" type="text" style="width:140px;" id="grpedit-www-<?=$idx?>" value="<?=$data[$idx]['www']?>" <? if( $data[$idx]['verified'] ) { ?>readonly onclick="showPopUp('This page is a verified business or service', '<br /><br />To change the website address, please notify <?=$siteName?><br /> using the feedback tool on the left.<br /><br /><br />');"<? } ?>/></div>
  <div class="value" id="grpedit-www1-<?=$idx?>"><? if( isset( $data[$idx]['www'] ) && $data[$idx]['www'] != ""  ) { echo findLinks($data[$idx]['www']); } ?></div>
  <? } ?>
  <div style="height:5px"></div>

  <div id="grpedit-address0-<?=$idx?>" style="display: none; padding-top: 5px;">Address:<br /><textarea name="address<?=$idx?>" id="grpedit-address-<?=$idx?>" style="width:140px;"><?=$data[$idx]['address']?></textarea></div>
  <div class="value" id="grpedit-address1-<?=$idx?>"><?if( isset( $data[$idx]['address'] ) && $data[$idx]['address'] != ""  ) { echo str_replace("\n", "<br />", $data[$idx]['address']); }?></div>

  <div style="height:5px"></div>

  <div id="grpedit-phone0-<?=$idx?>" style="display: none; padding-top: 5px;">Phone:<br />+<input name="phone<?=$idx?>" type="text" style="width:130px;" id="grpedit-phone-<?=$idx?>" value="<?=$data[$idx]['phone']?>" /></div>
  <div class="value" id="grpedit-phone1-<?=$idx?>"><? if( isset( $data[$idx]['phone'] ) && $data[$idx]['phone'] != ""  ) { ?>Tel: +<?=$data[$idx]['phone']; } ?></div>

  <div id="grpedit-cell0-<?=$idx?>" style="display: none; padding-top: 5px;">Cell:<br />+<input name="cell<?=$idx?>" type="text" style="width:130px;" id="grpedit-cell-<?=$idx?>" value="<?=$data[$idx]['cell']?>" /></div>
  <div class="value" id="grpedit-cell1-<?=$idx?>"><? if( isset( $data[$idx]['cell'] ) && $data[$idx]['cell'] != ""  ) { ?>Cell: +<?=$data[$idx]['cell']; } ?></div>

  <div id="grpedit-fax0-<?=$idx?>" style="display: none; padding-top: 5px;">Fax:<br />+<input name="fax<?=$idx?>" type="text" style="width:130px;" id="grpedit-fax-<?=$idx?>" value="<?=$data[$idx]['fax']?>" /></div>
  <div class="value" id="grpedit-fax1-<?=$idx?>"><? if( isset( $data[$idx]['fax'] ) && $data[$idx]['fax'] != "" ) { ?>Fax: +<?=$data[$idx]['fax']; } ?></div>

  <?
  if( $data[$idx]['address'] != "" && $action == "about" && $idx == 0) {
  ?>
  <div class="caption" style="font-size:8pt; margin-top:7px; height:16px; background-image: url(/images/map.png);"><a href="#map" style="font-size:8pt; padding-top:4px;">view map</a></div>
  <div style="clear:both;"></div>
  <? } ?>
  </div>
<?
}
?>
<div id="grpedit-NewLocations">

</div>

<div style="clear:both; display: none; width:100%; text-align:center; font-size: 8pt;" id="grpedit-addAnotherLocation">
  <a href="javascript:void(0);" onclick="javascript: addAnotherLocation();">add another location</a>
</div>

<script language="javascript" type="text/javascript">
<!--
var num_locations = <?=sizeof($data);?>;
-->
</script>


<?
}
?>


