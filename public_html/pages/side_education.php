<?php
/*
Lists the schools that a user has attended.  I don't think this is currently being used on the site.
*/

$present = -1;

$x = sql_query(
	"select u.uid,username,name,pic,start,stop,@present:=if(stop>year(now()),1,0) as present from education e
		inner join users u on u.uid=e.uid
		where school={$page['gid']}
		order by present,name
	");

?>

<div class="subhead" style="margin-top: 10px;">
	<div style="float: left;">Attended <span class="number">(<?=mysql_num_rows($x)?>)</span></div>
	<div style="float: right; font-size: 8pt; font-weight: normal; padding: 2px 5px 0 0;"><a href="javascript:void(0);" onclick="javascript:;">view all</a></div>
	<div style="clear: both;"></div>
</div>

<?php

while ($y = mysql_fetch_array($x, MYSQL_ASSOC))
{
	if ($y['present'] != $present)
	{
		$present = $y['present'];
		
		echo '<div style="clear: both; padding-top: 5px; font-weight: bold; font-size: 8pt;">';
		
		if ($present == 1)
			echo 'Currently attending';
		else
			echo 'Alumni';
		
		echo '</div>';
	}
	
	$profileURL = $API->getProfileURL($y['uid'], $y['username']);
	
	?>
	
	<div class="notification">
		<div class="pic">
			<a href="<?=$profileURL?>">
				<img src="<?=$API->getThumbURL(1, 48, 48, $API->getUserPic($y['uid'], $y['pic']))?>" alt="" />
			</a>
		</div>
		<div class="text">
			<a href="<?=$profileURL?>"><?=$y['name']?></a><br />
			<?=$y['start'] . "-" . ($present == 0 ? $y['stop'] : "Present")?>
		</div>
		<div class="clear"></div>
	</div>
	
	<?php
}
?>

<div style="clear: both; height: 5px;"></div>