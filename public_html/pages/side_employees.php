<?php
/*
Lists the employees of a business and vessels pages on the left side menu.
*/

$present = -1;

$x = sql_query(
	"select u.uid,username,name,pic,gname as occupation,@present:=if(stop=0,1,0) as present from work w
		inner join users u on w.uid=u.uid
		inner join pages g on g.gid=w.occupation
		where w.employer={$page['gid']}
		order by present desc,name
	");

?>

<div class="subhead parent_view_all" style="margin-top: 10px;">
	<div style="float: left;">Employees <span class="number">(<?=mysql_num_rows($x)?>)</span></div>
	<div class="view_all" style="float: right; font-size: 8pt; font-weight: normal; padding: 2px 5px 0 0;"><? if( $API->isLoggedIn() ) { ?><a href="javascript:void(0);" onclick="javascript:showPageMembers(<?= $page['gid'] ?>);">view all</a><? } ?></div>
	<div style="clear: both;"></div>
</div>

<?php

while ($y = mysql_fetch_array($x, MYSQL_ASSOC))
{
	if ($y['present'] != $present)
	{
		$present = $y['present'];

		echo '<div style="clear: both; padding-top: 5px; font-weight: bold; font-size: 8pt;">';

		if ($present == 1)
			echo 'Currently work here as';
		else
			echo 'Used to work here as';

		echo '</div>';
	}
	
	$profileURL = $API->getProfileURL($y['uid'], $y['username']);

	?>

	<div class="notification">
		<div class="pic">
      <? if( $loggedIn ) { ?>
			<a href="<?=$profileURL?>">
      <? } else { ?>
			<a href="javascript:void(0);" onclick="javascript:getStarted3(); return false;">
      <? } ?>
				<img src="<?=$API->getThumbURL(1, 48, 48, $API->getUserPic($y['uid'], $y['pic']))?>" alt="" />
			</a>
		</div>
		<div class="text">
      <? if( $loggedIn ) { ?>
			<a href="<?=$profileURL?>">
      <? } else { ?>
			<a href="javascript:void(0);" onclick="javascript:getStarted3(); return false;">
      <? } ?>
      <?=$y['name']?></a><br />
			<?=$y['occupation']?>
		</div>
		<div class="clear"></div>
	</div>
	
	<?php
}
?>

<div style="clear: both; height: 5px;"></div>