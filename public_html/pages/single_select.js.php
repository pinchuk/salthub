<?
function showSingleSelectDropDown_new($name, $w, $h, $tid)
{
	echo '<select name="' . $name. '" style="width: ' . $width . 'px; height: ' . $height . 'px;">';
	
	switch( $tid )
	{
		case 101: //Categories
			$sql = "select cat as type_id, catname as name, heading from categories where industry IN ( SELECT cat from categories where industry='" . PAGE_TYPE_BUSINESS . "') order by heading, catname";
			$q = mysql_query( $sql );

			$currentHeader = 0;
			while ($item = mysql_fetch_array($q, MYSQL_ASSOC))
			{
				if( $item['heading'] > 0 && ($currentHeader != $item['heading'] ) )
				{
					if ($currentHeader > 0)
						echo '</optgroup><optgroup></optgroup>';
					
					echo '<optgroup label="';
					echo quickQuery('select name from product_and_service_types where id=' . $item['heading']);
					echo '">';
					
					$currentHeader = $item['heading'];
				}
				
				echo '<option value="' . $item['type_id'] . '">' . $item['name'] . '</option>';
			}
			
			echo '</optgroup>';
			break;

		default:
		echo "Invalid tid"; break;
	}
	
	echo '</select>';
}

function showSingleSelectDropDown( $formName, $width, $height, $tid )
{
?>
<div style="width:<?=$width?>px; padding-right: 10px; float: left;">
	<div style="width:<?=$width?>px; float:left;">
	  <div style="width:<?=$width?>px; height:<?=$height?>px; border: 1px solid #555; position: relative; float:left; background-color:#fff;">
		  <input onclick="javascript:toggleSingleItemChooser(<?=$tid?>);" id="itemchoosertext-<?=$tid?>" type="text" READONLY style="cursor: pointer; margin: 0; padding: 0px 2px; border: 0; width:<?=$width - 24?>px; height: <?=$height?>px;">
      <img src="/images/dropdown.png" onclick="javascript:toggleSingleItemChooser(<?=$tid?>);" style="cursor: pointer; vertical-align: middle;" alt="" />

		  <div id="itemchooser-<?=$tid?>" class="itemchooser" style="width:233px;">
			  <div id="chooser-<?=$tid?>" class="chooser" style="width:227px;">
          <?getSingleSelectionContent( $formName, $tid );?>
        </div>
		  </div>
	  </div>
	</div>
  <div style="margin-bottom:10px;"></div>
</div>

<div style="clear: both;"></div>
<?
}

function getSingleSelectionContent( $name, $tid )
{

  switch( $tid )
  {
    case 101: //Categories
    {
      $sql = "select cat as type_id, catname as name, heading from categories where industry IN ( SELECT cat from categories where industry='" . PAGE_TYPE_BUSINESS . "') order by heading, catname";
      $q = mysql_query( $sql );

      $currentHeader = 0;
      while ($item = mysql_fetch_array($q, MYSQL_ASSOC))
      {
        if( $item['heading'] > 0 && ($currentHeader != $item['heading'] ) )
        {
          echo "<div style=\"font-size:9pt; font-weight:bold; margin-top:10px;\">";
          echo quickQuery( "select name from product_and_service_types where id=" . $item['heading'] );
          echo "</div>";
          $currentHeader = $item['heading'];
        }
      ?>
        <div style="margin-top:5px; margin-bottom:5px; margin-left:0px; line-height:200%;">
          <div style="float:left;">
            <input type="radio" style="width:22px; display:block;" name="<?=$name?>" value="<?=$item['type_id'];?>" onclick="javascript: selectSingleItem( <?=$tid?>, <?=$item['type_id'];?> );"/>
          </div>
          <div style="float:left; padding-top:2px;" id="textvalue-<?=$item['type_id'];?>"><?=$item['name']; ?></div>
          <div style="clear:both;"></div>
        </div>
      <?
      }
    }
    break;

    default: echo "Invalid tid"; break;

  }
}

?>

<script type="text/javascript">
function toggleSingleItemChooser(tidEditing)
{
	e = document.getElementById("itemchooser-" + tidEditing);
	isShown = e.style.display == "inline";

	e.style.display = isShown ? "none" : "inline";
}

function selectSingleItem( tidEditing, value )
{
	e = document.getElementById("itemchoosertext-" + tidEditing);
  f = document.getElementById("textvalue-" + value);
  if( e && f )
  {
    e.value = htmlspecialchars_decode( f.innerHTML );
  }

  toggleSingleItemChooser( tidEditing );
}

</script>