<?
/*
Used to dynamically reload the sub category selections on about-edit.php (About page editing).

Whenever a category selection changes on a non-business page, this page is called to get the new subcategories.
*/

include_once( "../inc/inc.php" );

if( empty( $page ) )
{
  $gid = $_GET['gid'];
  $cat = $_GET['cat'];

  $page = queryArray( "select cat,subcat,type from pages where gid='$gid'" );

  if( $page['cat'] != $cat )
  {
    $page['cat'] = $cat;
    $page['subcat'] = 0;
  }
}

$x = sql_query("select cat,catname from categories where cat IN (SELECT child FROM category_relationships where parent='" . $page['cat'] . "') order by catname");

if( mysql_num_rows( $x ) > 0 )
{
  $subcatname = quickQuery( "select catname from categories where cat='" . $page['subcat'] . "'" );
?>
	<div class="left"><? echo getTitle( $page['type'] ); ?>:</div>
	<div class="value" id="grpedit-subcat0">
		<select id="grpedit-subcat" name="subcat">
		<?php
		while ($cat = mysql_fetch_array($x))
			echo '<option ' . ($cat['cat'] == $page['subcat'] ? 'selected' : '') . ' value="' . $cat['cat'] . '">' . $cat['catname'] . '</option>';
		?>
		</select>
	</div>
<?
}

function getTitle( $type )
{
  switch( $type )
  {
    case PAGE_TYPE_BUSINESS: return "Product / Service";
    case PAGE_TYPE_VESSEL: return "Vessel Type";
    default:
      return "Subcategory";
  }
}
?>