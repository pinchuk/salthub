<?php
/*
This is not currently being used.  Was used to save about page information, currently being done by about-edit-save.php

Candidate for deletion
*/

include "../inc/inc.php";

function json_decode_nice($json, $assoc = FALSE){
    $json = str_replace(array("\n","\r"),"",$json);
    $json = preg_replace('/([{,])(\s*)([^"]+?)\s*:/','$1"$3":',$json);
    return json_decode($json,$assoc);
}

$data = json_decode_nice( $_POST['json_update'], true);


if( isset( $_POST['pid'] ) ) $data['pid'] = $_POST['pid'];
if( isset( $_POST['gid'] ) ) $data['gid'] = $_POST['gid'];

$gid = intval($data['gid']);

if (quickQuery("select admin from page_members where uid={$API->uid} and gid=$gid") == 0 && $API->admin == 0)
	die();

$cols = array('type', 'descr', 'cat',  'privacy', 'pid', 'products', 'services', 'subcat', 'phone0', 'email0', 'www0', 'address0', 'cell0', 'fax0', 'contact_person0');

if( $API->admin ) $cols[] = 'gname'; //Only the admin can edit the page name

//echo $_POST['json_update'];


if( isset( $data ) )
foreach ($data as $col => $val)
	{
    if( !in_array( $col, $cols ) ) continue;

    $val = urldecode( $val );
    $val = str_replace( "%u2019", "'", $val );
    $val = str_replace( "%u2013", "-", $val );
    $val = addslashes( unescape( $val ) );

    if( substr( $col, -1 ) == "0" )
      $col = substr( $col, 0, -1 );

		$update[] = "$col = \"" .$val . "\"";

		if ($col == "pid")
    {
//In case we want to delete the link from to the previous profile image
      $old_pid = quickQuery( "select pid from pages where gid='$gid'" );
      if( isset( $old_pid ) )
          sql_query( "delete from page_media where gid='$gid' and id='$old_pid' and type='P' limit 1" );   //Changing profile pictures
		  sql_query("insert into page_media (type,id,gid) values ('P', " . intval($val) . ", $gid)");
    }
	}


$cols = array( 'location_name', 'phone', 'email', 'www', 'address', 'cell', 'fax', 'contact_person');

for( $c = 0; $c < sizeof( $cols ); $c++ )
{


}



$data = json_decode($_POST['json_update'], true);
mysql_query("SET character_set_results = 'utf8', character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'");

switch( $data['cat'] )
{
  case 101: $gtype = 'L'; break;
  case 111: $gtype = 'C'; $API->setToDoItem( TODO_COMPANY_PAGE ); break;
  case 112:
  {
    $bid = quickQuery( "select id from boats where gid='$gid'" );

    if( $bid == 0 )
    {
      sql_query( "insert into boats (name,gid) values ('" . addslashes( unescape( urldecode( $data['gname'] ) ) ) . "', $gid)" );
      $bid = mysql_insert_id();
      $update[] = "glink=\"$bid\"";
    }

    $gtype = 'B';

    $update2 = array();
    $cols = array('exnames','year','shipyard','hullid','imo','mmsi','callsign','flag','navalarchitect','stylist','decorator','sistership','refit','length','beam','draught','hullsuperstr','engine','speedmax','speedcruise','rangenm','fuelltr','waterltr', 'guests', 'crew' );

    foreach ($data as $col => $val)
    {
      if( !in_array( $col, $cols ) ) continue;
      $val = urldecode( $val );
      $val = str_replace( "%u2019", "'", $val );
      $val = str_replace( "%u2013", "-", $val );
      $val = addslashes( unescape( $val ) );
      $update2[] = "$col = \"" . addslashes(htmlentities($val)) . "\"";
    }

    sql_query("update boats set " . implode(",", $update2) . " where gid='$gid' limit 1");
    //echo "update boats set " . implode(",", $update2) . " where gid='$gid' limit 1";
    //echo mysql_error();

  }
  break;

  case 113: $gtype = 'S'; break;
  case 102:
  case 103:
  case 105:
  case 107:
    $gtype = 'P';
  break;
  case 106:
    $gtype = 'O';
  break;
  default: $gtype = ''; break;
}

mysql_query("update pages set " . implode(",", $update) . ",gtype='$gtype' where gid=$gid");

//echo "update pages set " . implode(",", $update) . ",gtype='$gtype' where gid=$gid";
//echo mysql_error();






function code2utf($num){
  if($num<128)
    return chr($num);
  if($num<1024)
    return chr(($num>>6)+192).chr(($num&63)+128);
  if($num<32768)
    return chr(($num>>12)+224).chr((($num>>6)&63)+128)
          .chr(($num&63)+128);
  if($num<2097152)
    return chr(($num>>18)+240).chr((($num>>12)&63)+128)
          .chr((($num>>6)&63)+128).chr(($num&63)+128);
  return '';
}

function unescape($strIn, $iconv_to = 'UTF-8') {
  $strOut = '';
  $iPos = 0;
  $len = strlen ($strIn);
  while ($iPos < $len) {
    $charAt = substr ($strIn, $iPos, 1);
    if ($charAt == '%') {
      $iPos++;
      $charAt = substr ($strIn, $iPos, 1);
      if ($charAt == 'u') {
        // Unicode character
        $iPos++;
        $unicodeHexVal = substr ($strIn, $iPos, 4);
        $unicode = hexdec ($unicodeHexVal);
        $strOut .= code2utf($unicode);
        $iPos += 4;
      }
      else {
        // Escaped ascii character
        $hexVal = substr ($strIn, $iPos, 2);
        if (hexdec($hexVal) > 127) {
          // Convert to Unicode
          $strOut .= code2utf(hexdec ($hexVal));
        }
        else {
          $strOut .= chr (hexdec ($hexVal));
        }
        $iPos += 2;
      }
    }
    else {
      $strOut .= $charAt;
      $iPos++;
    }
  }
  if ($iconv_to != "UTF-8") {
    $strOut = iconv("UTF-8", $iconv_to, $strOut);
  }
  return $strOut;
}
?>




