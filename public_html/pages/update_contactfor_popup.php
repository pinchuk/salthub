<?
/*
Popup that enables the user to edit the "contact for" section of a Page.
*/

require( "../inc/inc.php" );

$gid = $_GET['gid'];
?>

<div style="font-size: 9pt; padding-bottom: 10px; text-align:center;">
<b>Update what this page should be contacted for</b><br />
<div style="text-align:center;"><div style="float:left; margin-left:30px;"><img src="/images/exclamation2.png" width="16" height="16" alt=""/></div><div style="float:left; font-weight:300;">&nbsp;&nbsp;doing so will increase your chance of being found in search</div></div>
<p />
<?
  $data = explode( chr(2), quickQuery( "select contactfor from pages where gid='$gid'" ) );
?>

<div style="text-align: left;">
<?
for( $c = 0; $c < 6; $c++ )
{
?>
	<div class="embedlabel"><div style="padding-top: 3px;"><? echo $c+1 ?>:</div></div>
	<div class="embedinput" style="height: 16px; font-weight: normal;">
		<input id="contactfor-<? echo $c ?>" value="<? if( $data[$c] != "" ) echo $data[$c]; ?>" type="text" size="35" maxlength="40">
</div>
<? } ?>
</div>
<div style="clear: both; height: 10px;"></div>

</div>
<div style="text-align:center;">
  <input type="button" class="button" value="Save" onclick="javascript:doUpdatePageInfo();" /> &nbsp; &nbsp; <input type="button" class="button" value="Cancel" onclick="javascript:closePopUp();" />
</div>