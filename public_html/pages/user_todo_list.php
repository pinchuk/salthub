<?
if (isset($API))
{
	$isAjax = false;
  getToDoList();
}
else
{
	include_once "../inc/inc.php";
	include_once "../inc/mod_comments.php";
	$isAjax = true;

	foreach ($_GET as $k => $v)
		$params[$k] = intval($v);

	ob_start();
	$return = getToDoList($params);
	$return['html'] = ob_get_contents();
	ob_end_clean();

	echo json_encode($return);
}


function getToDoList($params = array())
{
	global $API, $siteName, $gid;

?>
<div class="todo_list">
  <div class="section">
    <div class="title">
      Connect with twitter and facebook
    </div>

    <div class="todocontent">
      <div>
        <? if ($API->fbid) { ?>
          <img src="/images/check.png" width="16" height="16" alt="" />
        <? } else { ?>
          <img src="/images/bullet.png" width="16" height="16" alt="" />
        <? } ?>
        <img src="/images/facebook.png" alt="" />&nbsp; <?=$API->fbid ? '<a href="http://www.facebook.com/" target="_blank">view' : '<a href="/settings"><span style="color: #339900;">enable</span>'?></a>
      </div>

      <div>
        <? if ($API->twid) { ?>
          <img src="/images/check.png" width="16" height="16" alt="" />
        <? } else { ?>
          <img src="/images/bullet.png" width="16" height="16" alt="" />
        <? } ?>
        <img src="/images/twitter.png" alt="" />&nbsp; <?=$API->twid ? '<a href="http://www.twitter.com/" target="_blank">view' : '<a href="/settings"><span style="color: #339900;">enable</span>'?></a>
      </div>
    </div>

    <div class="content_text">
      <br />add spontaneously connects you blah blah
    </div>
  </div>

  <div class="divider"><hr /></div>

  <div class="section">
    <div class="title">
      Enhance your profile
    </div>

    <div class="todocontent">
      <div>
        <? if (!$API->getToDoItem(TODO_PROFILE) ) { ?>
          <img src="/images/bullet.png" width="16" height="16" alt="" />
        <? } else { ?>
          <img src="/images/check.png" width="16" height="16" alt="" />
        <? } ?>
        <a href="<? echo $API->getProfileURL(); ?>/about">Complete your profile info</a>
      </div>

      <div>
        <? if (!$API->getToDoItem(TODO_NOTIFICATIONS) ) { ?>
          <img src="/images/bullet.png" width="16" height="16" alt="" />
        <? } else { ?>
          <img src="/images/check.png" width="16" height="16" alt="" />
        <? } ?>
        <a href="/settings/notifications.php">Add what you can be contacted for</a>
      </div>

      <div>
        <? if (!$API->getToDoItem(TODO_CONTACT_INFO) ) { ?>
          <img src="/images/bullet.png" width="16" height="16" alt="" />
        <? } else { ?>
          <img src="/images/check.png" width="16" height="16" alt="" />
        <? } ?>
        <a href="<? echo $API->getProfileURL(); ?>/contactinfo">Update your contact info</a>
      </div>
    </div>

    <div class="content_text">
      <div><img src="/images/profile_large.png" width="40" height="50" alt="" style="float:left; margin-right:5px;"/>
      (recommended)<br />This is the most important step you can take on <? echo $siteName ?>.
      </div>
    </div>
  </div>

  <div class="divider"><hr /></div>

  <div class="section">
    <div class="title">
      Get connected
    </div>

    <div class="todocontent">
      <div>
        <? if (!$API->getToDoItem(TODO_INVITE_CONTACTS) ) { ?>
          <img src="/images/bullet.png" width="16" height="16" alt="" />
        <? } else { ?>
          <img src="/images/check.png" width="16" height="16" alt="" />
        <? } ?>
        <a href="/invite.php">Invite Contacts</a>
      </div>

      <div>
        <? if (!$API->getToDoItem(TODO_FIND_CONTACTS) ) { ?>
          <img src="/images/bullet.png" width="16" height="16" alt="" />
        <? } else { ?>
          <img src="/images/check.png" width="16" height="16" alt="" />
        <? } ?>
        <a href="/findpeople.php">Find contacts already on <? echo $siteName ?></a>
      </div>

    </div>

    <div class="content_text">
      <div style="float:left; color:#326798;" class="sites">
      	<div><img src="/images/facebook.png" alt="" /> Facebook</div>
      	<div><img src="/images/twitter.png" alt="" /> Twitter</div>
      	<div><img src="/images/hotmail.png" alt="" /> Hotmail</div>
      </div>
      <div style="float:left; padding-left:30px; color:#326798;" class="sites">
  	    <div><img src="/images/msn.png" alt="" /> MSN</div>
      	<div><img src="/images/yahoo.png" alt="" /> Yahoo</div>
      	<div><img src="/images/gmail.png" alt="" /> Gmail</div>
      </div>
    </div>
  </div>

  <div class="divider"><hr /></div>

  <div class="section">
    <div class="title">
      Connect with groups
    </div>

    <div class="todocontent">
      <div>
        <? if (!$API->getToDoItem(TODO_COMPANY_PAGE) ) { ?>
          <img src="/images/bullet.png" width="16" height="16" alt="" />
        <? } else { ?>
          <img src="/images/check.png" width="16" height="16" alt="" />
        <? } ?>
        <a href="/groups/claim_company.php">Claim or create your company page</a>
      </div>

      <div>
        <? if (!$API->getToDoItem(TODO_CREATE_GROUP) ) { ?>
          <img src="/images/bullet.png" width="16" height="16" alt="" />
        <? } else { ?>
          <img src="/images/check.png" width="16" height="16" alt="" />
        <? } ?>
        <a href="javascript:void(0);" onclick="javascript:showCreateGroup();">Create your own group</a>
      </div>

      <div>
        <? if (!$API->getToDoItem(TODO_EXPLORE_GROUPS) ) { ?>
          <img src="/images/bullet.png" width="16" height="16" alt="" />
        <? } else { ?>
          <img src="/images/check.png" width="16" height="16" alt="" />
        <? } ?>
        <a href="/groups">Explore groups</a>
      </div>

    </div>

    <div class="content_text">
      Vessels, companies, occupations and all sorts of items people have in common are sorted into groups.
    </div>

  </div>

  <div class="divider"><hr /></div>

  <div class="section">
    <div class="title">
      Use <? echo $siteName ?> with your phone
    </div>

    <div class="todocontent">
      <div>
        <? if (!$API->getToDoItem(TODO_CONFIG_MOBILE_UPLOAD) ) { ?>
          <img src="/images/bullet.png" width="16" height="16" alt="" />
        <? } else { ?>
          <img src="/images/check.png" width="16" height="16" alt="" />
        <? } ?>
      <a href="/settings">Configure mobile upload</a>
      </div>
    </div>

    <div class="content_text">
      <div style="float:left;">
        <img src="../images/iphone.png" width="57" height="64" alt="" />
      </div>
      <div style="float:left;">
      No app required!<br />
      Share media on the go from your phone in real time, using e-mail!
      </div>
    </div>
  </div>

  <div style="clear:both"></div>
</div>
<?
}
?>