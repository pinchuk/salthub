<?
/*
Displays a vessel's navigation history.  Only shown on Vessel pages.
*/

include_once '../inc/inc.php';

$limit = 5;
$inline = false;

if( isset( $_GET['limit'] ) )
{
  $olderthan = $_GET['olderthan'];
  $gid = intval($_GET['gid']);
  $limit = intval($_GET['limit']);

  $sql = "select * from boats_ais_history where gid='" . $gid . "' and last_update<'$olderthan' order by last_update desc limit " . $limit;
}
else
{
  $inline = true;
  
  ?>
  
<div style="width:100%; font-size:9pt; margin-top:5px;" id="vesselhistory">
  <div style="font-weight:bold;">
    <div style="float:left; width:110px; font-size:9pt; color:rgb(33,33,33);">Date/Time (UTC)</div>
    <div style="float:left; width:110px; font-size:9pt; color:rgb(33,33,33); text-align:center;">Fix</div>
    <div style="float:left; width:120px; font-size:9pt; color:rgb(33,33,33); text-align:center;">Position</div>
    <div style="float:left; width:50px; font-size:9pt; color:rgb(33,33,33); text-align:right;">Course</div>
    <div style="float:left; width:50px; font-size:9pt; color:rgb(33,33,33); text-align:right;">Kts</div>
    <div style="float:left; width:100px; font-size:9pt; color:rgb(33,33,33); text-align:center;">Status</div>
  </div>
  
  <?php
  $sql = "select * from boats_ais_history where gid='" . $gid . "' order by last_update desc limit " . $limit;
}


if( !$inline )
{
  ob_start();
}

$history_q = mysql_query( $sql );

if( mysql_num_rows( $history_q ) > 0 )
{    

    while( $hr = mysql_fetch_array( $history_q ) )
    {
      $time = strtotime( $hr['last_update'] );
?>
    <div>
      <div style="float:left; font-size:8pt; width:110px;"><?=date("Y-m-d", $time ) . " / " . date('H:i', $time );?></div>
      <div style="float:left; font-size:8pt; width:110px; text-align:center;"><?=$hr['dest'];?></div>
      <div style="float:left; font-size:8pt; width:120px; text-align:center;">
        <? if( $API->isLoggedIn() ) { ?>
          <a href="javascript:void(0);" onclick="javascript:openVesselMap(<?=$hr['gid'];?>,<?=$hr['id'];?>,1);"><?=number_format($hr['lat'], 5);?> / <?=number_format($hr['lon'], 5) ?></a>
        <? } else { ?>
          <a href="javascript:void(0);" onclick="javascript:getStarted3(); return false;" style="color:#ff8040;">Login to view</a>
        <? } ?>
      </div>
      <div style="float:left; font-size:8pt; width:50px; text-align:right;"><?=$hr['cog'] ?></div>
      <div style="float:left; font-size:8pt; width:50px; text-align:right;"><?=$hr['sog'] ?></div>
      <div style="float:left; font-size:8pt; width:100px; text-align:center;">
        <?
        switch( $hr['navstat'] )
        {
          case 0:
          case 8:
            echo "Underway";
          break;
          case 5:
            echo "Moored";
          break;
          default:
            echo "Anchored";
        }
        ?>
     </div>
     <div style="clear:both;"></div>
    </div>
<?
      $olderthan = $hr['last_update'];
    }
  }


if( $inline )
	echo '</div>';
else
{
  $html = ob_get_contents();
  ob_end_clean();
  header('Content-Type: application/json');
  echo json_encode( array( "id" => $olderthan, "html" => $html ) );
}

?>