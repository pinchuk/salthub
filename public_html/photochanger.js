var pcPhotos = [];
var pcPhotos = [];
var pcCurPage = 0;
var pcPerPage = 9;
var pcPhotoWidth = 59;
var pcTween;
var pcMore = 1;
var pcCacheReady = false;
var pcswfu;

html  = '<div id="photochanger" class="photochanger">';
html +=	'	<div class="title">';
html += '		<div style="float: left; padding-top: 2px;">Change profile photo:&nbsp; <a href="javascript:void(0);" onclick="javascript:slideup(\'photochanger\'); e=document.getElementById(\'pccontainer_show\'); if( e ) e.style.display=\'\';">close</a></div>';
html += '		<div id="pcuploadlinkcontainer" style="float: right; padding-right: 20px;"><span id="pcuploadlink">wait &#0133;</span></div>';
html += '		<div id="pcprogresscontainer" class="progress">';
html +=	'			<div id="pcprogress" style="height: 3px; width: 0px; background: #C0D9EC; margin-top: 5px; overflow: hidden;">&nbsp;</div>';
html +=	'		</div>';
html += '	</div>';
html += '	<div style="clear: both;"></div>';
html += '	<div class="arrow" id="pcprev" style="visibility: hidden;"><a href="javascript:void(0);" onclick="javascript:pcPage(false);"><img src="/images/arrow-left.png" alt="" /></a></div>';
html += '	<div class="pcpcontainer"><div id="pcphotos" class="pcphotos" style="width: 0;"></div></div>';
html += '	<div class="arrow" id="pcnext" style="text-align: right;"><a href="javascript:void(0);" onclick="javascript:pcPage(true);"><img src="/images/arrow-right.png" alt="" /></a></div>';
html += '</div>';

document.getElementById("pccontainer").innerHTML = html;
pcInitUpload();

// start new jquery code 2-12-14

function pcInitUpload()
{
	$('#photochanger .title div')
		.css('position', 'relative')
		.css('height', '1em')
		.css('line-height', '1em')
		.css('padding-top', '0')
		.css('overflow', 'hidden');
	
	$('#pcuploadlinkcontainer')
		.css('text-align', 'right')
		.html('<a href="javascript:void(0);" onclick="javascript:$(this).parent().find(\'.browse\').click();" class="upload">upload new photo</a><div style="position: relative; top: 3em;"><input class="browse" type="file" name="Filedata"></div>');
	
	$('#pcuploadlinkcontainer .browse').fileupload({
			url: '/upload/photo.php?pc=1&json=1',
			dataType: 'json',
			send: function (e, data)
			{
				$('#pcuploadlinkcontainer').html('starting upload &#0133;');
			},
			always: function (e, data)
			{
				pcInitUpload();
				
				if (typeof data.result === 'undefined')
					alert('Invalid photo uploaded');
				else
					toggleUpload(false, data.result);
			},
			progressall: function (e, data)
			{
				if (data.loaded == data.total)
					html = 'please wait &#0133;';
				else
					html = 'uploading (' + (data.loaded / data.total * 100).toFixed(0) + '%) &#0133;';
				
				$('#pcuploadlinkcontainer').html(html);
			}
		});
}

// end new jquery code 2-12-14

/*
function pcInitUpload()
{
	delete(pcswfu);

	pcswfu = new SWFUpload({
	debug: false,
	file_upload_limit : 0,
	file_queue_limit : 1,

	file_types : "*.jpg; *.jpeg; *.png; *.gif",
	file_types_description: "Images",

	upload_url: "/upload/photo.php?pc=1&s=" + session_id,
	flash_url: '/upload/swfupload.swf',
	flash9_url: '/upload/swfupload_fp9.swf',
	button_text: '<span class="xyzzy">upload a new photo</span>',
	button_action: SWFUpload.BUTTON_ACTION.SELECT_FILE,
	button_width: 105,
	button_height: 20,
	file_size_limit: "7 MB",
	disableDuringUpload: true,

	// Button Settings
	button_placeholder_id : 'pcuploadlink',
	button_text_style : '.xyzzy { font-size: 11; font-family: arial; color: #326798; text-align: right; }',
	button_text_top_padding: 0,
	button_text_left_padding: 0,
	button_window_mode: SWFUpload.WINDOW_MODE.TRANSPARENT,
	button_cursor: SWFUpload.CURSOR.HAND,

	file_queued_handler: function (x) { toggleUpload(true, null); },
	file_queue_error_handler : function (x, y, msg) { showPopUp2("Queue Error", msg); },
	upload_error_handler : function (x, y, msg) { showPopUp2("Upload Error", msg); toggleUpload(false, null); },
	upload_progress_handler : function (x, done, total)
	{
		document.getElementById("pcprogress").style.width = Math.round(done / total * 100) + "px";
	},
	upload_success_handler : function (x, json) {
//    alert( json );
		eval("data = " + json);
		toggleUpload(false, data);
	}
	});
}*/

function toggleUpload(yes, photo)
{
	if (yes)
	{
		//pcswfu.startUpload();
		//pcswfu.setButtonDisabled(true);
		//document.getElementById("pcprogress").style.width =  0;
		//document.getElementById("pcprogresscontainer").style.display = "inline";
	}
	else
	{
		//document.getElementById("pcprogresscontainer").style.display = "none";

		pcAddPhoto(photo, true);
		pcChangeProfilePic(photo.id);
		pcPage("start");
		//pcswfu.setButtonDisabled(false);
	}
}

function pcAddPhoto(photo, begin)
{
	e = document.getElementById("pcphotos");

	if (begin)
		pcPhotos.unshift(photo);
	else
		pcPhotos[pcPhotos.length] = photo;

  if( e )
  	e.style.width = (parseInt(e.style.width) + pcPhotoWidth) + "px";

  if( photo == null )
  {
    showPopup2( "Upload Error", "Error processing photo" );
    return;
  }

	newpic = document.createElement("img");
	newpic.id = "pcp-" + photo.id;
	newpic.src = photo.thumb;

  if( newpic.attachEvent )
  	newpic.attachEvent( "onclick", pcChangeProfilePic ); //Required for IE
  else
	  newpic.onclick = pcChangeProfilePic; //Everything else uses this

  if( e )
  	if (begin)
	  	e.insertBefore(newpic, e.firstChild);
  	else
	  	e.appendChild(newpic);

}

function pcGetMorePhotos()
{
	getAjax("/getpcphotos.php?s=" + pcPhotos.length, function (json)
	{
		eval("data = " + json + ";");
		firstRun = pcPhotos.length == 0;

		for (var i in data.photos)
			pcAddPhoto(data.photos[i]);
		
		pcMore = data.more;
		
		pcCacheReady = true;
		
		if (firstRun) //cache more photos
			pcGetMorePhotos();
	});
}

function pcChangeProfilePic(e)
{
	if (typeof e == "object") //event
	{
		if (e.target)
			img = e.target;
		else if (e.srcElement)
			img = e.srcElement;
		if (img.nodeType == 3) //thanks safari
			img = img.parentNode;

		id = img.id.substring(4);
	}
	else
		id = e;

	if (typeof page == "object")
	{
		target = "/pages/update_photo.php";
		data = "pid=" + id + "&gid=" + page.gid;
//		data = "json_update=" + escape(json_encode({'gid': page.gid, 'pid': id }));
	}
	else
	{ // profile
		target = "/settings/changeuserpic.php";
		data = "pic=" + id;
	}

	for (var i in pcPhotos)
		if (pcPhotos[i].id == id)
		{
			e = document.getElementById("profilepic");
			if (e) // large photo
				e.src = pcPhotos[i].img;
			else // small pic (settings page)
				document.getElementById("curuserpic").src = pcPhotos[i].thumb;
			
			postAjax(target, data, "void");
			break;
		}
}

function pcPage(next)
{
	scrollPages = 1;
	
	if (pcTween) // still in motion
		return;
	else if (next == "start")
	{
		scrollPages = pcCurPage;
		pcCurPage = 0;
		next = false;
	}
	else if (next && !pcCacheReady) // new pics not in yet
		return;
	else if (next)
		pcCurPage++;
	else
		pcCurPage--;
	
	e = document.getElementById("pcphotos");
	l = parseInt(e.style.left) ? parseInt(e.style.left) : 0;
	newl = l + pcPerPage * pcPhotoWidth * (next ? -scrollPages : scrollPages);
	pcTween = new Tween(e.style, "left", Tween.regularEaseInOut, l, newl, 1, "px");
	pcTween.onMotionFinished = function() { pcTween = null; }
	pcTween.start();
	
	document.getElementById("pcprev").style.visibility = pcCurPage == 0 ? "hidden" : "visible";
	
	if (pcMore == 0)
		document.getElementById("pcnext").style.visibility = pcPhotos.length > (pcCurPage + 1) * 9 ? "visible" : "hidden";
	
	if (pcMore == 1 && next) //cache more photos
		pcGetMorePhotos();
}

pcGetMorePhotos();
slidedown("photochanger", 80);
