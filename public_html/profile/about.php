<?php
/*
The About page for a user.  This page makes calls to many other pages to assemble all of the
sections into this page.

Includes:
employment.php
work.php
education.php
networks.php
personalinfo.php

Change Log:
2/5/2012 - Rearanged "The Basics" section, to add sector, remove "looking for"
7/29/2012 - Added support for showing connections popup when loading the page.

*/

include "profile_top.php";

if ($user['dob'] != "0000-00-00")
	$birthday = date("F j" . ($user['showyear'] == "1" ? ", Y" : ""), strtotime($user['dob']));
else
  if( $API->uid == $user['uid'])
    $birthday = "Prefer not to say?";
  else
    $birthday = "Prefer not to say";

if( empty( $user['location']) ) $user['location'] = "Plant Earth";

if( empty( $user['citizenship']) )
  if( $API->uid == $user['uid'])
    $citizenship = "Represent your country?";
  else
    $citizenship = "Prefer not to say";
else
{
  $citizenship = quickQuery("select country from loc_country where id=" . intval($user['citizenship']) );
  if( strlen( $citizenship ) > 3 ) $citizenship = ucwords(strtolower($citizenship)) ;
}

if( empty( $user['relationship'] ) )
  if( $API->uid == $user['uid'])
    $relationship = "Why are you here?";
  else
    $relationship = "Networking";
else
  $relationship = convertRelationship($user['relationship']);

if( empty( $user['gender'] ) )
  if( $API->uid == $user['uid'])
    $gender = "Prefer not to say?";
  else
    $gender = "Prefer not to say";
else
  $gender = convertGender($user['gender']);


$loggedIn = $API->isLoggedIn();

if( !$loggedIn )
{
  include( "../signup/signup_popup.php" );
}

?>
<div style="padding: 4px 0 0 5px; width: 549px;" id="aboutprofile">

<?

if( $API->userHasAccess($user['uid'], PRIVACY_BASICS ) && $API->isLoggedIn() ) {
?>


	<div class="subhead">The Basics</div>

	<div style="margin-top: 6px; border-top: 1px solid #d8dfea;">
	<div class="summaryinfo" style="width: 147px;"<?php if ($API->uid == $user['uid']) { ?> onmouseout="javascript:logMouseOver('location', false);" onmouseover="javascript:logMouseOver('location', true);"<?php } ?>>
		<div class="left">From:</div>
		<div class="right" id="user-location-edit"><a href="javascript:void(0);" onclick="javascript:updateUserInfo('location');">edit</a></div>
		<div class="value" id="user-location"><?php
		if ($user['loc_city'] > -1)
			echo ucwords(strtolower(quickQuery("select city from loc_city where id=" . $user['loc_city']))) . ", ";
		if ($user['loc_state'] > -1)
			echo ucwords(strtolower(quickQuery("select region from loc_state where id=" . $user['loc_state']))) . ", ";
		if ($user['loc_country'] > -1)
    {
      $country = quickQuery("select country from loc_country where id=" . $user['loc_country']);
      if( strlen( $country ) > 3 )
        $country = ucwords(strtolower($country));
			echo $country;
    }

    if( $user['loc_country'] == -1 && $user['loc_city'] == -1 && $user['loc_state'] == -1 )
    {
      echo "Planet Earth";
      if( $user['uid'] == $API->uid )
        echo "?";
    }
		?></div>
	</div>

	<div class="summaryinfo" style="width: 137px;"<?php if ($API->uid == $user['uid']) { ?> onmouseout="javascript:logMouseOver('citizenship', false);" onmouseover="javascript:logMouseOver('citizenship', true);"<?php } ?>>
		<div class="left">Citizenship:</div>
		<div class="right" id="user-citizenship-edit"><a href="javascript:void(0);" onclick="javascript:updateUserInfo('basicinfo');">edit</a></div>
		<div class="value" id="user-citizenship"><? echo $citizenship; ?></div>
	</div>
	<div class="summaryinfo" style="width: 127px;"<?php if ($API->uid == $user['uid']) { ?> onmouseout="javascript:logMouseOver('dob', false);" onmouseover="javascript:logMouseOver('dob', true);"<?php } ?>>
		<div class="left">Birthday:</div>
		<div class="right" id="user-dob-edit"><a href="javascript:void(0);" onclick="javascript:updateUserInfo('basicinfo');">edit</a></div>
		<div class="value" id="user-dob"><?=$birthday?></div>
	</div>
<? /*
	<div class="summaryinfo" style="width: 127px;"<?php if ($API->uid == $user['uid']) { ?> onmouseout="javascript:logMouseOver('relationship', false);" onmouseover="javascript:logMouseOver('relationship', true);"<?php } ?>>
		<div class="left">Looking For:</div>
		<div class="right" id="user-relationship-edit"><a href="javascript:void(0);" onclick="javascript:updateUserInfo('basicinfo');">edit</a></div>
		<div class="value" id="user-relationship"><? echo $relationship ?></div>
	</div>
*/ ?>
  <div class="summaryinfo" style="width: 109px; border-right: 0; margin-right: 0;"<?php if ($API->uid == $user['uid']) { ?> onmouseout="javascript:logMouseOver('gender', false);" onmouseover="javascript:logMouseOver('gender', true);"<?php } ?>>
		<div class="left">Gender:</div>
		<div class="right" id="user-gender-edit"><a href="javascript:void(0);" onclick="javascript:updateUserInfo('basicinfo');">edit</a></div>
		<div class="value" id="user-gender"><? echo $gender ?></div>
	</div>
	</div>

	<div style="clear: both; height: 25px;"></div>
<? }

if( !$loggedIn )
{
?>
  <div style="background-color:#FFF9D7; border:1px solid rgb(226,200,34); width:80%; padding:10px; padding-top:6px; margin-left:auto; margin-right:auto; font-size:9pt; color:#000;">
    <b>Do you know <?=$user['name'];?>?</b> Join <?=$user['name'];?>'s network of professionals and businesses in the maritime industry on <?=$siteName?>.
    Use your <a href="/fbconnect/login.php"><b>Facebook</b></a> or <a href="/twitter/login.php"><b>Twitter</b></a> account to sign in or complete the 30 second <a href="javascript:void(0);" onclick="javascript:getStarted2();"><b>account registration</b></a>. Once you've created your free account you'll have access
    to one of the most feature rich maritime websites in the world.  <a href="/start"><b>Take a look at what SaltHub has to offer</b></a>.
  </div>
<?
}
else
{

  if( $API->userHasAccess($user['uid'], PRIVACY_EMPLOYMENT ) ) {

  	ob_start();
  	echo '<div class="subhead">Seeking Employment</div>';
  	include "employment.php";
  	$html = ob_get_contents();
  	ob_end_clean();

  	if (count($seekingEmployment) > 0 || $API->uid == $uid)
  		echo $html;

  }


  if( $API->userHasAccess($user['uid'], PRIVACY_WORK ) ) {

  	ob_start();
  	include "work.php";
  	$html = ob_get_contents();
  	ob_end_clean();

  	if (($works[0]['dummy'] != 1 && trim( strip_tags( $html ) ) != '') || $API->uid == $uid)
    {
    	echo '<div class="subhead" style="margin-bottom: 10px;">Work</div><div id="work_contents">';
      echo $html . "</div>";
    }

  }


  if( $API->userHasAccess($user['uid'], PRIVACY_EDUCATION ) ) {

  	ob_start();
  	echo '<div class="subhead">Education</div>';
  	include "education.php";
  	$html = ob_get_contents();
  	ob_end_clean();

  	if (count($edus) > 0 || count($info) > 0 || $API->uid == $uid)
  		echo $html;

  }

  if( $API->userHasAccess($user['uid'], PRIVACY_NETWORKS ) )
  {
    if( quickQuery( "select count(*) from  pages left join page_members on page_members.gid=pages.gid where pages.subcat=" . CAT_CLUB_ASSOC . " and page_members.uid='" . $user['uid'] . "'" ) > 0 || $user['uid'] == $API->uid )
    {
  ?>
  	<div id="networkscontainer">
  		<div class="subhead" style="margin-bottom: 10px;">Clubs &amp; Associations</div>
  		<?php include "networks.php"; ?>
  	</div>
  <?
    }
  }

  if( $API->userHasAccess($user['uid'], PRIVACY_LIKES ) ) {

  	ob_start();
  	echo '<div class="subhead">Likes</div>';
  	include "personalinfo.php";
  	$html = ob_get_contents();
  	ob_end_clean();

  	if ($API->uid == $uid)
  		echo $html;
  	elseif (count($info) > 0)
  	{
  		//check to see if anything non-special exists
  		foreach ($info as $inf)
  			if ($inf['special'] == 0)
  			{
  				echo $html;
  				break;
  			}
  	}

  }
}
	?>


</div>

<?php if (end(explode('/', $_SERVER['REQUEST_URI'])) == 'message') { ?>
<script type="text/javascript">
showSendMessage(<?=$user['uid']?>, "<?=addslashes($user['name'])?>", "<?=$API->getThumbURL(1, 48, 48, $API->getUserPic($user['uid'], $user['pic']))?>");
</script>
<?php } ?>

<?

if ($API->isLoggedIn() && $API->userHasAccess( $user['uid'], PRIVACY_CONNECTIONS ) && end(explode('/', $_SERVER['REQUEST_URI'])) == 'connect')
{
?>
<script language="javascript" type="text/javascript">
  showFriendsPopup(<?=$user['uid'];?>);
</script>
<?
}
?>

