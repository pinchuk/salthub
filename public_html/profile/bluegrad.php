<?php

header("Content-Type: image/png");

$h = intval($_GET['h']);

if ($h == 0)
	$h = 55;

if ($h > 1000)
	$h = 1000;	

$begin = array(242, 251, 255);
$end = array(255, 255, 255);
$diff = array(($end[0] - $begin[0]) / $h, ($end[1] - $begin[1]) / $h, ($end[2] - $begin[2]) / $h);

$img = imagecreatetruecolor(1, $h);

for ($i = 0; $i < $h; $i++)
{
	$color = imagecolorallocate($img, $begin[0] + round($diff[0] * $i), $begin[1] + round($diff[1] * $i), $begin[2] + round($diff[2] * $i));
	imagerectangle($img, 0, $i, 1, $i + 1, $color);
}

imagepng($img, null, 0, PNG_ALL_FILTERS);

?>