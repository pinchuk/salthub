<?php
//$licType = quickQuery("select tid from personal_types where special=1");
//$degreeType = quickQuery("select tid from personal_types where special=2");

$degreeType = DEGREES_GRP;
$licType = LIC_AND_END_GRP;

unset( $info );

//$x = mysql_query("select g.gid,t.tid,t.name as tname,i.pid,i.name,special from personal_types t left join personal_items i on t.tid=i.tid left join personal p on p.pid=i.pid left join pages g on g.glink=i.pid and g.gtype='P' where p.uid={$API->uid} and (t.tid=5 or t.tid=8)order by special,t.name,i.name") or die(mysql_error());

$x = mysql_query("select g.gid,g.cat as tid,g.gname as name,categories.catname as tname from personal left join pages g on g.gid=personal.gid left join categories on categories.cat=g.subcat where personal.uid='" . $user['uid'] . "' and (g.cat='$licType' or g.cat='$degreeType') group by gid order by g.cat desc, g.gname" );
echo mysql_error();

while ($y = mysql_fetch_array($x, MYSQL_ASSOC))
	$info[] = $y;

?>

<div id="education" style="padding:0px; margin:0px;" onmouseover="javascript:document.getElementById('piedit-<?=$licType?>').style.visibility = 'visible';" onmouseout="javascript:document.getElementById('piedit-<?=$licType?>').style.visibility = 'hidden';">
	<div id="items-<?=$licType?>" style="padding:0px; margin:0px;">
		<?php include "educationinfo.php"; ?>
    <div style="clear:both;"></div>
	</div>
</div>



<div id="educationedit" style="display: none; padding-top: 10px;">
	<div style="font-size: 8pt; padding-bottom: 20px;">
		Complete your educational experience to connect with old and present classmates.&nbsp; By doing so, you'll also be able to find others who have attended the same schools and hold the same degrees and licenses.
	</div>

	<div class="education">
		<div class="period">
			<div class="smtitle2">Period:</div>
		</div>
		<div style="float: left;">
			<div class="smtitle2">School:</div>
		</div>
	</div>

	<div id="educontainer"></div>

	<div style="clear: both; height: 20px;"></div>

	<div class="itemchooserparent" id="itemchooserparent-<?=$licType?>" style="padding: 0;"></div>
	<div class="itemchooserparent" id="itemchooserparent-<?=$degreeType?>" style="padding: 0;"></div>

<!--
  <div style="float: left;">
		<div class="smtitle2" style="padding-bottom: 3px;">Degrees:</div>
		<textarea class="profilepreview" id="edu-degrees" style="background: #fff; height: 56px;"><?=$degrees?></textarea>
		<div style="padding-top: 5px; font-size: 8pt;">Separate entries with commas</div>
	</div>-->
</div>

<div style="clear: both; height: 10px;"></div>

<script language="javascript" type="text/javascript">
<!--
var schools = [];
var edus = <?=json_encode($edus)?>;
var eduShown = 0;
var eduMax = 0;

for (var i in edus)
	eduAdd(edus[i]);

eduCheckAdd(); //add a blank entry

function eduAdd(edu)
{
	html  = '<div class="education" id="edu-' + eduMax + '">';
	html += '	<div class="period">';
	for (j = 0; j < 2; j++)
	{
		if (j == 1) html += " to ";
		html += '<select id="edu-year' + j + '-' + eduMax + '"><option></option>';
		for (y = <?=date("Y") - 50?>; y <= <?=date("Y") + 8?>; y++)
		html += '<option value="' + y + '" ' + (y == (j == 0 ? edu.start : edu.stop) ? "selected" : "") + '>' + y + '</option>';
		html += '</select>';
	}
	html += '	</div>';
	html += '	<div style="float: left;">';
	html += '		<input onkeypress="javascript:eduCheckAdd();" type="text" id="edu-school-' + eduMax + '" style="width: 275px;" value="' + (edu.school_name ? edu.school_name : "") + '" />';
	html += '		&nbsp; &nbsp; &nbsp; <a href="javascript:void(0);" onclick="javascript:eduRemove(' + eduMax + ');">X</a>';
	html += '	</div>';
	html += '</div>';
	
	newdiv = document.createElement("div");
	newdiv.innerHTML = html;
	
	document.getElementById("educontainer").appendChild(newdiv);
	
	schools[eduMax] = new actb(document.getElementById("edu-school-" + eduMax), "school");
	if (edu.school) schools[eduMax].actb_val = edu.school;
	
	eduShown++;
	eduMax++;
}

function eduRemove(i)
{
	if (eduShown == 1)
	{
		document.getElementById("edu-year0-" + i).selectedIndex = 0;
		document.getElementById("edu-year1-" + i).selectedIndex = 0;
		document.getElementById("edu-school-" + i).value = "";
	}
	else
	{
		document.getElementById("edu-" + i).style.display = "none";
		eduShown--;
	}
}

function eduCheckAdd()
{
	for (i = 0; i < eduMax; i++)
	{
		if (document.getElementById("edu-" + i).style.display == "" && document.getElementById("edu-school-" + i).value == "")
			return;
	}
	
	eduAdd({'start':'0', 'stop':'0', 'school':''});
}
//-->
</script>