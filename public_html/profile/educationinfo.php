<?php
include_once "../inc/inc.php";

if (empty($user['uid']))
	$user['uid'] = $API->uid;

if (!isset($licType))
	$licType = quickQuery("select tid from personal_types where special=1");

if (!isset($degreeType))
	$degreeType = quickQuery("select tid from personal_types where special=2");

$edus = array();
$x = mysql_query("select start,stop,school,gname as school_name from education inner join pages on gid=school where uid=" . $user['uid'] . " order by start");
while ($y = mysql_fetch_array($x, MYSQL_ASSOC))
	$edus[] = $y;

if (is_array($info))
foreach ($info as $item)
	if ($item['tid'] == $degreeType)
		$degs[] = $item['name'];

if ($degs)
	$degrees = implode(", ", $degs);
else
	$degs = "";
?>
  <div class="edit" id="piedit-<?=$licType?>" style="float: right; padding:0px; padding-right: 5px; margin:0px;"><a href="javascript:void(0);" onclick="javascript:piEdit(<?=$licType?>); piEdit(<?=$degreeType?>); toggleEdit('education');"><?php if ($user['uid'] == $API->uid) echo "edit"; ?></a></div>
	<?php ob_start(); ?>
<? if( count( $edus ) > 0 ) { ?>
	<div style="width: 90%; float: left; margin-top:10px; padding-top:0px;">
		<div class="personalinfo" style="padding-top:0px; margin-top:0px;">
			<div class="cat" style="margin-bottom:6px;">Attended</div>
			<div style="float: left; width: 410px; clear:both; margin-left:5px; margin-bottom:7px;">
				<?php
				$html = array();
				foreach ($edus as $edu)
					$html[] = '<a href="' . $API->getPageURL($edu['school'], $edu['school_name']) . '">' . $edu['school_name'] . '</a>&nbsp;(' . $edu['start'] . '-' . $edu['stop'] . ')';
				echo implode(",&nbsp; ", $html);
				?>
			</div>
		</div>
	</div>
<?
}

$counts = array(0,0);
$subcats = array( 0,0,0,0,0,0,0,0 );
if (is_array($info)){
  foreach ($info as $i)
  {
    $counts[ ($i['tid'] == $licType) ]++;

    if( stristr( $i['tname'], "USCG" ) && stristr( $i['tname'], "Certificate" ) === false ) $subcats[0]++;
    if( stristr( $i['tname'], "MCA" ) && stristr( $i['tname'], "Certificate" ) === false ) $subcats[1]++;
    if( stristr( $i['tname'], "RYA" ) && stristr( $i['tname'], "Certificate" ) === false  ) $subcats[2]++;

    if( stristr( $i['tname'], "USCG" ) && stristr( $i['tname'], "Certificate" ) !== false ) $subcats[3]++;
    if( stristr( $i['tname'], "MCA" ) && stristr( $i['tname'], "Certificate" ) !== false ) $subcats[4]++;
    if( stristr( $i['tname'], "RYA" ) && stristr( $i['tname'], "Certificate" ) !== false ) $subcats[5]++;
    if( stristr( $i['tname'], "Unselected" ) !== false ) $subcats[6]++;

  }
}

if( $counts[1] > 0 ) { ?>
	<div class="personalinfo" style="padding-top:0px; margin-top:4px; margin-bottom:0px; padding-bottom:5px;">
		<div class="cat" style="margin-bottom:10px;">Licenses Held</div>

    <? if( $subcats[0] > 0 ) { ?>
   	<div class="personalinfo" style="padding-top:0px; margin-top:4px; margin-bottom:5px;">
  		<div class="cat" style="margin-bottom: 0px; font-size: 8pt; font-weight:300;">USCG Licenses</div>
  		<div style="float: left; clear:both; margin-left:5px; margin-bottom:7px;">
  			<?php
  			$c = count($info);
  			displayPersonalInfo2(array("tid" => $licType), $info, "USCG", false);
  			$c -= count($info);
  			?>
  		</div>
  	</div>
    <? }

    if( $subcats[1] > 0 ) {
    ?>
   	<div class="personalinfo" style="padding-top:0px; margin-top:4px; margin-bottom:5px;">
  		<div class="cat" style="margin-bottom: 0px; font-size: 8pt; font-weight:300;">MCA Licenses</div>
  		<div style="float: left; clear:both; margin-left:5px; margin-bottom:7px;">
  			<?php
  			$c = count($info);
  			displayPersonalInfo2(array("tid" => $licType), $info, "MCA", false);
  			$c -= count($info);
  			?>
  		</div>
  	</div>
    <?
    }

    if( $subcats[2] > 0 ) {
    ?>
   	<div class="personalinfo" style="padding-top:0px; margin-top:4px; margin-bottom:5px;">
  		<div class="cat" style="margin-bottom: 0px; font-size: 8pt; font-weight:300;">RYA Licenses</div>
  		<div style="float: left; clear:both; margin-left:5px; margin-bottom:7px;">
  			<?php
  			$c = count($info);
  			displayPersonalInfo2(array("tid" => $licType), $info, "RYA", false);
  			$c -= count($info);
  			?>
  		</div>
  	</div>
    <?
    }

   if( $subcats[3] > 0 ) {
    ?>
   	<div class="personalinfo" style="padding-top:0px; margin-top:4px; margin-bottom:5px;">
  		<div class="cat" style="margin-bottom: 0px; font-size: 8pt; font-weight:300;">USCG Certificates</div>
  		<div style="float: left; clear:both; margin-left:5px; margin-bottom:7px;">
  			<?php
  			$c = count($info);
  			displayPersonalInfo2(array("tid" => $licType), $info, "USCG", true);
  			$c -= count($info);
  			?>
  		</div>
  	</div>
    <?
    }

   if( $subcats[4] > 0 ) {
    ?>
   	<div class="personalinfo" style="padding-top:0px; margin-top:4px; margin-bottom:5px;">
  		<div class="cat" style="margin-bottom: 0px; font-size: 8pt; font-weight:300;">MCA Certificates</div>
  		<div style="float: left; clear:both; margin-left:5px; margin-bottom:7px;">
  			<?php
  			$c = count($info);
  			displayPersonalInfo2(array("tid" => $licType), $info, "MCA", true);
  			$c -= count($info);
  			?>
  		</div>
  	</div>
    <?
    }

   if( $subcats[5] > 0 ) {
    ?>
   	<div class="personalinfo" style="padding-top:0px; margin-top:4px; margin-bottom:5px;">
  		<div class="cat" style="margin-bottom: 0px; font-size: 8pt; font-weight:300;">RYA Certificates</div>
  		<div style="float: left; clear:both; margin-left:5px; margin-bottom:7px;">
  			<?php
  			$c = count($info);
  			displayPersonalInfo2(array("tid" => $licType), $info, "RYA", true);
  			$c -= count($info);
  			?>
  		</div>
  	</div>
    <?
    }
    if( $subcats[6] > 0 ) {
        ?>
        <div class="personalinfo" style="padding-top:0px; margin-top:4px; margin-bottom:5px;">
            <div class="cat" style="margin-bottom: 0px; font-size: 8pt; font-weight:300;">Other Licenses</div>
            <div style="float: left; clear:both; margin-left:5px; margin-bottom:7px;">
                <?php
                $c = count($info);
                displayPersonalInfo2(array("tid" => $licType), $info, "Unselected", false);
                $c -= count($info);
                ?>
            </div>
        </div>
    <?
    }
    ?>
  </div>

<? } if( $counts[0] > 0 ) { ?>

	<div class="personalinfo" style="padding-top:0px; margin-top:4px;">
		<div class="cat" style="margin-bottom:6px;">Degrees</div>
		<div style="float: left; clear:both; margin-left:5px;">
      <?php displayPersonalInfo(array("tid" => $degreeType), $info); ?>
    </div>
	</div>
<?
}

$eduhtml = ob_get_contents();
ob_end_clean();

if (count($edus) > 0 || count($info) > 0 )
	echo $eduhtml;
else
	echo '<div class="nodata" style="margin: 0;">Complete your educational experience to connect with old and present classmates.&nbsp; By doing so, you\'ll also be able to find others who have attended the same schools and hold the same degrees and licenses.</div>';



function displayPersonalInfo2($piType, &$info, $type, $certificate )
{
	global $API;

	$items = 0;

	if (is_array($info)){
//        if ($piType['tid'] != LIC_TID){
//            echo '<pre>';
//            print_r($info);
//            die;
//        }
        foreach ($info as $item)
        {
            if ($piType['tid'] != $item['tid']) continue;

        if( $certificate && (stristr( $item['tname'], $type ) === false || stristr( $item['tname'], "Certificate" ) === false ) ) continue;
        if( !$certificate && (stristr( $item['tname'], $type ) === false || stristr( $item['tname'], "Certificate" ) !== false ) ) continue;

            if ($items++ > 0)
          if( $piType['tid'] == LIC_TID || $piType['tid'] == DEGREE_TID )
            echo "<br/>";
//          else
//            echo ", ";

            if ($type == "Unselected"){
                echo '<div>'.$item['name'].'</div>';
                continue;
            }
            else if ($item['gid'])
                echo '<div><a href="' . $API->getPageURL($item['gid'], $item['name']) . '">' . $item['name'] . '</a></div>';
            else
                echo '<div>'.$item['name'].'</div>';
        }
    }
	return $items;
}
?>