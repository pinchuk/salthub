<?php
header("Content-type: text/javascript");
include "../inc/inc.php";
?>

var itemsChosen_emp = Array();

function newComboListBox_emp(index, targetid, title )
{
  tidEditing = index;

	html  = '<div style="width: 175px; float: left;">';
	html += '	<div style="width: 175px; float:left;">';
	html += '	  <div class="smtitle2" style="padding-bottom: 3px;">' + title + '</div>';
	html += '	  <div style="width: 175px; height: 18px; border: 1px solid #555; position: relative; float:left;">';
	html += '		  <input type="text" onkeyup="javascript:searchItems_emp(' + tidEditing + ' );" id="itemsearch-' + tidEditing + '" style="margin: 0; padding: 0px 2px; border: 0; width: 154px; height: 18px;"><img src="/images/dropdown.png" onclick="javascript:toggleItemChooser_emp(' + tidEditing + ');" style="cursor: pointer; vertical-align: top;" alt="" />';
	html += '		  <div id="itemchooser-' + tidEditing + '" class="itemchooser" style="width:170px;">';
	html += '			  <div id="chooser-' + tidEditing + '" class="chooser" style="width:170px;"></div>';
	html += '			  <div style="padding: 5px; text-align: center; background: #fff; border-top: 1px solid #555;">';
	html += '				  <input type="button" class="button" style="width:60px; margin:4px;" value="Save" onclick="javascript:saveItems_emp(' + tidEditing + '); toggleItemChooser_emp(' + tidEditing + ');" /> <input style="width:60px; margin:4px;" type="button" class="button" value="Cancel" onclick="javascript:toggleItemChooser_emp(' + tidEditing + '); loadItems_emp(0,' + tidEditing + ' );" />';
	html += '			  </div>';
	html += '		  </div>';
	html += '	  </div>';
	html += '	  <div style="clear:both; padding-top: 5px; font-size: 8pt;">Your Selections</div>';
	html += '	  <div id="selections-' + tidEditing + '" style="font-size:8pt; margin-bottom:15px;"></div>';
	html += '	  <div id="customselections-' + tidEditing + '" style="font-size:8pt; margin-bottom:15px;"></div>';
	html += '	</div>';
  html += ' <div style="margin-bottom:10px;"></div>';
	html += '</div>';
	html += '<div style="clear: both;"></div>';

	e = document.getElementById(targetid);
	e.innerHTML = html;
	e.style.display = "inherit";

	loadItems_emp(0, tidEditing);
}

function loadItems_emp(i, tidEditing)
{
	loadjscssfile("/profile/employmentCLBItems.js.php?i="+i+"&tid=" + tidEditing + "&hash=" + hash, "js");
}

function refreshSelections_emp( tidEditing )
{
  html = '';

  e = document.getElementById( "selections-" + tidEditing );
  if( e )
  {
  	for (var i in itemsChosen_emp[tidEditing])
  		if (itemsChosen_emp[tidEditing][i] != null)
	  		if (itemsChosen_emp[tidEditing][i][1] != null)
      	{
          if( tidEditing <= 11 && itemsChosen_emp[tidEditing][i][0] > 0 )
            html += '<a href="/profile/page-forward.php?gid=' + itemsChosen_emp[tidEditing][i][0] + '">' + itemsChosen_emp[tidEditing][i][1] + "</a><br />";
          else
            html += itemsChosen_emp[tidEditing][i][1] + "<br />";
        }

    if( html == '' )
      html = 'use dropdown above to select';
    e.innerHTML = html;
  }
}

function saveItemsHandler_emp( data ) { //alert( data )
 }
function saveItems_emp( tidEditing )
{
	pids = "";

	for (var i in itemsChosen_emp[tidEditing])
	{
		if (itemsChosen_emp[tidEditing][i] != null)
			if (typeof itemsChosen_emp[tidEditing][i][0] != "undefined")
				pids += "," +itemsChosen_emp[tidEditing][i][0];
	}

  data = "tid=" + tidEditing + "&items=" + pids.substring(1);

	postAjax("/profile/employment_save.php", data, "saveItemsHandler_emp" );
}

function toggleItemChooser_emp(tidEditing)
{
	e = document.getElementById("itemchooser-" + tidEditing);
	isShown = e.style.display == "inline";

	if (!isShown) // load item choices
	{
		document.getElementById("chooser-" + tidEditing).innerHTML = '&nbsp;';
		loadItems_emp(1, tidEditing );
	}

	e.style.display = isShown ? "none" : "inline";
}

function searchItems_emp(tidEditing)
{
  e = document.getElementById("itemsearch-" + tidEditing);

  if( !e )
  {
    alert( tidEditing );
  }

	q = e.value.toLowerCase();

	if (document.getElementById("itemchooser-" + tidEditing).style.display != "inline")
		toggleItemChooser_emp(tidEditing);

	i = 0;
	
	while (e = document.getElementById("nameitem-" + tidEditing + "-" + i))
		document.getElementById("item-" + i++).style.display = e.innerHTML.toLowerCase().indexOf(q) == -1 ? "none" : "";
}

function itemChosen_emp(i, pid, chosen, tidEditing)
{
	if (chosen)
	{
		name = document.getElementById("nameitem-" + tidEditing + "-" + i).innerHTML;
		itemsChosen_emp[tidEditing][itemsChosen_emp[tidEditing].length] = [pid, name];
	}
	else
	{
		for (var i in itemsChosen_emp[tidEditing])
		{
			if (itemsChosen_emp[tidEditing][i] != null)
				if (itemsChosen_emp[tidEditing][i][0] == pid)
				{
					itemsChosen_emp[tidEditing][i] = null;
					break;
				}
		}
	}

  refreshSelections_emp( tidEditing );
}

function saveObjective( div )
{
  e = document.getElementById( div );
  if( e )
  {
    data = "tid=14&obj=" + encodeURIComponent( e.value );
    postAjax("/profile/employment_save.php", data, "saveItemsHandler_emp" );
  }
}

function saveSeekingEmployment( div )
{
  e = document.getElementById( div );
  if( e )
  {
    val = selectedValue(e);

    data = "tid=15&se=" + val;
    postAjax("/profile/employment_save.php", data, "saveItemsHandler_emp" );
  }
}

function refreshEmploymentInfo()
{
  postAjax("/profile/employmentinfo.php", "", "refreshEmpHandler" );
}

function refreshEmpHandler(data)
{
  e = document.getElementById( "items-employment" );
  if( e ) e.innerHTML = data;
}