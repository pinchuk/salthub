<?php

unset( $info );
$uid = $user['uid'];

//$x = mysql_query("select * from users where uid='$uid'" );
//$user = mysql_fetch_array( $x );
$introText = "Complete this section to activate your profile on the job board. Select the occupation / position, location and what you're looking for in a job.  Complete this section along with education and work experience to complete your profile and resume.";
?>
<div id="employment" onmouseover="javascript:document.getElementById('piedit-employment').style.visibility = 'visible';" onmouseout="javascript:document.getElementById('piedit-employment').style.visibility = 'hidden';">
	<div id="items-employment">
		<?php include "employmentinfo.php"; ?>
	</div>
</div>

<div id="employmentedit" style="display: none; padding-top: 10px;">
	<div style="font-size: 8pt; padding-bottom: 10px;">
	  <? echo $introText; ?>
	</div>

  <div class="smtitle2" style="padding-bottom:10px;">
<!--    <input type="checkbox" name="seeking_employment" id="seeking_employment" onchange="saveSeekingEmployment('seeking_employment');" value="1" <? if( $user['seeking_employment'] ) echo "CHECKED"; ?>/> Available for employment-->
<?
$duration = array( "Not Seeking Employment", "Full Time", "Part Time", "Seasonal", "Contract", "Day Work", "Delivery" );
?>
    Availability:<br />
    <select id="seeking_employment" name="seeking_employment" onchange="saveSeekingEmployment('seeking_employment');" style="width:177px;" id="duration">
<? for( $c = 0; $c < sizeof( $duration ); $c++ ) { ?>
    <option value="<? echo $c ?>"<? if( $user['seeking_employment'] == $c ) echo " SELECTED"; ?>><? echo $duration[$c]; ?></option>
<? } ?>
    </select>


  </div>

  <div style="width:100%;">
    <div style="float:left; width:33%;">
      <div class="tcontent" style="width:180px; padding-left:0px; margin-left:0px; padding-top:5px;" id="positionBox"></div>
    </div>

    <div style="float:left; width:33%;">
      <div class="tcontent" style="width:180px; padding-left:0px; margin-left:0px; padding-top:5px;" id="sectorBox"></div>
    </div>

    <div style="float:left; width:33%;">
      <div class="tcontent" style="width:180px; padding-left:0px; margin-left:0px; padding-top:5px;" id="locationBox"></div>
    </div>
  </div>

  <div style="clear:both;"></div>

  <div>
    <div class="smtitle2" style="padding-bottom: 3px;">Objective:</div>
    <textarea name="employment_objective" id="employment_objective" style="width:532px;" onchange="javascript: saveObjective( 'employment_objective' );"><? echo $user['employment_objective']; ?></textarea>
  </div>

  <div style="clear: both; height: 10px;"></div>

  <div style="clear: both; padding-left: 300px;">
    <input type="button" class="button" value="Save" onclick="javascript:closeEmployment();" /> &nbsp;
    <input type="button" class="button" value="Cancel" onclick="javascript:closeEmployment();" /> &nbsp;
  </div>

</div>


<div style="clear: both; height: 10px;"></div>

<script type="text/javascript">
<!--
newComboListBox_emp( 11, "positionBox", "Occupation/Position:" );
newComboListBox_emp( 12, "sectorBox", "Sector:" );
newComboListBox_emp( 13, "locationBox", "Current Location:" );

function closeEmployment()
{
  e = document.getElementById( "seeking_employment" );

  if( !e ) return;

  if( itemsChosen_emp[11].length == 0 && e.checked )
  {
    alert( "Please select a position for your listing before continuing." );
  }
  else if( itemsChosen_emp[12].length == 0 && e.checked )
  {
    alert( "Please select a sector for your listing before continuing." );
  }
  else if( itemsChosen_emp[13].length == 0 && e.checked )
  {
    alert( "Please select a location for your listing before continuing." );
  }
  else
  {
    refreshEmploymentInfo();
    toggleEdit('employment');
  }
}
-->
</script>