<?php
include_once "../inc/inc.php";

include_once( "../employment/employment_functions.php" );

if( empty( $user ) )
{
  $q = sql_query( "select * from users where uid='" . $API->uid . "'" );
  $user = mysql_fetch_array( $q );
}

$introText = "Complete this section to activate your profile on the <a href=\"/employment/employees.php\">job board</a>. Select the occupation / position, location and what you're looking for in a job.  Complete this section along with education and work experience to complete your profile and resume.";

?>
<div class="edit" id="piedit-employment" style="float: right; padding: 0 5px 0 0;"><a href="javascript:void(0);" onclick="javascript:toggleEdit('employment');"><?php if ($user['uid'] == $API->uid) echo "edit"; ?></a>&nbsp;</div>
<?
if( $user['seeking_employment'] )
{
  $seekingEmployment = 1;

  $duration = array( "Not Seeking Employment", "Full Time", "Part Time", "Seasonal", "Contract", "Day Work", "Delivery" );

?>
<div style="margin-left:5px;">
	<div class="work" style="float:left; margin-top:-5px; padding-top:0px;">
    <div class="smtitle2" style="padding-bottom:5px;">Looking for:</div>
    I'm seeking <? echo strtolower( $duration[ $user['seeking_employment'] ] ); ?> employment <?=gidToName($user['employment_position'], "as an ", "in any position" );?> in <?=catToName($user['employment_sector'], "the ", "any");?> sector(s). I am currently located in <? echo countryToName( $user['employment_location'], "", "(not selected)" ); ?>.
  </div>

	<div class="work">
    <div class="smtitle2" style="padding-bottom:5px;">Objective:</div>
    <?= nl2br( $user['employment_objective'] ); ?>
  </div>

  <div style="clear:both; height:15px"></div>
</div>
<?
}
else
{
	echo '<div class="nodata" style="margin: 0;">' . $introText . '</div>';
}
?>