<?php
/*
This page shows a single feed item; usually linked from an external source (e.g., email, or facebook post)
*/

//Allow the FB crawler to see this site without logging in.
if( stristr( $_SERVER['HTTP_USER_AGENT'], "facebookexternal" ) !== false )
{
  $facebookCrawler = true;
}

include( "../inc/inc.php" );

$fid = $_GET['fid'];
$feed = queryArray( "select type, link, gid from feed where fid='$fid'" );

$imgurl = "";

if( $feed['type'] == "rss" )
{
  $rss = queryArray( "select * from rss_feed where id=" . $feed['link'] );
  $page = $API->getPageInfo( $rss['gid'] );

  if( $rss['image'] != '' )
  {
    $imgurl = $rss['image'];
  }
  else
  {
    $imgurl = "/img/119x95/" . $page['profile_pic'];
    if( $page['pid'] == 0 )
      $imgurl = $page['profile_pic'];

    $imgurl = "http://www.salthub.com/" . $imgurl;
  }

  $meta[] = array( "property" => "og:description", "content" => cutoffText( strip_tags($rss['preview']), 150 ) );
  $meta[] = array( "property" => "og:title", "content" => $rss['title'] );
  $meta[] = array( "property" => "og:url", "content" => "http://" . SERVER_HOST . "/profile/feed.php?fid=" . $fid );
  $meta[] = array( "property" => "og:site_name", "content" => $siteName );
  $meta[] = array( "property" => "og:type", "content" => "website" );
  $meta[] = array( "property" => "fb:app_id", "content" => $fbAppId );
}
else
{
    $query = "SELECT post FROM wallposts INNER JOIN feed ON feed.link = wallposts.sid WHERE feed.fid = $fid";
    $res = mysql_query($query);
    if ($res !== false){
        $post = mysql_fetch_assoc($res);
        $data = json_decode($post['post'], true);

        // new
        if (count($data) == 0){
            $result = array();
//            while ($row = mysql_fetch_assoc(mysql_query("SELECT type from feed where fid = $fid")))
//                $result[] = $row['type'];

            //if ($result['type'] == 'A'){
                $result = mysql_query("SELECT * FROM albums INNER JOIN feed ON feed.link = albums.id WHERE feed.fid = $fid");

                while ($row = mysql_fetch_assoc($result)){
                    $data['title'] = $row['title'];
                    $data['descr'] = $row['descr'];
                    $data['mainImage'] = $row['mainImage'];

                    $meta[] = array( "name" => "title", "content" => $data['title'] );
                    $meta[] = array( "name" => "description", "content" => $data['descr'] );
                }

            $photo = mysql_query("SELECT * FROM photos where id = ".$data['mainImage']);
            $img = array();
            while ($row = mysql_fetch_assoc($photo)){
                $data['image'] = 'http://'.$_SERVER['HTTP_HOST'].$API->getThumbURL(2, 635, 655, "/photos/" .  $row['aid'] . "/" . $row['hash'] . ".jpg");
                $data['link'] = 'http://'.$_SERVER['HTTP_HOST'].$API->getAlbumURL($row['aid']);
            }
            //}
            $_GET['descr'] = $data['descr'];
            $_GET['title'] = $data['title'];
        }

        $imgurl = strtolower($data['image']);

        $meta[] = array( "property" => "og:description", "content" => $data['descr'] );
        $meta[] = array( "property" => "og:title", "content" => $data['title'] );
        $meta[] = array( "property" => "og:url", "content" => strtolower($data['link']) );
        $meta[] = array( "property" => "og:site_name", "content" => $siteName );
        $meta[] = array( "property" => "og:type", "content" => "website" );
        $meta[] = array( "property" => "fb:app_id", "content" => $fbAppId );
    }
    else
        echo 'false';
}

if( $imgurl == "" )
  $imgurl = "http://www.salthub.com/images/salt_badge100.png";

$meta[] = array( "property" => "og:image", "content" => $imgurl );


if( $facebookCrawler )
{
  $API->uid = 91;
}

$borderStyle = 1;

$scripts[] = "/comments.js";
$scripts[] = "/tipmain.js";
$scripts[] = "/share.js.php";

include( "../header.php" );
//echo '<pre>';
//print_r($_SESSION);
//exit;
include "../inc/embedmedia.php";

?>
<div style="margin-left:10px;">
<?
$no_scroll = true;
include( "popup-feeditem.php" );
?>
</div>
<?
include "../footer.php";
?>
