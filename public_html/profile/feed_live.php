<?php
/*
This page shows a single feed item; usually linked from an external source (e.g., email, or facebook post)
*/

//Allow the FB crawler to see this site without logging in.
if( stristr( $_SERVER['HTTP_USER_AGENT'], "facebookexternal" ) !== false )
{
  $facebookCrawler = true;
}


include( "../inc/inc.php" );

$fid = $_GET['fid'];
$feed = queryArray( "select type, link, gid from feed where fid='$fid'" );

$imgurl = "";

if( $feed['type'] == "rss" )
{
  $rss = queryArray( "select * from rss_feed where id=" . $feed['link'] );
  $page = $API->getPageInfo( $rss['gid'] );

  if( $rss['image'] != '' )
  {
    $imgurl = $rss['image'];
  }
  else
  {
    $imgurl = "/img/119x95/" . $page['profile_pic'];
    if( $page['pid'] == 0 )
      $imgurl = $page['profile_pic'];

    $imgurl = "http://www.salthub.com/" . $imgurl;
  }

  $meta[] = array( "property" => "og:description", "content" => cutoffText( strip_tags($rss['preview']), 150 ) );
  $meta[] = array( "property" => "og:title", "content" => $rss['title'] );
  $meta[] = array( "property" => "og:url", "content" => "http://" . SERVER_HOST . "/profile/feed.php?fid=" . $fid );
  $meta[] = array( "property" => "og:site_name", "content" => $siteName );
  $meta[] = array( "property" => "og:type", "content" => "website" );
  $meta[] = array( "property" => "fb:app_id", "content" => $fbAppId );
}

if( $imgurl == "" )
  $imgurl = "http://www.salthub.com/images/salt_badge100.png";

$meta[] = array( "property" => "og:image", "content" => $imgurl );



if( $facebookCrawler )
{
  $API->uid = 91;
}

$borderStyle = 1;

$scripts[] = "/comments.js";
$scripts[] = "/tipmain.js";
$scripts[] = "/share.js.php";

include( "../header.php" );
include "../inc/embedmedia.php";

?>
<div style="margin-left:10px;">
<?
$no_scroll = true;
include( "popup-feeditem.php" );
?>
</div>
<?
include "../footer.php";
?>
