<? if( $API->userHasAccess($user['uid'], PRIVACY_FOLLOWME ) ) {

ob_start();
include "followme.php";
$follow_me_content = ob_get_contents();
ob_end_clean();

if( $found || $API->uid == $uid )
{
?>
		<div onmouseover="javascript:document.getElementById('edit-followme').style.visibility = 'visible';" onmouseout="javascript:document.getElementById('edit-followme').style.visibility = 'hidden';">
			<div class="subhead" style="margin-top: 10px;">Follow Me</div>
			<div style="font-size: 8pt; color: #555; padding-top: 5px;">
				Other Profiles
				<?php if ($API->uid == $uid) { ?>
				<div id="edit-followme" style="visibility: hidden; float: right; padding-right: 5px;">
					<a href="javascript:editFollowMe();">edit</a>
				</div>
				<?php } ?>
			</div>
			<div class="followme" id="followme">
			<? echo $follow_me_content; ?>
			</div>
		</div>
<? } } ?>