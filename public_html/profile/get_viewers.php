<?
/*
This is a module that displays other users who has previous viewed this profile or page.

Change Log
1/20/2012 - Created


*/

include_once( "../inc/inc.php" );

if( !isset( $view_type ) )
{
  $view_type = $_GET['vt'];
  $link = $_GET['l'];
}

$q = sql_query( "select uid from views where link='$link' and type='$view_type' and uid!='" . $API->uid . "' and uid!='$link' order by ts desc limit 5" );

if( mysql_num_rows( $q ) > 0 )
{
  $uids = array();
  while( $r = mysql_fetch_array( $q ) )
    $uids[] = $r['uid'];

  $q = sql_query( "select distinct link from views where type='$view_type' and link!='$link' and uid IN (" . implode( ",", $uids ) . ") order by ts desc limit 5" );
}

$num_results = mysql_num_rows( $q );
if( mysql_num_rows( $q ) > 0 )
{
  echo '<div class="subhead medium_font" style="margin-top: 5px;">Viewers of this ' . ($view_type=='R' ? 'profile' : 'page') . ' also viewed&hellip;</div>';
  echo '<div style="width:100%;">';

  switch( $view_type )
  {
    case 'R':
      while( $r = mysql_fetch_array( $q ) )
      {
        $uid = $r['link'];
        $purl = $API->getProfileURL( $uid );
        $img = "/img/48x48" . $API->getUserPic( $uid );


        $data = queryArray( "select pic, twusername, fbid, name, occupation, company from users where uid='" . $uid . "'" );

        if( sizeof( $data ) > 1 ) {

        $prof = $data['occupation'];
        $name = $data['name'];
        $co = $data['company'];

       	$parts = array(rawurlencode($purl), rawurlencode($API->getUserPic($uid, $data[pic])), rawurlencode($data['twusername']), rawurlencode($data['fbid']), rawurlencode($data['name']));
      	$id = implode("-", $parts);

        ?>
          <div style="width:100%; overflow:hidden; margin-top:5px;">
            <div style="float:left; height:48px; width:48px; margin-right:5px;">
              <? echo '<a id="' . md5(microtime()) . "-" . $id . '" href="' . $purl . '"  title="' . $fids . '" onmouseover="javascript:showTip(3, this);" onmouseout="javascript:tipMouseOut();">'; ?>
              <img src="<?=$img?>" width="48" height="48"></a>
            </div>
            <div style="float:left; width: 240px;font-size:9pt;">
            <?
            echo "<a href=\"" . $purl . "\"><b>" . $name . "</b></a><br />";
            echo '<span style="font-size:8pt;">';
            if( $prof != "" ) echo $prof;
            if( $co != "" && $prof != "" ) echo ", ";
            if( $co != "" ) echo $co;
            echo '</span>';
            ?>
            </div>
          </div>
        <?
        }
      }
    break;

    case 'G':
      while( $r = mysql_fetch_array( $q ) )
      {
        if( $r['link'] > 0 )
          showPagePreview( $r['link'], false );
      }
    break;
  }

  echo "</div>";
}
?>
