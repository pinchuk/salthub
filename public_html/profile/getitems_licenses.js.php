<?php

include "../inc/inc.php";
//header("Content-type: text/javascript");
header("Content-type: text/html");

$tid = intval($_GET['tid']);
$itemsChosen = array();
?>

var tidItem = <?=$tid?>;

<?php
$html = "";

$i = 0;
$customs = array();

$x = sql_query("select max(if(uid=" . $API->uid . ",1,0)) as has,g.gid,g.subcat, g.gname as name,g.popular from pages g left join personal on g.gid=personal.gid where g.cat=$tid group by gid order by gid");
//echo "alert('" . mysql_error() . "');";
//echo "alert( 'select max(if(uid=" . $API->uid . ",1,0)) as has,i.pid,i.name,popular from personal_items i natural left join personal where tid=$tid group by pid order by name');";
echo mysql_error();

ob_start();

$data = array();
while( $r = mysql_fetch_array( $x ) )
{
  $data[] = $r;
}

function displayLicenses( $subcat )
{
  global $data, $customs, $itemsChosen, $i, $tid;

  echo '<div style="margin-top:5px; margin-bottom:5px; margin-left:20px; line-height:200%;">';

  foreach( $data as $item )
  {
//    if( $item['subcat'] != $subcat){
//        continue;
//    }

  	if ($item['popular'] == 0 ) //custom item - a.k.a. one user typed in
    {
      if( $item['has'] == 1){
    		$customs[] = $item['name'];
      }
    }
  	else //popular item - a.k.a. one defined in admin
  	{
  		if ($item['has'] == 1){ //selected
  			$itemsChosen[] = $item;
        }

  		echo "<div class=\"item\" id=\"item-" . $i . "\"><input type=\"checkbox\" " . ($item['has'] == 0 ? "" : "checked") . " onchange=\"javascript:itemChosen(" . $i . ", " . $item['gid'] . ", this.checked, $tid);\" /><span id=\"nameitem-" . $i++ . "\">" . $item['name'] . "</span></div>";
  	}
  }

  echo '</div>';
}
?>
<div style="font-family:Arial;">
  <div style="font-size:10pt; font-weight:bold; color:#555; float:left;">USCG, MCA & RYA Licenses</div>
  <div style="float:right; color:#555; font-size:8pt;">
  view: <a href="#deck">deck licenses</a>&nbsp;&nbsp; <a href="#eng">engineering licenses</a> &nbsp;&nbsp; <a href="#end">certificates</a>
  </div>

  <div style="clear:both;"></div>

  <div style="font-size:10pt; font-weight:bold; margin-top:10px; color:rgb(0, 64, 128);"><u>Deck Licenses</u></div>
</div>

<a name="deck"></a>
<div style="font-family:Arial; line-height:115%">
  <div style="font-size:9pt; font-weight:bold; margin-top:10px;">USCG Master Unlimited Tonnage Licenses</div>
  <div style="font-size:8pt; font-style:normal;">*All suitable to have Sail, Aux Sail endorsements with tonnage limitations &/or Commercial Assisted Towing endorsement</div>
  <? displayLicenses( 1335 ); ?>

  <div style="font-size:9pt; font-weight:bold; margin-top:10px;">USCG Master Limited Tonnage Licenses </div>
  <div style="font-size:8pt; font-style:normal;">*All are limited to max tonnage on which 25% of experience is obtained, OR Limited to 150% tonnage which 50% of service is spent WHICHEVER IS GREATER</div>
  <div style="font-size:8pt; font-style:normal;">*All suitable to have Sail, Aux Sail endorsements with tonnage limitations &/or Commercial Assisted Towing endorsement</div>
  <? displayLicenses( 1336 ); ?>

  <div style="font-size:9pt; font-weight:bold; margin-top:10px;">USCG Master 1600 GRT Licenses</div>
  <div style="font-size:8pt; font-style:normal;">*3000 ITC must be requested to be stated on these licenses</div>
  <div style="font-size:8pt; font-style:normal;">*All suitable to have Sail, Aux Sail endorsements with tonnage limitations &/or Commercial Assisted Towing endorsement</div>
  <? displayLicenses( 1337 ); ?>

  <div style="font-size:9pt; font-weight:bold; margin-top:10px;">USCG Master 500 GRT Licenses</div>
  <div style="font-size:8pt; font-style:normal;">*All suitable to have Sail, Aux Sail endorsements with tonnage limitations &/or Commercial Assisted Towing endorsement</div>
  <? displayLicenses( 1338 ); ?>

  <div style="font-size:9pt; font-weight:bold; margin-top:10px;">USCG Master 200 GRT Licenses</div>
  <div style="font-size:8pt; font-style:normal;">*500 ITC must be requested to be stated on these licenses</div>
  <div style="font-size:8pt; font-style:normal;">*All suitable to have Sail, Aux Sail endorsements with tonnage limitations &/or Commercial Assisted Towing endorsements</div>
  <? displayLicenses( 1339 ); ?>

  <div style="font-size:9pt; font-weight:bold; margin-top:10px;">USCG Master 150 GRT Licenses</div>
  <div style="font-size:8pt; font-style:normal;">*All suitable to have Sail, Aux Sail endorsements with tonnage limitations &/or Commercial Assisted Towing endorsements</div>
  <? displayLicenses( 1340 ); ?>

  <div style="font-size:9pt; font-weight:bold; margin-top:10px;">USCG Master 100 GRT Licenses</div>
  <div style="font-size:8pt; font-style:normal;">*All suitable to have Sail, Aux Sail endorsements with tonnage limitations &/or Commercial Assisted Towing endorsements</div>
  <? displayLicenses( 1341 ); ?>

  <div style="font-size:9pt; font-weight:bold; margin-top:10px;">USCG Master 50 GRT Licenses</div>
  <div style="font-size:8pt; font-style:normal;">*All suitable to have Sail, Aux Sail endorsements with tonnage limitations &/or Commercial Assisted Towing endorsements</div>
  <? displayLicenses( 1342 ); ?>

  <div style="font-size:9pt; font-weight:bold; margin-top:10px;">USCG Master 25 GRT Licenses</div>
  <div style="font-size:8pt; font-style:normal;">*All suitable to have Sail, Aux Sail endorsements with tonnage limitations &/or Commercial Assisted Towing endorsements</div>
  <? displayLicenses( 1343 ); ?>

  <div style="font-size:9pt; font-weight:bold; margin-top:10px;">USCG Master Licenses of Towing Vessels</div>
  <div style="font-size:8pt; font-style:normal;">*STCW requirements only if STCW II/2 is not already held</div>
  <div style="font-size:8pt; font-style:normal;">*Tonnage restrictions apply depending upon previous experience as set out in CFR Title 46 11.464</div>
  <div style="font-size:8pt; font-style:normal;">*Limited means: to towing vessels less than 200grt to a local area within the Great Lakes, Inland Waters, or Western Rivers</div>
  <? displayLicenses( 1344 ); ?>

  <div style="font-size:9pt; font-weight:bold; margin-top:10px;">USCG Master of OSVs</div>
  <? displayLicenses( 1345 ); ?>

  <div style="font-size:9pt; font-weight:bold; margin-top:10px;">USCG Master of Uninspected Fishing Industry Vessels</div>
  <div style="font-size:8pt; font-style:normal;">*All suitable to have Sail, Aux Sail endorsements with tonnage limitations &/or Commercial Assisted Towing endorsements</div>
  <? displayLicenses( 1347 ); ?>

  <div style="font-size:9pt; font-weight:bold; margin-top:10px;">USCG Mate Unlimited Tonnage Licenses</div>
  <div style="font-size:8pt; font-style:normal;">*All suitable to have Sail, Aux Sail endorsements with tonnage limitations &/or Commercial Assisted Towing endorsements</div>
  <? displayLicenses( 1348 ); ?>

  <div style="font-size:9pt; font-weight:bold; margin-top:10px;">USCG Mate Limited Tonnage Licenses</div>
  <div style="font-size:8pt; font-style:normal;">*All are limited to max tonnage on which 25% of experience is obtained, OR Limited to 150% tonnage which 50% of service is spent WHICHEVER IS GREATER</div>
 <div style="font-size:8pt; font-style:normal;">*All suitable to have Sail, Aux Sail endorsements with tonnage limitations &/or Commercial Assisted Towing endorsements</div>
  <? displayLicenses( 1349 ); ?>

  <div style="font-size:9pt; font-weight:bold; margin-top:10px;">USCG Mate 1600 GRT Licenses</div>
  <div style="font-size:8pt; font-style:normal;">*3000 ITC must be requested to be stated on these licenses</div>
  <div style="font-size:8pt; font-style:normal;">*All suitable to have Sail, Aux Sail endorsements with tonnage limitations &/or Commercial Assisted Towing endorsements</div>
  <? displayLicenses( 1350 ); ?>

  <div style="font-size:9pt; font-weight:bold; margin-top:10px;">USCG Mate 500 GRT Licenses</div>
  <div style="font-size:8pt; font-style:normal;">*All suitable to have Sail, Aux Sail endorsements with tonnage limitations &/or Commercial Assisted Towing endorsements</div>
  <? displayLicenses( 1351 ); ?>

  <div style="font-size:9pt; font-weight:bold; margin-top:10px;">USCG Mate 200 GRT Licenses</div>
  <div style="font-size:8pt; font-style:normal;">*500 ITC must be requested to be stated on these licenses</div>
  <div style="font-size:8pt; font-style:normal;">*All suitable to have Sail, Aux Sail endorsements with tonnage limitations &/or Commercial Assisted Towing endorsements</div>
  <? displayLicenses( 1352 ); ?>

  <div style="font-size:9pt; font-weight:bold; margin-top:10px;">USCG Mate 150 GRT Licenses</div>
  <div style="font-size:8pt; font-style:normal;">*All suitable to have Sail, Aux Sail endorsements with tonnage limitations &/or Commercial Assisted Towing endorsements</div>
  <? displayLicenses( 1353 ); ?>

  <div style="font-size:9pt; font-weight:bold; margin-top:10px;">USCG Mate 100 GRT Licenses</div>
  <div style="font-size:8pt; font-style:normal;">*All suitable to have Sail, Aux Sail endorsements with tonnage limitations &/or Commercial Assisted Towing endorsements</div>
  <? displayLicenses( 1354 ); ?>

  <div style="font-size:9pt; font-weight:bold; margin-top:10px;">USCG Mate 50 GRT Licenses</div>
  <div style="font-size:8pt; font-style:normal;">*All suitable to have Sail, Aux Sail endorsements with tonnage limitations &/or Commercial Assisted Towing endorsements</div>
  <? displayLicenses( 1355 ); ?>

  <div style="font-size:9pt; font-weight:bold; margin-top:10px;">USCG Mate 25 GRT Licenses</div>
  <div style="font-size:8pt; font-style:normal;">*All suitable to have Sail, Aux Sail endorsements with tonnage limitations &/or Commercial Assisted Towing endorsements</div>
  <? displayLicenses( 1356 ); ?>

  <div style="font-size:9pt; font-weight:bold; margin-top:10px;">USCG Mate of Towing Vessels Licenses � STCW Requirements only if STCW II/1 is not already held</div>
  <div style="font-size:8pt; font-style:normal;">*Tonnage restrictions apply depending upon previous experience as set out in CFR Title 46 11.20, 11.464(a) Note1, 11.465</div>
  <? displayLicenses( 1357 ); ?>

  <div style="font-size:9pt; font-weight:bold; margin-top:10px;">USCG Apprentice Mate - Towing Vessels (non STCW) Licenses</div>
  <div style="font-size:8pt; font-style:normal;">*Limited means: to towing vessels less than 200grt to a local area within the Great Lakes, Inland Waters, or Western Rivers</div>
  <? displayLicenses( 1358 ); ?>

  <div style="font-size:9pt; font-weight:bold; margin-top:10px;">USCG Mate OSVs Licenses</div>
  <? displayLicenses( 1359 ); ?>

  <div style="font-size:9pt; font-weight:bold; margin-top:10px;">USCG Mate of Uninspected Fishing Industry Vessels Licenses</div>
  <div style="font-size:8pt; font-style:normal;">*All suitable to have Sail, Aux Sail endorsements with tonnage limitations &/or Commercial Assisted Towing endorsements</div>
  <? displayLicenses( 1360 ); ?>

  <div style="font-size:9pt; font-weight:bold; margin-top:10px;">USCG Other Deck Licenses</div>
  <? displayLicenses( 1361 ); ?>

  <div style="font-size:9pt; font-weight:bold; margin-top:10px;">USCG Staff Licenses</div>
  <? displayLicenses( 1362 ); ?>

  <div style="font-size:9pt; font-weight:bold; margin-top:10px;">USCG Special Licenses</div>
  <div style="font-size:8pt; font-style:normal;">*Geographic region restrictions apply as set out in CFR Title 46 11.703</div>
  <div style="font-size:8pt; font-style:normal;">*Geographic region and tonnage restriction apply s set out in CFR Title 46 10.209, 10.223, 10.231</div>
  <? displayLicenses( 1363 ); ?>

  <div style="font-size:9pt; font-weight:bold; margin-top:10px;">USCG Unlicensed Ratings</div>
  <? displayLicenses( 1364 ); ?>

  <div style="font-size:9pt; font-weight:bold; margin-top:10px;">MCA Merchant Navy Deck CoC (Commercial)</div>
  <div style="font-size:8pt; font-style:normal;">*For more detailed information refer to MCA MGN 92 (M) and MCA MSN 156 (M) <a href="http://www.dft.gov.uk/mca/">http://www.dft.gov.uk/mca/</a></div>
  <? displayLicenses( 1365 ); ?>

  <div style="font-size:9pt; font-weight:bold; margin-top:10px;">MCA Yacht Rated (for use on Yachts ONLY) Deck CoC</div>
  <div style="font-size:8pt; font-style:normal;">*for more detailed information refer to MCA MSN 1802 (M)</div>
  <? displayLicenses( 1366 ); ?>

  <div style="font-size:9pt; font-weight:bold; margin-top:10px;">MCA Boatmasters� Licenses � Non Oceans  - Masters only (Commercial)</div>
  <div style="font-size:8pt; font-style:normal;">*for more detailed information refer to MCA MSN 1808 (M), MSN 1776 and visit MCA One Stop Shop <a href="http://www.dft.gov.uk/mca/mcga07-home/workingatsea/mcga-trainingandcert/ds-ss-bml1stop.htm">http://www.dft.gov.uk/mca/mcga07-home/workingatsea/mcga-trainingandcert/ds-ss-bml1stop.htm</a></div>
  <? displayLicenses( 1367 ); ?>

  <div style="font-size:9pt; font-weight:bold; margin-top:10px;">MCA Fishing Deck CoC � (Commercial)</div>
  <div style="font-size:8pt; font-style:normal;">*for more detailed information refer to MCA MGN 121 (M), 411 (M+F) and visit the fishing page. <a href="http://www.dft.gov.uk/mca/mcga07-home/workingatsea/mcga-fishing.htm">http://www.dft.gov.uk/mca/mcga07-home/workingatsea/mcga-fishing.htm</a></div>
  <? displayLicenses( 1368 ); ?>
<!--
  <div style="font-size:9pt; font-weight:bold; margin-top:10px;">Additional MCA Deck Ratings required for "Special Ships"</div>
  <div style="font-size:8pt; font-style:normal;">*for more detailed information refer to MCA MGN 95 (M)</div>
  <? displayLicenses( 1369 ); ?>
-->
  <div style="font-size:9pt; font-weight:bold; margin-top:10px;">RYA Certifications</div>
  <div style="font-size:8pt; font-style:normal;">For more detailed information visit the RYA site: <a href="http://www.rya.org.uk/Pages/Home.aspx">http://www.rya.org.uk/Pages/Home.aspx</a></div>
  <? displayLicenses( 1370 ); ?>

  <div style="font-size:9pt; font-weight:bold; margin-top:10px;">RYA Instructors - Yachts</div>
  <? displayLicenses( 1371 ); ?>

  <div style="font-size:9pt; font-weight:bold; margin-top:10px;">RYA Instructors � Dinghys, Keelboards, Multihulls, PWC</div>
  <? displayLicenses( 1372 ); ?>

  <a name="eng"></a>
  <div style="font-size:10pt; font-weight:bold; color:rgb(0, 64, 128); margin-top:20px;"><u>Engineering Licenses</u></div>

  <div style="font-size:9pt; font-weight:bold; margin-top:10px;">USCG Engineer Licenses</div>
  <div style="font-size:8pt; font-style:normal;">*Horsepower limitations apply as set out in CFR Title 46 11</div>
  <div style="font-size:8pt; font-style:normal;">* Mode of Propulsion limitations apply (Steam/Motor/Gas Turbine) as set out in CFR Title 46 11</div>
  <? displayLicenses( 1373 ); ?>

  <div style="font-size:9pt; font-weight:bold; margin-top:10px;">USCG Engineer OSVs Licenses</div>
  <? displayLicenses( 1374 ); ?>

  <div style="font-size:9pt; font-weight:bold; margin-top:10px;">USCG Engineer MODU Licenses</div>
  <? displayLicenses( 1375 ); ?>

  <div style="font-size:9pt; font-weight:bold; margin-top:10px;">USCG Engineer Uninspected Fishing Vessels Licenses</div>
  <? displayLicenses( 1376 ); ?>

  <div style="font-size:9pt; font-weight:bold; margin-top:10px;">USCG Engineer QMED Licenses</div>
  <? displayLicenses( 1384 ); ?>

<!--  <div style="font-size:9pt; font-weight:bold; margin-top:10px;">USCG Unlicensed Ratings</div>
  <? displayLicenses( 1377 ); ?>
-->
  <div style="font-size:9pt; font-weight:bold; margin-top:10px;">MCA Merchant Navy Engineering CoC � (Commercial)</div>
  <div style="font-size:8pt; font-style:normal;">*For more detailed information refer to MCA MGN 93(M), MSN 156 (M)</div>
  <? displayLicenses( 1378 ); ?>

  <div style="font-size:9pt; font-weight:bold; margin-top:10px;">MCA Yacht Rated (for use on Yachts ONLY) Engineering CoC</div>
  <div style="font-size:8pt; font-style:normal;">*for more detailed information refer to MCA MSN 1802 (M)</div>
  <? displayLicenses( 1379 ); ?>

  <div style="font-size:9pt; font-weight:bold; margin-top:10px;">MCA Fishing Engineering CoC � (Commercial)</div>
  <div style="font-size:8pt; font-style:normal;">*for more detailed information refer to MCA MGN 121 (M), 411 (M+F)</div>
  <? displayLicenses( 1380 ); ?>

  <a name="end"></a>
  <div style="font-size:10pt; font-weight:bold; margin-top:10px; color:rgb(0, 64, 128);"><u>Certificates</u></div>

  <div style="font-size:9pt; font-weight:bold; margin-top:10px;">USCG Deck Certificates</div>
  <? displayLicenses( 1381 ); ?>

  <div style="font-size:9pt; font-weight:bold; margin-top:10px;">USCG Engineering Certificates</div>
  <? displayLicenses( 1385 ); ?>

  <div style="font-size:9pt; font-weight:bold; margin-top:10px;">MCA Deck Certificates</div>
  <? displayLicenses( 1382 ); ?>

  <div style="font-size:9pt; font-weight:bold; margin-top:10px;">MCA Engineering Certificates</div>
  <? displayLicenses( 1386 ); ?>

  <div style="font-size:9pt; font-weight:bold; margin-top:10px;">RYA Deck Certificates</div>
  <? displayLicenses( 1383 ); ?>

  <div style="font-size:9pt; font-weight:bold; margin-top:10px;">RYA Engineering Certificates</div>
  <? displayLicenses( 1387 ); ?>

</div>
<?

$html = ob_get_contents();
ob_end_clean();

echo "html = '';";
for( $c = 0; $c < strlen( $html ); $c+= 150 )
{
  echo "html += '" . str_replace( "\r", "", str_replace( "\n", "", str_replace( "'", "\'", substr( $html, $c, 150 ) ) ) ) . "';\n";
}

?>

<?
if ($_GET['i'] == "1") { ?>

document.getElementById("chooser-<? echo $tid ?>").innerHTML = html;
searchItems(<? echo $tid ?>);

<?php } ?>

<?
    $itemsChosen = array();
    foreach ($data as $value){
        if ($value['has'] == 1 && $value['popular'] != 0)
            $itemsChosen[] = $value;
    }
?>

itemsChosen[<? echo $tid ?>] = [<?php
$i = 0;

foreach ($itemsChosen as $item)
{
	if ($i > 0) echo ", ";
	echo "[" . $item['gid'] . ", '" . addslashes($item['name']) . "']";
	$i++;
}
?>];
<? $customs = array_unique($customs); ?>
document.getElementById("customitems-" + <? echo $tid ?>).value = "<?=implode(", ", $customs)?>";

updateProfilePreview(<? echo $tid ?>);
refreshSelections(<? echo $tid ?> );