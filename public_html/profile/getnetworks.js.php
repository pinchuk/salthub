<?php

include "../inc/inc.php";

$uid = intval($_GET['uid']);

if ($uid == 0)
	$uid = $API->uid;

$networks = array();
/*
This following query wasn't working; the user had to be in the network, before he/she could see it.

$x = mysql_query("
	select max(if(uid=$uid,1,0)) as member,nid,name,popular,city,region,country from networks n
		natural left join network_users u
		join loc_city c on n.cid=c.id
		join loc_state s on c.region_id=s.id
		join loc_country cnt on cnt.id=s.country_id
	where u.uid=$uid
	group by nid order by name
	");
*/


$x = mysql_query("select pages.gid as nid,pages.gname as name,pages.address,pages.popular from pages where pages.subcat='" . CAT_CLUB_ASSOC . "' order by pages.gname");
/*

	select nid,name,popular,city,region,country,gid from networks n
		join loc_city c on n.cid=c.id
		join loc_state s on c.region_id=s.id
		join loc_country cnt on cnt.id=s.country_id
	group by nid order by name
	");

*/


$memberships = array();
$q = mysql_query("select pages.gid from page_members left join pages on page_members.gid=pages.gid where (pages.cat=" . CAT_CLUB_ASSOC . " OR pages.subcat=" . CAT_CLUB_ASSOC . ")and page_members.uid='$uid'");
//$q = mysql_query( "select * from network_users where uid='$uid'" );
while( $r = mysql_fetch_array( $q ) )
{
  $memberships[] = $r['gid'];
}

while ($y = mysql_fetch_array($x, MYSQL_ASSOC))
{
  $y['member'] = 0;
  if(in_array( $y['nid'], $memberships ) ) $y['member'] = 1;

  $y['address'] = str_replace( "<br />", " ", nl2br( $y['address'] ) );

	$networks[] = $y;
}

?>

var userNetworks = <?=json_encode($networks)?>;

function toggleNetworkChooser(i)
{
	e = document.getElementById("networkchooser" + i);
	isShown = e.style.display == "inline";

	html = "";
	
	if (!isShown) // load item choices
	{
		for (var j in userNetworks)
		{
			if (i == 0 && userNetworks[j].popular == "1")
				html += getNetworkDDHTML(userNetworks[j]);
	
			if (i == 1)
				html += getNetworkDDHTML(userNetworks[j]);
		}
	}
	
	document.getElementById("netchooser" + i).innerHTML = html;

	e.style.display = isShown ? "none" : "inline";
}

function networkChecked(nid, checked)
{
	for (var i in userNetworks)
		if (userNetworks[i].nid == nid)
		{
			userNetworks[i].member = checked ? 1 : 0;
			break;
		}

  refreshNetworkSelections();
}

function refreshNetworkSelections()
{
  html = '';

  e = document.getElementById( "netselections" );
  if( e )
  {
  	for (var i in userNetworks)
  		if (userNetworks[i] != null)
	  		if (userNetworks[i].member == 1)
      	{
          html += '<a href="/page/' + userNetworks[i].nid + '-fwd">' + userNetworks[i].name + "</a><br />";
        }

    if( html == '' )
      html = 'use dropdown above to select';
    e.innerHTML = html;
  }
}

function getNetworkDDHTML(net)
{
	html  = '<div class="networkdd">';
	html += '	<div class="chk">';
	html += '		<input type="checkbox" value="' + net.nid + '" ' + (net.member == '1' ? 'checked' : '') + ' onchange="javascript:networkChecked(' + net.nid + ', this.checked);" />';
	html += '	</div>';
	html += '	<div class="name">';
	html += '		' + net.name;
if( net.address != "" )
	html += '		<div class="loc">in ' + net.address + '</div>';
	html += '	</div>';
	html += '	<div style="clear: both;"></div>';
	html += '</div>';

	return html;
}

function getNetworkHTML(net, edit)
{
	html  = '<div class="network' + (edit ? "" : " network2") + '">';
	html += '	<a href="/page/' + net.nid + '-fwd">' + net.name + '</a><br />';
	if (edit)
	{
	html += '	<div class="editlinks">';
	html += '		<a href="javascript:void(0);" onclick="javascript:if (confirm(\'Are you sure you want to leave this network?\')) networkChecked(' + net.nid + ', false); saveNetworks();">Leave network</a> &nbsp;&nbsp; <a href="javascript:void(0);" onclick="javascript:loadShare(\'page\', 1, {\'gid\':\''+net.gid+'\', \'gname\':\'' +  net.name + '\'} );">Invite contacts to this network</a>';
	html += '	</div>';
	}
	html += '</div>';
	
	return html;
}

function saveNetworks()
{
	nids = "";
	
	for (var i in userNetworks)
		if (userNetworks[i].member == "1")
			nids += "," + userNetworks[i].nid;
	
	postAjax("/profile/savenet.php", "nids=" + nids.substring(1), "showNetworks");

	//update the page "behind" the network edit
	showNetworks();
}


function showNetworks()
{
	htmlNet = "";
	htmlEdit = "";
	nets = 0;

	for (var i in userNetworks)
	{
		if (userNetworks[i].member == "1")
		{
			htmlNet += getNetworkHTML(userNetworks[i], false);
			htmlEdit += getNetworkHTML(userNetworks[i], true);
			nets++;
		}
	}

	if (nets == 0)
	{
		document.getElementById("networks").innerHTML = '<div class="nodata">Members of your real world clubs &amp; associations are on <?=$siteName?>.&nbsp; Enter a yacht club, watersports team, club, association, or any other networks to which you belong.&nbsp; Then, you can connect and exchange information with others from the same clubs &amp; associations.</div>';
//		document.getElementById("networks-belong").innerHTML = "&nbsp;";
	}
	else
	{
		document.getElementById("networks").innerHTML = '<div class="smtitle2" style="float:left; color:#555; width:100px; padding-left:5px;">Connected To</div><div style="float:left;">' + htmlNet + '</div><div style="clear: both;"></div>';
//		document.getElementById("networks-belong").innerHTML = htmlEdit;
	}

}

function searchNetworks(i, q)
{
	html = "";
	
	q = q.toLowerCase();
	
	for (var j in userNetworks)
	{
		if (i == 0 && userNetworks[j].popular == "1")
			if (userNetworks[j].name.toLowerCase().indexOf(q) > -1)
				html += getNetworkDDHTML(userNetworks[j]);

		if (i == 1)
			if (userNetworks[j].name.toLowerCase().indexOf(q) > -1)
				html += getNetworkDDHTML(userNetworks[j]);
	}
	
	document.getElementById("netchooser" + i).innerHTML = html;

	e = document.getElementById("networkchooser" + i);
	e.style.display = "inline";
}

refreshNetworkSelections();
showNetworks();

if (isLoggedIn == user.uid || <?=count($networks)?> > 0) document.getElementById('networkscontainer').style.display = "";