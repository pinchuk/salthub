<?php
$_GET['type'] = $action == "videos" ? "V" : "P";
$_GET['uid'] = $user['uid'];
$_GET['p'] = 0;

$word = typeToWord($_GET['type']);

?>

<div class="subhead"><?=ucwords($word)?>s of <?=$gid == 0 ? $user['name'] : "You in " . $page['gname']?></div>
<div id="usermedia-1"><?php $_GET['tagged'] = 1; include "media_ajax.php"; ?></div>

<div class="subhead" style="margin-top: <?=$gid == 0 ? 15 : 0?>px;"><?=ucwords($word) . "s" . ($gid == 0 ? " by " . $user['name'] : "")?></div>
<div id="usermedia-0"><?php $_GET['tagged'] = 0; include "media_ajax.php"; ?></div>