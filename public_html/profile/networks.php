<div onmouseover="javascript:document.getElementById('networkseditlink').style.visibility = 'visible';" onmouseout="javascript:document.getElementById('networkseditlink').style.visibility = 'hidden';">
	<div class="edit" id="networkseditlink" style="padding-right: 5px;"><a href="javascript:void(0);" onclick="javascript:toggleEdit('networks');"><?php if ($user['uid'] == $API->uid) echo "edit"; ?></a></div>
	<div id="networks"></div>
</div>

<div id="networksedit" style="display: none;">
	<div style="font-size: 8pt; margin-bottom:10px;">
		Members of your real world clubs &amp; associations are on <?=$siteName?>.&nbsp; Select a yacht club, watersports team, club, association, or any other networks to which you belong.&nbsp;
		Then, you can connect and exchange information with others from the same clubs &amp; associations.
	</div>

	<div style="float: left; width:250px;">      <? $i = 0; ?>
    	<div class="smtitle2" style="padding-bottom: 3px;">Popular Clubs &amp; Associations:</div>
    	<div style="width: 200px; height: 18px; border: 1px solid #555; position: relative;">
    		<input type="text" onkeyup="javascript:searchNetworks(<?=$i?>, this.value);" id="networksearch<?=$i?>" style="margin: 0; padding: 0px 2px; border: 0; width: 179px; height: 18px;"><img src="/images/dropdown.png" onclick="javascript:toggleNetworkChooser(<?=$i?>);" style="cursor: pointer; vertical-align: top;" alt="" />
    		<div id="networkchooser<?=$i?>" class="itemchooser" style="z-index: <?=500 - $i?>;">
    			<div id="netchooser<?=$i?>" class="chooser"></div>
    			<div style="padding: 5px; text-align: center; background: #fff; border-top: 1px solid #555;">
    				<input type="button" class="button" value="Apply" onclick="javascript:saveNetworks(); toggleNetworkChooser(<?=$i?>);" /> &nbsp; <input type="button" class="button" value="Cancel" onclick="javascript:toggleNetworkChooser(<?=$i?>);" />
    			</div>
    		</div>
    	</div>

      <div style="clear:both; padding-top: 5px; font-size: 8pt; color:rgb(50, 103, 152);">Your Selections</div>
      <div id="netselections" style="font-size:8pt;"></div>

  </div>

  <div style="float:right; width:250px;">
      <div style="text-align: center; width: 202px; font-size: 9pt; font-weight: bold;">
			Don't see your club or association?
      <div style="margin-top:3px;">
  			<input type="button" class="button" value="Add New" onclick="javascript: location='/pages/page_create.php?sel=5';" />
			  <input type="button" class="button" value="Close" onclick="javascript:toggleEdit('networks');" style="margin-left:15px;"/>
      </div>
		</div>
	</div>
</div>

<div style="clear: both; height: 15px;"></div>

<script language="javascript" type="text/javascript">
<!--
loadjscssfile("/profile/getnetworks.js.php?uid=<?=$user['uid']?>", "js");
//-->
</script>