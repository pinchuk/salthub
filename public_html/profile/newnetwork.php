<?php

include "../inc/inc.php";

$badwords = array(
	"shit",
	"fuck",
	"cunt",
	"bitch",
	"goddamn",
	"god damn",
	" ass "
	);

$name = trim($_POST['n']);
$cid = intval($_POST['c']);

if (strlen($name) == 0)
	die(json_encode(array("error" => "You must enter a name for the network.")));

$lname = strtolower(stripslashes($name));

foreach ($badwords as $word)
	if (strpos($lname, $word) !== false)
		die(json_encode(array("error" => "The network name cannot contain profanities.")));

$gid = 0;
if( quickQuery( "select count(*) from pages where gname='$name'" ) == 0 )
{
  $x = mysql_query( "select region, country, city from loc_city left join loc_state s on loc_city.region_id=s.id left join loc_country cnt on cnt.id=s.country_id where loc_city.id=$cid" );

  $address = '';

  if( $y = mysql_fetch_array( $x ) )
  {
    $address =ucwords(strtolower( $y['city'] )) . ", " . ucwords(strtolower($y['region'])) . " \n" . $y['country'];
  }

  mysql_query("insert into pages (gname,gtype,subcat,address) values ('$name'," . PAGE_TYPE_BUSINESS . "," . CAT_NETWORKS . ",'$address')");
  $gid = mysql_insert_id();

}


mysql_query("insert into networks (name,cid,gid) values ('$name',$cid,$gid)");
$nid = mysql_insert_id();

$x = mysql_query("
	select 1 as member,nid,name,popular,city,region,country from networks n
		natural left join network_users
		join loc_city c on n.cid=c.id
		join loc_state s on c.region_id=s.id
		join loc_country cnt on cnt.id=s.country_id
	where nid=$nid
	group by nid order by name
	");

$net = mysql_fetch_array($x, MYSQL_ASSOC);

foreach (array("city", "region", "country") as $i)
	$net[$i] = ucwords(strtolower($net[$i]));

echo json_encode($net);


?>