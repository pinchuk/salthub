<?php

include_once "../inc/inc.php";

if (empty($user['uid']))
	$user['uid'] = intval($_GET['uid']);

$piTypes = array(0 => array("tid" => "about", "name" => "About Me"));
	
$x = mysql_query("select tid,name,special from personal_types order by name");
while ($y = mysql_fetch_array($x, MYSQL_ASSOC))
	$piTypes[$y['tid']] = $y;

unset( $info );

$x = mysql_query("select g.gid,g.gname as name,t.tid,t.name as tname,special from personal p left join pages g on g.gid=p.gid left join personal_types t on g.cat=t.tid where p.uid=" . $user['uid'] . " order by special,t.name,name") or die(mysql_error());
while ($y = mysql_fetch_array($x, MYSQL_ASSOC))
	$info[] = $y;

foreach ($piTypes as $piType)
{
	$html = "";

	if ($piType['special'] > 0) continue; // for licenses
	if ($piType['tid'] == "about") continue; // about used to be here - left the code below in case todd wants it back someday
	
	$html .= '<div class="personalinfo" onmouseover="javascript:piMouseOver(\'' . $piType['tid'] . '\', true);" onmouseout="javascript:piMouseOver(\'' . $piType['tid'] . '\', false);">';
	$html .= '	<div class="cat">' . $piType['name'] . ':</div>';
	$html .= '	<div style="float: left;">';
	$html .= '		<div id="items-display-' . $piType['tid'] . '">';
	$html .= '			<div class="items" id="items-' . $piType['tid'] . '">';
	$items = 0;

	if ($piType['tid'] == "about")
	{
		$txtAbout = quickQuery("select txt from aboutme where uid=" . $user['uid']);
		if (empty($txtAbout))
			$items = 0;
		else
		{
			$html .= $API->convertChars(nl2br($txtAbout)); //str_replace("\n", "<br />", $txtAbout);
			$items = 1;
		}
	}
	else
	{
		ob_start();
		$items = displayPersonalInfo($piType, $info);
		$html .= ob_get_contents();
		ob_end_clean();
	}
	
	if ($items == 0)
	{
		if ($user['uid'] == $API->uid)
			$html .= 'You don\'t have any listed.&nbsp; Add one by <a href="javascript:void(0);" onclick="javascript:piEdit(\'' . $piType['tid'] . '\');">clicking here</a>.';
		else // don't display anything
			continue;
	}
	$html .= '			</div>';
	$html .= '			<div class="edit" id="piedit-' . $piType['tid'] . '"><a href="javascript:void(0);" onclick="javascript:piEdit(\'' . $piType['tid'] . '\');">' . ($user['uid'] == $API->uid ? 'edit' : '') . '</a></div>';
	$html .= '		</div>';
	$html .= '		<div class="itemchooserparent" id="itemchooserparent-' . $piType['tid'] . '"></div>';
	$html .= '	</div>';
	$html .= '</div>';
	
	echo $html;
}
?>

<div style="clear: both;"></div>

<script language="javascript" type="text/javascript">
<!--
function piMouseOver(i, over)
{
	document.getElementById("piedit-" + i).style.visibility = over ? "visible" : "hidden";
}

var txtAbout = "<?=str_replace("\n", "\\n", addslashes($txtAbout))?>";

//-->
</script>