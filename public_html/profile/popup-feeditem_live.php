<?
/*
This script loads a single feed item to be displayed in a popup window.  Called from /common.js
*/

include_once "../inc/inc.php";
include_once "getlogentries.php";
include_once "../inc/mod_comments.php";

if( empty( $fid ) ) $fid = $_GET['fid'];

$uid = quickQuery( "select uid from feed where fid='$fid'" );
$gid = quickQuery( "select gid from feed where fid='$fid'" );

?>

<div style="width: 630px;">
  <div class="trending" style="float:left; width: 450px; height: 550px; padding: 10px; <?if( empty( $no_scroll ) ) { echo 'overflow-y: scroll; overflow-x:hidden;'; } ?> margin-bottom:5px;">
  <? getLogEntries(array("gid" => $gid, "uid" => $uid, "mini" => 0, "feedLimit" => 1, "olderthan" => $fid + 1 )); ?>
  </div>

  <div style="float:left; width:150px;">
  <? if( $API->adv ) { ?>
	<div style="padding: 10px 0 0 15px;">
		<?php showAd("skyscraper"); ?>
	</div>
  <? } ?>
  </div>

  <div style="clear:both;"></div>
</div>
