function showBlueGrad()
{
	e = document.getElementById("bluegrad");
	
	if (e)
	{
		h = getHeight(e);
		e.style.backgroundImage = "url(/profile/bluegrad.php?h=" + h + ")";
	}
}

function tbHide()
{
	document.getElementById("divtb1").style.display = "none";
	document.getElementById("divtb2").style.display = "none";
}

function deleteFeed(id, hash)
{
	postAjax("/profile/deletefeed.php", "id=" + id + "&hash=" + hash, "deleteFeedHandler");
}

function deleteFeedHandler(id)
{
	document.getElementById("feed-" + id).style.display = "none";

  var k = document.getElementById("deletefeed-" + id)
  if( k )
  	k.style.display = "none";
}

function tlfGo(n)
{
	getAjax("/profile/thelatestfrom.php?n=" + n + "&cur=" + tlfCurrent, "tlfGoHandler");
}

function tlfGoHandler(html)
{
	x = html.split(":");
	tlfCurrent = x[0];
	html = html.substring(tlfCurrent.length + 1);
	document.getElementById('tlfbubble').innerHTML = html;
}

var pageUserMediaTagged;
function pageUserMedia(uid, t, isTagged, p, gid)
{
	pageUserMediaTagged = isTagged;
	data = "gid=" + gid + "&type=" + t + "&tagged=" + isTagged + "&p=" + p + "&uid=" + uid;
	getAjax("/profile/media_ajax.php?" + data, pageUserMediaHandler);
}

function pageUserMediaHandler(data)
{
	document.getElementById("usermedia-" + pageUserMediaTagged).innerHTML = data;
}