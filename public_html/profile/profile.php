<?php
/*
This file is the basis for all the personal profile and "Page" screens.  External files are called from where to change the content of the pages
as needed based on query string values that are passed in.  Users don't access this page directly, they are forwarded here from the .htaccess
file, if they attempt to access something in /user or /page

Change Log
11/4/2011 - Added support for facebook linter.
9/1/2012 - Added support for Likes/Tweets of "Live Vessel Tracking"
9/28/2012 - Modified Meta data for Vessel pages, /LikeTrack
9/29/2012 - Added support for /NavHistory for vessel pages (shows "vessel track")
12/15/2012 - Changed title for SEO, made pages accessible through "/vessel/ID-Name" URL
*/

//keep register_globals = off
// ini_set("display_errors", 1);
// error_reporting(E_ALL);
if( stristr( $_SERVER['HTTP_USER_AGENT'], "facebookexternal" ) !== false )
{
  $facebookCrawler = true;
  $noLogin = true;
}

include "../inc/inc.php";


$uid = intval($_GET['uid']);
$action = strtolower($_GET['action']);
$gid = intval($_GET['gid']);

if( $facebookCrawler )
{
  $API->uid = 91;
}

if ($gid > 0)
{
	$page = $API->getPageInfo($gid);
  $user = $page;
  $user['uid'] = $page['gid'];
	if (empty($page)){
		die("Page not found");
    }

  if( quickQuery( "select industry from pages natural join categories where pages.gid='" . $gid . "'" ) )
  {
    if( isset( $_SESSION['recentPages'] ) )
    {
      $recentPages = explode( ",", $_SESSION['recentPages'] );


      array_unshift( $recentPages, $gid );

      if( sizeof( $recentPages ) > 30 )
        array_pop( $recentPages );

      $recentPages = array_unique( $recentPages );
    }
    else
      $recentPages = array( $gid );

    $_SESSION['recentPages'] = implode(",",$recentPages);
  }
  //This is to allow pages to be public if they're companies.
  $anonymousPageTypes = array( PAGE_TYPE_BUSINESS, ); //, PAGE_TYPE_VESSEL );
  $anonymousPageCategories = array( );// CAT_PROFESSIONS );

  if( ( in_array( $page['type'], $anonymousPageTypes ) || in_array( $page['cat'], $anonymousPageCategories ) ) && //Check page types and cateogires
        ($action == "about" || $action == "" || ($action == "logbook" && $gid==15528)) && //Check to make sure we're in the right section
        !$API->IsLoggedIn() || isset($_GET['curl'])) // And not logged in..
  {
    $noLogin = true;
    $API->uid = 0;
  }
  else
  {
    $API->requireLogin();

  	if (quickQuery("select count(*) from page_blocked where gid=$gid and uid={$API->uid}") == 1)
  	{
  		die("<script>alert('You have been banned from this page.');window.location.href = '/pages';</script>");
  	}

    if( $page['member'] == 0 && $page['privacy'] == PRIVACY_SELECTED && !$API->admin)
    {
      header( "Location: /pages/?private" );
      exit;
    }
  }
}
else
{
  $noLogin = true;
}

if( $noLogin && $facebookCrawler != true)
  if( !isSSL() )
    transitionToSSL();

if ($uid == 0)
{
	if ($_GET['name']) //look up uid from username
	{
    $original_name = $_GET['name'];
    if( $site != "m" ){
        $_GET['name'] = str_replace( ".", " ", $_GET['name'] );
    }

    $inactive = false;

    $user = queryArray("select uid, active, verify from users where username='" . $_GET['name'] . "'");

    $uid = intval( $user['uid'] );
		if ($uid == 0)
		{
      $user = queryArray("select uid, active, verify from users where username='" . $original_name . "'");

      $uid = intval( $user['uid'] );
      if( $uid == 0 )
      {
  			header("Location: /404.php");
	  		die();
      }
		}

        if( $user['verify'] == 0 || $user['active'] == 0 )
        {
            header("Location: /404.php?inactive=1");
            die();
        }
	}
} // if( $uid == 0 )
else
{
  if( intval( quickQuery( "select active from users where uid='$uid'" ) ) != 1 )
  {
  	header("Location: /404.php?inactive=1");
 		die();
  }
}

if ($uid == 0 && !$API->isLoggedIn() && !$noLogin)
{
	header("Location: /404.php");
	die();
}
elseif ($uid == 0)
	$uid = $API->uid;


if( $gid == 0 )
{
  $x = mysql_query("select * from users where uid='$uid' and active=1");

  if (mysql_num_rows($x) > 0)
  {
    $user = mysql_fetch_array($x, MYSQL_ASSOC);
    $user['cfriends'] = $API->getFriendsCount($user['uid']);
  }
  else
  {
    $user['cfriends'] = 0;
  }
}
else
{
  $user['cfriends'] = $API->getPageMembersCount($gid);
}

$pg = intval($_GET['p']);

$mediaCount = $API->getMediaCount($gid == 0 ? $uid : $gid, $gid > 0);

$vidCount = $mediaCount[0];
$picCount = $mediaCount[1];
$albCount = $mediaCount[2];

if( $action == "contactinfo" || $action=="contactfor" ) $action = "about";

if ($_GET['t'] == "P" || $action == "photos")
{
	$type = "P";
	$word = "Photos";
	$perPage = 5;
	$pages = ceil($albCount / $perPage);
  if( $site == "m" )
  	$mediaQuery = mysql_query("select @type:='$type',media.id as aid,media.title,media.descr,media.created,hash,photos.id,mainImage from albums as media inner join photos on mainImage=photos.id where media.uid=$uid order by created desc limit " . ($pg * $perPage) . ",$perPage");
  else
  	$mediaQuery = mysql_query("select @type:='$type',media.id as aid,media.title,media.descr,media.created,hash,photos.id,mainImage from albums as media inner join photos on mainImage=photos.id where" . $API->getPrivacyQuery() . "media.uid=$uid order by created desc limit " . ($pg * $perPage) . ",$perPage");

}
elseif ($_GET['t'] == "V" || $action == "videos")
{
	$type = "V";
	$word = "Videos";
	$perPage = 10;
	$pages = ceil($vidCount / $perPage);
	$mediaQuery = mysql_query("select @type:='$type',id,hash,views,title,descr,created,jmp from videos as media where" . $API->getPrivacyQuery(null, true, "", false) . " ready=1 and uid=" . $uid . " order by created desc limit " . ($pg * $perPage) . ",$perPage");
}
elseif( $action == "logbook" || $action == "logbook2")
{
  $word = "Profile";
  $action = "profile";
}
elseif ($action == "about")
{
	$word = "About Me Page";
}
elseif( $action == "editpage" && $gid > 0 )
{
  $word = "Edit Page";
}
else
{
  //DEFAULT PAGE
/*
	$action = "profile";
	$word = "Profile";
*/
	$action = "about";
	$word = "About Me Page";
}



if (isset($type))
{
	$scripts[] = "http://s7.addthis.com/js/250/addthis_widget.js#username=mediabirdy";
	$scripts[] = "/email.js";
}

if ($gid == 0){
  switch( $site )
  {
    case "m": $title = $user['name'] . "'s $word"; break;
    case "s":
//        $q = mysql_query("select uid from users where name like '%".$user['name']."%'");
//        if ($q !== false){
//            while ($row = mysql_fetch_assoc($q)){
//                $uid = $row['uid'];
//            }
//        }

        $q = mysql_query("select txt from aboutme where uid = ".$user['uid']);
        if ($q !== false){
            $descr = 'No description';
            while ($row = mysql_fetch_assoc($q)){
                $descr = $row['txt'];
            }
        }
        
        $meta_url = "http://" . SERVER_HOST . $API->getProfileURL($user['uid'], $user['username']);
        $page = $API->getProfileURL($user['uid'], $user['username']);


        $meta[] = array( "property" => "og:description", "content" => $API->convertChars($descr) );
        $meta[] = array( "property" => "og:title", "content" => $user['name'] );
        $meta[] = array( "property" => "og:url", "content" => $meta_url );
//        $meta[] = array( "property" => "og:image", "content" => "http://" . SERVER_HOST . $API->getThumbURL(0, 119, 95, $API->getUserPic($user['uid'])) );
        $meta[] = array( "property" => "og:site_name", "content" => $siteName );
        $meta[] = array( "property" => "og:type", "content" => "article" );
        $meta[] = array( "property" => "fb:app_id", "content" => $fbAppId );

//      $title = $user['name'];
//      if( $user['company'] != '' ) $title .= " - " . $user['company'];
//      if( $user['occupation'] != '' ) $title .= " - " . $user['occupation'];
////      $user['aboutme'] = quickQuery("select txt from aboutme where uid='" . $API->uid . "'" );
////      if( $user['aboutme'] != '' ) $title .= " - " . $user['aboutme'];
//      $title .= " - Resume - About";
//
//      $firstname = strtok( $user['name'], " " );
//      $descr = "View " . $user['name'] . "'s professional profile on $siteName. The world's largest maritime network, helping professionals like $firstname discover Businesses, Professionals, Vessels, Marine Traffic, Maritime Jobs, Maritime News, Media.";
    break;
  }
}
else
{
  $name = $page['gname'];
  if( $page['type'] == PAGE_TYPE_VESSEL )
  {
    $exnames = quickQuery( "select exnames from boats left join pages on pages.glink=boats.id where pages.gid='" . $page['gid'] . "'" );
    $length = quickQuery( "select length from boats left join pages on pages.glink=boats.id where pages.gid='" . $page['gid'] . "'" );

    if( $page['subcatname'] != "" )
      $name = $page['subcatname'] . " - " . $name;
    if( $exnames != "" ) $name .= " (ex. " . $exnames . ")";
    if( $length != "" ) $name .= " - " . $length . " ft. / " . round($length / 3.2808399, 2) . " m.";
  }


  $title = $name;
  if( $page['subcatname'] != "" && $page['type'] != PAGE_TYPE_VESSEL )
  {
    $title .= " - " . $page['subcatname'];
  }

  if( strlen( $title ) < 55 )
  {
    if( $page['catname'] != "" ) $title .= $page['catname'];
      $title .= " photo and video";
  }


  $descr = strip_tags( $page['descr'] );

  if( strlen( $descr ) < 150 )
  {
    $members='';
    $q = sql_query( "select name from users left join page_members on page_members.uid=users.uid where page_members.gid='$gid'" );
    while( $r = mysql_fetch_array( $q ) )
    {
      if( strlen( $members ) > 0 ) $members .= ",";
      $members .= $r['name'];
    }
  }


  $meta_url = "http://" . SERVER_HOST . $API->getPageURL($page['gid'], $page['gname'] );

  if( strstr( $_SERVER['REQUEST_URI'], "LiveTrack" ) !== false || strstr( $_SERVER['REQUEST_URI'], "NavHistory" ) !== false  ) {
    $title = "Track " . $name;
    $descr = "View the current location and track of $name Click on image to view more live data, specs, photos & to connect with this vessel & over 100K others.";
    $meta_url .= "/LiveTrack";
  }

  $meta[] = array( "property" => "og:description", "content" => $descr );
  $meta[] = array( "property" => "og:title", "content" => strip_tags($title) );
  $meta[] = array( "property" => "og:url", "content" => $meta_url );

//  $meta[] = array( "property" => "og:image:width", "content" => 500 );
//  $meta[] = array( "property" => "og:image:height", "content" => 500 );

  $meta[] = array( "property" => "og:image", "content" => "http://" . SERVER_HOST . $API->getThumbURL(0, 300, 300, $page['profile_pic'] ) );
  $meta[] = array( "property" => "og:site_name", "content" => $siteName );
  $meta[] = array( "property" => "og:type", "content" => "article" );
  $meta[] = array( "property" => "fb:app_id", "content" => $fbAppId );

  $descr .= "-" . $members;

  $descr = cutOffText( strip_tags( $descr  ), 145 );
}

$background = "#fff";
$jsVar['action'] = $action;
$scripts[] = "/showembedmedia.js";
$scripts[] = "/profile/profile.js";
$scripts[] = "/pages/media_chooser.js.php";
$scripts[] = "/comments.js";
$scripts[] = "/share.js.php";
$scripts[] = "/profile/employment.js.php";
//$scripts[] = "/report.js.php";
$scripts[] = "/yesnobox.js";
$scripts[] = "/actb.js";
if ($gid > 0)
{
  $scripts[] = "/pages/pages.js.php";
  $scripts[] = "/pages/multi_select.js.php?gid=$gid";
}
include "../header.php";

if ($isDevServer)
	echo '<div style="position: absolute; top: 150px; left: 30px; color: red; font-weight: bold;">' . $user['uid'] . '</div>';

?>

<div style="clear: both;"></div>

<?php

$showPrivacyMessage = false;

if( $gid == 0 )
{ 
  // 0 = default (logbook) - "cut-in" on left
  // 1 = video, photo, about pages - solid box
  // 2 = mb - no borders
  
  
  if( $site == "s" && $action == "profile" && !$API->userHasAccess($user['uid'], PRIVACY_LOG ) )   { $action = "about";  }
  if( ( $site == "s" && $action == "videos" && !$API->userHasAccess($user['uid'], PRIVACY_VIDEOS ) ) || !$API->isLoggedIn() ) { $showPrivacyMessage = true; $action = "about"; }
  if( ( $site == "s" && $action == "photos" && !$API->userHasAccess($user['uid'], PRIVACY_PHOTOS ) ) || !$API->isLoggedIn() ) { $showPrivacyMessage = true; $action = "about"; }
   
}

if ($site == "s" && $action != "profile")
	$borderStyle = 1;
elseif ($site == "m")
	$borderStyle = 2;
else
	$borderStyle = 0;


?>

<script language="javascript" type="text/javascript">
<!--
<?php
if ($gid > 0)
{
	echo 'var page = ' . json_encode($page) . ';
	var currentPage = page.gid;';
}
else //profile
{
	foreach (array("uid", "username", "name", "pic") as $field)
		$user2[$field] = $user[$field];

	echo 'var user = ' . json_encode($user2) . ';';
}
?>

//-->
</script>

<?
if( $action == "editpage" ) //The form element needs to enclode the contact info tab on the side
{
  echo '<form action="/pages/about-edit-save.php" method="post">';
}

?>

<div style="float: left; <?=$borderStyle == 1 ? "border: 1px solid #d8dfea; border-top: 0;" : ""?>">
	<div class="profileleft" style="<?=$borderStyle == 1 ? "border-left: 0; border-bottom: 0;" : ""?>" id="profileleft">
		<div class="minheight" style="background-image: url(/images/bgprofilepic.png); background-position: 10px 0px; background-repeat: repeat-x;">&nbsp;</div>
		<div class="leftcolumn_container">
			<div class="profilepic" style="text-align: center;">
				<div class="borderhider">&nbsp;</div>
				<?php

				// determine if photo can be changed
//				if ( ($action == "profile" || $action=="photos" || $action=="videos") && !isset($page))
				if ( ($action == "profile" || $action=="photos" || $action=="videos" || $action == "about") && isset($page))
				{
					if (isset($user))
					{
						if ($API->uid == $user['uid']){
							$canChange = true;
                        }
					}
				}
				else if ($action == "about" || $action=="editpage")
				{
					if (isset($page))
					{
						if ($page['admin'] == 1 || $API->admin == 1){
							$canChange = true;
                        }
					}
				}

        $noimage = false;
				if ($gid == 0)
        {
					$profilePic = array("url" => $API->getProfileURL($user['uid'], $user['username']), "img" => $API->getUserPic($user['uid'], $user['pic']));
          $profilePic['img'] = $API->getThumbURL(0, 178, 266, $profilePic['img']);
        }
				else
        {

				  $profilePic = array("url" => $API->getPageURL($page['gid'], $page['gname']), "img" => $page['profile_pic'] );

          if( stristr( $profilePic['img'], "images" ) === false )
            $profilePic['img'] = $API->getThumbURL(0, 178, 266, $profilePic['img']);
        }

        if( $profilePic['img'] == "/photos/0/.jpg" )
          $noimage = true;

				if ($canChange && $site == "s")
				{
					echo '<div class="profilepic" onmouseover="javascript:document.getElementById(\'profilepicchange\').style.display = \'inline\';" onmouseout="javascript:document.getElementById(\'profilepicchange\').style.display = \'\';">';
					echo '	<div onclick="javascript:showPhotoChanger();" class="change" id="profilepicchange"><img src="/images/image.png" alt="" />change photo</div>';
				}

				echo "<a href=\"" . $API->getMediaURL("P", $user[$gid > 0 ? 'pid' : 'pic']) . "\">";
				echo "<img id=\"profilepic\" src=\"" . $profilePic['img'] . "\" " . ($noimage?" width=\"178\"":"") . " alt=\"\" />";
				echo "</a>";

				if ($canChange && $site == "s")
					echo '</div>';
				?>
			</div>
			<?php

			if ($site == "s" && $action == "about" && $gid == 0)
      {
				include "side_about.php";
/*
        echo '<div id="sidediv">';
				include "side.php";
        echo '</div>';
*/
      }
			elseif ($site == "s" && $action != "profile" && $gid == 0)
				include "side_media.php";
			else
      {
        echo '<div id="sidediv">';
				include "side.php";
        echo '</div>';
      }
			?>
		</div>
	</div>

	<div class="profilecontent_container" id="profilecontent_container" style="<?=$borderStyle == 0 ? "" : "border-right: 0;"?>">
		<?php if (!($action == "profile" && $site == "s")) { ?>
		<div class="minheight">&nbsp;</div>
		<?php } ?>
		<div class="profilecontent<?=$borderStyle == 0 ? "" : " profilecontent_minus1"?>">

    <div id="pccontainer"></div>

			<?php

      $loggedIn = $API->isLoggedIn();
      if( $loggedIn && $action == "about" && !$API->userHasAccess($user['uid'], PRIVACY_MAX ) )
        $privacyMsg = '<table cellpadding="5" cellspacing="0" align="center" bgcolor="#FFF9D7" style="border: 1px solid #E2C822; margin-top:10px;" width="550"><tr><td><div style="padding: 9px 6px; font-weight: bold; font-size: 10pt; color: #000;">The information in this user\'s profile is only available to some people. If you would like to connect with ' . $user['name'] . ', <a href="javascript:void(0);" id="sendConnect" onclick="javascript:addFriend(' . $user['uid'] . ', \'sendConnect\', \'(request sent!)\');">request to connect</a> or <a href="javascript:void(0);" onclick="javascript:showSendMessage(' . $user['uid'] . ', \'' . $user['name'] . '\', \'/img/48x48/' . $profilePic['img'] . '\');">send a message</a>.</div></td></tr></table>';
      else
        $privacyMsg = '';

      if( $gid == 0 && !$API->userHasAccess($user['uid'], PRIVACY_MAX ) )
      {
?>
      <div style="width: 560px; overflow-x: visible; position: relative;">
  		<div class="phead">
  			<div class="username">
  				<div>
  					<?php
  					if( empty( $user['gid'] ) || (isset( $user['gid'] ) && intval( $user['gid'] ) == 0 ) )
  					{
  						echo '<a href="' . $API->getProfileURL($user['uid'], $user['username']) . '">' . $user['name'] . '</a>';
  					}
  					else
  					{
  						echo '<a href="' . $API->getPageURL($user['gid'], $user['gname']) . '">' . $user['gname'] . '</a>';

  						if ($user['boat']['length'])
  							echo "<span>{$user['boat']['length']} ft. / " . round($user['boat']['length'] / 3.2808399, 2) . " m.</span>";

  						if ($user['boat']['ex'])
  							echo "<span>(ex. " . implode(", ", $user['boat']['ex']) . ")</span>";
  					}
  					?>
  				</div>
  			</div>
        <div style="height:28px; background-image: url(/images/pix_d8dfea.png); background-position: 0 26px; background-repeat: repeat-x; width:570px; margin-left:-15px;"></div>
  		</div>


			<?
        echo $privacyMsg;
        echo '</div>';

      }
      else
  			showUserWithItems($gid == 0 ? $user : $page, $vidCount, $picCount);


      if( $showPrivacyMessage )
      {
        echo '<div style="width: 563px; overflow-x: hidden;">' . $privacyMsg . '</div>';
      }

			if ($site == "s")
			{
        $gid = $page['gid'];

        //tracking views
        $view_type = ($gid > 0 ? 'G' : 'R');
        $link = ($gid > 0 ? $gid : $user['uid']);

        if( $view_type != 'R' || $link != $API->uid )
        {
          sql_query( "update views set ts=Now(), num_views=num_views+1 where uid='" . $API->uid . "' and type='$view_type' and link='" . $link . "'" );
          if( mysql_affected_rows() == 0 )
          {
            sql_query( "insert into views (type,link,uid,ts,num_views) values ('$view_type', $link, '" . $API->uid . "', Now(), 1 )" );
          }
        }
        //end tracking views

				switch ($action)
				{
					case "profile": 
					include "profile_s.php";
					break;

					case "about":
					if ($gid == 0)
						include "about.php";
					else
						include "../pages/about.php";
					break;

          case "editpage":
            include "../pages/about-edit.php";
          break;

					default:
  					include "media.php";
					break;
				}
			}
			else
				include "profile_m.php";


			?>
		</div>
	</div>
	<div style="clear: both;"></div>
</div>

<?php if (!($action == "profile" && $site == "s")) { ?>
<div style="width: 135px; float: left; position: relative;">
	<?php if ($site == "m") { ?>
	<div style="top: 0px; left: -185px; background: #fff; position: absolute; width: 185px; height: 26px;">
		<?php
		if (!isset($type))
		{
			$x = mysql_query("select uid,username from users where active=1 and uid < $uid order by uid desc limit 1");
			if (mysql_num_rows($x) == 0)
				$x = mysql_query("select uid,username from users where active=1 order by uid desc limit 1");
			$prevUser = mysql_fetch_array($x, MYSQL_ASSOC);

			$x = mysql_query("select uid,username from users where active=1 and uid > $uid order by uid limit 1");
			if (mysql_num_rows($x) == 0)
				$x = mysql_query("select uid,username from users where active=1 order by uid limit 1");
			$nextUser = mysql_fetch_array($x, MYSQL_ASSOC)
			?>
			<div style="font-size: 10pt; padding: 5px 10px 0 3px; width:200px;">
				<div style="float:right;">
        <a href="<?=$API->getProfileURL($prevUser['uid'], $prevUser['username'])?>"><img src="/images/left.png" alt="" /> previous user</a>
        &nbsp;
        <a href="<?=$API->getProfileURL($nextUser['uid'], $nextUser['username'])?>">next user <img src="/images/right.png" alt="" /></a>
        </div>
			</div>
			<?php
		}
		?>
	</div>
	<?php } else { ?>
	<div style="position: absolute; top: 0; left: -3px; background: white; height: 26px;">&nbsp;</div>
	<?php } ?>
  <? if( $API->adv ) { ?>
	<div style="padding: <?=$site == "s" && $action == "about" ? 26 : 26?>px 0 0 15px;">
    <?
    //Special handling for vessels in the Ft Lauderdale region.
    if( isset( $page['gid'] ) )
    {
      if( $page['type'] == PAGE_TYPE_VESSEL &&
        $page['boat']['lat'] > 25 && $page['boat']['lat'] < 27 &&
        $page['boat']['lon'] > -80.5 && $page['boat']['lon'] < -79 )
        {
          //Show Tow boat US ad.
          showTallAd( 76 );
        }
    }
    ?>
		<?php showAd("skyscraper"); ?>
	</div>
  <? } ?>
</div>
<?php } ?>

<?
if( $action == "editpage" ) //The form element needs to enclode the contact info tab on the side
{
  echo '</form>';
}
?>




<?php 
include "../inc/embedmedia.php";
if ($API->uid == $user['uid'] && $gid <= 0 )
{
	$_SESSION['profileuser'] = $user;
	$_SESSION['piTypes'] = $piTypes;
	$scripts[] = "/profile/profilemgmt.js.php";
}

if (isset($page))
{
  $_SESSION['pagedata'] = $page;
}

?>
<script language="javascript" type="text/javascript">
<!--
<? if( $_GET['action'] == "contactinfo" ) { ?>
if( typeof( updateUserInfo )  == 'function' )
  updateUserInfo('occupation');
<? } else if( $_GET['action'] == "contactfor" ) { ?>
if( typeof( updateUserInfo )  == 'function' )
  updateUserInfo('contactfor');
<? } else if( isset( $_GET['add'] ) ) { ?>
  loadShare('page', 1, {'gid':'<?=$gid?>', 'gname':'<?=addslashes($page['gname'])?>'} );
<? }

if( isset( $_GET['notifications'] ) ) { ?>
  showNotifications();
<? }

if( isset( $_GET['connections'] ) ) { ?>
  showFriendsPopup(<? echo $API->uid; ?> );
<? }

if( isset( $_GET['addfriends'] ) && $gid > 0) { ?>
  loadShare('page',1,{'gid':'<?=$gid?>','gname':'<?=$page['gname']?>'} );
<? }

if( isset( $_GET['fid'] ) ) { ?>
  openFeedItemPopup( <?=$_GET['fid']?> );
<?
}

$member = quickQuery( "select count(*) from page_members where gid='" . $gid . "' and uid='" . $API->uid . "'" );
echo 'var pageMember = ' . ($member ? '1;' : '0;');
?>

-->
</script>

<?
include "../footer.php";
?>

