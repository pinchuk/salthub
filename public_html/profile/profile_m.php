<div style="padding-left: 10px;">
<?php
	
	if (isset($type)) //videos or photos
	{
		?>
		<div class="pageid" style="background-image: url(/images/<?=$type == "V" ? "television" : "images"?>.png);"><?=ucwords(typeToWord($type))?>s</div>
		<div style="height: 10px; clear: both;"></div>
		<?php
		if (isset( $mediaQuery ) && mysql_num_rows($mediaQuery) > 0)
		{
			while ($tmp = mysql_fetch_array($mediaQuery, MYSQL_ASSOC))
				$media[] = $tmp;
			
			for ($i = 0; $i < count($media); $i++)
				showMediaPreview($type, $media[$i], $i == count($media) - 1);
			
				
			include "../inc/pagination.php";
		}
		else
			echo '<div style="padding: 15px; font-weight: bold; font-size: 11pt; text-align: center;">This user does not have any ' . typeToWord($type) . 's.</div>';
	}
	else // user statistics
	{
		$tix = quickQuery("select count(*) from invites where uid=$uid");
		?>
		<div style="clear: both; padding-top: 10px; width: 540px;">
			<?php
			showBlurbs($user, $_GET['r'] == 1);
			?>
			<div class="blurbdivider"></div>
			<div class="blurb" style="width: 275px; float: left;">
				<div class="viewprofile"><img src="/images/chart_bar.png" alt="" /> <?=$API->uid == $uid ? "your " : ""?>statistics</div>
				<span>Joined:</span>&nbsp; <?=formatDate($user['joined'])?><br />
				<span>Uploads:</span>&nbsp; <?=$picCount + $vidCount?><br />
				<span>Comments:</span>&nbsp; <?=intval(quickQuery("select count(*) from comments inner join photos on comments.link=photos.id where type=\"P\" and photos.uid=$uid")) + intval(quickQuery("select count(*) from comments inner join videos on comments.link=videos.id where type=\"V\" and videos.uid=$uid"))?><br />
				<span>Views:</span>&nbsp; <?=intval(quickQuery("select count(*) from media_views inner join photos on media_views.id=photos.id where type=\"P\" and photos.uid=$uid")) + intval(quickQuery("select count(*) from media_views inner join videos on media_views.id=videos.id where type=\"V\" and videos.uid=$uid"))?>
			</div>
			<div class="blurb" style="float: left; width: 247px; padding-right: 10px;">
				<div class="viewprofile"><img src="/images/money.png" alt="" /> <?=$API->uid == $uid ? "your " : ""?>stats for next drawing</div>
				<span>Invitations sent:</span>&nbsp; <?=$user['invites']?><br />
				<span>Confirmed invitations:</span>&nbsp; <?=$tix?><br />
				<span>Tickets for drawing:</span>&nbsp; <?=$tix?><br />
				<?php if ($uid == $API->uid) { ?>
					<span>Your invite link:</span><br />
					<span style="font-size: 10pt; font-weight: normal;">
					<font style="color: #326798;">http://<?=$_SERVER['HTTP_HOST']?>/?ref=<?=$uid?></font><br />
					Copy the link above, then post it on your blog, in an article, or send it to friends.&nbsp;
					Everytime someone joins mediaBirdy with your link, you'll receive another entry into our monthly drawing.
					</span>
				<?php } ?>
			</div>
			<div style="clear: both; height: 25px;"></div>
		</div>
		<?php
	}
	?>
	
	<div style="clear: both; padding-top: 15px;"><?php showHowAbout(); ?></div>
	
	<?php
	
?>
</div>