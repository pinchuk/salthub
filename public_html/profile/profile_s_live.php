<?php
/*
This is the SaltHub specific implemention of the profile page.  Called from /profile/profile.php

CHANGE LOG

9/2/2011 - Not showing "next log" and "previous log" on the user profile page; kept it for pages though.  (~ line 71)
1/20/2012 - Added support for tracking who viewed this page/profile (uses "views" table; previously only used for tracking media views)
12/22/2012 - Moves view tracking to profile.php because it was only incrementing views when the log book was being clicked on.
*/

include "../inc/mod_comments.php";

if ($gid > 0)
{
	$y = mysql_query("(select gid,gname from pages where gid > $gid and privacy=" . PRIVACY_EVERYONE . " order by gid limit 1) union (select gid,gname from pages where gid < $gid and privacy=" . PRIVACY_EVERYONE . " order by gid limit 1)");
	$x = mysql_query("(select gid,gname from pages where gid < $gid and privacy=" . PRIVACY_EVERYONE . " order by gid desc limit 1) union (select gid,gname from pages where gid > $gid and privacy=" . PRIVACY_EVERYONE . " order by gid desc limit 1)");
}
else
{
	$x = mysql_query("(select uid,name from users where uid < {$user['uid']} order by uid desc limit 1) union (select uid,name from users where uid > {$user['uid']} order by uid desc limit 1)");
	$y = mysql_query("(select uid,name from users where uid > {$user['uid']} order by uid limit 1) union (select uid,name from users where uid < {$user['uid']} order by uid limit 1)");

}

$prevUser = mysql_fetch_array($x, MYSQL_ASSOC);
$nextUser = mysql_fetch_array($y, MYSQL_ASSOC);


if ($action == "profile")
	echo '<div style="position: relative; height: 0; width: 0;"><div style="position: absolute; top: -53px; left: 561px; height: 26px; font-size: 9pt; width: 5px; text-align: center; background: #fff;">&nbsp;</div></div>';

?>


<?php

include "profile_top.php";

?>

<!-- close 2 divs from including file -->
</div></div>

<div style="border: 1px solid #555; border-right: 1px solid #808080; border-bottom: 1px solid #d8dfea; float: left;">
	<div style="border-left: 1px solid #808080; border-top: 1px solid #808080;">
		<div class="minheight">&nbsp;</div>
		<div style="float: left; padding: 5px; width: 752px;">
 		  <?php if ($API->isLoggedIn() ) { ?>

			<div style="float: left;" id="divtb1container">
				<div style="padding-bottom: 5px; margin-bottom: 5px; background: #EDEFF4; position: relative;" id="divtb1">
					<script language="javascript" type="text/javascript">
					<!--
					<?php
					if ($user['uid'] != $API->uid) echo "tbTarget = " . $user['uid'] . ";\n";
					echo "currentLog = " . $user['uid'] . ";\n";
					?>
					tbLayout = 2;
					tbPlace();

					tlfCurrent = <?=$user['uid']?>;
					//-->
					</script>
					<?php if ($gid > 0 && $page['member'] == 0) { ?>
					<div class="mustjoin" id="mustjoin" onclick="javascript:showPopUp2('Not a page member', 'You must join this page before<br />posting on its log.');">&nbsp;</div>
					<?php } ?>

					<?php if ($gid == 0 && !$API->userHasAccess( $user['uid'], PRIVACY_LOG_POSTING ) ) { ?>
					<div class="mustjoin" id="mustjoin" onclick="javascript:showPopUp2('Not Connected', 'You must be connected with this user before posting to this log.');">&nbsp;</div>
					<?php } ?>
				</div>
			</div>
      <? } ?>
			<div style="float: right;">
 		  <?php if ($API->isLoggedIn() ) { ?>
				<div style="background: #EDEFF4; width: 301px; height: 91px; padding: 5px; text-align: right; margin-bottom: 5px; position: relative;" id="divtb2">
					<img src="/images/bluex.png" alt="" onclick="javascript:window.location.href = '<?=($gid == 0 ? $API->getProfileURL($uid) : $API->getPageURL($gid)) . "/about"?>';" style="cursor: pointer;" />

<? if( $gid > 0 ) { //THIS MAY BE TEMPORARY? ?>
          <div style="position: absolute; top: -25px; left: 115px; font-size: 9pt; width: 205px; text-align: center;">
						<a href="<?=$gid == 0 ? $API->getProfileURL($prevUser['uid'], $prevUser['username']) : $API->getPageURL($prevUser['gid'], $prevUser['gname'])?>">previous <?=$gid == 0 ? "log" : "page"?>&nbsp; <img src="/images/left.png" alt="" /></a>&nbsp;
						<a href="<?=$gid == 0 ? $API->getProfileURL($nextUser['uid'], $nextUser['username']) : $API->getPageURL($nextUser['gid'], $nextUser['gname'])?>"><img src="/images/right.png" alt="" />&nbsp; next <?=$gid == 0 ? "log" : "page"?></a>
					</div>
<? } ?>
				</div>
        <? } ?>
				<div style="float: right; width: 300px;">
          <? if( $API->adv ) { ?>
					<div class="subhead" style="margin-bottom: 5px;">
						<div style="float:left;">Sponsors</div><div style="float:right;"><a href="http://www.salthub.com/adv/create.php" style="font-size:8pt; font-weight:300; color:rgb(0, 64, 128);">create an ad</a>&nbsp;</div><div style="clear:both;"></div>
					</div>
					<?php
					showAd("companion");
          }

					if (false && $gid == 0)
					{
					?>
					<div class="subhead" style="margin: 10px 0 5px;">
						The Latest From
					</div>
					<div style="position: relative; height: 10px;">
						<div style="position: absolute; width: 295px; text-align: right; font-size: 9pt;">
							<a href="javascript:void(0);" onclick="javascript:tlfGo(-1);">previous <img src="/images/left.png" alt="" /></a>&nbsp; <a href="javascript:void(0);" onclick="javascript:tlfGo(1);"><img src="/images/right.png" alt="" /> next</a>
						</div>
					</div>
					<div class="bubble_container">
						<div class="bubble" id="tlfbubble" style="height:50px;">
							<?php //include "thelatestfrom.php"; ?>
						</div>
					</div>
					<?php
					}
					?>
          <?
          $view_type = ($gid > 0 ? 'G' : 'R');
          $link = ($gid > 0 ? $gid : $user['uid']);
          include "get_viewers.php";
          if( $num_results == 0 ) include "../pages/gyml.php";
          ?>
					<div style="clear: both; height: 10px;"></div>
          <? if( $API->adv ) { ?>
					<div class="subhead" style="margin-bottom: 5px;">
						<div style="float:left;">Sponsors</div><div style="float:right;"><a href="http://www.salthub.com/adv/create.php" style="font-size:8pt; font-weight:300; color:rgb(0, 64, 128);">create an ad</a>&nbsp;</div><div style="clear:both;"></div>
					</div>
					<?php showAd("companion"); } ?>


				</div>
			</div>
			<div style="float: left; width: 441px;">
				<div class="subhead" style="margin-bottom: 5px;">
					Log Entries
				</div>
				<?php
        $get_log_entry_uid =  $user['uid'];

        if( $page['type'] == PAGE_TYPE_VESSEL)
        {
          $filter = $_GET['f'];
          if( empty( $filter ) || $filter == 0 )
              $filter = 1;
        ?>
        <div class="logentryfilters" style="margin-left:0px; padding-left:0px; padding:0px; padding-bottom:3px;">
          <table width="100%">
            <tr>

              <td width="20"><img src="/images/book_open.png" style="margin-left:0px;" width="16" height="16" alt="" /></td><td nowrap width="20"><a href="<?=$page['url']?>/logbook?f=1"><? if( $filter == 1 ) echo "<b>"; ?>all<? if( $filter == 1 ) echo "</b>"; ?></a></td>
              <td width="20"><img src="/images/photos.png" width="16" height="16" alt="" /></td><td nowrap width="90"><a href="<?=$page['url']?>/logbook?f=2"><? if( $filter == 2 ) echo "<b>"; ?>media &amp; posts<? if( $filter == 2 ) echo "</b>"; ?></a></td>
              <td width="20"><img src="/images/sailing-ship-icon.png" width="16" height="16" alt="" /></td><td nowrap width="90"><a href="<?=$page['url']?>/logbook?f=3"><? if( $filter == 3 ) echo "<b>"; ?>navigation data<? if( $filter == 3 ) echo "</b>"; ?></a></td>
<!--              <td><img src="/images/map.png" width="16" height="16" alt="" /></td><td nowrap><a href="<?=$page['url']?>/logbook?f=4"><? if( $filter == 4 ) echo "<b>"; ?>vessel tracking<? if( $filter == 4 ) echo "</b>"; ?></a></td>-->
              <td></td>
            </tr>
          </table>
        </div>
        <?
          switch( $filter )
          {
            case 2:
              $get_log_entry_uid = -11;
            break;

            case 3:
              $get_log_entry_uid = -10;
            break;

            default: break;
          }
        }

		
		if ($_GET['action'] == "logbook2") // new log
		{
			include "newlog.php";
		}
		else
		{
				include_once "getlogentries.php";
				//show log entries and return the last feedno (# of feed headings), last feed id, and the name of the next empty div
				echo '<div style="clear: both;" id="feeds">';
				$gleResult = getLogEntries(array("uid" => $get_log_entry_uid, "gid" => $gid));
				echo '</div><div style="clear: both; height: 15px;"></div>';

				if ($gleResult['more']) // there are more feed entries left
				{
				?>
				<div id="feedshowmore" class="feedshowmore">
					<a href="javascript:void(0);" onclick="javascript:showMoreLogEntries();">show more &#0133;</a>
				</div>
				<?php }
				
		}
		
				$soc = array(
					array("fbid", "Facebook"),
					array("twid", "Twitter")
				);

        if( $gid == 0 )
  				foreach ($soc as $s)
  				{
  					if ($user['uid'] == $API->uid && empty($user[$s[0]]) && $_COOKIE['hide-nag-add' . $s[1]] != "1")
  					{
  					?>
  					<div id="nag-add<?=$s[1]?>" style="background-image: url(/images/<?=strtolower(substr($s[1], 0, 1))?>.png);" class="nagadd">
  						<div style="width: 310px; float: left;">Allow <?=$siteName?> users to find you on <?=$s[1]?>.<br /><a href="/settings">Enable my <?=$s[1]?> account &gt;&gt;</a></div>
  						<div style="float: right; width: 16px; text-align: right;"><a href="javascript:void(0);" onclick="javascript:hideNag('add<?=$s[1]?>');">X</a></div>
  						<div style="clear: both;"></div>
  					</div>
  					<?php
  					}
  				}
				?>
			</div>
		</div>
		<div style="clear: both;"></div>
<!--	</div>
</div> these 2 divs are closed in the including file-->

<script language="javascript" type="text/javascript">
<!--
var lastCommentId = new Array();

var fidold = <?=intval($gleResult['fidold'])?>;
var fidnew = <?=intval($gleResult['fidnew'])?>;
var logEntryResponseReceived = true;

function showNewLogEntries()
{
	url = "/profile/getlogentries.php?gid=<?=$gid?>&uid=<?=$user['uid']?>&newerthan=" + fidnew;
	
	getAjax(url, function(data)
	{
    if( data == "" || data == null ) return;

		eval("json = " + data + ";");

		if (json.fidnew == null)
			return; //nothing new

		addFeed(json.html, true);
		
		fidnew = json.fidnew;
	});
}

function addFeed(html, top)
{
	newdiv = document.createElement("div");
	newdiv.innerHTML = html;
	newdiv.id = "newfeed" + new Date().getTime();

	e = document.getElementById("feeds");

	if (top)
		e.insertBefore(newdiv, e.firstChild);
	else
		e.appendChild(newdiv);

	h = getHeight(newdiv);
	slidedown(newdiv.id, h, true);
}

function showMoreLogEntries()
{
	$('.feedshowmore').addClass('showmore_wait');
	
	if (!logEntryResponseReceived)
		return;
	
	logEntryResponseReceived = false;
	
	q = 'gid=<?=$gid?>&uid=<?=$user['uid']?>&olderthan=' + fidold;
	log('clicked more log entries...' + q);
	
	$.ajax({
		type: 'GET',
		url: '/profile/getlogentries.php?' + q,
		success: function (data)
		{
			$('.feedshowmore').removeClass('showmore_wait');
			
			log('response more log entries');
			logEntryResponseReceived = true;
			
			if (data == "" || data == null)
				return;
			
			eval("json = " + data + ";");

			if (json.more == "0") //reached the end of the user's feed
				document.getElementById("feedshowmore").style.display = "none";
			else if (json.fidnew == null)
				return; //didn't return any results - user probably should not have gotten here

			addFeed(json.html, false);

			fidold = json.fidold;
		}
	});
}

/*getAjax( '/profile/thelatestfrom.php?a', function(data)
{
  e = document.getElementById( 'tlfbubble' );
  if( e ) e.innerHTML = data;
} );*/

setInterval("showNewLogEntries()", 10000);

<?
$visited = queryArray( "select visited,uid from page_members where uid='" . $API->uid . "' and gid='" . $gid . "'" );

if( ( isset( $visited['uid'] )  && $visited['visited'] == 0 ) || isset( $_REQUEST['join'] ) )
{
?>
  joinPage( <? echo $gid ?>, "side", "<?=$API->pageToGtype($gid); ?>" );
<?
}
?>
//-->
</script>
