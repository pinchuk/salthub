<?php
/*
This script displays the very top section of the profile page.
*/

if ($gid == 0)
	$userStatus = quickQuery("select post from wallposts where uid=" . $user['uid'] . " and uid_to=" . $user['uid'] . " order by sid desc");



if( $user['uid'] == $API->uid )
{
  if( empty( $user['company']) ) $user['company'] = "You own or work at?";
  if( empty( $user['location']) ) $user['location'] = "Planet Earth?";
  if( empty( $user['occupation']) ) $user['occupation'] = "What do you do?";
}
else
{
  if( empty( $user['company']) ) $user['company'] = "I'll get to this later";
  if( empty( $user['location']) ) $user['location'] = "Planet Earth";
  if( empty( $user['occupation']) ) $user['occupation'] = "Member of " . $siteName;
}


?>

<div class="profiletop <?=$gid == 0 ? "" : "profiletoppage"?>" id="bluegrad">
	<div class="descr" id="profiledescr" onmouseover="javascript:showIfExists('edit-about', true);" onmouseout="javascript:showIfExists('edit-about', false);" style="<?=$action == "about" ? "height: auto;" : ""?>">
		<?php
		echo '<span id="txtprofiledescr">';

		if ($gid == 0)
		{
			$txtAbout = quickQuery("select txt from aboutme where uid=" . $user['uid']);
			if (trim($txtAbout) == "" && $uid == $API->uid)
				echo 'Add something about yourself or do some bragging <a href="javascript:void(0);" onclick="javascript:toggleUserAboutEdit();" style="font-weight:bold;">here</a>. Examples: have 3 kids, own a fishing boat, surf and work in the yacht charter biz.';
      else
      {
        if( quickQuery( "select admin from users where uid='" . $user['uid'] . "'" ) )
          echo findLinks( str_replace("\n", "<br />", $txtAbout), false );
        else
    			echo str_replace("\n", "<br />", $API->convertChars($txtAbout));
      }
		}
		else
			echo str_replace("\n", "<br />", $API->convertChars($page['descr']));

		echo '</span>';

		if ($action == "profile" || $gid > 0)
		{
			?>
			<div class="more" id="descrmore" onclick="javascript:window.location.href = '<?=$gid == 0 ? $API->getProfileURL($user['uid']) : $page['url']?>/about';"><span>&nbsp; &#0133; </span>more&nbsp; <img src="/images/arrow_down.png" alt="" /></div>
			<?php
		}
		elseif ($action == "about")
		{
		?>
			<div style="clear: both; height: 5px;"></div>
			<?
      if ($user['uid'] == $API->uid) { ?>
			<div style="float: left; font-size: 8pt; font-weight:bold; display: none;" id="edit-about"><a href="javascript:void(0);" onclick="javascript:toggleUserAboutEdit();">edit</a></div>
			<? } 
      
      if( $txtAbout != "" )
      {
      ?>      
			<div style="float: right; font-size: 8pt;"><a href="javascript:void(0);" onclick="javascript:toggleUserAboutEdit();">less&nbsp; <img src="/images/arrow_up.png" alt="" /></a></div>
      <?
      }
      ?>
      
			<div style="clear: both;"></div>
			<?php
		}
		?>
	</div>
	<?php if ($user['uid'] == $API->uid && ($action == "about" || $action == "profile") && $API->isLoggedIn()) { ?>
	<div id="profiledescredit" class="descr" style="display: none; height: auto;">
		<textarea id="txtabout" style="width: 535px; height: 100px;"><?=$txtAbout?></textarea>
		<div style="text-align: center; padding-top: 5px;"><input type="button" onclick="javascript:saveUserAbout();" class="button" value="Save Changes" /></div>
	</div>
	<?php } ?>
	<?php if ($gid == 0) { ?>
<? /*

	<div class="summaryinfo" style="width: 171px;"<?php if ($API->uid == $user['uid']) { ?> onmouseout="javascript:logMouseOver('location', false);" onmouseover="javascript:logMouseOver('location', true);"<?php } ?>>
		<div class="left">From:</div>
		<div class="right" id="user-location-edit"><a href="javascript:void(0);" onclick="javascript:updateUserInfo('location');">edit</a></div>
		<div class="value" id="user-location"><?php
		if ($user['loc_city'] > -1)
			echo ucwords(strtolower(quickQuery("select city from loc_city where id=" . $user['loc_city']))) . ", ";
		if ($user['loc_state'] > -1)
			echo ucwords(strtolower(quickQuery("select region from loc_state where id=" . $user['loc_state']))) . ", ";
		if ($user['loc_country'] > -1)
    {
      $country = quickQuery("select country from loc_country where id=" . $user['loc_country']);
      if( strlen( $country ) > 3 )
        $country = ucwords(strtolower($country));
			echo $country;
    }

    if( $user['loc_country'] == -1 && $user['loc_city'] == -1 && $user['loc_state'] == -1 )
    {
      echo "Planet Earth";
      if( $user['uid'] == $API->uid )
        echo "?";
    }
		?></div>
	</div>
  <div class="summaryinfo" style="width: 147px;"<?php if ($API->uid == $user['uid']) { ?> onmouseout="javascript:logMouseOver('networks', false);" onmouseover="javascript:logMouseOver('networks', true);"<?php } ?>>
		<div class="left">Networks:</div>
		<div class="right" id="user-networks-edit"><a href="javascript:void(0);" onclick="javascript:updateUserInfo('networks');">edit</a></div>
		<div class="value" id="user-networks"></div>
	</div>
*/

  $sector = "None Selected";
  if( $user['sector']> 0 )
    $sector = quickQuery( "select catname from categories where cat='" . $user['sector'] . "'" );
?>
	<div class="summaryinfo" style="width: 142px;"<?php if ($API->uid == $user['uid']) { ?> onmouseout="javascript:logMouseOver('sector', false);" onmouseover="javascript:logMouseOver('sector', true);"<?php } ?>>
		<div class="left">Sector:</div>
		<div class="right" id="user-sector-edit"><a href="javascript:void(0);" onclick="javascript:updateUserInfo('company');">edit</a></div>
		<div class="value" id="user-sector"><?=$sector?></div>
	</div>
	<div class="summaryinfo" style="width: 188px;"<?php if ($API->uid == $user['uid']) { ?> onmouseout="javascript:logMouseOver('occupation', false);" onmouseover="javascript:logMouseOver('occupation', true);"<?php } ?>>
		<div class="left">I'm a:</div>
		<div class="right" id="user-occupation-edit"><a href="javascript:void(0);" onclick="javascript:updateUserInfo('company');">edit</a></div>
		<div class="value" id="user-occupation"><?=$user['occupation']?></div>
	</div>
	<div class="summaryinfo" style="width: 186px; border-right: 0; margin-right: 0;"<?php if ($API->uid == $user['uid']) { ?> onmouseout="javascript:logMouseOver('company', false);" onmouseover="javascript:logMouseOver('company', true);"<?php } ?>>
		<div class="left">Company/Vessel:</div>
		<div class="right" id="user-company-edit"><a href="javascript:void(0);" onclick="javascript:updateUserInfo('company');">edit</a></div>
		<div class="value" id="user-company"><?=$user['company']?></div>
	</div>
	<?php } else { ?>
	<div class="summaryinfo" style="width: 175px;"> 
		<div class="left">Category:</div>
		<div class="value"><?=$page['catname']?></div>
	</div>
	<div class="summaryinfo" style="width: 175px;"> 
		<div class="left">Accessible by:</div>
		<div class="value"><?=$page['privacy'] == PRIVACY_EVERYONE ? "The world" : "Only members"?></div>
	</div> 
	<div class="summaryinfo" style="width: 175px; border-right: 0; margin-right: 0;">
		<div class="left">Created by:</div>

		<div class="value" style="color: #555;">
	  		<?php
  			if ( empty($page['uid']) || $page['uid'] == "" ) {
          $gid2 = quickQuery( "select gid from pages where gname='$siteName'" );
          echo '<a href="' . $API->getPageURL($gid2) . '">' . $siteName . '</a>';
         } else { ?>
          <a href="<?=$API->getProfileURL($page['uid'])?>"><?=$page['name']?></a><?=$page['uid'] == $API->uid ? '' : ' &nbsp;|&nbsp; ' . friendLink($page['uid'])?>
        <? } ?>
    </div>
	</div>
	<?php } ?>
	<div style="clear: both;"></div>
</div>

<script language="javascript" type="text/javascript">
<!--
//check height of profile status
e = document.getElementById("profiledescr");

if (action == "about")
	showBlueGrad();

if (e)
{
	h = getHeight(e);

	e = document.getElementById("descrmore");
	if (h > 50 && e)
		e.style.display = "inline";
}
//-->
</script>