<?php

header("Content-type: text/javascript");

include "../inc/inc.php";

$user = $_SESSION['profileuser'];
unset($_SESSION['profileuser']);

if (empty($user))
{
	$x = mysql_query('select * from users where uid=' . $API->uid);
	
	if (mysql_num_rows($x) == 0)
		die('alert("User not found");');
	
	$user = mysql_fetch_array($x, MYSQL_ASSOC);
}

$dob = explode("-", $user['dob']);

$adminSite = $_GET['isAdmin'];

?>

var updating;

var LIC_TID = 101;
var DEGREE_TID = 1297;



var itemsChosen = Array();

var selCountryHTML = '<?php
		$x = sql_query("select id,country from loc_country order by priority desc,country");
		while ($y = mysql_fetch_array($x, MYSQL_ASSOC))
    {
      $country = $y['country'];
      if( strlen( $country ) > 3 )
        $country = ucwords( strtolower( $country ) );
			echo '<option ' . ($user['loc_country'] == $y['id'] ? 'selected' : '') . ' value="' . $y['id'] . '">' . addslashes($country) . '</option>';
    }
		?>';

var locationHTML;
locationHTML  = '<div style="text-align: left;">';
locationHTML += '	<div class="embedlabel">Country:</div>';
locationHTML += '	<div class="embedinput" style="height: 16px;"><select id="user-country" onchange="javascript:updateLocationSelect(\'state\', this.value);" style="width: 250px;">';
locationHTML += '		' + selCountryHTML;
locationHTML += '		</select></div>';
locationHTML += '	<div class="embedlabel">Region:</div>';
locationHTML += '	<div class="embedinput" style="height: 16px;" id="userinfo-state"><select id="user-state" onchange="javascript:updateLocationSelect(\'city\', this.value);" style="width: 250px;">';
locationHTML += '		<?php
if ($user['loc_country'] == -1)
	$user['loc_country'] = 220;  // USA USA USA

$x = sql_query("select id,region from loc_state where country_id=" . intval($user['loc_country']) . " order by region");
while ($y = mysql_fetch_array($x, MYSQL_ASSOC))
	echo '<option ' . ($user['loc_state'] == $y['id'] ? 'selected' : '') . ' value="' . $y['id'] . '">' . addslashes(ucwords(strtolower($y['region']))) . '</option>';
?>';
locationHTML += '		</select></div>';
locationHTML += '	<div class="embedlabel">City:</div>';
locationHTML += '	<div class="embedinput" style="height: 16px;" id="userinfo-city"><select id="user-city" style="width: 250px;">';
locationHTML += '		<?php
if ($user['loc_state'] > -1)
{
	$x = sql_query("select id,city from loc_city where region_id=" . $user['loc_state'] . " order by city");
	while ($y = mysql_fetch_array($x, MYSQL_ASSOC))
		echo '<option ' . ($user['loc_city'] == $y['id'] ? 'selected' : '') . ' value="' . $y['id'] . '">' . addslashes(ucwords(strtolower($y['city']))) . '</option>';
}
else
  echo '<option value="-1">-</option>';
?>';
locationHTML += '		</select></div>';
locationHTML += '	<div style="clear: both; height: 10px;"></div>';
locationHTML += '</div>';

var updateUserInfo_callback = updateHandler;

function updateUserInfo(x, callback)
{
	if (typeof callback == 'undefined')
		updateUserInfo_callback = updateHandler;
	else
		updateUserInfo_callback = callback;
	
	updating = x;
	text = "";
	html = "";

	if (updating == "company")
	{
		text = "Update your professional information";
		html += '<div style="text-align: left;">';
		html += '	 <div class="embedlabel"><div style="padding-top: 3px;">Occupation:</div></div>';
		html += '  <div class="embedinput" style="font-weight: normal;">';
		html += '		 <input id="occupation" value="<? if( $user['occupation'] != "What do you do?" ) echo addSlashes( $user['occupation'] ); ?>" type="text" size="35" maxlength="100" >';
		html += '	    <br />For example: "Maritime Consultant / Expert"';
		html += '	 </div>';
		html += '  <div style="text-align: left;">';
		html += '	  <div class="embedlabel"><div style="padding-top: 3px;">Sector:</div></div>';
		html += '	  <div class="embedinput" style="height: 16px; font-weight: normal;">';
		html += '		<select id="sector" style="width:200px">';
    html += '   <option value="0">(None Selected)</option>';
<?
$q2 = sql_query( "select * from categories where cattype='G' and industry='" . PAGE_TYPE_BUSINESS . "' order by catname" );
while( $r2 = mysql_fetch_array( $q2 ) )
{
?>
    html += '    <option value="<? echo $r2['cat']; ?>"<? if( $r2['cat'] == $user['sector'] ) echo " SELECTED"; ?>><? echo $r2['catname']; ?></option>';
<?
}
?>
		html += '		</select>';
		html += '	  </div>';
		html += '	</div>';

		html += '	<div class="embedlabel"><div style="padding-top: 3px;">Company/<br />Vessel:</div></div>';
		html += '	<div class="embedinput" style="height: 16px; font-weight: normal;">';
		html += '		<input id="company" value="<? if( $user['company'] != "You own or work at?" ) echo addSlashes( $user['company'] ); ?>" type="text" size="35">';
		html += '	</div>';

    html += '</div>';
		html += '<div style="clear: both; height: 10px;"></div>';

//		value = "<?=addslashes($user['company'])?>";
	}
	else if (updating == "occupation")
	{
		text = "Update your contact information";
//		value = "<?=addslashes($user['occupation'])?>";
		html += '<div style="text-align: left;">';
		html += '	<div class="embedlabel"><div style="padding-top: 3px;">Public Email:</div></div>';
		html += '	<div class="embedinput" style="height: 16px; font-weight: normal;">';
		html += '		<input id="public_email" value="<? if( $user['public_email'] != "" ) echo addSlashes( $user['public_email']); ?>" type="text" size="35">';
		html += '	</div>';
		html += '	<div class="embedlabel"><div style="padding-top: 3px;">Acct. Email:</div></div>';
		html += '	<div class="embedinput" style="font-weight: normal; margin-top:3px;">';
		html += '		<? echo $user['email']; ?><br /><a href="/settings/">edit my account settings</a>';
		html += '	</div>';
		html += '	<div class="embedlabel"><div style="padding-top: 3px;">Tel: +</div></div>';
		html += '	<div class="embedinput" style="height: 16px; font-weight: normal;">';
		html += '		<input id="public_phone" value="<? if( $user['public_phone'] != "" ) echo addSlashes( $user['public_phone']); ?>" type="text" size="35">';
		html += '	</div>';
		html += '	<div class="embedlabel"><div style="padding-top: 3px;">Cell: +</div></div>';
		html += '	<div class="embedinput" style="height: 16px; font-weight: normal;">';
		html += '		<input id="cell" value="<? if( $user['cell'] != "" ) echo addSlashes( $user['cell'] ); ?>" type="text" size="35">';
		html += '	</div>';
		html += '	<div class="embedlabel"><div style="padding-top: 3px;">Fax: +</div></div>';
		html += '	<div class="embedinput" style="height: 16px; font-weight: normal;">';
		html += '		<input id="fax" value="<? if( $user['fax'] != "" ) echo addSlashes( $user['fax']); ?>" type="text" size="35">';
		html += '	</div>';
    html += '</div>';
		html += '<div style="clear: both; height: 10px;"></div>';

	}
	else if (updating == "contactfor")
	{
		text = 'Add keywords<br /><div style="text-align:center; width:100%;"><div style="float:left; margin-left:30px;"><img src="/images/help.png" width="16" height="16" alt=""/></div><div style="float:left; font-weight:300; width:300px;">&nbsp;&nbsp;Each keyword or phrase is another opportunity to increase your chance of appearing in search results. For example, "Yacht Captain", "Project Manager", or "Surveyor".</div></div>';
<?
  $data = explode( chr(2), $user['contactfor'] );
?>
		html += '<div style="text-align: left;">';
<?
for( $c = 0; $c < 6; $c++ )
{
?>
		html += '	<div class="embedlabel"><div style="padding-top: 3px;"><? echo $c+1 ?>:</div></div>';
		html += '	<div class="embedinput" style="height: 16px; font-weight: normal;">';
		html += '		<input id="contactfor-<? echo $c ?>" value="<? if( $data[$c] != "" ) echo $data[$c]; ?>" type="text" size="35" maxlength="40">';
		html += '	</div>';
<? } ?>
    html += '</div>';
		html += '<div style="clear: both; height: 10px;"></div>';

	}
	else if (updating == "basicinfo")
	{
		text = "Update your basic information";
		html += '<div style="text-align: left;">';
		html += '	<div class="embedlabel"><div style="padding-top: 3px;">Birthday:</div></div>';
		html += '	<div class="embedinput" style="height: 16px; font-weight: normal;">';
		html += '		<select id="dobm"><option></option>';
		<?php
		for ($i = 1; $i <= 12; $i++)
		echo "html += '<option value=\"$i\" " . (intval($dob[1]) == $i ? "selected" : "") . ">" . date("F", strtotime("2010-$i-1")) . "</option>';";
		?>
		html += '		</select>';
		html += '		<select id="dobd"><option></option>';
		<?php
		for ($i = 1; $i <= 31; $i++)
		echo "html += '<option value=\"$i\" " . (intval($dob[2]) == $i ? "selected" : "") . ">$i</option>';";
		?>
		html += '		</select>';
		html += '		<select id="doby"><option></option>';
		<?php
		for ($i = date("Y") - 100; $i <= date("Y") - 18; $i++)
		echo "html += '<option value=\"$i\" " . (intval($dob[0]) == $i ? "selected" : "") . ">$i</option>';";
		?>
		html += '		</select>';
		html += '		<input type="checkbox" id="dobyearpub" <?=$user['showyear'] == "1" ? "checked" : ""?> /> show year';
		html += '	</div>';
		html += '	<div class="embedlabel"><div style="padding-top: 3px;">Gender:</div></div>';
		html += '	<div class="embedinput" style="height: 16px; font-weight: normal;">';
		html += '		<div style="float: left;">';
		html += '			<select id="gender"><option></option>';
		<?php
		foreach (array("M", "F") as $i)
		echo "html += '<option value=\"$i\" " . ($user['gender'] == $i ? "selected" : "") . ">" . convertGender($i) . "</option>';";
		?>
		html += '			</select>';
		html += '		</div>';
		html += '		<div style="float: left; width: 75px; padding-left: 28px;">';
		html += '			<div class="smtitle2" style="padding-top: 3px;">Looking For:</div>';
		html += '		</div>';
		html += '		<div style="float: left;">';
		html += '			<select id="relationship"><option></option>';
		<?php
		foreach (array("W", "N", "F", "D", "R") as $i)
		echo "html += '<option value=\"$i\" " . ($user['relationship'] == $i ? "selected" : "") . ">" . convertRelationship($i) . "</option>';";
		?>
		html += '			</select>';
		html += '		</div>';
		html += '	</div>';
		html += '	<div class="embedlabel"><div style="padding-top: 3px;">Citizenship:</div></div>';
		html += '	<div class="embedinput" style="height: 16px;">';
		html += '			<select id="citizenship"><option></option>';
		html += '<?php
		$x = sql_query("select id,country from loc_country order by priority desc,country");
		while ($y = mysql_fetch_array($x, MYSQL_ASSOC))
    {
      $country = $y['country'];
      if( strlen( $country) > 3 ) $country = ucwords(strtolower($y['country']));
			echo '<option ' . ($user['citizenship'] == $y['id'] ? 'selected' : '') . ' value="' . $y['id'] . '">' . addslashes($country) . '</option>';
    }
		?>';
		html += '			</select>';
		html += '	</div>';
		html += '</div>';
		html += '<div style="clear: both; height: 10px;"></div>';
	}
	else if (updating == "location")
	{
		text = "Update your location";
		html += locationHTML;
	}
	
	if (text != "")
	{
		content = '<div style="font-size: 9pt; padding-bottom: 10px;">' + text + '<p />';
		
		if (html == "")
			content += '<input type="text" id="updateinfo" value="' + value + '" style="width: 250px;" />';
		else
			content += html;
			
		content += '</div><input type="button" class="button" value="Save" onclick="javascript:doUpdateUserInfo();" /> &nbsp; &nbsp; <input type="button" class="button" value="Cancel" onclick="javascript:closePopUp();" />';
		
		showPopUp2("", content);
		return;
	}
}

function doUpdateUserInfo()
{
	e = document.getElementById("user-" + updating);

	if (updating == "basicinfo")
	{
  	closePopUp();

		y = document.getElementById("doby");
		m = document.getElementById("dobm");
		d = document.getElementById("dobd");
		g = document.getElementById("gender");
		c = document.getElementById("citizenship");
		r = document.getElementById("relationship");

		obj = ({
					'dob': y.value + '-' + m.value + '-' + d.value,
					'gender': g.value,
					'relationship': r.value,
					'showyear': document.getElementById("dobyearpub").checked ? 1 : 0,
					'citizenship': selectedValue(c)
				});

		if (d.options[d.selectedIndex].text != "" && m.options[m.selectedIndex].text && y.options[y.selectedIndex].text != "")
		{
			bday = m.options[m.selectedIndex].text + " " + d.options[d.selectedIndex].text;
			if (obj.showyear == 1)
				bday += ", " + y.options[y.selectedIndex].text;
		}
		else
			bday = "";

		if ($('#user-dob').length > 0)
		{
			document.getElementById("user-dob").innerHTML = bday;
			document.getElementById("user-citizenship").innerHTML = c.options[c.selectedIndex].text;
	//		document.getElementById("user-relationship").innerHTML = r.options[r.selectedIndex].text;
			document.getElementById("user-gender").innerHTML = g.options[g.selectedIndex].text;
		}
		
		postAjax("/profile/updatebasicinfo.php", "json_info=" + escape(json_encode(obj)), updateUserInfo_callback);

		return;
	}
	else if (updating == "contactfor")
  {
  	closePopUp();

		text = "";
    for( c = 0; c < 6; c++ )
    {
      if( document.getElementById("contactfor-" + c).value != '' )
        text += document.getElementById("contactfor-" + c).value + String.fromCharCode(2);
    }

		postAjax("/profile/updatecontactfor.php", "data=" + escape(text), updateUserInfo_callback);
  }
	else if (updating == "location")
	{
  	closePopUp();

		text = "";
		c = document.getElementById("user-country");
		s = document.getElementById("user-state");
		ci = document.getElementById("user-city");

		if (ci.options[ci.selectedIndex].text == "-")
			ci = -1;
		else
		{
			text = ci.options[ci.selectedIndex].text + ", ";
			ci = ci.value;
		}

		if (s.options[s.selectedIndex].text == "-")
			s = -1;
		else
		{
			text += s.options[s.selectedIndex].text + ", ";
			s = s.value;
		}

		if (c.options[c.selectedIndex].text == "-")
			c = -1;
		else
		{
			text += c.options[c.selectedIndex].text;
			c = c.value;
		}

		e.innerHTML = text;

		v = [c,s,ci].join("|");
	}
	else if (updating == "occupation")
	{
  	closePopUp();

		e2 = document.getElementById("public_phone");
		e3 = document.getElementById("cell");
		e4 = document.getElementById("fax");
		e5 = document.getElementById("public_email");

		obj = ({
          'updating':updating,
					'public_phone': e2.value,
					'cell': e3.value,
					'fax': e4.value,
          'public_email': e5.value,
				});

		postAjax("/profile/updatecontactinfo.php", "json_info=" + escape(json_encode(obj)), updateUserInfo_callback);

  }
  else if( updating == "company" )
  {
		e1 = document.getElementById("occupation");
		e2 = document.getElementById("company");
		e3 = document.getElementById("sector");

    if( e1 && e1.value == "" )
    {
      alert( "Please enter a headline before saving." );
      return;
    }

   	closePopUp();

		obj = ({
          'updating': updating,
					'occupation': e1.value,
          'company' : e2.value,
          'sector': selectedValue( e3 )
				});

		postAjax("/profile/updatecontactinfo.php", "json_info=" + escape(json_encode(obj)), updateUserInfo_callback);
  }
	else
	{


		v = document.getElementById("updateinfo").value;
		e.innerHTML = v;
	}

	postAjax("/profile/updateuserinfo.php", "f=" + updating + "&v=" + escape(v), updateUserInfo_callback );
}

function updateHandler(data)
{
  window.location.href = '<? echo $API->getProfileUrl(); ?>/about';
}

function updateLocationSelect(f, v)
{
  if( document.getElementById("userinfo-" + f) )
  	document.getElementById("userinfo-" + f).innerHTML = '<img src="/images/wait_sm.gif" alt="" />';
	getAjax("/profile/locationselect.php?f=" + f + "&v=" + v, "updateLocationSelectHandler");
}

function updateLocationSelectHandler(data)
{
	x = data.split("|");
	document.getElementById("userinfo-" + x[0]).innerHTML = x[1];
}

function saveFollowMe()
{
	data  = "save=1";
	data += "&linkedin_url=" + escape(document.getElementById("follow-linkedin_url").value);
	data += "&ytid=" + escape(document.getElementById("follow-ytid").value);
	data += "&www=" + escape(document.getElementById("follow-www").value);

	postAjax("/profile/followme.php", data, function(data) {
  	closePopUp();
    location.reload(true);
  });

}

function editFollowMe()
{
	html  = '<div class="followmepopup">';
	html += '	<div class="site"><img src="/images/salt_badge_48x48.png" style="width: 48px;" alt="" />http://<?=SERVER_HOST . $API->getProfileURL()?></div>';
	html += '	<div class="site"><div style="height: 2px; background: #c0c0c0; overflow: hidden;">&nbsp;</div></div>';
	html += '	<div class="site"><img src="/images/f.png" alt="" /><span>Facebook</span><?=$user['fbid'] ? "http://www.facebook.com/profile.php?id=" . $user['fbid'] : '<a href="/settings">Enable account</a>'?></div>';
	html += '	<div class="site"><img src="/images/t.png" alt="" /><span>Twitter</span><?=$user['twid'] ? "http://twitter.com/" . $user['twusername'] : '<a href="/settings">Enable account</a>'?></div>';
	html += '	<div class="site"><img src="/images/linkedin.png" alt="" /><span>LnkedIn</span>http://www.linkedin.com/<input type="text" id="follow-linkedin_url" value="<?=$user['linkedin_url']?>" /></div>';
	html += '	<div class="site"><img src="/images/youtube.png" alt="" /><span>YouTube</span>http://www.youtube.com/<input type="text" id="follow-ytid" value="<?=$user['ytid']?>" /></div>';
	html += '	<div class="site"><img src="/images/layout_header.png" alt="" /><span>My website</span>http://<input type="text" id="follow-www" value="<?=$user['www']?>" /></div>';
	html += '	<div class="site" style="text-align: center;"><input type="button" class="button" value="Save" onclick="javascript:saveFollowMe();" /> &nbsp; &nbsp; <input type="button" class="button" value="Cancel" onclick="javascript:closePopUp();" /></div>';
	html += '</div>';

	showPopUp("Manage your website and social profile links", html);
}

piTypes = <?=json_encode($_SESSION['piTypes'])?>;

var profilePreview;

function stopPiEdit(tidEditing)
{
	document.getElementById("itemchooserparent-" + tidEditing).style.display = "none";

	if (tidEditing == LIC_TID || tidEditing == DEGREE_TID)
		toggleEdit("education");
}

function saveUserAbout()
{
	txtAbout = document.getElementById("txtabout").value;
	postAjax("/profile/saveabout.php", "about=" + txtAbout, "void");

	txtAbout = htmlentities(txtAbout);
	txtAbout = txtAbout.replace(/\n/gi, "<br />");
	
	document.getElementById("txtprofiledescr").innerHTML = txtAbout;
	
	toggleUserAboutEdit();
}

function toggleUserAboutEdit()
{
	e = document.getElementById("profiledescr");
	f = document.getElementById("profiledescredit");
	
	if (e.style.display == "none")
	{
		e.style.display = "";
		f.style.display = "none";
	}
	else
	{
		e.style.display = "none";
		f.style.display = "";
	}
	
	showBlueGrad();
}

function editAbout() //not used
{
	html  = '<textarea id="editabout" style="padding: 2px; width: 435px; height: 65px; line-height: 15px;">' + txtAbout + '</textarea>';
	html += '<div style="padding-top: 5px; text-align: right;">';
	html += '	<input type="button" class="button" value="Save" onclick="javascript:saveAbout();" /> &nbsp; <input type="button" class="button" value="Cancel" onclick="javascript:cancelEditAbout();" />'
	html += '</div>';
	
	document.getElementById("items-display-about").style.display = "none";
	
	e = document.getElementById("itemchooserparent-about");
	e.innerHTML = html;
	e.style.padding = "0";
	e.style.display = "inherit";
}

function saveAbout() //not used
{
	cancelEditAbout();
	txtAbout = document.getElementById("editabout").value;
	
	document.getElementById("items-about").innerHTML = txtAbout.replace(/\n/gi, "<br />");
	
	postAjax("/profile/saveabout.php", "about=" + escape(txtAbout), "void");
}

function cancelEditAbout() //not used
{
	document.getElementById("items-display-about").style.display = "";
	document.getElementById("itemchooserparent-about").style.display = "none";
}

function piEdit(i)
{
	if (i == "about")
	{
		editAbout();
		return;
	}

	if (document.getElementById("educationedit").style.display != "none" && i != 5)
		toggleEdit("education");

  var tidEditing = i;
  tname = "";

	for (var t in piTypes)
	{
    if( i == DEGREE_TID && piTypes[t].tid == LIC_TID )
      continue;

		e = document.getElementById("itemchooserparent-" + piTypes[t].tid);
		if ( e ) //Degrees can be shown at the same time as licenses
		{
			e.innerHTML = "";
			e.style.display = "none";
		}

		if (piTypes[t].tid == i)
		{
			tname = piTypes[t].name;
      if( tname == "Licenses" )
        tname = "Lic &amp; Certificates";
		}
	}

	html  = '<div style="width: 420px; padding-right: 10px; float: left;">';
	html += '	<div style="width: 200px; float:left;">';
	html += '	  <div class="smtitle2" style="padding-bottom: 3px;">Popular ' + tname + ':</div>';
	html += '	  <div style="width: 190px; height: 18px; border: 1px solid #555; position: relative; float:left;">';
	html += '		  <input type="text" onkeyup="javascript:searchItems(' + tidEditing + ' );" id="itemsearch-' + tidEditing + '" style="margin: 0; padding: 0px 2px; border: 0; width: 169px; height: 18px;"><img src="/images/dropdown.png" onclick="javascript:toggleItemChooser(' + tidEditing + ');" style="cursor: pointer; vertical-align: top;" alt="" />';

if( tidEditing == LIC_TID )
{
	html += '		  <div id="itemchooser-' + tidEditing + '" class="itemchooser" style="width:550px; height:400px;">';
	html += '			  <div id="chooser-' + tidEditing + '" class="chooser" style="height:360px;"></div>';
}
else
{
	html += '		  <div id="itemchooser-' + tidEditing + '" class="itemchooser">';
	html += '			  <div id="chooser-' + tidEditing + '" class="chooser"></div>';
}
	html += '			  <div style="padding: 5px; text-align: center; background: #fff; border-top: 1px solid #555;">';
	html += '				  <input type="button" class="button" value="Apply" onclick="javascript:saveItems(' + tidEditing + '); toggleItemChooser(' + tidEditing + ');" /> &nbsp; <input type="button" class="button" value="Cancel" onclick="javascript:toggleItemChooser(' + tidEditing + '); loadItems(0,' + tidEditing + ' );" />';
	html += '			  </div>';
	html += '		  </div>';
	html += '	  </div>';
	html += '	  <div style="clear:both; padding-top: 5px; font-size: 8pt;">Your Selections</div>';
	html += '	  <div id="selections-' + tidEditing + '" style="font-size:8pt; margin-bottom:15px;"></div>';
	html += '	</div>';
	html += '	<div style="width: 200px; float:left; margin-left:10px;">';
	html += '	  <div class="smtitle2" style="padding-bottom: 3px;">If we forgot yours, add it here:</div>';
  html += '   <input type="text" id="customitems-' + tidEditing + '" onkeyup="javascript:updateProfilePreview(' + tidEditing + ');" style="margin: 0; padding: 0px 2px; border: 1px solid #555; width: 190px; height: 18px;">';
	html += '	  <div style="padding-top: 5px; font-size: 8pt;">Separate entries with commas</div>';
if( i != LIC_TID ) {
	html += '	  <div style="padding-top: 15px; text-align: center;">';
	html += '		<input type="button" class="button" value="Save" onclick="javascript:saveItems(' + tidEditing + '); stopPiEdit(' + tidEditing + ');" /> &nbsp; <input type="button" class="button" value="Cancel" onclick="javascript:stopPiEdit(' + tidEditing + ');" />';
	html += '	</div>';
} else {
  html += ' <div style="margin-bottom:10px;"></div>';
}
	html += '</div></div>';
	if (parseInt(piTypes[tidEditing].special) == 0)
	{
//	html += '<div style="width: 215px; float: left;">';
//	html += '	<div class="smtitle2">Profile Preview for ' + tname + ':</div>';
//	html += '		<div class="profilepreview" id="profilepreview"></div>';
//	html += '</div>';
	}
	html += '<div style="clear: both;"></div>';

	e = document.getElementById("itemchooserparent-" + i);
	e.innerHTML = html;
	e.style.display = "inherit";
	
	profilePreview = "I like the following " + tname + ":&nbsp ";

	loadItems(0, tidEditing);
}

//var tidEditing;

function loadItems(i,tidEditing)
{
  if( tidEditing == LIC_TID )
  	loadjscssfile("/profile/getitems_licenses.js.php?i=" + i + "&tid=" + tidEditing + "&hash=" + hash, "js");
  else
  	loadjscssfile("/profile/getitems.js.php?i=" + i + "&tid=" + tidEditing + "&hash=" + hash, "js");
}

function refreshSelections( tidEditing )
{
  html = '';

  e = document.getElementById( "selections-" + tidEditing );

  if( e )
  {
  	for (var i in itemsChosen[tidEditing])
  		if (itemsChosen[tidEditing][i] != null)
	  		if (itemsChosen[tidEditing][i][1] != null)
      	{
          html += '<a href="/profile/page-forward.php?gid=' +  itemsChosen[tidEditing][i][0] + '">' + itemsChosen[tidEditing][i][1] + "</a><br />";
        }

    if( document.getElementById("customitems-" + tidEditing) )
    {
    	var custom = document.getElementById("customitems-" + tidEditing).value;
	    if (custom != "")
      {
  		  html += custom.replace( ",", "<br />" );
      }
    }

    if( html == '' )
      html = 'use dropdown above to select';
    e.innerHTML = html;
  }
}

function saveItems( tidEditing )
{
  if( tidEditing == DEGREE_TID ) tidEditing = LIC_TID;

	if (typeof apply == "undefined")
		apply = false;

	pids = "";
	for (var i in itemsChosen[tidEditing])
	{
		if (itemsChosen[tidEditing][i] != null)
			if (typeof itemsChosen[tidEditing][i][0] != "undefined")
				pids += "," + itemsChosen[tidEditing][i][0];
	}

	data = "tid0=" + tidEditing + "&pids0=" + pids.substring(1) + "&custom0=" + escape(document.getElementById("customitems-" + tidEditing ).value);

  if( tidEditing == LIC_TID ) //Include degrees when updating
  {
  	pids = "";
  	for (var i in itemsChosen[DEGREE_TID])
  	{
  		if (itemsChosen[DEGREE_TID][i] != null)
  			if (typeof itemsChosen[DEGREE_TID][i][0] != "undefined")
  				pids += "," + itemsChosen[DEGREE_TID][i][0];
  	}

    data += "&tid1=" + DEGREE_TID + "&pids1=" + pids.substring(1) + "&custom1=" + escape(document.getElementById("customitems-" + DEGREE_TID).value);
  }

	if (tidEditing == LIC_TID || tidEditing == DEGREE_TID ) //education
	{
		var saveEdus = [];
		for (i = 0; i < eduMax; i++)
		{
			if (document.getElementById("edu-" + i).style.display == "none")
				continue;

			obj = {'start': document.getElementById("edu-year0-" + i).value, 'stop': document.getElementById("edu-year1-" + i).value};

			if (schools[i].actb_val == null)
				obj['school'] = {'txt': document.getElementById("edu-school-" + i).value};
			else
				obj['school'] = {'val': schools[i].actb_val};

			saveEdus.push(obj);
		}
		data += "&json_edus=" + escape(json_encode(saveEdus));
}

  editing = tidEditing;

	e = document.getElementById("items-" + editing);
	if (e)
		e.innerHTML = "updating &#0133;";

	postAjax("/profile/saveabout.php", data, function (data)
	{
		e = document.getElementById("items-" + editing);

		if (e)
    {
			e.innerHTML = data;
    }
	});
}

function toggleItemChooser(tidEditing)
{
	e = document.getElementById("itemchooser-" + tidEditing);
	isShown = e.style.display == "inline";

	if (!isShown) // load item choices
	{
    if( document.getElementById("customitems-" + tidEditing) )
  		document.getElementById("customitems-" + tidEditing).value = "";

		document.getElementById("chooser-" + tidEditing).innerHTML = '&nbsp;';
		loadItems(1, tidEditing );
	}
	
	e.style.display = isShown ? "none" : "inline";
}

function searchItems(tidEditing)
{
  e = document.getElementById("itemsearch-" + tidEditing);

  if( !e )
  {
    alert( tidEditing );
  }

	q = e.value.toLowerCase();

	if (document.getElementById("itemchooser-" + tidEditing).style.display != "inline")
		toggleItemChooser(tidEditing);

	i = 0;
	
	while (e = document.getElementById("nameitem-" + i))
		document.getElementById("item-" + i++).style.display = e.innerHTML.toLowerCase().indexOf(q) == -1 ? "none" : "";
}

function itemChosen(i, pid, chosen, tidEditing)
{
	if (chosen)
	{
		name = document.getElementById("nameitem-" + i).innerHTML;
		itemsChosen[tidEditing][itemsChosen[tidEditing].length] = [pid, name];
	}
	else
	{
		for (var i in itemsChosen[tidEditing])
		{
			if (itemsChosen[tidEditing][i] != null)
				if (itemsChosen[tidEditing][i][0] == pid)
				{
					itemsChosen[tidEditing][i] = null;
					break;
				}
		}
	}

  refreshSelections( tidEditing );

	updateProfilePreview(tidEditing);
}

function updateProfilePreview(tidEditing)
{
	e = document.getElementById("profilepreview");

	if (!e) return;

	items = "";
	for (var i in itemsChosen[tidEditing])
		if (itemsChosen[tidEditing][i] != null)
			if (itemsChosen[tidEditing][i][1] != null)
				items += ", " + itemsChosen[tidEditing][i][1];

	custom = document.getElementById("customitems-" + tidEditing).value;
	if (custom != "")
		items += ", " + custom;

	if (items == "")
		txt = "";
	else
		txt = profilePreview + items.substring(2) + ".";


	e.innerHTML = txt;
}

////work////

var newWork = new Array();

function selectWorkType()
{
  html = '<div style="width:400px; height:75px; text-align:center; margin-top:20px;">Is this Job on a Vessel?<div style=" margin-top: 20px; text-align:center;"><input type="button" name="Yes" value="Yes" onclick="javascript: addWork( 1 ); closePopUp();" class="button" />&nbsp;&nbsp;<input type="button" name="No" value="No" class="button" onclick="javascript: addWork( 0 ); closePopUp();" /> </div></div>';
  showPopUp( '', html );
}

function addWork( vessel )
{
	getAjax("/profile/addwork.php?n=" + newWork.length + "&vessel=" + vessel, function (html)
	{
    temp = document.getElementById( 'work_buttons' )
      if( temp ) temp.style.display='';
		newdiv = document.createElement("div");
		newdiv.innerHTML = html;
		document.getElementById('newworks').appendChild(newdiv);
		
			$('.isvessel').last().attr('checked', vessel == 1);
		
		//employers['n' + newWork.length] = new actb(document.getElementById("employer-n" + newWork.length), (vessel==1?'vessel':'employer') );
		occupations['n' + newWork.length] = new actb(document.getElementById("occupation-n" + newWork.length), "occupation");

		newWork[ newWork.length ] = vessel;
		addWorkEditHandlers();
	});
}

function toggleEdit(w)
{
	e = document.getElementById(w);

  if( e )
	if (e.style.display == "none")
	{
		document.getElementById(w + "edit").style.display = "none";
		e.style.display = "";
	}
	else
	{
		document.getElementById(w + "edit").style.display = "";
		e.style.display = "none";
	}
}

function saveWork()
{
	//items = ['month-0', 'month-1', 'year-0', 'year-1', 'employer', 'occupation', 'descr', 'www'];
	items = ['month-0', 'month-1', 'year-0', 'year-1', 'descr', 'www', 'isvessel'];

	works = new Array();

	for (i = 0; i <newWork.length; i++)
	{
		wid = "n" + i;
    if( wid == '' ) continue;

		obj = {};

		e = document.getElementById("remove-" + wid);
		if (e && e.checked)
    {
      var temp = document.getElementById("work-container-" + wid);
      if( temp ) temp.style.display = 'none';
      continue;
    }

    if( wid.substring(0,1) == 'n' )
    {
      //Must be a new one
      var index = parseInt( wid.substring( 1 ) );
      obj.vessel = newWork[index];
    }

		obj.present = 0;

    e = document.getElementById("present-" + wid);
    if( e ) obj.present = e.checked ? 1 : 0;

		for (var j in items)
    {
      temp = document.getElementById(items[j] + "-" + wid);
      if( temp == null ) continue;

      if( temp.value == "" && ( j == 0 || j == 2) ) {
        alert( 'Please verify that starting dates have been completed before continuing.' );
        return false;
      }

      if( items[j] == "descr" )
   			obj[items[j]] = encodeURIComponent( temp.value );
      else
   			obj[items[j]] = temp.value;
    }
	
	obj['isvessel'] = $('#isvessel-' + wid).attr('checked');
	//alert(obj['isvessel']);

    temp = document.getElementById("employer-" + wid);
    if( temp == null ) continue;
    temp = document.getElementById("occupation-" + wid);
    if( temp == null ) continue;

		if (employers[wid] > 0 )
    {
			obj['employer'] = {'val': employers[wid]};
    }
    else
    {
			obj['employer'] = {'txt': document.getElementById("employer-" + wid).value};


      if( document.getElementById("employer-" + wid).value == "" )
      {
        alert( "Please verify that the employer fields have been completed before continuing." );
        return false;
      }
    }

		if (occupations[wid].actb_val == null)
    {
			obj['occupation'] = {'txt': document.getElementById("occupation-" + wid).value};
      if( document.getElementById("occupation-" + wid).value == "" )
      {
        alert( "Please verify that the occupation fields have been completed before continuing." );
        return false;
      }
    }
		else
			obj['occupation'] = {'val': occupations[wid].actb_val};

		works.push(obj);
	}

	for (i = 0; i < wids.length; i++)
	{
		wid = wids[i];
    if( wid == '' ) continue;

		obj = {};

		e = document.getElementById("remove-" + wid);
		if (e && e.checked)
    {
      var temp = document.getElementById("work-container-" + wid);
      if( temp ) temp.style.display = 'none';
      continue;
    }

    if( wid.substring(0,1) == 'n' )
    {
      //Must be a new one
      var index = parseInt( wid.substring( 1 ) );
      obj.vessel = newWork[index];
    }

		obj.present = 0;

    e = document.getElementById("present-" + wid);
    if( e ) obj.present = e.checked ? 1 : 0;

		for (var j in items)
    {
      temp = document.getElementById(items[j] + "-" + wid);
      if( temp == null ) continue;

      if( items[j] == "descr" )
   			obj[items[j]] = encodeURIComponent( temp.value );
      else
 	  		obj[items[j]] = temp.value;
    }

	obj['isvessel'] = $('#isvessel-' + wid).attr('checked');
	//alert($('#' + items[j] + "-" + wid).attr('checked'));
	
    temp = document.getElementById("employer-" + wid);
    if( temp == null ) continue;
    temp = document.getElementById("occupation-" + wid);
    if( temp == null ) continue;

		if (employers[wid] > 0 )
			obj['employer'] = {'val': employers[wid] };
    else
    {
			obj['employer'] = {'txt': document.getElementById("employer-" + wid).value};

      if( document.getElementById("employer-" + wid).value == "" )
      {
        alert( "Please verify that the employer fields have been completed before continuing." );
        return false;
      }
    }

		if (occupations[wid].actb_val == null)
    {
			obj['occupation'] = {'txt': document.getElementById("occupation-" + wid).value};

      if( document.getElementById("occupation-" + wid).value == "" )
      {
        alert( "Please verify that the occupation fields have been completed before continuing." );
        return false;
      }
    }
		else
			obj['occupation'] = {'val': occupations[wid].actb_val};

		works.push(obj);
	}

	postAjax("/profile/savework.php", "json_works=" + escape(json_encode(works)), function(html)
	{
    e = document.getElementById('work');
    if( e )
  		e.innerHTML = html;

  	for (i = 0; i < newWork.length; i++)
    {
      var temp = document.getElementById( 'remove2-n' + i );
      if( temp )
        temp.style.display='';
    }
	});
	
	toggleEdit("work");
}

///end work///

///network///
function newNetwork()
{
	html  = '<div style="text-align: left;"><div class="embedlabel" style="line-height: 22px;">Name:</div>';
	html += '<div class="embedinput">';
	html += '	<input type="text" id="newnetname" style="width: 246px;" />';
	html += '</div></div>' + locationHTML;
	html += '<div style="clear: both; padding-top: 15px; text-align: center;">';
	html += '	<input type="button" class="button" value="Add<? if(!$adminSite) { ?> and Join<? } ?>" onclick="javascript:doNewNetwork();" /> &nbsp;&nbsp; <input type="button" class="button" value="Cancel" onclick="javascript:closePopUp();" />';
	html += '</div>';

	showPopUp("Add a network", html);
}

function doNewNetwork()
{
	city = document.getElementById("user-city").value;
	name = escape(document.getElementById("newnetname").value);

	postAjax("/profile/newnetwork.php", "c=" + city + "&n=" + name, "doNewNetworkHandler");
}

function doNewNetworkHandler(x)
{
	eval("var data = " + x + ";");
	
	if (data.error)
		alert(data.error);
	else
	{
<? if( !$adminSite ) { ?>
		userNetworks[userNetworks.length] = data;
		saveNetworks();
		closePopUp();
<? } else { ?>
    location.reload();

<? } ?>
	}
}

function saveWorkAndEducation()  //For people who are just signing up
{
  e1 = document.getElementById("occupation");
  e6 = document.getElementById("sector");

  if( selectedValue( e6 ) == 0 || e1.value == '' )
  {
    alert( "Please choose a sector and headline before continuing." );
    return false;
  }

  obj = ({
  			'occupation': e1.value,
        'sector': selectedValue( e6 ),
        'signup':1
  		});

  postAjax("/profile/updatecontactinfo.php", "json_info=" + escape(json_encode(obj)), "void" );

  saveWork();

  var saveEdus = [];
  for (i = 0; i < eduMax; i++)
  {
  	if (document.getElementById("edu-" + i).style.display == "none")
  		continue;

  	obj = {'start': document.getElementById("edu-year0-" + i).value, 'stop': document.getElementById("edu-year1-" + i).value};

  	if (schools[i].actb_val == null)
  		obj['school'] = {'txt': document.getElementById("edu-school-" + i).value};
  	else
  		obj['school'] = {'val': schools[i].actb_val};

  	saveEdus.push(obj);
  }

  data = "json_edus=" + escape(json_encode(saveEdus));

  e = document.getElementById("about");
  if( e )
  {
    if( e.value != 'Add something about yourself or do some bragging here. Examples: have 3 kids, own a fishing boat, surf and work in the yacht charter biz.' )
      data += "&about=" + escape( e.value );
  }

//  alert( data );
	postAjax("/profile/saveabout.php", data, function (data)
	{
     window.location.href = '/signup/addphoto.php';
	});

}

function changeEmployerSelection( wid, gid, name )
{
  employers[wid] = gid;
  e = document.getElementById( 'employer-' + wid );
  if( e )
    e.value = name;
}





/////START autocomplete/////

$('#workedit').ready(
	function ()
	{
		addWorkEditHandlers();
	}
);

function addWorkEditHandlers()
{
	$('#workedit input').not('.autocomplete').focus(
		function ()
		{
			$('#workedit ul.autocomplete').remove();
		}
	);
	
	addAutocompleteHandlers();
}

function ac_get_employer_or_vessel(data)
{
	log(data, 'autocomplete response received for ' + data.guid);
	
	html  = '<ul class="autocomplete">';
	for (i in data.results)
	{
		html += '<li data-gid="' + data.results[i].gid + '">';
		if (data.results[i].type == 'V')
		{
			img_url = (typeof data.results[i].container_url == 'string' ? data.results[i].container_url + '/' + data.results[i].hash + '_square.jpg' : '/images/no_vessel.png');
			html += '<img src="' + img_url + '">';
			html += '<div><span class="name">' + data.results[i].title + '</span><br>';
			if (typeof data.results[i].exnames == 'string')
				html += 'ex. ' + data.results[i].exnames;
			html += '<br>' + data.results[i].length_ft + ' ft / ' + data.results[i].length_m + ' m';
		}
		else
		{
			img_url = (typeof data.results[i].container_url == 'string' ? data.results[i].container_url + '/' + data.results[i].hash + '_square.jpg' : '/images/company_default.png');
			html += '<img src="' + img_url + '">';
			html += '<div><span class="name">' + data.results[i].title + '</span><br>' + data.results[i].type_str;
		}
		html += '</div></li>';
	}
	html += '</ul>';
	
	$('ul.autocomplete').remove();
	
	if (data.results.length > 0)
	{
		$(html).appendTo('#workedit').css('top', $(el).offset().top - $('#workedit').offset().top + $(el).height() + 5).css('left', $(el).offset().left - $('#workedit').offset().left);
		
		$('ul.autocomplete li').mousedown(
			function ()
			{
				el = $('input[data-guid="' + guid + '"]');
				
				wid = $(el).attr('id').substring(9);
				gid = $(this).attr('data-gid');
				title = $(this).find('.name').text();
				
				changeEmployerSelection(wid, gid, title);
				
				$('ul.autocomplete').remove();	
			}
		);
	}
}