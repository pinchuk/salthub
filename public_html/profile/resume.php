<?php

include '../inc/inc.php';

// add required javascript files
$scripts[] = '/profile/profilemgmt.js.php';

// get user info from the db
include 'resume_top_query.php';

$content_wide = true;
include '../header.php';

if ($is_staging_server)
	echo '<div style="position: absolute; left: 5px;">' . $user['uid'] . '<!-- user data: ' . var_export($user, true) . ' --></div>';

?>

<div class="resume_top">
	<div class="wrapper">
		<?php include 'resume_top.php'; ?>
	</div>
	<div style="clear: both;"></div>
	<?php include 'my_pages.php'; ?>
</div>

<div class="resume_content">
	<div class="right">
		<div class="thin_left">
			<h3 class="subhead"><span>Connections</span></h3>
			<div class="blue_box connections" onclick="javascript:loadShare('suggest', 1, <?=$user['uid']?>);">
				<h4 class="bigtext">Help <?=current(explode(' ', $user['name']))?> Connect</h4>
				<div class="photos">
					<?php
					$x = mysql_query("select uid,pic from users where active=1 and uid != " . $API->uid . " and pic > 0 order by rand() limit 2");
					
					while ($y = mysql_fetch_array($x, MYSQL_ASSOC))
						echo '<img src="' . $API->getThumbURL(1, 48, 48, $API->getUserPic($y['uid'], $y['pic'])) . '" alt>';
					?>
				</div>
				<p>Do you know someone who may know or think should know <?=current(explode(' ', $user['name']))?>?</p>
				<span class="bigtext">Get Started &gt;</span>
				<div style="clear: both;"></div>
			</div>
		</div>
		
		<div class="thin_right">
			<h3 class="subhead"><span>Sponsors</span></h3>
			<?php showAd("companion"); ?>
		</div>
	</div>
	
	<div class="left">
		<h3 class="subhead"><span>People You May Know</span></h3>
		<div class="pymk_box blue_box no_pad">
			<div id="pymk_container0" class="pymkbig">
				<?php $limit = 18; $_GET['i'] = 0; include "../getpymk.php"; ?>
			</div>
			
			<a href="javascript:void(0);" onclick="javascript:pymkShowMore(this, 0, 9, false);">Show more &#0133;</a>
		</div>
	</div>
</div>

<script type="text/javascript">
is_own_resume = <?=$user['uid'] == $API->uid ? 'true' : 'false'?>;

function updatedUserInfo(data)
{
	$('.resume_top>div.wrapper').load('/profile/resume_top.php?uid=<?=$user['uid']?>', '', initializeHoverLinks);
}

function toggleEmail()
{
	dur = 250;
	
	if ($('.resume_top .email').is(':visible'))
		$('.resume_top .email').children().fadeOut(dur, function () { $(this).parent().slideUp(dur) } );
	else
		$('.resume_top .email').slideDown(dur, function () { $(this).children().fadeIn(dur) } );
}

$('.resume_top .change_photo input').fileupload({
		url: "/upload/photo.php?userpic=1&json=1",
		dataType: 'json',
		send: function (e, data)
		{
			$('.resume_top .upload').hide();
			$('.resume_top .uploading').css('width', $('.resume_top .upload').width()).html('Uploading &#0133;').show();
		},
		always: function (e, data)
		{
			if (typeof data.result === 'undefined')
				alert('Invalid photo uploaded');
			else
			{
				$('.resume_top .user_photo>img').attr('src', data.result.img);
				$.post('/settings/changeuserpic.php', 'pic=' + data.result.id);
				
				$('.resume_top .change_photo ul.carousel').html('');
				changePhotoScroll(0);
			}
			
			$('.resume_top .uploading').hide();
			$('.resume_top .upload').show();
		},
		progressall: function (e, data)
		{
			if (data.loaded == data.total)
				html = 'Please wait &#0133;';
			else
				html = 'Uploading (' + (data.loaded / data.total * 100).toFixed(0) + '%)';
			
			$('.resume_top .uploading').html(html);
		}
	});

// moves the change photo carousel to the specified photo
// will load more photos if none available
// true = scrolls left
// false = scrolls right
// number = scrolls to specified photo
function changePhotoScroll(scroll_left)
{
	if (typeof scroll_left == 'boolean')
	{
		cur_pos = parseInt($('.resume_top .change_photo ul.carousel').attr('data-pos'));
		
		if (scroll_left)
			n = cur_pos + 3;
		else
			n = cur_pos - 3;
		
		if (n < 0)
			n = 0;
	}
	else
		n = scroll_left; // specified the number of the photo to scroll
	
	loaded = $('.resume_top .change_photo ul.carousel li').length; // number of photos currently loaded
	more = $('.resume_top .change_photo ul.carousel').attr('data-more'); // if there are more photos to be loaded
	
	if (loaded == 0 || (n >= loaded && more == 1))
	{
		$.ajax({
			url: '/getpcphotos.php?s=' + n,
			dataType: 'json',
			success: function (json)
			{
				for (i in json.photos)
				{
					li = $('<li><img src="' + json.photos[i].img + '" data-id="' + json.photos[i].id + '" alt><img class="overlay" src="/images/use_for_profile_image.png" alt></li>');
					
					$(li).mouseover(
						function ()
						{
							$(this).children('.overlay').show();
						}
					);
					
					$(li).mouseout(
						function ()
						{
							$(this).children('.overlay').hide();
						}
					);
					
					$(li).click(
						function ()
						{
							$('.resume_top .user_photo>img').attr('src', $(this).children('img').attr('src'));
							$.post('/settings/changeuserpic.php', 'pic=' + $(this).children('img').attr('data-id'));
						}
					);
					
					$('.change_photo ul.carousel').append($(li));
				}

				// expand carousel so no wrapping and smooth scrolling
				loaded = $('.resume_top .change_photo ul.carousel li').length;
				$('.resume_top .change_photo ul.carousel').css('width', loaded * 193);
				
				$('.resume_top .change_photo ul.carousel').attr('data-more', json.more);
				
				changePhotoScroll(n);
			}
		});
	}
	else if (n < loaded)
	{
		// each photo is 118px wide, with a margin of 15px
		$('.resume_top .change_photo ul.carousel').animate( { left: -15 + n * -133 } );
		$('.resume_top .change_photo ul.carousel').attr('data-pos', n);
		
		$('.resume_top .change_photo .arrow_right').css('display', n + 3 >= loaded && more == 0 ? 'none' : 'block');
		$('.resume_top .change_photo .arrow_left').css('display', n == 0 ? 'none' : 'block');
	}
}

function showChangePhoto()
{
	changePhotoScroll(0);
	$('.user_summary .button').fadeOut(250);
	$('.user_info h4').fadeOut(500,
		function ()
		{
			$('.user_info')
				.attr('data-original_height', $('.user_info').css('height'))
				.css('height', $('.user_info h4').height());
			$('.resume_top .change_photo').slideDown();
		}
	);
}

function hideChangePhoto()
{
	$('.resume_top .change_photo').slideUp(500,
		function ()
		{
			$('.user_info h4').fadeIn(500);
			$('.user_info').css('height', $('.user_info').attr('data-original_height'));
			$('.user_summary div.button').fadeIn(500);
		}
	);
}

function saveAboutEdit()
{
	$('#about_edit .options').hide();
	$('#about_edit .wait').show();
	
	data = new Object();
	data.about = $('#about_edit textarea').val();
	data.ajax = 1;
	
	$.ajax({
		type: 'POST',
		url: '/profile/saveabout.php',
		data: data,
		complete: function (data)
		{
			if (data.responseJSON.success)
			{
				$('.resume_top h4').html(data.responseJSON.about);
				toggleAboutEdit();
			}
		}
	});
}

function toggleAboutEdit(show)
{
	ta = $('#about_edit');
	
	dur = 250;
	
	if ((typeof show == "boolean" && !show) || (typeof show == "undefined" && $(ta).is(':visible')))
	{
		$(ta).fadeOut(dur);
	}
	else if (!$(ta).is(':visible'))
	{
		p = $('.resume_top h4').position();
		
		$(ta).children('textarea')
			.val($('.resume_top h4').text())
			.attr('data-changed', 'no');
		
		$(ta)
			.css('top', p.top - 6)
			.css('width', $('.resume_top h4').width() - 2);
		
		$(ta).children('.wait').hide();
		$(ta).children('.options').show();
		
		$(ta).fadeIn(dur);
	}
	else // user is moving mouse rapidly over textarea
	{
		$(ta).stop().hide();
	}
}

/*function adjustUserPhotoHeight()
{
	min_height = $('.resume_top div.user_info h3').height() + $('.resume_top div.user_info h4').height() + $('.resume_top div.buttons').height();
	new_height = $('.resume_top div.user_photo').height() - 40;
	
	$('.resume_top div.user_info').css('height', new_height < min_height ? min_height : new_height);
}*/

$('.resume_top div.user_photo img.user_photo_img').load(
	function ()
	{
		/*if (!$('.resume_top .change_photo').is(':visible'))
			adjustUserPhotoHeight();*/
		
		$('.resume_top div.user_photo').mouseover(
			function ()
			{
				if (!$('.resume_top .change_photo').is(':visible'))
					$('.resume_top div.user_photo a.button').show();
			}
		);
		
		$('.resume_top div.user_photo').mouseout(
			function ()
			{
				$('.resume_top div.user_photo a.button').hide();
			}
		);
		
		//showChangePhoto();
	}
);

var page_timer = 0;
$('.resume_top .myPages').ready(
	function ()
	{
		// set up hover about edit
		if (is_own_resume)
		{
			$('.resume_top h4').mouseenter(
				function ()
				{
					toggleAboutEdit(true);
				}
			);
		}
		
		// set up triggers to hide textarea when focus lost
		$('#about_edit').mouseleave(
			function ()
			{
				if ($(this).children('textarea').attr('data-changed') == 'no' && !$(this).children('textarea').is(':focus'))
					toggleAboutEdit(false);
			}
		);
		
		$('#about_edit').children('textarea').blur(
			function ()
			{
				if ($(this).attr('data-changed') == 'no')
					toggleAboutEdit(false);
			}
		).keypress(
			function ()
			{
				$(this).attr('data-changed', 'yes');
			}
		);
		
		// set up my pages dropdown
		$('#pages_link, .resume_top .myPages').mouseenter(
			function ()
			{
				if (page_timer > 0)
				{
					clearTimeout(page_timer);
					page_timer = 0;
				}
				
				fadePages(true);
			}
		);
		
		$('#pages_link, .resume_top .myPages').mouseleave(
			function ()
			{
				if (page_timer == 0)
					page_timer = setTimeout('fadePages(false);', 500);
			}
		);
	}
);

function fadePages(fade_in)
{
	o = $('#pages_link').offset();
	
	e = $('.resume_top .myPages').css('left', o.left).css('top', o.top + $('.resume_top h2').height());
	
	if (fade_in)
		$(e).slideDown(100);
	else
		$(e).slideUp(100);
}

$(document).ready(
	function ()
	{
		initializeHoverLinks();
		initializeDropdowns();
	}
);

function initializeDropdowns()
{
	$('.button').not('[data-menuinit]').each(
		function ()
		{
			if ($(this).children('ul.menu').length > 0)
			{
				$(this).attr('data-menuinit', 'yes');
				
				$(this).click(
					function ()
					{
						$(this).children('ul.menu').slideToggle();
					}
				);
			}
		}
	);
}

function initializeHoverLinks()
{
	$('a[data-onhover]').not('[data-hoverinit]').each(
		function ()
		{
			// says the element has been initalized so it's not again
			$(this).attr('data-hoverinit', 'yes');
			
			e = $('<span class="hover"></span>')
				.html($(this).attr('data-onhover'));
			
			dur = 200;
			
			$(this)
				.wrapInner('<span class="default"></span>')
				.children('.default')
					.css('display', 'inline-block')
					.parent()
				.css('position', 'relative')
				.append($(e))
				.mouseover(
					function ()
					{
						$(this).children('.hover')
							.css('width', $(this).children('.default').width())
							.css('height', $(this).children('.default').height())
							.fadeIn(dur);
						
						$(this).children('.default').animate({ 'opacity': 0 }, dur);
					}
				).mouseleave(
					function ()
					{
						$(this).children('.hover').fadeOut(dur);
						$(this).children('.default').animate({ 'opacity': 1 }, dur);
					}
				);
		}
	);
}

$(window).resize(
	function ()
	{
		if ($(window).width() <= 965)
			$('body').addClass('thin');
		else
			$('body').removeClass('thin');
	}
);

$(window).resize();
</script>

<?php

include '../footer.php';

?>