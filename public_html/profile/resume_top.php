<?php

include_once '../inc/inc.php';

// get user info from the db
include_once 'resume_top_query.php';

// used to display contact info on resume
$contact_info = array();

$contact_info[] = array(
	'update' => 'occupation',
	'img' => 'mobile_phone',
	'name' => 'mobile phone number',
	'val' => formatPhoneNumber($user['cell'])
	);
	
$contact_info[] = array(
	'update' => 'occupation',
	'img' => 'desk_phone',
	'name' => 'work phone number',
	'val' => formatPhoneNumber($user['public_phone'])
	);

$contact_info[] = array(
	'update' => 'occupation',
	'img' => 'inbox_symbol',
	'request_message' => 'I would like to send you a message outside of ' . $siteName . ', but noticed that you have not added a public e-mail address.  Please add one so that myself and others have an alternate way to reach you.',
	'val' => $user['public_email']
	);

$contact_info[] = array(
	'update' => 'occupation',
	'img' => 'fax_symbol',
	'name' => 'fax number',
	'val' => formatPhoneNumber($user['fax'])
	);

$contact_info[] = array(
	'update' => 'basicinfo',
	'img' => 'marker_symbol',
	'name' => 'location',
	'val' => implode(', ', array_filter(array(ucwords(strtolower($user['city'])), ucwords(strtolower($user['region'])), $user['country'])))
	);

// strings for prepopulated request messages
$request_subject = 'Contact information request';
$request_message = 'I would like to contact you but noticed that you have not added a %s.&nbsp; Please add one so that myself and others can get in touch with you.';

// social sites to display on resume
$sites = array();
$sites[] = array("fbid", "http://www.facebook.com/profile.php?id=", "f.png");
$sites[] = array("twusername", "http://twitter.com/", "t.png");
$sites[] = array("linkedin_url", "http://www.linkedin.com/", "linkedin.png");
$sites[] = array("ytid", "http://www.youtube.com/", "youtube.png");
$sites[] = array("www", "http://", "layout_header.png");

?>

	<ul>
		<li>
			<h2><a href="resume"><?=$user['name']?></a></h2>
			<a href="/pages" class="iconlink" id="pages_link"><img src="/images/page.png" alt><?=current(explode(' ', $user['name']))?>&rsquo;s Pages</a>
			<a href="#" class="download">Download my resume</a>
		</li>
		<li>
			<div class="user_summary">
				<div class="user_photo">
					<a href="<?=$API->getPhotoURL($user['pic'], 'Profile Photos')?>"><img class="user_photo_img" src="<?=$API->getThumbURL(0, 178, 266, $API->getUserPic($user['uid'], $user['pic']))?>" alt></a>
					<?php
					if ($API->uid == $user['uid'])
						echo '<a class="button change_photo_button" href="javascript:void(0);" onclick="javascript:showChangePhoto();"><img src="/images/image.png" alt>Change Photo</a>';
					?>
				</div>
				
				<div class="change_photo">
					Change profile photo: <a href="javascript:void(0);" onclick="javascript:hideChangePhoto();">Close</a>
					<a href="javascript:void(0);" onclick="javascript:$(this).siblings('.browse').click();" class="upload">Upload New Photo</a>
					<span class="uploading"></span>
					<div class="carousel_container">
						<ul class="carousel"></ul>
					</div>
					<a href="javascript:void(0);" onclick="javascript:changePhotoScroll(false);" class="arrow_left"></a>
					<a href="javascript:void(0);" onclick="javascript:changePhotoScroll(true);" class="arrow_right"></a>
					<input class="browse" type="file" name="Filedata[]">
				</div>
				
				<div class="user_info_buttons">
					<div class="user_info">
						<?php
						$text = ($user['cat'] > 0 ? $user['catname'] . ' - ' : '') . $user['occupation'] . ' @ ' . $user['company'];
						
						if ($API->uid == $user['uid']) // show edit
							echo '<h3><a data-onhover="(Change this)" href="javascript:void(0);" onclick="javascript:updateUserInfo(\'company\', updatedUserInfo);">' . $text . '</a></h3>';
						else
							echo '<h3>' . $text . '</h3>';
						?>
						<h4><?=$user['about_me']?></h4>
						<div id="about_edit">
							<textarea></textarea>
							<div class="options">
								<img src="/images/x.png" onclick="javascript:toggleAboutEdit(false);" alt>
								<img src="/images/accept.png" onclick="javascript:saveAboutEdit();" alt>
							</div>
							<div class="wait">
								<img src="/images/resume_wait16.gif" alt>
							</div>
						</div>
					</div>
					
					<div class="buttons">
						<?php
						if ($API->uid == $user['uid'])
						{
							// user's own resume - show edit button
							
							?>
							
							<div class="button button_icon_right">Edit<img src="/images/arrow_down.png" alt>
								<ul class="menu button">
									<li><a href="javascript:void(0);" onclick="javascript:updateUserInfo('location', updatedUserInfo);">Location</a></li>
									<li><a href="javascript:void(0);" onclick="javascript:updateUserInfo('occupation', updatedUserInfo);">Contact Information</a></li>
									<li><a href="javascript:void(0);" onclick="javascript:updateUserInfo('company', updatedUserInfo);">Professional Information</a></li>
									<li><a href="javascript:void(0);" onclick="javascript:editFollowMe();">Other Profiles</a></li>
								</ul>
							</div>
							
							<?php
						}
						else
						{
							// only show connect/message buttons if not own resume
							
							echo createConnectButton(
								'<a class="button" href="javascript:void(0);" onclick="%s"><img src="/images/user_add.png" alt>Connect</a>',
								'<a class="button button_disabled" href="javascript:void(0);" onclick="%s"><img src="/images/user.png" alt>Pending</a>',
								'<a class="button" href="javascript:void(0);" onclick="%s"><img src="/images/check.png" alt>Accept</a>',
								'<a class="button button_disabled" href="javascript:void(0);" onclick="%s"><img src="/images/connected.png" alt>Connected</a>',
								'<a class="button button_disabled" href="javascript:void(0);" onclick="%s"><img src="/images/wait16.gif" alt>Wait &#0133;</a>',
								$user['uid']
							);
							
							echo '<a href="javascript:void(0);" onclick="javascript:showSendMessage(' . $user['uid'] . ', \'' . htmlentities($user['name']) . '\', \'' . $API->getThumbURL(0, 85, 128, $API->getUserPic($user['uid'], $user['pic'])) . '\');" class="button"><img src="/images/email_add.png" alt>Message</a>';
						}
						?>
						<div class="button button_icon_right viewmore">View More<img src="/images/arrow_down.png" alt></div>
					</div>
				</div>
			</div>
			<div class="right">
				<div class="thin_left">
					<ul class="user_contact">
						<?php
						for ($i = 0; $i < count($contact_info); $i++)
						{
							$classes = array('info_' . $i);
							
							if ($i == count($contact_info) - 1) // will make the last piece of info on a line by itself when in widescreen
								$classes[] = 'clear_wide';
							
							$class = implode(' ', $classes);
							$info = $contact_info[$i];
							
							if (empty($info['val']) && $user['uid'] == $API->uid) // no information here and it's the current user's resume
								echo '<li class="' . $class . '"><a href="javascript:void(0);" onclick="javascript:updateUserInfo(\'' . $info['update'] . '\', updatedUserInfo);" class="contact"><img src="/images/' . $info['img'] . '.png" alt><img src="/images/plus.png" class="add" alt>add</a></li>';
							elseif (empty($info['val'])) // no information here and someone else is viewing it
							{
								if ($info['name']) // use request message "template"
									$msg = sprintf($request_message, $info['name']);
								else // todd wants a custom message for this one
									$msg = $info['request_message'];
								
								echo '<li class="' . $class . '"><a href="javascript:void(0);" onclick="javascript:showSendMessage(' . $user['uid'] . ', \'' . addslashes($user['name']) . '\', \'' . $API->getThumbURL(0, 48, 48, $API->getUserPic($user['uid'], $user['pic'])) . '\', \'' . addslashes($request_subject) . '\', \'' . addslashes($msg) . '\');" class="contact"><img src="/images/' . $info['img'] . '.png" alt>Request</a></li>';
							}
							elseif ($info['val'] == $user['public_email']) // user has public email, so show 'view email' link
							{
								echo '<li class="' . $class . '"><a class="contact view_email" href="javascript:void(0);" onclick="javascript:toggleEmail();"><img src="/images/' . $info['img'] . '.png" alt>view e-mail address</a><div class="contact email">';
								
								if ($user['uid'] == $API->uid) // user's own resume - show edit
									echo '<a href="javascript:void(0);" onclick="javascript:updateUserInfo(\'' . $info['update'] . '\', updatedUserInfo);" data-onhover="(change)">' . $info['val'] . '</a>';
								else
									echo '<a href="mailto:' . $info['val'] . '">' . $info['val'] . '</a>';
								
								echo '</div></li>';
							}
							elseif ($user['uid'] == $API->uid) // user's own profile - let them edit the info
								echo '<li class="' . $class . '"><span class="contact"><img src="/images/' . $info['img'] . '.png" alt><a data-onhover="(change)" href="javascript:void(0);" onclick="javascript:updateUserInfo(\'' . $info['update'] . '\', updatedUserInfo);">' . $info['val'] . '</a></span></li>';
							else // other info
								echo '<li class="' . $class . '"><span class="contact"><img src="/images/' . $info['img'] . '.png" alt>' . $info['val'] . '</span></li>';
						}
						?>
					</ul>
				</div>
				<div class="thin_right">
					<ul class="user_social">
						<?php
						foreach ($sites as $site)
							if (!empty($user[$site[0]]))
								echo '<li><a href="' . $site[1] . $user[$site[0]] . '" target="_blank"><img src="/images/' . $site[2] . '" alt></a></li>';
						?>
					</ul>
				</div>
			</div>
		</li>
	</ul>

<?php

// creates the connect with a friend button
function createConnectButton($html_connect, $html_pending, $html_accept, $html_connected, $html_wait, $friend_uid, $friend_status = null)
{
	global $API;
	
	$can_accept = false;
	
	if (is_null($friend_status))
	{
		$x = mysql_query("select * from friends where (id1='$friend_uid' and id2='" . $API->uid . "') or (id2='$friend_uid' and id1='" . $API->uid . "') limit 1");
		
		if (mysql_num_rows($x) > 0)
		{
			$row = mysql_fetch_array($x, MYSQL_ASSOC);

			$friend_status = $row['status'];
			
			if ($friend_status == 0 && $row['id1'] == $friend_uid)
				$can_accept = true;
		}
		else
			$friend_status = -1;
	}
	
	$onclick = "javascript:connectButton_Click(this);";
	$html_inject = $onclick . '" data-uid="' . $friend_uid . '" data-reqstatus="%d" data-wait="' . htmlentities(sprintf($html_wait, 'javascript:void(0);')) . '" data-finish="';
	
	if ($friend_status == 1) // already friends
		$ret = sprintf($html_connected, '');
	elseif ($friend_status == 0 && !$can_accept) // pending connection sent by this user
		$ret = sprintf($html_pending, '');
	elseif ($friend_status == -1) // not friends, no request yet
	{
		$html_inject .= htmlentities(sprintf($html_pending, 'javascript:void(0);'));
		$html_inject  = sprintf($html_inject, 0); // we will request status = 0 (pending friends)
		$ret = sprintf($html_connect, $html_inject);
	}
	elseif ($can_accept) // other user sent request
	{
		$html_inject .= htmlentities(sprintf($html_connected, 'javascript:void(0);'));
		$html_inject  = sprintf($html_inject, 1); // we will request status = 1 (friends)
		$ret = sprintf($html_accept, $html_inject);
	}
	
	return $ret;
}

function formatPhoneNumber($phoneNumber) {
    $phoneNumber = preg_replace('/[^0-9]/','',$phoneNumber);

    if(strlen($phoneNumber) > 10) {
        $countryCode = substr($phoneNumber, 0, strlen($phoneNumber)-10);
        $areaCode = substr($phoneNumber, -10, 3);
        $nextThree = substr($phoneNumber, -7, 3);
        $lastFour = substr($phoneNumber, -4, 4);

        $phoneNumber = '+'.$countryCode.' '.$areaCode.' '.$nextThree.' '.$lastFour;
    }
    else if(strlen($phoneNumber) == 10) {
        $areaCode = substr($phoneNumber, 0, 3);
        $nextThree = substr($phoneNumber, 3, 3);
        $lastFour = substr($phoneNumber, 6, 4);

        $phoneNumber = ''.$areaCode.' '.$nextThree.' '.$lastFour;
    }
    else if(strlen($phoneNumber) == 7) {
        $nextThree = substr($phoneNumber, 0, 3);
        $lastFour = substr($phoneNumber, 3, 4);

        $phoneNumber = $nextThree.' '.$lastFour;
    }

    return $phoneNumber;
}

?>