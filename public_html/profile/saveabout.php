<?php

include "../inc/inc.php";

if (isset($_POST['ajax']))
	header('Content-type: application/json');

$API->setToDoItem( TODO_PROFILE );

////////////// save about info //////////////
if (isset($_POST['about']))
{
	sql_query("delete from aboutme where uid=" . $API->uid);
	sql_query("insert into aboutme (uid,txt) values (" . $API->uid . ",'" . str_replace("  ", "&nbsp; ", $_POST['about']) . "')");
	
	if (isset($_POST['ajax']))
		echo json_encode(array('success' => true, 'about' => $_POST['about']));
	
	return;
}

////////////// save education info //////////////
elseif (isset($_POST['json_edus']))
{
	$edus = json_decode($_POST['json_edus'], true);

	sql_query("delete from education where uid=" . $API->uid);
	
	//echo "<xmp>"; print_r($edus); echo "</xmp>";
	
	foreach ($edus as $edu)
	{
		$edu['start'] = intval($edu['start']);
		$edu['stop'] = intval($edu['stop']);

		if ($edu['start'] > $edu['stop'] || $edu['start'] == 0 || $edu['stop'] == 0 || (empty($edu['school']['txt']) && empty($edu['school']['val'])))
			continue;
		
		if ($edu['school']['txt'])
		{
			//user entered custom text, see if this school exists already as a page
			$edu['school']['txt'] = htmlentities($edu['school']['txt']);

			$gid = quickQuery("select gid from pages where gname='" . addslashes($edu['school']['txt']) . "' and cat=" . CAT_SCHOOL);

			if (empty($gid))
			{
				//page does not exist, so let's create it
				$gid = createPageWithInfo($edu['school']['txt'], CAT_SCHOOL);
			}
		}
		else
		{
			$gid = intval($edu['school']['val']);
			
			//check page privacy
			if (quickQuery("select privacy from pages where gid=$gid") != PRIVACY_EVERYONE)
				continue;
		}
		
		// see if user is in page already
		if (quickQuery("select count(*) from page_members where gid=$gid and uid={$API->uid}") == 0)
		{
			// add user to page
			sql_query("insert into page_members (gid,uid) values ($gid,{$API->uid})");
			$API->feedAdd("G", $gid, null, null, $gid);
		}
		
		// add education info
		sql_query("insert into education (uid,start,stop,school) values ({$API->uid},{$edu['start']},{$edu['stop']},$gid)");
	}

  $API->feedAdd("PR7", 0 );

}

////////////// save personal info //////////////
if ($_POST['tid0'])
{
	//check to see if we are updating 2 types at once - e.g. degrees and licenses
	for ($i = 0; $i < 2; $i++)
	{
		if ($_POST["tid$i"])
		{
			$updates[] = array(
					"tid" => intval($_POST["tid$i"]),
					"custom" => $_POST["custom$i"],
					"pids" => $_POST["pids$i"]
				);
		}
	}

	foreach ($updates as $update)
	{
		$tid = $update['tid'];

    $likes = array( 100,102,103,105,107 );

    if( $tid == CAT_LICDEG )
    {
      $API->feedAdd("PR6", 0 );
    }
    else if( in_array( $tid, $likes ) )
    {
      $API->feedAdd("PR9", 0 );
    }


    $subscribed = array();
    $q = sql_query( "select gid from personal where uid='$API->uid' and gid in (select gid from pages where cat=$tid)" );
    while( $r = mysql_fetch_array( $q ) )
    {
      $subscribed[] = $r['gid'];
    }

		sql_query("delete from personal where uid=" . $API->uid . " and gid in (select gid from pages where cat=$tid)");

		$gids = array();

		//look at user "custom" entries and see if they already exist in the db
		$custom = explode(",", $update['custom']);
		if (count($custom) > 0)
		{
			foreach ($custom as $c)
			{
				$cust = trim($c);

				if (empty($cust)) continue;

        $cust = addslashes( $cust );
				$gid = quickQuery("select gid from pages where gname='$cust' and cat=$tid");

				if (empty($gid)) // could not find it
				{
          $cust = stripslashes( $cust );
					$gid = createPageWithInfo($cust, $tid);
				}

				$gids[] = $gid; //add it below
			}
		}

		//process pre-defined pids
		foreach (explode(",", $update['pids']) as $gid)
		{
			$gid = intval($gid);
			$gids[] = $gid;
		}

		//add user pids to the db and add the user to the pages
    $added = array();
		foreach ($gids as $gid)
		{
			if ($gid > 0)
			{
				sql_query("insert into personal (gid,uid) values ($gid," . $API->uid . ")");

				if ($gid)
				{
					// see if user is in page already
					if (quickQuery("select count(*) from page_members where gid=$gid and uid={$API->uid}") == 0)
					{
						// add user to page
						sql_query("insert into page_members (gid,uid) values ($gid,{$API->uid})");
						$API->feedAdd("G", $gid, null, null, $gid);
					}
				}

        $added[] = $gid;
			}
		}

    //Check to see if we left any groups we were in.
    for( $c = 0; $c < sizeof($subscribed); $c++ )
    {
      if( !in_array( $subscribed[$c], $added ) )
      {
        //If so, then delete the user from the page.
        sql_query( "delete from page_members where uid='" . $API->uid . "' and gid='" . $subscribed[$c] . "' limit 1" );
      }
    }
	}
}

////////////// generate html for ajax update //////////////


if (isset($_POST['json_edus'])) //we update the info differently for education
{
  $x = mysql_query("select g.gid,g.cat as tid,g.gname as name,categories.catname as tname from personal left join pages g on g.gid=personal.gid left join categories on categories.cat=g.subcat where personal.uid='" . $API->uid . "' and (g.cat='101' or g.cat='1297') order by g.cat desc, g.gname" );

//  $x = sql_query("select g.gid,t.tid,t.name as tname,g.gname as name,special from personal p left join pages g on g.gid=p.gid left join personal_types t on t.tid=g.subcat where p.uid={$API->uid} and (g.cat=101 or g.cat=1297) order by special desc,t.name,name") or die(mysql_error());
  while ($y = mysql_fetch_array($x, MYSQL_ASSOC))
	  $info[] = $y;


	include "educationinfo.php";
}
else
{
  $x = sql_query("select g.gid,t.tid,t.name as tname,g.gname as name,special from personal p left join pages g on g.gid=p.gid left join personal_types t on t.tid=g.cat where p.uid={$API->uid} order by special,t.name,name") or die(mysql_error());
  while ($y = mysql_fetch_array($x, MYSQL_ASSOC))
  	$info[] = $y;

	$items = displayPersonalInfo(array("tid" => $tid), $info);
	
	if ($items == 0)
		echo 'You don\'t have any listed.&nbsp; Add one by <a href="javascript:void(0);" onclick="javascript:piEdit(' . $piType['tid'] . ');">clicking here</a>.';
}

$API->reloadAdData();
?>