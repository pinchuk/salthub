<?php
/*
This page saves the user's work history information (called from the user's profile page)
*/

include "../inc/inc.php";

$API->setToDoItem( TODO_PROFILE );

$works = json_decode($_POST['json_works'], true);
$savePrev = $_POST['savePrev'];

if( isset( $savePrev ) && $savePrev==1 ) $savePrev = true;
else $savePrev = false;

//if( !$savePrev )
  mysql_query("delete from work where uid={$API->uid}");

 //print_r($works);
  
foreach ($works as $w)
{

	if (intval($w['month-0']) == 0)
		$w['month-0'] = 1;

	if (intval($w['year-0']) == 0)
		$w['year-0'] = date("Y");

	$w['start'] = intval($w['year-0']) . "/" . intval($w['month-0']) . "/1 00:00:00";
	$w['stop'] = intval($w['year-1']) . "/" . intval($w['month-1']) . "/1 00:00:00";

	if ($w['present'] == 1 || strtotime($w['start']) > strtotime($w['stop']))
		$w['stop'] = '0000-00-00 00:00:00';

	foreach (array("occupation", "employer") as $f)
	{
		if ($w[$f]['val'])
			$w[$f]['val'] = intval($w[$f]['val']);
		elseif ($w[$f]['txt'])
		{
			$name = trim( addslashes($w[$f]['txt']) );

      $type = PAGE_TYPE_BUSINESS;
      if( $f == "occupation" ) $type = PAGE_TYPE_PROFESSIONAL;
      else if( $w['isvessel'] == 'on' || $w['isvessel'] == 'checked'  ) $type = PAGE_TYPE_VESSEL;      
      
			$gid = quickQuery("select gid from pages where gname='$name' and type='$type' and privacy=0");

			if ($gid)
				$w[$f]['val'] = $gid;
			else
			{
        if( $f == "occupation" )
        {
  				mysql_query("insert into pages (gname,type,cat) values ('$name','" . PAGE_TYPE_PROFESSIONAL . "'," . CAT_PROFESSIONS . ")");
	  			$w[$f]['val'] = mysql_insert_id();
        }
        else
        {
          $type = PAGE_TYPE_BUSINESS;
          if( $w['isvessel'] == 'on' ||$w['isvessel'] == 'checked' ) $type = PAGE_TYPE_VESSEL;
  				mysql_query("insert into pages (gname,type) values ('$name','" . $type . "')");
	  			$w[$f]['val'] = mysql_insert_id();
        }
			}
		}
		else
    {
			continue;
    }

    if( quickQuery( "select count(*) from page_members where gid='" . $w[$f]['val'] . "' and uid='" . $API->uid . "'" ) == 0 )
    {
  		mysql_query("insert into page_members (gid,uid) values ({$w[$f]['val']},{$API->uid})");

	  	if (mysql_insert_id())
		  	$API->feedAdd("G", $w[$f]['val'], null, null, $w[$f]['val']);
    }

		$gids[$f][] = $w[$f]['val'];
	}

	$work['uid'] = $API->uid;
	$work['start'] = $w['start'];
	$work['stop'] = $w['stop'];
	$work['descr'] = addslashes( urldecode( $w['descr'] ));
	$work['www'] = addslashes(htmlentities($w['www']));
	$work['employer'] = $w['employer']['val'];
	$work['occupation'] = $w['occupation']['val'];

	$q = "insert into work (" . implode(",", array_keys($work)) . ") values ('" . implode("','", $work) . "')";
	mysql_query($q);
}

$API->feedAdd("PR5", 0 );


ob_start();
include "workinfo.php";
$html = ob_get_contents();
ob_end_clean();

echo $html;

$API->reloadAdData();
?>