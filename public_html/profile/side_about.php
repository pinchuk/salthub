<?php
/*
This is the left side menu called when the user is on the "About" section of a user profile.

12/22/2012 - Changed sorting of Connections so that connections that are most viewed by the user are listed first.
*/

  $loggedIn = $API->isLoggedIn();


if( $loggedIn )
{
  include "side_contactinfo.php";
  include "side_contactfor.php";
  include "followme_full.php";
}
else
{
  showSignUpModule($user['name']);
}

?>

<div style="margin-top: 10px;">
	<?php


  if( (isset( $page ) || $API->userHasAccess($user['uid'], PRIVACY_CONNECTIONS ) ) && $loggedIn )
  {
		?>
		<div class="subhead parent_view_all_2 <?=$gid == 0 ? "medium_font" : ""?>" style="margin-top: 10px;">
			<div style="float: left;"><?=$gid == 0 ? "Connections" : "Members"?> <span class="number">(<?=$user['cfriends']?>)</span></div>
			<?php if ($user['cfriends'] > 0) { ?>
			<div class="view_all_2" style="float: right; font-size: 8pt; font-weight: normal; padding: 2px 5px 0 0;"><a href="javascript:void(0);" onclick="javascript:<?=$gid == 0 ? "showFriendsPopup({$user['uid']})" : "showPageMembers($gid)"?>;">view all</a></div>
			<?php } ?>
			<div style="clear: both;"></div>
		</div>
		<?php
		if ($user['cfriends'] > 0 )
		{
			$openDivs = 0;
			$i = 0;

			if ($gid == 0)
      {
//				$x = $API->getFriends($user['uid'], "rand()", 6);
        //To support special sorting of connections based on how often the user has looked at other connections
        // this code was puleld out of $API->getFriends()
        $x = array();
        $user_uid = $user['uid'];
        $limit = 6;

  			$q = sql_query("select if(id1=$user_uid,id2,id1) as uid,username,name,pic, views.num_views from friends
                 inner join users on users.uid=if(id1=$user_uid,id2,id1)
                 left join views on views.type='R' AND views.link=if(id1=$user_uid,id2,id1) AND views.uid='$user_uid'
                 where (id1=$user_uid or id2=$user_uid) and status=1 and users.active=1
                 order by num_views desc" . ($limit > 0 ? " limit $limit" : "") );
  			while ($y = mysql_fetch_array($q, MYSQL_ASSOC))
	  			$x[] = $y;
      }
			else
      {
				$x = $API->getPageMembers($page['gid'], "rand()", 6);
      }
      foreach( $x as $y )
      {
      	$profileURL = $API->getProfileURL($y['uid'], $y['username']);
        $occupation = quickQuery( "select occupation from users where uid='" . $y['uid'] . "'" );
      	?>
      	<div class="notification">
      		<div class="pic">
      			<a href="<?=$profileURL?>">
      				<img src="<?=$API->getThumbURL(1, 48, 48, $API->getUserPic($y['uid'], $y['pic']))?>" alt="" width="48" height="48"/>
      			</a>
      		</div>
      		<div class="text">
      			<a href="<?=$profileURL?>"><?=$y['name']?></a><br />
            <? if( $occupation != "" ) { echo $occupation; } else { echo "Member of " . $siteName; } ?>
      		</div>
      		<div class="clear"></div>
      	</div>
      	<?php
      }
		}
  }
  ?>
  <div style="clear:both;"></div>
  <?
	if ($user['cfriends'] < 10)
	{
		if ($user['uid'] == $API->uid)
    {
			showGetConnected();
    }
		elseif ($API->isLoggedIn())
			showSuggestFriends($user['uid']);
	}
	?>
</div>