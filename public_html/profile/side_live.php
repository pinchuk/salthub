<?php
/*
Displays the left side of both user profiles and Pages.  Calls many other files, based on the type of profile that we're looking at.

*/

    include_once "../inc/inc.php";


    if( empty( $page ) && (isset( $_POST['gid'] ) || isset( $_GET['gid'] ) ))
    {
      $user['uid'] = $API->uid;
      $gid = $_POST['gid'];
      if( empty( $gid ) ) $gid = $_GET['gid'];
      $page = $API->getPageInfo($gid);

      $user['cfriends'] = $API->getPageMembersCount($gid);

    	if (empty($page))
    		die("Page not found");

    	if (quickQuery("select count(*) from page_blocked where gid=$gid and uid={$API->uid}") == 1)
    	{
    		die("<script>alert('You have been banned from this page.');window.location.href = '/pages';</script>");
    	}
    }

    $loggedIn = $API->isLoggedIn();

		if ($site == "s")
		{
			if ($gid == 0)
      {
        if( $loggedIn )
        {
  				include "side_contactinfo.php";
    		  include "side_contactfor.php";
        	include "followme_full.php";
        }
        else
        {
          showSignUpModule($user['name']);
        }
      }
			else
			{

      if ($page['member'] == 0 && $loggedIn ) { ?>
		<div id="join-side">
			<div class="orangebutton" onclick="javascript:joinHTML = '<div class=\'bluebutton\' style=\'cursor: default;\'>Joined</div>'; <? if( isset( $user ) ) { ?>joinPage(<?=$gid?>, 'side', '<?=$API->pageToGtype( $gid ); ?>'); <? } else { ?> <? } ?>">
				<a href="javascript:void(0);">Connect With Page</a>
			</div>
		</div>
		<? }

    if( !$loggedIn ) {
      showSignUpModule($page['gname']);
    }
    ?>
		<div style="font-size: 8pt; line-height: 12pt; margin: 10px 0;">
			<div id="joinedlinks" style="<?=$page['member'] == 1 || $API->admin ? "" : "display: none;"?>">
				<a href="javascript:void(0);" onclick="javascript:loadShare('page', 1, {'gid':'<?=$gid?>', 'gname':'<?=addslashes($page['gname'])?>'} );">Add People to Page</a><br />
				<a href="javascript:void(0);" onclick="javascript:addPageMedia('V');">Add Videos to Page</a><br />
				<a href="javascript:void(0);" onclick="javascript:addPageMedia('P');">Add Photos to Page</a><br />
				<div style="<?=($page['admin'] == 1 || $API->admin ) ? "" : "display: none;"?>">
					<a href="javascript:void(0);" onclick="javascript:showSendMessage('<?=implode("', '", array('G' . $page['gid'], addslashes($page['gname']), $API->getThumbURL(0, 48, 48, $API->getPageImage($page['gid'] ) )))?>');">Message Members</a><br />
					<a href="javascript:void(0);" onclick="javascript:showPageMembers(<?=$page['gid']?>);">Edit Members</a><br />
					<a href="<?=$page['url']?>/editpage">Edit Page</a><br />
				</div>
				<a href="javascript:void(0);" onclick="javascript:leavePage(<?=$gid?>);">Leave this Page</a><br />
			</div>
      <? if( $loggedIn ) { ?>
			<a href="javascript:void(0);" onclick="javascript:showReport('G', <?=$gid?>);">Report this Page</a><br />
      <? } ?>
		</div>
<? if (!isset($_GET['edit'])) { ?>
		<table border="0" cellpadding="0" cellspacing="0" class="mediaactions">
			<tr>
    		<td style="padding-right: 20px; padding-top:5px; padding-left:10px;">
          <a type="button_count" href="javascript:void(0);" onclick="javascript: openPopupWindow( 'http://www.facebook.com/sharer/sharer.php?u=<? echo urlencode("http://" . SERVER_HOST . $API->getPageURL($page['gid'], $page['gname']) ); ?>', 'Share', 550, 320 );">
            <img src="/images/facebook_share.png" width="58" height="20" alt="" />
          </a>
        </td>

				<td><a href="http://twitter.com/share" class="twitter-share-button" data-text="<?=$page['gname']?>" data-count="horizontal" data-via="<?=SITE_VIA?>">Tweet</a></td>
			</tr>
		</table>
		<?php
}

  		include "../pages/side_contactinfo.php";

      if( $page['type'] != PAGE_TYPE_VESSEL && $page['type'] != PAGE_TYPE_PROFESSIONAL )
      {
				include "../pages/side_contactfor.php";
        include( "../pages/follow_page_full.php" );
      }

			if ($page['type'] == PAGE_TYPE_BUSINESS || $page['type'] == PAGE_TYPE_VESSEL)
				include "../pages/side_employees.php";
			elseif ($page['cat'] == CAT_SCHOOL )
				include "../pages/side_education.php";

		} //If this is a page


    if( (isset( $page ) || $API->userHasAccess($user['uid'], PRIVACY_CONNECTIONS ) ) )
    {
  		?>
  		<div class="subhead" style="margin-top: 10px;">
  			<div style="float: left;"><?=$gid == 0 ? "Connections" : "Members"?> <span class="number">(<?=$user['cfriends']?>)</span></div>
  			<?php if ($user['cfriends'] > 0 && $loggedIn ) { ?>
  			<div style="float: right; font-size: 8pt; font-weight: normal; padding: 2px 5px 0 0;"><a href="javascript:void(0);" onclick="javascript:<?=$gid == 0 ? "showFriendsPopup({$user['uid']})" : "showPageMembers($gid)"?>;">view all</a></div>
  			<?php } ?>
  			<div style="clear: both;"></div>
  		</div>
  		<?php
  		if ($user['cfriends'] > 0 )
  		{
  			$openDivs = 0;
  			$i = 0;

  			if ($gid == 0)
        {
          if($loggedIn )
    				$x = $API->getFriends($user['uid'], "rand()", 6);
        }
  			else
        {
  				$x = $API->getPageMembers($page['gid'], "rand()", 6);
        }
        foreach( $x as $y )
        {
          if( $y['uid'] == $user['uid'] ) continue;

        	$profileURL = $API->getProfileURL($y['uid'], $y['username']);
          $occupation = quickQuery( "select occupation from users where uid='" . $y['uid'] . "'" );
        	?>
        	<div class="notification">
        		<div class="pic">
              <? if( $loggedIn ) { ?>
        			<a href="<?=$profileURL?>">
              <? } else { ?>
        			<a href="javascript:void(0);" onclick="javascript:getStarted3(); return false;">
              <? } ?>
        				<img src="<?=$API->getThumbURL(1, 48, 48, $API->getUserPic($y['uid'], $y['pic']))?>" alt="" width="48" height="48"/>
        			</a>
        		</div>
        		<div class="text">
              <? if( $loggedIn ) { ?>
        			<a href="<?=$profileURL?>">
              <? } else { ?>
        			<a href="javascript:void(0);" onclick="javascript:getStarted3(); return false;">
              <? } ?>
              <?=$y['name']?></a><br />
              <? if( $occupation != "" ) { echo $occupation; } else { echo "Member of " . $siteName; } ?>
        		</div>
        		<div class="clear"></div>
        	</div>
        	<?php
        }
  		}
    }
    echo '<div style="clear: both; height: 10px;"></div>';

    if( $loggedIn )
    {
  		if ($user['cfriends'] < 10 && !$page )
  		{
  			if ($user['uid'] == $API->uid)
  				showGetConnected();
  			else
        {
          showSuggestFriends($user['uid']);
        }
  		}
      else
        showSuggestPage( $page['gid'] );
    }


    if( $gid == 0 ) {
      $q = sql_query( "select distinct gid from feed where uid='" . $user['uid'] . "' and (type='W' or type='G' or type='P' or type='V') and gid!=0 order by ts desc limit 3" );

      if( mysql_num_rows( $q ) > 0 )
      {
?>
		<div class="subhead" style="margin-top: 10px;">
			<div style="float: left;">Active Pages</div>
<? if( $user['uid'] == $API->uid ) { ?>
			<div style="float: right; font-size: 8pt; font-weight: normal; padding: 2px 5px 0 0;"><a href="/pages">view all</a></div>
<? } ?>
 			<div style="clear: both;"></div>
    </div>
<?
      while( $y = mysql_fetch_array( $q ) )
      {
        $gname = quickQuery( "select gname from pages where gid='" . $y['gid'] . "'" );
        if( $gname == '' ) continue;

        $pageURL = $API->getPageUrl( $y['gid'] );
?>

      	<div class="notification">
      		<div class="pic">
      			<a href="<?=$pageURL?>">
      				<img src="<?=$API->getThumbURL(1, 48, 48, $API->getPageImage($y['gid'] ) )?>" width="48" height="48" alt="" />
      			</a>
      		</div>
      		<div class="text">
      			<a href="<?=$pageURL?>"><?=$gname?></a><br />
      		</div>
      		<div class="clear"></div>
      	</div>
<?
      }

      }
    }

    echo '<div style="clear: both; height: 10px;"></div>';




		}




		if (empty($page['gtype']) && 1==0) //disable this for the time bing
		for ($j = 0; $j < 2; $j++)
		{
			$openDivs = 0;

      $area = PRIVACY_VIDEOS;
      if( $j == 1 ) $area = PRIVACY_PHOTOS;

      if( !$API->userHasAccess($user['uid'], $area ) ) continue;

			?>
			<div style="clear: both; padding-top: 10px;">
				<div class="subhead">
					<div style="float: left;"><?=$j == 0 ? "Video" : "Photo"?>s</div>
					<div style="float: right; font-size: 8pt; font-weight: normal; padding: 2px 5px 0 0;"><a href="<?=$gid == 0 ? $API->getProfileURL($user['uid'], $user['username']) : $API->getPageURL($page['gid'], $page['gname'])?>/<?=$j == 0 ? "video" : "photo"?>s">view all</a></div>
					<div style="clear: both;"></div>
				</div>
			</div>
			<?php

			$pageFilter = ($gid == 0 ? "" : "inner join page_media gm on gm.id=media.id and gm.type=@type and gid=$gid");

      $x = array();

			$i = 0;
			if ($j == 1)
      {
				$x[0] = mysql_query("select @type:='P'," . ($site == "s" ? "ptitle,pdescr," : "") . "media.id,title,hash from photos as media " . str_replace("@type", "'P'", $pageFilter) . " inner join albums on media.aid=albums.id where " . ( $gid > 0 ? "1=1 and" : $API->getPrivacyQuery()) . ($site == "s" ? " albType < 2" : "1=1") . ($gid == 0 ? " and media.uid={$user['uid']}" : "") . " order by media.created desc limit 6") or die(mysql_error());
        //echo "select @type:='P'," . ($site == "s" ? "ptitle,pdescr," : "") . "media.id,title,hash from photos as media " . str_replace("@type", "'P'", $pageFilter) . " inner join albums on media.aid=albums.id where " . ( $gid > 0 ? "1=1 and" : $API->getPrivacyQuery()) . ($site == "s" ? " albType < 2" : "1=1") . ($gid == 0 ? " and media.uid={$user['uid']}" : "") . " order by media.created desc limit 6";

        if( mysql_num_rows($x[0]) < 6 && $site != "m")
        {
          $word = "photo";
          $type = "P";
          $uid = $user['uid'];
          $gid = $page['gid'];
		      $select = "select @type:='$type',media.id,hash,ifnull(ptitle,title) as title,ifnull(pdescr,descr) as descr "; //set aid to -1 so it's not highlighted - we are showing single pictures anyway
        	$q = "from {$word}_tags left join contacts on contacts.cid={$word}_tags.cid and contacts.uid={$word}_tags.uid inner join {$word}s as media on media.id={$word}_tags.{$type}id " . ($type == "P" ? "inner join albums on media.aid=albums.id" : "") . " where " . ( $gid > 0 ? "1=1 and" : $API->getPrivacyQuery()) . " media.uid!=$uid and (({$word}_tags.uid=$uid and {$word}_tags.cid=0) or (eid=$uid))" . ($gid == 0 ? "" : " and media.id in (select id from page_media gm where gm.gid=$gid and gm.type='$type') limit 6");

				  $x[1] = mysql_query( $select . $q );
        }
      }
			else
      {
				$x[0] = mysql_query("select @type:='V',media.id,title,hash from videos as media " . str_replace("@type", "'V'", $pageFilter) . " where " . ( $gid > 0 ? "1=1 and" : $API->getPrivacyQuery()) . " ready=1 " . ($gid == 0 ? " and media.uid={$user['uid']}" : "") . " order by created desc limit 6") or die(mysql_error());

        if( mysql_num_rows($x[0]) < 6 && $site != "m")
        {
          $word = "video";
          $type = "V";
          $uid = $user['uid'];
          $gid = $page['gid'];
          $select = "select @type:='$type',media.id,hash,title,descr ";
        	$q = "from {$word}_tags left join contacts on contacts.cid={$word}_tags.cid and contacts.uid={$word}_tags.uid inner join {$word}s as media on media.id={$word}_tags.{$type}id " . ($type == "P" ? "inner join albums on media.aid=albums.id" : "") . " where " . ( $gid > 0 ? "1=1 and" : $API->getPrivacyQuery()) . " media.uid!=$uid and (({$word}_tags.uid=$uid and {$word}_tags.cid=0) or (eid=$uid))" . ($gid == 0 ? "" : " and media.id in (select id from page_media gm where gm.gid=$gid and gm.type='$type') limit 6");

				  $x[1] = mysql_query( $select . $q );
        }
      }

      $rows = mysql_num_rows($x[0]);
      if( sizeof( $x ) > 1 )
        $rows += mysql_num_rows($x[1]);

			if ( $rows == 0)
			{
        //echo "select @type:='P'," . ($site == "s" ? "ptitle,pdescr," : "") . "media.id,title,hash from photos as media " . str_replace("@type", "'P'", $pageFilter) . " inner join albums on media.aid=albums.id where " . $API->getPrivacyQuery() . ($site == "s" ? " albType < 2" : "1=1") . ($gid == 0 ? " and media.uid={$user['uid']}" : "") . " order by media.created desc limit 6";

				echo '<div class="nomedia">This ' . ($gid == 0 ? "user" : "page") . ' doesn\'t have<br/>any ' . ($j == 0 ? "video" : "photo") . 's.';
				if ($page['member'] == 1)
				echo $gid == 0 ? '' : '&nbsp; <a href="javascript:void(0);" onclick="javascript:addPageMedia(\'' . ($j == 0 ? 'V' : 'P') . '\');">Add some</a>.';
				echo '</div>';
			}
			else
        for( $k = 0; $k < sizeof( $x ) && $k < 2 & $i < 6; $k++ )
  				while ($media = mysql_fetch_array($x[$k], MYSQL_ASSOC))
  				{
            if( $i > 6 ) break;
  					if ($i == 0)
  					{
  						echo '<div style="clear: both;">';
  						$openDivs++;
  					}
  					$mediaURL = $API->getMediaURL($j == 0 ? "V" : "P", $media['id'], $media['ptitle'] ? $media['ptitle'] : $media['title']);
  					?>
  					<div style="width: <?=$i == 2 ? 56 : 61?>px; overflow:visible;" class="logmedia">
  						<a href="<?=$mediaURL?>">
  							<img src="<?=$API->getThumbURL(0, 55, 75, $API->getMediaThumb($j == 0 ? "V" : "P", $media['id'], $media['hash']))?>" />
  						</a>
              <? if( $j == 0 ) { ?>
  						<br /><a href="<?=$mediaURL?>"><?=$media['ptitle'] ? $media['ptitle'] : $media['title']?></a>
              <? } ?>
  					</div>
  					<?php
  					if ($i == 2)
  					{
  						echo '</div>';
  						$i = -1;
  						$openDivs--;
  					}

  					$i++;
  				}

			for ($i = 0; $i < $openDivs; $i++)
				echo '</div>';
      $openDivs = 0;
			?>
			<div style="clear: both; height: 10px;"></div>
			<?php
		}


		?>