<div class="subhead" style="margin-top: 10px;">Upload</div>

<?php
showNoAppRequired();

if ($action == "about")
{
	if ($user['uid'] == $API->uid)
		showGetConnected();
	else
		showSuggestFriends($user['uid']);
}
elseif ($user['uid'] == $API->uid)
	showCompleteProfileInformation();
?>