<?php

include "../inc/inc.php";

$info = json_decode($_POST['json_info'], true);

$dob = strtotime($info['dob']);

if (empty($dob))
	$info['dob'] = '0000-00-00';

$update = array();

foreach (array("dob", "gender", "relationship", "showyear", "citizenship") as $k)
	$update[] = "$k='" . addslashes(htmlentities($info[$k])) . "'";

mysql_query("update users set " . implode(",", $update) . " where uid=" . $API->uid);

$API->setToDoItem( TODO_PROFILE );
?>