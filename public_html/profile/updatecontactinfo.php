<?php

include "../inc/inc.php";

$info = json_decode($_POST['json_info'], true);

$update = array();

if( $info['signup'] == 1 )
  foreach (array("occupation", "sector") as $k)
	  $update[] = "$k='" . addslashes(htmlentities($info[$k])) . "'";
else
{
  switch( $info['updating'] )
  {
    case "company":
      foreach (array("sector", "occupation", "company" ) as $k)
	      $update[] = "$k='" . addslashes(htmlentities($info[$k])) . "'";

      $API->feedAdd("PR2", 0 );
    break;

    case "occupation":
      foreach (array("public_phone", "cell", "fax", "public_email") as $k)
	      $update[] = "$k='" . addslashes(htmlentities($info[$k])) . "'";
    break;
  }
}

mysql_query("update users set " . implode(",", $update) . " where uid=" . $API->uid);

$API->setToDoItem( TODO_CONTACT_INFO );
?>