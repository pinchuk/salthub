<?
global $API;

if (isset($API))
{
	$isAjax = false;
  getToDoList();
}
else
{
	include_once "../inc/inc.php";
	include_once "../inc/mod_comments.php";
	$isAjax = true;
  $isEmail = true;

	foreach ($_GET as $k => $v)
		$params[$k] = intval($v);

	ob_start();
	$return = getToDoList($params);
	$return['html'] = ob_get_contents();
	ob_end_clean();

//	echo json_encode($return);
  echo $return['html'];
}


function getToDoList($params = array() )
{
	global $API, $siteName, $gid, $isEmail;

  $host = "http://" . $_SERVER['HTTP_HOST'];

//if( $isEmail )
{
?>

<STYLE type="text/css">
<!--

.todo_list
{
  font-size: 10pt;
  color: #326798;
  margin-top: 3px;
}

.todo_list .divider
{
  clear:both;
  padding-top:5px;
  padding-bottom:5px;

}

.todo_list .divider hr
{
  width: 95%;
}

.todo_list .section
{
  margin-top:-10px;
  width:50%;
}

.todo_list .section .title
{
  color:#000;
}

.todo_list .section .todocontent
{
  float:left;
  width: 210px;
  padding:0px;
}

.todo_list .section .todocontent div
{
  font-size:9pt;
  padding-top: 3px;
  padding-bottom:2px;
}

.todo_list .section .content_text
{
  margin-top:-5px;
  font-size:8pt;
  color:#555555;
  float:right;
  width: 200px;
}

.todo_list .section .content_text .sites div
{
  padding-top: 2px;
  padding-bottom:2px;
}

-->
</style>
<?
}
?>
<table border="0" style="width: 431px; padding: 10px; background-color:#EDEFF4; border: 1px solid #c0c0c0;" class="todo_list">
<tr>
  <td class="section" width="50%">
    <div class="title">
      Connect with twitter and facebook
    </div>

    <div class="todocontent">
<?
$fb = quickQuery( "select fbusername from users where uid='" . $API->uid . "'" );
$tw = quickQuery( "select twusername from users where uid='" . $API->uid . "'" );
?>
      <div>
        <? if ($fb != '' ) { ?>
          <img src="<?=$host ?>/images/check.png" width="16" height="16" alt="" />
        <? } else { ?>
          <img src="<?=$host ?>/images/bullet.png" width="16" height="16" alt="" />
        <? } ?>
        <img src="<?=$host ?>/images/facebook.png" alt="" />&nbsp; <?=($fb != '' ) ? '<a href="http://www.facebook.com/" target="_blank">view' : '<a href="/settings"><span style="color: #339900;">enable</span>'?></a>
      </div>

      <div>
        <? if ($tw != '' ) { ?>
          <img src="<?=$host ?>/images/check.png" width="16" height="16" alt="" />
        <? } else { ?>
          <img src="<?=$host ?>/images/bullet.png" width="16" height="16" alt="" />
        <? } ?>
        <img src="<?=$host ?>/images/twitter.png" alt="" />&nbsp; <?=($tw != '' ) ? '<a href="http://www.twitter.com/" target="_blank">view' : '<a href="javascript:void(0);" onclick="javascript:showEnableAcct(1);"><span style="color: #339900;">enable</span>'?></a>
      </div>
    </div>
  </td>

  <td class="section" width="50%">
    <div class="content_text">

    <table width="100%" border="0">
    <tr>
      <td width="52">
        <img src="<?=$host ?>/images/fb_twit_sm.png" width="51" height="52" alt="" style="float:left; margin-right:5px;"/>
      </td>
      <td>
        Enable your social accounts. Post once on SaltHub and connect to all.
      </td>
    </tr>
    </table>

    </div>
  </td>
</tr>

<tr>
  <td colspan="2"><div class="divider"><hr /></div></td>
</tr>

<tr>
  <td class="section" width="50%">
    <div class="title">
      Enhance your profile
    </div>

    <div class="todocontent">
      <div>
        <? if (!$API->getToDoItem(TODO_PROFILE) ) { ?>
          <img src="<?=$host ?>/images/bullet.png" width="16" height="16" alt="" />
        <? } else { ?>
          <img src="<?=$host ?>/images/check.png" width="16" height="16" alt="" />
        <? } ?>
        <a href="<? echo $API->getProfileURL(); ?>/about">Complete your profile info</a>
      </div>

      <div>
        <? if (!$API->getToDoItem(TODO_CONTACT_FOR) ) { ?>
          <img src="<?=$host ?>/images/bullet.png" width="16" height="16" alt="" />
        <? } else { ?>
          <img src="<?=$host ?>/images/check.png" width="16" height="16" alt="" />
        <? } ?>
        <a href="<? echo $API->getProfileURL(); ?>/contactfor">Add what you can be contacted for</a>
      </div>

      <div>
        <? if (!$API->getToDoItem(TODO_CONTACT_INFO) ) { ?>
          <img src="<?=$host ?>/images/bullet.png" width="16" height="16" alt="" />
        <? } else { ?>
          <img src="<?=$host ?>/images/check.png" width="16" height="16" alt="" />
        <? } ?>
        <a href="<? echo $API->getProfileURL(); ?>/contactinfo">Update your contact info</a>
      </div>
    </div>

  </td>

  <td class="section" width="50%">
    <div class="content_text">
    <table width="100%" border="0">
    <tr>
      <td width="41">
        <img src="<?=$host ?>/images/profile_large.png" width="40" height="50" alt="" style="float:left; margin-right:5px;"/>
      </td>
      <td>
      (recommended)<br />This is the most important step you can take on <? echo $siteName ?>.
      </td>
    </tr>
    </table>
    </div>
  </td>
</tr>

<tr>
  <td colspan="2"><div class="divider"><hr /></div></td>
</tr>

<tr>
  <td class="section" width="50%">
    <div class="title">
      Get connected
    </div>

    <div class="todocontent">
      <div>
        <? if (!$API->getToDoItem(TODO_INVITE_CONTACTS) ) { ?>
          <img src="<?=$host ?>/images/bullet.png" width="16" height="16" alt="" />
        <? } else { ?>
          <img src="<?=$host ?>/images/check.png" width="16" height="16" alt="" />
        <? } ?>
        <a href="<?=$host ?>/invite.php">Invite Contacts</a>
      </div>

      <div>
        <? if (!$API->getToDoItem(TODO_FIND_CONTACTS) ) { ?>
          <img src="<?=$host ?>/images/bullet.png" width="16" height="16" alt="" />
        <? } else { ?>
          <img src="<?=$host ?>/images/check.png" width="16" height="16" alt="" />
        <? } ?>
        <a href="/findpeople.php">Find contacts already on <? echo $siteName ?></a>
      </div>
    </div>
  </td>

  <td class="section" width="50%">
    <div class="content_text">
    <table width="100%" border="0">
    <tr>
      <td width="50%">
        <div style="float:left; color:#326798;" class="sites">
        	<div><img src="<?=$host ?>/images/facebook.png" alt="" /> Facebook</div>
        	<div><img src="<?=$host ?>/images/twitter.png" alt="" /> Twitter</div>
        	<div><img src="<?=$host ?>/images/hotmail.png" alt="" /> Hotmail</div>
        </div>
      </td>
      <td width="50%">
        <div style="float:left; color:#326798;" class="sites">
    	    <div><img src="<?=$host ?>/images/msn.png" alt="" /> MSN</div>
        	<div><img src="<?=$host ?>/images/yahoo.png" alt="" /> Yahoo</div>
        	<div><img src="<?=$host ?>/images/gmail.png" alt="" /> Gmail</div>
        </div>
      </td>
    </tr>
    </table>

    </div>
  </td>
</tr>

<tr>
  <td colspan="2"><div class="divider"><hr /></div></td>
</tr>

<tr>
  <td class="section" width="50%">
    <div class="title">
      Connect with pages
    </div>

    <div class="todocontent">
      <div>
        <? if (!$API->getToDoItem(TODO_COMPANY_PAGE) ) { ?>
          <img src="<?=$host ?>/images/bullet.png" width="16" height="16" alt="" />
        <? } else { ?>
          <img src="<?=$host ?>/images/check.png" width="16" height="16" alt="" />
        <? } ?>
        <a href="/pages/claim_company.php">Claim or add your company page</a>
      </div>

      <div>
        <? if (!$API->getToDoItem(TODO_CREATE_GROUP) ) { ?>
          <img src="<?=$host ?>/images/bullet.png" width="16" height="16" alt="" />
        <? } else { ?>
          <img src="<?=$host ?>/images/check.png" width="16" height="16" alt="" />
        <? } ?>
        <a href="/pages/page_create.php">Create your own page</a>
      </div>

      <div>
        <? if (!$API->getToDoItem(TODO_EXPLORE_GROUPS) ) { ?>
          <img src="<?=$host ?>/images/bullet.png" width="16" height="16" alt="" />
        <? } else { ?>
          <img src="<?=$host ?>/images/check.png" width="16" height="16" alt="" />
        <? } ?>
        <a href="/pages">Explore pages</a>
      </div>
    </div>
  </td>

  <td class="section" width="50%">
    <div class="content_text">
      Vessels, companies, occupations and all sorts of items people have in common are sorted into pages.
    </div>
  </td>
</tr>

<tr>
  <td colspan="2"><div class="divider"><hr /></div></td>
</tr>

<tr>
  <td class="section" width="50%">
    <span class="title">Use <? echo $siteName ?> with your phone</span>

    <span class="todocontent">
      <div>
        <? if (!$API->getToDoItem(TODO_CONFIG_MOBILE_UPLOAD) ) { ?>
          <img src="<?=$host ?>/images/bullet.png" width="16" height="16" alt="" />
        <? } else { ?>
          <img src="<?=$host ?>/images/check.png" width="16" height="16" alt="" />
        <? } ?>
      <a href="/settings">Configure mobile upload</a>
      </div>
    </span>
  </td>


  <td class="section" width="50%">
    <div style="float:left;" class="content_text">
    <table width="100%" border="0">
    <tr>
      <td width="65"><img src="<?=$host ?>/images/iphone.png" width="57" height="64" alt="" style="float:left;"/></td>
      <td><b>No app required!</b><br />Share media on the go from your phone in real time, using e-mail!</td>
    </tr>
    </table>
    </div>
  </td>
</tr>

</table>
<?
}
?>