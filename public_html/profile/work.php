<div id="work" onmouseover="javascript:document.getElementById('workeditlink').style.visibility = 'visible';" onmouseout="javascript:document.getElementById('workeditlink').style.visibility = 'hidden';">
	<?php
	include "workinfo.php";
	?>
</div>

<?php if ($user['uid'] == $API->uid) { ?>

<div id="workedit" style="display: none;">

	<div style="font-size: 8pt; padding-bottom: 15px;">
		Complete your work experience to connect with past and present co-workers.&nbsp; By doing so, you will also be able to exchange information with others who share your occupation.
	</div>

	<?php
	$wids = array();
	foreach ($works as $work)
	{
		showEditWork($work);
		$wids[] = $work['wid'];
	}
	?>

	<div id="newworks"></div>

	<div style="clear: both; padding-left: 25px; width:504px; text-align:center;">
    <span <? if( sizeof( $wids ) == 0 ) echo 'style="display:none;"'; ?> id="work_buttons">
      <input type="button" class="button" value="Save" onclick="javascript:saveWork();" /> &nbsp;
  		<input type="button" class="button" value="Cancel" onclick="javascript:toggleEdit('work');" /> &nbsp;
    </span>
    <input type="button" class="button" value="Add Job" onclick="javascript:selectWorkType();" Xonclick="javascript:addWork(0);" />
	</div>

</div>


<script language="javascript" type="text/javascript">
<!--
var works = <?=json_encode($works)?>;
var wids = ['<?=implode("', '", $wids)?>'];
var employers = [];
var occupations = [];

// autocomplete
for (var i in works)
{
//	employers[i] = new actb(document.getElementById("employer-" + i), "employer");
//	employers[i].actb_val = works.employer;
	occupations[i] = new actb(document.getElementById("occupation-" + i), "occupation");
	occupations[i].actb_val = works.occupation;
}
//-->
</script>
<? } ?>

<div style="clear: both; height: 10px;"></div>