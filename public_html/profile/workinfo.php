<?php

if (empty($user['uid']))
	$user['uid'] = $API->uid;

$x = mysql_query(
	"select * from
		(select wid,unix_timestamp(start) as start,
			unix_timestamp(stop) as stop,
			if(stop=0,1,0) as present,
			w.descr,
			employer,
			occupation,
			w.www,g1.gname as employer_name,
			g2.gname as occupation_name 
		from work w
			left join pages g1 on g1.gid=employer
			left join pages g2 on g2.gid=occupation
		where w.uid={$user['uid']} AND g1.gid > 0)
	as tmp order by present desc,start desc"
	);

$works = array();
while ($y = mysql_fetch_array($x, MYSQL_ASSOC))
	$works[$y['wid']] = $y;

$i = 0;
$present = true;

/*
if (count($works) == 0)
{
	$i = 1;
	$works['X'] = array("dummy" => 1, "wid" => "X"); //at least make the divs and stuff
}
*/

echo '<div class="edit" id="workeditlink" style="padding-right: 5px;">';
if ($user['uid'] == $API->uid) echo '<a href="javascript:void(0);" onclick="javascript:toggleEdit(\'work\');">edit</a>';
echo '</div>';

foreach ($works as $work)
{
  if ($i++ == 0 && $work['present'] == "1")
  	echo '<div class="smtitle2" style="float: left;">Current Employment</div>';
  elseif ($work['present'] == "0" && $present)
  {
  	$present = false;
  	echo '<div class="smtitle2" style="clear: left; float: left;' . ($i == 1 ? '' : ' padding-top: 15px;') . '">Past Employment</div>';
  }

  $data = queryArray( "select type, pid, gname from pages where gid='" . $work['employer'] . "'" );
  $type = $data['type'];

  if( $type == PAGE_TYPE_VESSEL )
    $exnames = quickQuery( "select exnames from boats where gid='" . $work['employer'] . "'" );

  $pageUrl = $API->getPageURL($work['employer'], $work['employer_name']);
  ?>
  <div class="work">
    <div class="title">
      <a href="<?=$API->getPageURL($work['occupation'], $work['occupation_name'])?>"><?=$work['occupation_name']?></a>
      <span style="font-weight:300;"><?= ($type==PAGE_TYPE_VESSEL?" on ":" at "); ?></span>
      <a href="<?=$pageUrl;?>"><?=$data['gname']?></a>
      <? if( $exnames != "" ) echo ' <span style="font-size:8pt; color:#555; font-weight:300;">(ex. ' . $exnames . ')</span>'; ?>
      <span style="font-size:8pt; color:#555; font-weight:300;"><?= ($type==PAGE_TYPE_VESSEL?" (vessel) ":" (company) "); ?></span>
    </div>
    <div class="date">
      <?=date("F Y", $work['start'])?> - <?=$work['present'] == "1" ? "Present" : date("F Y", $work['stop']);

      $timeText = "";
      if( $work['present'] == "1" )
        $timeText = timeToHumanTime2( time() - $work['start'] );
      else
        $timeText = timeToHumanTime2( $work['stop'] - $work['start'] );
      echo ' (' . $timeText . ')';
      ?>
    </div>

    <div class="preview" style="text-align:center;">
      <?
        echo '<a href="' . $pageUrl . '"><img width="140" src="' . $API->getThumbURL( false, 178, 95, $API->getPageImage( $work['employer'] ) ) . '"/></a>';
      ?>
    </div>

    <div class="descr">
      <?=nl2br($work['descr']);?>

<?
if( $work['www'] ) {
?>
				<br /><br /><?=findLinks($work['www'])?>
<? } ?>
    </div>


    <div style="clear:both;"></div>
  </div>

	<?
}

if( $API->uid == $user['uid'] && $i == 0 )
	echo '<div class="nodata">Complete your work experience to connect with past and present co-workers.&nbsp; By doing so, you will also be able to exchange information with others who share your occupation.</div>';
?>