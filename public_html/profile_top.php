<?php
if ($gid == 0)
	$userStatus = quickQuery("select post from wallposts where uid=" . $user['uid'] . " and uid_to=" . $user['uid'] . " order by sid desc");



if( $user['uid'] == $API->uid )
{
  if( empty( $user['company']) ) $user['company'] = "You own or work at?";
  if( empty( $user['location']) ) $user['location'] = "Plant Earth?";
  if( empty( $user['occupation']) ) $user['occupation'] = "What do you do?";
}
else
{
  if( empty( $user['company']) ) $user['company'] = "I'll get to this later";
  if( empty( $user['location']) ) $user['location'] = "Plant Earth";
  if( empty( $user['occupation']) ) $user['occupation'] = "Member of " . $siteName;
}


?>

<div class="profiletop <?=$gid == 0 ? "" : "profiletopgroup"?>" id="bluegrad">
	<div class="descr" id="profiledescr" onmouseover="javascript:showIfExists('edit-about', true);" onmouseout="javascript:showIfExists('edit-about', false);" style="<?=$action == "about" ? "height: auto;" : ""?>">
		<?php
		echo '<span id="txtprofiledescr">';
		
		if ($gid == 0)
		{
			$txtAbout = quickQuery("select txt from aboutme where uid=" . $user['uid']);
			if (trim($txtAbout) == "" && $uid == $API->uid)
				echo 'Add something about yourself or do some bragging <a href="javascript:void(0);" onclick="javascript:toggleUserAboutEdit();">here</a>. Examples: have 3 kids, own a fishing boat, surf and work in the yacht charter biz.';
			
			echo nl2br($txtAbout); //str_replace("\n", "<br />", $txtAbout);
		}
		else
			echo nl2br($group['descr']); //str_replace("\n", "<br />", $group['descr']);
		
		echo '</span>';
		
		if ($action == "profile" || $gid > 0)
		{
			?>
			<div class="more" id="descrmore" onclick="javascript:location = '<?=$gid == 0 ? $API->getProfileURL($user['uid']) : $group['url']?>/about';"><span>&nbsp; &#0133; </span>more&nbsp; <img src="/images/arrow_down.png" alt="" /></div>
			<?php
		}
		elseif ($action == "about")
		{
		?>
			<div style="clear: both; height: 5px;"></div>
			<?php if ($user['uid'] == $API->uid) { ?>
			<div style="float: left; font-size: 8pt; display: none;" id="edit-about"><a href="javascript:void(0);" onclick="javascript:toggleUserAboutEdit();">edit</a></div>
			<?php } ?>
			<div style="float: right; font-size: 8pt;"><a href="<?=$API->getProfileURL($user['uid'])?>">less&nbsp; <img src="/images/arrow_up.png" alt="" /></a></div>
			<div style="clear: both;"></div>
			<?php
		}
		?>
	</div>
	<?php if ($user['uid'] == $API->uid && ($action == "about" || $action == "profile")) { ?>
	<div id="profiledescredit" class="descr" style="display: none; height: auto;">
		<textarea id="txtabout" style="width: 535px; height: 100px;"><?=$txtAbout?></textarea>
		<div style="text-align: center; padding-top: 5px;"><input type="button" onclick="javascript:saveUserAbout();" class="button" value="Save Changes" /></div>
	</div>
	<?php } ?>
	<?php if ($gid == 0) { ?>
	<div class="summaryinfo" style="width: 171px;"<?php if ($API->uid == $user['uid']) { ?> onmouseout="javascript:logMouseOver('location', false);" onmouseover="javascript:logMouseOver('location', true);"<?php } ?>>
		<div class="left">From:</div>
		<div class="right" id="user-location-edit"><a href="javascript:void(0);" onclick="javascript:updateUserInfo('location');">edit</a></div>
		<div class="value" id="user-location"><?php
		if ($user['loc_city'] > -1)
			echo ucwords(strtolower(quickQuery("select city from loc_city where id=" . $user['loc_city']))) . ", ";
		if ($user['loc_state'] > -1)
			echo ucwords(strtolower(quickQuery("select region from loc_state where id=" . $user['loc_state']))) . ", ";
		if ($user['loc_country'] > -1)
    {
      $country = quickQuery("select country from loc_country where id=" . $user['loc_country']);
      if( strlen( $country ) > 3 )
        $country = ucwords(strtolower($country));
			echo $country;
    }

    if( $user['loc_country'] == -1 && $user['loc_city'] == -1 && $user['loc_state'] == -1 )
    {
      echo "Planet Earth";
      if( $user['uid'] == $API->uid )
        echo "?";
    }
		?></div>
	</div>
<!--
  <div class="summaryinfo" style="width: 147px;"<?php if ($API->uid == $user['uid']) { ?> onmouseout="javascript:logMouseOver('networks', false);" onmouseover="javascript:logMouseOver('networks', true);"<?php } ?>>
		<div class="left">Networks:</div>
		<div class="right" id="user-networks-edit"><a href="javascript:void(0);" onclick="javascript:updateUserInfo('networks');">edit</a></div>
		<div class="value" id="user-networks"></div>
	</div>
-->
	<div class="summaryinfo" style="width: 196px;"<?php if ($API->uid == $user['uid']) { ?> onmouseout="javascript:logMouseOver('company', false);" onmouseover="javascript:logMouseOver('company', true);"<?php } ?>>
		<div class="left">Company/Vessel:</div>
		<div class="right" id="user-company-edit"><a href="javascript:void(0);" onclick="javascript:updateUserInfo('company');">edit</a></div>
		<div class="value" id="user-company"><?=$user['company']?></div>
	</div> 
	<div class="summaryinfo" style="width: 144px; border-right: 0; margin-right: 0;"<?php if ($API->uid == $user['uid']) { ?> onmouseout="javascript:logMouseOver('occupation', false);" onmouseover="javascript:logMouseOver('occupation', true);"<?php } ?>>
		<div class="left">I'm a:</div>
		<div class="right" id="user-occupation-edit"><a href="javascript:void(0);" onclick="javascript:updateUserInfo('occupation');">edit</a></div>
		<div class="value" id="user-occupation"><?=$user['occupation']?></div>
	</div> 
	<?php } else { ?>
	<div class="summaryinfo" style="width: 175px;"> 
		<div class="left">Category:</div>
		<div class="value"><?=$group['catname']?></div>
	</div>
	<div class="summaryinfo" style="width: 175px;"> 
		<div class="left">Accessible by:</div>
		<div class="value"><?=$group['privacy'] == PRIVACY_EVERYONE ? "The world" : "Only members"?></div>
	</div> 
	<div class="summaryinfo" style="width: 175px; border-right: 0; margin-right: 0;">
		<div class="left">Created by:</div>

		<div class="value" style="color: #555;">
	  		<?php
  			if ( empty($group['uid']) || $group['uid'] == "" ) {
          $gid2 = quickQuery( "select gid from groups where gname='$siteName'" );
          echo '<a href="' . $API->getGroupURL($gid2) . '">' . $siteName . '</a>';
         } else { ?>
          <a href="<?=$API->getProfileURL($group['uid'])?>"><?=$group['name']?></a><?=$group['uid'] == $API->uid ? '' : ' &nbsp;|&nbsp; ' . friendLink($group['uid'])?>
        <? } ?>
    </div>
	</div>
	<?php } ?>
	<div style="clear: both;"></div>
</div>

<script language="javascript" type="text/javascript">
<!--
//check height of profile status
e = document.getElementById("profiledescr");

if (action == "about")
	showBlueGrad();

if (e)
{
	h = getHeight(e);

	e = document.getElementById("descrmore");
	if (h > 50 && e)
		e.style.display = "inline";
}
//-->
</script>