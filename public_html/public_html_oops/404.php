<?php
/*
Displayed when a 404 (page not found) error occurs anywhere on the web server.
*/

$ext = substr(strtolower($_SERVER['REQUEST_URI']), -4);
if ($ext == ".jpg" || $ext == ".png")
{
	header("Location: /images/noimage.jpg");
	die();
}

if (file_exists(substr($_SERVER['REQUEST_URI'], 1) . ".php"))
{
	header("Location: " . substr($_SERVER['REQUEST_URI'], 1) . ".php");
	die();
}

$noLogin = true;

include "header.php";


if( isset( $_GET['inactive'] ) ) { echo 'Sorry, this user is inactive. <a href="javascript:void(0);" onclick="javascript:history.go(-1);">Back</a>'; } else {
?>

Page not found.

<? } include "footer.php"; ?>