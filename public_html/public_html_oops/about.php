<?php
/*
The "About Salthub" page.

This page doesn't do anything interesting besides display text.
*/

include_once "inc/inc.php";
include_once "inc/recaptchalib.php";

$title = "About | $siteName";

$page_header = "About $siteName";
  $sub_header = $siteName . ' is the leading social network for professionals and businesses who make a living on and around the water.';

$scripts[] = "/index.js";
$noheader = true;
include "signup/newheader.php";
include "header_gradient.php";

?>

<div class="graygradient">
	<div class="headcontainer">
		<div class="entryslide">
			<?=$htmlSignIn?>
			<div class="slidecontent">
				<span class="title">About <?=$siteName?></span>

				<div class="text">
A funny thing happens when people love what they do &mdash; their enthusiasm becomes contagious and spreads from one person to the next, then to the next and the next.  That's exactly the kind of energy found at <?=$siteName?>.

<p>

It also helps that we are located in Fort Lauderdale, Fl. Fort Lauderdale is  The "Yachting Capital of the World", one of the busiest Cruise Ports in the world and one of the top 4 Cargo Ports in the USA. Home to 140,000 marine related jobs, 42,000 boats and approximately 100 marinas and boatyards. Not to mention the world's largest "in-water" boat show.
				</div>
				
			</div>
			
			<img src="/images/saltmap.jpg" class="bigimage" alt="" />
		</div>
	</div>
</div>

<div style="padding: 10px 5px 5px 5px; font-family: arial; font-size: 11pt; width:1050px; margin-left: auto; margin-right:auto;">

  <div style="clear:left; float:left; width:101px;">
    <!--<img src="images/salt_badge100.png" width="100" height="100" alt="" style="float: left; padding: 4px 10px 0 0;"/>-->
    <a href="/page/15528-SaltHub">
    <img  src="images/about_pics.png" width="101" height="202" alt="" style="margin-top:10px;"/>
    </a>
  </div>

	<div style="float: left; width: 605px; color: #555; margin-left:10px;">
	
	
	
	
		<div class="bigtext" style="color: #555;">About <a href="/page/15528-SaltHub"><span style="color: #326798;"><?=$siteName?>.<!--<span style="color: #ff8040;">.</span>-->com</span></a></div>

 SaltHub is a professional network for

 individuals and businesses that maintain an identity in the maritime

 industries. It is a dynamic and collaborative utility consisting of

 many feature sets and tools that have been designed to bring like

 minded professionals together, while allowing its users to freely move

 and navigate the site across 9 maritime industries.

 

 

 

 

<p /> <div class="bigtext" style="color: #555;">Maritime Industries</div>

 

 The industries include Yachting, Commercial Shipping, Oil & Gas

 Drilling, Commercial Fishing, Military & Regulatory, Recreational

 Fishing & Boating, Port & Marina Management and Cruise Shipping for

 Professionals and Businesses.

 

<p /> <div class="bigtext" style="color: #555;">Features and Tools</div>

 SaltHub allows its users to drill down and find information, Vessels,

 Businesses and Professionals specific to the industry they work in, or

 users may cross over and explore + interact with Businesses and

 Professionals in parallel maritime industries. This is done through

 profiles, professional pages, our maritime directory (5,000+

 companies), job postings, the employee directory, vessel directory

 (100,000+ vessels), our advertising platform, photos, videos and more.

 

<p /> <div class="bigtext" style="color: #555;">Integration</div>

 The site has been fully integrated with popular networks such as

 Facebook and Twitter. It also allows users to add RSS feeds to their

 pages, making it easy to spontaneously and continually connect with

 decision makers and consumers across other popular websites, blogs and news resources.

 

<p /> <div class="bigtext" style="color: #555;">Mission</div>

 SaltHub's mission is to provide Professional Mariners and Maritime

 Businesses, the power to Share And Learn Together, through a central

 Hub of connections. Allowing them to openly communicate in one central

 location, while providing a platform to be heard and discovered.

 


	<p />	<div class="bigtext" style="color: #555;">Contact us</div>
		Copyright holders that would like to report copyright-infringing content published on <?=$siteName?> can follow the DMCA complaint procedure located in our <a href="/tos.php">Terms of Service</a>.&nbsp; The rest of you can find us at:&nbsp; 1323 SE 17th St., Suite 690, Ft. Lauderdale, FL 33316

		<p />

		<div style="text-align: center;">
			&mdash; or &mdash;
		</div>

		<div style="text-align: center; clear: both; padding: 15px 0;">
			<script language="javascript" type="text/javascript">
			<!--
			var emails = ["feedback", "advertising", "business_dev"];
			for (var e in emails)
			{
				document.write('<a href="javascript:void(0);" onclick="javascript: openSendMessagePopup(\'\',\'\',\'SaltHub\',1187301,0,0,0 );">' + emails[e] + '@<?=$siteName?>.com</a>');
				if (e < emails.length - 1)
					document.write('&nbsp; <img src="/images/bullet.gif" style="padding-bottom: 2px;" alt="" /> &nbsp;');
			}
			//-->
			</script>
		</div>
		
		<p />
	</div>
	<div style="float: right; width: 170px;">
		<div class="bigtext" style="color: #555;">Other</div>
		<ul style="line-height: 25px; font-family: Arial; font-size: 10pt; padding-left: 15px; margin: 0;">
			<li><a href="http://twitter.com/salt_hub" target="_new">follow us on twitter</a></li>
			<li><a href="http://www.facebook.com/pages/SaltHub/196023203785837" target="_new">follow us on facebook</a></li>
<!--
      <li><a href="javascript:void(0);" onclick="window.open('/howitworks.php', 'HowItWorks','location=0,status=0,scrollbars=0,width=500,height=670');">how it works</a></li>
			<li><a href="javascript:void(0);" onclick="window.open('/whyitworks.php', 'WhyItWorks','location=0,status=0,scrollbars=0,width=500,height=450');">why you should join</a></li>
-->
			<li><a href="javascript:void(0);" onclick="window.open('/email.php?1', 'Email','location=0,status=0,scrollbars=1,width=580,height=600');">on the phone</a></li>
			<li><a href="javascript:void(0);" onclick="window.open('/tos.php?1', 'Terms of use','location=0,status=0,scrollbars=1,width=580,height=600');">terms of use</a></li>
			<li><a href="javascript:void(0);" onclick="window.open('/privacy.php?1', 'Privacy','location=0,status=0,scrollbars=1,width=580,height=600');">privacy</a></li>
		</ul>
	</div>

</div>

<div style="clear: both;"></div>

<?php include "signup/newfooter.php"; ?>
