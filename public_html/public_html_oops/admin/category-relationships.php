<?php

include( "../inc/inc.php" );

$q = mysql_query( "select * from categories where industry=" . PAGE_TYPE_BUSINESS );
$businesses = Array();
while( $r = mysql_fetch_array( $q ) )
  $businesses[] = $r['cat'];

$columns = Array();
$q = mysql_query( "select * from categories where industry=" . PAGE_TYPE_BUSINESS );
while( $r = mysql_fetch_array( $q ) )
  $columns[] = Array( $r['catname'], $r['cat'] );


if( isset( $_POST['sav'] ) )
{
  $child = intval($_POST['child']);
  $parent = intval($_POST['parent']);
  $sel = $_POST['sel'];

  if( $sel )
  {
    if( quickQuery( "select count(*) from category_relationships where child='$child' and parent='$parent'" )==0)
    {
      mysql_query( "insert into category_relationships (child,parent) values ($child, $parent)" );
    }
  }
  else
  {
    mysql_query( "delete from category_relationships where child=$child and parent=$parent" );
  }

  exit;
}

include "header.php";
?>
<style>
.tablematrix
{
  background-color:#555;
  font-size:8pt;
}

.tablematrix td
{
  background-color:#fff;
  text-align:center;
}

.tablematrix th
{
  background-color:#eee;
  width: 75px;
}

</style>

<div style="width:2000px; position:absolute; top:185px; left:340px;">
<div style="font-size:14pt;">Category Connections</div>

<table class="tablematrix">
<tr>
  <th>Subcategory</th>
<?
for( $c = 0; $c < sizeof( $columns ); $c++ )
{
?>
  <th><?=$columns[$c][0];?></th>
<?
}
?>
</tr>

<?

$q = mysql_query( "select * from categories where industry in (" . implode( ",", $businesses ) . ")" );
while( $r = mysql_fetch_array( $q ) )
{
  $parents = Array();

  $q2 = mysql_query( "select parent from category_relationships where child='" . $r['cat'] . "'" );
  while( $r2 = mysql_fetch_Array( $q2 ) )
    $parents[] = $r2['parent'];


?>
<tr>
  <td><?=$r['catname'];?></td>
<?
for( $c = 0; $c < sizeof( $columns ); $c++ )
{
  $child = $r['cat'];
  $parent = $columns[$c][1];
?>
  <td><input type="checkbox" name="a<?= $r['cat'] ?>-<?= $parent ?>" value="<?=$columns[$c][1]?>"<?=(in_array( $columns[$c][1], $parents) ? " CHECKED" : "" ); ?> onchange="javascript: changeSelection(<?= $child ?>, <?=$parent ?>, this.checked);"></td>
<?
}
?>
</tr>
<?
}
?>
</table>

<div style="clear:both;"></div>
</div>

<script>

function changeSelection(child,parent,selected)
{
  postAjax("/admin/category-relationships.php", "sav=1&child=" + child + "&parent=" + parent + "&sel=" + (selected?"1":"0"), alert);
}

</script>

<?
include "footer.php"; ?>