<?php

/*
8/26/2011 - Added TODO list constants

*/


// t = tagged in a photo
// T = tagged in a video
// g = created page
// G = joined page
// C = commented
// L = liked/disliked
// W = wallpost
// A = album
// P = photo
// V = video
// F = feed
// J = joined splash

if ($site == "s")
{
	define('SERVER_HOST', "www.salthub.com");
	define('SERVER_JMP', "salthub");
	define('SITE_VIA', "Salt_Hub");
}
else
{
	define('SERVER_HOST', "www.mediabirdy.com");
	define('SERVER_JMP', "mbirdy");
	define('SITE_VIA', "media_Birdy");
}

//////////////////////////////////////////////////////////////

// Personal_types
define("PT_MOVIES", 102);
define("PT_TV", 107);
define("PT_MUSIC", 103);
define("PT_BOOKS", 105);
define("PT_DEGREES", 1297);
define("PT_LIC", 101);
define("PT_ACTIVITIES", 1298);
//////////////////////////////////////////////////////////////

// Page categories
define("CAT_NOTWR", 100);
define("CAT_LICDEG", 101);
define("CAT_MOVIES", 102);
define("CAT_MUSIC", 103);
define("CAT_NETWORKS", 104);
define("CAT_BOOKS", 105);
define("CAT_PROFESSIONS", 106);
define("CAT_TV", 107);
define("CAT_VESSELS", 108);
define("CAT_WR", 109);
define("CAT_WORK", 110);
define("CAT_SCHOOL", 113);

//////////////////////////////////////////////////////////////

// Permanent Categories
define("CAT_COMM", 50); // commercial vessels
define("CAT_OIL", 51); // oil, gas, exploration
define("CAT_CRUISE", 52); // cruise ships and industry
define("CAT_YACHTS", 53); // yachts, tenders, and toys
define("CAT_MIL", 54); // military and history
define("CAT_SAIL", 55); // sailing vessels
define("CAT_EDU", 56); // education and safety

//////////////////////////////////////////////////////////////

// Send notifications
define("NOTIFY_PREF_FB", 0);
define("NOTIFY_PREF_TW", 10);
define("NOTIFY_PREF_MB", 20);
define("NOTIFY_PREF_EMAIL", 30);
define("NOTIFY_PREF_MY", 40); // not implemented

// Show the following activities on Facebook
define("NOTIFY_PREF_FB_UPLOAD", 1);
define("NOTIFY_PREF_FB_LIKE", 2);
define("NOTIFY_PREF_FB_COMMENT", 3);
define("NOTIFY_PREF_FB_ITAG", 4);
define("NOTIFY_PREF_FB_ADDTAG", 5);
define("NOTIFY_PREF_FB_MESSAGE", 6); // not implemented
define("NOTIFY_PREF_FB_TAGGED", 7);

// Show the following activities on Twitter
define("NOTIFY_PREF_TW_UPLOAD", 11);
define("NOTIFY_PREF_TW_LIKE", 12);
define("NOTIFY_PREF_TW_COMMENT", 13);
define("NOTIFY_PREF_TW_ITAG", 14);
define("NOTIFY_PREF_TW_ADDTAG", 15);
define("NOTIFY_PREF_TW_MESSAGE", 16); // not implemented
define("NOTIFY_PREF_TW_TAGGED", 17);

//Notifications
define("NOTIFY_PREF_MB_LIKE", 22);
define("NOTIFY_PREF_MB_COMMENT", 23);
define("NOTIFY_PREF_MB_ADDTAG", 24);
define("NOTIFY_PREF_MB_ADDFRIEND", 25);
define("NOTIFY_PREF_MB_MESSAGE", 26);
define("NOTIFY_PREF_MB_TAGGED", 27);
define("NOTIFY_PREF_MB_CMTSAME", 28);
define("NOTIFY_PREF_MB_WALLPOST", 29); // not implemented
define("NOTIFY_PREF_MB_GROUP", 21); // not implemented
define("NOTIFY_PREF_MB_PAGE_UPDATE", 130);
define("NOTIFY_PREF_WEEKLY_PAGES_SUMMARY", 131);
define("NOTIFY_PREF_WEEKLY_PROFILE_SUMMARY", 132);

// Show the following activities on MySpace (not implemented)
define("NOTIFY_PREF_MY_SHARE", 31);
define("NOTIFY_PREF_MY_LIKE", 32);
define("NOTIFY_PREF_MY_COMMENT", 33);

//////////////////////////////////////////////////////////////

define("NOTIFY_VIDEO_UPLOADED", 51);
define("NOTIFY_ALBUM_UPLOADED", 52);
define("NOTIFY_VIDEO_LIKE", 53);
define("NOTIFY_PHOTO_LIKE", 54);
define("NOTIFY_VIDEO_COMMENT", 55);
define("NOTIFY_PHOTO_COMMENT", 56);
define("NOTIFY_FRIEND_ADD", 57);
define("NOTIFY_FRIEND_SUGGEST", 58);
define("NOTIFY_VIDEO_TAGGED", 59);
define("NOTIFY_PHOTO_TAGGED", 60);
define("NOTIFY_MESSAGE", 61);
define("NOTIFY_CMTSAME", 62);
define("NOTIFY_ADDTAG", 63);
define("NOTIFY_ITAG", 64);
define("NOTIFY_ADDTAG_SOC", 65);
define("NOTIFY_WALL_COMMENT", 66);
define("NOTIFY_GROUP_ADD", 67);
define("NOTIFY_ALBUM_UPLOADED", 75);

define("NOTIFY_GROUP_COMMENT", 68);
define("NOTIFY_GROUP_NEW_CONNECT", 69);
define("NOTIFY_GROUP_NEW_VID", 70);
define("NOTIFY_GROUP_NEW_PHOTO", 71);
define("NOTIFY_GROUP_ADMIN", 72);
define("NOTIFY_GROUP_CLAIMED", 73 );
define("NOTIFY_GROUP_JOB_POSTED", 74 );

//////////////////////////////////////////////////////////////

define("PRIVACY_EVERYONE", 0); //the world
define("PRIVACY_FRIENDS", 85); //only my friends
define("PRIVACY_SELECTED", 170); //friends selected
define("PRIVACY_SELF", 255); //only me

//These are profile privacy settings
define("PRIVACY_MAX", 1 );
define("PRIVACY_BASICS", 2 );
define("PRIVACY_EDUCATION", 3 );
define("PRIVACY_WORK", 4 );
define("PRIVACY_NETWORKS", 5 );
define("PRIVACY_LIKES", 6 );
define("PRIVACY_LOG", 7 );
define("PRIVACY_VIDEOS", 8 );
define("PRIVACY_PHOTOS", 9 );
define("PRIVACY_CONNECTIONS", 10 );   //aka friends
define("PRIVACY_LOG_POSTING", 11 );   //prevent non-connections from posting
define("PRIVACY_EMPLOYMENT", 12 );
define("PRIVACY_FOLLOWME", 13 );
define("PRIVACY_NUM_ITEMS", 14 ); //number of privacy items

//////////////////////////////////////////////////////////////

define("SITE_FACEBOOK", 0);
define("SITE_TWITTER", 1);
define("SITE_OURS", 2);
define("SITE_HOTMAIL", 3);
define("SITE_YAHOO", 4);
define("SITE_GMAIL", 5);
define("SITE_MANUAL", 6);
define("SITE_CUSTOM", 7);

//////////////////////////////////////////////////////////////

define("TODO_PROFILE", 0);
define("TODO_NOTIFICATIONS", 1);
define("TODO_CONTACT_FOR", 1);
define("TODO_CONTACT_INFO", 2);
define("TODO_INVITE_CONTACTS", 3);
define("TODO_FIND_CONTACTS", 4);
define("TODO_COMPANY_PAGE", 5);
define("TODO_CREATE_GROUP", 6);
define("TODO_EXPLORE_GROUPS", 7);
define("TODO_CONFIG_MOBILE_UPLOAD", 8);

define( "LIC_AND_END_GRP", 101 );
define( "DEGREES_GRP", 1297 );
define( "OCCUPATION_GRP", 106 );

define( "PAGE_TYPE_BUSINESS", 1388 );
define( "PAGE_TYPE_VESSEL", 1389 );
define( "PAGE_TYPE_ENTERTAINMENT", 1390 );
define( "PAGE_TYPE_PROFESSIONAL", 1392 );

?>