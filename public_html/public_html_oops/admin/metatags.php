<?php

include "../inc/inc.php";

if( isset( $_POST['addnew'] ) )
{
  $script = addslashes( $_POST['script'] );
  $title = addslashes( $_POST['title'] );
  $descr = addslashes( $_POST['descr'] );
  $keywords = addslashes( $_POST['keywords'] );

  mysql_query( "insert into meta_tags (script, title, descr, keywords) values ('$script', '$title', '$descr', '$keywords')" );
  echo mysql_error();
}
else if( isset( $_GET['id'] ) )
{
  $id = intval( $_GET['id'] );
  $field = $_GET['field'];
  $value = addslashes( $_GET['value'] );

  mysql_query( "update meta_tags set $field='$value' where id='$id' limit 1" );
  echo mysql_error();
  echo "OK";
  exit;
}
else if( isset( $_GET['did'] ) )
{
  $id = intval( $_GET['did'] );
  mysql_query( "delete from meta_tags where id='$id' limit 1" );
  echo mysql_error();
}


include "header.php";

$sql = "select * from meta_tags order by script";
$q = mysql_query( $sql );

echo mysql_error();

?>
<style>
td
{
border: 1px solid black; border-left: 0; padding: 10px 4px;
}
</style>
<h1>Meta Tags &amp; Page Titles</h1>

<div style="font-size:9pt;">
<br />
Instructions:<br />
<ol>
  <li>Go to the page in which you want to modify the meta data.</li>
  <li>View source and find "script =".</li>
  <li>The value that script is assigned to is the value required for the "Page" column.</li>
  <li>Add Page, Title, Keywords, Description under "New Entry"</li>
  <li>Click "Save New Entry".</li>
</ol>
<br />
Changes made to these fields are saved automatically.
</div>


<div>
  <div style="clear:both;"></div>

  <table border="0" style="font-size: 9pt; border-left: 1px solid black; margin-top: 10px; width:760px; font-size:11px; float:left;" cellpadding="0" cellspacing="0">
  <tr style="background: black; color: white;">
    <td>Page</td>
    <td>Title</td>
    <td>Keywords</td>
    <td>Description</td>
  </tr>

<?
  while( $r = mysql_fetch_array( $q ) )
  {
	$odd = !$odd;

  $bg = ($odd ? "ddd" : "eee");
  if( $r['done'] ) $bg = "CCFF99";
	echo "<tr style=\"background: #" . $bg . ";\">";

?>
    <td><input name="script" value="<?=$r['script']?>" onchange="updateComments(<?=$r['id'];?>,'script',this.value);" /><br />(<a href="metatags.php?did=<?=$r['id']?>" onclick="return confirm('Are you sure you want to delete this entry?');">delete</a>)</td>
    <td align="center"><textarea name="title" rows="2" cols="25" onchange="updateComments(<?=$r['id'];?>,'title',this.value);"><?=$r['title'];?></textarea></td>
    <td align="center"><textarea name="keywords" rows="2" cols="25" onchange="updateComments(<?=$r['id'];?>,'keywords',this.value);"><?=$r['keywords'];?></textarea></td>
    <td align="center"><textarea name="descr" rows="2" cols="25" onchange="updateComments(<?=$r['id'];?>,'descr',this.value);"><?=$r['descr'];?></textarea></td>
	</tr>
<?
  }
?>

  <form action="metatags.php" method="POST">

  <tr>
    <td colspan="4" height="20" align="center">New Entry</td>
  </tr>
  <tr>
    <td valign="top"><input name="script" value="" /></td>
    <td align="center"><textarea name="title" rows="2" cols="25"></textarea></td>
    <td align="center"><textarea name="keywords" rows="2" cols="25"></textarea></td>
    <td align="center"><textarea name="descr" rows="2" cols="25"></textarea></td>
  </tr>
  <tr>
    <td colspan="4" align="center">
      <input type="submit" name="addnew" value="Save New Entry" />
    </td>
  </tr>
  </form>

  </table>
</div>

<script>
<!--
function updateComments(id, field, value)
{
  getAjax("/admin/metatags.php?id=" + id + "&field=" + encodeURIComponent(field) + "&value=" + encodeURIComponent(value), "void");
}
-->
</script>


<?php include "footer.php"; ?>