<?php

include "header.php";

switch( $_GET['email'] )
{
  case 1:
    $title = "Welcome";
    $day = "Sent 1 Day After Joining";
    $img = "delayed_email.png";
  break;

  case 2:
    $title = "Page Summary";
    $day = "Sent Every Thursday @ 1 PM EST";
    $img = "weekly_page_activity_email.png";
  break;

  case 3:
    $title = "Profile Summary";
    $day = "Sent Every Monday @ 1 PM EST";
    $img = "profile_summary_email.png";
  break;

  case 4:
    $title = "Vessel &amp; Connection Summary";
    $day = "Sent Every Monday @ 6:30 AM EST";
    $img = "weekly_vessel_updates_email.png";
  break;

  case 5:
    $title = "You have connections";
    $day = "Sent 2 and 20 days after registration";
    $img = "you_have_connections_email.png";
  break;

}
?>
<h1>Static Content - <? echo $title ?></h1>
<?=$day?>
<br /><br />
<img src="/images/<? echo $img ?>"  alt="" />

<?php include "footer.php"; ?>