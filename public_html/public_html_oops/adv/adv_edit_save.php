<?

/*
Saves advertisement data after editing or creating a new ad.
*/


require( "../inc/inc.php" );

$ID = intval( $_POST['ID'] );

if( stristr( $_POST['url'], "http://" ) === false )
  $_POST['url'] = "http://" . $_POST['url'];

$update = array();
$update['body'] = addslashes( $_POST['body'] );
$update['title'] = addslashes( $_POST['title'] );
$update['url'] = addslashes( $_POST['url'] );
$update['pid'] = addslashes( $_POST['pid'] );

if( $ID == 0 )
{
  $update['campaign'] = intval( $_POST['campaign'] );
  $update['uid'] = $API->uid;

  $keys = array();
  $values = array();
  foreach( $update as $k => $v )
  {
    $keys[] = $k;
    $values[] = $v;
  }

  $sql = "insert into ads2 (" . implode( ",", $keys ) . ") values ('" . implode( "','", $values ) . "')";
  sql_query( $sql );
  echo mysql_error();
}
else
{
  if( quickQuery( "select complete from ads2 where id='$ID'" ) > 0 )
    $status = $_POST['complete'];
  else
    $status = 0;

  $update['complete'] = $status;

  $update2 = array();
  foreach( $update as $k => $v )
  {
    $update2[] = $k . "='" . $v . "'";
  }

  $sql = "update ads2 set " . implode( ",", $update2 ) . " where uid='" . $API->uid . "' and ID='" . $ID . "'";
  sql_query( $sql );
  echo mysql_error();
}

header( "Location: campaign-details.php?ID=" . $_POST['campaign'] );
?>