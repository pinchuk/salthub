<?php
/*
Javascript functions that are common between various pages in the advertisment section of the site.
*/

header("Content-type: text/javascript");
include "../inc/inc.php";
?>

var updating;
var itemsChosen = Array();
var comboListBoxIndex = 0;

function newComboListBox(index, targetid ) //, title )
{
  tidEditing = index;

	html  = '<div style="width: 420px; padding-right: 10px; float: left;">';
	html += '	<div style="width: 250px; float:left;">';
//	html += '	  <div class="smtitle2" style="padding-bottom: 3px;">' + title + '</div>';
	html += '	  <div style="width: 250px; height: 18px; border: 1px solid #555; position: relative; float:left;">';
	html += '		  <input type="text" onkeyup="javascript:searchItems(' + tidEditing + ' );" id="itemsearch-' + tidEditing + '" style="margin: 0; padding: 0px 2px; border: 0; width: 229px; height: 18px;"><img src="/images/dropdown.png" onclick="javascript:toggleItemChooser(' + tidEditing + ');" style="cursor: pointer; vertical-align: top;" alt="" />';
	html += '		  <div id="itemchooser-' + tidEditing + '" class="itemchooser" style="width:233px;">';
	html += '			  <div id="chooser-' + tidEditing + '" class="chooser" style="width:227px;"></div>';
	html += '			  <div style="padding: 5px; text-align: center; background: #fff; border-top: 1px solid #555;">';
	html += '				  <input type="button" class="button" style="width:75px; margin:4px;" value="Apply" onclick="javascript:saveItems(' + tidEditing + '); toggleItemChooser(' + tidEditing + ');" /> <input style="width:75px; margin:4px;" type="button" class="button" value="Cancel" onclick="javascript:toggleItemChooser(' + tidEditing + '); loadItems(0,' + tidEditing + ' );" />';
	html += '			  </div>';
	html += '		  </div>';
	html += '	  </div>';
	html += '	  <div style="clear:both; padding-top: 5px; font-size: 8pt;">Your Selections</div>';
	html += '	  <div id="selections-' + tidEditing + '" style="font-size:8pt; margin-bottom:15px;"></div>';
	html += '	</div>';
  html += ' <div style="margin-bottom:10px;"></div>';
	html += '</div>';
	html += '<div style="clear: both;"></div>';

	e = document.getElementById(targetid);
	e.innerHTML = html;
	e.style.display = "inherit";

	loadItems(0, tidEditing);
}

function loadItems(i, tidEditing)
{
	loadjscssfile("/adv/loadComboListBoxItems.js.php?i=" + i + "&tid=" + tidEditing + "&hash=" + hash, "js");
}

function refreshSelections( tidEditing )
{
  html = '';

  e = document.getElementById( "selections-" + tidEditing );
  if( e )
  {
  	for (var i in itemsChosen[tidEditing])
  		if (itemsChosen[tidEditing][i] != null)
	  		if (itemsChosen[tidEditing][i][1] != null)
      	{
          html += '<a href="/profile/page-forward.php?gid=' +  itemsChosen[tidEditing][i][0] + '">' + itemsChosen[tidEditing][i][1] + "</a><br />";
        }

    if( html == '' )
      html = 'use dropdown above to select';
    e.innerHTML = html;
  }
}

function saveItemsHandler( data ) { //alert( data )
 }
function saveItems( tidEditing )
{
	pids = "";

	for (var i in itemsChosen[tidEditing])
	{
		if (itemsChosen[tidEditing][i] != null)
			if (typeof itemsChosen[tidEditing][i][0] != "undefined")
				pids += "," + itemsChosen[tidEditing][i][0];
	}

  data = "tid=" + tidEditing + "&items=" + pids.substring(1);

	postAjax("/adv/save_ad_preferences.php", data, "saveItemsHandler" );
}

function toggleItemChooser(tidEditing)
{
	e = document.getElementById("itemchooser-" + tidEditing);
	isShown = e.style.display == "inline";

	if (!isShown) // load item choices
	{
		document.getElementById("chooser-" + tidEditing).innerHTML = '&nbsp;';
		loadItems(1, tidEditing );
	}

	e.style.display = isShown ? "none" : "inline";
}

function searchItems(tidEditing)
{
  e = document.getElementById("itemsearch-" + tidEditing);

  if( !e )
  {
    alert( tidEditing );
  }

	q = e.value.toLowerCase();

	if (document.getElementById("itemchooser-" + tidEditing).style.display != "inline")
		toggleItemChooser(tidEditing);

	i = 0;
	
	while (e = document.getElementById("nameitem-" + i))
		document.getElementById("item-" + i++).style.display = e.innerHTML.toLowerCase().indexOf(q) == -1 ? "none" : "";
}

function itemChosen(i, pid, chosen, tidEditing)
{
	if (chosen)
	{
		name = document.getElementById("nameitem-" + tidEditing + "-" + i).innerHTML;
		itemsChosen[tidEditing][itemsChosen[tidEditing].length] = [pid, name];
	}
	else
	{
		for (var i in itemsChosen[tidEditing])
		{
			if (itemsChosen[tidEditing][i] != null)
				if (itemsChosen[tidEditing][i][0] == pid)
				{
					itemsChosen[tidEditing][i] = null;
					break;
				}
		}
	}

  refreshSelections( tidEditing );
}

function changeIndustry( selection )
{
  data = "tid=1&items=" + selection;
	postAjax("/adv/save_ad_preferences.php", data, "saveItemsHandler" );

  data = "tid=2&items=";
	postAjax("/adv/save_ad_preferences.php", data, "saveItemsHandler" );

  itemsChosen[2] = Array();
  refreshSelections(2);
}

function saveKeywords( )
{
  e = document.getElementById( 'keywords' );

  if( e )
  {
    data = "tid=6&items=" + encodeURIComponent(e.value);
  	postAjax("/adv/save_ad_preferences.php", data, "saveItemsHandler" );
  }
}

function refreshAd()
{
  title = document.getElementById('adtitle');
  body = document.getElementById('adbody');
  img = document.getElementById('adimg');

  srctitle = document.getElementById('title');
  srcbody =  document.getElementById('body');

  if( title && srctitle && srctitle.value != '' )
  {
    title.innerHTML = srctitle.value;
  }

  if( body && srcbody && srcbody.value != ''  )
  {
    if( srcbody.value.length > 150 )
      srcbody.value = srcbody.value.substring( 0, 149 );
    body.innerHTML = srcbody.value;
  }

}

function checkEditForm()
{
  title = document.getElementById('title');
  body = document.getElementById('body');
  url = document.getElementById('url');
  pide = document.getElementById('pid');

  pide.value = pid;

  if( title.value == "" ) { alert('You must enter a title for your advertisment'); return false; }
  if( body.value == "" ) { alert('You must enter a body for your advertisment'); return false; }
  if( url.value == "" ) { alert('You must enter a url for your advertisment'); return false; }
  if( pid == 0 ) { alert('You must upload a photo for your advertisment'); return false; }

  return true;
}