<?php
include "../inc/inc.php";
include "../header.php";
?>

<style>
<!--
.billing_header { color: #326798; margin-top:3px; padding-left:5px; font-size:10pt; font-weight:bold; }
.billing_itemhead { margin-top:15px; }
.billing_item {}
.hr { width:100%; height: 1px; color:#d8dfea; background-color:#d8dfea; margin-bottom:5px;}

.order_itemhead { clear:both; padding-left:10px; margin-top:5px; width:300px; float:left; font-size:9pt; font-weight:bold; }
.order_item { padding-right:10px; margin-top:5px; width:100px; float:right; font-size:9pt; font-weight:bold; }

-->
</style>



<? include( "adv_top_menu.php" ); ?>

<form action="billing2.php" method="POST" onsubmit="javascript:return submitForm();">

<div class="contentborder">
  <div class="strong" style="margin-top:20px; font-size:13pt;">Funding &amp; Credits</div>


  <div style="background-color:#f3f8fb; border:1px solid #d8dfea; width:500px; float:left; padding-bottom:9px; padding-top:6px;">

    <div class="billing_header">Payment Source</div>


    <!--<div class="billing_header">Add a Payment Source</div>-->

    <div style="float:left; padding-left:5px; font-size:8pt;">
      <div class="billing_itemhead" style="font-weight:bold;">Method:</div>
      <div class="billing_item">
      <select name="method" id="method" onchange="javascript: idx = this.selectedIndex; changeMethods(this.options[idx].value);">
        <option value="1">Credit Card</option>
<?
$promo = quickQuery( "select id from ad_promos where Now() > start and Now() < end and redemptions<max_redemptions" );
if( isset( $promo ) && $promo != "" )
{
  if( quickQuery( "select count(*) from ad_promo_redemptions where uid='" . $API->uid . "' and promo='$promo'" ) == 0 )
  {
?>
        <option value="2" SELECTED>Promo Code</option>
<?
  }
  else
    unset( $promo );
}
?>
      </select>
      </div>
    </div>

    <div style="clear:both;"></div>

    <div id="cc">
      <div style="float:left; padding-left:5px; font-size:8pt;">
        <div class="billing_itemhead">Cardholder First Name:</div>
        <div class="billing_item"><input type="text" name="firstname" maxlength="50" style="width: 180px;"/></div>

        <div class="billing_itemhead">Cardholder Last Name:</div>
        <div class="billing_item"><input type="text" name="lastname" maxlength="50" style="width: 180px;"/></div>

        <div class="billing_itemhead">Card Number:</div>
        <div class="billing_item"><input type="text" name="cc_number" maxlength="20" style="width: 180px;"/></div>

        <div class="billing_itemhead">Security Code:</div>
        <div class="billing_item"><input type="text" name="cc_code" maxlength="5" style="width: 50px;"/></div>

        <div class="billing_itemhead">Expiration Date:</div>
        <div class="billing_item">
        <select name="expmo">
          <? for( $c = 1; $c <= 12; $c++ ) echo '<option value="'. sprintf("%02d", $c) . '">' . $c . '</option>'; ?>
        </select>
        <select name="expyr">
          <? for( $c = intval(date("Y")); $c < intval(date("Y")+10); $c++ ) echo '<option value="'. $c . '">' . $c . '</option>'; ?>
        </select>
        </div>

        <div style="clear:both;"></div>


      </div>

      <div style="float:right; text-align:left; padding-right:5px; font-size:8pt;">
        <div class="billing_itemhead">Billing Address:</div>
        <div class="billing_item"><input type="text" name="address" maxlength="250" style="width: 200px;"/></div>

        <div class="billing_itemhead">City:</div>
        <div class="billing_item"><input type="text" name="city" maxlength="50" style="width: 200px;"/></div>

        <div class="billing_itemhead">State / Province:</div>
        <div class="billing_item">
        <select name="state" style="width:200px;" id="states">
  <?
  $q = sql_query( "select * from loc_state where country_id=220 order by region" );
  while( $r = mysql_fetch_array( $q ) )
  {
  ?>
        <option value="<? echo $r['region']; ?>"><? echo ucfirst($r['region']); ?></option>
  <?
  }
  ?>
        </select>
        </div>

        <div class="billing_itemhead">Postal Code:</div>
        <div class="billing_item"><input type="text" name="zip" maxlength="50" style="width: 70px;"/></div>

        <div class="billing_itemhead">Country:</div>
        <div class="billing_item">
        <select name="country" onchange="javascript: idx = this.selectedIndex; reloadStates(this.options[idx].value);" style="width:200px;">
  <?
  $q = sql_query( "select * from loc_country order by priority desc, country" );
  while( $r = mysql_fetch_array( $q ) )
  {
  ?>
        <option value="<? echo $r['id']; ?>"><? echo $r['country']; ?></option>
  <?
  }
  ?>
        </select>
        </div>

      </div>

      <div style="clear:left; padding-left:5px; font-size:8pt; padding-top:20px;">
        <input type="checkbox" name="recurring" checked="checked"/> Replenish Credits Monthly
        <a href="javascript: void(0);" onclick="javascript: showPopUp('', 'TBD');"><img src="/images/help.png" width="16" height="16" alt="" /></a>
      </div>

    </div>

    <div id="promo">
<?
if( isset( $promo ) && $promo != "" )
{
?>
      <div style="float:left; padding-left:5px; font-size:8pt;">
        <div class="billing_itemhead">Select Promotion:</div>
        <div class="billing_item" style="float:left;"><!--<input type="text" name="code" maxlength="50" style="width: 200px;" onkeyup="javascript: getPromoCodeStatus(this.value);"/>-->
          <select name="promo">
            <option value="<? echo $promo; ?>"><? echo quickQuery( "select description from ad_promos where id='$promo'" ); ?> (<? echo number_format(quickQuery( "select value from ad_promos where id='$promo'" )); ?> Credits)</option>
          </select>
        </div>

        <div id="promo_value" style="padding-left:5px; padding-top:4px; float:left;"></div>
      </div>
<?
}
?>
    </div>

    <div style="clear:both;"></div>

    <div class="billing_itemhead" style="padding-left: 8px;"><input type="submit" class="button" value="Submit"/></div>
  </div>

  <div style="background-color:#f3f8fb; border:1px solid #d8dfea; width:431px; float:right; padding-bottom:9px; padding-top:6px;">
    <div class="billing_header" style="font-size:10pt; margin-bottom:10px;">Order Information</div>

    <div class="order_itemhead">Account activation:</div>
    <div class="order_item" style="text-align:right;">$0.00</div>

    <div class="order_itemhead">Initial credit installment: (<span id="initialcredits">0</span> Credits)</div>
    <div class="order_item" style="text-align:right;">$<span id="initialcredits_cost">0.00</span></div>

    <div style="clear:both; height:1px; padding-top:10px; margin-bottom:10px; border-bottom: 1px dotted #326798; width:100%;"></div>

    <div class="billing_header" style="font-size:10pt; margin-bottom:10px;">Credits &amp; Specials</div>

    <div class="order_itemhead">Additional credits:
      <select name="addlcredits" id="addlcredits" onchange="javascript:recalc();">
      <? for( $c = 0; $c <= 10000; $c+=1000 ) { echo '<option value="' . $c . '">' . number_format( $c ) . '</option>'; } ?>
      </select>
    </div>
    <div class="order_item" style="text-align:right;">$<span id="addlcredits_cost">0.00</span></div>

    <div class="order_itemhead">Package:
      <select name="pkg" id="pkg" onchange="javascript:recalc();">
      <option value="0">-</option>
      <?
      $q2 = mysql_query( "select * from ad_packages order by name" );
      while( $r2 = mysql_fetch_array( $q2 ) )
      {
      ?>
        <option value="<? echo $r2['id']; ?>"><? echo $r2['name']; ?></option>
      <?
      }
      ?>
      </select>
    </div>
    <div class="order_item" style="text-align:right;">$<span id="pkg_cost">0.00</span></div>

    <div style="clear:both; height:1px; padding-top:10px; margin-bottom:10px; border-bottom: 1px dotted #326798; width:100%;"></div>

    <div class="order_itemhead">Total Purchases:</div>
    <div class="order_item" style="text-align:right;">$<span id="total_purchases">0.00</span></div>

    <div class="order_itemhead">Sales Tax:</div>
    <div class="order_item" style="text-align:right;">--</div>

    <div style="clear:both; height:1px; padding-top:10px; margin-bottom:10px; border-bottom: 1px dotted #326798; width:100%;"></div>

    <div class="order_itemhead" style="color:#000;">Total:</div>
    <div class="order_item" style="text-align:right; color:#000;">$<span id="total_cost">0.00</span></div>

  </div>

  <div style="background-color:#f3f8fb; border:1px solid #d8dfea; width:431px; float:right; padding-bottom:9px; padding-top:6px; margin-top:10px;">
    <div style="padding-top:10px; padding-left:10px;  font-size:14pt; font-weight:bold; float:left;">
      Credits after payment <a href="javascript: void(0);" onclick="javascript: showPopUp('', 'TBD');"><img src="/images/help.png" width="16" height="16" alt="" /></a>
    </div>

    <div style="padding-top:10px; padding-right:10px; font-size:14pt; font-weight:bold; float:right;"><span id="total_credits"><? echo number_format( intval( quickQuery( "select ad_credits from users where uid='" . $API->uid . "'" ) ) ); ?></span></div>
  </div>

  <div style="clear:both;"></div>


  <div style="clear:both;"></div>

</div>

</form>

<script type="text/javascript">
<!--
var valid           = 1;  //=0
var cpmrate         = <? echo quickQuery( "select content from static where id='cpm_rate'" ); ?>;
var currentCredits  = parseInt( "<? echo intval( quickQuery( "select ad_credits from users where uid='" . $API->uid . "'" ) ); ?>" );
var initialCredits  = 0; //Not sure how these are filled in?
var initialCost     = 0;

var packageCredits = new Array();
var packageCost = new Array();

<?
$q2 = mysql_query( "select * from ad_packages order by name" );
while( $r2 = mysql_fetch_array( $q2 ) )
{
?>
packageCredits[<? echo $r2['id'] ?>] = <? echo $r2['credits']; ?>;
packageCost[<? echo $r2['id'] ?>] = <? echo $r2['cost']; ?>;
<?
}
?>
function addCommas(nStr)
{
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
  if( x.length > 1 )
  	x2 = x.length > 1 ? '.' + x[1] : '';
  else
    x2 = "";

	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}
	return x1 + x2;
}

function recalc()
{
  totalCost = 0;
  totalCredits = currentCredits;

  var creditCost = new Number();

  //Retrieve fields
  addlcredits = document.getElementById( "addlcredits" );
  if( !addlcredits ) return;
  addlcredits_cost = document.getElementById( "addlcredits_cost" );
  if( !addlcredits_cost ) return;
  pkg = document.getElementById( "pkg" );
  if( !pkg ) return;
  pkg_cost = document.getElementById( "pkg_cost" );
  if( !pkg_cost ) return;
  initcredits = document.getElementById( "initialcredits" );
  if( !initcredits ) return;
  initcredits_cost = document.getElementById( "initialcredits_cost" );
  if( !initcredits_cost ) return;

  total_cost = document.getElementById( "total_cost" );
  if( !total_cost ) return;
  total_credits = document.getElementById( "total_credits" );
  if( !total_credits ) return;
  total_purchases = document.getElementById( "total_purchases" );
  if( !total_purchases ) return;

  //Initial Credits Calculation
  credits = initialCredits;
  creditCost = initialCost;
  initcredits_cost.innerHTML = addCommas( creditCost.toFixed(2) );
  totalCredits  += credits;
  totalCost     += creditCost;

  //Addl Credits Calculation
  credits = parseInt( selectedValue( addlcredits ) );
  creditCost = Math.floor( credits / 1000 ) * cpmrate;
  addlcredits_cost.innerHTML = addCommas( creditCost.toFixed(2) );
  totalCredits  += credits;
  totalCost     += creditCost;

  //Pkg Credits Calculation
  credits = 0;
  creditCost = 0;
  pkgid = selectedValue( pkg );
  if( pkgid > 0 )
  {
    credits = packageCredits[pkgid];
    creditCost = packageCost[pkgid];
  }
  pkg_cost.innerHTML = addCommas( creditCost.toFixed(2) );

  totalCredits  += credits;
  totalCost     += creditCost;

  total_cost.innerHTML = addCommas( totalCost.toFixed(2) ) ;
  total_credits.innerHTML = addCommas(totalCredits );
  total_purchases.innerHTML = addCommas(totalCost.toFixed(2) );
}

recalc();

function getPromoCodeStatus( code )
{
  if( code == "" || code.length <= 4 )
  {
    valid = 0;
    e = document.getElementById("promo_value");
    if( e )
      e.innerHTML = '';
    return;
  }

	postAjax("/adv/getCodeStatus.php", "code=" + code, "getPromoCodeStatusHandler" );
}

function getPromoCodeStatusHandler( data )
{
  parts = data.split( ";" );
  valid = parts[0];

  e = document.getElementById("promo_value");
  if( e )
    e.innerHTML = parts[1];
}

function submitForm()
{
  method = 0;
  e = document.getElementById("method");

  if( e )
  {
    method = e.options[ e.selectedIndex ].value;
  }

  if( method == 2 && valid == 0 )
  {
    alert( "Please enter a valid promo code before proceeding." );
    return false;
  }
  return true;
}

function changeMethods( method )
{
  e = 0;
  h = 0;
  switch( method )
  {
    case "1": e = document.getElementById("cc"); h = document.getElementById("promo"); break;
    case "2": h = document.getElementById("cc"); e = document.getElementById("promo"); break;
  }

  if( e && h )
  {
    e.style.display='';
    h.style.display='none';
  }
}

function reloadStates( country )
{

	postAjax("/adv/getStates.php", "country=" + country, "reloadStatesHandler" );
}

function reloadStatesHandler( data )
{
  e = document.getElementById("states");
  if( e )
  {
    e.innerHTML = data;
  }
}

e = document.getElementById("method");
if( e )
{
  changeMethods( e.options[ e.selectedIndex ].value );
}

<?
if( isset( $_GET['r'] ) ) {
?>
  showPopUp( '', '<div style="width:400px; text-align:center;"><b>Your purchase is complete!</b><br /><br />Your available credits have been updated<br /> and are now ready for use.</div>' );
<?
}
?>
-->
</script>
<?
include "../footer.php";
?>