<?
/*
Edit/Create advertisment campaigns
*/

$scripts[] = "/adv/advertising.js.php";

include( "../inc/inc.php" );
include "../header.php";

$ID = intval( $_GET['ID'] );

$q = sql_query( "select * from ad_campaigns where uid='" . $API->uid . "' and ID='$ID'" );

$adq = sql_query( "select * from ads2 where campaign='" . $ID . "'" );

if( mysql_num_rows( $q ) == 0 )
  die( "Invalid Campaign!" );

$r = mysql_fetch_array( $q );
?>

<script type="text/javascript">
<!--
function onLoadTargeting()
{
  newComboListBox( 2, "categoryBox" );
  newComboListBox( 3, "occupationBox" );
  newComboListBox( 4, "pageTypeBox" );
  newComboListBox( 5, "fromBox" );
}

function onLoadEditAd()
{
  pcInitUpload();
}
-->
</script>


<style>
.campaign_table {
  background-color: rgb(216, 223, 234);
  width:100%;
}

.campaign_table tr th {
  background-color: #EEEEEE;
  color:rgb(50, 103, 152);
  font-size: 9pt;
  height: 30px;
}

.campaign_table tr td
{
  background-color: #FFF;
  font-size: 9pt;
  height: 40px;
}

.shead {
  font-size:9pt;
  rgb(85, 85, 85);
  text-align:right;
  width:150px;
  clear:left;
  float:left;
  font-weight:700;
  padding: 8px 10px 5px 0pt;
}

.scontent {
  width: 360px;
  font-size:9pt;
  rgb(85, 85, 85);
  float:left;
  padding: 8px 10px 5px 0pt;
}

</style>

<? include( "adv_top_menu.php" ); ?>
<div class="contentborder">

  <div class="strong" style="margin-top:25px; font-size:13pt;">Campaign: <?= $r['name']; ?> </div>

  <div style="background-color:#f3f8fb; border:1px solid #d8dfea; width:550px; float:left; margin-bottom:10px; padding-top:6px;">
    <div class="shead" style="padding-top:12px;">Compaign Name:</div>
    <div class="scontent">
      <div style="float:left; padding-top:4px;" id="display_name">
        <? echo $r['name']; ?> <a href="javascript:void(0);" onclick="javascript: document.getElementById('display_name').style.display='none'; document.getElementById('edit_name').style.display=''; ">edit</a>
      </div>
      <div style="display:none;" id="edit_name">
        <input name="name" type="text" id="campaign_name" style="width:200px;"/ value="<? echo $r['name']; ?>"> <a href="javascript:void(0);" onclick="javascript: saveCampaignName();"><img src="../images/check.png" width="16" height="16" alt="" /> save</a>
      </div>
    </div>

    <div style="clear:both;"></div>

    <div class="shead">
      <div style="color:#326798;">Targeting</div>
    </div>
    <div class="scontent">
       <a href="javascript:void(0);" onclick="javascript: showPopupUrl('/adv/create2.php?ID=<?=$ID?>&popup', onLoadTargeting );">edit</a>
    </div>

    <div class="shead">
      Industry:
    </div>
    <div class="scontent">
    <?
    $industry = quickQuery( "select industry from ad_campaigns where id='$ID'" );
    if( $industry == 0 || $industry=="" )
      $industry = 111;
    echo quickQuery( "select catname from categories where cat='$industry'" )
    ?>
    </div>

    <div class="shead">
      Category:
    </div>
    <div class="scontent">
    <?
    $results = Array();
    $q = sql_query( "select catname from ad_preferences inner join categories on categories.cat = ad_preferences.type_id where ad_preferences.type='2' and ad='" . $ID . "'" );
    while( $r2 = mysql_fetch_array( $q ) )
    {
      if( $r2['catname'] == "Unselected" ) $results[] = "All";
      else $results[] = $r2['catname'];
    }
    echo implode( ", ", $results );
    ?>
    </div>

    <div class="shead">
      Occupation:
    </div>
    <div class="scontent">
    <?
    $results = Array();
    $q = sql_query( "select gname from ad_preferences inner join pages on pages.gid = ad_preferences.type_id where ad_preferences.type='3' and ad='" . $ID . "'" );
    while( $r2 = mysql_fetch_array( $q ) )
      $results[] = $r2['gname'];
    if( sizeof( $results ) == 0 ) echo "All";
    else echo implode( ", ", $results );
    ?>
    </div>

    <div class="shead">
      Page Types:
    </div>
    <div class="scontent">
    <?
    $results = Array();
    $q = sql_query( "select catname from ad_preferences inner join categories on categories.cat = ad_preferences.type_id where ad_preferences.type='4' and ad='" . $ID . "'" );
    while( $r2 = mysql_fetch_array( $q ) )
    {
      if( $r2['catname'] == "Unselected" ) $results[] = "All";
      else $results[] = $r2['catname'];
    }
    echo implode( ", ", $results );
    ?>
    </div>

    <div class="shead">
      From:
    </div>
    <div class="scontent">
    <?
    $results = Array();
    $q = sql_query( "select country from ad_preferences inner join loc_country on loc_country.id = ad_preferences.type_id where ad_preferences.type='5' and ad='" . $ID . "'" );
    while( $r2 = mysql_fetch_array( $q ) )
    {
      $results[] = $r2['country'];
    }
    if( sizeof( $results ) == 0 ) echo "All";
    else echo implode( ", ", $results );
    ?>
    </div>

    <div class="shead">
      Keywords:
    </div>
    <div class="scontent">
    <?
    echo $r['keywords'];
    ?>
    </div>

    <div class="shead">
      <div style="color:#326798;">Schedule</div>
    </div>
    <div class="scontent">
       <a href="javascript:void(0);" onclick="javascript: showPopupUrl('/adv/create3.php?ID=<?=$ID?>&popup' );">edit</a>
    </div>

    <div class="shead">
      Show:
    </div>
    <div class="scontent">
    <?
    echo ($r['runtime']==0?'Continuously' : 'Between the dates of ' . date( 'm/d/Y', strtotime($r['start']) ) . " and " . date( 'm/d/Y', strtotime($r['end']) ) );
    ?>
    </div>

    <div class="shead">
      Status:
    </div>
    <div class="scontent">
      <?
      switch($r['status'])
      {
        case 0: echo '<a href="create3.php?ID=' . $r['id'] . '">Incomplete</a>'; break;
        case 1: echo '<span style="color:#00CC00;">Active</span>'; break;
        case 2: echo 'Disabled'; break;
      }
      ?>
    </div>

    <div class="shead">
      Type:
    </div>
    <div class="scontent">
      CPM = cost per 1,000 impressions<br />Current CPM Rate = $<? echo strip_tags( quickQuery( "select content from static where id='cpm_rate'" ) ); ?>.00 (USD)
    </div>

    <div class="shead">
      Impressions:
    </div>

    <div class="scontent">
      <?=$r['daily_budget'];?> impressions daily
    </div>

  </div>

  <div style="clear:both;"></div>

  <? if( mysql_num_rows( $adq ) == 0 ) { ?>
    <div style="clear:both;">
    <div>This campaign does not have any advertisements in it. </div>
    <div style="float:left;"><a href="javascript:void(0);" onclick="javascript: showPopupUrl('/adv/adv_edit_container.php?c=<?=$ID?>', onLoadEditAd );"><img src="/images/add.png" width="16" height="16" /></div> <div style="float:left;">&nbsp;Create a new ad</div></a>
    </div>
  <? } else {
  $totals = Array();

  if( !isset( $_GET['range'] ) ) $_GET['range'] = 7;
  ?>
  <div style="clear:both; float:left; width:300px; font-weight:bold; color:rgb(85,85,85); font-size:13pt;">Advertisements in this Campaign </div>
  <div style="float:left; font-size:9pt; padding-top:4px;">
    <a href="javascript:void(0);" onclick="javascript: showPopupUrl('/adv/adv_edit_container.php?c=<?=$ID?>', onLoadEditAd );"><div style="float:left;"><img src="/images/add.png" width="16" height="16" /></div> <div style="float:left;">&nbsp;Create a new ad</div></a>
  </div>
  <div style="float:right; font-size:9pt;">
    View Statistics for:
    <select id="range" onchange="javascript: window.location='campaign-details.php?ID=<?=$ID?>&range=' + selectedValue( this );">
      <option value="0"<? if( $_GET['range'] == 0 ) echo " SELECTED"; ?>>Today</option>
      <option value="1"<? if( $_GET['range'] == 1 ) echo " SELECTED"; ?>>Yesterday</option>
      <option value="2"<? if( $_GET['range'] == 2 ) echo " SELECTED"; ?>>Last 7 Days</option>
      <option value="3"<? if( $_GET['range'] == 3 ) echo " SELECTED"; ?>>Last 15 Days</option>
      <option value="4"<? if( $_GET['range'] == 4 ) echo " SELECTED"; ?>>Last 30 Days</option>
      <option value="5"<? if( $_GET['range'] == 5 ) echo " SELECTED"; ?>>This Month</option>
      <option value="6"<? if( $_GET['range'] == 6 ) echo " SELECTED"; ?>>Previous Month</option>
      <option value="7"<? if( $_GET['range'] == 7 ) echo " SELECTED"; ?>>All</option>
    </select>
  </div>
  <table class="campaign_table" cellspacing="1" cellpadding="4">
  <tr>
    <th>Advertisement</th>
    <th>Tools</th>
    <th>Status</th>
    <th>Impressions</th>
    <th>Shared</th>
    <th>Clicks</th>
    <th>CTR</th>
    <th>Avg. CPM</th>
    <th>Debits</th>
  </tr>
<?
$num_ads = 0;
while( $ads = mysql_fetch_array( $adq ) ) {
  $url = $ads['url'];
  $hash = quickQuery( "select hash from photos where id='" . $ads['pid'] . "'" );

  //Get Statistics
  $num_ads++;
  $data = getAdStats( $ads['id'], $_GET['range'] );
  $totals['impressions'] += $data['impressions'];
  $totals['shares'] += $data['shares'];
  $totals['clicks'] += $data['clicks'];
  $totals['avgctr'] += $data['clicks'] / $data['impressions'] * 100;
  $totals['avgcpm'] += ( $data['cost'] / $data['impressions'] ) * 1000;
  $totals['cost'] += $data['cost'];
?>
  <tr>
    <td width="255">
      <div class="ad" style="float:left; height:75px; width:255px; cursor:pointer;">
        <div style="float:left; width:105px;">
          <div class="image" style="float:left;"><a href="<? echo $url ?>" target="_new"><img src="/img/100x75/photos/<? echo $ads['pid'] ?>/<? echo $hash ?>.jpg" width="100" height="75" border="0"/></a></div>
        </div>
        <div style="float:left; width:150px;">
          <div class="title" style="float:left;" onclick="javascript: location='<? echo $url ?>';"><? echo $ads['title']; ?></div>
          <div class="body" style="float:left;" onclick="javascript: location='<? echo $url ?>';"><? echo $ads['body'];?></div>
        </div>
      </div>
      <div style="clear:both; font-size:7pt; padding-top:3px;">
      URL: <a href="<? echo $url ?>"><? echo $url ?></a>
      </div>
    </td>

    <td>
      <a href="javascript:void(0);" onclick="javascript: pid=<?=$ads['pid'];?>; showPopupUrl('/adv/adv_edit_container.php?c=<?=$ID?>&ID=<?= $ads['id'] ?>', onLoadEditAd );">edit</a><br />
      <a href="adv_duplicate.php?ID=<?=$ads['id']; ?>&a" onclick="return confirm('Are you sure you want to duplicate this ad?');">duplicate</a><br />
      <a href="adv_delete.php?ID=<?=$ads['id']; ?>&a" onclick="return confirm('Are you sure you want to delete this ad?');">delete</a>
    </td>
    <td align="center">
      <?
      switch($ads['complete'])
      {
        case 0: echo 'Pending'; break;
        case 1: echo 'Disabled'; break;
        case 2: echo '<span style="color:#00CC00;">Approved</span>'; break;
      }
      ?>
    </td>
    <td align="right"><?=number_format($data['impressions'],0);?></td>
    <td align="center"><?=number_format($data['shares'],0);?></td>
    <td align="right"><?=number_format($data['clicks'],0);?></td>
    <td align="right"><?= number_format( $data['clicks'] / $data['impressions'] * 100, 2 ); ?>%</td>
    <td align="right">$<?= number_format( ( $data['cost'] / $data['impressions'] ) * 1000, 2 ); ?></td>
    <td align="right"><?= number_format( $data['cost'], 2 ); ?></td>
  </tr>
<?
}
if( $num_ads > 0 ) {
  $totals['avgctr'] /= $num_ads;
  $totals['avgcpm'] /= $num_ads;

?>
  <tr style="font-weight:bold;">
    <td><b>Totals for these ads</b></td>
    <td colspan="2"></td>
    <td align="right"><?=number_format($totals['impressions'],0);?></td>
    <td align="center"><?=number_format($totals['shares'],0);?></td>
    <td align="right"><?=number_format($totals['clicks'],0);?></td>
    <td align="right"><?=number_format($totals['avgctr'],2); ?>%</td>
    <td align="right">$<?=number_format($totals['avgcpm'],2); ?></td>
    <td align="right"><?=number_format($totals['cost'],2); ?></td>
  </tr>
  <tr style="font-weight:bold;">
    <td><b>Available Credits</b></td>
    <td colspan="7"></td>
    <td align="right"><?=number_format( quickQuery( "select ad_credits from users where uid='" . $API->uid . "'" ), 2 ); ?></td>
  </tr>
<?
}
?>
  </table>

  <? } // end mysql_num_rows == 0?>
  <div style="clear:both;"></div>
</div>


<script type="text/javascript">
<!--
var pid = 0;

function saveCampaignName()
{
  name = document.getElementById( 'campaign_name' ).value;

  data = "name=" + name + "&ID=<? echo $ID ?>";
  postAjax( "/adv/adv_update_name.php", data, saveNameResponse );
}

function saveNameResponse( data )
{
  if( admin && data != "OK" )
    alert( data );
  else
    window.location.reload();
}

-->
</script>

<?
$scripts[] = "/adv/photouploader.js";

include "../footer.php";
?>