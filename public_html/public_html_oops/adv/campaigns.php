<?
/*
List of advertisment campaigns.  Users see this page first to select which campaign they want to add/edit advertisments for.
*/

include( "../inc/inc.php" );
include "../header.php";

$q = sql_query( "select * from ad_campaigns where uid='" . $API->uid . "'" );
?>

<style>
.campaign_table {
  background-color: rgb(216, 223, 234);
  width:100%;
}

.campaign_table tr th {
  background-color: #EEEEEE;
  color:rgb(50, 103, 152);
  font-size: 9pt;
  height: 30px;
}

.campaign_table tr td
{
  background-color: #FFF;
  font-size: 9pt;
  height: 40px;
}

</style>

<? include( "adv_top_menu.php" ); ?>
<div class="contentborder">
<div style="clear:both; padding-top:10px;"></div>

  <? if( mysql_num_rows( $q ) == 0 ) { ?>
  <div class="strong" style="margin-top:20px; font-size:13pt;">Ad Campaigns</div>
  <div>
    You currently do not have any ad campaigns under your account.  <a href="create.php">Create a new ad campaign</a>.
  </div>
  <? } else {
  $totals = Array();

  if( !isset( $_GET['range'] ) ) $_GET['range'] = 7;
  ?>
  <div style="clear:both; float:left; width:300px; font-weight:bold; color:rgb(85,85,85); font-size:13pt;">Ad Campaigns</div>

  <div style="float:right; font-size:9pt;">
    View Statistics for:
    <select id="range" onchange="javascript: window.location='campaign-details.php?ID=<?=$ID?>&range=' + selectedValue( this );">
      <option value="0"<? if( $_GET['range'] == 0 ) echo " SELECTED"; ?>>Today</option>
      <option value="1"<? if( $_GET['range'] == 1 ) echo " SELECTED"; ?>>Yesterday</option>
      <option value="2"<? if( $_GET['range'] == 2 ) echo " SELECTED"; ?>>Last 7 Days</option>
      <option value="3"<? if( $_GET['range'] == 3 ) echo " SELECTED"; ?>>Last 15 Days</option>
      <option value="4"<? if( $_GET['range'] == 4 ) echo " SELECTED"; ?>>Last 30 Days</option>
      <option value="5"<? if( $_GET['range'] == 5 ) echo " SELECTED"; ?>>This Month</option>
      <option value="6"<? if( $_GET['range'] == 6 ) echo " SELECTED"; ?>>Previous Month</option>
      <option value="7"<? if( $_GET['range'] == 7 ) echo " SELECTED"; ?>>All</option>
    </select>
  </div>

  <table class="campaign_table" cellspacing="1" cellpadding="4">
  <tr>
    <th>Campaigns</th>
    <th>Tools</th>
    <th>Status</th>
    <th>Impressions</th>
    <th>Shared</th>
    <th>Clicks</th>
    <th>CTR</th>
    <th>Avg. CPM</th>
    <th>Debits</th>
  </tr>
<?
$num_ads = 0;
while( $r = mysql_fetch_array( $q ) ) {
  $data = getCampaignStats( $r['id'], $_GET['range'] );
  $totals['impressions'] += $data['impressions'];
  $totals['shares'] += $data['shares'];
  $totals['clicks'] += $data['clicks'];
  $totals['avgctr'] += $data['clicks'] / $data['impressions'] * 100;
  $totals['avgcpm'] += ( $data['cost'] / $data['impressions'] ) * 1000;
  $totals['cost'] += $data['cost'];
  $num_ads++
?>
  <tr>
    <td><a href="campaign-details.php?ID=<?=$r['id']; ?>"><b><? echo $r['name']; ?></b></a></td>
    <td>
      <a href="campaign-details.php?ID=<?=$r['id']; ?>">view</a><br />
      <a href="adv_duplicate.php?ID=<?=$r['id']; ?>&c">duplicate</a><br />
    </td>
    <td>
      <?
      switch($r['status'])
      {
        case 0: echo '<a href="create2.php?ID=' . $r['id'] . '">Incomplete</a>'; break;
        case 1: echo '<span style="color:#00CC00;">Active</span>'; break;
        case 2: echo 'Disabled'; break;
      }
      ?>
    </td>
    <td align="right"><?=number_format($data['impressions'],0);?></td>
    <td align="center"><?=number_format($data['shares'],0);?></td>
    <td align="right"><?=number_format($data['clicks'],0);?></td>
    <td align="right"><?= number_format( $data['clicks'] / $data['impressions'] * 100, 2 ); ?>%</td>
    <td align="right">$<?= number_format( ( $data['cost'] / $data['impressions'] ) * 1000, 2 ); ?></td>
    <td align="right"><?= number_format( $data['cost'], 2 ); ?></td>
  </tr>
<?
}
  $totals['avgctr'] /= $num_ads;
  $totals['avgcpm'] /= $num_ads;

?>
  <tr style="font-weight:bold;">
    <td><b>Totals for these campaigns</b></td>
    <td colspan="2"></td>
    <td align="right"><?=number_format($totals['impressions'],0);?></td>
    <td align="center"><?=number_format($totals['shares'],0);?></td>
    <td align="right"><?=number_format($totals['clicks'],0);?></td>
    <td align="right"><?=number_format($totals['avgctr'],2); ?>%</td>
    <td align="right">$<?=number_format($totals['avgcpm'],2); ?></td>
    <td align="right"><?=number_format($totals['cost'],2); ?></td>
  </tr>
  <tr style="font-weight:bold;">
    <td><b>Available Credits</b></td>
    <td colspan="7"></td>
    <td align="right"><?=number_format( quickQuery( "select ad_credits from users where uid='" . $API->uid . "'" ), 2 ); ?></td>
  </tr>
  </table>

  <? } // end mysql_num_rows == 0?>
  <div style="clear:both;"></div>
</div>
<?
include "../footer.php";
?>