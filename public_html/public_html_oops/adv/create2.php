<?php
/*
Step 2 of advertisment creation.
*/

include "../inc/inc.php";

$scripts[] = "/adv/advertising.js.php";

$ID = intval( $_POST['ID'] );
if( $ID == 0 ) $ID = intval( $_GET['ID'] );

$_SESSION['campaignid'] = $ID;

if( isset( $_GET['popup'] ) ) $popup = true;
else $popup = false;

if( stristr( $_POST['url'], "http://" ) === false )
  $_POST['url'] = "http://" . $_POST['url'];

$offensive = false;
$q = sql_query( "select word from dirty_words" );
while( $r = mysql_fetch_array( $q ) )
{
  if( strpos( $update['body'], $r['word'] ) !== false ) { $offensive = true; $update['body'] = ''; }
  if( strpos( $update['title'], $r['word'] ) !== false ) { $offensive = true; $update['title'] = ''; }
}

if( $ID )
{
  //We are no longer saving anything if we came here from another place.
/*
  $update2 = array();
  foreach( $update as $k => $v )
  {
    $update2[] = $k . "='" . $v . "'";
  }

  $sql = "update ads2 set " . implode( ",", $update2 ) . " where uid='" . $API->uid . "' and ID='" . $ID . "'";
  sql_query( $sql );
*/
}
else if( $ID == 0 || $ID == "" )
{
  $sql = "update photos set aid=884372, privacy=255 where id='" . $_POST['pid'] . "' and uid='" . $API->uid . "' limit 1";
  sql_query( $sql );

  $sql = "insert into ad_campaigns (uid, name) values ('" . $API->uid . "', '" . addslashes( $_POST['name'] ) . "')";
  sql_query( $sql );
  echo mysql_error();
  $ID = mysql_insert_id();

  $update = array();
  $update['body'] = addslashes( $_POST['body'] );
  $update['title'] = addslashes( $_POST['title'] );
  $update['url'] = addslashes( $url );
  $update['pid'] = addslashes( $_POST['pid'] );
  $update['uid'] = $API->uid;
  $update['campaign'] = $ID;

  $keys = array();
  $values = array();
  foreach( $update as $k => $v )
  {
    $keys[] = $k;
    $values[] = $v;
  }

  $sql = "insert into ads2 (" . implode( ",", $keys ) . ") values ('" . implode( "','", $values ) . "')";
  sql_query( $sql );

  echo mysql_error();
}

$q = mysql_query( "select * from ad_campaigns where ID='$ID'" );

if( mysql_num_rows( $q ) == 0 )
  die( "invalid campaign!" );

$r = mysql_fetch_array( $q );

if( !$popup ) include "../header.php";

$industry = $r['industry'];
if( $industry == 0 || $industry=="" )
{
  $industry = 111;
}

?>

<? if( !$popup ) include( "adv_top_menu.php" ); ?>

<div class="contentborder"<? if( $popup ) echo ' style="width:525px;"'; ?>>
  <div class="strong" style="<? if( !$popup ) echo 'margin-top:20px;'; ?> font-size:13pt;">Campaign settings</div>

  <div style="background-color:#f3f8fb; border:1px solid #d8dfea; margin-top:3px; width:500px; float:left; padding-bottom:9px; padding-top:6px;">
    <? //include( 'adv_create_menu.php' ); ?>

    <div style="color:#326798; font-size:9pt; font-weight:bold;  padding-left:5px; padding-top:5px;">About your ad</div>
    <div style="float:right; width:120px; font-size:8pt; color:#555;">Select the industry and category which best fits your ad.</div>

    <div class="thead" style="width:100px;">Industry:</div>
    <div class="tcontent" style="width:250px; padding-left:0px; margin-left:0px; padding-top:5px;" id="industryBox">
      <select name="industry" style="width:253px;" onchange="changeIndustry( this.value );">
      <option value="-1">All</option>
<?
$q2 = sql_query( "select * from categories where cattype='G' and industry='" . PAGE_TYPE_BUSINESS ."' order by catname" );
while( $r2 = mysql_fetch_array( $q2 ) )
{
?>
      <option value="<? echo $r2['cat']; ?>"<? if( $r2['cat'] == $industry ) echo " SELECTED"; ?>><? echo $r2['catname']; ?></option>
<?
}
?>
      </select>
    </div>

    <div class="thead" style="width:100px;">Category:</div>
    <div class="tcontent" style="width:250px; padding-left:0px; margin-left:0px; padding-top:5px;" id="categoryBox">
<!--      <select name="category" style="width:250px;">
<?
$q2 = sql_query( "select * from categories where cattype='C' order by catname" );
while( $r2 = mysql_fetch_array( $q2 ) )
{
?>
      <option value="<? echo $r2['cat']; ?>"<? if( $r2['cat'] == $r['category'] ) echo " SELECTED"; ?>><? echo $r2['catname']; ?></option>
<?
}
?>
      </select>
-->    </div>

    <div style="color:#326798; font-size:9pt; font-weight:bold; clear:both; padding-top:20px; padding-left:5px;">Demographics</div>
    <div style="float:right; width:120px; font-size:8pt; color:#555;">Tip: Too much refinement will limit the exposure of your ad.</div>

    <div class="thead" style="width:100px;">Occupation:</div>
    <div class="tcontent"  style="width:250px; padding-left:0px; margin-left:0px; padding-top:5px;" id="occupationBox">

    </div>

   <div style="color:#326798; font-size:9pt; font-weight:bold; clear:both; padding-top:20px; padding-left:5px;">By Pages</div>
    <div style="float:right; width:120px; font-size:8pt; color:#555;">Select page types and target members in these pages</div>

    <div class="thead" style="width:100px;">Page Types:</div>
    <div class="tcontent" style="width:250px; padding-left:0px; margin-left:0px; padding-top:5px;" id="pageTypeBox">
<!--
    <select name="pagetype" style="width:250px;">
<?
$q2 = sql_query( "select * from categories where cattype='G' and industry=0 order by catname" );
while( $r2 = mysql_fetch_array( $q2 ) )
{
?>
      <option value="<? echo $r2['cat']; ?>"<? if( $r2['cat'] == $r['pagetypes'] ) echo " SELECTED"; ?>><? echo $r2['catname']; ?></option>
<?
}
?>
      </select>-->
    </div>

    <div style="color:#326798; font-size:9pt; font-weight:bold; clear:both; padding-top:20px; padding-left:5px;">Location</div>
    <div style="float:right; width:120px; font-size:8pt; color:#555;">Only show this ad to users from these countries.</div>

    <div class="thead" style="width:100px;">From:</div>
    <div class="tcontent" style="width:250px; padding-left:0px; margin-left:0px; padding-top:5px;" id="fromBox">
<!--      <select name="country" style="width:250px;">
<?
$q2 = sql_query( "select country,id from loc_country order by priority desc" );
while( $r2 = mysql_fetch_array( $q2 ) )
{
?>
      <option value="<? echo $r2['id']; ?>"<? if( $r2['id'] == $r['country'] ) echo " SELECTED"; ?>><? echo $r2['country']; ?></option>
<?
}
?>
      </select>-->
    </div>

    <div style="color:#326798; font-size:9pt; font-weight:bold; clear:both; padding-top:20px; padding-left:5px;">User Profile</div>
    <div style="float:right; width:120px; font-size:8pt; color:#555;">Target keywords shown in users bio and contact for section.</div>

    <div class="thead" style="width:100px;">Keywords:</div>
    <div class="tcontent" style="width:250px; padding-top:5px;">
      <input id="keywords" type="text" name="keywords" style="width:250px;" value="<? echo $r['keywords']; ?>"/><br />
      <span style="font-size:8pt;">(Comma Seperated)</span>
    </div>

  </div>


  <? // include( "creatives.php" ); ?>


  <div style="clear:left; float:left;">
  <? if( !$popup ) { ?>
    <input style="margin-left:3px; margin-top:10px;" type="button" class="button" name="next" value="Next - Select Campaign Schedule &gt;" onclick="javascript: saveKeywords(); location='create3.php?ID=<? echo $ID ?>';"/>
    <input style="margin-left:3px; margin-top:10px;" type="button" class="button" name="save" value="Save &amp; Complete Later" onclick="javascript: saveKeywords(); location='campaigns.php';"/>
  <? } else { ?>
    <input style="margin-left:3px; margin-top:10px;" type="button" class="button" name="save" value="Save &amp; Close" onclick="javascript: saveKeywords(); window.location.reload();"/>
  <? } ?>
  </div>

  <div style="clear:both;"></div>
</div>



<script type="text/javascript">
<!--
var pid = 0;

//newComboListBox( 1, "industryBox" );
newComboListBox( 2, "categoryBox" );
newComboListBox( 3, "occupationBox" );
newComboListBox( 4, "pageTypeBox" );
newComboListBox( 5, "fromBox" );

-->
</script>
<?
if( !$popup ) include "../footer.php";
?>