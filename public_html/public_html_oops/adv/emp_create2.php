<?php
include "../inc/inc.php";

$scripts[] = "/employment/employment.js.php";

$ID = intval( $_POST['ID'] );

$update = array();
$update['body'] = addslashes( $_POST['body'] );
$update['title'] = addslashes( $_POST['title'] );
$update['url'] = addslashes( $_POST['url'] );
$update['pid'] = addslashes( $_POST['pid'] );
$update['gid'] = addslashes( $_POST['gid'] );

if( $_POST['usePID'] == 0 && $_POST['imageURL'] != '' )  //The image that the user wants to use is not a PID, it's is provided as a URL.
{
  $url = $_POST['imageURL'];

  //Save image
  $contents = file_get_contents($url);

  $source = "/tmp/jobPic" . end(explode('.', $source));
  file_put_contents( $source, $contents );

  $_GET['pc'] = 1;
  global $fromEmail;
  $fromEmail = true;

  ob_start();
  include_once "../upload/photo.php";
  processPhoto($source, true);
  $json = json_decode(ob_get_contents(), true);
  ob_end_clean();

  $update['pid'] = $json['id'];
}

$offensive = false;
$q = sql_query( "select word from dirty_words" );
while( $r = mysql_fetch_array( $q ) )
{
  if( strpos( $update['body'], $r['word'] ) !== false ) { $offensive = true; $update['body'] = ''; }
  if( strpos( $update['title'], $r['word'] ) !== false ) { $offensive = true; $update['title'] = ''; }
}

$sql = "update photos set aid=884372, privacy=255 where id='" . $update['pid'] . "'" . ($API->admin ? '' : " and uid='" . $API->uid ."'" );
sql_query( $sql );

if( $ID )
{
  $update2 = array();
  foreach( $update as $k => $v )
  {
    $update2[] = $k . "='" . $v . "'";
  }

  $sql = "update jobs set " . implode( ",", $update2 ) . " where uid='" . $API->uid . "' and ID='" . $ID . "'";
  sql_query( $sql );
}

if( $ID == 0 ) $ID = intval( $_REQUEST['ID'] );

if( $ID == 0 || $ID == "" )
{
  $keys = array();
  $values = array();

  $update['created'] = date( "Y-m-d" );

  foreach( $update as $k => $v )
  {
    $keys[] = $k;
    $values[] = $v;
  }
  $keys[] = "uid";
  $values[] = $API->uid;

  $sql = "insert into jobs (" . implode( ",", $keys ) . ") values ('" . implode( "','", $values ) . "')";
  sql_query( $sql );

echo mysql_error();

  $ID = mysql_insert_id();
}

if( $offensive )
{
  header( "Location: emp_create.php?off=&ID=" . $ID );
  exit;
}

if( isset( $_POST['save'] ) )
{
  header( "Location: emp_create.php?sav" );
  exit;
}

$_SESSION['jobid'] = $ID;

echo mysql_error();

$q = mysql_query( "select * from jobs where ID='$ID'" );

if( mysql_num_rows( $q ) == 0 )
  die( "invalid job listing!" );

$r = mysql_fetch_array( $q );

include "../header.php";

$industry = $r['industry'];
if( $industry == 0 || $industry=="" )
{
  $industry = 111;
}

$hash2 = quickQuery( "select hash from photos where id=" . $r['pid'] );

include( "employment_functions.php" );
?>

<? include( "emp_top_menu_admin.php" ); ?>

<form action="emp_create3.php" method="POST">
<input type="hidden" name="ID" value="<? echo $ID; ?>" />

<div class="contentborder">
  <div style="clear:both;"></div>
  <div class="strong" style="margin-top:10px; font-size:13pt;">Create a job posting</div>

  <div style="background-color:#f3f8fb; border:1px solid #d8dfea; margin-top:3px; width:500px; float:left; padding-bottom:9px; padding-top:6px;">
    <? include( 'emp_create_menu.php' ); ?>

    <div style="color:#326798; font-size:9pt; font-weight:bold;  padding-left:5px; padding-top:5px;">Target</div>
<!--    <div style="float:right; width:120px; font-size:8pt; color:#555;">Select the industry and category which best fits your ad.</div>-->

    <div class="thead" style="width:100px;">Sector:</div>
    <div class="tcontent" style="width:250px; padding-left:0px; margin-left:0px; padding-top:5px;" id="industryBox">
      <select name="sector" style="width:253px;" id="sector" onchange="javascript: refreshListing();">
<?
$q2 = sql_query( "select * from categories where cattype='G' and industry='" . PAGE_TYPE_BUSINESS . "' order by catname" );
while( $r2 = mysql_fetch_array( $q2 ) )
{
?>
      <option value="<? echo $r2['cat']; ?>"<? if( $r2['cat'] == $industry ) echo " SELECTED"; ?>><? echo $r2['catname']; ?></option>
<?
}
?>
      </select>
    </div>

    <div style="color:#326798; font-size:9pt; font-weight:bold; clear:both; padding-top:20px; padding-left:5px;">Demographics</div>

    <div class="thead" style="width:100px;">Position:</div>
    <div class="tcontent"  style="width:250px; padding-left:0px; margin-left:0px; padding-top:5px;" id="occupationBox">
      <select name="position" style="width:250px;" id="position" onchange="javascript: refreshListing();">
<?
$q2 = sql_query( "select gname, gid from pages where cat='" . CAT_PROFESSIONS . "' and popular=1 order by gname" );
while( $r2 = mysql_fetch_array( $q2 ) )
{
?>
      <option value="<? echo $r2['gid']; ?>"<? if( $r2['gid'] == $r['position'] ) echo " SELECTED"; ?>><? echo $r2['gname']; ?></option>
<?
}
?>
      </select>
    </div>

    <div style="color:#326798; font-size:9pt; clear:both; padding-top:20px; padding-left:5px; cursor:pointer;" onclick="javascript: toggleVessels();"><b>Vessel</b> (optional, select to show)</div>

    <div id="vesseldiv" style="display:none;">

    <div class="thead" style="width:100px;">Type:</div>
    <div class="tcontent"  style="width:250px; padding-left:0px; margin-left:0px; padding-top:5px;">
      <select name="vessel_type" style="width:250px;">
      <option value="">(Select)</option>
<?
$q2 = sql_query( "select catname, cat from categories where cattype='B' order by catname" );
while( $r2 = mysql_fetch_array( $q2 ) )
{
?>
      <option value="<? echo $r2['cat']; ?>"<? if( $r2['cat'] == $r['vessel_type'] ) echo " SELECTED"; ?>><? echo $r2['catname']; ?></option>
<?
}
?>
      </select>
    </div>

<?
$size = array( "0 - 35 ft (0 - 10m)", "36 - 65 ft (10 - 19m)", "66 - 90 ft (19 - 27m)", "91 - 130 ft (27 - 39m)", "131 - 150 ft (39 - 45m)", "151 - 180 ft (45 - 54m)", "181 - 200 ft (54 - 60m)", "201 - 250 ft (60 - 76m)", "251 - 300 ft (76 - 91m)", "301 - 400 ft (91 - 121m)", "401 - 600 ft (121 - 182m)", "601 - 1000 ft (182 - 304m)", "1000ft+ (304m+)" );
?>
    <div class="thead" style="width:100px;">Size:</div>
    <div class="tcontent"  style="width:250px; padding-left:0px; margin-left:0px; padding-top:5px;">
      <select name="vessel_size" style="width:250px;">
      <option value="">(Select)</option>
<?
for( $c = 0; $c < sizeof( $size ); $c++ )
{
?>
      <option value="<? echo $c ?>"<? if( $c == $r['vessel_size'] ) echo " SELECTED"; ?>><? echo $size[$c]; ?></option>
<?
}
?>
      </select>
    </div>


<?
$charter = array( "Charter", "Private", "Both" );
?>
    <div class="thead" style="width:100px;">Charter or<br /> Private:</div>
    <div class="tcontent"  style="width:250px; padding-left:0px; margin-left:0px; padding-top:5px;">
      <select name="vessel_charter" style="width:250px;">
      <option value="">(Select)</option>
<?
for( $c = 0; $c < sizeof( $charter ); $c++ )
{
?>
      <option value="<? echo $c ?>"<? if( $c == $r['vessel_charter'] ) echo " SELECTED"; ?>><? echo $charter[$c]; ?></option>
<?
}
?>
      </select>
    </div>

    </div>


    <div style="color:#326798; font-size:9pt; font-weight:bold; clear:both; padding-top:20px; padding-left:5px;">Job Location</div>

    <div class="thead" style="width:100px;">Country:</div>
    <div class="tcontent"  style="width:250px; padding-left:0px; margin-left:0px; padding-top:5px;">
      <select name="country" style="width:250px;" id="user-country" onchange="javascript: updateLocationSelect('state', this.value); refreshListing();">
<?
$country = $r['country'];

$q2 = sql_query( "select country,id from loc_country order by priority desc, country" );
while( $r2 = mysql_fetch_array( $q2 ) )
{
?>
      <option value="<? echo $r2['id']; ?>"<? if( $r2['id'] == $r['country'] ) echo " SELECTED"; ?>><? echo $r2['country']; ?></option>
<?
  if( $country == 0 ) $country = $r2['id'];
}
?>
      </select>
    </div>

    <div class="thead" style="width:100px;">State:</div>
    <div class="tcontent"  style="width:250px; padding-left:0px; margin-left:0px; padding-top:5px;" id="userinfo-state">
      <select name="state" style="width:250px;" id="user-state" onchange="javascript: updateLocationSelect('city', this.value); refreshListing();">
<?
$state = $r['state'];
$q2 = sql_query( "select region,id from loc_state where country_id='$country' order by region" );
while( $r2 = mysql_fetch_array( $q2 ) )
{
?>
      <option value="<? echo $r2['id']; ?>"<? if( $r2['id'] == $r['state'] ) echo " SELECTED"; ?>><? echo ucwords(strtolower($r2['region']) ); ?></option>
<?
  if( $state == 0 ) $state = $r2['id'];
}
?>
      </select>
    </div>

    <div class="thead" style="width:100px;">City:</div>
    <div class="tcontent"  style="width:250px; padding-left:0px; margin-left:0px; padding-top:5px;" id="userinfo-city">
      <select name="city" style="width:250px;" id="user-city" onchange="javascript: refreshListing();">
<?
$q2 = sql_query( "select city,id from loc_city where region_id='$state' order by city" );
while( $r2 = mysql_fetch_array( $q2 ) )
{
?>
      <option value="<? echo $r2['id']; ?>"<? if( $r2['id'] == $r['city'] ) echo " SELECTED"; ?>><? echo ucwords(strtolower($r2['city'])); ?></option>
<?
}

$coname = "";
if( $r['gid'] == 0 )
  $coname = quickQuery( "select name from users where uid='" . $r['uid'] . "'" );
else
  $coname = quickQuery( "select gname from pages where gid='" . $r['gid'] . "'" );
?>
      </select>
    </div>

    <div style="clear:both;"></div>

    <div class="strong" style="color: #326798; margin-top:10px; margin-left:40px; font-size:13pt;">Job Listing Preview</div>

    <div style="background-color:#ffffff; border:1px solid #d8dfea; margin-left:40px; width:430px; height:150px; overflow:hidden;">
      <div class="job_listing" style="padding:5px;">
        <div class="image" id="limg"><? if( isset( $r ) ) echo '<img src="/img/100x75/photos/' . $r['pid'] . '/' . $hash2 . '.jpg" width="100" height="75" />'; else echo '<img src="/images/salt_badge100x75.png" width="100" height="75" />'; ?></div>
        <div class="title"><span id="ltitle"><?if( isset( $r ) ) echo $r['title']; else echo $title_default?></span> - <? echo $coname; ?> - <span class="subtitle2" id="lsubtitle"><?if( isset( $r ) ) echo getSubTitle( $r ); ?></span></div>
        <div class="body" id="lbody"><? if( isset( $r ) ) echo nl2br($r['body']); else echo $body_default?></div>
        <div style="clear:both;"></div>
      </div>
    </div>


  </div>


  <? include( "my_jobs.php" ); ?>

  <div style="clear:left;"></div>

  <div style="clear:left; float:left;">
    <input style="margin-left:3px; margin-top:10px;" type="submit" class="button" name="next" value="Next &gt;"/>
    <input style="margin-left:3px; margin-top:10px;" type="submit" class="button" name="save" value="Save"/>
  </div>

  <div style="clear:both;"></div>
</div>

</form>

<script type="text/javascript">
<!--
function updateLocationSelect(f, v)
{
  if( document.getElementById("userinfo-" + f) )
  	document.getElementById("userinfo-" + f).innerHTML = '<img src="/images/wait_sm.gif" alt="" />';
	getAjax("/employment/locationselect.php?f=" + f + "&v=" + v, "updateLocationSelectHandler");
}

function updateLocationSelectHandler(data)
{
	x = data.split("|");
	document.getElementById("userinfo-" + x[0]).innerHTML = x[1];
}


function toggleVessels()
{
  e = document.getElementById( "vesseldiv" );

  if( e )
  {
    if( e.style.display == '' )
      e.style.display = 'none';
    else
      e.style.display = '';
  }
}

function refreshListing()
{
  subtitle = document.getElementById('lsubtitle');

  srccountry =  document.getElementById('user-country');
  srcstate =  document.getElementById('user-state');
  srccity =  document.getElementById('user-city');

  if( subtitle )
  {
    cnty = srccountry.options[ srccountry.selectedIndex ].text;
    state = srcstate.options[ srcstate.selectedIndex ].text;

    if( state != "-" )
      cnty = state + " " + cnty;

    if( srccity )
    {
      city = srccity.options[ srccity.selectedIndex ].text;

      if( city != "-" )
        cnty = city + ", " + cnty;
    }

    subtitle.innerHTML = cnty;
  }
}

refreshListing();
-->
</script>
<?
include "../footer.php";
?>