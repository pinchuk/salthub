<?
/*
Saves ad preferences when the user selects new items in the drop downs during the ad creation process.

Related to "loadComboListBoxItems.js.php"
*/

include "../inc/inc.php";

if( empty( $_SESSION['campaignid'] ) )
{
  die('No ad id saved.');
}

$ad = $_SESSION['campaignid'];
$type = $_POST['tid'];
$items = $_POST['items'];

$items = explode( ",", $items );

if( $type == 1 ) //update industry
  sql_query( "update ad_campaigns set industry='" . $items[0] . "' where id='$ad' and uid='" . $API->uid . "'" );
else if( $type == 6 ) //update keyword
  sql_query( "update ad_campaigns set keywords='" . $_POST['items'] . "' where id='$ad' and uid='" . $API->uid . "'" );
else
{
  sql_query( "delete from ad_preferences where ad='$ad' and type='$type' and uid='" . $API->uid . "'" );

  for( $c = 0; $c < sizeof( $items ); $c++ )
  {
//    if( intval( $items[$c] ) > 0 )    //Commented out because "All" is == 0
      sql_query( "insert into ad_preferences (uid, ad, type, type_id) values (" . $API->uid . ", $ad, $type, " . intval( $items[$c] ) . ")" );
  }
}

if( $type == 5 )
{
  sql_query( "update ad_campaigns set locations='," . $_POST['items'] . ",' where id='$ad' and uid='" . $API->uid . "'" );
}

?>OK