<?
/*
Container for creatives.php - a script that displays a user's advertisements and their current status  
*/

include( "../inc/inc.php" );
include "../header.php";
?>

<? include( "adv_top_menu.php" ); ?>
<div class="contentborder" style="padding-bottom:15px;">
  <? include( "creatives.php" ); ?>
  <div style="clear:both;"></div>
</div>
<?
include "../footer.php";
?>