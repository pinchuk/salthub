<?php

include_once '../inc/inc.php';
header('Content-Type: application/json');

if (empty($_GET['t']))
	$_GET['t'] = 'VPvpU'; //get em all

$q = addslashes($_REQUEST['q']);
$n = intval($_REQUEST['n']);

$guid = substr($_REQUEST['guid'], 0, 36);

$type_strings = array('U' => $siteName . ' User', 'v' => 'Video', 'p' => 'Photo');

if (!empty($q))
{
	if ($n < 1 || $n > 100)
		$n = 100;
	
	$select_pages = array();
	$types = array();
	$union = array();
	$join = array();
	$cats = array();
	$query = array();
	
	$include_pages = false;
	
	foreach (str_split($_GET['t']) as $t)
		switch ($t)
		{
			case 'V':
				$include_pages = true;
				$select_pages['exnames'] = "trim(exnames)";
				$select_pages['length_ft'] = "length";
				$select_pages['length_m'] = "(length*0.3048)";
				$types[] = PAGE_TYPE_VESSEL;
				$union[] = "select pages.gid from boats inner join pages on boats.id=pages.glink where name != '' and exnames like ' {$q}%'";
				$join[] = "LEFT JOIN boats ON glink=boats.id";
				break;
			
			case 'P': //all pages
				$include_pages = true;
				$types[] = 'type';
				break;
			
			case 'E':
				$include_pages = true;
				$types[] = PAGE_TYPE_BUSINESS;
				break;
			
			case 'S':
				$include_pages = true;
				$cats[] = CAT_SCHOOL;
				break;
			
			case 'U':
				$query[] = "select 'U' as type,users.uid,replace(username,' ','.') as username,name as title,container_url,hash from users
								left join photos on users.pic=photos.id
								where active=1 and name like '%{$q}%'";
				break;
		}
	
	$sel_pages = '';
	
	if (count($select_pages) > 0)
		foreach ($select_pages as $k => $v)
			$sel_pages .= ",$v as $k";
	
	foreach (str_split($_GET['t']) as $t)
		switch ($t)
		{
			case 'v':
				$include_media = true;
				$query[] = "select videos.cat,'v' as type,container_url,videos.hash,title,catname as cat_str,videos.id as media_id,replace(username,' ','.') as username,name,duration,descr,users.uid,photos.hash as user_thumb from videos
								inner join users on users.uid=videos.uid
								inner join photos on photos.id=users.pic
								inner join categories on videos.cat=categories.cat
								where ready=1 and videos.privacy=" . PRIVACY_EVERYONE . " and title like '{$q}%'";
				break;
			
			case 'p':
				$include_media = true;
				$query[] = "select photos.cat,'p' as type,container_url,photos.hash,title as album_title,photos.ptitle as title,catname as cat_str,photos.aid,photos.id as media_id,replace(username,' ','.') as username,name,descr,users.uid,p2.hash as user_thumb from photos
								left join users on users.uid=photos.uid
								left join photos p2 on p2.id=users.pic
								left join albums on photos.aid=albums.id
								left join categories on photos.cat=categories.cat
								where photos.privacy=" . PRIVACY_EVERYONE . " and photos.ptitle like '{$q}%'";
				break;
		}
	
	if ($include_pages)
	{
		array_unshift($union, "SELECT gid from pages where gname LIKE '{$q}%' AND pages.privacy=" . PRIVACY_EVERYONE . " and (" .
									(count($types) > 0 ? "type=" . implode(' or type=', $types) . (count($cats) > 0 ? ' or ' : '') : '') .
										(count($cats) > 0 ? " cat=" . implode(' or cat=', $cats) : "") . "
										)");
		
		$query[] = "SELECT pages.cat,pages.gid,type,users.container_url,hash,gname as title,catname as cat_str{$sel_pages}
					FROM pages
					natural join (
						(" . implode(")\n\t\t\t\t\tunion (", $union) . ")
					) as gids
					" . implode ("\n", $join) . "
					LEFT JOIN photos ON photos.id=pid
					LEFT JOIN users ON photos.uid=users.uid
					inner join categories on categories.cat=pages.cat
					group by pages.gid";
	}
	
	if (@$_GET['g'] == '0') // do not sort results (one query, limit $n)
	{
		$new_query = array();
		
		$i = 0;
		foreach ($query as $q)
		{
			$table = 'derived' . ++$i;
			
			$new_query[] = "($q) " . $table . ($i > 1 ? ' on true' : '');
		}
		
		$new_query_str = 'select * from ' . implode("\n\n\t\t\tleft join\n\n", $new_query);
		$queries = array($new_query_str);
	}
	else
		$queries = $query; // sort results by type (separate queries, limit $n * # of queries)
	
	$starttime = microtime(true);
	
	$results = array();
	$queries = array_reverse($queries);
	$executed_queries = array();
	
	foreach ($queries as $q)
	{
		$executed_queries[] = "$q order by " . (empty($table) ? '' : "{$table}.") . "title limit $n";
		//die(end($executed_queries));
		
		$x = mysql_query(end($executed_queries));
		if (mysql_num_rows($x) > 0)
		{
			while ($y = mysql_fetch_array($x, MYSQL_BOTH))
			{
				$i = 0;
				foreach ($y as $k => $v)
				{
					if ($i % 2 == 0)
					{
						$val = $v;
						unset($y[$k]);
					}
					else
						$y[$k] = $val;
					
					$i++;
				}
				
				$type = '';
				$type_str = '';
				
				switch ($y['type'])
				{
					case PAGE_TYPE_VESSEL:
						$type = 'V';
						break;
					
					case PAGE_TYPE_BUSINESS:
						$type = 'E'; // (= employer)
						break;
				}
				
				if (empty($type)) // type has not yet been assigned - maybe we can figure it out from the category
					switch ($y['cat'])
					{
						case CAT_SCHOOL:
							$type = 'S';
							$type_str = 'School';
							break;
					}
				
				if (empty($type_str))
					$type_str = get_type_string($y['type']);
				
				$y['type_str'] = $type_str;
				$y['type'] = empty($type) ? $y['type'] : $type;
				$results[] = array_filter($y, 'strlen');
			}
		}
	}
}

$return = array('guid' => $guid, 'results' => $results);

$time = microtime(true) - $starttime;

if ($isDevServer)
	$return['time'] = $time;

if ($_GET['debug'] == 1 && $isDevServer)
{
	echo "Time: $time\n\n";
	echo "====================== QUERIES (" . count($executed_queries) . ") ======================\n";
	print_r($executed_queries);
	
	echo "\n\n====================== RESULTS (" . count($results) . ") ======================\n";
	print_r($return);
}
else
	echo json_encode($return);

function get_type_string($id)
{
	global $type_strings;
	
	if (empty($type_strings[$id]))
		$type_strings[$id] = quickQuery("select catname from categories where cat='{$id}'");
	
	return $type_strings[$id];
}
?>