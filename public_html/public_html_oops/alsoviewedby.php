<?php
/*
A module that is included by /media.php which shows a small picture of users who have also viewed this media.

*/

include_once "inc/inc.php";

if( empty( $type ) ) $type = $_GET['type'];
if( empty( $link ) ) $link = intval($_GET['link']);
if( empty( $p ) ) $p = intval( $_GET['p'] );

$i = 0;
$x = sql_query("select twusername,fbid,name,views.uid,pic,username from views inner join users on views.uid=users.uid where active=1 and type='" . $type . "' and link=" . $link . ($API->isLoggedIn() ? " and users.uid != " . $API->uid : "") . " order by ts desc limit " . ($p * 8) . ",10");
if (mysql_num_rows($x) > 0)
{
  while ($user = mysql_fetch_array($x, MYSQL_ASSOC))
	{
		if ($i++ == 9) break;
		$profileURL = $API->getProfileURL($user['uid'], $user['username']);
		$profilePic = $API->getUserPic($user['uid'], $user['pic']);
		$parts = array(rawurlencode($profileURL), rawurlencode($profilePic), rawurlencode($user['twusername']), rawurlencode($user['fbid']), rawurlencode($user['name']));
		$id = implode("-", $parts);
		?>
		<a id="<?=$i?>-<?=$id?>" onmouseover="javascript:showTip3(this,<?=$user['uid'];?>,'U');" onmouseout="javascript:tipMouseOut();" href="<?=$profileURL?>"><img src="<?=$API->getThumbURL(1, 24, 24, $profilePic)?>" alt="" /></a>
		<?php
	}
?>
<br/>also viewed by<?php if (mysql_num_rows($x) > 9) { ?>&nbsp; <!--<a href="javascript:void(0);" onclick="javascript:moreAlsoViewedBy();">(more)</a>--><?php } ?>
<?php
}
?>