<?
/*
Contains billing functions that are common across mosts scripts in /billing.

9/9/2012 - Added: When a user purchases a directory listing, the page is automatically verified
*/

function billingRedeemCoupon( $code )
{
  global $API;

  $id = intval( quickQuery( "select id from billing_promos where code='$code' AND Now() < end AND redemptions < max_redemptions" ) );

  if( quickQuery( "select count(*) from billing_promo_redemptions where uid='" . $API->uid . "' and promo='$id'" ) == 0 )
  {

    if( $id > 0 )
    {
      $value = intval( quickQuery( "select value from billing_promos where id='$id'" ) );
      sql_query( "update users set ad_credits=ad_credits+$value where uid='" . $API->uid . "'" );
      sql_query( "update billing_promos set redemptions=redemptions+1 where id='$id'" );
      sql_query( "insert into billing_promo_redemptions (uid, promo) values ('" . $API->uid . "', '$id')" );
      return true;
    }
    else
      return false;
  }
  else
    return false;
}

function billingAddPayment( $total, $credits, $credits_purchased, $transactionid, $profile )
{
  global $API;
  $uid = $API->uid;
  mysql_query( "insert into billing_purchase_history (uid, day, total, credits_used, credits_purchased, transactionid, profile) values ($uid, Now(), '$total', '$credits', '$credits_purchased', '$transactionid', '$profile')" );
  return mysql_insert_id();
}

function billingAddService( $type, $purchase, $link, $qty )
{
  global $API;

  $uid = $API->uid;

  $daysOfService = quickQuery( "select days_of_service from billing_purchase_types where id='" . $type . "'" );
  if( $daysOfService <= 0 ) { echo "Invalid product type "; return; }

  $expires = ( $daysOfService > 0 ? 1 : 0) ;

  $id = quickQuery( "select id from billing_account_services where uid='" . $API->uid . "' and type='$type' and link='$link'" );

  if( $id > 0 )  //Either add days of service, or add a new service
    mysql_query( "update billing_account_services set purchase='$purchase', expiration=date_add( expiration, INTERVAL $daysOfService DAY ) where id='$id' limit 1" );
  else
    mysql_query( "insert into billing_account_services (uid, type, expiration, expires, purchase, link) values ($uid, $type, date_add( Now(), INTERVAL $daysOfService DAY), '$expires', '$purchase', '$link')" );

  switch( $type )
  {
    case PURCHASE_TYPE_AD_CAMPAIGN: //Advertising campaign
      mysql_query( "update ad_campaigns set status=1 where id='$link' limit 1" );
    break;

    case PURCHASE_TYPE_JOB_LISTING: //Job Listing
      mysql_query( "update jobs set funded=1 where id='$link' limit 1" );
    break;

    case PURCHASE_TYPE_DIR_LISTING: //Directory Listing
    {
      mysql_query( "update pages set dir_listing_funded=1 where gid='$link' limit 1" );

      //If the page isn't verified yet, then verify it!
      $verified = quickQuery( "select verified from pages where gid='$link'" );
      if( $verified == 0)
      {
        $API->verifyPage( $link );
      }
    }
    break;

    case PURCHASE_TYPE_IND_LISTING: //Sector Listings
      mysql_query( "update pages set sectors_funded=sectors_funded+$qty where gid='$link' limit 1" );

      $cats = $_SESSION['unfunded_sectors'][$link];
      for($c=0; $c < sizeof( $cats ) && $c < $qty; $c++ )
      {
        $item=$cats[$c];
        mysql_query( "insert into page_categories (gid,cat) values ($link, $item)" );
      }

      unset( $_SESSION['unfunded_sectors'][$link] );
    break;

    case PURCHASE_TYPE_CAT_LISTING: //Products & Services Listing
      mysql_query( "update pages set categories_funded=categories_funded+$qty where gid='$link' limit 1" );

      $cats = $_SESSION['unfunded_categories'][$link];

      for($c=0; $c < sizeof( $cats ) && $c < $qty; $c++ )
      {
        $item=$cats[$c];
        mysql_query( "insert into page_categories (gid,cat) values ($link, $item)" );
      }

      unset( $_SESSION['unfunded_categories'][$link] );
    break;

  }

  $cost = quickQuery( "select cost from billing_purchase_types where id='$type'" );
  mysql_query( "insert into billing_purchase_history_items (payment, quantity, cost, type) values ($purchase, $qty, $cost, $type)" );

  return mysql_insert_id();
}

function billingAddItemToCart( $type, $link, $qty = 1)
{
  global $_SESSION;

  if( !is_array( $_SESSION['cart'] ) )
    $_SESSION['cart'] = array();

  $index = sizeof( $_SESSION['cart'] );

  while( array_key_exists( $index, $_SESSION['cart'] ) )
    $index++;

  $_SESSION['cart'][$index] = array( 'type'=>$type, 'link'=>$link, 'qty'=>$qty );

  return true;
}

function billingGetCart()
{
  global $_SESSION;
  return $_SESSION['cart'];
}

function billingRemoveItemFromCart( $index )
{
  global $_SESSION;

  if( array_key_exists( $index, $_SESSION['cart'] ) )
  {
    unset( $_SESSION['cart'][$index] );
    return true;
  }
  else
    return false;
}

function billingClearCart()
{
  global $_SESSION;
  unset( $_SESSION['cart'] );
}

function billingGetCreditCost( $credits )
{
  if( $credits <= 99 ) return $credits;
  else if( $credits <= 999 ) return round($credits * .9);
  else if( $credits <= 4999 ) return round($credits * .8);
  else return round($credits * .7);
}


?>