<?
/*
Removes an item from the shopping cart.
*/

include_once( "../inc/inc.php" );
require( "billing-functions.php" );

$index = $_GET['index'];

billingRemoveItemFromCart( $index );

header( "Location: index.php" );
?>