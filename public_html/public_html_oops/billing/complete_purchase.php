<?
/*
Finalizes a user's pruchase by attempting to charge their credit card and adding the purchase into the database.

This script uses billing profiles to complete the purchase
*/

include( "../inc/inc.php" );
include( "billing-functions.php" );
include( "../inc/authnet/AuthorizeNet.php" );

$success = 0;

$profiles = quickQuery( "select count(*) from billing_profiles where uid='" . $API->uid . "'" );
if( $profiles == 0 )
{
  $errmsg = "You must add a payment method before submitting your purchase.";
  header( "Location: index.php?err=" . urlencode( $errmsg ) );
  exit;
}

$profile_data = queryArray( "select id, code, AN_profile_id, AN_payment_id, AN_address_id from billing_profiles where uid='" . $API->uid . "' order by `primary` desc" );

if( sizeof( $profile_data ) == 0 )
{
  $errmsg = "Cannot find payment profile.";
  header( "Location: index.php?err=" . urlencode( $errmsg ) );
  exit;
}

$addlcredits = $_POST['addlcredits'];
$pkg = $_POST['pkg'];

if( sizeof( $cart ) == 0 )
  $credit_cost = $addlcredits;
else
  $credit_cost = billingGetCreditCost( $addlcredits );

$pkg_cost = 0;

if( intval( $pkg ) > 0 )
  $pkg_cost = quickQuery( "select cost from billing_packages where id='" . $pkg . "'" );


$credits = 0;

$cart = billingGetCart();
$total = $pkg_cost + $credit_cost;

$request = new AuthorizeNetCIM('6Ssw73Esc5', '53GkA95XjF33y99k');
$request->setSandbox(false);


$transaction = new AuthorizeNetTransaction;

$transaction->customerProfileId = $profile_data['AN_profile_id'];
$transaction->customerPaymentProfileId = $profile_data['AN_payment_id'];
$transaction->customerShippingAddressId =$profile_data['AN_address_id'];
$transaction->cardCode = $profile_data['code'];

foreach( $cart as $index => $item )
{
  $type = $item['type'];
  $data = queryArray( "select `desc`, cost from billing_purchase_types where id='$type'" );
  $total += $data['cost'] * $item['qty'];

  $lineItem              = new AuthorizeNetLineItem;
  $lineItem->itemId      = $item['type'];
  $lineItem->name        = $data['desc'];
  $lineItem->description = $data['desc'];
  $lineItem->quantity    = $item['qty'];
  $lineItem->unitPrice   = $data['cost'];
  $lineItem->taxable     = "false";

  $transaction->lineItems[] = $lineItem;
}

if( $addlcredits > 0 )
{
  $lineItem              = new AuthorizeNetLineItem;
  $lineItem->itemId      = 1;
  $lineItem->name        = "Additional Credits";
  $lineItem->description = "Additional Credits";
  $lineItem->quantity    = "1";
  $lineItem->unitPrice   = $credit_cost;
  $lineItem->taxable     = "false";

  $transaction->lineItems[] = $lineItem;
}

//Take credits first, then charge the rest
$approved = false;

if( sizeof( $cart ) > 0 )
{
  $avail_credits = quickQuery( "select ad_credits from users where uid='" . $API->uid . "'" );

  if( $avail_credits < $total )
  {
    $credits = $avail_credits; //# of credits used;
    $avail_credits -= $total;
    $total = abs( $avail_credits );
    $avail_credits = 0;
  }
  else
  {
    $avail_credits -= $total;
    $credits = $total;
    $approved = true;
    $total = 0;
  }

  mysql_query( "update users set ad_credits='$avail_credits' where uid='" . $API->uid . "' limit 1" );
}


if( !$approved ) //If we didn't have enough credits..
{
  $transaction->amount = $total;
  $response = $request->createCustomerProfileTransaction("AuthCapture", $transaction, "x_email_customer=FALSE");

  if( $response->isOk() )
  {
    $transactionResponse = $response->getTransactionResponse();
    $approved = $transactionResponse->approved;
  }
}

if( $approved )
{
  $transactionId = $transactionResponse->transaction_id;

  //Need to record successful transaction here.
  $purchase = billingAddPayment( $total, $credits, $addlcredits, $transactionId, $profile_data['id'] );

  foreach( $cart as $index => $item )
    billingAddService( $item['type'], $purchase, $item['link'], $item['qty'] );

  if( $addlcredits )
  {
    mysql_query( "update users set ad_credits=ad_credits+" . intval($addlcredits) . " where uid='" . $API->uid . "' limit 1" );

    //Add payment history for the additional credits
    mysql_query( "insert into billing_purchase_history_items (payment, quantity, cost, type) VALUES ($purchase, 1, $credit_cost, 6)" );
  }

  billingClearCart();

  $success = 1;

  ob_start();
  $email = true;
  $_GET['p'] = $purchase;
  include( "receipt.php" );
  $buffer = ob_get_contents();
  ob_end_clean();

  $email = quickQuery( "select email from users where uid='" . $API->uid . "'" );

  emailAddress( $email, "$siteName Receipt", $buffer, "cr" );
}
else
{
  $errmsg = "Purchase declined: " . $response->response;
}

header( "Location: index.php?err=" . $errmsg . "&a=" . $success );


/*
AIM Method



    $amount = quickQuery( "select cost from ad_packages where id='" . $_POST['pkg'] . "'" );
    $credits = quickQuery( "select credits from ad_packages where id='" . $_POST['pkg'] . "'" );
    $country = quickQuery( "select country from loc_country where ID='" . $_POST['country'] . "'" );
    $email = quickQuery( "select email from users where uid='" . $API->uid . "'" );

    $transaction = new AuthorizeNetAIM('6Ssw73Esc5', '7dAXPKyF999y65ST');  //	665kw8pR2WabQX6h //7dAXPKyF999y65ST
    $transaction->setSandbox(false);
    $transaction->amount    = $amount;
    $transaction->first_name= $_POST['firstname'];
    $transaction->last_name = $_POST['lastname'];
    $transaction->card_num  = $_POST['cc_number'];
    $transaction->card_code = $_POST['cc_code'];
    $transaction->exp_date  = $_POST['expmo'] . "/" . $_POST['expyr'];
    $transaction->address   = $_POST['address'];
    $transaction->city      = $_POST['city'];
    $transaction->state     = $_POST['state'];
    $transaction->zip       = $_POST['zip'];
    $transaction->country   = $country;
    $transaction->email     = $email;


    $response = $transaction->authorizeAndCapture();

    if ($response->approved) {
      $amount = intval( $amount );
      sql_query( "insert into cc_transactions (uid,amount,transaction_id,day,type) values (" . $API->uid . ", '" . $amount . "', '" . $response->transaction_id . "', NOW(), 1 );" );
      sql_query( "update users set ad_credits=ad_credits+$credits where uid='" . $API->uid . "'" );
      header( "Location: billing.php?r=" . $response->transaction_id );
    } else {
      $data = explode( "Response Reason Text: ", $response->error_message );
      header( "Location: cc_response.php?r=" . urlencode( $data[ sizeof( $data ) - 1 ] ) );
    }


*/



?>