<?php
/*
Index page for the user's payment and purchase information.
*/

include "../inc/inc.php";

if( !$isDevServer ) // i.e., if this is the 'live server'...
{
  if( empty( $_SERVER['HTTPS'] ) )
  {
    header( "Location: https://www." . $siteName . ".com/billing" );
    exit;
  }
}


include "../header.php";
require( "billing-functions.php" );
include( "top_menu.php" );
?>


<div class="contentborder" >
<div style="clear:both;"></div>

  <div style="width:470px; float:left;">

    <div class="strong" style="margin-top:20px; font-size:13pt;">Payment Sources</div>

    <div style="background-color:#f3f8fb; border:1px solid #d8dfea; width:425px; padding:10px;">

      <!--<div style="color:#326798; font-size:9pt; font-weight:bold;  padding-left:5px; padding-top:5px;">Payment Sources</div>-->

  <?

  $q = mysql_query( "select * from billing_profiles where uid='" . $API->uid . "'" );
  $paymentProfiles = mysql_num_rows( $q );
  if( $paymentProfiles == 0 )
  {
  ?>
      <div style="width:90%; font-size:9pt;">
        A payment method is required before completing your order. Please <a href="javascript: void(0);" onclick="javascript:newPaymentProfile();" style="font-weight:bold;">add a payment source</a> before proceeding.<br /><br />
        Note: If you have existing credits, your card will not be charged.
      </div>
  <?
  }
  else
  {
?>
  <table style="width:420px; font-size:9pt;" border="0">
<?
    while( $r = mysql_fetch_array( $q ) )
    {
      $img = ""; $type = "";
      switch( $r['first_digit'] )
      {
        case 3: $img = "amex_30x22.png"; $type = "American Express"; break;
        case 4: $img = "visa_30x22.png"; $type = "Visa"; break;
        case 5: $img = "mc_30x22.png"; $type = "Master"; break;
        case 6: $img = "disc_30x22.png"; $type = "Discover"; break;
      }
    ?>
      <tr id="profile<?=$r['id'];?>">
        <td width="40"><?=($r['primary'] == 1 ? "primary" : ""); ?></td>
        <td><input type="radio" <?=($r['primary'] == 1 ? "CHECKED" : ""); ?> name="primary" value="<?=$r['id'];?>" onclick="if( this.checked ) { getAjax( '/billing/set_primary_profile.php?id=<?=$r['id'];?>','reloadPage' ); }"/></td>
        <td><img src="/images/<?=$img;?>" width="30" height="22"></td>
        <td><?=$type; ?> Card ending in <?=$r['last_four']; ?></td>
        <td align="right"><a href="javascript:void(0);" onclick="javascript: if( confirm( 'Are you sure you want to delete this profile?' ) ) { getAjax( '/billing/delete_profile.php?id=<?=$r['id'];?>','reloadPage' ); } ">remove</a></td>
      </tr>
    <?
    }
?>
  </table>

  <div style="font-size:9pt; margin-top:10px;">
  <a href="javascript: void(0);" onclick="javascript:newPaymentProfile();">add a payment source</a>
  </div>

  <?
  $rp = quickQuery( "select recurring_payments from users where uid='" . $API->uid . "'" );
  ?>
  <div style="clear:left; padding-left:5px; font-size:8pt; padding-top:20px;">
    <input type="checkbox" name="recurring" <?if( $rp == 1 ) {?>checked="checked"<?}?> onchange="javascript: on=(this.checked?1:0); getAjax('/billing/set_recurring_payments.php?on=' + on,'void'); "/> I agree to the <?=$siteName?> Payment Policies in the TOS.
    <a href="javascript: void(0);" onclick="javascript: window.open('/tos.php?1', 'Terms of use','location=0,status=0,scrollbars=1,width=580,height=600');"><img src="/images/help.png" width="16" height="16" alt="" /></a>
  </div>
  <?
  }
  ?>

      <div style="clear:both;"></div>

    </div>


    <div>
      <div style="float:left; text-align:center; margin-top:4px;">
        <span id="cdSiteSeal1"><script type="text/javascript" src="//tracedseals.starfieldtech.com/siteseal/get?scriptId=cdSiteSeal1&amp;cdSealType=Seal1&amp;sealId=55e4ye7y7mb736b7e9a46664d373a1daddy7mb7355e4ye7dc9c49e1411ef78cb"></script></span>
      </div>

      <div style="float:left; padding-left:4px; padding-top:4px;">
        <a href="https://www.securitymetrics.com/site_certificate.adp?s=www%2esalthub%2ecom&amp;i=1009256" target="_blank" >
        <img src="https://www.securitymetrics.com/images/sm_ccsafe_whborder.gif" alt="SecurityMetrics for PCI Compliance, QSA, IDS, Penetration Testing, Forensics, and Vulnerability Assessment" border="0"></a>
      </div>
    </div>

  </div>

  <div style="width:450px; float:left;">
    <div class="strong" style="margin-top:20px; font-size:13pt;">Coupons &amp; Gift Cards</div>

    <div style="background-color:#f3f8fb; border:1px solid #d8dfea; padding:10px; width:450px;">
      <form action="/billing/redeem_coupon.php" method="POST">

      <div style="float:left; width: 90px; color:rgb(85,85,85); font-size:9pt; font-weight:bold; padding-top:3px;">Enter code:</div>
      <div style="float:left; width: 240px; margin-left:10px;">
        <!--<input name="code" type="text" style="width:180px;"/>-->
        <input name="code1" size="4"/>
        <input name="code2" size="4"/>
        <input name="code3" size="4"/>
        <input name="code4" size="4"/>
      </div>
      <div style="float:right; margin-right:25px; text-align:center;">
        <input type="submit" name="Apply" value="Apply" class="button"/>
      </div>

      </form>

      <div style="clear:both;"></div>
    </div>

<?
    $cart = billingGetCart();
    if( sizeof( $cart ) == 0 ) //There are no items in our shopping cart
    {
?>

    <div class="strong" style="margin-top:20px; font-size:13pt;">Purchase Credits</div>

    <div style="background-color:#f3f8fb; border:1px solid #d8dfea; padding:10px; width:450px;">
      <form action="/billing/complete_purchase.php" method="POST" onsubmit="javascript: <?if( $paymentProfiles == 0 ) { ?>paymentProfileRequired(); return false; <? } ?>value = parseInt( document.getElementById('credits').value ); if( value == 'NaN' || value=='0' ) { alert('Please enter a number of credits before continuing.'); return false;}">

      <div style="float:left; color:rgb(85,85,85);font-size:9pt; font-weight:bold; padding-top:3px;">Additional Credits:</div>
      <div style="float:left; margin-left:10px;">
        <!--<input id="credits" name="addlcredits" type="text" style="width:80px;" onkeyup="javascript: creditChanged(this);" value="0"/>-->
        <select name="addlcredits" id="addlcredits" onchange="javascript:creditChanged(this);">
          <option value="0">0</option>
          <option value="100">100</option>
          <option value="200">200</option>
          <option value="500">500</option>
          <option value="750">750</option>
          <option value="1000">1000</option>
          <option value="2000">2000</option>
          <option value="3000">3000</option>
          <option value="4000">4000</option>
        <? for( $c = 5000; $c <= 10000; $c+=1000 ) { echo '<option value="' . $c . '">' . number_format( $c ) . '</option>'; } ?>
        </select>
      </div>

      <div style="float:right; margin-right:25px; text-align:center;">
        <input type="submit" name="Apply" value="Apply" class="button"/>
      </div>

      </form>

      <div style="clear:both; font-size:8pt; color:#555;">Selecting apply will complete your purchase</div>

      <div style="clear:both; width: 450px; padding-top:10px; font-weight:bold;">
        <div style="float:left; color:rgb(85,85,85); font-size:9pt;">Total Purchase:</div>
        <div style="margin-right:25px; font-size:9pt; font-weight:bold; float:right; font-weight:bold;">$<span id="total_purchases">0.00</span></div>
      </div>

      <div style="clear:both; width: 450px; padding-top:10px;  font-weight:bold;">
        <div style="float:left; color:rgb(85,85,85); font-size:12pt;">Credits before transaction:</div>
        <div style="padding-right:25px; font-size:12pt; float:right;"><? echo number_format( intval( quickQuery( "select ad_credits from users where uid='" . $API->uid . "'" ) ),2 ); ?></div>
      </div>


      <div style="clear:both; width:450px; padding-top:10px;  font-weight:bold;">
        <div style="float:left; color:rgb(85,85,85); font-size:12pt;">Credits after payment:</div>
        <div style="padding-right:25px; font-size:12pt; float:right;"><span id="total_credits"><? echo number_format( intval( quickQuery( "select ad_credits from users where uid='" . $API->uid . "'" ) ),2 ); ?></span></div>
        <div style="clear:both;">
      </div>

      <div style="clear:both;"></div>
    </div>
<?
}
else
{
?>
  <form action="/billing/complete_purchase.php" method="POST" onsubmit="<?if( $paymentProfiles == 0 ) { ?>paymentProfileRequired(); return false; <? } ?>">

  <div class="strong" style="margin-top:20px; font-size:13pt;">Your Order</div>

  <div style="background-color:#f3f8fb; border:1px solid #d8dfea; padding:10px; width:450px;">
    <div class="billing_header" style="font-size:10pt; margin-bottom:10px;">Order Information</div>

<?
$total = 0;
foreach( $cart as $index => $item ) {
  $type = $item['type'];

  $data = queryArray( "select `desc`, cost from billing_purchase_types where id='$type'" );
  echo mysql_error();

  $total += $data['cost'] * $item['qty'];
?>
    <div class="order_itemhead"><?=$data['desc'] ?>
    <?
    if( $item['type'] == 1 ) echo ' ("' . quickQuery( "select name from ad_campaigns where id='" . $item['link'] . "'" ) . '") ';
    else if( $item['type'] == 2 ) echo ' ("' . quickQuery( "select title from jobs where id='" . $item['link'] . "'" ) . '") ';
    if( $item['qty'] > 1 ) { echo ' (Qty: ' . $item['qty'] . ')'; } ?>
    <a href="cart_remove.php?index=<?=$index?>">remove</a>
    </div>
    <div class="order_item" style="text-align:right;">$<?= number_format( $data['cost'] * $item['qty'],2 ); ?></div>
<?
}
?>
    <div class="order_itemhead">Required credits</div>
    <div class="order_item" style="text-align:right;">$<?= number_format( $total, 2 ); ?></div>

    <div style="clear:both; height:1px; padding-top:10px; margin-bottom:10px; border-bottom: 1px dotted #326798; width:100%;"></div>

    <div class="billing_header" style="font-size:10pt; margin-bottom:10px;">Credits &amp; Specials</div>

    <div class="order_itemhead" style="width:320px;">Purchase discounted credits:
      <select name="addlcredits" id="addlcredits" onchange="javascript:recalc();">
        <option value="0">0</option>
        <option value="100">100 - 10% Off</option>
        <option value="200">200 - 10% Off</option>
        <option value="500">500 - 10% Off</option>
        <option value="750">750 - 10% Off</option>
        <option value="1000">1000 - 20% Off</option>
        <option value="2000">2000 - 20% Off</option>
        <option value="3000">3000 - 20% Off</option>
        <option value="4000">4000 - 20% Off</option>
      <? for( $c = 5000; $c <= 10000; $c+=1000 ) { echo '<option value="' . $c . '">' . number_format( $c ) . ' - 30% Off</option>'; } ?>
      </select>

      <a href="javascript: void(0);" onclick="javascript: showPopUp('Note:', '<br /><br />The purchase of discounted credits will not<br />be applied toward the existing transaction.<br /><br /><br />');"><img src="/images/help.png" width="16" height="16" alt="" /></a>

    </div>
    <div class="order_item" style="text-align:right;">$<span id="addlcredits_cost">0.00</span></div>

    <div class="order_itemhead" style="width:320px;">Package:
      <select name="pkg" id="pkg" onchange="javascript:recalc();" style="width:225px;" disabled="disabled">
      <option value="0">-</option>
      <?
      $q2 = mysql_query( "select * from billing_packages order by name" );
      while( $r2 = mysql_fetch_array( $q2 ) )
      {
      ?>
        <option value="<? echo $r2['id']; ?>"><? echo $r2['name']; ?></option>
      <?
      }
      ?>
      </select>
    </div>

    <div class="order_item" style="text-align:right;">$<span id="pkg_cost">0.00</span></div>

    <div style="clear:both; height:1px; padding-top:10px; margin-bottom:10px; border-bottom: 1px dotted #326798; width:100%;"></div>

    <div class="order_itemhead">Total Purchases:</div>
    <div class="order_item" style="text-align:right;">$<span id="total_purchases">0.00</span></div>

    <div class="order_itemhead">Sales Tax:</div>
    <div class="order_item" style="text-align:right;">--</div>

    <div style="clear:both; height:1px; padding-top:10px; margin-bottom:10px; border-bottom: 1px dotted #326798; width:100%;"></div>

    <div class="order_itemhead" style="color:#000;">Total:</div>
    <div class="order_item" style="text-align:right; color:#000;">$<span id="total_cost">0.00</span></div>

    <div style="clear:both; width: 450px; padding-top:10px;  font-weight:bold;">
      <div style="float:left; color:rgb(85,85,85); font-size:12pt; padding-left:10px;">Credits before transaction:</div>
      <div style="padding-right:10px; font-size:12pt; float:right;"><? echo number_format( intval( quickQuery( "select ad_credits from users where uid='" . $API->uid . "'" ) ),2 ); ?></div>
    </div>

    <div style="clear:both; width: 450px; padding-top:10px;  font-weight:bold;">
      <div style="float:left; color:rgb(85,85,85); font-size:12pt; padding-left:10px;">Credits after transaction:
      </div>
      <div style="padding-right:10px; font-size:12pt; float:right;"><span id="total_credits2"><? echo number_format( intval( quickQuery( "select ad_credits from users where uid='" . $API->uid . "'" ) ),2 ); ?></span>

      </div>
    </div>

    <div style="clear:both;"></div>

  </div>

  <div style="clear:both;"></div>

  <div style="text-align:center; margin-top:20px; margin-bottom:20px;">
    <input type="submit" name="" value="Complete Order" class="button" style="padding:10px;"/>
  </div>

  </form>
<?
}
?>
  </div>


  <div style="clear:both;"></div>
</div>
  <div style="clear:both;"></div>

</div>


<script type="text/javascript">
<!--
var initialCredits  = <?=intval( quickQuery( "select ad_credits from users where uid='" . $API->uid . "'" ) );?>;
var valid           = 1;  //=0
var initialCost     = <?=number_format( $total, 2 )?>;

var packageCredits = new Array();
var packageCost = new Array();

<?
$q2 = mysql_query( "select * from billing_packages order by name" );
while( $r2 = mysql_fetch_array( $q2 ) )
{
?>
packageCredits[<? echo $r2['id'] ?>] = <? echo $r2['credits']; ?>;
packageCost[<? echo $r2['id'] ?>] = <? echo $r2['cost']; ?>;
<?
}
?>

function getCreditCost( value )
{
  if( value <= 99 ) purchase = value;
  else if( value <= 999 ) purchase = Math.round(value * .9);
  else if( value <= 4999 ) purchase = Math.round(value * .8);
  else purchase = Math.round(value * .7);
  return purchase;
}

function creditChanged(e)
{
  purchase = 0;
  value = parseInt( selectedValue(e) );

  if( e.value == "" )
  {
    value = 0.00;
  }
  else if( value == "NaN" )
  {
    e.value = 0;
    value = 0;
  }

  total_credits = document.getElementById( "total_credits" );
  if( !total_credits ) return;
  total_purchases = document.getElementById( "total_purchases" );
  if( !total_purchases ) return;

  total_credits.innerHTML = addCommas( (initialCredits + value).toFixed(2) );

  //purchase = getCreditCost( value );
  purchase = value;

  total_purchases.innerHTML = addCommas( purchase.toFixed(2) );
}



function recalc()
{
  totalCost = 0;

  var creditCost = new Number();

  //Retrieve fields
  addlcredits = document.getElementById( "addlcredits" );
  if( !addlcredits ) return;
  addlcredits_cost = document.getElementById( "addlcredits_cost" );
  if( !addlcredits_cost ) return;
  pkg = document.getElementById( "pkg" );
  if( !pkg ) return;
  pkg_cost = document.getElementById( "pkg_cost" );
  if( !pkg_cost ) return;

  total_cost = document.getElementById( "total_cost" );
  if( !total_cost ) return;
  total_credits2 = document.getElementById( "total_credits2" );
  if( !total_credits2 ) return;
  total_purchases = document.getElementById( "total_purchases" );
  if( !total_purchases ) return;

  //Initial Credits Calculation
  totalCredits2 = initialCredits;
  totalCost = initialCost;

  //Addl Credits Calculation
  credits = parseInt( selectedValue( addlcredits ) );
  creditCost = getCreditCost( credits );
  addlcredits_cost.innerHTML = addCommas( creditCost.toFixed(2) );
  totalCredits2  += credits;
  totalCost     += creditCost;

  //Pkg Credits Calculation
  credits = 0;
  creditCost = 0;
  pkgid = selectedValue( pkg );
  if( pkgid > 0 )
  {
    credits = packageCredits[pkgid];
    creditCost = packageCost[pkgid];
  }
  pkg_cost.innerHTML = addCommas( creditCost.toFixed(2) );

  totalCredits2  += credits;
  totalCost     += creditCost;

  totalCredits2 -=totalCost;

  total_cost.innerHTML = addCommas( totalCost.toFixed(2) ) ;
  total_credits2.innerHTML = addCommas(totalCredits2.toFixed(2) );
  total_purchases.innerHTML = addCommas(totalCost.toFixed(2) );
}

recalc();

function paymentProfileRequired()
{
  showPopUp( '', '<div style="text-align:center;">A payment method is required before completing your order.<br /> Please add a payment source before proceeding.</div>' );
}


function newPaymentProfile()
{
   showPopupUrl( "/billing/new_profile_popup.php?a" );
}

function reloadPage() {  document.location.reload(); };

<? if( isset( $_GET['err'] )  ) {
  if( $_GET['err'] != "" ) {
    switch( $_GET['err'] )
    {
      case "0":
  ?>
    showPopUp( 'Thank you', '<br /><div style="text-align:center; width:400px;">The additional credits have been applied to<br /> your account and are available for immediate use.</div><br /><br />' );
  <?
      break;
      case "1":
  ?>
    showPopUp( 'Coupon Redemption Error', '<br />Sorry, the coupon code that you\'re attempting to claim is not valid.<br /><br />' );
  <?
      break;

      default:
  ?>
        showPopUp( 'Error', '<br /><?=$_GET['err'];?><br /><br />' );
  <?
      break;
   }
  }
} else if( intval( $_GET['a'] ) == 1 ) { ?>
   showPopUp( 'Your purchase has been approved', '<div style=\"text-align:center; width:400px; font-side:9pt;\"><br />Thank you for your purchase.<br /><br />-The <?=$siteName?> Team<br /><br /></div>' );
<? } ?>
-->
</script>
<?
include $_SERVER["DOCUMENT_ROOT"] . "/footer.php";
?>