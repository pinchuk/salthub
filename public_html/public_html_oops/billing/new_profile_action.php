<?
/*
Adds a new billing profile to a user's account.  The profile can be used for recurring payments or one-time payments, and is required to make a purchase.
*/

include_once( "../inc/inc.php" );
include( "../inc/authnet/AuthorizeNet.php" );

error_reporting(E_ERROR | E_WARNING | E_PARSE);

$first_name= $_POST['firstname'];
$last_name = $_POST['lastname'];
$card_num  = $_POST['cc_number'];
$card_code = $_POST['cc_code'];
$exp_date  = $_POST['expyr'] . "-" . $_POST['expmo'];
$address   = $_POST['address'];
$city      = $_POST['city'];
$state     = $_POST['state'];
$zip       = $_POST['zip'];
$profile   = $_POST['descr'];
$errmsg = "";

$last_four = substr( $card_num, -4 );
$first_digit = substr( $card_num, 0, 1 );
$first_name = addslashes( $first_name );
$last_name = addslashes( $last_name );
$address = addslashes( $address );
$city = addslashes( $city );
$zip = addslashes( $zip );
$state = $_POST['state'];
$country = $_POST['country'];

$sql = "insert into billing_profiles (uid, firstname, lastname, address,city,state,zip,country,last_four,first_digit,profile,code,`primary`) values ('" . $API->uid . "', '$first_name', '$last_name', '$address', '$city', '$state', '$zip', '$country', '$last_four', '$first_digit', '$profile', '$card_code', 1)";
mysql_query( $sql );

if( mysql_error() )
{
  $errmsg = mysql_error();
  //echo "<br /><br />$sql";
}
else
{
  $profile_id = mysql_insert_id();

  //Create new customer profile
  $country = quickQuery( "select country from loc_country where id='" . $_POST['country'] . "'" );
  $email = quickQuery( "select email from users where uid='" . $API->uid . "'" );

  $first_name= $_POST['firstname'];
  $last_name = $_POST['lastname'];
  $card_num  = $_POST['cc_number'];
  $card_code = $_POST['cc_code'];
  $exp_date  = $_POST['expyr'] . "-" . $_POST['expmo'];
  $address   = $_POST['address'];
  $city      = $_POST['city'];
  $state     = $_POST['state'];
  $zip       = $_POST['zip'];

  // Create new customer profile
  $request = new AuthorizeNetCIM('6Ssw73Esc5', '53GkA95XjF33y99k');
  $request->setSandbox(false);

  //Create a new customer profile
  $profile = new AuthorizeNetCustomer;
  $profile->description = $_POST['descr'];
  $profile->merchantCustomerId = $profile_id;
  $profile->email = $email;

  $response = $request->createCustomerProfile($profile);


  if( !$response->isOk() )
  {
    $errmsg = "CIM Error: Error creating customer profile. " . $response->response;
  }
  else
  {
    $AN_profile_id = $response->getCustomerProfileId();

    // Add payment profile.
    $paymentProfile = new AuthorizeNetPaymentProfile;
    $paymentProfile->customerType = "individual";
    $paymentProfile->payment->creditCard->cardNumber = $card_num;
    $paymentProfile->payment->creditCard->expirationDate = $exp_date;
    $paymentProfile->billTo->firstName = $first_name;
    $paymentProfile->billTo->lastName = $last_name;
    $paymentProfile->billTo->address = $address;
    $paymentProfile->billTo->city = $city;
    $paymentProfile->billTo->state = $state;
    $paymentProfile->billTo->zip = $zip;
    $paymentProfile->billTo->country = $country;

    $response = $request->createCustomerPaymentProfile($AN_profile_id, $paymentProfile);

    if( !$response->isOk() )
    {
      $errmsg = "CIM Error: Error creating payment profile. " . $response->response;
    }
    else
    {
      $AN_payment_id = $response->getPaymentProfileId();

      // Add shipping address.
      $addresso = new AuthorizeNetAddress;
      $addresso->firstName = $first_name;
      $addresso->lastName = $last_name;
      $addresso->address = $address;
      $addresso->city = $city;
      $addresso->state = $state;
      $addresso->zip = $zip;
      $addresso->country = $country;
      $response = $request->createCustomerShippingAddress($AN_profile_id, $addresso);

      if( !$response->isOk() )
      {
        $errmsg = "CIM Error: Error creating address profile " . $response->response;
      }
      else
      {
        $AN_address_id = $response->getCustomerAddressId();

        $response = $request->validateCustomerPaymentProfile( $AN_profile_id, $AN_payment_id, $AN_address_id, $card_code, "liveMode" );

        $responseData = explode( ",", $response->xml->directResponse );

        if( $responseData[0] != 1 )
        {
          $errmsg = "CIM Error: Unable to validate credit card. Reason: " . $responseData[3];
        }
      }
    }
  }

  if( strlen( $errmsg ) > 0 )
  {
    mysql_query( "delete from billing_profiles where id='$profile_id' limit 1" );

    $request->deleteCustomerProfile( $AN_profile_id );
  }
  else
  {
    mysql_query( "update billing_profiles set AN_profile_id='" . $AN_profile_id . "', AN_payment_id='" . $AN_payment_id . "', AN_address_id='" . $AN_address_id . "' where id='$profile_id' limit 1" );
    $errmsg = mysql_error();

    mysql_query( "update billing_profiles set primary=0 where id!='$profile_id' and uid='" . $API->uid . "'" );

  }

}

header( "Location: /billing/index.php?err=" . urlencode($errmsg) );
/*
if( strlen( $errmsg ) > 0 )
  echo $errmsg;
else
  echo "OK";
*/
mysql_close();

?>