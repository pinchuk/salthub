<?
/*
Redeems a coupon by adding a record of the redemption and increasing the credits available to the user - based on the promotion (entered through /admin).
*/

require( "../inc/inc.php" );
require( "billing-functions.php" );

$code = $_POST['code1'] . $_POST['code2'] . $_POST['code3'] . $_POST['code4'];

$err = "0";
if( !billingRedeemCoupon( $code ) )
{
  $err = "1";
}

header( "Location: index.php?err=$err" );
?>