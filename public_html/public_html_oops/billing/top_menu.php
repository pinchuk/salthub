<!-- Top menu for the billing section -->
<div class="bigtext" style="color: #555; margin-bottom:-23px;">
	<div class="contentleft piconly">
		<div class="profilepic_container">
			<div class="profilepic" style="text-align: center;">
				<div class="borderhider">&nbsp;</div><div class="borderhider2">&nbsp;</div>
				<a href="<?=$API->getProfileURL()?>"><img src="<?=$API->getThumbURL(1, 48, 48, $API->getUserPic())?>" alt="" /></a>
			</div>
		</div>
	</div>

  <?
	$user = array("uid" => $API->uid, "username" => $API->username, "name" => $API->name);

	$items = array(
			array("/images/creditcards.png", "billing", "/billing/index.php"),
			array("/images/money.png", "account history", "/billing/history.php"),
			array("/images/application_view_tile.png", "ad campaigns", "/adv"),
			array("/images/page.png", "pages", "/pages"),
			array("/images/application_view_tile.png", "my job listings", "/employment/view_my_jobs.php")
		);

	showUserWithItems($user, $items );
  ?>
</div>