<?php

$fromEmail = true;
$_SERVER["DOCUMENT_ROOT"] = getcwd();
include "inc/inc.php";

include "upload/video.php";
include "upload/photo.php";
include "upload/newalbum.php";

$mbox = imap_open("{vidbirdy.netbrix.net:143/imap/novalidate-cert}INBOX", "gallery.vidbirdy", "odom05") or die("no");
   
if ($hdr = imap_check($mbox))
	$msgCount = $hdr->Nmsgs;
else
	die();

$overview = imap_fetch_overview($mbox, "1:$msgCount", 0);
$size = sizeof($overview);

for ($j = $size - 1; $j >= 0; $j--)
{
	$val = $overview[$j];
	if ($val->deleted == 1) continue; //msg has been flagged for deletion thus already processed
	
	$msgno = $val->msgno;
	$subject = htmlentities(addslashes($val->subject));
	
	echo "Subject: $subject\n";
	
	if (empty($subject))
	{
		imap_delete($mbox, $msgno);
		continue; // they're too lazy to put a title, we're too lazy to read the e-mail
	}
	
	$to = ereg_replace("[^A-Za-z0-9]", "", current(explode("@", $val->to)));
	echo "To: $to\n";
	$API->uid = quickQuery("select uid from users where active=1 and phone='" . addslashes($to) . "'");
	
	if (empty($API->uid)) // we don't know who this is, too bad!
	{
		imap_delete($mbox, $msgno);
		continue; // delete the message and move to the next
	}
	
	$body = trim(get_part($mbox, $msgno, "TEXT/PLAIN"));
	if (empty($body))
		$body = trim(strip_tags(get_part($mbox, $msgno, "TEXT/HTML")));
	
	if (empty($body))
	{
		imap_delete($mbox, $msgno);
		continue; //cya
	}
	
	$body = htmlentities(addslashes($body));
	
	unset($files);
	unset($att);
	
	$struct = imap_fetchstructure($mbox, $msgno);
	$contentParts = count($struct->parts);
	
	if ($contentParts >= 2)
	{
		for ($i=2;$i<=$contentParts;$i++)
			$att[$i-2] = imap_bodystruct($mbox,$msgno,$i);
		
		for ($k=0;$k<sizeof($att);$k++)
		{
			if ($att[$k]->parameters[0]->value == "us-ascii" || $att[$k]->parameters[0]->value    == "US-ASCII") {
				if ($att[$k]->parameters[1]->value != "") {
					$files[$k] = $att[$k]->parameters[1]->value;
				}
			} elseif ($att[$k]->parameters[0]->value != "iso-8859-1" &&    $att[$k]->parameters[0]->value != "ISO-8859-1") {
				$files[$k] = $att[$k]->parameters[0]->value;
			}
		}
	}
	
	if (is_array($files))
	{
		for ($f = 0; $f < count($files); $f++)
		{
			$ext = strtolower(ereg_replace("[^A-Za-z0-9]", "", end(explode(".", $files[$f])))); //get file extension and make sure there aren't any surprises in there
			$hash = md5(microtime());
			$source = "/tmp/$hash.$ext";
			$l = fopen($source, "w");
			fwrite($l, imap_base64(imap_fetchbody($mbox, $msgno, $contentParts - $f)));
			fclose($l);
			
			$info = getimagesize($source);
			if ($info['mime']) //it's an image!!
			{
				$id = processPhoto();
				if ($id > 0)
					$photoIds[$API->uid][$msgno][$id] = array("title" => $subject, "descr" => $body); //store the info - we will be making an album later
			}
			else //video or just rubbish - we'll let the video script decide for us
			{
				// rename the file so the script recognizes it
				$target = "/tmp/vidupload-$hash";
				rename($source, $target);
				$source = $target;
				
				$id = processVideo();
				if ($id > 0)
					sql_query("update videos set title='$subject',descr='$body' where id=$id");
			}
		}
	}
	
	imap_delete($mbox, $msgno);
}

imap_close($mbox);

if (is_array($photoIds))
	foreach ($photoIds as $uid => $albums)
	{
		$API->uid = $uid;
		
		foreach ($albums as $album => $photos)
		{
			$_POST = array(); // POST will contain the album information for the /upload/newalbum.php script
			
			foreach ($photos as $id => $photo)
			{
				if (empty($_POST['m'])) //set the first image as the mainimage
					$_POST['m'] = $id;
				
				$_POST['title'] = $photo['title'];
				$_POST['descr'] = $photo['descr'];
				$_POST['ids'] .= "," . $id;
			}
			
			$_POST['ids'] = substr($_POST['ids'], 1); //get rid of the comma in the front
			
			//create the album
			createAlbum();
		}
	}

// i didn't write these functions
function get_mime_type(&$structure) {
$primary_mime_type = array("TEXT", "MULTIPART","MESSAGE", "APPLICATION", "AUDIO","IMAGE", "VIDEO", "OTHER");
if($structure->subtype) {
return $primary_mime_type[(int) $structure->type] . '/' .$structure->subtype;
}
return "TEXT/PLAIN";
}
function get_part($stream, $msg_number, $mime_type, $structure = false,$part_number    = false) {

if(!$structure) {
	$structure = imap_fetchstructure($stream, $msg_number);
}
if($structure) {
	if($mime_type == get_mime_type($structure)) {
		if(!$part_number) {
			$part_number = "1";
		}
		$text = imap_fetchbody($stream, $msg_number, $part_number);
		if($structure->encoding == 3) {
			return imap_base64($text);
		} else if($structure->encoding == 4) {
			return imap_qprint($text);
		} else {
		return $text;
	}
}

	if($structure->type == 1) /* multipart */ {
	while(list($index, $sub_structure) = each($structure->parts)) {
		if($part_number) {
			$prefix = $part_number . '.';
		}
		$data = get_part($stream, $msg_number, $mime_type, $sub_structure,$prefix .    ($index + 1));
		if($data) {
			return $data;
		}
	} // END OF WHILE
	} // END OF MULTIPART
} // END OF STRUTURE
return false;
} // END OF FUNCTION

?>
