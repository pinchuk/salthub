var isDevServer = false;

//////////new ajax magic//////////

function plural(n, sing, plur, hideNumber)
{
	if (typeof plur == "undefined")
		plur = sing + "s";
	
	ret = '';
	
	if (!hideNumber)
		ret += n + ' ';
	
	if (n == 1)
		ret += sing;
	else
		ret += plur;
	
	return ret;
}

/////////////////////likes////////////////////////////

function setLike(type, link, like)
{
	$.post('/ajax/set_like.php', { link: link, type: type, like: like })
	.success(
		function (json)
		{
			updateLikes(json);
		}
	);
}

function removeUsersWhoLikePopup()
{
	clearInterval(tUsersWhoLikePopup);
	$('.users_that_like_popup').remove();
}

var tUsersWhoLikePopup = 0;

function add_users_who_like_handlers()
{
	$('.mod_comments').each(
		function ()
		{
			$(this).find('.like_buttons').mouseleave(
				function ()
				{
					clearTimeout(tUsersWhoLikePopup);
					tUsersWhoLikePopup = setTimeout("removeUsersWhoLikePopup();", 1000);
				}
			).mouseover(
				function (e)
				{
					showUsersWhoLike($(this).closest('.mod_comments').find('.show_more'), e);
				}
			);
		}
	);
}

function showUsersWhoLike(el, e)
{
	removeUsersWhoLikePopup();
	
	like = $(el).parent().hasClass('likes');
	
	json = json_decode($(el).parent().parent().attr('data-json'));
	
	users = like ? json.likes : json.dislikes;
	
	html  = '<ul class="users_that_like_popup">';
	
	limit = 5;
	for (i = 0; i < users.length && i < limit; i++)
		html += '<li><a href="/user/' + users[i].username + '"><img src="' + users[i].container_url + '/' + users[i].hash + '_square.jpg"></a></li>';
	
	html += '</ul>';
	
	offset = $(el).closest('.mod_comments').find('.like_buttons').offset();
	$(html).css('top', offset.top - 35).css('left', offset.left).appendTo($('body'));
	
	$('.users_that_like_popup').mouseenter(
		function ()
		{
			clearTimeout(tUsersWhoLikePopup);
		}
	);
	
	$('.users_that_like_popup').mouseleave(
		function ()
		{
			clearTimeout(tUsersWhoLikePopup);
			tUsersWhoLikePopup = setTimeout("removeUsersWhoLikePopup();", 1000);
		}
	);
}

function updateLikes(jsons)
{
	for (i in jsons)
	{
		json = jsons[i];
		
		html = '';
		
		for (i = 0; i < 2; i++)
			if ((i == 0 ? json.nLikes : json.nDislikes) > 0)
			{
				html += '<div class="' + (i == 0 ? '' : 'dis') + 'likes">';
				
				if (json.this_user_likes == (i == 0 ? 1 : 0))
				{
					html += 'You';
					if ((i == 0 ? json.nLikes : json.nDislikes) > 1)
						html += ' and <span class="show_more">' + plural((i == 0 ? json.nLikes : json.nDislikes) - 1, 'other') + '</span>';
				}
				else
					html += '<span class="show_more">' + plural((i == 0 ? json.nLikes : json.nDislikes), 'person', 'people') + '</span>';
				
				html += ' ';
				if (i == 1) html += 'dis';
				
				if (json.this_user_likes == (i == 0 ? 1 : 0) && (i == 0 ? json.nLikes : json.nDislikes) == 1)
					html += 'like';
				else
					html += plural((i == 0 ? json.nLikes : json.nDislikes), 'likes', 'like', true);
					
				html += ' this.</div>';
				
				if (json.this_user_likes == (i == 0 ? 1 : 0))
					html += '<div class="remove"><a href="javascript:void(0);">Unlike</a></div>';
			}
		
		html += '<div style="clear: both;"></div>';
		
		el = $('.users_that_like[data-type="' + json.type + '"][data-link="' + json.link + '"]');
		
		$(el).attr('data-json', json_encode(json));
		
		if (json.nLikes + json.nDislikes > 0)
			$(el).html(html).slideDown();
		else
			$(el).slideUp();
		
		$(el).find('.remove a').click(
			function ()
			{
				json = json_decode($(this).parent().parent().attr('data-json'));
				setLike(json.type, json.link, -1);
			}
		);
		
		add_users_who_like_handlers();
		
		/*$(el).find('a.show_more').click(
			function ()
			{
				like = $(this).parent().hasClass('likes');
				json = json_decode($(this).parent().parent().attr('data-json'));
				
				users = like ? json.likes : json.dislikes;
				
				console.log(users);
				
					html  = '<div class="bpopup users_that_like_modal">';
					html += '	<div class="content">';
					html += '		<div class="subhead">People Who Like This</div>';
					html += '		<ul class="users">';
				for (i in users)
				{
					html += '			<li>';
					html += '				<img src="' + users[i].container_url + '/' + users[i].hash + '_square.jpg">';
					html += '				<div class="userinfo">';
					html += '					' + users[i].name + '<br>';
					html += '					' + users[i].company;
					html += '				</div>';
					html += '			</li>';
				}
					html += '		</ul>';
					html += '	<div style="clear: both;"></div>';
					html += '	</div>';
					html += '</div>';
				$(html).bPopup();
				
			}
		);*/
	}
}

function compose_from_log(el_logentry, opts)
{
	user_info = json_decode($(el_logentry).attr('data-actor'));
	opts.uid = user_info.uid;
	
	html  = '<div class="bpopup compose_popup" style="width: 600px; height: 343px;">';
	html += '	<div class="contents">';
	html += '		<div class="subhead">Compose Message</div>';
	html += '		<label><span class="text">To:</span><span class="name">' + user_info.name + '</label>';
	html += '		<label><span class="text">Subject:</span><input type="text" name="subject"></label>';
	html += '		<label><span class="text">Message:</span><textarea name="message"></textarea></label>';
	html += '		<div class="buttons"><input class="button bClose" type="button" value="Cancel"><input class="button bClose send" type="button" value="Send"></div>';
	html += '	</div>';
	html += '</div>';
	
	$('.compose_popup').remove();
	$(html).bPopup({ modalClose: false });
	
	$('.compose_popup .send').click(
		function ()
		{
			opts.subject = $(this).parent().parent().find('input[name="subject"]').val();
			opts.body = $(this).parent().parent().find('textarea').val();
			
			$.ajax({
				type: 'POST',
				url: '/ajax/send_message.php',
				data: opts,
				success: function (json)
				{
					if (json.error)
						alert(json.error);
					
					$('.compose_popup').remove();
				},
				error: function ()
				{
					alert('Some error occurred.');
				},
				complete: function ()
				{
					$('.compose_popup').remove();
				}
			});
		}
	);
}

$('.users_that_like').ready(
	function ()
	{
		getAllLikes();
	}
);

function getAllLikes()
{
	var items = {};
	i = 0;
	
	$('.users_that_like').each(
		function ()
		{
			items[i++] = { link: $(this).attr('data-link'), type: $(this).attr('data-type') };
		}
	);
	
	$.post('/ajax/get_likes.php', { limit: 5, items: json_encode(items) })
		.success(
			function (json)
			{
				updateLikes(json);
			})
		.error(
			function (e)
			{
				alert('Some error occurred.');
			});
}

//////////end ajax magic//////////


var frE, frC, addToLogPending, pagesToAdd;

function showHeaderNotifications()
{
	document.getElementById("notifications_container").style.display = "inline";
	
	getAjax("/messaging/notifications.php?style=2", function (html)
	{
		document.getElementById("headnotifications").innerHTML = html;
	});
}

function gymlShuffle()
{
	getAjax("/pages/previews.php?o=r&limit=4", function(js)
	{
		eval('data = ' + js);
		document.getElementById("gyml").innerHTML = data.html;
	}
	);
}

function logMouseOver(f, over)
{
	e = document.getElementById("user-" + f + "-edit");
	
	if (e)
		e.style.visibility = over ? "visible" : "hidden";
}

function showIfExists(id, show)
{
	e = document.getElementById(id);
	
	if (e)
		e.style.display = show ? "inline" : "none";
}

function shareInPage(type, id, hash)
{
	getAjax("/pages/share.php?type=" + type + "&id=" + id + "&hash=" + hash, shareInPageHandler);
}

var gids = null;
function shareInPageHandler(x)
{
  data = "";

  if( x != "" )
  {
    chunks = x.split(String.fromCharCode(1));
    html = chunks[0];
    gids = chunks[1].split(",");
  }

	showPopUp( '', html, [515, 487], 'no');
}

function searchShareInPage(value)
{
/*
	q = q.toLowerCase();

	for (var i in pagesToAdd)
	{
		e = document.getElementById("gp-" + pagesToAdd[i].gid);
		if (e)
			e.style.display = pagesToAdd[i].gname.toLowerCase().indexOf(q) == -1 ? "none" : "";
	}
*/
  if( !gids ) return;

  value = value.toUpperCase();

  for( i = 0; i < gids.length; i++ )
  {
    parent = document.getElementById( "media-" + gids[i] );
    searchData = document.getElementById( "media-search-" + gids[i] );

    if( parent && searchData )
    {
      var term = searchData.innerHTML.toUpperCase();
      if( value == null || term.indexOf( value ) >= 0 )
        parent.style.display='';
      else
        parent.style.display='none';
    }
  }
}
/*
function sortPagesToAdd(type, id, sort)
{
	getAjax("/pages/previews.php?o=m&type=" + type + "&id=" + id + "&sort=" + sort, sortPagesToAddHandler);
}

function sortPagesToAddHandler(x)
{
	eval('data = ' + x + ';');
	document.getElementById("pagestoadd").innerHTML = data.html;
	pagesToAdd = data.pages;
}
*/

function addPageExistingMedia(type, id, gid, remove)
{
//	e = document.getElementById("addlink-" + gid + type + id);

  if( remove ) remove = '1';
  else remove = '0';

	postAjax("/pages/addpagemedia.php", "r=" + remove + "&type=" + type + "&id=" + id + "&gid=" + gid, "addPageExistingMediaHandler");
}

function addPageExistingMediaHandler(x)
{
//  alert( x );
//	eval('data = ' + x + ';');
//	document.getElementById("addlink-" + data.id).innerHTML = data.text;

  if (typeof showNewLogEntries == "function")
  {
    showNewLogEntries();
  }
}

function addToLog(type, id, newCaption, e)
{
	if (e.innerHTML == newCaption) return;

	addToLogPending = [e, newCaption];

	postAjax("/profile/addtolog.php", "type=" + type + "&id=" + id, "addToLogHandler");
}

var joinPageTab = 0;

function joinPageChangeTab( tab )
{
  for( i = 0; i < 4; i++ )
  {
    e = document.getElementById('jg' + i );
    if( e )
      e.style.display='none';

    e = document.getElementById('jt' + i );
    if( e )
      e.style.fontWeight='';
  }

  e = document.getElementById('jg' + tab );
  if( e )
    e.style.display='';

  e = document.getElementById('jt' + tab );
  if( e )
    e.style.fontWeight='bold';

  joinPageTab = tab;
}

var occupation;
var employer;
var defaultJoinPageTab;

function joinPage(gid, hash, type, defaultTab)
{

  if( defaultTab == undefined )
    defaultJoinPageTab = 0;
  else
    defaultJoinPageTab = defaultTab;

  showPopupUrl( '/pages/join_page_popup.php?gid=' + gid, joinPageLoad );
}

function joinPageLoad()
{
  occupation = new actb(document.getElementById("occupation"), "occupation" );
  employer = new actb(document.getElementById("employer"), "employer" );

  if( defaultJoinPageTab > 0 )
    joinPageChangeTab( defaultJoinPageTab );
}

function joinPage2(gid, hash, handler)
{
  tab = joinPageTab;

  if( tab == 1 )
  {
    if( document.getElementById("month-0-X").value == "" ||
        document.getElementById("year-0-X").value == "" ||
        (document.getElementById("month-1-X").value == "" &&
        document.getElementById("year-1-X").value == "" && !document.getElementById("present-X").checked ) ||
        (occupation.actb_val == null && document.getElementById("occupation").value == "") )
    {
      alert( 'You must fill out all fields to continue.' );
      return;
    }

    items = ['month-0', 'month-1', 'year-0', 'year-1'];
    works = new Array();
    obj = {};

    obj.present = 0;

    e = document.getElementById("present-X");
    if( e ) obj.present = e.checked ? 1 : 0;

		for (var j in items)
			obj[items[j]] = document.getElementById(items[j] + "-X").value;

		obj['employer'] = {'val': gid};

		if (occupation.actb_val == null)
			obj['occupation'] = {'txt': document.getElementById("occupation").value};
		else
			obj['occupation'] = {'val': occupation.actb_val};

		works.push(obj);

  	postAjax("/profile/savework.php", "json_works=" + escape(json_encode(works)) + "&savePrev=1", function(data){} );
  }
  else if( tab == 2 )
  {
    if( document.getElementById("year-0-Y").value == "" ||
        document.getElementById("year-1-Y").value == "" )
    {
      alert( 'You must fill out all fields to continue.' );
      return;
    }


    var saveEdus = [];

  	obj = {'start': document.getElementById("year-0-Y").value };

    if( document.getElementById("present-Y").checked )
    {
      var theDate=new Date();
      obj['stop'] = theDate.getFullYear();
    }
    else
      obj['stop'] = document.getElementById("year-1-Y" ).value;

 		obj['school'] = {'val': gid};

  	saveEdus.push(obj);

    postAjax("/profile/saveabout.php", "json_edus=" + escape(json_encode(saveEdus)), function(data){} );
  }
  else if( tab == 3 )
  {
    if( document.getElementById("month-0-Z").value == "" ||
        document.getElementById("year-0-Z").value == "" ||
        (document.getElementById("month-1-Z").value == "" &&
        document.getElementById("year-1-Z").value == "" && !document.getElementById("present-Z").checked ) ||
        (employer.actb_val == null && document.getElementById("employer").value == "") )
    {
      alert( 'You must fill out all fields to continue.' );
      return;
    }

    items = ['month-0', 'month-1', 'year-0', 'year-1'];
    works = new Array();
    obj = {};

    obj.present = 0;

    e = document.getElementById("present-Z");
    if( e ) obj.present = e.checked ? 1 : 0;

		for (var j in items)
			obj[items[j]] = document.getElementById(items[j] + "-Z").value;

		obj['occupation'] = {'val': gid};

		if (employer.actb_val == null)
			obj['employer'] = {'txt': document.getElementById("employer").value};
		else
			obj['employer'] = {'val': employer.actb_val};

		works.push(obj);

  	postAjax("/profile/savework.php", "json_works=" + escape(json_encode(works)), function(data){} );
  }
  else if( tab == 4 )
  {
    postAjax("/profile/addlicense.php", "l=" + gid, function(data){} );
  }

  if( handler == undefined )
    postAjax("/pages/join.php", "gid=" + gid + "&hash=" + hash, "joinPageHandler" );
  else
    postAjax("/pages/join.php", "gid=" + gid + "&hash=" + hash, handler );
}

function joinPageHandler( data )
{
  closePopUp();
  eval( data );
}

function deleteNotification(nid)
{
	postAjax("/messaging/deletenotification.php", "nid=" + nid, "void");
	document.getElementById("notification-" + nid).style.display = "none";

	e = document.getElementById("notifications-number");
	e.innerHTML = parseInt(e.innerHTML) - 1;
}

function showNotifications()
{

  if( document.getElementById( 'notifications' ) )
  {
		showPopUp("", '<div id="notifypopup" style="height: 280px; overflow: auto; padding-right:5px;"></div>', 311, "no", 0, notificationsReplace);
		setTimeout('document.getElementById("notifypopup").appendChild(document.getElementById("notifications"));', 100);
  }
  else
  	getAjax("/messaging/notifications.php?style=3", "showNotificationsHandler");
}

function notificationsReplace()
{
  if(document.getElementById("notificationscontainer"))
  {
  	document.getElementById("notificationscontainer").appendChild(document.getElementById("notifications"));
  }
}

function showNotificationsHandler(data)
{
	showPopUp("", data, 311);
}

function addToLogHandler(data)
{
	if (data == "OK")
		addToLogPending[0].innerHTML = addToLogPending[1];
	else
		addToLogPending[0].innerHTML = "Error";
	
	addToLogPending[0].style.cursor = "default";
}

function deleteFriend(uid, id, caption )
{
  if( typeof caption == "string" )
  	document.getElementById(id).innerHTML = caption;
  else
  	document.getElementById(id).style.display = "none";
	postAjax("/addfriend.php", "uid=" + uid + "&s=-1", "void");
}

function confirmFriend(uid, id, captionFriends)
{
	document.getElementById(id).innerHTML = "Wait &#0133;";
	postAjax("/addfriend.php", "uid=" + uid + "&s=1", "document.getElementById('" + id + "').innerHTML = '" + captionFriends + "'; void");

	e = document.getElementById("numfreqs");
	if (e)
		e.innerHTML = parseInt(e.innerHTML) - 1;
}

function addFriend(uid, id, captionPending)
{
	div = (typeof id == "string") ? div = document.getElementById(id) : id;
	
	div.innerHTML = "Wait &#0133;";
	
	postAjax("/addfriend.php", "uid=" + uid + "&s=0", function (data)
	{
		div.innerHTML = captionPending;
	});
}

var friendsPopupUid = null;
var friendsPopupQ = null;

function showFriendsPopup(uid, q)
{
	if (typeof q == "undefined")
		friendsPopupQ = "*";
	else
		friendsPopupQ = q;

	if (typeof uid == "undefined" || typeof uid == "object")
		uid = friendsPopupUid;
	else
		friendsPopupUid = uid;
	
	e = document.getElementById("friendspopup");

	if (e)
		showFriendsPopupHandler("");
	else
	{
		showPopUp('', '<div id="friendspopup"><div class="loginmessage"><img src="/images/barwait.gif" alt="" /></div>', [515, 487], 'no');
		getAjax("/getfriends.php?uid=" + uid, "showFriendsPopupHandler");
	}
}

function showFriendsPopupHandler(data)
{
	e = document.getElementById("friendspopup");

	if (e)
	{
		if (data != "")
			e.innerHTML = data;

		fs = ['f', 'r', 's'];

		for (var i in fs)
    {
			if( document.getElementById("friendspopup-" + fs[i]) )
        document.getElementById("friendspopup-" + fs[i]).style.display = (friendsPopupQ == fs[i] || friendsPopupQ == "*") ? "" : "none";
    }
	}
}

var shareLoaded = false;

function showCreatePage()
{
	loadShare("newpage", 1);
}

function loadShare(p, show, params, gid)
{
	if (typeof gid == "undefined")
    gid=0;

	if (typeof params == "undefined")
		params = "";
	else if (typeof params == "object")
		params = "&json_params=" + escape(json_encode(params));

	if (typeof show == "undefined")
		show = 0;

	if (shareLoaded)
	{
		if (show == 1)
      if( typeof showShare != "undefined" )
  			showShare();
	}
	else
		loadjscssfile("/share.js.php?p=" + p + "&show=" + show + "&params=" + params + "&gid=" + gid, "js");

	shareLoaded = true;
}

function showPageMembers(gid)
{
	getAjax("/pages/members.php?gid=" + gid,
			function(html)
			{
				showPopUp({'content': '<div id="pagemembers"></div>', 'width': 509});
				document.getElementById("pagemembers").innerHTML = html;
			}
		);
}

var searchPopupDisabled = false;
var searchInterval = 0;
/*
function searchKeypress(event, obj, divName, company_only )
{
  searchPopupDisabled = false;
  dupeName = false;

	kc = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
	if (kc == 13 && divName=="")
		searchDo();

  if( obj.value.length > 2 )
  {
  	getAjax("/search_preview.php?q=" + obj.value + "&c=" + (divName.length > 0 ? 1 : 0) + "&co=" + company_only + "&divname=" + divName,
      function( html )
      {
        if( searchPopupDisabled )
          return;

        if( html.indexOf( '<!--DUPE-->', 0 ) > -1 )
          dupeName = true;

        e = document.getElementById('suggestionBox' + divName);
        if( e )
        {
          if( html != "0" )
          {
            e.innerHTML = html;
            e.style.display = '';
          }
          else
            e.style.display = 'none';
        }
      }
    );
  }
}
*/

function searchKeypress(event, obj, divName, company_only )
{
  searchPopupDisabled = false;
  dupeName = false;

	kc = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
	if (kc == 13 && divName=="")
		searchDo();

  if( searchInterval > 0 )
  {
    window.clearInterval( searchInterval );
    searchInterval = 0;
  }

  if( obj.value.length > 2 )
  {
    searchInterval = window.setInterval( 'searchAction("' + obj.value + '", "' + divName + '", "' + company_only + '" );', 700 );
  }
}

function searchAction( objvalue, divName, company_only )
{
  window.clearInterval( searchInterval );
  searchInterval = 0;

  url = "/search_preview.php?q=" + objvalue + "&c=" + (divName.length > 0 ? 1 : 0) + "&co=" + company_only + "&divname=" + divName;
	log(url);

  getAjax(url,
    function( html )
    {
      if( searchPopupDisabled )
        return;

      if( html.indexOf( '<!--DUPE-->', 0 ) > -1 )
        dupeName = true;

      e = document.getElementById('suggestionBox' + divName);
      if( e )
      {
        if( html != "0" )
        {
          e.innerHTML = html;
          e.style.display = '';
        }
        else
          e.style.display = 'none';
      }

    }
  );
}

var closeInterval = 0;
function searchLostFocus( interval, divName )
{
  if( interval == -1 )
  {
    closeInterval = window.setInterval( 'searchLostFocus(' + closeInterval + ', "' + divName + '" );', 150 );
  }
  else
  {
    e = document.getElementById('suggestionBox' + divName);
    if( e )
    {
      e.style.display='none';
    }
    window.clearInterval( closeInterval );
  }
}

function searchDo()
{
	if (typeof searchType != "undefined")
		suffix = "&t=" + searchType;
	else
		suffix = "";
	
	for (i = 0; i < 2; i++)
	{
		e = document.getElementById("txtsearch" + (i == 0 ? "" : i));
		if (!e) continue;
		
		q = e.value;
		if (q != "" && q != "Search ...") window.location='/search.php?q=' + escape(q) + suffix;
	}
}

function findPos(obj) {
	var curleft = curtop = 0;
	if (obj.offsetParent) {
			do {
					curleft += obj.offsetLeft;
					curtop += obj.offsetTop;
			} while (obj = obj.offsetParent);
	return [curleft,curtop];
	}
	}
	
function getScrollTop(){
    if(typeof pageYOffset!= 'undefined'){
        //most browsers
        return pageYOffset;
    }
    else{
        var B= document.body; //IE 'quirks'
        var D= document.documentElement; //IE with doctype
        D= (D.clientHeight)? D: B;
        return D.scrollTop;
    }
}

function setScrollTop(id, y)
{
	document.getElementById(id).scrollTop = y;
}
	
function getCursorPosition(e) {
    e = e || window.event;
    var cursor = {x:0, y:0};
    if (e.pageX || e.pageY) {
        cursor.x = e.pageX;
        cursor.y = e.pageY;
    } 
    else {
        cursor.x = e.clientX + 
            (document.documentElement.scrollLeft || 
            document.body.scrollLeft) - 
            document.documentElement.clientLeft;
        cursor.y = e.clientY + 
            (document.documentElement.scrollTop || 
            document.body.scrollTop) - 
            document.documentElement.clientTop;
    }
    return [cursor.x, cursor.y];
}

function hideNag(w)
{
	postAjax("/hidenag.php", "w=" + w, "void");
	document.getElementById("nag-" + w).style.display = "none";
}


function getHeight(e) {
    return Math.max(e.scrollHeight, e.offsetHeight, e.clientHeight);
}

String.prototype.reverse = function(){
splitext = this.split("");
revertext = splitext.reverse();
reversed = revertext.join("");
return reversed;
}

function getDocSize()
{
	var w = 0;
	var h = 0;

	//IE
	if(!window.innerWidth)
	{
		//strict mode
		if(!(document.documentElement.clientWidth == 0))
		{
			w = document.documentElement.clientWidth;
			h = document.documentElement.clientHeight;
		}
		//quirks mode
		else
		{
			w = document.body.clientWidth;
			h = document.body.clientHeight;
		}
	}
	//w3c
	else
	{
		w = window.innerWidth;
		h = window.innerHeight;
	}
	return [w,h];
}

function fireEvent(obj,evt){

	var fireOnThis = obj;
	if( document.createEvent ) {
	  var evObj = document.createEvent('MouseEvents');
	  evObj.initEvent( evt, true, false );
	  fireOnThis.dispatchEvent(evObj);
	} else if( document.createEventObject ) {
	  fireOnThis.fireEvent('on'+evt);
	}
}

function showSendMessage(uid, name, pic, subject, message )
{
  if( subject == undefined ) subject = "";
  if( message == undefined ) message = "";

	html  = '<div style="font-size: 8pt; color: #555;">';
	html += '	<div style="float: left; width: 85px;">';
	html += '		<img width="48" height="48" src="' + pic + '" alt="" style="padding-bottom: 2px;" /><br />';
	html += '		' + name;
	html += '	</div>';
	html += '	<div style="float: left; padding-left: 10px; font-weight: bold;">';
	html += '		<div style="height: 12pt;">Subject:</div><input type="text" id="msgsubj" maxlength="50" style="width: 285px;" value="' + subject + '"/>';
	html += '		<div style="padding-top: 5px;">';
	html += '			<div style="height: 12pt;">Message:</div><textarea id="msgbody" style="width: 285px; height: 95px;">' + message + '</textarea>';
	html += '		</div>';
	html += '		<div style="padding: 5px 0px; text-align: center; height: 16px;" id="msgbuttons">';
	html += '			<input type="button" class="button" value="Send" onclick="javascript:sendMessage(\'' + uid + '\');" /> &nbsp; &nbsp;';
	html += '			<input type="button" class="button" value="Cancel" onclick="javascript:closePopUp();" />';
	html += '		</div>';
	html += '	</div>';
	html += '	<div style="clear: both;"></div>';
	html += '</div>';
	
	showPopUp("", html, 0);
}

function sendMessage(uid)
{
	subj = escape(document.getElementById("msgsubj").value);
	body = escape(document.getElementById("msgbody").value);
	
	if (subj == "")
	{
		alert("Please enter a subject.");
		return;
	}

	if (body == "")
	{
		alert("Please type a message.");
		return;
	}
	
	document.getElementById("msgbuttons").innerHTML = "<img src='/images/tbpreload.gif' alt='' />";

	postAjax("/messaging/sendmessage.php", "uid=" + uid + "&subj=" + subj + "&body=" + body, function(data)
	{
    data = data.trim();
		if (data == "OK")
			showPopUp2("", "Your message has been sent.");
		else
    {
      if( admin )
  			showPopUp2("", "Error: " + data );
      else
  			showPopUp2("", "There was an error sending your message.&nbsp; Please try again later.");
    }
	});
}

function fbLogin()
{
	FB.Connect.requireSession(fbloggedin);
	//FB.ensureInit( function(){ FB.Connect.requireSession(fbloggedin); });
	return false;
}

function showLogin(hideButton, title, querystring )
{
  if( typeof title == "undefined" )
    title = "Login with your favorite site";

  if( typeof querystring == "undefined" )
    querystring = "";

	if (typeof hideButton == "undefined")
		hideButton = -1;
	
	content  = '<div style="text-align: center;">';
	content += '	<div style="margin: 10px 0px;">Just click a button below to connect.</div>';
	content += '	<div style="height: 25px; padding-bottom: 10px;">';
	if (hideButton != 0)
	content += '		<span id="loginfb"><a href="/fbconnect/login.php?' + querystring + '"><img src="/images/fbconnect.gif" alt="" /></a></span>';
	if (hideButton != -1)
	content += '		<span id="loginspace">&nbsp; &nbsp;</span>';
	if (hideButton != 1)
	content += '		<span id="logintw"><a href="/twitter/login.php?' + querystring + '"><img src="/images/twitter.gif" alt="" /></a></span>';
	content += '	</div>';
	content += '</div>';
	
	showPopUp(title, content, 0);
}

function actionClicked(e)
{
	x = e.getElementsByTagName('a');

	if (typeof x[0] == "object")
		fireEvent(x[0], "click");
}

function pymkShuffle(i)
{
	getAjax("/getpymk.php?i=" + i, "pymkShuffleHandler");
	document.getElementById("pymk_container" + i).style.visibility = "hidden";
}

function pymkSearch(i, q)
{
	getAjax("/getpymk.php?i=" + i + "&q=" + escape(q), "pymkShuffleHandler");
	document.getElementById("pymk_container" + i).style.visibility = "hidden";
}

function pymkShuffleHandler(html)
{
  data_array = html.split(String.fromCharCode(1));

	i = data_array[0];
	html = data_array[1];

  e = document.getElementById("pymk_container" + i);
  if( e )
  {
    e.innerHTML = html;
  	e.style.visibility = "visible";
  }
}

function suggestionsShuffle()
{
	getAjax("/get_suggestions.php?i=1", "suggestionsShuffleHandler");
	document.getElementById("suggestion_container").style.visibility = "hidden";
}

function suggestionsShuffleHandler(html)
{
	document.getElementById("suggestion_container").innerHTML = html;
	document.getElementById("suggestion_container").style.visibility = "visible";
}

function Shuffle( divname, url )
{
	getAjax("/" + url + "&divname=" + divname, "ShuffleHandler");
	document.getElementById(divname).style.visibility = "hidden";
}

function ShuffleHandler( data )
{
  data_array = data.split(String.fromCharCode(1));
	divname = data_array[0];
	html = data_array[1];

  e = document.getElementById(divname);
  if( e )
  {
  	e.innerHTML = html;
	  e.style.visibility = "visible";
  }
}

function showPhotoChanger(alturl)
{
	loadjscssfile("/photochanger.js", "js");
}

String.prototype.endsWith = function(str) 
{return (this.match(str+"$")==str)}

String.prototype.startsWith = function(str){
    return (this.indexOf(str) === 0);
}


function checkEmail(str) {

		var at="@"
		var dot="."
		var lat=str.indexOf(at)
		var lstr=str.length
		var ldot=str.indexOf(dot)
		if (str.indexOf(at)==-1){
		   return false
		}

		if (str.indexOf(at)==-1 || str.indexOf(at)==0 || str.indexOf(at)==lstr){
		   return false
		}

		if (str.indexOf(dot)==-1 || str.indexOf(dot)==0 || str.indexOf(dot)==lstr){
		    return false
		}

		 if (str.indexOf(at,(lat+1))!=-1){
		    return false
		 }

		 if (str.substring(lat-1,lat)==dot || str.substring(lat+1,lat+2)==dot){
		    return false
		 }

		 if (str.indexOf(dot,(lat+2))==-1){
		    return false
		 }
		
		 if (str.indexOf(" ")!=-1){
		    return false
		 }

 		 return true					
	}

function loadjscssfile(filename, filetype){
 if (filetype=="js"){ //if filename is a external JavaScript file
  var fileref=document.createElement('script')
  fileref.setAttribute("type","text/javascript")
  fileref.setAttribute("src", filename)
 }
 else if (filetype=="css"){ //if filename is an external CSS file
  var fileref=document.createElement("link")
  fileref.setAttribute("rel", "stylesheet")
  fileref.setAttribute("type", "text/css")
  fileref.setAttribute("href", filename)
 }
 if (typeof fileref!="undefined")
  document.getElementsByTagName("head")[0].appendChild(fileref)
}

function isArray(obj) {
   if (typeof obj == "undefined" || obj == null ) return false;
   if (obj.constructor.toString().indexOf("Array") == -1)
      return false;
   else
      return true;
}

function getFormVals(frm) {
        vals = "";
        for (i = 0; i < frm.elements.length; i++) {
                if (frm.elements[i].name == "") {
                        continue;
                }
                vals += "&" + frm.elements[i].name + "=";
                if (frm.elements[i].type == "hidden" ||
                        frm.elements[i].type == "text" ||
                        frm.elements[i].type == "textarea") {
                        vals += frm.elements[i].value;
                } else if (frm.elements[i].type == "checkbox") {
                        if (frm.elements[i].checked) {
                                vals += "on";
                        } else {
                                vals += "off";
                        }
                } else if (frm.elements[i].type == "select-one") {
                        vals += frm.elements[i].options[frm.elements[i].selectedIndex].value;
                }
        }
        vals = vals.substring(1);
        return vals;
}

function json_encode (mixed_val) {
    // Returns the JSON representation of a value
    //
    // version: 1009.2513
    // discuss at: http://phpjs.org/functions/json_encode
    // +      original by: Public Domain (http://www.json.org/json2.js)
    // + reimplemented by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +      improved by: Michael White
    // +      input by: felix
    // +      bugfixed by: Brett Zamir (http://brett-zamir.me)
    // *        example 1: json_encode(['e', {pluribus: 'unum'}]);
    // *        returns 1: '[\n    "e",\n    {\n    "pluribus": "unum"\n}\n]'
    /*
        http://www.JSON.org/json2.js
        2008-11-19
        Public Domain.
        NO WARRANTY EXPRESSED OR IMPLIED. USE AT YOUR OWN RISK.
        See http://www.JSON.org/js.html
    */
    var retVal, json = this.window.JSON;
    try {
        if (typeof json === 'object' && typeof json.stringify === 'function') {
            retVal = json.stringify(mixed_val); // Errors will not be caught here if our own equivalent to resource
                                                                            //  (an instance of PHPJS_Resource) is used

            if (retVal === undefined) {
                throw new SyntaxError('json_encode');
            }
            return retVal;
        }
      }
      catch(err)
      {}
    //If the first round doesn't work, fall through to the back up method
    try
    {
        var value = mixed_val;

        var quote = function (string) {
            var escapable = /[\\\"\u0000-\u001f\u007f-\u009f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g;
            var meta = {    // table of character substitutions
                '\b': '\\b',
                '\t': '\\t',
                '\n': '\\n',
                '\f': '\\f',
                '\r': '\\r',
                '"' : '\\"',
                '\\': '\\\\'
            };

            escapable.lastIndex = 0;
            return escapable.test(string) ?
                            '"' + string.replace(escapable, function (a) {
                                var c = meta[a];
                                return typeof c === 'string' ? c :
                                '\\u' + ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
                            }) + '"' :
                            '"' + string + '"';
        };


        var str = function (key, holder) {
            var gap = '';
            var indent = '    ';
            var i = 0;          // The loop counter.
            var k = '';          // The member key.
            var v = '';          // The member value.
            var length = 0;
            var mind = gap;
            var partial = [];
            var value = holder[key];

            // If the value has a toJSON method, call it to obtain a replacement value.
            if (value && typeof value === 'object' &&
                typeof value.toJSON === 'function') {
                value = value.toJSON(key);
            }

            // What happens next depends on the value's type.
            switch (typeof value) {
                case 'string':
                    return quote(value);

                case 'number':
                    // JSON numbers must be finite. Encode non-finite numbers as null.
                    return isFinite(value) ? String(value) : 'null';

                case 'boolean':
                case 'null':
                    // If the value is a boolean or null, convert it to a string. Note:
                    // typeof null does not produce 'null'. The case is included here in
                    // the remote chance that this gets fixed someday.

                    return String(value);

                case 'object':
                    // If the type is 'object', we might be dealing with an object or an array or
                    // null.
                    // Due to a specification blunder in ECMAScript, typeof null is 'object',
                    // so watch out for that case.
                    if (!value) {
                        return 'null';
                    }
                    if ((this.PHPJS_Resource && value instanceof this.PHPJS_Resource) ||
                        (window.PHPJS_Resource && value instanceof window.PHPJS_Resource)) {
                        throw new SyntaxError('json_encode');
                    }

                    // Make an array to hold the partial results of stringifying this object value.
                    gap += indent;
                    partial = [];

                    // Is the value an array?
                    if (Object.prototype.toString.apply(value) === '[object Array]') {
                        // The value is an array. Stringify every element. Use null as a placeholder
                        // for non-JSON values.

                        length = value.length;
                        for (i = 0; i < length; i += 1) {
                            partial[i] = str(i, value) || 'null';
                        }

                        // Join all of the elements together, separated with commas, and wrap them in
                        // brackets.
                        v = partial.length === 0 ? '[]' :
                                gap ? '[\n' + gap +
                                partial.join(',\n' + gap) + '\n' +
                                mind + ']' :
                                '[' + partial.join(',') + ']';
                        gap = mind;
                        return v;
                    }

                    // Iterate through all of the keys in the object.
                    for (k in value) {
                        if (Object.hasOwnProperty.call(value, k)) {
                            v = str(k, value);
                            if (v) {
                                partial.push(quote(k) + (gap ? ': ' : ':') + v);
                            }
                        }
                    }

                    // Join all of the member texts together, separated with commas,
                    // and wrap them in braces.
                    v = partial.length === 0 ? '{}' :
                            gap ? '{\n' + gap + partial.join(',\n' + gap) + '\n' +
                            mind + '}' : '{' + partial.join(',') + '}';
                    gap = mind;
                    return v;
                case 'undefined': // Fall-through
                case 'function': // Fall-through
                default:
                    throw new SyntaxError('json_encode');
            }
        };

        // Make a fake root object containing our value under the key of ''.
        // Return the result of stringifying the value.
        return str('', {
            '': value
        });

    } catch(err) { // Todo: ensure error handling above throws a SyntaxError in all cases where it could
                            // (i.e., when the JSON global is not available and there is an error)
        if (!(err instanceof SyntaxError)) {
            throw new Error('Unexpected error type in json_encode()');
        }
        this.php_js = this.php_js || {};
        this.php_js.last_error_json = 4; // usable by json_last_error()
        return null;
    }

}

function htmlentities(str) {
    return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
}



////////////////

var timerID = new Array();
var startTime = new Array();
var obj = new Array();
var endHeight = new Array();
var moving = new Array();
var dir = new Array();
var ddd = [];

function slidedown(objname, h, resetstyle){
  if(moving[objname])
          return;

  //if(document.getElementById(objname).style.display != "none")
          //return; // cannot slide down something that is already visible

  moving[objname] = true;
  dir[objname] = "down";
  startslide(objname, h, resetstyle);
}

function slideup(objname, h, resetstyle){
  if(moving[objname])
    return;

        //if(document.getElementById(objname).style.display == "none")
                //return; // cannot slide up something that is already hidden

  moving[objname] = true;
  dir[objname] = "up";
  startslide(objname, h, resetstyle);
}

function startslide(objname, h, resetstyle){
  obj[objname] = document.getElementById(objname);
  if (resetstyle)
  {
  	obj[objname].style.overflow = "hidden";
  	obj[objname].style.height = 0;
  }

  endHeight[objname] = parseInt(h ? h : obj[objname].style.height);
  obj[objname].style.display = "block";

  timerID[objname] = setInterval('slidetick("' + objname + '", ' + (resetstyle ? 'true' : 'false') + ');',10); //timerlen
  startTime[objname] = (new Date()).getTime();
}

function slidetick(objname, resetstyle){
	slideAniLen = 1500;
  var elapsed = (new Date()).getTime() - startTime[objname];

  if (elapsed > slideAniLen)
    endSlide(objname, resetstyle)
  else {
    var d =Math.round(elapsed / slideAniLen * endHeight[objname]);
    var o =Math.round(elapsed / slideAniLen * 100);

    if(dir[objname] == "up")
    {
      o = 100 - o;
      d = endHeight[objname] - d;
    }

    obj[objname].style.height = d + "px";
    ddd[ddd.length] = o;
    obj[objname].style.opacity = o / 100;
    obj[objname].style.filter = 'alpha(opacity = ' + o + ')';
  }

  return;
}

function endSlide(objname, resetstyle){
  obj[objname] = document.getElementById(objname);
  clearInterval(timerID[objname]);

	o = 100;
  if(dir[objname] == "up")
  {
    obj[objname].style.display = "none";
		o = 0;
	}

		if (resetstyle)
		{
		obj[objname].style.height = "";
		obj[objname].style.overflow = "";
		}
		else
		{
        obj[objname].style.height = endHeight[objname] + "px";
		}
        obj[objname].style.opacity = o / 100;
        obj[objname].style.filter = 'alpha(opacity = ' + o + ')';

        delete(moving[objname]);
        delete(timerID[objname]);
        delete(startTime[objname]);
        delete(endHeight[objname]);
        delete(obj[objname]);
        delete(dir[objname]);
}

function toggleSlide( objname )
{
  obj = document.getElementById(objname);
  if( obj.style.display == "none" )
    slidedown( objname );
  else
    slideup( objname );
}

////////////////common functions from autocomplete//////////////////

/* Event Functions */

// Add an event to the obj given
// event_name refers to the event trigger, without the "on", like click or mouseover
// func_name refers to the function callback when event is triggered
function addEvent(obj,event_name,func_name){
	if (obj.attachEvent){
		obj.attachEvent("on"+event_name, func_name);
	}else if(obj.addEventListener){
		obj.addEventListener(event_name,func_name,true);
	}else{
		obj["on"+event_name] = func_name;
	}
}

// Removes an event from the object
function removeEvent(obj,event_name,func_name){
	if (obj.detachEvent){
		obj.detachEvent("on"+event_name,func_name);
	}else if(obj.removeEventListener){
		obj.removeEventListener(event_name,func_name,true);
	}else{
		obj["on"+event_name] = null;
	}
}

// Stop an event from bubbling up the event DOM
function stopEvent(evt){
	evt || window.event;
	if (evt.stopPropagation){
		evt.stopPropagation();
		evt.preventDefault();
	}else if(typeof evt.cancelBubble != "undefined"){
		evt.cancelBubble = true;
		evt.returnValue = false;
	}
	return false;
}

// Get the obj that starts the event
function getElement(evt){
	if (window.event){
		return window.event.srcElement;
	}else{
		return evt.currentTarget;
	}
}
// Get the obj that triggers off the event
function getTargetElement(evt){
	if (window.event){
		return window.event.srcElement;
	}else{
		return evt.target;
	}
}
// For IE only, stops the obj from being selected
function stopSelect(obj){
	if (typeof obj.onselectstart != 'undefined'){
		addEvent(obj,"selectstart",function(){ return false;});
	}
}

/*    Caret Functions     */

// Get the end position of the caret in the object. Note that the obj needs to be in focus first
function getCaretEnd(obj){
	if(typeof obj.selectionEnd != "undefined"){
		return obj.selectionEnd;
	}else if(document.selection&&document.selection.createRange){
		var M=document.selection.createRange();
		try{
			var Lp = M.duplicate();
			Lp.moveToElementText(obj);
		}catch(e){
			var Lp=obj.createTextRange();
		}
		Lp.setEndPoint("EndToEnd",M);
		var rb=Lp.text.length;
		if(rb>obj.value.length){
			return -1;
		}
		return rb;
	}
}
// Get the start position of the caret in the object
function getCaretStart(obj){
	if(typeof obj.selectionStart != "undefined"){
		return obj.selectionStart;
	}else if(document.selection&&document.selection.createRange){
		var M=document.selection.createRange();
		try{
			var Lp = M.duplicate();
			Lp.moveToElementText(obj);
		}catch(e){
			var Lp=obj.createTextRange();
		}
		Lp.setEndPoint("EndToStart",M);
		var rb=Lp.text.length;
		if(rb>obj.value.length){
			return -1;
		}
		return rb;
	}
}
// sets the caret position to l in the object
function setCaret(obj,l){
	obj.focus();
	if (obj.setSelectionRange){
		obj.setSelectionRange(l,l);
	}else if(obj.createTextRange){
		m = obj.createTextRange();		
		m.moveStart('character',l);
		m.collapse();
		m.select();
	}
}
// sets the caret selection from s to e in the object
function setSelection(obj,s,e){
	obj.focus();
	if (obj.setSelectionRange){
		obj.setSelectionRange(s,e);
	}else if(obj.createTextRange){
		m = obj.createTextRange();		
		m.moveStart('character',s);
		m.moveEnd('character',e);
		m.select();
	}
}

/*    Escape function   */
String.prototype.addslashes = function(){
	return this.replace(/(["\\\.\|\[\]\^\*\+\?\$\(\)])/g, '\\$1');
}
String.prototype.trim = function () {
    return this.replace(/^\s*(\S*(\s+\S+)*)\s*$/, "$1");
};
/* --- Escape --- */

/* Offset position from top of the screen */
function curTop(obj){
	toreturn = 0;
	while(obj){
		toreturn += obj.offsetTop;
		obj = obj.offsetParent;
	}
	return toreturn;
}
function curLeft(obj){
	toreturn = 0;
	while(obj){
		toreturn += obj.offsetLeft;
		obj = obj.offsetParent;
	}
	return toreturn;
}
/* ------ End of Offset function ------- */

/* Types Function */

// is a given input a number?
function isNumber(a) {
    return typeof a == 'number' && isFinite(a);
}

/* Object Functions */

function replaceHTML(obj,text){
	while(el = obj.childNodes[0]){
		obj.removeChild(el);
	};
	
	newspan = document.createElement("span");
	newspan.innerHTML = text;
	
	//obj.appendChild(document.createTextNode(text));
	obj.appendChild(newspan);
}

function updateMediaCategory( id, type, cat, hash )
{
  postAjax("/admin/update_media_category.php", "type=" + type + "&id=" + id + "&cat=" + cat + "&hash=" + hash, "updateMediaCategoryHandler" ); //, function( data ) {alert( data); } );
}

function updateMediaCategoryHandler( data )
{
}

function sendFeedback()
{
  if( document.getElementById('feedbackText') )
  {
    text = document.getElementById('feedbackText').value;
    request_uri = "";
    postAjax("/send_feedback.php", "text=" + text + "&script=" + script + "&requesturi=" + encodeURIComponent(request_uri), "sendFeedbackHandler" );
  }
}

function sendFeedbackHandler( data )
{

  if( document.getElementById('feedbackText') )
  {
    document.getElementById('feedbackText').value = '';

  }

  if( document.getElementById('feedbackContainer') )
  {
    document.getElementById('feedbackContainer').innerHTML = '<br /><br /><br /><h3>Feedback Sent,<br /> Thank you!</h3>';
  }
}

function selectedValue(selBox) {
  return selBox.options[selBox.selectedIndex].value;
}

function selectedText(selBox) {
  return selBox.options[selBox.selectedIndex].text;
}


function loadGetListed()
{
	postAjax("/pages/page_create_popup.php", '', function (data)
	{
    showPopUp("", data, 0);
	});
}


function getFormVals2(frm) {
	vals = "";
	for (i = 0; i < frm.elements.length; i++) {
		if (frm.elements[i].name == "") {
			continue;
		}
		vals += "&" + frm.elements[i].name + "=";
		if (frm.elements[i].type == "hidden" || frm.elements[i].type == "text" || frm.elements[i].type == "textarea") {
      temp = frm.elements[i].value;
      temp = temp.replace( "&", "&amp;" );
			vals += temp;
		} else if (frm.elements[i].type == "checkbox") {
			if (frm.elements[i].checked) {
				vals += "on";
			} else {
				vals += "off";
			}
		} else if (frm.elements[i].type == "select-one") {
			vals += escape(frm.elements[i].options[frm.elements[i].selectedIndex].value);
		}
	}
	vals = vals.substring(1);
	return vals;
}

function createCookie(name,value,days) {
	if (days) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else var expires = "";
	document.cookie = name+"="+value+expires+"; path=/";
}

function sendEmail(email, message, subject)
{
  post = 'email=' + email;
  if( message != "" )
    post += '&message=' + message;
  if( typeof subject !== "undefined" && subject != "" )
    post += '&subject=' + subject;

	postAjax("/send_email.php", post, function (data)
	{
    showPopUp("Send Email", data, 0);
	});
}

function sendEmailAction()
{
  to = document.getElementById( 'to' ).value;
  from = document.getElementById( 'from' ).value;
  subject = document.getElementById( 'subject' ).value;
  message = document.getElementById( 'message' ).value;

	postAjax("/send_email.php", 'send=1&to=' + encodeURIComponent(to) + '&from=' + encodeURIComponent(from) + '&subject=' + encodeURIComponent(subject) + '&message=' + encodeURIComponent(message), function (data)
     { if( data != 'OK' ) alert( data ); closePopUp() } );
}

function showLinksToPage( gid )
{
	e = document.getElementById("linkspopup");

	if (e)
		showLinksToPageHandler("");
	else
	{
		showPopUp('', '<div id="linkspopup"><div class="loginmessage"><img src="/images/barwait.gif" alt="" /></div>', [515, 487], 'no');
		getAjax("/getlinkstopage.php?gid=" + gid, showLinksToPageHandler);
	}
}

function showLinksToPageHandler( data )
{
	e = document.getElementById("linkspopup");

	if (e)
	{
		if (data != "")
			e.innerHTML = data;
  }
}


var showPopupUrlOnLoad = null;
function showPopupUrl( url, onLoadEvent )
{
  if(typeof onLoadEvent == "undefined")
    showPopupUrlOnLoad = null;
  else
    showPopupUrlOnLoad = onLoadEvent;

  getAjax(url, showPopupUrlHandler );
}

function showPopupUrlHandler(data)
{
	showPopUp("", data );

  if( showPopupUrlOnLoad != null )
    showPopupUrlOnLoad();
}

function hideDiv( divname )
{
  e = document.getElementById( divname );
  if( e )
    e.style.display='none';
}

function addCommas(nStr)
{
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
  if( x.length > 1 )
  	x2 = x.length > 1 ? '.' + x[1] : '';
  else
    x2 = "";

	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}
	return x1 + x2;
}

function reloadStates( country )
{

	postAjax("/adv/getStates.php", "country=" + country, "reloadStatesHandler" );
}

function reloadStatesHandler( data )
{
  e = document.getElementById("states");
  if( e )
  {
    e.innerHTML = data;
  }
}
function showPasswordBox()
{
	document.getElementById("txtpassword_container").innerHTML = '<input type="password" id="txtpassword" name="password" onkeypress="javascript:return trySubmit(this,event);" />';
	document.getElementById("txtpassword").focus();
}

function doCaptcha()
{
  var pswd = new String( document.forms['frmsignup'].elements['password'].value );
  var uname = new String( document.forms['frmsignup'].elements['uname'].value );
  var email = new String( document.forms['frmsignup'].elements['email'].value );

  if( uname.length < 3 | uname.length > 16 )
  {
    showPopUp2( "Whoops!",  "<p>User names must between 3 and 16 characters and contain only letters, spaces, numbers, and a period.", 500);
    return false;
  }

  if( uname.length == uname.replace(" ", "").length )
  {
    showPopUp2( "Whoops!",  "<p>Please enter your first and last name.", 500);
    return false;
  }

  if( pswd.length < 5 | pswd.length > 20 )
  {
    showPopUp2( "Whoops!",  "<p>Your password must be between 5 and 20 characters.", 500);
    return false;
  }

  if( document.forms['frmsignup'].elements['year'][0].selected == true | document.forms['frmsignup'].elements['month'][0].selected == true | document.forms['frmsignup'].elements['day'][0].selected == true )
  {
    showPopUp2( "Whoops!",  "<p>You have chosen an invalid date of birth.", 500);
    return false;
  }

  if( email.length < 4 )
  {
    showPopUp2( "Whoops!",  "<p>The e-mail address you have entered is invalid.", 500);
    return false;
  }

	showPopUp("Please verify that you\'re human", '<center><div id="recaptchaparent" style="padding: 5px 0;"></div><input type="button" class="button" value="Sign Up" onclick="javascript:submitSignUp();" /> &nbsp;&nbsp; <input type="button" class="button" onclick="javascript:closeCaptcha();" value="Cancel" /></center>', 0, "no", true);

	e = document.getElementById("recaptcha");
	document.getElementById("recaptchaparent").appendChild(e);
	e.style.display = "";
}

function submitSignUp()
{
	closeCaptcha();

	vals = getFormVals(document.forms['frmrecaptcha']).split("&");
	for (var i in vals)
	{
		x = vals[i].split("=");
		eval("document.forms['frmsignup']." + x[0] + ".value = '" + x[1] + "';");
	}

	document.forms['frmsignup'].submit();
}

function closeCaptcha()
{
	e = document.getElementById("recaptcha");
	document.getElementById("recaptchaoldparent").appendChild(e);
	e.style.display = "";

	closePopUp();
}


function trySubmit(txt, e)
{
	var keycode;

	if (window.event)
		keycode = window.event.keyCode;
	else if (e)
		keycode = e.which;
	else
		return true;

	if (keycode == 13)
	{
		txt.form.submit();
		return false;
	}
	else
		return true;
}

function getStarted2()
{
  if( document.getElementById( "getstarted" ) )
  {
  	document.getElementById("getstarted").style.display = "";
	  document.getElementById("newuname").focus();
  }
  else
  {
    showPopupUrl( "/signup/signup_popup.php?a=1", "void" );
  }
}

function getStarted3()
{
  showPopupUrl( "/signup/login_redirect_popup.php?a=1", "void" );
  return;

  html = '<div style=" width: 250px; margin-left:auto; margin-right:auto; padding:20px; font-size:8pt;"><div style="font-size:14pt;">Want to access this page?</div><div style="font-size:9pt;">Sign in or create an account.</div><div style="margin-top:20px;"><div style="margin-top: 10px; text-align: center;">';
  html += '<input type="button" style="height: 23px; width: 62px; background: #FFF8CC; border: 1px solid #FF9A66; color: #3B5998; font-weight: bold; font-size: 9pt; font-family: arial;" value="Sign Up" onclick="closePopUp(); getStarted2();"></div>';
  html += '<div style="text-align:center; color: #808080; padding-top:3px;">or login with</div><div style="text-align:center; color: #808080; padding-top:5px;">';
  html += '<a href="/start"><img src="/images/s_badge 20x20.png" width="20" height="20"/></a> <a href="/fbconnect/login.php"><img src="/images/facebook16.png" /></a> <a href="/twitter/login.php"><img src="/images/twitter16.png" /></a></div></div></div>';

  showPopUp( 'Whoops!', html );
}

function hideUser( uid, div, source )
{
	getAjax("/settings/hide_user_action.php?uid=" + uid, function( data ) {

  if( div != "" )
  {
    if( source == "pymk" )
    {
      url = "/getpymk.php?i=2&limit=1&div=" + div;
    }
    else if( source == "s_and_r" )
    {
      url = "/get_suggestions.php?i=2&limit=1&div=" + div;
    }

    getAjax( url, function( data ) {
      e = document.getElementById( div );
      if( e )
      {
        if( source == "pymk" )
        {
          data_array = data.split(String.fromCharCode(1));
    	    html = data_array[1];
        }
        else
        {
          html = data;
        }

        e.innerHTML = html;
      }
    } );
  }

  } );
}

function alternateDisplay( id1, id2, showmore )
{
  e1 = document.getElementById( id1 );
  e2 = document.getElementById( id2 );
  eshowmore = document.getElementById( showmore );

  if( e1 && e2 )
  {
    if( e1.style.display == '' )
    {
      e1.style.display = 'none';
      e2.style.display = '';
      eshowmore.innerHTML = 'hide';
    }
    else
    {
      e2.style.display = 'none';
      e1.style.display = '';
      eshowmore.innerHTML = 'show more';
    }
  }

}

function showEmbedMedia(url, jmp, type, id)
{
	scrollY = window.pageYOffset || document.documentElement.scrollTop || 0;

//	document.getElementById("embed-embed").value = "<scri" + "pt language=\"javascript\" type=\"text/javascript\" src=\"http://" + hostName + "/embed.js.php?t=" + type + "&id=" + id + "\"></scri" + "pt>";
	document.getElementById("embed-full").value = "http://" + hostName + url;
	document.getElementById("embed-short").value = "http://j.mp/" + jmp;
	document.getElementById("embedmedia").style.top = scrollY + "px";
	document.getElementById("embedmedia").style.display = "inline";

	document.body.style.overflow = "hidden";
}

function hideEmbedMedia()
{
	document.body.style.overflow = "";
	document.getElementById("embedmedia").style.display = "";
}

function openSendMessagePopup(msg, subj, name, pic, uid, gid, em)
{
  window.open( '/messaging/send_message_popup.php?name=' + encodeURIComponent(name) + '&msg=' + encodeURIComponent(msg) + '&subj=' + encodeURIComponent(subj) + '&pic=' + pic + '&uid=' + uid + '&gid=' + gid + '&em=' + em, 'messaging', 'width=540,height=414', true );
}

function openPopupWindow(url, title, width, height )
{
  window.open( url, title, 'width=' + width + ',height=' + height, true );
}

String.prototype.trim = function() {
	return this.replace(/^\s+|\s+$/g,"");
}
String.prototype.ltrim = function() {
	return this.replace(/^\s+/,"");
}
String.prototype.rtrim = function() {
	return this.replace(/\s+$/,"");
}

function openVesselMap( gid, navPoint, showTrack )
{
  showPopupUrl( "/vessels/vessel_frame.php?gid=" + gid + "&navPoint=" + navPoint + "&showTrack=" + showTrack );
}

function openFeedItemPopup( fid )
{
  showPopupUrl( "/profile/popup-feeditem.php?fid=" + fid );
}


function showEnableAcct(a, title, querystring )
{
  if( typeof querystring == "undefined" )
    querystring = "";

	if (a == 2)
		window.location.href ='/signup/resetpw.php?uid=<?=$API->uid?>&r=<?=$API->generateDeleteCommentHash("resetpw", $API->uid)?>';
	else
		showLogin(1 - a, title, querystring);
}

function setPostAs( site, sm_id, on )
{
  if( on ) on = 1;
  else on = 0;

  getAjax("/profile/setPostAsPreference.php?site=" + site + "&sm_id=" + sm_id + "&on=" + on, function(data) { } );
}

function htmlspecialchars_decode (string, quote_style) {
  // http://kevin.vanzonneveld.net
  // +   original by: Mirek Slugen
  // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // +   bugfixed by: Mateusz "loonquawl" Zalega
  // +      input by: ReverseSyntax
  // +      input by: Slawomir Kaniecki
  // +      input by: Scott Cariss
  // +      input by: Francois
  // +   bugfixed by: Onno Marsman
  // +    revised by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // +   bugfixed by: Brett Zamir (http://brett-zamir.me)
  // +      input by: Ratheous
  // +      input by: Mailfaker (http://www.weedem.fr/)
  // +      reimplemented by: Brett Zamir (http://brett-zamir.me)
  // +    bugfixed by: Brett Zamir (http://brett-zamir.me)
  // *     example 1: htmlspecialchars_decode("<p>this -&gt; &quot;</p>", 'ENT_NOQUOTES');
  // *     returns 1: '<p>this -> &quot;</p>'
  // *     example 2: htmlspecialchars_decode("&amp;quot;");
  // *     returns 2: '&quot;'
  var optTemp = 0,
    i = 0,
    noquotes = false;
  if (typeof quote_style === 'undefined') {
    quote_style = 2;
  }
  string = string.toString().replace(/&lt;/g, '<').replace(/&gt;/g, '>');
  var OPTS = {
    'ENT_NOQUOTES': 0,
    'ENT_HTML_QUOTE_SINGLE': 1,
    'ENT_HTML_QUOTE_DOUBLE': 2,
    'ENT_COMPAT': 2,
    'ENT_QUOTES': 3,
    'ENT_IGNORE': 4
  };
  if (quote_style === 0) {
    noquotes = true;
  }
  if (typeof quote_style !== 'number') { // Allow for a single string or an array of string flags
    quote_style = [].concat(quote_style);
    for (i = 0; i < quote_style.length; i++) {
      // Resolve string input to bitwise e.g. 'PATHINFO_EXTENSION' becomes 4
      if (OPTS[quote_style[i]] === 0) {
        noquotes = true;
      } else if (OPTS[quote_style[i]]) {
        optTemp = optTemp | OPTS[quote_style[i]];
      }
    }
    quote_style = optTemp;
  }
  if (quote_style & OPTS.ENT_HTML_QUOTE_SINGLE) {
    string = string.replace(/&#0*39;/g, "'"); // PHP doesn't currently escape if more than one 0, but it should
    // string = string.replace(/&apos;|&#x0*27;/g, "'"); // This would also be useful here, but not a part of PHP
  }
  if (!noquotes) {
    string = string.replace(/&quot;/g, '"');
  }
  // Put this in last place to avoid escape being double-decoded
  string = string.replace(/&amp;/g, '&');

  return string;
}

function swapElementDisplay( id1, id2 )
{
  e1 = document.getElementById( id1 );
  e2 = document.getElementById( id2 );
  
  if( e1 && e2 )
  {
    temp = e2.style.display;
    e2.style.display = e1.style.display;  
    e1.style.display = temp;
  }
}


function json_decode (str_json) {
  // http://kevin.vanzonneveld.net
  // +      original by: Public Domain (http://www.json.org/json2.js)
  // + reimplemented by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // +      improved by: T.J. Leahy
  // +      improved by: Michael White
  // *        example 1: json_decode('[\n    "e",\n    {\n    "pluribus": "unum"\n}\n]');
  // *        returns 1: ['e', {pluribus: 'unum'}]
/*
    http://www.JSON.org/json2.js
    2008-11-19
    Public Domain.
    NO WARRANTY EXPRESSED OR IMPLIED. USE AT YOUR OWN RISK.
    See http://www.JSON.org/js.html
  */

  var json = this.window.JSON;
  if (typeof json === 'object' && typeof json.parse === 'function') {
    try {
      return json.parse(str_json);
    } catch (err) {
      if (!(err instanceof SyntaxError)) {
        throw new Error('Unexpected error type in json_decode()');
      }
      this.php_js = this.php_js || {};
      this.php_js.last_error_json = 4; // usable by json_last_error()
      return null;
    }
  }

  var cx = /[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g;
  var j;
  var text = str_json;

  // Parsing happens in four stages. In the first stage, we replace certain
  // Unicode characters with escape sequences. JavaScript handles many characters
  // incorrectly, either silently deleting them, or treating them as line endings.
  cx.lastIndex = 0;
  if (cx.test(text)) {
    text = text.replace(cx, function (a) {
      return '\\u' + ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
    });
  }

  // In the second stage, we run the text against regular expressions that look
  // for non-JSON patterns. We are especially concerned with '()' and 'new'
  // because they can cause invocation, and '=' because it can cause mutation.
  // But just to be safe, we want to reject all unexpected forms.
  // We split the second stage into 4 regexp operations in order to work around
  // crippling inefficiencies in IE's and Safari's regexp engines. First we
  // replace the JSON backslash pairs with '@' (a non-JSON character). Second, we
  // replace all simple value tokens with ']' characters. Third, we delete all
  // open brackets that follow a colon or comma or that begin the text. Finally,
  // we look to see that the remaining characters are only whitespace or ']' or
  // ',' or ':' or '{' or '}'. If that is so, then the text is safe for eval.
  if ((/^[\],:{}\s]*$/).
  test(text.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, '@').
  replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').
  replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {

    // In the third stage we use the eval function to compile the text into a
    // JavaScript structure. The '{' operator is subject to a syntactic ambiguity
    // in JavaScript: it can begin a block or an object literal. We wrap the text
    // in parens to eliminate the ambiguity.
    j = eval('(' + text + ')');

    return j;
  }

  this.php_js = this.php_js || {};
  this.php_js.last_error_json = 4; // usable by json_last_error()
  return null;
}


function log(what, id)
{
	if (isDevServer)
	{
		if (id)
			console.log('=============== ' + id + ' ===============');
		console.log(what);
		
		if (id)
			console.log('==========================================');
	}
}

function new_guid()
{
    var S4 = function ()
    {
        return Math.floor(
                Math.random() * 0x10000 /* 65536 */
            ).toString(16);
    };

    return (
            S4() + S4() + "-" +
            S4() + "-" +
            S4() + "-" +
            S4() + "-" +
            S4() + S4() + S4()
        );
}

$('input[data-default]').ready(
	function ()
	{
		$('input[data-default]').each(
			function ()
			{
				if ($(this).val() == '')
					$(this).val($(this).attr('data-default'));
				
				$(this).focus(
					function ()
					{
						if ($(this).val() == $(this).attr('data-default'))
							$(this).val('');
					}
				);
				
				$(this).blur(
					function ()
					{
						if ($(this).val() == '')
							$(this).val($(this).attr('data-default'));
					}
				);
				
				$(this).closest('form').submit(
					function ()
					{
						$(this).find('input[data-default]').each(
							function ()
							{
								if ($(this).val() == $(this).attr('data-default'))
									$(this).val('');
							}
						);
					}
				);
			}
		);
	}
);

/////////autocomplete////////////
$('document').ready( addAutocompleteHandlers );

function addAutocompleteHandlers()
{
	$('input[data-autocomplete]').keyup(
		function (e)
		{
			if (e.keyCode == 8) //backspace
			{
				min = parseInt($(this).attr('data-ac_min'));
				
				if (min > 0 && $(this).val().length < min)
					log('ignoring autocomplete request for ' + guid + ' [' + func + ']');
				else
					$(this).keypress();
			}
		}
	);
	
	$('input[data-autocomplete]').keypress(
		function ()
		{
			if (typeof $(this).attr('data-guid') != 'string')
				$(this).attr('data-guid', new_guid());
			
			guid = $(this).attr('data-guid');
			php = $(this).attr('data-autocomplete');
			func = typeof $(this).attr('data-ac_func') == 'string' ? $(this).attr('data-ac_func') : $(this).attr('data-autocomplete');
			min = parseInt($(this).attr('data-ac_min'));
			
			if (min > 0 && $(this).val().length + 1 < min)
				log('ignoring autocomplete request for ' + guid + ' [' + php + ']');
			else
			{
				log('queuing autocomplete request for ' + guid + ' [' + php + ']');
				
				clearTimeout($(this).attr('data-timer'));
				$(this).attr('data-timer', setTimeout("requestAutocomplete('" + guid + "');", 500));
			}
		}
	);
}

function requestAutocomplete(guid)
{
	el = $('input[data-guid="' + guid + '"]');
	php = $(el).attr('data-autocomplete');
	func = typeof $(el).attr('data-ac_func') == 'string' ? $(el).attr('data-ac_func') : $(el).attr('data-autocomplete');
	limit = parseInt($(el).attr('data-ac_limit'));
	
	if (!(limit > 0))
		limit = 100;
	
	log('sending autocomplete request for ' + guid + ' [' + php + ']');
	
	query = { q: $(el).val(), n: limit, guid: guid };
	
	$.ajax({
		type: 'POST',
		url: '/ajax/' + php + '.php',
		data: query,
		success: eval('ac_' + func)
	});
}

function getClosestRelativeParent(e)
{
	var closestRelativeParent = $(e).parents().filter(function() { 
	  // reduce to only relative position or "body" elements
	  var $this = $(this);
	  return $this.is('body') || $this.css('position') == 'relative';
	}).slice(0,1);
	
	return closestRelativeParent;
}

function parseDuration(secs)
{
	s = secs % 60;
	m = Math.floor(secs / 60);
	
	if (s.toString().length == 1)
		s = '0' + s.toString();
	
	return m + ':' + s;
}

/////////////header search////////////////
function ac_get_search(data, no_mouse_bindings)
{
	log(data, 'autocomplete response received for ' + data.guid);
	
	ul = $('<ul class="autocomplete"></ul>');
	for (i in data.results)
	{
		li = $('<li></li>');
		if (data.results[i].type == 'v' || data.results[i].type == 'p')
			img_url = data.results[i].container_url + '/' + data.results[i].hash + '.jpg';
		else if (data.results[i].type == 'U' && typeof data.results[i].hash != 'string')
			img_url = '/images/nouser.jpg';
		else
			img_url = (typeof data.results[i].container_url == 'string' ? data.results[i].container_url + '/' + data.results[i].hash + '_square.jpg' : '/images/company_default.png');
		
		html  = '<img src="' + img_url + '">';
		html += '<div><span class="name">' + data.results[i].title + '</span><br>';
		
		if (data.results[i].type == 'V' && data.results[i].exnames)
			html += 'ex. ' + data.results[i].exnames;
		else
			html += data.results[i].type_str;
		
		html += '<br>';
		
		if (data.results[i].type == 'v')
			html += 'Duration: ' + parseDuration(data.results[i].duration);
		else if (data.results[i].type == 'V')
			html += data.results[i].length_ft + ' ft / ' + data.results[i].length_m + ' m';
		else if (data.results[i].type == 'E')
			html += data.results[i].cat_str;
		else if (data.results[i].type == 'p')
			html += 'by ' + data.results[i].name;
		
		$(li).html(html);
		$(li).attr('data-json', json_encode(data.results[i]));
		$(ul).append($(li));
	}
	
	$('ul.autocomplete').remove();
	
	if (data.results.length > 0)
	{
		parent = getClosestRelativeParent($('input[data-guid="' + data.guid + '"]'));
		$(ul).appendTo(parent);
				
		$('ul.autocomplete li').click(
			function ()
			{
				json = json_decode($(this).attr('data-json'));
				
				if (json.type == 'p')
					location = '/photo/' + json.media_id + '-' + escape(json.title);
				else if (json.type == 'v')
					location = '/video/' + json.media_id + '-' + escape(json.title);
				else if (json.type == 'U')
					location = '/user/' + escape(json.username);
				else
					location = '/page/' + json.gid + '-' + escape(json.title);
			}
		);
		
		$(document).click(
			function ()
			{
				if ($(this).hasClass('.autocomplete') || $(this).attr('data-autocomplete'))
				{
				}
				else
					$('ul.autocomplete').remove();
			}
		);
	}
	
	return ul;
}
