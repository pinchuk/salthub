function getUserContacts()
{
	getAjax("/showcontacts.php?", "document.getElementById('contacts_container').innerHTML =");
}

function removeContact(cid)
{
	postAjax("/removecontact.php", "cid=" + cid, "getUserContacts");
}

var cfields = ['name', 'cell', 'work', 'email', 'www'];
var cediting = 0;

function editContact(cid)
{
	cediting = cid;
	prefix = "cmanitem-" + cid + "-";
	name = document.getElementById(prefix + "name").innerHTML;
	html  = '<div class="contacteditleft"><img width="48" height="48" src="' + document.getElementById(prefix + "pic").src + '" alt="" /><br />' + name + '</div>';
	html += '<div class="contactedit">';
	
	ctitles = ['name', 'cell phone', 'work phone', 'e-mail', 'website'];
	for (var i in cfields)
	{
		try
		{
			val = document.getElementById(prefix + cfields[i]).innerHTML;
		}
		catch (e)
		{
			val = "";
		}
		html += '<div><span>' + ctitles[i] + ':</span><input type="text" id="edit-' + cfields[i] + '" value="' + val + '" /></div>';
	}
	
	html += '</div>';
	html += '<div style="clear: both; padding-top: 15px; text-align: center;"><input type="button" class="button" value="&nbsp; OK &nbsp;" onclick="javascript:updateContact();" /> &nbsp; <input type="button" class="button" value="Cancel" onclick="javascript:closePopUp(true);" /></div>';
	
	showPopUp("Edit Contact", html, 375);
}

function updateContact()
{
	post = "";
	for (var i in cfields)
		post += "&" + cfields[i] + "=" + escape(document.getElementById("edit-" + cfields[i]).value);
	
	postAjax("/updatecontact.php", "cid=" + cediting + post, "updateContactHandler");
}

function updateContactHandler(data)
{
	getUserContacts();
	closePopUp(true);
}

getUserContacts();
