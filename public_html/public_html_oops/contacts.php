<?php

include "inc/inc.php";
$API->requireSite("s");

include "header.php";

?>

<div style="border: 1px solid #d8dfea; border-top: 0;">
	<div class="contentleft piconly">
		<div class="profilepic_container">
			<div class="profilepic" style="text-align: center;">
				<div class="borderhider">&nbsp;</div><div class="borderhider2">&nbsp;</div>
				<a href="<?=$API->getProfileURL()?>"><img width="48" height="48" src="<?=$API->getThumbURL(1, 48, 48, $API->getUserPic())?>" alt="" /></a>
			</div>
		</div>
	</div>

	<div class="content_container_border">
	<?php

	$user = array("uid" => $API->uid, "username" => $API->username, "name" => $API->name);

	$items = array(
			array("/images/email_add.png", "invite contacts", "/invite.php"),
			array("/images/find.png", "find people", "/findpeople.php"),
			array("/images/vcard.png", "contact manager", "/contacts.php")
		);

	showUserWithItems($user, $items);

	?>
	</div>

	<div class="contentright">
		<?php include "inc/connect.php"; ?>
    <? if( $site != "m" && $API->adv ) { ?>
		<div class="subhead" style="padding-top:0px; margin: 0px 0 5px;"><div style="float:left;">Sponsors</div><div style="float:right;"><a href="http://www.salthub.com/adv/create.php" style="font-size:8pt; font-weight:300; color:rgb(0, 64, 128);">create an ad</a>&nbsp;</div><div style="clear:both;"></div></div>
		<?php showAd("companion"); } ?>
		<?php include "inc/pymk.php"; ?>
	</div>

	<div class="content_container_full" style="clear: left;">
		<div class="pageid" style="background-image: url(/images/vcard.png);">Contact Manager</div>
		
		<div style="border: 1px solid #d8dfea; background: #F3F8FB; padding: 5px;">
			<div class="smtitle"><img src="/images/page_add.png" />Find your social contacts and people you e-mail.<span>(recommended)</span></div>
			<?php include "inc/contacts.php"; showContactsTop(); ?>
		</div>

		<div style="border: 1px solid #d8dfea; background: #F3F8FB; padding: 5px; margin-top: 10px;">
			<div class="smtitle"><img src="/images/page.png" />Contacts from <?=$siteName?> and other networks</div>
			<div style="font-size: 9pt;">
				View and manage all your contacts below, even phone numbers.&nbsp; Remember, you can also share videos, photos, and more with contacts not yet on <?=$siteName?>.&nbsp; All you have to do is add them above &ndash; how cool is that?
			</div>
			<div id="contacts_container" style="padding-top: 5px;"></div>
			<div style="clear: both;"></div>
		</div>
	</div>

	<div style="clear: both;"></div>
</div>

<script language="javascript" type="text/javascript">
<!--
var hasSocialSites = [<?=quickQuery("select concat(if(fbsess is null,0,1),',',if(twtoken is null,0,1),',',verify) from users where uid=" . $API->uid)?>];
//-->
</script>

<?php
$scripts[] = "/contacts.js";
$scripts[] = "/invite.js";
include "footer.php";
?>