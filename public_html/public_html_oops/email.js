var mailType;
var mailId;

function showEmail(type, id)
{
	if (!isLoggedIn)
	{
		showPopUp("", '<div style="padding: 25px; font-size: 12pt; text-align: center; font-weight: bold;">Please log in before e-mailing your friends.</div>', 400);
		return false;
	}
	
	mailType = type;
	mailId = id;

	if (type == "S")
		tit = "Share this page by e-mail";
	else if (type == "V")
		tit = "Share this video by e-mail";
	else if (type == "J")
		tit = "Share this job listing by e-mail";
	else
		tit = "Share this photo by e-mail";
	
	html  = '		<div id="email-input" class="emailpopup">';
	html += '			<form name="frmemail">';
	//html += '				<div class="smtitle" id="emailoverlaytitle" style="border-bottom: 2px solid #d8dfea; padding: 0 0 3px 3px; margin-bottom: 5px;">Share this media by e-mail</div>';
	html += '				<div class="embedlabel">To:</div>';
	html += '				<div class="embedinput"><textarea name="email-to" id="email-to" style="height: 45px;"></textarea></div>';
	html += '				<div class="embedlabel">Message:</div>';
	html += '				<div class="embedinput"><textarea name="email-body" style="height: 90px;"></textarea></div>';
	html += '				<div style="clear: both; text-align: right; padding-top: 10px; padding-right: 3px; height: 25px;">';
	html += '					<div id="email-buttons">';
	html += '						<input type="button" value="Cancel" class="button" onclick="javascript:closePopUp(true);" /> &nbsp;';
	html += '						<input type="button" value="Send" class="button" onclick="javascript:sendEmail();" />';
	html += '					</div>';
	html += '					<div id="email-wait" style="display: none; font-size: 9pt;">';
	html += '						<img src="/images/wait326798_w.gif" alt="" style="vertical-align: middle;" />&nbsp; Sending message ...&nbsp;';
	html += '					</div>';
	html += '				</div>';
	html += '			</form>';
	html += '		</div>';
	html += '		<div class="loginmessage" style="display: none;" id="email-message">';
	html += '			Your message has been sent.';
	html += '		</div>';

	showPopUp(tit, html, 440);
	
	return true;
}

function sendEmail()
{
	if (document.getElementById("email-to").value == "")
	{
		alert("You must enter at least one e-mail address.");
		return;
	}
	
	document.getElementById("email-buttons").style.display = "none";
	document.getElementById("email-wait").style.display = "";
	
	postAjax("/emailmedia.php", getFormVals(document.forms.frmemail) + "&type=" + mailType + "&id=" + mailId, "sendEmailHandler");
}

function sendEmailHandler(data)
{
	document.getElementById("email-input").style.display = "none";
	document.getElementById("email-message").style.display = "";
	
	if (data != "OK")
		document.getElementById("email-message").innerHTML = "There was a problem sending your message.";
}
