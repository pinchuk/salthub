<?php
$title = "Phone Upload";
if (isset($_GET['1']))
{
	include "inc/inc.php";
	?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
	<link rel="stylesheet" href="/style.css" type="text/css" />
	<title>Phone Upload | <?=$siteName?></title>

</head>

<body style="background: #d8dfea; margin: 5px;">
	<?php
}
else
	include "header.php";
?>

<div style="width: 554px; background: #d8dfea; <?=isset($_GET['1']) ? "" : "border: 3px solid #fff; padding: 25px; margin: 15px 0 0 13px; "?>float: left;">
	<div style="padding-left: 10px;">
		<div class="bigtext" style="font-size: 18pt;">
			No app required!
			<div style="color: #000;">
				Share media using your phone in real time!
			</div>
		</div>
		<div style="padding: 5px 0 0 5px; color: #555; font-size: 9pt; line-height: 16pt;">
			<ol>
				<?php if (!$API->isLoggedIn()) { ?>
				<li><a href="/?login">Sign into <?=$siteName?></a><?=$site == "m" ? " using Facebook or Twitter" : ""?>.</li>
				<?php } ?>
				<li>
					Go to <a href="/settings"<?=isset($_GET['1']) ? " target=\"_new\"" : ""?>>Settings</a> on <?=$siteName?>, and create your custom e-mail address, and save.
					<div style="padding: 2px 0 0 0px; width: 471px; height: 81px; margin: 5px 0px; font-size: 9pt;">
          <!--<div style="background-image: url(/images/emailsettings.png); padding: 2px 0 0 0px; width: 471px; height: 81px; margin: 5px 0px; font-size: 9pt;">-->
          <img src="images/emailsettings.png" width="471" height="81" alt="" usemap="#learnhowmap"/>
          <map name="learnhowmap">
            <area shape="rect" coords="213,59,293,78" href="/settings"/>
          </map>
					</div>
				</li>
				<li>Select a video or photo from your phone or computer to upload.</li>
				<li>In the subject field, enter the title for your media.</li>
				<li>In the message body, enter the description for the media (limited to 104 characters)</li>
				<li>Send the e-mail, and <?=$siteName?> will instantly process the message and post it in your profile.</li>
			</ol>
			<div style="padding-left: 10px;">
				How cool is that!&nbsp; Your media will now show on Twitter, Facebook, and <?=$siteName?> for all your followers, <? echo ($site=="m") ? 'friends' : 'connections'; ?>, and
				fans to see.<?php if (!$API->isLoggedIn()) { ?>&nbsp; <a href="/?login">Sign in</a> and give it a try.<?php } ?>
			</div>
		</div>
	</div>
	<div style="text-align: center; margin: 20px 0 10px;">
		<img src="/images/phoneemail.jpg" width="530" height="362" alt="" />
	</div>
	
	<?php if (!isset($_GET['1'])) { ?>
	<div style="margin-top: 30px; float: right;">
		<div style="float: left; font-size: 8pt; color: #808080; padding-right: 10px;">share this page</div>
		<div style="float: left;"><a class="addthis_button_facebook"></a></div>
		<div style="float: left;"><a addthis:description="http://<?=$_SERVER['HTTP_HOST']?>/email.php" addthis:title="<?=$media['title']?>" class="addthis_button_twitter"></a></div>
		<div style="float: left;"><a onclick="javascript:showEmail('S', 'email.php');" href="javascript:void(0);"><span class="at300bs at15t_email"></span></a></div>
		<div style="clear: both;"></div>
	</div>
	<?php } ?>
</div>

<?php if (!isset($_GET['1'])) { ?>
<div style="float: right; padding: 64px 2px 0 0;">
	<?php if( $API->adv ) { showAd("companion"); } ?>
</div>
<?php } ?>

<?php
if (isset($_GET['1']))
	echo "</body></html>";
else
{
	$scripts[] = "http://s7.addthis.com/js/250/addthis_widget.js#username=mediabirdy";
	$scripts[] = "/email.js";
	include "footer.php";
}
?>