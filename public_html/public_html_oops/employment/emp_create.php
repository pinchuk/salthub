<?php
/*
Create a job listing, step 1
*/

include "../inc/inc.php";
include "../header.php";

$pid = 0;
$gid = 0;

if( isset( $_REQUEST['ID'] ) )
{
  $ID = intval( $_REQUEST['ID'] );
  $query = sql_query( "select * from jobs where ID='" . intval($_REQUEST['ID']) . "'" . ($API->admin ? '' : " and uid='" . $API->uid ."'" ) );
  $r = mysql_fetch_array( $query );
  $pid = $r['pid'];
  $gid = $r['gid'];
  $hash2 = quickQuery( "select hash from photos where id=" . $r['pid'] );
}

$title_default = "Captains needed";
$body_default  = "Captains needed for driving boats around.";

function getSubTitle( $r )
{
  if( $r['sector'] > 0 )
    echo quickQuery( "select catname from categories where cat='" . $r['sector'] . "'" ) . " | ";
  if( $r['position'] > 0 )
    echo quickQuery( "select gname from pages where gid='" . $r['position'] . "'" ) . " | ";
  if( $r['country'] > 0 )
    echo quickQuery( "select country from loc_country where id='" . $r['country'] . "'" );
}
?>

<? include( "emp_top_menu_admin.php" ); ?>

<form action="emp_create2.php" method="POST" onsubmit="javascript:return submitForm();">
<input type="hidden" name="pid" id="pid" value="0" />
<input type="hidden" name="ID" value="<? echo intval($_REQUEST['ID']) ?>" />
<input type="hidden" name="imageURL" id="imageURL" value="" />
<input type="hidden" name="usePID" id="usePID" value="" />

<div class="contentborder">
  <div style="clear:both;"></div>

  <?
  if( empty( $ID ) ) echo '<div class="strong" style="margin-top:10px; font-size:13pt;">Create a job posting</div>';
  else echo '<div class="strong" style="margin-top:10px; font-size:13pt;">Edit your job posting</div>';
  ?>
  <div style="background-color:#f3f8fb; border:1px solid #d8dfea; width:500px; float:left; padding-bottom:9px; padding-top:6px;">
    <? include( 'emp_create_menu.php' ); ?>

    <div class="thead" style="width:50px;">Post as:</div>
    <div class="tcontent" style="width:360px; padding-top:6px;">
      <div id="yellowbox2" style="color:#555; width:315px; margin-left:15px; background-color:#fff9d7; border-style:solid; border-color:#e2c822; border-width:1px; text-align:center; font-size:9pt; padding:5px; margin-top:10px; margin-bottom:10px;">
        You can post job listings under your personal or company name.  If you don't already have your company set up on <? echo $siteName ?>, we recommend adding it before posting jobs.
      </div>

      <div style="text-align:center; color:#555; width:100%;">
        Posting as

        <span id="pa1">
          <? if( $gid == 0 ) echo quickQuery( "select name from users where uid='" . $API->uid . "'" ); else echo quickQuery( "select gname from pages where gid='" . $gid . "'" ); ?> <span style="font-size:8pt;">(<a href="javascript:void(0);" onclick="document.getElementById('pa1').style.display='none'; document.getElementById('pa2').style.display='';">Change</a>)</span>
        </span>

        <span style="display:none;" id="pa2">
          <select name="gid">
            <option value="0"><? echo quickQuery( "select name from users where uid='" . $API->uid . "'" ); ?></option>
  <?
  $q2 = sql_query( "select pages.gname, pages.gid from page_members left join pages on pages.gid=page_members.gid where page_members.admin=1 and page_members.uid='" . $API->uid . "' order by pages.gname" );
  while( $r2 = mysql_fetch_array( $q2 ) )
  {
    if( $r2['gname'] == "" ) continue;
  ?>
          <option value="<? echo $r2['gid']; ?>"<? if( $r2['gid'] == $r['gid'] ) echo " SELECTED"; ?>><? echo $r2['gname']; ?></option>
  <?
  }
  ?>
          </select>
        </span>

      </div>

      <div style="text-align:center; color:#555; clear:both; margin-top:15px; margin-bottom:10px;">
        <input class="button" type="button" value="Add Company Page" style="width:150px" onclick="javascript: loadGetListed();"/>
      </div>

    </div>

    <div style="clear:both;"></div>

    <div id="yellowbox" style="color:#555; width:315px; margin-left:75px; background-color:#fff9d7; border-style:solid; border-color:#e2c822; border-width:1px; text-align:center; font-size:9pt; padding:5px; margin-top:10px; margin-bottom:10px;">
      Post the link of a job listing from another website above or manually enter the listing information below.
    </div>

    <div class="thead" style="width:50px;">URL:</div>
    <div class="tcontent" style="width:360px;">
      <input type="text" id="sourceurl" maxlength="255" value="" style="width: 350px;" />
    </div>
    <div style="float:left; padding-top:4px;">
      <input type="button" name="" value="Go" onclick="javascript: retrieveUrlData();"/>
    </div>

    <div style="clear:both;"></div>

    <div style="margin:15px 0px 15px 13px; width:95%; border-bottom: 1px dotted rgb(50, 103, 152);"></div>

    <div class="thead" style="width:200px; text-align:left; padding-left: 15px; margin-bottom:10px;">Your job posting</div>


    <div class="thead" style="width:50px;">Title:</div>
    <div class="tcontent" style="width:360px;">
      <input type="text" name="title" id="title" style="width: 350px;" value="<?if( isset( $r ) ) echo $r['title']; else echo $title_default; ?>" onchange="javascript: refreshListing();" onkeyup="javascript: refreshListing();" onclick="javascript: if( this.value=='<?=$body_default?>' ) this.value='';"/>
    </div>

    <div class="thead" style="width:50px;">Image:</div>
    <div class="tcontent" style="width:360px;">
     	<div id="pccontainer" style="padding-left: 0px; float: left;"></div>
      <!--<input name="image" type="file" size="28"/>-->
    </div>

    <div class="thead" style="width:50px;">Body:</div>
    <div class="tcontent" style="width:360px;">
      <textarea id="body" name="body" style="width:350px; height:70px;" onchange="javascript: refreshListing();" onkeyup="javascript: refreshListing();" onclick="javascript: if( this.value=='<?=$body_default?>' ) this.value='';"><?if( isset( $r ) ) echo $r['body']; else echo $body_default; ?></textarea>
    </div>

    <div class="thead" style="width:50px;">URL:</div>
    <div class="tcontent" style="width:360px;">
      <input type="text" name="url" id="url" maxlength="255" style="width: 350px;" value="<?if( isset( $r ) ) echo $r['url']; else echo $www_default; ?>" onclick="javascript: if( this.value=='<?=$www_default?>' ) this.value='';"/>
    </div>

    <div style="clear:both;"></div>

    <div class="strong" style="color: #326798; margin-top:3px; margin-left:40px; font-size:13pt;">Job Listing Preview</div>

    <div style="background-color:#ffffff; border:1px solid #d8dfea; margin-left:40px; width:430px; height:150px; overflow:hidden;">
      <div class="job_listing" style="padding:5px;">
        <div class="image" id="limg"><? if( isset( $r ) ) echo '<img src="/img/100x75/photos/' . $r['pid'] . '/' . $hash2 . '.jpg" width="100" height="75" />'; else echo '<img src="/images/salt_badge100x75.png" width="100" height="75" />'; ?></div>
        <div class="title" id="ltitle"><?if( isset( $r ) ) echo $r['title']; else echo $title_default?></div>
        <div class="subtitle" id="lsubtitle"><?if( isset( $r ) ) echo getSubTitle( $r ); ?></div>
        <div class="body" id="lbody"><? if( isset( $r ) ) echo nl2br($r['body']); else echo $body_default?></div>

        <div id="photoSwitch" style="float:left; clear:left; display:none;">
          <div style="float:left; width:45px; text-align:center;"><input style="margin-left:3px; margin-top:10px;" type="button" class="button" name="prev" value=" &lt; " onclick="rotateImage(-1);" /></div>
          <div style="float:left; width:45px; text-align:center;"><input style="margin-left:3px; margin-top:10px;" type="button" class="button" name="next" value=" &gt; " onclick="rotateImage(1);"/></div>
          <div style="clear:both;"></div>
        </div>


        <div style="clear:both;"></div>


      </div>
    </div>

    <div style="clear:both;"></div>

  </div>

  <div id="myjobs">
  <? include( "my_jobs.php" ); ?>
  </div>

  <div style="clear:left;"></div>


  <div style="float:left;">
    <input style="margin-left:3px; margin-top:10px;" type="submit" class="button" name="next" value="Next &gt;" />
    <input style="margin-left:3px; margin-top:10px;" type="submit" class="button" name="save" value="Save" />
  </div>

  <div style="clear:left;"></div>

</div>

</form>

<script type="text/javascript">
<!--
var pid = <? echo $pid ?>;
var usePID = true;
var images = Array();
var imageIndex = 0;

function loadPage( p )
{
  getAjax("/employment/my_jobs.php?p=" + p, "updateMyJobs");
}


function updateMyJobs(data)
{
  e = document.getElementById( "myjobs" );
  if( e )
    e.innerHTML = data;
}

function retrieveUrlData( )
{
  e = document.getElementById( "sourceurl" );
  if( e )
    url = e.value;
  else
    return;

  post = "url=" + encodeURIComponent( url );
  postAjax("/employment/getJobData.php", post, "responseHandler");

  urle = document.getElementById('url');
  if( urle && urle.value == "" )
  {
    urle.value = url;
  }

  e = document.getElementById("yellowbox");
  if( e )
  {
    e.innerHTML = '<img src="/images/wait.gif" width="50" height="50" />';
  }
}

function responseHandler( data )
{
  chunks = data.split( String.fromCharCode(1) );

  title = document.getElementById('title');
  body = document.getElementById('body');

  if( title )
    title.value = chunks[0];

  if( body )
    body.value = chunks[1];

  if( typeof chunks[2] != "undefined"  )
  {
    images = chunks[2].split( String.fromCharCode(2) );
  }
  imageIndex = 0;

  e = document.getElementById("yellowbox");
  if( e )
  {
    e.innerHTML = 'Information from the provided URL has been retrieved.  Please review the title and description to verify the text is correct.';
  }

  togglePhotoSwitch( true );
  refreshListing();
}

function nl2br (str, is_xhtml) {
  var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '' : '<br>';
  return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
}

function refreshListing()
{
  title = document.getElementById('ltitle');
  body = document.getElementById('lbody');
  img = document.getElementById('limg');

  srctitle = document.getElementById('title');
  srcbody =  document.getElementById('body');

  if( title && srctitle && srctitle.value != '' )
  {
    title.innerHTML = srctitle.value;
  }

  if( body && srcbody && srcbody.value != ''  )
  {
//    if( srcbody.value.length > 150 )
//      srcbody.value = srcbody.value.substring( 0, 149 );
    body.innerHTML = nl2br( srcbody.value, false );
  }

}

function togglePhotoSwitch( on )
{

  e = document.getElementById( "photoSwitch" );
  if( !e )
    return;

  if( on && images.length > 0 )
  {
    imageIndex = 0;
    e.style.display='';
    usePID = false;
    rotateImage(0);
  }
  else
  {
    e.style.display='none';
    usePID = true;
  }
}

function rotateImage( rot )
{
  imageIndex += rot;

  if( imageIndex < 0 ) imageIndex = images.length-1;
  if( imageIndex >= images.length ) imageIndex = 0;

  e = document.getElementById("limg");
  if( e )
  {
    e.innerHTML = '<img src="' + images[imageIndex] + '" width="' + pcPhotoWidth + '" height="' + pcPhotoHeight + '"/>';
  }
}

function submitForm()
{
  title = document.getElementById('title');
  body = document.getElementById('body');
  url = document.getElementById('url');
  pide = document.getElementById('pid');
  usePIDe = document.getElementById('usePID');
  imageURL = document.getElementById('imageURL');

  pide.value = pid;

  if( usePID )
    usePIDe.value = 1;
  else
  {
    usePIDe.value = 0;
    imageURL.value = images[imageIndex];
  }

  if( name.value == "" ) { alert('You must enter a name for your job listing'); return false; }
  if( title.value == "" ) { alert('You must enter a title for your job listing'); return false; }
  if( body.value == "" ) { alert('You must enter a body for your job listing'); return false; }
  if( url.value == "" ) { alert('You must enter a url for your job listing'); return false; }
  if( pid == 0 && usePID ) { alert('You must upload a photo for your job listing'); return false; }

  return true;
}
<?
if( isset( $_REQUEST['sav'] ) )
{
?>
showPopUp('', '<br /><br /><b>Your work has been saved.  You may access it under "Your Job Listings".</b><br /><br /><br />');
<?
}

if( isset( $_REQUEST['off'] ) )
{
?>
showPopUp('', '<br /><br /><b>You listing contains language that violates our terms of service.  Please revise the listing before continuing.</b><br /><br /><br />');
<?
}

if( isset( $_REQUEST['complete'] ) )
{
?>
showPopUp('', '<div style="width:380px; text-align:center; margin:20px;"><b>Your job listing has been successfully saved.<br /><br /></b><input type="button" class="button" onclick="location=\'job_renew.php?ID=<? echo $_REQUEST['complete'] ?>\';" value="Checkout"> <input type="button" class="button" onclick="location=\'jobs_detail.php?d=<? echo $_REQUEST['complete'] ?>\';" value="View Job Listing"><br /><br /><span style="font-weight:bold; font-size:9pt;">Note: Job listings will only run after checkout and approval.</span></div>');
<?
}
?>

-->
</script>
<?
$scripts[] = "/employment/photouploader.js";
include "../footer.php";
?>