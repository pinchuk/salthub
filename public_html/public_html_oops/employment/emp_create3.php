<?php
/*
Create a job listing, step 3
*/

include "../inc/inc.php";

$scripts[] = "/employment/employment.js.php";

$ID = $_POST['ID'];

if( isset( $_POST['save'] ) || isset( $_POST['next'] ) )
{
  $update = array(
    "city = '" . $_POST['city'] . "'",
    "state = '" . $_POST['state'] . "'",
    "country = '" . $_POST['country'] . "'",
    "vessel_type = '" . addslashes($_POST['vessel_type'] ) . "'",
    "vessel_size = '" . addslashes($_POST['vessel_size'] ) . "'",
    "vessel_charter = '" . addslashes($_POST['vessel_charter'] ) . "'",
    "position = '" . addslashes($_POST['position'] ) . "'",
    "sector = '" . addslashes($_POST['sector'] ) . "'"
  );


  mysql_query( "update jobs set " . implode( ",", $update ) . " where id='$ID'" );

  if( isset( $_POST['save'] ) )
  {
    header( "Location: /employment/emp_create.php?sav" );
  }
}

if( $ID == 0 ) $ID = intval( $_REQUEST['ID'] );
$_SESSION['jobid'] = $ID;

$q = mysql_query( "select * from jobs where id='$ID'" );

if( mysql_num_rows( $q ) == 0 )
  die( "invalid job listing!" );

$r = mysql_fetch_array( $q );

include "../header.php";
include( "emp_top_menu_admin.php" );

$hash2 = quickQuery( "select hash from photos where id=" . $r['pid'] );

include( "employment_functions.php" );

?>

<form action="emp_create4.php" method="POST">
<input type="hidden" name="ID" value="<? echo $ID ?>" />

<div class="contentborder">
  <div style="clear:both;"></div>
  <div class="strong" style="margin-top:10px; font-size:13pt;">Create a job posting</div>

  <div style="background-color:#f3f8fb; border:1px solid #d8dfea; margin-top:3px; width:500px; float:left; padding-bottom:9px; padding-top:6px;">
    <? include( 'emp_create_menu.php' ); ?>
    <div style="color:#326798; font-size:9pt; font-weight:bold;  padding-left:5px; padding-top:5px;">Compensation</div>

    <div class="thead" style="width:100px;">Salary:</div>
    <div class="tcontent" style="width:250px; padding-left:0px; margin-left:0px; padding-top:8px;">
<?
$salary = array( "negotiable", "less than 20,000", "20,000 - 30,000", "30,000 - 40,000", "40,000 - 50,000", "50,000 - 60,000", "60,000- 70,000", "70,000 - 80,000", "80,000 - 100,000", "100,000 - 125,000", "125,000 - 150,000", "150,000 - 200,000", "200,000 +");
?>
      <select name="salary" style="width:253px;" id="salary">
<? for( $c = 0; $c < sizeof( $salary ); $c++ ) { ?>
      <option value="<? echo $c ?>"<? if( $r['salary'] == $c ) echo " SELECTED"; ?>><? echo $salary[$c]; ?></option>
<? } ?>
      </select>
    </div>

    <div style="clear:both; padding-top:20px;"></div>
    <div class="thead" style="width:100px;">Beneftis:</div>
    <div class="tcontent" style="width:250px; padding-left:0px; margin-left:0px; padding-top:8px;" id="benefitsBox">
    </div>
<?
$duration = array( "Full Time", "Part Time", "Seasonal", "Contract", "Day Work", "Delivery" );
?>
    <div class="thead" style="width:100px;">Duration:</div>
    <div class="tcontent" style="width:250px; padding-left:0px; margin-left:0px; padding-top:8px;">
      <select name="duration" style="width:253px;" id="duration">
<? for( $c = 0; $c < sizeof( $duration ); $c++ ) { ?>
      <option value="<? echo $c ?>"<? if( $r['duration'] == $c ) echo " SELECTED"; ?>><? echo $duration[$c]; ?></option>
<? } ?>
      </select>
    </div>

    <div style="clear: both; color:#326798; font-size:9pt; font-weight:bold;  padding-left:5px; padding-top:5px;">Company Details</div>

    <div class="thead" style="width:100px;">About the<br />company:</div>
    <div class="tcontent" style="width:250px; padding-left:0px; margin-left:0px; padding-top:5px;">
      <textarea name="about_the_company" style="width:253px; height:80px;"><? echo $r['about_the_company']; ?></textarea>
    </div>


    <div style="clear: both; color:#326798; font-size:9pt; font-weight:bold;  padding-left:5px; padding-top:5px;">Activate Listing</div>

    <div class="thead" style="width:100px;">Status:</div>
    <div class="tcontent" style="width:250px; padding-left:0px; margin-left:0px; padding-top:8px;">

		<select name="complete">
      <option value="2"<? if( $r['complete'] == 2 ) echo " SELECTED"; ?>>Active</option>
      <option value="1"<? if( $r['complete'] == 1 ) echo " SELECTED"; ?>>Disabled</option>
    </select>

    </div>


    <div style="clear:both;"></div>

    <div class="strong" style="color: #326798; margin-top:10px; margin-left:40px; font-size:13pt;">Job Listing Preview</div>

    <div style="background-color:#ffffff; border:1px solid #d8dfea; margin-left:40px; width:430px; height:150px; overflow:hidden;">
      <div class="job_listing" style="padding:5px;">
        <div class="image" id="limg"><? if( isset( $r ) ) echo '<img src="/img/100x75/photos/' . $r['pid'] . '/' . $hash2 . '.jpg" width="100" height="75" />'; else echo '<img src="/images/salt_badge100x75.png" width="100" height="75" />'; ?></div>
        <div class="title" id="ltitle"><?if( isset( $r ) ) echo $r['title']; else echo $title_default?> - <span class="subtitle2"><?if( isset( $r ) ) echo getSubTitle( $r ); ?></span></div>
        <div class="body" id="lbody"><? if( isset( $r ) ) echo nl2br($r['body']); else echo $body_default?></div>
        <div style="clear:both;"></div>
      </div>
    </div>

  </div>

  <? include( "my_jobs.php" ); ?>

  <div style="clear:both;"></div>

  <div style="clear:left; float:left;">
<? if( !quickQuery( "select funded from jobs where id='" . $ID . "'" ) ) { ?>
    <input style="margin-left:3px; margin-top:10px;" type="submit" class="button" name="next" value="Next - Billing"/>
<? } ?>
    <input style="margin-left:3px; margin-top:10px;" type="submit" class="button" name="save" value="Save Ad"/>
  </div>

  <div style="clear:both;"></div>
</div>

</form>

<script type="text/javascript">
<!--
newComboListBox( 21, "benefitsBox" );
-->
</script>
<?
include "../footer.php";
?>