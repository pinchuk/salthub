<?php
/*
List of available (potential) employees.  Users get listed on this site by make themselves available for work
on their user profile page. 

11/5/2011 - Created
*/

//Allow the FB crawler to see this site without logging in.
if( stristr( $_SERVER['HTTP_USER_AGENT'], "facebookexternal" ) !== false )
{
  $facebookCrawler = true;
  $noLogin = true;
}


include( "../inc/inc.php" );

$meta[] = array( "property" => "og:description", "content" => "SaltHub's employment pages are a free resource for professionals who live, work, and engage in and around the water. Find and list shore based and seagoing positions associated with maritime and water related sectors." );
$meta[] = array( "property" => "og:title", "content" => "$siteName employment site" );
$meta[] = array( "property" => "og:url", "content" => "http://" . SERVER_HOST . "/employment/employees.php" );
$meta[] = array( "property" => "og:image", "content" => "http://www.salthub.com/images/employment.png" );
$meta[] = array( "property" => "og:site_name", "content" => $siteName );
$meta[] = array( "property" => "og:type", "content" => "website" );
$meta[] = array( "property" => "fb:app_id", "content" => $fbAppId );

if( $facebookCrawler )
{
  $API->uid = 91;
}

$borderStyle = 1;


include( "../header.php" );


?>

<div style="float: left; <?=$borderStyle == 1 ? "border: 1px solid #d8dfea; border-top: 0;" : ""?>">
  <!-- Left panel -->
	<div class="profileleft" style="<?=$borderStyle == 1 ? "border-left: 0; border-bottom: 0;" : ""?>" id="profileleft">
		<div class="minheight" style="background-image: url(/images/bgprofilepic.png); background-position: 10px 0px; background-repeat: repeat-x;">&nbsp;</div>

		<div class="leftcolumn_container">
			<div class="profilepic" style="text-align: center;">
				<div class="borderhider">&nbsp;</div>

        <div class="profilepic">
          <img src="/images/employment.png" width="178" height="208"/>
        </div>
			</div>

<?
        echo '<div id="sidediv">';
				include "side.php";
        echo '</div>';
?>
		</div>
	</div>


  <!-- Main content -->
	<div class="profilecontent_container" id="profilecontent_container" style="<?=$borderStyle == 0 ? "" : "border-right: 0;"?>">
		<div class="minheight">&nbsp;</div>

		<div class="profilecontent<?=$borderStyle == 0 ? "" : " profilecontent_minus1"?>">
      <? include( "emp_top_menu.php" ); ?>

      <div style="clear:both;"></div>


      <div class="strong" style="margin-left:10px; margin-top:15px; font-size:12pt; margin-bottom:0px; padding-bottom:0px;">
        <div style="float:left; margin-right:5px;"><img src="/images/disconnect.png" width="16" height="16" alt="" /></div>Connect with Employees &amp; Companies
      </div>

      <div style="font-size:9pt; margin-left:10px; margin-right:8px; margin-top:5px;">
      SaltHub's employment pages are a free resource for professionals who live, work, and engage in and around the water. Find and list shore based and seagoing positions associated with maritime and water related sectors.
      </div>

      <div id="searchResults" style="padding-left:10px;">
        <? include( "search.php" ); ?>
      </div>
		</div>

	  <div style="clear: both;"></div>
  </div>
</div>

<div style="float:left;">
  <!-- Advertisements -->
  <div style="width: 135px; float: left; position: relative;">
    <div style="position: absolute; top: 0; left: -3px; background: white; height: 26px;">&nbsp;</div>
    <? if( $API->adv ) { ?>
  	<div style="padding: <?=$site == "s" && $action == "about" ? 26 : 26?>px 0 0 15px;">
  		<?php showAd("skyscraper"); ?>
  	</div>
    <? } ?>
  </div>
</div>

<?php
include "../footer.php";
?>
