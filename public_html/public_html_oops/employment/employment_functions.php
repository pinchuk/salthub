<?
/*
Common PHP functions used throughout the employment site.
*/

function gidToName( $gid, $prefix, $default, $uid = NULL )
{
  global $API;

  $gids = explode(",", $gid );
  $text = "";
  if( sizeof( $gids ) > 0 && $gid != "0" ) $text = $prefix;

//  if( is_null( $uid ) ) echo "sbfb";

  $i = 0;
  for( $c = 0; $c < sizeof( $gids ) && (is_null( $uid ) || $i < 3); $c++ )
  {
    $gid = $gids[$c];
    if( intval( $gid ) == 0 ) continue;
    $gname = quickQuery( "select gname from pages where gid='$gid'" );
    if( $i > 0 ) $text .= ", ";
    $i++;
    $text .= '<a href="' . $API->getPageURL( $gid, $gname ) . '">' . $gname . '</a>';
  }

  if( sizeof( $gids ) > 3 && isset( $uid ) )
    $text .= ', <a href="' . $API->getProfileUrl( $uid ) . '">more</a>';

  if( $text == "")
    return $default;
  else
    return $text;
}

function catToName( $cat, $prefix, $default )
{
  global $API;

  $cats = explode(",", $cat );
  $text = "";
  if( sizeof( $cats ) > 0 && $cat != "0" ) $text = $prefix;

  $i = 0;
  for( $c = 0; $c < sizeof( $cats ); $c++ )
  {
    $cat = $cats[$c];
    if( intval( $cat ) == 0 ) continue;
    $gname = quickQuery( "select catname from categories where cat='$cat'" );
    if( $i > 0 ) $text .= ", ";
    $i++;
    $text .= $gname;
  }

  if( $text == "")
    return $default;
  else
    return $text;
}

function countryToName( $cty, $prefix, $default, $uid = NULL )
{
  global $API;

  $ctys = explode(",", $cty );
  $text = "";
  if( sizeof( $ctys ) > 0 && $cty != "0" ) $text = $prefix;

  $i = 0;
  for( $c = 0; $c < sizeof( $ctys ) && (is_null( $uid ) || $i < 3); $c++ )
  {
    $cty = $ctys[$c];
    if( intval( $cty ) == 0 ) continue;
    $gname = quickQuery( "select country from loc_country where id='$cty'" );
    if( $i > 0 ) $text .= ", ";
    $i++;
    $text .= $gname;
  }

  if( $text == "")
    return $default;
  else
    return $text;
}

function getSubTitle( $r )
{
  global $API;
/*
  if( $r['sector'] > 0 )
    echo quickQuery( "select catname from categories where cat='" . $r['sector'] . "'" ) . " | ";
  if( $r['position'] > 0 )
    echo quickQuery( "select gname from pages where gid='" . $r['position'] . "'" ) . " | ";
*/
  if( $r['gid'] == 0 )
    echo quickQuery( "select name from users where uid='" . $r['uid'] . "'" );
  else
    echo "<a href=\"" . $API->getPageURL($r['gid']) . "\">" . quickQuery( "select gname from pages where gid='" . $r['gid'] . "'" ) . "</a>";

  if( $r['city'] != 0 || $r['state'] != 0 || $r['country'] != 0 ) echo " - ";
  if( $r['city'] > 0 )
    echo ucwords( strtolower(quickQuery( "select city from loc_city where id='" . $r['city'] . "'" ) ) . ", " );
  if( $r['state'] > 0 )
    echo ucwords( strtolower(quickQuery( "select region from loc_state where id='" . $r['state'] . "'" ) ) . " " );
  if( $r['country'] > 0 )
  {
    $country = quickQuery( "select country from loc_country where id='" . $r['country'] . "'" );
    if( strlen( $country ) > 3 ) $country = ucwords(strtolower($country));
    echo $country;
  }
}


?>