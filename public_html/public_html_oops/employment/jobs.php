<?php
/*
Primarily, this script is a container for job_search.php

11/18/2011 - Created
*/

$scripts[] = "/employment/jobs.js.php";
$scripts[] = "http://s7.addthis.com/js/250/addthis_widget.js#username=mediabirdy";


//Allow the FB crawler to see this site without logging in.
if( stristr( $_SERVER['HTTP_USER_AGENT'], "facebookexternal" ) !== false )
{
  $facebookCrawler = true;
  $noLogin = true;
}


include( "../inc/inc.php" );
include( "employment_functions.php" );

$url = "http://" . SERVER_HOST . "/employment/";
$image = "http://www.salthub.com/images/salt_badge100.png";

if( isset( $_REQUEST['d'] ) )
{
  $d = $_REQUEST['d'];
  $q = sql_query( "select * from jobs where ID='$d'" );

  if( $r = mysql_fetch_array( $q ) )
  {
    $hash = quickQuery( "select hash from photos where id=" . $r['pid'] );
    $title = $r['title'];
    $desc = cutOffText( $r['body'],150 );
    $url = "http://" . SERVER_HOST . "/employment/jobs.php?d=" . $d;
    $image = "http://" . SERVER_HOST . "/img/100x75/photos/" . $r['pid'] . "/" . $hash . ".jpg";
  }
}

$meta[] = array( "property" => "og:description", "content" => $desc );
$meta[] = array( "property" => "og:title", "content" => $title );
$meta[] = array( "property" => "og:url", "content" => $url );
$meta[] = array( "property" => "og:image", "content" => $image );
$meta[] = array( "property" => "og:site_name", "content" => $siteName );
$meta[] = array( "property" => "og:type", "content" => "website" );
$meta[] = array( "property" => "fb:app_id", "content" => $fbAppId );

if( $facebookCrawler )
{
  $API->uid = 91;
}

$borderStyle = 1;


//Save previous Searches
$srch_typ = 'emp_search';
$save_cnt = 10;
$found = false;

for( $c = 0; $c < $save_cnt; $c++ )
  if( $_COOKIE[$srch_typ . $c ] == $_SERVER['QUERY_STRING'] )
    $found = true;

if( sizeof( $_SERVER['argv'] ) > 0 && !$found )
{
  for( $c = $save_cnt; $c >= 1 ; $c-- )
    setcookie( $srch_typ . $c, $_COOKIE[$srch_typ . ($c - 1)],time()+60*60*24*30 );
  setcookie( $srch_typ . '0', $_SERVER['QUERY_STRING'],time()+60*60*24*30 );
}

//
$title = ""; $descr = ""; //Use default title and description
include( "../header.php" );
?>

<div style="float: left; <?=$borderStyle == 1 ? "border: 1px solid #d8dfea; border-top: 0;" : ""?>">
  <!-- Left panel -->
	<div class="profileleft" style="<?=$borderStyle == 1 ? "border-left: 0; border-bottom: 0;" : ""?>" id="profileleft">
		<div class="minheight" style="background-image: url(/images/bgprofilepic.png); background-position: 10px 0px; background-repeat: repeat-x;">&nbsp;</div>

		<div class="leftcolumn_container">
			<div class="profilepic" style="text-align: center;">
				<div class="borderhider">&nbsp;</div>

        <div class="profilepic">
          <img src="/images/employment.png" width="178" height="208"/>
        </div>
			</div>

<?
        echo '<div id="sidediv">';
				include "jobs_side.php";
        echo '</div>';
?>
		</div>
	</div>


  <!-- Main content -->
	<div class="profilecontent_container" id="profilecontent_container" style="<?=$borderStyle == 0 ? "" : "border-right: 0;"?>">
		<div class="minheight">&nbsp;</div>

		<div class="profilecontent<?=$borderStyle == 0 ? "" : " profilecontent_minus1"?>">
      <? include( "emp_top_menu.php" ); ?>

      <div style="clear:both;"></div>


      <div class="strong" style="margin-left:10px; margin-top:15px; font-size:12pt; margin-bottom:0px; padding-bottom:0px;">
       <div style="float:left; margin-right:5px;"><img src="/images/disconnect.png" width="16" height="16" alt="" /></div>Connect with Employees &amp; Companies
      </div>

      <div style="font-size:9pt; margin-left:10px; margin-right:8px; margin-top:5px;">
      SaltHub's employment pages are a free resource for professionals who live, work, and engage in and around the water. Find and list shore based and seagoing positions associated with maritime and water related sectors.
      </div>

      <div class="subhead" style="margin-left:10px; cursor:pointer; margin-top: 10px; margin-bottom:-10px; margin-right:8px;">
        Job Listings
      </div>

      <div id="searchResults" style="padding-left:10px; margin-top:10px;">
        <? include( "jobs_search.php" ); ?>
      </div>
		</div>

	  <div style="clear: both;"></div>
  </div>
</div>

<div style="float:left;">
  <!-- Advertisements -->
  <div style="width: 135px; float: left; position: relative;">
    <div style="position: absolute; top: 0; left: -3px; background: white; height: 26px;">&nbsp;</div>
    <? if( $API->adv ) { ?>
  	<div style="padding: <?=$site == "s" && $action == "about" ? 26 : 26?>px 0 0 15px;">
  		<?php showAd("skyscraper"); ?>
  	</div>
    <? } ?>
  </div>
</div>

<script type="text/javascript">
<!--
<?
$query = "";

foreach ($_GET as $k => $v)
	if ($k != "p")
		$query .= "&" . htmlentities("$k=$v");
?>
function changePage( p )
{
  getAjax("/employment/jobs_search.php?p=" + p + "<?=$query?>", "changePageResponse");
}
-->
</script>


<?
include "../footer.php";
?>
