<?
/*
Details about a job listing.  Also contains a bunch of information about other users who are associated with this company.

11/24/2011 - Created
*/

$noLogin = true;

$scripts[] = "/employment/jobs.js.php";

//Allow the FB crawler to see this site without logging in.
if( stristr( $_SERVER['HTTP_USER_AGENT'], "facebookexternal" ) !== false )
{
  $facebookCrawler = true;
}

include( "../inc/inc.php" );

include( "employment_functions.php" );

$ID = $_GET['d'];

$sql = "select * from jobs where ID='" . $ID . "'";
$q = sql_query( $sql );

if( !($r = mysql_fetch_array( $q )) )
{
  die( "Invalid job" );
}




$hash = quickQuery( "select hash from photos where id=" . $r['pid'] );
$title = $r['title'];
$desc = cutOffText( $r['body'],150 );
$url = "http://" . SERVER_HOST . "/employment/jobs.php?d=" . $ID;
$image = "http://" . SERVER_HOST . "/img/100x75/photos/" . $r['pid'] . "/" . $hash . ".jpg";

$meta[] = array( "property" => "og:description", "content" => $desc );
$meta[] = array( "property" => "og:title", "content" => $title );
$meta[] = array( "property" => "og:url", "content" => $url );
$meta[] = array( "property" => "og:image", "content" => $image );
$meta[] = array( "property" => "og:site_name", "content" => $siteName );
$meta[] = array( "property" => "og:type", "content" => "website" );
$meta[] = array( "property" => "fb:app_id", "content" => $fbAppId );


$descr = "";
$title = $r['title'] . " | ";
if( $r['position'] > 0 )
{
  $descr = quickQuery( "select gname from pages where gid='" . $r['position'] . "'" ) . " | ";
  $title .= $descr;

}
$title .= "Employment | $siteName";
$descr .= $r['title'];

if( $facebookCrawler )
{
  $API->uid = 91;
}

include( "../header.php" );

$hash = quickQuery( "select hash from photos where id=" . $r['pid'] );

if( $r['gid'] == 0 )
  $company = quickQuery( "select name from users where uid='" . $r['uid'] . "'" );
else
  $company = "<a href=\"/page/" . $r['gid'] . "-a\">" . quickQuery( "select gname from pages where gid='" . $r['gid'] . "'" ) . "</a>";

$url = "http://" . SERVER_HOST . "/employment/jobs_detail.php?d=" . $ID;

if( !$API->isLoggedIn() ) {
  $_SESSION['login_redirect'] = $url;
}


if( $r['uid'] != $API->uid && !$API->admin && $r['complete'] != 2 )
{
  echo "Sorry, this job listing has currently inactive.";
  include "../footer.php";
  exit;
}

?>


  <!-- Main content -->
  <div style="width:100%;">

		<div style="width:100%;">
      <div style="width:100%; overflow: visible; position: relative;">    <!--563px-->
        <div class="phead" style="width: 100%;">
        	<div class="username" style="width: 100%;">
            <div style="float:left; overflow:visible; margin-right:5px;">
              <a href="<?=$API->getProfileURL( $r['uid'] )?>"><img src="/img/100x75/photos/<? echo $r['pid'] . "/" . $hash . ".jpg"; ?>" width="100" height="75"/></a>
            </div>
            <? $size = 18;  if( strlen( $r['title'] ) > 40 ) $size = 14; ?>
        		<div class="strong" style="font-size:<?=$size;?>pt; float:left; padding-top:5px; width:830px; overflow:hidden;">
              <? echo $r['title']; ?>
        		</div>
        	</div>

          <div style="height:28px; background-image: url(/images/pix_d8dfea.png); background-position: 0 26px; background-repeat: repeat-x; width:100%; margin-left:-10px;"></div>
        </div>
        <div style="margin-left: 105px; padding-left: 10px; clear:both; width:550px; font-size:10pt; padding-top:3px;">
<? if( $API->isLoggedIn() ) { ?>
          <div style="float:left;"><div style="float:left; margin-top:1px;"><img src="/images/arrow_rotate_anticlockwise.png" width="16" height="16"/></div>&nbsp;<a href="/employment/jobs.php?q=">back to job listings</a></div>

          <div style="float:left; margin-left:15px;"><div style="float:left; margin-top:2px;"><img src="/images/shape_flip_horizontal.png" width="16" height="16"/></div>&nbsp;<a href="/employment/jobs.php?position=<? echo $r['position'] ?>">similar jobs</a></div>

          <div style="float:left; margin-left:15px;">|</div>
<? if( $API->admin || $r['uid'] == $API->uid ) { ?>
          <div style="float:left; margin-left:15px;"><div style="float:left; margin-top:1px;"><img src="/images/disk.png" width="16" height="16"/></div>&nbsp;<a href="emp_create.php?ID=<?=$r['id'];?>">edit job</a></div>
<? } else { ?>
          <div style="float:left; margin-left:15px;"><div style="float:left; margin-top:1px;"><img src="/images/disk.png" width="16" height="16"/></div>&nbsp;<a href="javascript:void(0);" onclick="javascript:saveJob(<?=$r['id'];?>);">save job</a></div>
<? } ?>
          <div style="float:left; margin-left:15px;"><div style="float:left; margin-top:1px;"><img src="/images/tag_add.png" width="16" height="16"/></div>&nbsp;<a href="/employment/jobs.php?sj">my saved jobs</a></div>
<? } else { ?>
          <div style="float:left;">&nbsp;</div>
<?
}
?>
        </div>
      </div>

  </div>

	<div style="width:100%; border-left:1px solid #d8dfea; border-right:1px solid  #d8dfea; border-bottom:1px solid  #d8dfea; z-index:100; margin-top:-3px; font-size:9pt;">
    <div style="clear:both; margin-left:5px; padding:5px; float:left; width:530px; padding-top:40px;">

<?
if( ( $r['complete'] != 2 || $r['approved'] != 1 || $r['funded'] == 0 ) && $r['uid'] == $API->uid )
{
?>
    <table cellpadding="5" cellspacing="0" bgcolor="#FFF9D7" style="border: 1px solid #E2C822; margin-top:10px;" width="550"><tr><td><div style="padding: 9px 6px; font-weight: bold; font-size: 10pt; color: #000;">
  <? if( $r['complete'] != 2 || $r['approved'] != 1 ) { ?>
    This job posting is pending review. If you have any questions about this posting, please <a href="javascript:void(0);" onclick="javascript: openSendMessagePopup('','','<?=$siteName?>',1187301,0,0,0 );">send a message</a> to the <?=$siteName?> team.
  <? } else if( $r['funded'] == 0 ) { ?>
    This job posting is currently inactive. Click <a href="job_renew.php?ID=<?=$r['id']?>">here</a> to activate it.
  <? } ?>
    </div></td></tr></table> <br />
<?
}
?>


        <div class="strong" style="font-size:16pt;">
          <a href="/page/<? echo $r['position'] . "-a"; ?>" style="color:#555;"><? $position = quickQuery( "select gname from pages where gid='" . $r['position'] . "'" ); echo $position; ?></a> <span style="font-weight:300; font-size:11pt;">- <? echo getSubTitle( $r ); ?></span>
        </div>

        <table border="0" cellpadding="0" cellspacing="0" class="mediaactions" style="margin-top:10px">
        	<tr>
        		<td style="padding-right: 20px; padding-top:5px;">
              <a type="button_count" href="javascript:void(0);" onclick="javascript: openPopupWindow( 'http://www.facebook.com/sharer/sharer.php?u=<? echo urlencode("http://" . SERVER_HOST . "/employment/jobs_detail.php?d=" . $r['id'] ); ?>', 'Share', 550, 320 );">
                <img src="/images/facebook_share.png" width="58" height="20" alt="" />
              </a>
            </td>

        		<td>
              <a href="http://twitter.com/share" class="twitter-share-button" data-url="<? echo "http://" . SERVER_HOST . "/employment/jobs_detail.php?d=" . $r['id']; ?>" data-text="<? echo $r['title']; ?>" data-count="horizontal" data-via="<?=SITE_VIA?>">Tweet</a>
            </td>
        	</tr>
        </table>


<? if( $API->isLoggedIn() ) { ?>
        <div style="clear:both; width:550px; font-size:10pt; padding-top:10px;">
          <div style="float:left; width:100px;"><div style="float:left; margin-top:1px;"><img src="/images/profile_add.png" width="16" height="16"/></div>&nbsp;<span id="add2log" style="color:#326798;"><a href="javascript:void(0);" onclick="javascript:addJobToLog(<? echo $r['id']; ?>);">add to my log</a></span></div>

          <div style="float:left; margin-left:15px;"><div style="float:left; margin-top:2px;"><img src="/images/email_add.png" width="16" height="16"/></div>&nbsp;<a href="javascript:void(0);" onclick="loadShare('compose', 1, {'body':'<? echo urlencode($url); ?>','subj':'<? echo urlencode($r['title']); ?>'});">share / e-email</a></div>

          <div style="float:left; margin-left:15px;"><div style="float:left; margin-top:1px;"><img src="/images/link_add.png" width="16" height="16"/></div>&nbsp;<a href="javascript:void(0);" onclick="javascript:shareLink(<?=$r['id'];?>);">share link</a></div>

          <div style="float:left; margin-left:15px;">|</div>

          <div style="float:left; margin-left:15px;"><div style="float:left; margin-top:1px;"><img src="/images/report.png" width="16" height="16"/></div>&nbsp;<a href="javascript:void(0);" onclick="javascript:showReport('j', <?=$r['id']?>);">report</a></div>

          <div style="clear:both;"></div>
        </div>
<? } ?>

        <div class="strong" style="clear:both; font-size:14pt; margin-top:20px;">
          About the Job
        </div>
<?
$found = false;

if( $r['gid'] > 0 && $API->isLoggedIn())
{
  $friends = $API->getFriendsOfFriends(false);

  if( sizeof( $friends ) > 0 )
  {
    $conn_q = sql_query( "SELECT page_members.uid FROM page_members WHERE page_members.gid='" . $r['gid'] . "' and page_members.uid IN (" . implode( ",", $friends ) . ")" );
    if( mysql_num_rows( $conn_q ) > 0 )
    {
      $found = true;
      echo '<span style="font-size:8pt;">You have ' . mysql_num_rows( $conn_q ) . ' potential link' . (mysql_num_rows( $conn_q ) > 1 ? 's' : '') . ' to this company - <a href="javascript:void(0);" onclick="javascript:showLinksToPage(' . $r['gid'] . ');">view them now</a></span><div style="clear:both;">';
      $i=0;
      while( ($conn_r = mysql_fetch_array( $conn_q )) && ($i++ < 6 ) )
      {
        $purl = $API->getProfileURL( $conn_r['uid'] );
        $data = queryArray( "select pic, twusername, fbid, name from users where uid='" . $conn_r['uid'] . "'" );
       	$parts = array(rawurlencode($purl), rawurlencode($API->getUserPic($conn_r['uid'], $data[0])), rawurlencode($data['twusername']), rawurlencode($data['fbid']), rawurlencode($data['name']));
      	$id = implode("-", $parts);
        echo '<div style="float:left; padding-left:3px;">';
        echo '<a id="' . md5(microtime()) . "-" . $id . '" href="' . $purl . '"  title="' . $fids . '" onmouseover="javascript:showTip(3, this);" onmouseout="javascript:tipMouseOut();"><img src="/img/48x48' . $API->getUserPic( $conn_r['uid'] ) . '" width="48" height="48"></a>';
        echo '</div>';
      }
      echo '</div><div style="clear:both; height:3px;"></div>';
    }
  }
}
else if( !$API->isLoggedIn() )
{
  echo '<span style="font-size:8pt;">' . $siteName . ' jobs are social. <a href="http://www.' . $siteName . '.com/">Create an account</a> to connect with others and join pages to build your network.<br /> You never know who can help you land your next job!.</span><br />';
  for( $c = 0; $c < 4; $c++ )
    echo '<div style="float:left; padding-left:3px;"><img src="/images/nouser.jpg" width="48" height=48"></div>';
  echo '<div style="clear:both;"></div>';
}

if( !$found && $API->isLoggedIn() )
{
  echo '<span style="font-size:8pt;">' . $siteName . ' jobs are social. Connect with others and join <a href="/pages">pages</a> to build your network.<br /> You never know who can help you land your next job!</span><div style="clear:both;">';

  for( $c = 0; $c < 5; $c++ )
  {
    echo '<div style="float:left; padding-left:3px;">';
    echo '<img src="/images/nouser.jpg" width="48" height="48">';
    echo '</div>';
  }
  echo '</div><div style="clear:both; height:3px;"></div>';
}


$salary = array( "negotiable", "less than 20,000", "20,000 - 30,000", "30,000 - 40,000", "40,000 - 50,000", "50,000 - 60,000", "60,000- 70,000", "70,000 - 80,000", "80,000 - 100,000", "100,000 - 125,000", "125,000 - 150,000", "150,000 - 200,000", "200,000 +");
$duration = array( "Full Time", "Part Time", "Seasonal", "Contract", "Day Work", "Delivery" );
$benefits = array("Health Plan", "Retirement Plan", "Profit Sharing", "Company Car", "Expense Account", "Paid Vacation", "Overtime");
$charter = array( "Charter", "Private", "Both" );
$size = array( "0 - 35 ft (0 - 10m)", "36 - 65 ft (10 - 19m)", "66 - 90 ft (19 - 27m)", "91 - 130 ft (27 - 39m)", "131 - 150 ft (39 - 45m)", "151 - 180 ft (45 - 54m)", "181 - 200 ft (54 - 60m)", "201 - 250 ft (60 - 76m)", "251 - 300 ft (76 - 91m)", "301 - 400 ft (91 - 121m)", "401 - 600 ft (121 - 182m)", "601 - 1000 ft (182 - 304m)", "1000ft+ (304m+)" );

$city = ucwords( strtolower(quickQuery( "select city from loc_city where id=" . $r['city'] ) ) );
$state = ucwords( strtolower(quickQuery( "select region from loc_state where id=" . $r['state'] ) ) );
$country = quickQuery( "select country from loc_country where id=" . $r['country'] );
if( strlen( $country ) > 3 ) $country = ucwords( strtolower( $country ) );

$benefit_items = explode( ",", $r['benefits'] );
$benefits_text = "";
for( $c = 0; $c < sizeof( $benefit_items ); $c++ )
{
  if( $benefit_items[$c] != "" )
  {
    if( strlen( $benefits_text ) > 0 ) $benefits_text .= ", ";
    $benefits_text .= $benefits[ $benefit_items[$c] ];
  }
}

?>

        <table border="0" cellpadding="0" cellspacing="2" style="margin-top:5px; font-size:10pt;">
        <tr><td width="100"><b>Company:</b></td><td><?= $company ?></td></tr>
        <tr><td><b>Location:</b></td><td><? if( $r['city'] > 0 ) echo $city . ", "; if( $r['state'] > 0 ) echo $state . " "; if( $r['country'] > 0 ) echo $country;?></td></tr>
        <tr><td><b>Sector:</b></td><td><? echo quickQuery( "select catname from categories where cat=" . $r['sector'] ); ?></td></tr>
        <tr><td><b>Position:</b></td><td><? if( $API->isLoggedIn() ) { ?><a href="/page/<? echo $r['position'] . "-a"; ?>"><? } ?><? echo quickQuery( "select gname from pages where gid='" . $r['position'] . "'" ); ?><? if( $API->isLoggedIn() ) { ?></a><? } ?></td></tr>
        <tr><td><b>Salary:</b></td><td><? echo $salary[ $r['salary'] ]; ?></td></tr>
        <tr><td><b>Benefits:</b></td><td><? echo $benefits_text; ?></td></tr>
        <tr><td><b>Duration:</b></td><td><? echo $duration[ $r['duration'] ]; ?></td></tr>
        <tr><td><b>Posted:</b></td><td><? echo timeToHumanTime( strtotime($r['created']) ); ?> ago</td></tr>
        </table>

<? if( $r['vessel_type'] > 0 ) { ?>
        <div class="strong" style="font-size:14pt; margin-top:20px;">
          Vessel Details
        </div>

        <table border="0" cellpadding="0" cellspacing="2" style="margin-top:5px; font-size:10pt;">
        <tr><td width="100"><b>Type:</b></td><td><? echo quickQuery( "select catname from categories where cat=" . $r['vessel_type'] ); ?></td></tr>
        <tr><td><b>Size:</b></td><td><? echo $size[ $r['vessel_size'] ]; ?></td></tr>
        <tr><td><b>Private or Charter:</b></td><td valign="bottom"><? echo $charter[$r['vessel_charter']]; ?></td></tr>
        </table>
<? } ?>
        <div class="strong" style="font-size:14pt; margin-top:20px;">
          Job Description
        </div>

        <div style="margin-top:10px;"><b>About Us:</b></div>
        <div><? echo nl2br($r['about_the_company']);?></div>

        <div style="margin-top:20px;"><b>Responsibilities:</b></div>
        <div><? echo nl2br($r['body']);?></div>

<? if( $API->isLoggedIn() ) { ?>
        <div style="margin-top:20px; text-align:center; margin-bottom:20px;">
          <input type="button" class="orangebutton" name="" value="Apply Now" onclick="javascript: showSendMessage2(<? echo $r['uid']; ?>, '<?=quickQuery( "select name from users where uid='" . $r['uid'] . "'" );?>', '/img/48x48<?= $API->getUserPic( $r['uid'] ); ?>', '<? echo $r['title'] ?>', '');"/>
        </div>
<? } ?>
    </div>

    <div style="float:right; width:300px; margin-top:-50px; margin-left:10px; padding-top:40px; margin-right:10px;">
<? if( $API->isLoggedIn() ) { ?>
  		<div class="subhead" style="margin-bottom: 10px;">Connect</div>

      <div style="background-color:#f3f8fb; border:1px solid #a1a1a1; padding:15px; width:267px; margin-bottom:10px;">
        <div>
          <div style="float:left; width:25px;"><img src="/images/application_form_add.png" width="16" height="16" alt="" /></div>
          <div style="float:left; font-weight: bold;">Posted by: <a href="<? echo $API->getProfileURL($r['uid']); ?>"><? echo quickQuery( "select name from users where uid=" . $r['uid'] ); ?></a></div>
        </div>

        <div style="float:left; clear:both; margin-left:30px; margin-top:3px;">
          <a href="<? echo $API->getProfileURL($r['uid']); ?>"><img src="/img/48x48<?= $API->getUserPic( $r['uid'] ); ?>" width="48" height="48"></a>
        </div>
        <div style="float:left; margin-left:20px; margin-top:3px;">

          <div style="float:left; margin-right:5px;"><? if( $r['uid'] != $API->uid ) { ?><img src="/images/add_connection_green.png" width="16" height="16" alt="" /><? } ?></div>
          <div style="float:left; padding-top:3px;"><? echo friendLink($r['uid']); ?></div>
          <div style="clear:both; padding-top:5px;"></div>
          <div style="float:left; margin-right:5px; padding-top:2px;"><img src="/images/email_add.png" width="16" height="16" alt="" /></div>
          <div style="float:left; padding-top:3px;"><a href="javascript:void(0);" onclick="javascript:showSendMessage(<? echo $r['uid']; ?>, '<?=quickQuery( "select name from users where uid='" . $r['uid'] . "'" );?>', '/img/48x48<?= $API->getUserPic( $r['uid'] ); ?>' );">Send message</a></div>
        </div>

        <div style="clear:both;"></div>

<? if( $r['gid'] > 0 ) {
  $page_url = $API->getPageURL( $r['gid'] );
  $company = quickQuery( "select gname from pages where gid=" . $r['gid'] );
?>
        <div style="width:294; margin-left:3px; margin-top:10px; margin-bottom:10px; border-bottom: 1px solid #d8dfda;"></div>

        <div>
          <div style="float:left; width:25px;">&nbsp;</div>
          <div style="float:left; font-weight: bold;">Company: <a href="<? echo $API->getPageUrl($r['gid']); ?>"><? echo $company ?></a></div>
        </div>

        <div style="float:left; clear:both; margin-left:30px; margin-top:3px;">
          <a href="<? echo $API->getPageURL($r['gid']); ?>"><img src="/img/48x48<?= $API->getUserPic( $r['uid'], quickQuery( "select pid from pages where gid=" . $r['gid'] ) ); ?>" width="48" height="48"></a>
        </div>
        <div style="float:left; margin-left:20px; margin-top:3px;">

          <? if( quickQuery( "select count(*) from page_members where uid='" . $API->uid . "' and gid='" . $r['gid'] . "'" ) == 0 ) { ?>
          <div style="float:left; margin-right:5px;"><img src="/images/add_connection_green.png" width="16" height="16" alt="" /></div>
          <div style="float:left; padding-top:3px;"><a href="<?=$page_url ?>?join">Connect</a></div>
          <div style="clear:both; padding-top:5px;"></div>
          <? } ?>

          <div style="float:left; margin-right:5px;"><img src="/images/layout_header.png" width="16" height="16" alt="" /></div>
          <div style="float:left; padding-top:3px;"><a href="<?=$page_url ?>">Company page</a></div>
          <div style="clear:both; padding-top:5px;"></div>

          <div style="float:left; margin-right:5px;"><img src="/images/wrench.png" width="16" height="16" alt="" /></div>
          <div style="float:left; padding-top:3px;"><a href="jobs.php?company=<?=$company?>">All company jobs</a></div>
        </div>

<? } ?>
        <div style="clear:both;"></div>

        <div style="margin-top:5px; text-align:center;">
          <input type="button" class="orangebutton" name="" value="Apply Now" onclick="javascript: showSendMessage2(<? echo $r['uid']; ?>, '<?=quickQuery( "select name from users where uid='" . $r['uid'] . "'" );?>', '/img/48x48<?= $API->getUserPic( $r['uid'] ); ?>', '<? echo $r['title'] ?>', '');"/>
        </div>

      </div>

      <div style="clear:both;"></div>
<? } else { ?>
	<div class="subhead" style="margin-top: 10px; margin-bottom:10px;">Login to Apply</div>

	<div style="width: 280px; margin-top:3px; background-color:#f3f8fb; padding:10px;">
			<div style="padding-left: 15px;">
				<div style="font-family: arial; color: #555555; font-size: 16pt; font-weight: bold;">Apply Now</div>
				<div style="font-family: arial; color: #555555; font-size: 8pt; margin-top: 5px; width: 240px;">
					Meet new people and connect with the ones you already know.
				</div>
			</div>
			<table border=0 cellpadding=0 cellspacing=0 style="margin-top: 13px;">
				<form method="post" action="/signup/entry.php" id="frmsignup">
				<input type="hidden" name="recaptcha_challenge_field" value="" />
				<input type="hidden" name="recaptcha_response_field" value="" />
				<tr>
					<td style="text-align: right; padding-bottom: 5px; padding-right: 5px; font-family: arial; font-weight: bold; font-size: 8pt; color: #555555;">Your Name:</td>
					<td style="padding-bottom: 5px;"><input type="text" maxlength="16" name="uname" id="newuname" value="<?=htmlentities($_POST['uname'])?>" style="border: 1px solid #7F9DB9; width: 132px;"></td>
				</tr>
				<tr>
					<td style="padding-right: 5px; padding-bottom: 5px; text-align: right; font-family: arial; font-weight: bold; font-size: 8pt; color: #555555;">Password:</td>
					<td style="padding-bottom: 5px;"><input type="password" maxlength="16" name="password" style="border: 1px solid #7F9DB9; width: 132px;"></td>
				</tr>
				<tr>
					<td style="padding-bottom: 5px; padding-right: 5px; font-family: arial; font-weight: bold; font-size: 8pt; color: #555555;">E-mail Address:</td>
					<td style="padding-bottom: 5px;"><input type="text" name="email" value="<?=$emailEntered?>" style="border: 1px solid #7F9DB9; width: 132px;"></td>
				</tr>
				<tr>
					<td style="padding-right: 5px; text-align: right; font-family: arial; font-weight: bold; font-size: 8pt; color: #555555;">Birthday:</td>
					<td>
						<select style="width: 62px; height: 21px; border: 1px solid #7F9DB9;" name="month">
							<option>Month</option>
							<option value="1"<? if( $_POST['month'] == 1 ) echo " SELECTED"; ?>>Jan</option>
							<option value="2"<? if( $_POST['month'] == 2 ) echo " SELECTED"; ?>>Feb</option>
							<option value="3"<? if( $_POST['month'] == 3 ) echo " SELECTED"; ?>>Mar</option>
							<option value="4"<? if( $_POST['month'] == 4 ) echo " SELECTED"; ?>>Apr</option>
							<option value="5"<? if( $_POST['month'] == 5 ) echo " SELECTED"; ?>>May</option>
							<option value="6"<? if( $_POST['month'] == 6 ) echo " SELECTED"; ?>>Jun</option>
							<option value="7"<? if( $_POST['month'] == 7 ) echo " SELECTED"; ?>>Jul</option>
							<option value="8"<? if( $_POST['month'] == 8 ) echo " SELECTED"; ?>>Aug</option>
							<option value="9"<? if( $_POST['month'] == 9 ) echo " SELECTED"; ?>>Sep</option>
							<option value="10"<? if( $_POST['month'] == 10 ) echo " SELECTED"; ?>>Oct</option>
							<option value="11"<? if( $_POST['month'] == 11 ) echo " SELECTED"; ?>>Nov</option>
							<option value="12"<? if( $_POST['month'] == 12 ) echo " SELECTED"; ?>>Dec</option>
						</select>
						<select style="width: 50px; height: 21px; border: 1px solid #7F9DB9;" name="day">
							<option>Day</option>
							<?php
							for ($i = 1; $i <= 31; $i++)
		if( $i == $_POST['day'] )
								echo "<option value=$i SELECTED>$i</option>";
		else
								echo "<option value=$i>$i</option>";
							?>
						</select>
						<select style="width: 54px; height: 21px; border: 1px solid #7F9DB9;" name="year">
							<option>Year</option>
							<?php
							for ($i = date("Y"); $i > date("Y") - 100; $i--)
		if( $i == $_POST['year'] )
								echo "<option value=$i SELECTED>$i</option>";
		else
								echo "<option value=$i>$i</option>";
							?>
						</select>
					</td>
				</tr>
				<tr>
					<td colspan=3 style="padding-top: 15px; padding-left: 20px; color: #808080; font-family: arial; font-size: 8pt;">
						By clicking sign up, you agree to the <a href="/privacy.php" style="color: #326798; text-decoration: none;">Privacy Policy</a><br>
							and <a href="/tos.php" style="color: #326798; text-decoration: none;">Terms of Use</a>.
						<div style="margin-top: 10px; text-align: center;">
							<input type="submit" style="height: 23px; width: 62px; background: #FFF8CC; border: 1px solid #FF9A66; color: #3B5998; font-weight: bold; font-size: 9pt; font-family: arial;" value="Sign Up">
						</div>

            <div style="text-align:center; color: #808080; padding-top:3px;">
              or login with
            </div>
            <div style="text-align:center; color: #808080; padding-top:5px;">
              <a href="/fbconnect/login.php"><img src="/images/facebook16.png" /></a> <a href="/twitter/login.php"><img src="/images/twitter16.png" /></a>
            </div>
					</td>
				</tr>
				</form>
			</table>
	</div>


<? } ?>

      <?
      if( $API->adv )
      {
      ?>
  		<div class="subhead" style="margin-top: 10px; margin-bottom:10px;"><div style="float:left;">Sponsors</div><div style="float:right;"><a href="http://www.salthub.com/adv/create.php" style="font-size:8pt; font-weight:300; color:rgb(0, 64, 128);">create an ad</a>&nbsp;</div><div style="clear:both;"></div></div>
  		<?php showAd("companion"); } ?>


<?
if( $r['gid'] > 0 && $API->isLoggedIn())
{
  $fof = $friends;
  $friends = $API->getFriendsUids( $API->uid );

  $links = array();
  $links[0] = array();
  $links[1] = array();
  $links[2] = array();

  if( sizeof( $fof ) > 0 )
  {

    $conn_q = sql_query( "SELECT page_members.uid FROM page_members WHERE page_members.gid='" . $r['gid'] . "' and page_members.uid IN (" . implode( ",", $fof ) . ")" );
    while( $conn_r = mysql_fetch_array( $conn_q ) )
    {
      if( in_array( $conn_r['uid'], $friends ) )
        $links[0][] = $conn_r['uid'];
      else if( in_array( $conn_r['uid'], $fof ) )
        $links[1][] = $conn_r['uid'];
      else
        $links[2][] = $conn_r['uid'];
    }


  if( mysql_num_rows( $conn_q ) > 0 )
  {

?>
  		<div class="subhead" style="margin-top: 10px; margin-bottom:5px;">Company Connections</div>

      <div style="font-size:9pt;">
      <span style="font-size:8pt;">Did you know that 80% of all new jobs are landed through connections?  Below are the connections you need to know about.</span>

<?
$titles = array( 'Direct connections', 'Indirect connections', 'Outside your network' );
$i = 0;
for( $c = 0; $c < 3; $c++ )
{
  if( sizeof( $links[$c] ) > 0 )
  {
    if( $i > 0 )
      echo '<div style="clear:both; border-bottom: 2px dotted #c0d9ec; height:1px; width:280px margin-left:10px; margin-top:5px; margin-bottom:5px;"></div>';
    else
      echo '<div style="margin-top:10px;"></div>';

    $i++;

    echo '<div style="font-weight:bold; margin-top:5px; margin-bottom:5px;">' . $titles[$c] . '</div>';


    for( $d = 0; $d < sizeof( $links[$c] ) && $d < 2; $d++ )
    {
      $uid = $links[$c][$d];
      $purl = $API->getProfileURL( $uid );
      $img = "/img/48x48" . $API->getUserPic( $uid );
      $prof = quickQuery( "select occupation from users where uid='$uid'" );
      $name = quickQuery( "select name from users where uid='$uid'" );


      $data = queryArray( "select pic, twusername, fbid, name from users where uid='" . $uid . "'" );
     	$parts = array(rawurlencode($purl), rawurlencode($API->getUserPic($uid, $data[0])), rawurlencode($data['twusername']), rawurlencode($data['fbid']), rawurlencode($data['name']));
    	$id = implode("-", $parts);

      if( $c == 0 )
      {
?>
      <div style="height:55px; float:left; width:290px; overflow:hidden;">
        <div style="float:left; height:48px; width:48px; margin-right:5px;">
          <? echo '<a id="' . md5(microtime()) . "-" . $id . '" href="' . $purl . '"  title="' . $fids . '" onmouseover="javascript:showTip(3, this);" onmouseout="javascript:tipMouseOut();">'; ?>
          <img src="<?=$img?>" width="48" height="48"></a>
        </div>
        <div style="float:left; width:200px;">
        <?
        echo "<a href=\"" . $purl . "\"><b>" . $name . "</b></a><br />";
        echo '<span style="font-size:8pt;">' . $prof . '</span><br />';
        ?>
        <a style="font-size:8pt;" href="javascript:void(0);" onclick="javascript:showSendMessage(<?=$uid?>, '<?=addslashes($name)?>', '<?=$img?>');">Send message</a>
        </div>
      </div>
      <?
      }
      else
      {
?>
      <div style="width:290px; overflow:hidden;">
        <div style="float:left; height:48px; width:48px; margin-right:5px;">
          <? echo '<a id="' . md5(microtime()) . "-" . $id . '" href="' . $purl . '"  title="' . $fids . '" onmouseover="javascript:showTip(3, this);" onmouseout="javascript:tipMouseOut();">'; ?>
          <img src="<?=$img?>" width="48" height="48"></a>
        </div>
        <div style="float:left; width:235px; overflow:hidden;">
        <?
        echo "<a href=\"" . $purl . "\"><b>" . $name . "</b></a> - ";
        ?>
        <a style="font-size:8pt" href="javascript:void(0);" onclick="javascript:showSendMessage(<?=$uid?>, '<?=addslashes($name)?>', '<?=$img?>');">request a referral</a>
        <br />
        <span style="font-size:8pt;">
        <?
        $connections = $API->getMutualFriendsWithPage( $uid, $r['gid'] );
        $nc = sizeof( $connections );
        if( $nc > 0 )
        {
          echo 'Is connected to ' . $nc . ($nc > 1 ? ' people' : ' person') . ' associated with this company<br />';
          for( $e = 0; $e < $nc && $e < 6; $e++ )
          {
            $purl = $API->getProfileURL( $connections[$e]['uid'] );
            $img = "/img/24x24" . $API->getUserPic( $connections[$e]['uid'] );
            echo '<a href="' . $purl . '"><img style="float:left; border:1px solid #000; margin:3px;" src="' . $img . '" width="24" height="24"></a>';
          }
        }
        else
           echo $prof;
        ?>
        </span>
        </div>
      </div>
<?
      }

    }

    echo '<div style="clear:both; width:100%; text-align:right;"><a href="javascript:void(0);" onclick="javascript:showLinksToPage(' . $r['gid'] . ');">view all</a></div>';
  }
}
    echo "</div>";
  }

  }
}
?>

    </div>

    <div style="clear:both;"></div>
  </div>

</div>

<script type="text/javascript">
<!--
function showSendMessage2(uid, name, pic, subject, message )
{
	html  = '<div style="font-size: 8pt; color: #555;">';
	html += '	<div style="float: left; width: 85px;">';
	html += '		<img src="' + pic + '" alt="" style="padding-bottom: 2px;" /><br />';
	html += '		' + name;
	html += '	</div>';
	html += '	<div style="float: left; padding-left: 10px; font-weight: bold;">';
	html += '		<div style="height: 12pt;">Subject:</div><input type="text" id="msgsubj" maxlength="50" style="width: 285px;" value="' + subject + '"/>';
	html += '		<div style="padding-top: 5px;">';
	html += '			<div style="height: 12pt;">Message:</div><textarea id="msgbody" style="width: 285px; height: 95px;">' + message + '</textarea><br />';
	html += '			Your profile will be included with your application.<br /><a href="<? echo $API->getProfileURL(); ?>">Update &gt;&gt;</a>';
	html += '		</div>';

	html += '		<div style="height: 12pt; margin-top:10px;">Email:</div><input type="text" id="msgemail" maxlength="50" style="width: 175px;" value="<? if( $API->uid > 0 ) echo quickQuery( "select public_email from users where uid=" . $API->uid ); ?>"/>';
	html += '		<div style="height: 12pt; margin-top:10px;">Telephone:</div>+ <input type="text" id="msgtelephone" maxlength="50" style="width: 165px;" value=""/>';

	html += '		<div style="padding: 5px 0px; text-align: center; height: 16px;" id="msgbuttons">';
	html += '			<input type="button" class="button" value="Send" onclick="javascript:sendMessage2(\'' + uid + '\');" /> &nbsp; &nbsp;';
	html += '			<input type="button" class="button" value="Cancel" onclick="javascript:closePopUp();" />';
	html += '		</div>';
	html += '	</div>';
	html += '	<div style="clear: both;"></div>';
	html += '</div>';

	showPopUp("", html, 0);
}

function sendMessage2(uid)
{
	email = escape(document.getElementById("msgemail").value);
	telephone = escape(document.getElementById("msgtelephone").value);

	subj = escape(document.getElementById("msgsubj").value);
	body = escape(document.getElementById("msgbody").value);

  body += '\n\nAdditional Contact Information\nEmail: ' + email + '\nTelephone: ' + telephone;

	if (subj == "")
	{
		alert("Please enter a subject.");
		return;
	}

	if (body == "")
	{
		alert("Please type a message.");
		return;
	}

	document.getElementById("msgbuttons").innerHTML = "<img src='/images/tbpreload.gif' alt='' />";

	postAjax("/messaging/sendmessage.php", "uid=" + uid + "&subj=" + subj + "&body=" + body, function(data)
	{
		if (data == "OK")
			showPopUp2("", "Your message has been sent.");
		else
			showPopUp2("", "There was an error sending your message.&nbsp; Please try again later.");
	});
}

-->
</script>

<?
include "../footer.php";
?>

