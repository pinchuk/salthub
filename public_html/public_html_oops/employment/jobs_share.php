<?
/*
Displays a popup for users to retrieve the URLS of the job listing so that they can send it to others.

11/27/2011 - Created
*/

include( "../inc/inc.php" );

$ID = $_GET['d'];

$jmp = quickQuery( "select jmp from jobs where id='$ID'" );
$url = "http://" . SERVER_HOST . "/employment/jobs_detail.php?d=" . $ID;

if( $jmp == "" )
{
  $jmp = shortenURL( $url );
  sql_query( "update jobs set jmp='$jmp' where id='" . $ID . "'" );
}

?>

<div style="width:380px;">
  <div style="color: #326798; font-size:9pt; font-weight:bold;">
    share the URL
  </div>

  <div style="width:380; marin-left:3px; border-bottom: 1px solid #d8dfea;"></div>

  <div style="float:left; width:100px; text-align:right;">
    <div style="float:right; font-size:9pt; font-weight:bold; padding-top:4px; margin-right:5px;">Full URL:</div>
    <div style="float:right; margin-right:3px; padding-top:4px;"><img src="/images/link_add.png" width="16" height="16" alt="" /></div>
  </div>

  <div style="float:left;">
    <textarea style="width:250px; height:30px;" readonly="readonly"><? echo $url ?></textarea>
  </div>

  <div style="clear:both; padding-top:5px;"></div>

  <div style="float:left; width:100px; text-align:right;">
    <div style="float:right; font-size:9pt; font-weight:bold; padding-top:4px; margin-right:5px;">Short URL:</div>
    <div style="float:right; margin-right:3px; padding-top:4px;"><img src="/images/link_add.png" width="16" height="16" alt="" /></div>
  </div>

  <div style="float:left;">
    <textarea style="width:250px; height:30px;" readonly="readonly">http://j.mp/<? echo $jmp ?></textarea>
  </div>

  <div style="clear:both;"></div>
</div>
