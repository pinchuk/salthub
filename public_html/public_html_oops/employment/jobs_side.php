<?
/*
Contains the primary method of searching job listings. Queries are sent to the web server whenever the user changes a parameter and the results are automatically displayed.
*/

include_once( "../inc/inc.php" );

$div = array( "sector", "position", "location", "nationality", "licandend", "degrees" );
$type = 0;


?>

<script language="javascript" type="text/javascript">
<!--
var conditions = new Array();
function showDetail( id )
{
  postAjax("/employment/jobs_detail.php", "ID=" + id, "responseHandler");
}

function refreshListings()
{
  post = "a=0";
  for( i = 0; i < conditions.length; i++ )
  {
    if( !conditions[i] ) continue;

    found = false;
    post += "&c" + i + "=";
    for( var j in conditions[i] )
    {
      if( conditions[i][j][0] > 0 )
      {
        found = true;
        post += conditions[i][j][0] + ",";
      }
    }
    if( !found )
      post+= "0";
    else
      post = post.substring( 0, post.length - 1 );
  }

  postAjax("/employment/jobs_search.php", post, "responseHandler");
}

function condChange( type, id, name, value, div )
{
  if( !conditions[type] ) conditions[type] = new Array();

  conditions[type][id] = new Array();
  conditions[type][id][0] = value;
  conditions[type][id][1] = name;

  e = document.getElementById( div + "-compact" );

  if( e )
    e.innerHTML = '';

  post = "a=0";
  for( i = 0; i < conditions.length; i++ )
  {
    if( !conditions[i] ) continue;

    found = false;
    post += "&c" + i + "=";
    for( var j in conditions[i] )
    {
      if( conditions[i][j][0] > 0 )
      {
        found = true;
        post += conditions[i][j][0] + ",";
        if( e && i == type )
          e.innerHTML += '<a href="javascript:void(0);" title="\''+div+'-'+conditions[i][j][0]+'\'" onclick="javascript:e=document.getElementById(\''+div+'-'+conditions[i][j][0]+'\'); if( e ) e.checked=false; condChange(\''+i+'\', \''+j+'\', \''+conditions[i][j][1]+'\',0,\''+div+'\');">X</a> ' + conditions[i][j][1] + '<br />';
      }
    }
    if( !found )
      post+= "0";
    else
      post = post.substring( 0, post.length - 1 );
  }

  postAjax("/employment/jobs_search.php", post, "responseHandler");
}

function responseHandler( data )
{
  e = document.getElementById( "searchResults" );
  if( e )
  {
    e.innerHTML = data;
  }
}

function swapDivs( div )
{
  e = document.getElementById( div );
  f = document.getElementById( div + "-compact" );

  if(e&&f)
  {
    if( e.style.display == 'none' )
    {
      toggleSlide( div );
      f.style.display = 'none';
    }
    else
    {
      toggleSlide( div + "-compact" );
      e.style.display = 'none';
    }
  }

<? for( $c = 0; $c < sizeof( $div ); $c++ ) { ?>
  if( div != '<? echo $div[$c] ?>' )
  {
    e = document.getElementById( '<? echo $div[$c] ?>');
    f = document.getElementById( "<? echo $div[$c] ?>-compact" );

    if(e&&f)
    {
      e.style.display='none';
      f.style.display='block';
    }
  }
<? } ?>
}
-->
</script>

<table border="0" cellpadding="0" cellspacing="0" class="mediaactions" style="margin-top:5px">
	<tr>
		<td style="padding-right: 20px; padding-top:5px; padding-left:10px;">
      <a type="button_count" href="javascript:void(0);" onclick="javascript: openPopupWindow( 'http://www.facebook.com/sharer/sharer.php?u=<? echo urlencode("http://" . SERVER_HOST . "/employment" ); ?>', 'Share', 550, 320 );">
        <img src="/images/facebook_share.png" width="58" height="20" alt="" />
      </a>
    </td>
		<td>
      <a href="http://twitter.com/share" class="twitter-share-button" data-url="http://j.mp/sBhOjZ" data-text="SaltHub's job pages are for professionals in the maritime & water related sectors. Find & list positions." data-count="horizontal" data-via="<?=SITE_VIA?>">Tweet</a>
    </td>
	</tr>
</table>

<form action="jobs.php" method="GET">

<div class="subhead" style="cursor:pointer; margin-top: 10px;">Job Search</div>

<div style="color:#555; font-size:9pt; margin-top:5px;">Keywords:</div>
<div><input name="q" type="text" id="keywords" style="width:155px;" value="<? echo $_GET['q']; ?>"/></div>

<div style="color:#555; font-size:9pt; margin-top:5px;">Job Title:</div>
<div><input name="title" type="text" id="title" style="width:155px;" value="<? echo $_GET['title']; ?>"/></div>

<div style="color:#555; font-size:9pt; margin-top:5px;">Company:</div>
<div><input name="company" type="text" id="company" style="width:155px;" value="<? echo $_GET['company']; ?>"/></div>

<div style="color:#555; font-size:9pt; margin-top:5px;">Country:</div>
<div>
  <select name="country" style="width:160px;" id="user-country" onchange="javascript: updateLocationSelect('state', this.value);">
  <option value="0"></option>
  <?
  $country = $_GET['country'];

  $q2 = sql_query( "select country,id from loc_country order by priority desc, country" );
  while( $r2 = mysql_fetch_array( $q2 ) )
  {
  ?>
        <option value="<? echo $r2['id']; ?>"<? if( $r2['id'] == $country ) echo " SELECTED"; ?>><? echo $r2['country']; ?></option>
  <?
    if( $country == 0 ) $country = $r2['id'];
  }
  ?>
  </select>
</div>


<div style="color:#555; font-size:9pt; margin-top:5px;">State:</div>
<div style="width:155px; padding-left:0px; margin-left:0px; margin-top:5px;" id="userinfo-state">
  <select name="state" style="width:160px;" id="user-state" onchange="javascript: updateLocationSelect('city', this.value);">
  <option value="0"></option>
<?
$state = $_GET['state'];
$q2 = sql_query( "select region,id from loc_state where country_id='$country' order by region" );
while( $r2 = mysql_fetch_array( $q2 ) )
{
  if( $r2['region'] != "-" )
  {
?>
  <option value="<? echo $r2['id']; ?>"<? if( $r2['id'] == $state ) echo " SELECTED"; ?>><? echo ucwords(strtolower($r2['region']) ); ?></option>
<?
  }
  if( $state == 0 ) $state = $r2['id'];
}
?>
  </select>
</div>

<div style="color:#555; font-size:9pt; margin-top:5px;">City:</div>
<div style="width:155px; padding-left:0px; margin-left:0px; margin-top:5px;" id="userinfo-city">
  <select name="city" style="width:160px;" id="user-city">
  <option value="0"></option>
<?
$q2 = sql_query( "select city,id from loc_city where region_id='$state' order by city" );
while( $r2 = mysql_fetch_array( $q2 ) )
{
  if( $r2['city'] != "-" ) {
?>
  <option value="<? echo $r2['id']; ?>"<? if( $r2['id'] == $_GET['city'] ) echo " SELECTED"; ?>><? echo ucwords(strtolower($r2['city'])); ?></option>
<?
 }
}
?>
  </select>
</div>

<div style="color:#555; font-size:9pt; margin-top:10px;">
  <input type="submit" class="button" value="Search" />
</div>

</form>

<? include( "jobs_saved_searches.php" ); ?>

<script type="text/javascript">
<!--
function updateLocationSelect(f, v)
{
  if( document.getElementById("userinfo-" + f) )
  	document.getElementById("userinfo-" + f).innerHTML = '<img src="/images/wait_sm.gif" alt="" />';
	getAjax("/employment/locationselect2.php?f=" + f + "&v=" + v, "updateLocationSelectHandler");
}

function updateLocationSelectHandler(data)
{
	x = data.split("|");
	document.getElementById("userinfo-" + x[0]).innerHTML = x[1];
}
-->
</script>
