<?
/*
Side bar for searching through users seeking employment.
*/

include_once( "../inc/inc.php" );

$div = array( "sector", "position", "location", "nationality", "licandend", "degrees" );
$type = 0;


?>

<script language="javascript" type="text/javascript">
<!--
var conditions = new Array();

function condChange( type, id, name, value, div, page )
{
  if( !conditions[type] ) conditions[type] = new Array();

  if( typeof page  == "undefined" )
  {
    page = 0;
  }
  
  conditions[type][id] = new Array();
  conditions[type][id][0] = value;
  conditions[type][id][1] = name;

  e = document.getElementById( div + "-compact" );

  if( e )
    e.innerHTML = '';

  post = "a=0";
  for( i = 0; i < conditions.length; i++ )
  {
    if( !conditions[i] ) continue;

    found = false;
    post += "&c" + i + "=";
    for( var j in conditions[i] )
    {
      if( conditions[i][j][0] > 0 )
      {
        found = true;
        post += conditions[i][j][0] + ",";
        if( e && i == type )
          e.innerHTML += '<a href="javascript:void(0);" title="\''+div+'-'+conditions[i][j][0]+'\'" onclick="javascript:e=document.getElementById(\''+div+'-'+conditions[i][j][0]+'\'); if( e ) e.checked=false; condChange(\''+i+'\', \''+j+'\', \''+conditions[i][j][1]+'\',0,\''+div+'\');">X</a> ' + conditions[i][j][1] + '<br />';
      }
    }
    if( !found )
      post+= "0";
    else
      post = post.substring( 0, post.length - 1 );
  }
  postAjax("/employment/search.php", post, "responseHandler");
}


function responseHandler( data )
{
  e = document.getElementById( "searchResults" );
  if( e )
  {
    e.innerHTML = data;
  }
}

function swapDivs( div )
{
  e = document.getElementById( div );
  f = document.getElementById( div + "-compact" );

  if(e&&f)
  {
    if( e.style.display == 'none' )
    {
      toggleSlide( div );
      f.style.display = 'none';
    }
    else
    {
      toggleSlide( div + "-compact" );
      e.style.display = 'none';
    }
  }

<? for( $c = 0; $c < sizeof( $div ); $c++ ) { ?>
  if( div != '<? echo $div[$c] ?>' )
  {
    e = document.getElementById( '<? echo $div[$c] ?>');
    f = document.getElementById( "<? echo $div[$c] ?>-compact" );

    if(e&&f)
    {
      e.style.display='none';
      f.style.display='block';
    }
  }
<? } ?>
}

function changePage( p,q  )
{
  getAjax("/employment/search.php?p=" + p + q, "responseHandler");
}

-->
</script>

<table border="0" cellpadding="0" cellspacing="0" class="mediaactions" style="margin-top:5px">
	<tr>
		<td style="padding-right: 20px; padding-top:5px; padding-left:10px;">
      <a type="button_count" href="javascript:void(0);" onclick="javascript: openPopupWindow( 'http://www.facebook.com/sharer/sharer.php?u=<? echo urlencode("http://" . SERVER_HOST . "/employment/employees.php" ); ?>', 'Share', 550, 320 );">
        <img src="/images/facebook_share.png" width="58" height="20" alt="" />
      </a>
    </td>

		<td>
      <a href="http://twitter.com/share" class="twitter-share-button" data-url="http://j.mp/sBhOjZ" data-text="SaltHub's job pages are for professionals in the maritime & water related sectors. Find & list positions." data-count="horizontal" data-via="<?=SITE_VIA?>">Tweet</a>
    </td>
	</tr>
</table>


<div class="subhead" style="cursor:pointer; margin-top: 10px;" onclick="swapDivs('<?=$div[$type]?>');">
	<div style="float: left;">Sector</div>
  <div style="float:right;">+</div>
	<div style="clear: both;"></div>
</div>

<div class="jobs-side" id="<?=$div[$type]?>" style="display:block; height:170px;">
<?
$q = sql_query( "select * from categories where industry=" . PAGE_TYPE_BUSINESS . " and cattype='G'" );
while( $r = mysql_fetch_array( $q ) )
{
  $val = $r['cat'];
  $name = $r['catname'];
?>
  <div><input type="checkbox" id="<?=$div[$type]?>-<?=$val?>" value="<? echo $val; ?>" onchange="javascript: condChange('<?=$type;?>', <?=$val?>, '<?=$name?>', this.checked ? this.value : 0,'<?=$div[$type]?>' );"/><a href="javascript:void(0);" onclick="javascript:e=document.getElementById('<?=$div[$type]?>-<?=$val?>'); if(e) { e.checked = !e.checked; condChange('<?=$type;?>', <?=$val?>, '<?=$name?>', e.checked ? e.value : 0,'<?=$div[$type]?>' ); }"><?=$name?></a></div>
<?
}
?>
</div>

<div class="jobs-side" id="<?=$div[$type]?>-compact" style="display:none;"></div>

<!-- Position -->
<?$type++; ?>
<div class="subhead" style="cursor:pointer; margin-top: 10px;" onclick="swapDivs('<?=$div[$type]?>');">
	<div style="float: left;">Position</div>
  <div style="float:right;">+</div>
	<div style="clear: both;"></div>
</div>


<div class="jobs-side" id="<?=$div[$type]?>" style="display:none; height:180px; overflow-y:scroll;">
<?
$q = sql_query( "select * from pages where cat='" . OCCUPATION_GRP. "' and popular=1 order by gname" );
while( $r = mysql_fetch_array( $q ) )
{
  $val = $r['gid'];
  $name = $r['gname'];
?>
  <div><input type="checkbox" id="<?=$div[$type]?>-<?=$val?>" value="<? echo $val; ?>" onchange="javascript: condChange('<?=$type;?>', <?=$val?>, '<?=$name?>', this.checked ? this.value : 0,'<?=$div[$type]?>' );"/><a href="javascript:void(0);" onclick="javascript:e=document.getElementById('<?=$div[$type]?>-<?=$val?>'); if(e) { e.checked = !e.checked; condChange('<?=$type;?>', <?=$val?>, '<?=$name?>', e.checked ? e.value : 0,'<?=$div[$type]?>' ); }"><?=$name?></a></div>
<?
}
?>
</div>

<div class="jobs-side" id="<?=$div[$type]?>-compact" style="display:none;"></div>

<!-- Location -->
<?$type++; ?>
<div class="subhead" style="cursor:pointer; margin-top: 10px;" onclick="swapDivs('<?=$div[$type]?>');">
	<div style="float: left;">Location</div>
  <div style="float:right;">+</div>
	<div style="clear: both;"></div>
</div>

<div class="jobs-side" id="<?=$div[$type]?>" style="display:none; height:180px; overflow-y:scroll;">
<?
$q = sql_query( "select * from loc_country order by priority desc,country" );
while( $r = mysql_fetch_array( $q ) )
{
  $val = $r['id'];
  $name = $r['country'];
?>
  <div><input type="checkbox" id="<?=$div[$type]?>-<?=$val?>" value="<? echo $val; ?>" onchange="javascript: condChange('<?=$type;?>', <?=$val?>, '<?=$name?>', this.checked ? this.value : 0,'<?=$div[$type]?>' );"/><a href="javascript:void(0);" onclick="javascript:e=document.getElementById('<?=$div[$type]?>-<?=$val?>'); if(e) { e.checked = !e.checked; condChange('<?=$type;?>', <?=$val?>, '<?=$name?>', e.checked ? e.value : 0,'<?=$div[$type]?>' ); }"><?=$name?></a></div>
<?
}
?>
</div>

<div class="jobs-side" id="<?=$div[$type]?>-compact" style="display:none;"></div>

<!-- Nationality -->
<?$type++; ?>
<div class="subhead" style="cursor:pointer; margin-top: 10px;" onclick="swapDivs('<?=$div[$type]?>');">
	<div style="float: left;">Citizenship</div>
  <div style="float:right;">+</div>
	<div style="clear: both;"></div>
</div>

<div class="jobs-side" id="<?=$div[$type]?>" style="display:none; height:180px; overflow-y:scroll;">
<?
$q = sql_query( "select * from loc_country order by priority desc,country" );
while( $r = mysql_fetch_array( $q ) )
{
  $val = $r['id'];
  $name = $r['country'];
?>
  <div><input type="checkbox" id="<?=$div[$type]?>-<?=$val?>" value="<? echo $val; ?>" onchange="javascript: condChange('<?=$type;?>', <?=$val?>, '<?=$name?>', this.checked ? this.value : 0,'<?=$div[$type]?>' );"/><a href="javascript:void(0);" onclick="javascript:e=document.getElementById('<?=$div[$type]?>-<?=$val?>'); if(e) { e.checked = !e.checked; condChange('<?=$type;?>', <?=$val?>, '<?=$name?>', e.checked ? e.value : 0,'<?=$div[$type]?>' ); }"><?=$name?></a></div>
<?
}
?>
</div>

<div class="jobs-side" id="<?=$div[$type]?>-compact" style="display:none;"></div>

<!-- Lic and Endorsements -->
<?$type++; ?>
<div class="subhead" style="cursor:pointer; margin-top: 10px;" onclick="swapDivs('<?=$div[$type]?>');">
	<div style="float: left;">Lic &amp; Endorsements</div>
  <div style="float:right;">+</div>
	<div style="clear: both;"></div>
</div>

<div class="jobs-side" id="<?=$div[$type]?>" style="display:none; height:180px; overflow-y:scroll;">
<?
$q = sql_query( "select * from pages where cat='" . LIC_AND_END_GRP. "' order by gname" );
while( $r = mysql_fetch_array( $q ) )
{
  $val = $r['gid'];
  $name = $r['gname'];
?>
  <div><input type="checkbox" id="<?=$div[$type]?>-<?=$val?>" value="<? echo $val; ?>" onchange="javascript: condChange('<?=$type;?>', <?=$val?>, '<?=$name?>', this.checked ? this.value : 0,'<?=$div[$type]?>' );"/><a href="javascript:void(0);" onclick="javascript:e=document.getElementById('<?=$div[$type]?>-<?=$val?>'); if(e) { e.checked = !e.checked; condChange('<?=$type;?>', <?=$val?>, '<?=$name?>', e.checked ? e.value : 0,'<?=$div[$type]?>' ); }"><?=$name?></a></div>
<?
}
?>
</div>

<div class="jobs-side" id="<?=$div[$type]?>-compact" style="display:none;"></div>

<!-- Degrees -->
<?$type++; ?>
<div class="subhead" style="cursor:pointer; margin-top: 10px;" onclick="swapDivs('<?=$div[$type]?>');">
	<div style="float: left;">Degrees</div>
  <div style="float:right;">+</div>
	<div style="clear: both;"></div>
</div>

<div class="jobs-side" id="<?=$div[$type]?>" style="display:none; height:180px; overflow-y:scroll;">
<?
$q = sql_query( "select * from pages where cat='" . DEGREES_GRP. "' order by gname" );
while( $r = mysql_fetch_array( $q ) )
{
  $val = $r['gid'];
  $name = $r['gname'];
?>
  <div><input type="checkbox" id="<?=$div[$type]?>-<?=$val?>" value="<? echo $val; ?>" onchange="javascript: condChange('<?=$type;?>', <?=$val?>, '<?=$name?>', this.checked ? this.value : 0,'<?=$div[$type]?>' );"/><a href="javascript:void(0);" onclick="javascript:e=document.getElementById('<?=$div[$type]?>-<?=$val?>'); if(e) { e.checked = !e.checked; condChange('<?=$type;?>', <?=$val?>, '<?=$name?>', e.checked ? e.value : 0,'<?=$div[$type]?>' ); }"><?=$name?></a></div>
<?
}
?>
</div>

<div class="jobs-side" id="<?=$div[$type]?>-compact" style="display:none;"></div>
