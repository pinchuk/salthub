<?
/*
Container for my_jobs.php - which shows the listing of jobs that a user has created.
*/

include( "../inc/inc.php" );
include "../header.php";

include( "emp_top_menu_admin.php" );
?>
<div class="contentborder">
  <div style="clear:both; margin-top:10px;"></div>
  <? include( "my_jobs.php" ); ?>
  <div style="clear:both;"></div>
</div>

<?
include "../footer.php";
?>