<?
if( isset( $_GET['skip'] ) )
{
  setcookie( "skipfbconnect", 1 );
  header( "Location: /welcome" );
  exit;
}

require( "inc/inc.php" );
require( "header.php" );

?>

  <div style="width:500px; margin-left:250px; margin-top:100px;  margin-bottom:100px;">
    Your facebook session is no longer valid. This can occur if your session has expired or if you've changed your facebook password.
    <br /><br />
    Please login to facebook to renew your facebook session.  You will be forwarded to the facebook login page in 10 seconds.
    <br /><br />
    <a href="/fbconnect/login.php">Renew Facebook Session</a>
    &nbsp;&nbsp;
    <a href="/facebook_error.php?skip">Don't Renew Session</a>
  </div>

<script type="text/javascript">
<!--
setTimeout( 'document.location = "/fbconnect/login.php";', 10000 );
-->
</script>

<?

require( "footer.php" );
?>