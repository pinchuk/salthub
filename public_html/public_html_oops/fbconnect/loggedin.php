<?php
/*
Facebook login script.  This is called after the Facebook server authenticates a user.
This script is also responsible for creating user accounts for those signing in through
facebook for the first time.
*/

include "../inc/inc.php";

if( isset( $_REQUEST['error_reason'] ) )
{
  header( "Location: /" );
  exit;
}
else
{
  unset( $params );


  if($_SESSION['state'] && ($_SESSION['state'] === $_REQUEST['state']))
  {
    $code = $_REQUEST["code"];
    $my_url = "http://www.salthub.com/fbconnect/loggedin.php";

    $token_url = "https://graph.facebook.com/oauth/access_token?"
         . "client_id=" . $fbAppId . "&redirect_uri=" . urlencode($my_url)
         . "&client_secret=" . $fbSecret . "&code=" . $code;

    $response = file_get_contents($token_url);
    $params = null;
    parse_str($response, $params);
  }
  else {
    //Reset session, start the sign up process again.
    foreach (array_keys($_SESSION) as $k)
  	  unset($_SESSION[$k]);

    header( "Location: /fbconnect/login.php" );
    exit;
  }
}

if( empty( $params['access_token'] ) )
{
  echo "Invalid access token";
  exit;
}
else
{
  $facebook = $API->startFBSession( null, $params['access_token'] );

  $fbinfo = $facebook->api('/me');

  $_SESSION['fbid'] = $fbinfo['id'];
  $API->fbid = $fbinfo['id'];

  $gender = ucfirst( substr( $fbinfo['gender'], 0, 1 ) );
  $bd = $API->fbSafeCallMethod($API->uid, $facebook, "users.getInfo", array("uids" => array($facebook->getUser()), "fields" => array("birthday")));
  $birthday = $bd[0]['birthday'];
}

if (empty($API->fbid))
{
	header("Location: /");
  exit;
}


/*
Get a long term access token from the short term one.
*/
/*
$url = "https://graph.facebook.com/oauth/access_token?client_id=" . $fbAppId . "&client_secret=" . $fbSecret . "&grant_type=fb_exchange_token&fb_exchange_token=" . $session['access_token'];

// Initialize session and set URL.
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);

// Set so curl_exec returns the result instead of outputting it.
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

// Get the response and close the channel.
$response = curl_exec($ch);
curl_close($ch);

parse_str($response, $params);


$session['access_token'] = $params['access_token'];
$session['expires'] = $params['expires'];
$session_without_sig = $session;
unset( $session['sig'] );
$session['sig'] = Facebook::generateSignature( $session, $fbSecret );

$_SESSION['fbsess'] = $session;
*/

$x = sql_query("select active,uid,name,admin" . ($site == "s" ? ",verify" : "") . " from users where fbid=" . $_SESSION['fbid'] );

if (empty($API->uid) && mysql_num_rows($x) == 0)
{

	// new user
  if( $site == "s" )
  	sql_query("insert into users (email,joined,ip,lastlogin,fbid,fbaccesstoken,fbexpires,name,fbusername,active,verify,gender,dob) values ('{$fbinfo['email']}',now()," . ip2long($_SERVER['REMOTE_ADDR']) . ",now()," . $fbinfo['id'] . ",'{$_SESSION['fbaccess_token']}','{$_SESSION['fbexpires']}','" . addslashes($fbinfo['name']) . "','" . addslashes($fbinfo['name']) . "',0,1,'$gender','$birthday')"); // create user with fb info
  else
  	sql_query("insert into users (email,joined,ip,lastlogin,fbid,name,fbusername,active) values ('{$fbinfo['email']}',now()," . ip2long($_SERVER['REMOTE_ADDR']) . ",now()," . $fbinfo['id'] . ",'" . addslashes($fbinfo['name']) . "','" . addslashes($fbinfo['name']) . "', 0)"); // create user with fb info

	$_SESSION['uid'] = mysql_insert_id();
	$_SESSION['name'] = $fbinfo['name'];
	$_SESSION['admin'] = 0;
	$API->uid = $_SESSION['uid'];
  $API->name = $_SESSION['name'];

  $_GET['cat'] = 2; //Set the photo category
  unset($_GET['userpic']);
	$_SESSION['pic'] = $API->saveUserPicture(SITE_FACEBOOK, "http://graph.facebook.com/{$API->fbid}/picture?type=large");

	sql_query("update users set pic={$_SESSION['pic']} where uid={$API->uid}");
  //Hide photos until they activate their account.
  sql_query( "update photos set reported=2 where uid='" . $API->uid . "' and reported=0" );

  if( $site != "m" )
  {
//     This is done when you get to the confirmation step now.
//  	$API->sendConfirmationEmail($fbinfo['email']);
  }

  $API->convertCustomContactsToUserContacts();

	$API->feedAdd("J", SITE_FACEBOOK);
	spamWall();
	$API->addReferral();
	exec("export LD_LIBRARY_PATH=/usr/local/lib; /usr/local/bin/php " . $_SERVER['DOCUMENT_ROOT'] . "/../updatesocial.php " . $_SESSION['uid']);

  if( isset( $_SESSION['joingid'] ) )
  {
		$gid = intval($_SESSION['joingid']);
		sql_query("insert into page_members (gid,uid) values ({$gid}," . $_SESSION['uid'] . ")");

    if( isset( $_SESSION['friend_uid'] ) )
    {
      sql_query( "insert into friends (id1, id2, status) values (" . $_SESSION['uid'] . "," . $_SESSION['friend_uid'] . ", 0)" );
  		$API->feedAdd("G", $gid, $_SESSION['uid'], $_SESSION['friend_uid'], $gid );
    }
    else
      $API->feedAdd("G", $gid, $_SESSION['uid'], $_SESSION['uid'], $gid );
  }

  $API->fbGetPages();
  $API->prepWelcomeEmail();

  $uid = $_SESSION['uid'];
  $API->uid = $_SESSION['uid'];
  
  include( "../inc/create_you_have_connections_email.php" );

	if ($site == "m")
		header("Location: /follow.php");
	else
		header("Location: /welcome");  
		//header("Location: /signup/update_profile.php");

//As of 10-31-2012, the user is forwarded to the welcome page
//Where the remainder of the sign up process happens.

}
else
{
	// existing user
  $fbAccountUID = quickQuery("select uid from users where fbid=" . $_SESSION['fbid'] );

  if( isset( $API->uid ) )
  {
    //If the user is trying to login with a facebook account, and the current account is not verified
    // just forward the user to their facebook account.
    $currentAccountVerified = quickQuery( "select verify from users where uid='" . $API->uid . "'" );
    if( $fbAccountUID != $API->uid && $currentAccountVerified == 0 )
    {
      //Log out current user
      foreach (array_keys($_SESSION) as $k)
        unset($_SESSION[$k]);

      $_SESSION['fbid'] = $fbinfo['id'];
      $_SESSION['fbaccess_token'] = $params['access_token'];

      unset( $API->uid );
    }
  }

	if ( empty( $API->uid ) ) //Account exists, user was not already logged in with an account
	{		
    $info = mysql_fetch_array($x, MYSQL_ASSOC);

    if ($info['active'] == -1) //banned
		{
			header("Location: /banned.php");
			die();
		}

		if (empty($info['name']))
		{
			$info['name'] = $fbinfo['name'];
			sql_query("update users set name='" . addslashes($fbinfo['name']) . "' where uid=" . $info['uid']);
		}

		$_SESSION['uid'] = $info['uid'];
		$_SESSION['name'] = $info['name'];
		$_SESSION['admin'] = $info['admin'];
		$_SESSION['verify'] = $info['verify'];

    $API->restoreSessionVariables($info['uid']);

    $API->uid = $info['uid'];
	}
	else
  {
      if(  mysql_num_rows($x) > 0 && $fbAccountUID != $API->uid )
    {
      //Trying to merge account
  		$_SESSION['merge'] = array("fbid" => $_SESSION['fbid'], "fbusername" => addslashes($fbinfo['name']), "acct" => "f");
  		unset($_SESSION['fbid']);
  		unset($_SESSION['fbsess']);
  		$API->saveUserPicture(SITE_FACEBOOK, "http://graph.facebook.com/{$API->fbid}/picture?type=large");
  		header("Location: /merge.php");
      exit;
    }
    //else the user is already signed in!
  }

  if( $_SESSION['uid'] )
  	$_SESSION['pic'] = quickQuery("select pic from users where uid='" . $_SESSION['uid'] . "'");
  sql_query("update users set ip=" . ip2long($_SERVER['REMOTE_ADDR']) . ",lastlogin=now(),fbid=" . $_SESSION['fbid'] . ",fbaccesstoken='{$_SESSION['fbaccess_token']}',fbusername='" . addslashes($fbinfo['name']) . "',gender='$gender',dob='$birthday' where uid=" . $_SESSION['uid']);
  $_SESSION['verify'] = 1;

  //If the current account is not verified, we update the user's account information
  $currentAccountVerified = quickQuery( "select verify from users where uid='" . $API->uid . "'" );
  if( $currentAccountVerified == 0 )
  {
    mysql_query( "update users set email='{$fbinfo['email']}', name='" . addslashes($fbinfo['name']) . "', username='" . addslashes($fbinfo['name']) . "', verify=1 WHERE uid='" . $API->uid . "' limit 1" );
  	$_SESSION['name'] = $fbinfo['name'];
  	$_SESSION['username'] = $fbinfo['name'];
  	$_SESSION['admin'] = 0;
    $_SESSION['uid'] = $API->uid;
  }

  $pic = $API->saveUserPicture(SITE_FACEBOOK, "http://graph.facebook.com/{$API->fbid}/picture?type=large");

	if( $pic >= 0 )
  {
    //New profile picture
    $_SESSION['pic'] = $pic;
    sql_query("update users set pic={$_SESSION['pic']} where uid={$API->uid}");
  }

  $API->fbGetPages();

	if ($_SESSION['addacct'])
	{
		unset($_SESSION['addacct']);
		$API->feedAdd("J", SITE_FACEBOOK, $_SESSION['uid'], $_SESSION['uid']);

		header("Location: /settings/");
	}
	elseif (isset($_SESSION['lastMediaViewed']))
  {
		header("Location: " . $API->getMediaURL($_SESSION['lastMediaViewed']['type'], $_SESSION['lastMediaViewed']['id']));
  }
	else
  {
    if( isset( $_SESSION['login_redirect'] ) && $_SESSION['login_redirect'] != '' )
    {
      header( "Location: " . $_SESSION['login_redirect'] );
    }
    else
    {
    	header("Location: /welcome");
    }
    
    $_SESSION['login_redirect'] = '';
  }
}

function spamWall()
{
	global $facebook, $API, $siteName, $site;
	//spam the user's wall letting the world know he joined mediabirdy

	$action_links[0]['text'] = "Find me on $siteName";
	$action_links[0]['href'] = "http://" . $_SERVER['HTTP_HOST'] . $API->getProfileURL();

	$attachment['media'][0]['type'] = "image";
	if ($site == "m")
	{
		$attachment['media'][0]['src'] = "http://" . $_SERVER['HTTP_HOST'] . "/images/birdy2.png";
		$attachment['name'] = "I just joined $siteName!";
		$attachment['caption'] = "$siteName does one simple thing:  it allows you to share media on Facebook and Twitter!";
		$message = "Look at this! It lets us share our videos and photos on Facebook and Twitter with one upload, in real time!";
	}
	else
	{
		$attachment['media'][0]['src'] = "http://" . $_SERVER['HTTP_HOST'] . "/images/sitelogo.png";
		$attachment['name'] = "About $siteName.com";

		$attachment['caption'] = "$siteName allows you to grow your B2B, B2C and professional relationships in the Maritime industries. Be discovered and more!";
//		$attachment['caption'] = "$siteName connects people who live, work and engage in and around the water.  It's used by enthusiasts, businessses and professsionals around the globe.  Give it a try.";
    $message = "I'm using $siteName. A professional network for individuals and businesses that maintain an identity in the maritime industries. "  . $_SERVER['HTTP_HOST'] . "/?ref=" . $_SESSION['uid'];
	}

  $attachment['media'][0]['type'] = "image";
	$attachment['media'][0]['src'] = "http://" . $_SERVER['HTTP_HOST'] . "/images/salt_badge100.png";
	$attachment['media'][0]['href'] = "http://" . $_SERVER['HTTP_HOST'] . "/?ref=" . $_SESSION['uid'];
	$attachment['href'] = "http://" . $_SERVER['HTTP_HOST'] . "/?ref=" . $_SESSION['uid'];

	$API->fbSafeCallMethod(null, $facebook, 'facebook.stream.publish',
		array('message' => $message,
			  'attachment' => json_encode($attachment),
			  'action_links' => json_encode($action_links)
			  )
		);

}

?>
