var api_key = "f0182cecbf86fe327794dba2fccd4b2c";
var fbPermissionChecking = 0;
var fbHadAllPermissions = true;

if (script == "invite")
	var fbPermissionsNeeded = ["publish_stream"];
else
	var fbPermissionsNeeded = ["user_status", "publish_stream", "offline_access", "email"];

function fbCheckPermissionsHandler(data)
{
	//alert("fbCheckPermissionsHandler " + data);
	if (data != "0") // we have this permission
	{
		if (fbPermissionChecking < fbPermissionsNeeded.length)
			checkNextPermission(); //check the next
		else
			fbLoginComplete(); //we have them all
	}
	else // we don't have this permission, so try asking for it
	{
		FB.Connect.showPermissionDialog(fbPermissionsNeeded.join(","), fbCheckPermissionsGranted, false, null);
		fbHadAllPermissions = false;
	}
}

function fbLoginComplete()
{
	if (script == "invite")
		getFBContacts();
	else
		window.location = "/fbconnect/loggedin.php";
}

function fbCheckPermissionsGranted(result)
{
	//alert("fbCheckPermissionsGranted " + result);
	if (result == fbPermissionsNeeded.join(","))
		// we have them all
		fbLoginComplete();
	else
		// if not, we give up and let the user try to login again
		alert("Before you continue, you must grant these Facebook permissions to mediaBirdy.");
}

function fbloggedin(uid)
{
	fbPermissionChecking = 0;
	checkNextPermission();
}

function checkNextPermission()
{
	FB.Facebook.apiClient.users_hasAppPermission(fbPermissionsNeeded[fbPermissionChecking++], fbCheckPermissionsHandler);
}

FB.init(api_key, "/fbconnect/xd_receiver.htm");
