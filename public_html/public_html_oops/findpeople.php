<?php

include "inc/inc.php";
$API->requireLogin();
//$API->requireSite("s");

include "header.php";

$API->setToDoItem( TODO_FIND_CONTACTS );
?>

<script language="javascript" type="text/javascript">
<!--
script = "invite"; //trick the invite script
//-->
</script>

<div style="border: 1px solid #d8dfea; border-top: 0;">
	<div class="contentleft piconly">
		<div class="profilepic_container">
			<div class="profilepic" style="text-align: center;">
				<div class="borderhider">&nbsp;</div><div class="borderhider2">&nbsp;</div>
				<a href="<?=$API->getProfileURL()?>"><img width="48" height="48" src="<?=$API->getThumbURL(1, 48, 48, $API->getUserPic())?>" alt="" /></a>
			</div>
		</div>
	</div>

	<div class="content_container_border">
	<?php

	$user = array("uid" => $API->uid, "username" => $API->username, "name" => $API->name);

	$items = array(
			array("/images/email_add.png", "invite contacts", "/invite.php"),
			array("/images/find.png", "find people", "/findpeople.php"),
			array("/images/vcard.png", "contact manager", "/contacts.php")
		);

	showUserWithItems($user, $items);

	?>
	</div>

	<div class="contentright">
		<?php include "inc/connect.php"; ?>
    <? if( $site != "m" && $API->adv ) { ?>
		<div class="subhead" style="padding-top:0px; margin: 0px 0 5px;"><div style="float:left;">Sponsors</div><div style="float:right;"><a href="http://www.salthub.com/adv/create.php" style="font-size:8pt; font-weight:300; color:rgb(0, 64, 128);">create an ad</a>&nbsp;</div><div style="clear:both;"></div></div>
		<?php showAd("companion"); } ?>
		<?php include "pages/gyml.php"; ?>
	</div>

	<div class="content_container_full" style="clear: left;">
		<div class="pageid" style="background-image: url(/images/find.png);">Find People on <?=$siteName?></div>
		<div style="font-size: 9pt; padding-bottom: 10px;">
			<?=$siteName?> is used by friends, families, enthusiasts, businesses, and professionals around the globe.&nbsp; The same ones you know or want to know in the real world.&nbsp; Use the tools below to find them.
		</div>

		<div style="border: 1px solid #d8dfea; background: #F3F8FB; padding: 5px;">
			<div class="smtitle"><img src="/images/page_add.png" />Find your social contacts and people you e-mail.<span>(recommended)</span></div>
			<?php include "inc/contacts.php"; showContactsTop(); ?>
		</div>

		<div style="border: 1px solid #d8dfea; padding: 5px; margin-top:10px;">
      <div class="smtitle"><img src="/images/person_tie.png" />People where you work, past and present</div>
      <div style="margin: 0px 0 0 0px;" class="pymkbig">
        <? $num_results = 12; include "inc/people_where_you_work_interface.php"; ?>
      </div>
    </div>

		<div style="border: 1px solid #d8dfea; background: #F3F8FB; padding: 5px; margin-top: 10px;">
			<div class="smtitle"><img src="/images/page.png" />People you may know or want to know</div>
			<div style="font-size: 9pt;">
				The results below are provided by comparing your profile data and using the social graph to find matches.&nbsp; For better results, we strongly recommend completing your <a href="<?=$API->getProfileURL();?>">profile information</a>.
			</div>
			<div id="pymk_container1" class="pymkbig">
				<?php $limit = 12; $_GET['like'] = $params['q']; include "getpymk.php"; ?>
			</div>
			<div style="clear: both; padding: 10px; text-align: right;">
				<a href="javascript:void(0);" onclick="javascript:pymkShuffle(1);" style="font-size: 9pt; text-decoration: none;"><img src="/images/arrow_rotate_anticlockwise.png" style="vertical-align: bottom;" alt="" /> Shuffle matches</a>
			</div>
		</div>

		<div style="border: 1px solid #d8dfea; background: #F3F8FB; padding: 5px; margin-top: 10px;">
			<div class="smtitle"><img src="/images/page.png" />Find people manually</div>
			<div style="font-size: 9pt; padding-bottom: 10px;">
				Enter a name or an e-mail address below to find people and businesses you may know.
			</div>
			<input type="text" class="txtsearch" id="txtpymksearch" style="width: 215px; height: 20px; font-size: 9pt; background-position-y: 3px;" /><input style="font-size: 9pt; height: 20px;" type="text" onclick="javascript:document.getElementById('pymk_container2').innerHTML = '';document.getElementById('txtpymksearch').value = '';" onfocus="document.getElementById('txtpymksearch').focus();" class="btnx" value="X" />
			&nbsp; <input type="button" class="button" value="&nbsp; Search &nbsp;" onclick="javascript:pymkSearch(2, document.getElementById('txtpymksearch').value);" style="height: 24px;" />
			<div id="pymk_container2" class="pymkbig"></div>
			<div style="clear: both;"></div>
		</div>
	</div>

	<div style="clear: both;"></div>
</div>

<?php
$scripts[] = "/invite.js";
$scripts[] = "/fbinvited.js.php";
include "footer.php";
?>