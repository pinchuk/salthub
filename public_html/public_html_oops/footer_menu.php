<?
/*
The menu used on the footer of the site.  This is used for people who are not logged in.
*/

if( !isset( $footer_width ) ) $footer_width=960;
?>

<style>
.footerbar
{
  margin-top:4px;
  clear:both;
  width:100%;
  height:170px;
  background-color:#edeff4;
  border-top:2px solid #adbbca;
  font-size:9pt;
}

.footergroup
{
  float:left;
  padding:20px;
  width:120px;
}

.footergroup .title
{
  font-weight:bold;
  float:left;
  clear:left;
  color: #333;
  margin-bottom:10px;
}

.footergroup a
{
  float:left;
  clear:both;
}

.footerbottom
{
  font-size:9pt;
  padding-top:10px;
  height:25px;
  background-color:#3c3c3c;
  color:#fff;
  width:100%;
  margin-left:auto;
  margin-right:auto;
}

.footerbottom div a {
   color:#fff;
}

</style>

  <div class="footerbar">
    <div style="width:<?=$footer_width?>px; margin-left:auto; margin-right:auto;">

    <div class="footergroup">
      <span class="title">Tools On <?=$siteName?></span>
      <a href="/about-advertising.php">Advertise</a>
<? if( $API->isLoggedIn() ) {?>
      <a href="/pages/page_create.php">Add your Business</a>
      <a href="/pages/page_create.php?sel=2">Add your Vessel</a>
<? } else { ?>
      <div style="clear:both;"></div>
      Add your Business<br />
      Add your Vessel<br />
<? } ?>
      <a href="/pages/claim_company.php">Claim your Business</a>
<? if( $API->isLoggedIn() ) { ?>
      <a href="/employment/emp_create.php">Post a Job</a>
<? } else { ?>
      <div style="clear:both;"></div>
      Post a Job
<? } ?>
      <a href="/vessels/vessel_live_tracking.php">Ship Tracking | AIS</a>
    </div>

    <div class="footergroup">
      <span class="title" style="width:125px;">Help and Account Info</span>
      <a href="/signup/resetpw.php">Login Assistance</a>
<? if( $API->isLoggedIn() ) {?>
      <a href="/settings">Settings</a>
      <a href="/settings/photos.php">Manage My Photos</a>
      <a href="/settings/videos.php">Manage My Videos</a>
      <a href="/billing">Billing</a>
<? if( $API->admin ) { ?>
      <a href="/admin">Administration</a>
<? } ?>
<? if( $script == "media" ) { ?>
      <div style="clear:both;"></div>
      <? if( $_GET['t'] == "P" ) { ?>
      <a href="javascript:void(0);" onclick="javascript:adminDeleteMedia();">Delete Photo</a>
      <? } elseif( $_GET['t'] == "V" ) { ?>
      <a href="javascript:void(0);" onclick="javascript:adminDeleteMedia();">Delete Video</a>
      <? } ?>
<? } ?>

<? } else { ?>
      Settings<br />
      Manage My Photos<br />
      Manage My Videos<br />
      Billing<br />
<? }?>
    </div>

    <div class="footergroup">
      <span class="title">Business &amp; News</span>
      <a href="/page/15528-SaltHub/logbook">SaltHub News</a>
      <a href="/about.php">About Us</a>
      <a href="/tos.php">Terms of Service</a>
      <a href="/signup/sitemap.php">Site Map</a>
      <a href="/privacy.php">Privacy</a>
      <a href="javascript:void(0);" onclick="javascript:openSendMessagePopup('','','SaltHub',1187580,0,0,0 );">Contact Us</a>
    </div>

    <div class="footergroup">
      <span class="title">Explore <?=$siteName?></span>
      <a href="/directory.php">Business Directory</a>
<? if( $API->isLoggedIn() ) {?>
      <a href="/vessels">Vessel Directory</a>
      <a href="/employment">Employment</a>
      <a href="/pages">Professional Pages</a>
      <a href="/mediahome.php">Popular Media</a>
      <a href="/search_m.php?t=V">Videos</a>
      <a href="/search_m.php?t=P">Photos</a>
<? } else { ?>
      Vessel Directory<br />
      Employment<br />
      Professional Pages<br />
      Popular Media<br />
      Videos<br />
      Photos<br />
<? } ?>
    </div>

    <div class="footergroup">
      <span class="title">Connect with Others</span>
<? if( $API->isLoggedIn() ) {?>
      <a href="/invite.php">Invite Connections</a>
      <a href="/findpeople.php">Find Connections</a>
      <a href="/contacts.php">Contact Manager</a>
      <a href="javascript:void(0);" onclick="javascript:showFriendsPopup(<?=$API->uid?>);">My Connections</a>
<? } else { ?>
      Invite Connections<br />
      Find Connections<br />
      Contact Manager<br />
      My Connections<br />
<? } ?>
    </div>

    <div class="footergroup">
      <span class="title">Connect with Us</span>
      <a href="/page/15528-SaltHub"><img src="http://pelorusholdings.com/images/salthub.png" style="vertical-align: bottom;" width="20" height="20"/> SaltHub</a>
			<a href="http://www.facebook.com/pages/SaltHub/196023203785837" style="margin-top:5px;"><img src="/images/facebook16.png" style="vertical-align: bottom;" alt="" /> facebook</a>
			<a href="http://twitter.com/#!/<?=SITE_TWITTER_SN?>" style="margin-top:5px;"><img src="/images/twitter16.png" style="vertical-align: bottom;" alt="" /> Twitter</a>
    </div>


    <div style="clear:both;"></div>

    </div>

  </div>
    <div class="footerbottom">
      <div style="width:<?=$footer_width?>px; margin-left:auto; margin-right:auto;">
        <div style="float:left; margin-left:100px;">&copy; 2012 <?=$siteName?></div>
        <div style="float:right; margin-right:100px;"><a href="#top" style="text-decoration:underline;">Top</a></div>
      </div>
    </div>

<? if( $API->admin ) { ?>
<script language="javascript" type="text/javascript">
<!--
function adminDeleteMedia()
{
if (confirm("Are you sure you want to delete this media?"))
	postAjax("/settings/deletemedia.php", "admindelete=1&type=<?=$type?>&chk<?=$media['id']?>=on", "adminDeleteMediaHandler");
}

function adminDeleteMediaHandler(data)
{
alert("Media deleted.");
}




//-->
</script>
<? } ?>