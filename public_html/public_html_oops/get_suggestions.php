<?php
/*
Generates content for the home page "connect and engage" section, which is basically a list of
suggested contacts to connect to.

*/

if (!isset($API)) //not from an include
{
	include_once "inc/inc.php";
}

//Load blocked uids
$blocked_uids = array();
$q = mysql_query( "select blocked_uid from blocked_users where uid='" . $API->uid . "'" );
while( $r = mysql_fetch_array( $q ) )
  $blocked_uids[] = $r['blocked_uid'];

if( empty( $limit ) )
{
  if( isset( $_GET['limit'] ) )
    $limit = intval( $_GET['limit'] );
  else
    $limit = 3;
}

$uids = array();
$friends = array();

$q = sql_query( "select if(id1=" . $API->uid . ",id2,id1) as uid,status from friends where (" . $API->uid . ") in (id1,id2)" );
while( $r = mysql_fetch_array( $q ) )
  if( $r['status'] == 1 )
    $friends[] = $r['uid'];
  else
    $uids[] = $r['uid'];

$q = sql_query( "select id2 from friend_suggestions where id1='" . $API->uid . "'" );
while( $r = mysql_fetch_array( $q ) )
  if( !in_array( $r['id2'], $friends ) )
    $uids[] = $r['id2'];


$q = sql_query( "select eid from contacts where site='2' and uid='" . $API->uid . "' and eid" );
while( $r = mysql_fetch_array( $q ) )
  if( !in_array( $r['eid'], $friends ) )
    $uids[] = $r['eid'];

$q = sql_query( "select users.uid from users inner join contacts on users.email=contacts.eid where contacts.uid='" . $API->uid . "' and contacts.site > 2" );
while( $r = mysql_fetch_array( $q ) )
  if( isset( $r['uid'] ) && !in_array( $r['uid'], $friends ) )
    $uids[] = $r['uid'];

//Look for people with the same email domain
//if( sizeof( $uids ) < 10 )
{


  $email = quickQuery( "select email from users where uid='" . $API->uid . "'" );

  if( $email != "" )
  {
    $data = explode( "@", $email );



    if( sizeof( $data ) > 1 )
    {
      $domain = strtolower( trim( $data[1] ) );

      $restricted_domains = array( "gmail.com", "yahoo.com", "aol.com", "hotmail.com", "msn.com" );

      if( !in_array( $domain, $restricted_domains ) )
      {
        $q = sql_query( "select users.uid from users where users.uid!='" . $API->uid . "' and users.email like '%$domain%'" );
        while( $r = mysql_fetch_array( $q ) )
          if( isset( $r['uid'] ) && !in_array( $r['uid'], $friends ) )
            $uids[] = $r['uid'];
      }
    }
  }
}


$friends = $API->getFriendsUids();

if( $limit == 1 )
{
  $blocked_uids = array_merge( $_SESSION['s_and_r_uids'], $blocked_uids );
}
else
{
  $_SESSION['s_and_r_uids'] = array();
}

$uids = array_diff( $uids, $blocked_uids );
$uids = array_diff( $uids, $friends );

$uids = array_unique( $uids );

$cond = getConditions( $uids );

$sql = "select uid,username,pic,name from users where active=1 and uid != " . $API->uid . " and $cond order by rand() limit $limit";
//echo $sql;

$x = sql_query($sql);
/*
if( mysql_num_rows( $x ) < $limit )
{
  $q = sql_query( "select uid from users where pic>0 and active=1 order by rand() limit $limit" );
  while( $r = mysql_fetch_array( $q ) )
  {
    if( !in_array( $r['uid'], $blocked_uids ) )
      $uids[] = $r['uid'];
  }

  $cond = getConditions( $uids );

  $x = sql_query("select uid,username,pic,name from users where active=1 and uid != " . $API->uid . " and $cond order by rand() limit $limit");
}
*/

$c = 0;

$num_results = mysql_num_rows( $x );

while ($user = mysql_fetch_array($x, MYSQL_ASSOC))
{
  $c++;
	$profileURL = $API->getProfileURL($user['uid'], $user['username']);

  $div_name = "s_and_r-$c";
  if( $limit == 1 )
  {
    $div_name = $_GET['div'];
  }
  else
  {
    //We'll only show the div if we don't already have one.
    // this will be the case whenever limit > 1
	?>
	<div class="pymk" id="<?=$div_name?>">
  <? } ?>
		<a href="<?=$profileURL?>" onmouseout="javascript:tipMouseOut();" onmouseover="javascript:showTip2(this,<?=$user['uid']?>,'U');">
			<img src="<?=$API->getThumbURL(1, 48, 48, $API->getUserPic($user['uid'], $user['pic']))?>" alt=""/>
		</a>
		<div class="nowrap">
			<a href="<?=$profileURL?>" class="userlink"><?=$user['name']?></a><br />
			<?=friendLink($user['uid'], null, null, true)?><br />
			<a href="javascript:void(0);" onclick="javascript:showSendMessage(<?=$user['uid']?>, '<?=addslashes($user['name'])?>', '<?=$API->getThumbURL(0, 85, 128, $API->getUserPic($user['uid'], $user['pic']))?>');">Send message</a><br />
			<span id="hide<?=$user['uid'];?>_s"><a href="javascript:void(0);" onclick="javascript:hideUser(<?=$user['uid']?>, '<?=$div_name?>', 's_and_r');">Hide</a></span>
		</div>
  <? if( $limit > 1 ) { ?>
	</div>
	<?
  }

  $_SESSION['s_and_r_uids'][] = $user['uid'];
}




function getConditions( $uids )
{
  $i = 0;

  $cond = "(";
  foreach( $uids as $key => $value )
    if( $value != "" )
    {
      if( $i > 0 )
        $cond .= " OR ";
      $cond .= "uid=" . $value;
      $i++;
    }
  $cond .= ")";
  return $cond;
}
?>