<?
/*
Renders a list of pages that friends of the user has recently visited, similar to "get_suggestions.php".  Used on /home.php
*/

unset( $q );


if (!isset($API)) //not from an include
{
	include_once "inc/inc.php";
}

if( isset( $_GET['divname'] ) )
{
  echo $_GET['divname'] . chr(1);
}

$order = "lastviewed";

if( empty( $type ) )
{
  $type = $_GET['type'];
}

$friends = $API->getFriendsOfFriends();

$sql =  "SELECT DISTINCT p.gid, p.pid, p.gname FROM pages AS p INNER JOIN
(
	SELECT f.gid, f.ts FROM feed f INNER JOIN
	(
		SELECT DISTINCT gid FROM page_members WHERE uid IN ( " . implode( ",", $friends ) . ")
	) m ON m.gid = f.gid
) f ON p.gid=f.gid";

if( $type == "V" )
{
  //Vessel updates only
  $sql .= " WHERE p.type = " . PAGE_TYPE_VESSEL;
}
else
{
  $sql .= " WHERE p.type != " . PAGE_TYPE_VESSEL;
}

$sql .= " ORDER BY f.ts DESC";

file_put_contents ("/tmp/test.sql", $sql . "\n");

if( isset( $_GET['page'] ) && $_GET['page'] > 0 )
{
  $page = $_GET['page'];
  $offset = $page * 4;
  $sql .= " limit $offset, 4";
}
else
  $sql .= " limit 4";

if( sizeof( $friends ) > 0 )
{
  $q = mysql_query( $sql );
}

if( empty( $q ) || mysql_num_rows( $q ) < 4 )
{
  if( $type == "V" )
  {
    $sector = quickQuery( "select sector from users where uid='" . $API->uid . "'" );

    switch( $sector )
    {
      case 1331: $default_cat = 1223; break;
      case 1327: $default_cat = 1215; break;
      case 1305: $default_cat = 1216; break;
      case 1309: $default_cat = 1219; break;
      default:
        $default_cat = 1221;
      break;
    }
  }


  $sql = "SELECT SEARCH_RESULTS.gid, pages.pid, pages.gname from ( (
        select distinct
            gid
        from
            feed
        where LENGTH(type) > 0 AND ts > DATE_SUB(NOW(), INTERVAL 2 DAY) AND
            gid IN (SELECT gid FROM `pages` WHERE ";
    if( $type == "V" ) $sql .= "pages.cat=$default_cat AND pages.type=";
    else $sql .= "pages.type!=";
  $sql .= PAGE_TYPE_VESSEL . ") limit 50 ) ) SEARCH_RESULTS,
     pages WHERE pages.gid=SEARCH_RESULTS.gid
    AND pages.pid>0 AND pages.privacy=" . PRIVACY_EVERYONE . " order by rand() limit 4";

     
  $q = mysql_query( $sql );
}

if( empty( $fromEmail ) )
{
  while( $r = mysql_fetch_array( $q ) )
  {

    $gid = $r['gid'];
    $title = $r['gname'];
    $url = $API->getPageURL($gid);

    $info = $API->getPageInfo( $gid );

    if( stristr( $info['profile_pic'], "images" ) )
      $img = $info['profile_pic'];
    else
      $img = "/img/100x80" . $info['profile_pic'];

    echo '<div style="padding-left: 6px; float:left; width:100px; text-align:center; font-size:11px;">';
    echo '	<a href="' . $url . '"><img height="80" width="100" src="' . $img . '" alt="" />';
      echo '<br>' . $title;
    echo '</a>';
    echo '<div style="clear: both;"></div>';
    echo '</div>';
  }
}
else
{
  echo '<table cellpadding="0" cellspacing="10"><tr>';
  while( $r = mysql_fetch_array( $q ) )
  {
    $gid = $r['gid'];
    $title = $r['gname'];
    $purl = "http://www.salthub.com" . $API->getPageURL($gid);

    $info = $API->getPageInfo( $gid );

    if( stristr( $info['profile_pic'], "images" ) )
      $img = "http://www.salthub.com" . $info['profile_pic'];
    else
      $img = "http://www.salthub.com/img/100x80" . $info['profile_pic'];

    echo '<td align="center" style="font-size:9pt; " width="125" valign="top">
            ';
    echo '<a style="text-decoration: none; color: #326798;" href="' . $purl . '">
            ';
    echo '<img src="' . $img . '" width="100" height="75"><br />' . $title . '</a></td>';

  }
  echo '</tr></table>';
}
?>
