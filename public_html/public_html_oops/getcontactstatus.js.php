<?php

header("Content-type: text/javascript");

include "inc/inc.php";

$API->requireSite("s");
$API->requireLogin();

$previous_entries = array();

$x = sql_query("select eid,cid,c.name,site,u.uid,pic,invited,status as friends from contacts c
	left join users u on eid=if(site=" . SITE_OURS . ",u.uid,if(site=" . SITE_FACEBOOK . ",fbid,if(site=" . SITE_TWITTER . ",twid,u.email)))
	left join friends on ((id1={$API->uid} and id2=u.uid) or (id2={$API->uid} and id1=u.uid))
	where c.uid={$API->uid} and site != " . SITE_OURS . " and (u.uid != {$API->uid} or u.uid is null) order by c.name");

while ($y = mysql_fetch_array($x, MYSQL_ASSOC))
{
	$y['siteword'] = convertWordSite($y['site']);

  if( strpos( $y['eid'], "@" ) !== false )
  {
    $q = sql_query( "select name, uid, pic, email from users where email='" . $y['eid']. "'" );

    if( $r = mysql_fetch_array( $q ) )
    {
      $y['name'] = $r['name'];
      $y['name2'] = '<a href="' . $API->getProfileURL($r['uid']) . '">' . $r['name'] . "</a><br>" . $r['email'];
      $y['uid'] = $r['uid'];
    }
  }


	if ($y['uid'])
		$y['pic'] = $API->getThumbURL(1, 48, 48, $API->getUserPic($y['uid'], $y['pic']));
	elseif ($y['site'] == SITE_FACEBOOK || $y['site'] == SITE_TWITTER)
		$y['pic'] = $API->getContactPic($y);
	else
		$y['pic'] = "/images/social/48x48/{$y['siteword']}.png";

	unset($y['eid']);

  if( !in_array( $y['name'] . $y['site'], $previous_entries ) )
  {
  	$contacts[] = $y;
    $previous_entries[] = $y['name'] . $y['site'];
  }
}

echo "var contactStatus = " . json_encode($contacts) . ";";

?>

function getContactStatusHTML(c, color)
{
	html  = '<div class="contactstatus"' + (color ? ' style="background: #f7f7f7;"' : '') + '>';
	html += '	<div style="float: left;">';
	html +=	'		<img class="userpic" src="' + c.pic + '" alt="" />';
	html += '	</div>';
	html += '	<div class="info">';
if( typeof(c.name2) != "undefined"  )
	html += '		<div class="user">' + c.name2 + '</div>';
else
	html += '		<div class="user">' + c.name + '</div>';

	html += '		<div class="status">' + (c.uid ? "is a member" : (c.invited == 1 ? "invited" : "")) + '</div>';
	html += '	</div>';
	html += '	<div class="actions">';
	html += '		<img src="/images/' + c.siteword + '.png" alt="" />';
	html += '		<div class="link">';
	if (c.uid) // is member
	{
	if (c.friends == null)
	html += '			<a href="javascript:void(0);" onclick="javascript:addFriend(' + c.uid + ', this.parentNode, \'connection request pending\');">connect</a>';
	else if (c.friends == 0)
	html += '			connection request pending';
	else
	html += '			<a href="javascript:void(0);" onclick="javascript:showSendMessage(' + c.uid + ', \'' + c.name + '\', \'' + c.pic + '\');">send message</a>';
	}
	else if (c.invited == 1)
	html += '			<a href="javascript:void(0);" onclick="javascript:sendInvitation(' + c.cid + ', this.parentNode);">send reminder invitation</a>';
	else
	html += '			<a href="javascript:void(0);" onclick="javascript:sendInvitation(' + c.cid + ', this.parentNode);">invite</a>';
	html += '		</div>';
	html += '	</div>';
	html += '	<div style="clear: both;"></div>';
	html += '</div>';
	
	return html;
}