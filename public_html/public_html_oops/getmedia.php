<?php
/*
Change Log

8/10/2011 CB - Modified photo and video queries to improve performance; only viewing photos and videos that are public.  Removed the left join with users table


*/

include_once "inc/inc.php";
include_once "inc/mod_media.php";

$uid = intval($_GET['uid']);
$p = intval($_GET['p']);

if ($_GET['t'] == "V")
{
	$type = "V";
	$qPrefix = "select @type:='$type',id,views,created,title,descr,media.uid,name,username,hash from videos as media" . ($_GET['h1'] == "ive" ? " inner join views on link=media.id" : "") . " inner join users on users.uid=media.uid where" . $API->getPrivacyQuery() . "active=1 and ready=1";
}
else
{
	$type = "P";
//	$qPrefix = "select @type:='$type',media.id,views,media.created,title,descr,media.uid,name,username,hash" . ($site == "s" ? ",pdescr,ptitle" : "") . " from photos as media" . ($_GET['h1'] == "ive" ? " inner join views on link=media.id" : "") . " inner join users on users.uid=media.uid inner join albums on albums.id=media.aid where" . $API->getPrivacyQuery() . "active=1";
//	$qPrefix = "select @type:='$type',media.id,views,media.created,title,descr,media.uid,hash" . ($site == "s" ? ",pdescr,ptitle" : "") . " from photos as media" . ($_GET['h1'] == "ive" ? " inner join views on link=media.id" : "") . " inner join albums on albums.id=media.aid where (media.privacy=0 and media.reported=0)";
	$qPrefix = "select @type:='$type',media.id,views,media.created,media.uid,hash,aid" . ($site == "s" ? ",pdescr,ptitle" : "") . " from photos as media" . ($_GET['h1'] == "ive" ? " inner join views on link=media.id" : "") . " where (media.privacy=0 and media.reported=0 and media.uid!=832 and media.aid != 884372)";
}

$qSuffix = "limit " . ($p * 5) . ",6";

if ($_GET['h1'] == "browse")
{
	$h1 = "browse";
	$h2 = $_GET['h2'];

	switch ($_GET['h2'])
	{
		case "new":
		$qMid = "order by created desc";
		break;

		case "user":
		$qMid = "and media.uid=$uid order by created desc";
		break;

		default: //recently viewed
		$h2 = "recent";
		$qMid = "order by lastviewed desc";
		break;
	}
}
else //media ive viewed
{
	if (!$API->isLoggedIn())
		die();
	$h1 = "ive";
	$h2 = "";
	$qMid = "and views.uid=" . $API->uid . " order by views.ts desc";
}

//echo "$qPrefix $qMid $qSuffix";
$x = sql_query("$qPrefix $qMid $qSuffix");
echo mysql_error();

$i = 0;
$c = mysql_num_rows($x);

ob_start();
if ($p > 0 || $c > 5) {
?>
<div style="font-size: 9pt; padding: 5px 0 0 3px;">
	<div style="width: 65px; float: left;"><?php if ($p > 0) { ?><a href="javascript:void(0);" onclick="javascript:showMedia('<?=$h1?>', '<?=$h2?>', '<?=$type?>', <?=$p - 1?>);">Previous <img src="/images/left.png" alt="" /></a><?php } else echo "&nbsp;" ?></div>
	<?php if ($c > 5) { ?><a href="javascript:void(0);" onclick="javascript:showMedia('<?=$h1?>', '<?=$h2?>', '<?=$type?>', <?=$p + 1?>);"><img src="/images/right.png" alt="" /> Next</a><?php } ?>
	<div style="clear: both;"></div>
</div>
<?php
$pnHTML = ob_get_contents();
ob_end_clean();
echo $pnHTML;
}

if ($c > 0)
	while ($media = mysql_fetch_array($x, MYSQL_ASSOC))
	{
		if (++$i < 6)
			showMedia($media, $type);
	}
else
{
	?>
	<div class="noresults2">
		No <?=$type == "V" ? "videos" : "photos"?> found in this category.
	</div>
	<?php
}

echo $pnHTML;
?>