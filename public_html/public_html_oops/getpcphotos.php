<?php

include "inc/inc.php";

$s = intval($_GET['s']);
$l = intval($_GET['l']);

if ($l == 0)
	$l = 9;

$x = sql_query("select id,hash from photos where uid={$API->uid} order by created desc limit $s," . ($l + 1)); //add 1 to limit to peek and see if more photos exist
while ($y = mysql_fetch_array($x, MYSQL_ASSOC))
{
	$y['thumb'] = $API->getThumbURL(1, 48, 48, "/photos/{$y['id']}/{$y['hash']}.jpg");
	$y['img'] = $API->getThumbURL(0, 178, 266, "/photos/{$y['id']}/{$y['hash']}.jpg");
	unset($y['hash']);
	$data['photos'][] = $y;
}

if (count($data['photos']) == $l + 1) // more photos exist than what we got
{
	$data['more'] = 1;
	array_pop($data['photos']);
}
else
	$data['more'] = 0;

echo json_encode($data);

?>