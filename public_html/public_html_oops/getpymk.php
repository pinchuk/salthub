<?
/*
Renders a list of "People you may know" for a given user.  Used in various places on the site, including /home.php

*/

$not_from_include = false;

if (!isset($API)) //not from an include
{
  $not_from_include = true;

	$i = intval($_GET['i']);

	echo $i;
  echo chr(1);

	include_once "inc/inc.php";

	switch($i)
  {
    case 0: $limit = 6; break;
    case 1: $limit = 12; break;
    default: $limit = intval( $_GET['limit'] ); break;
  }

	$like = $_GET['like'];
}

$blocked_uids = array();
$q = mysql_query( "select blocked_uid from blocked_users where uid='" . $API->uid . "'" );
while( $r = mysql_fetch_array( $q ) )
  $blocked_uids[] = $r['blocked_uid'];

$blocked_uids[] = 832; //Make sure that "SaltHub" is not added to the list.

if( empty( $limit ) ) $limit = 6;

$limit *= 3;

$results = array();

if( !isset( $like ) )
{
  $friends = $API->getFriendsUids();
  $friends[] = $API->uid;
  $fof = $API->getFriendsOfFriends();

  for( $c = 0; $c < sizeof( $fof ) && $c < $limit; $c++ )
    if( !in_array( $fof[$c], $friends ) )
      $results[] = $fof[$c];
}


//Find people in this user's sector
if( sizeof( $results ) < $limit )
{
  $limit1 = $limit - sizeof( $results );

  $my_sector = 0;
  if( $like == "" )
    $my_sector = quickQuery( "select sector from users where uid='" . $API->uid . "'" );

  $query = "select uid from users where " . ($like ? "(email like '%$like%' or name like '%$like%' or contactfor like '%$like%' or username like '%$like%') and " : "") . "active=1 and uid != " . $API->uid . " and pic>0";
  if( $my_sector > 0 )
    $query .= " and sector='$my_sector'";
  $query .= " order by rand() limit $limit1";

  $x = sql_query($query);

  while ($result = mysql_fetch_array($x, MYSQL_ASSOC))
  {
    if( $result['uid'] != $API->uid )
      $results[] = $result['uid'];
  }
}




//If we still don't have enough people, just pick anyone with a photo.

if( sizeof( $results ) < $limit && empty( $like ) )
{
  $limit1 = $limit - sizeof( $results );

  $query = "select uid from users where active=1 and uid != " . $API->uid . " and pic>0 and active=1";
  $query .= " order by rand() limit $limit1";

  $x = sql_query($query);

  while ($result = mysql_fetch_array($x, MYSQL_ASSOC))
  {
    if( $result['uid'] != $API->uid )
      $results[] = $result['uid'];
  }
}

//If we're only getting one user, this means that the user selected "hide", so
// we don't want the user to see other users that are currently being displayed.
if( $limit == 1 )
{
  $blocked_uids = array_merge( $_SESSION['pymk_uids'], $blocked_uids );
  $results = array_diff( $results, $blocked_uids );
}
else
{
  $_SESSION['pymk_uids'] = array();
}

if( $like == "" )
{
  $results = array_diff( $results, $blocked_uids );
  $results = array_diff( $results, $friends );
}

$num_results = sizeof( $results );

shuffle( $results );

$c = 0;
$limit /= 3;
foreach( $results as $uid )
{
  if( intval( $uid ) == 0 || $uid == $API->uid )
    continue;
  else
    $c++;

  $user = $API->getUserInfo( $uid, "uid, username, pic, name" );

  if( sizeof( $user ) == 0 )
  {
    $c--;
    continue;
  }

	$profileURL = $API->getProfileURL($user['uid'], $user['username']);

  $div_name = "pymk-$c";
  if( $limit == 1 )
  {
    $div_name = $_GET['div'];
  }
  else
  {
    //We'll only show the div if we don't already have one.
    // this will be the case whenever limit > 1
	?>
	<div class="pymk" id="<?=$div_name?>">
  <? } ?>
		<a href="<?=$profileURL?>" onmouseout="javascript:tipMouseOut();" onmouseover="javascript:showTip2(this,<?=$user['uid']?>,'U');">
			<img src="<?=$API->getThumbURL(1, 48, 48, $API->getUserPic($user['uid'], $user['pic']))?>" alt="" />
		</a>
		<div class="nowrap">
			<a href="<?=$profileURL?>" class="userlink"><?=$user['name']?></a><br />
			<?=friendLink($user['uid'])?><br />
			<a href="javascript:void(0);" onclick="javascript:showSendMessage(<?=$user['uid']?>, '<?=addslashes($user['name'])?>', '<?=$API->getThumbURL(0, 85, 128, $API->getUserPic($user['uid'], $user['pic']))?>');">Send message</a><br />
			<span id="hide<?=$user['uid'];?>_p"><a href="javascript:void(0);" onclick="javascript:hideUser(<?=$user['uid']?>, '<?=$div_name?>', 'pymk' );">Hide</a></span>
		</div>
  <? if( $limit > 1 ) { ?>
	</div>
	<?
  }

  $_SESSION['pymk_uids'][] = $uid;

  if( $c >= $limit ) break;
}

if( $not_from_include )
  mysql_close();

?>