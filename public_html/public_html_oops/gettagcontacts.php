<?php

include "inc/inc.php";
$API->requireLogin();

$nameList = Array();

if ($_GET['type'] == "P")
	$type = "P";
else
	$type = "V";

$id = intval($_GET['id']);


if( $type == "P" )
{
  $xq = mysql_query("select width,height from photos where id=$id");
  if (mysql_num_rows($xq) == 0) die();

  $dims = mysql_fetch_array($xq, MYSQL_ASSOC);

  $full = scaleImage($dims['width'], $dims['height'], 635, 655);
  $prev = scaleImage($dims['width'], $dims['height'], 250, 258);

  $ratio = $prev[0] / $full[0];

  $tags = array();

  $x = mysql_query("select cid,x,y,custom from " . typeToWord($type) . "_tags where uid=" . $API->uid . " and " . strtolower($type) . "id=$id");
}
else
  $x = mysql_query("select cid, custom from " . typeToWord($type) . "_tags where uid=" . $API->uid . " and " . strtolower($type) . "id=$id");

if (mysql_num_rows($x) > 0)
	while ($y = mysql_fetch_array($x, MYSQL_ASSOC)) $tags[] = $y;

$x = mysql_query("select * from ((select 0 as cid,\"" . $API->name . "\" as name,null as eid) union (select cid,name,eid from contacts where uid=" . $API->uid . ")) as tmp order by name");

while ($contact = mysql_fetch_array($x, MYSQL_ASSOC))
{
	$found = false;

	if (count($tags) > 0)
  {
		foreach ($tags as $tag)
    {
			if ($contact['cid'] == $tag['cid'])
			{
        $tag['found'] = true;
        $tag['x'] = intval($tag['x']) * $ratio;
        $tag['y'] = intval($tag['y']) * $ratio;

        echo $contact['name'] . chr(1) . $contact['cid'] . chr(1) . $tag['x']. chr(1) . $tag['y'] . chr(1) . $tag['custom'] . chr(1) . 0 . chr(2);
				$found = true;
				break;
			}
    }

    //This is to avoid duplication since custom contacts are added to the normal contact list.
    if( !$tag['found'] )
    {
	  	foreach ($tags as $tag)
      {
        $name = quickQuery( "select name from custom_tags where ID='" . $tag['cid'] . "'" );
        if( strtoupper($name) == strtoupper($contact['name']) )
          $found=true; //Skip the one to avoid the duplicates, it will be picked up in the next foreach loop below
      }
    }
  }

  if( !$found && strtoupper($contact['name']) != strtoupper($contact['eid']) )
    echo $contact['name'] . chr(1) . $contact['cid'] . chr(1) . "-1" . chr(1) . "-1" . chr(1) . "0" . chr(1) . 0  . chr(2);

  $nameList[] = $contact['name'];
}

if (count($tags) > 0)
	foreach ($tags as $tag)
    if( !$tag['found'] && $tag['custom'] )
    {
      $name = quickQuery( "select name from custom_tags where ID='" . $tag['cid'] . "' and gid=0" );
      if( !empty( $name ) )
      {
        $tag['found'] = true;
        $tag['x'] = intval($tag['x']) * $ratio;
        $tag['y'] = intval($tag['y']) * $ratio;

        echo $name . chr(1) . $tag['cid'] . chr(1) . $tag['x']. chr(1) . $tag['y'] . chr(1) . $tag['custom'] . chr(1) . 0 . chr(2);
        $nameList[] = $name;
      }
    }

if (count($tags) > 0)
	foreach ($tags as $tag)
    if( !$tag['found'] && $tag['custom'] )
    {
      $name = quickQuery( "select name from custom_tags where ID='" . $tag['cid'] . "' and gid>0" );
      if( !empty( $name ) )
      {
        $tag['found'] = true;
        $tag['x'] = intval($tag['x']) * $ratio;
        $tag['y'] = intval($tag['y']) * $ratio;

        echo $name . chr(1) . $tag['cid'] . chr(1) . $tag['x']. chr(1) . $tag['y'] . chr(1) . $tag['custom'] . chr(1) . 1 . chr(2);
        $nameList[] = $name;
      }
    }

$x = mysql_query("select gname, pages.gid from pages left join page_members on page_members.gid=pages.gid where pages.active=1 and page_members.uid=" . $API->uid . " order by gname");
while( $pages = mysql_fetch_array( $x ) )
{
  if( !in_array( $pages['gname'], $nameList ) )
    echo $pages['gname'] . chr(1) . $pages['gid'] . chr(1) . "-1" . chr(1) . "-1" . chr(1) . "2" . chr(1) . 1 . chr(2);
}

?>