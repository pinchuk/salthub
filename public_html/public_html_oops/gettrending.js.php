<?php

header("Content-type: text/javascript");

include "inc/inc.php";

$notIn = "'" . str_replace(",", "','", $_GET['notIn']) . "'";

// limits
$l1 = intval($_GET['l1']);
$l2 = intval($_GET['l2']);

if ($l1 > 10) $l1 = 10;
if ($l2 > 10) $l2 = 10;

$l3 = $l1 + $l2;
if ($l3 == 0) $l3 = 6;
/*
$query = "select @type:=type as type,id,ifnull(ptitle,title) as title,hash,unix_timestamp(gr8) as gr8 from (
(
select @type:='P' as type,privacy,reported,media.id,media.uid,title,ptitle,hash,greatest(ifnull(media.created,0),ifnull(viewed,0),ifnull(liked,0),ifnull(commented,0)) as gr8
	from photos as media inner join albums on media.aid=albums.id
	left join (select id,max(ts) as viewed from media_views where type='P' group by id) as mv on media.id=mv.id
	left join (select link,max(ts) as liked from likes where type='P' group by link) as l on media.id=l.link
	left join (select link,max(created) as commented from comments where type='P' group by link) as c on media.id=c.link
	" . ($l1 > 0 ? "limit $l1" : "") . "
)
union
(
select @type:='V' as type,privacy,reported,media.id,media.uid,title,null as ptitle,hash,greatest(ifnull(media.created,0),ifnull(viewed,0),ifnull(liked,0),ifnull(commented,0)) as gr8
	from videos as media
	left join (select id,max(ts) as viewed from media_views where type='V' group by id) as mv on media.id=mv.id
	left join (select link,max(ts) as liked from likes where type='V' group by link) as l on media.id=l.link
	left join (select link,max(created) as commented from comments where type='V' group by link) as c on media.id=c.link
	" . ($l2 > 0 ? "limit $l2" : "") . "
)
) as media
where concat(type,media.id) not in ($notIn) and media.privacy=0 and media.reported=0 and gr8 > from_unixtime(" . intval($_GET['t']) . ") order by " . ($l1 ? "type desc," : "") . "gr8 desc limit $l3";
*/

$query = "select @type:=type as type,id,title as title,hash, aid,lastviewed from (
(
select @type:='P' as type,privacy,reported,media.id,media.uid,ptitle as title,hash,lastviewed,aid from photos as media order by lastviewed desc " . ($l1 > 0 ? "limit $l1" : "limit $l3") . "
)
union
(
select @type:='V' as type,privacy,reported,media.id,media.uid,title,hash,lastviewed,null as aid from videos as media order by lastviewed desc " . ($l2 > 0 ? "limit $l2" : "limit $l3") . "
)
) as media
where concat(type,media.id) not in ($notIn) and media.privacy=0 and media.reported=0 and lastviewed > from_unixtime(". intval($_GET['t']) . ") order by lastviewed desc limit $l3";


// $l = fopen("/home/mediabirdy.com/q", "w");
// fwrite($l, $query);
// fclose($l);

unset( $lastUpdated );
$media = array();

$x = sql_query($query) or die(mysql_error());

while ($y = mysql_fetch_array($x, MYSQL_ASSOC))
{
	if (!isset($lastUpdated))
		$lastUpdated = strtotime($y['lastviewed']);

  if( $y['type'] == 'P' && is_null( $y['title'] ) )
  {
    $y['title'] = quickQuery( "select title from albums where id='" . $y['aid'] . "'" );
  }

	$y['url'] = $API->getMediaURL($y['type'], $y['id'], $y['title']);
	$y['img'] = $API->getThumbURL(1, 100, 75, "/" . typeToWord($y['type']) . "s/" . $y['id'] . "/" . $y['hash'] . ".jpg");
	unset($y['hash'], $y['lastviewed']);
	$media[] = $y;
}

echo "var lastUpdated = $lastUpdated;\nvar mediaQueue = " . json_encode($media) . ";";

mysql_close();

?>