<?php
/*
Header file that is included by most pages, this file also includes header_s.php, which is the header bar
shown at the top of the page.

The file renders the meta data, java script includes.
*/

if ($site == "m")
{
	if ($_SERVER['HTTP_HOST'] != "www.mediabirdy.com")
	{
		header("Location: http://www.mediabirdy.com" . $_SERVER['REQUEST_URI']);
		die();
	}
}

include_once "inc/inc.php";


if ($site == "s" && !$noLogin)
	$API->requireLogin();



unset($_SESSION['addacct']); //this is set to "true" on the settings page - for when a user wants to add a tw/fb acct to their mb acct

//if ($API->isLoggedIn())
//	mysql_query("update users set lastaction=now() where uid=" . $API->uid);

//$isIE = strpos($_SERVER['HTTP_USER_AGENT'], "MSIE");


if( $site == "s" && stristr( $script, "/" ) !== false && empty( $title ) && $descr == "" )
{
  $dir = current( explode( "/", $script ) );
  switch( $dir )
  {
    case "employment":
      $title = "Employment | SaltHub - Find Maritime Jobs - Post Maritime Jobs - Crew - Marine Professionals";
      $descr = "Find and list shore based and seagoing positions associated with maritime and water related sectors.";
    break;

    case "pages":
      $title = "Pages | $siteName - Be heard - Get discovered - Connect";
    break;
  }
}

if (empty($title))
{
  if ($site == "m")
  	$title = "$siteName - Upload to Facebook and Twitter";
  else
  	$title = $defaultPageTitle;
}

//Lookup to see if we have have new meta data set in from admin
$meta_tags = queryArray( "select title, keywords, descr from meta_tags where script='$script'" );
if( $meta_tags != NULL )
{
  $title = $meta_tags['title'];
  $descr = $meta_tags['descr'];
  $keywords = $meta_tags['keywords'];
}



if( $isDevServer )
  $title = "SH DEV [{$API->uid}] - " . $title;

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<meta http-equiv="X-UA-Compatible" content="IE=8" />
<?
if( isset( $meta ) )
{
  for( $c = 0; $c < sizeof( $meta ); $c++ )
  {
    if( isset( $meta[$c]['property'] ) && isset( $meta[$c]['content'] ) )
    {
      $property = $meta[$c]['property'];
      $content = $meta[$c]['content'];
      echo "<meta property=\"$property\" content=\"$content\"/>
";
    }
  }
}
?>

	<link rel="stylesheet" href="/style_<?=$site?>.css?ver=<?=filemtime($_SERVER['DOCUMENT_ROOT'] . "/style_$site.css")?>" type="text/css" />
	<link rel="stylesheet" href="/style.css?ver=<?=filemtime($_SERVER['DOCUMENT_ROOT'] . "/style.css")?>" type="text/css" />

<?
$css[] = "/jQuery.popeye/css/popeye/jquery.popeye.css";
$css[] = "/jQuery.popeye/css/popeye/jquery.popeye.style.css";

	if (is_array($css))
		foreach ($css as $url)
			echo "\t<link rel=\"stylesheet\" href=\"$url\" type=\"text/css\" />\n";
	?>
	<link rel="shortcut icon" href="/favicon_<?=$site?>.ico" />
	<meta name="title" content="<?=$title?>" />
<?
  if (!isset($descr ) && $site == "s" ) $descr = $defaultMetaDescription;
?>
  <meta name="description" content="<?=$descr?>" />

<? if( isset( $keywords ) ) { ?>
  <meta name="keywords" content="<?=$keywords?>" />
<? } ?>

<?php if ($thumb) { ?>	<link rel="image_src" href="<?=$thumb?>" / >
<?php }

$scripts[] = "/jQuery.popeye/lib/popeye/jquery.popeye-2.1.js";
$scripts[] = "/media.js";
$scripts[] = "/tipmain.js";
$scripts[] = "/simpleajax.js";
$scripts[] = "/common.js";
$scripts[] = "/jquery/jquery.tabSlideOut.v1.3.js";
$scripts[] = "/jquery/jquery.bpopup-0.8.0.min.js";
//$scripts[] = "/login.js.php";
$scripts[] = "/dropdown.js";
$scripts[] = "/report.js.php";
$scripts[] = "/tinybox/tinybox.js";
$scripts[] = "/upload/swfupload.js";
$scripts[] = "/upload/upload.js.php";
$scripts[] = "/upload/upload_photo.js";
$scripts[] = "/inc/tweetbox.js.php";
$scripts[] = "/crossbrowser.js";
$scripts[] = "/tween.js";
//$scripts[] = "/json.js";
$scripts[] = "http://static.ak.connect.facebook.com/js/api_lib/v0.4/FeatureLoader.js.php/en_US";
$scripts[] = "http://static.ak.fbcdn.net/connect.php/js/FB.Share";
$scripts[] = "http://connect.facebook.net/en_US/all.js#appId=" . $fbAppId . "&amp;xfbml=1";
$scripts[] = "http://platform.twitter.com/widgets.js";
if ($site == "s")
	$scripts[] = "/tags.js";

array_unshift( $scripts, "/jquery/jquery.min.js" );

   
loadJS($scripts);
unset($scripts);


?>
	<title><?=$title?></title>

	<script language="javascript" type="text/javascript">
		var isLoggedIn = <?=$API->isLoggedIn() ? $API->uid : "false"?>;
		var admin = <?=intval($API->admin)?>;
		var lastTime = <?=time()?>;
		var addthis_share = { templates: { twitter: '{{title}}: {{description}} via @<?=SITE_TWITTER_SN?>' } }
		var hostName = "<?=SERVER_HOST?>";
		var script = "<?=$script?>";
		var session_id = "<?=session_id()?>";
		var hash = "<?=$hash?>";
		var siteName = "<?=$siteName?>";
		var site = "<?=$site?>";
		<?php
		if ($isDevServer) echo 'var isDevServer = true;';
		if (is_array($jsVar))
			foreach ($jsVar as $k => $v)
				echo "\t\t$k = \"" . addslashes($v) . "\"";
		?>
<?php
if (isset($_SESSION['error']))
{
	echo 'showPopUp2("", "' . addslashes($_SESSION['error']) . '", 0);';
	unset($_SESSION['error']);
}
 ?>
	</script>
<? include_once( "inc/tracking_code.php" ); ?>
</head>

<body onload="javascript:if (typeof initPage == 'function') initPage();">

<?php if ($isDevServer) { ?>
<div style="font-size: 24pt; color: red; position: absolute; top: 0px; font-weight: bold;">
	DEV SERVER
</div>
<?php } ?>

<?php

//include "inc/loginoverlay.php";

include "header_$site.php";

if( $API->admin )
  $API->startTimer();
?>

<a name="top"></a>
<div class="content"<?=$background ? " style=\"background: $background;\"" : ""?>>
	<div id="tiplayer" style="visibility: hidden; position: absolute; z-index: 1000;"></div>
