<?php



if ($API->isLoggedIn())
{
	$unreadMsgs = $API->getUnreadMessageCount();

	$navLinks = array(
		"Home" => "/welcome",
		"Profile" => $API->getProfileURL(),
		"Connect" => array(
			array("Contact Manager", "/contacts.php", "vcard"),
			array("Connections", "javascript:showFriendsPopup(" . $API->uid . ");", "user"),
			array("Find Friends", "/findpeople.php", "find"),
			array("Invite Friends", "/invite.php", "email_notice")
				),
		"Inbox" . ($unreadMsgs > 0 ? " ($unreadMsgs)" : "") => array(
			array("Inbox" . ($unreadMsgs > 0 ? " ($unreadMsgs)" : ""), "/messaging/inbox.php", "email"),
			array("Connection Requests", "/messaging/inbox.php", "page_add"),
			array("Compose Message", "/messaging/compose.php", "email_add")
				),
		"Explore" => array(
			array("Popular Media", "/mediahome.php", "camera"),
			array("Videos", "/search_m.php?t=V", "television"),
			array("Photos", "/search_m.php?t=P", "images"),
			array("Pages", "/pages", "page"),
			array("Webs", "#", "world")
				),
		"Settings" => array(
			array("Account Settings", "/settings", "application_edit"),
			array("Manage My Photos", "/settings/photos.php", "images"),
			array("Manage My Videos", "/settings/videos.php", "television"),
			array("Billing", "/billing", "creditcards"),
			array("Advertising", "/adv", "sound_mega_phone"),
			array("Create a Page", "/pages/page_create.php", "page_add"),
			array("Post a job", "/employment/emp_create.php", "application_form_add")
				),
		"Logout" => "/logout.php"
		);
}
else
{
	$navLinks = array(
		"Home" => "/",
		"Connect" => array(
			array("Contact Manager", "/", "vcard"),
			array("Connections", "/", "user"),
			array("Find Connections", "/", "find"),
			array("Invite Connections", "/", "email_add")
				),
		"Explore" => array(
			array("Popular Media", "/", "camera"),
			array("Videos", "/", "television"),
			array("Photos", "/", "images"),
			array("Pages", "/", "page"),
			array("Webs", "/", "world")
				),
		"Sign Up" => "/",
		"Log In" => "/"
		);
}

?>

<!--<div class="waterheader"></div>-->

<div class="nav2">
	<div class="nav2content">
		<div class="title"><a href="/"><img src="/images/wavelogo.png" alt="" /></a></div>
<? if ($API->isLoggedIn() ) { ?>
    <div class="userpic">
      <? echo '<a href="' . $API->getProfileURL() . '" title="' . $API->name . '\'s Log Book"><img src="' . $API->getThumbURL(1, 24, 24, $API->getUserPic()) . '" alt="" /></a>';?>
    </div>

    <div class="black" style="margin-right:10px;"></div>

 	  <div class="image" style="margin-right:5px;"><a href="javascript: void(0);" onclick="javascript:showTweetBox(); return false;" title="Upload"><img src="/images/upload.png" width="16" height="16" alt="" /></a></div>

 	  <div class="image"><a href="javascript: void(0);" onclick="javascript:loadShare('compose', 1); return false;" title="Compose Message"><img src="/images/compose.png" width="16" height="16" alt="" /></a></div>

		<div class="image" style="padding-left:5px;">
      <?
      if( $unreadMsgs > 0 )
        echo '<a href="/messaging/inbox.php" title="' . plural($unreadMsgs, "unread message") . '"><img src="/images/email_notice.png" alt="" /></a>';
      else
        echo '<a href="/messaging/inbox.php" title="Inbox"><img src="/images/email.png" alt="" /></a>';
      ?>
    </div>

		<div class="image">
			<?php
			  $numNotifications = $API->getNotificationsCount();
      ?>
			<a href="javascript:void(0);" onclick="javascript:showNotifications();" title="You have <?=plural($numNotifications, "notification")?>"><img src="/images/notice_<? if ($numNotifications > 0) echo "red"; else echo "grey"; ?>.png" width="16" height="16" alt="" /></a>
			<div id="notifications_container">
				<div id="headnotifications">
					<div class="subhead">Notifications <span class="number">(<?=$numNotifications?>)</span></div>
					<div style="padding: 20px; text-align: center;">
						<img src="/images/wait.gif" alt="" />
					</div>
				</div>
			</div>
		</div>

    <? $numNotifications += $unreadMsgs; ?>

 		<div class="image"><a href="javascript:void(0);" title="Connections &amp; Management" onclick="javascript:showFriendsPopup(<? echo $API->uid; ?> );">
      <img src="/images/add_connection_<? echo ($API->getFriendRequestCount() > 0 ) ? "red" : "grey";?>.png" width="16" height="16" alt="" /></a>
    </div>

    <div style="width:7px; float:left;">&nbsp;</div>

		<div class="search" id="header_search">
			<div style="float: left;">
        <div style="position: relative;">
  				<input class="txt" data-autocomplete="get_search" data-ac_min="3" data-ac_limit="50" type="text" id="txtsearch" data-default="Search <?=$siteName?> ..." class="txtsearch">
          <div id="suggestionBox" style="padding:0px; position: absolute; left:5px; top:27px; width:288px; height:465px; z-index:101; display:none;">
          </div>
        </div>

			</div>
			<div class="bino"><a href="javascript:void(0);" onclick="javascript:searchDo();"><img src="/images/find.png" alt="" /></a></div>

		<?php
		showNavDrop('<span class="moresearch">explore</span><img src="/images/arrow_down.png" alt="" />', array(
			array("Business Directory", "/directory.php", "vcard"),
			array("Vessel Directory", "/vessels", "anchor"),
			array("Vessel Heat Map", "/vessels/vessel_live_tracking.php", "flames"),
			array("Employment", "/employment", "person_tie"),
			array("Pages", "/pages", "page"),
			array("Popular Media", "/mediahome.php", "camera"),
			array("Videos", "/search_m.php?t=V", "television"),
			array("Photos", "/search_m.php?t=P", "images")
				), "top: 22px; left: 0px;");
		?>

			<div style="clear: both;"></div>
		</div>

    <div style="width:75px; float:left;">&nbsp;</div>

		<div class="links"<?=$hideHeaderLinks ? ' style="visibility: hidden;"' : ""?>>
			<div style="float:left; padding-left:10px; padding-right:10px; padding-top:1px;"><a href="/">Home</a></div>

      <div style="float:left; padding-top:1px; padding-left:10px; padding-right:15px;">
			<?php
			showNavDrop("Connect", array(
				array("Contact Manager", "/contacts.php", "vcard"),
				array("Connections", "javascript:showFriendsPopup(" . $API->uid . ");", "user"),
				array("Find Connections", "/findpeople.php", "find"),
				array("Invite Connections", "/invite.php", "email_add")
					), "top: 20px; left: -20px;");
			?>
      </div>
		</div>


    <div style="padding-top:1px;">
    <?
		showNavDrop('Account <img src="/images/arrow_down_white.png"  width="11" height="9" alt="" />', array(
			array("Account Settings", "/settings", "application_edit"),
			array("Manage My Photos", "/settings/photos.php", "images"),
			array("Manage My Videos", "/settings/videos.php", "television"),
			array("Billing", ($isDevServer?"":("https://www." . $siteName . ".com")) . "/billing", "creditcards"),
			array("Advertising", "/adv", "sound_mega_phone"),
			array("Create a Page", "/pages/page_create.php", "page_add"),
			array("Post a job", "/employment/emp_create.php", "application_form_add"),
			array("Logout", "/logout.php", "door_out")
				), "top: 20px; left: -85px;");
		?>
    </div>

		<div style="clear: both;"></div>
<? } else { ?>
<div style="background: #1B3E5D; padding-bottom: 15px; margin-left: 15px; height: 27px;">
  <div style="float:left; padding-right:20px; color:#ffffff; font-size:9pt;"><a style=" margin-left:20px; color:#fff;" href="http://<?=SERVER_HOST?>">Home</a></div>
  <div style="float:left; padding-right:20px; color:#ffffff; font-size:9pt;"><a style="color:#fff;" href="/about.php">What is <? echo $siteName ?>?</a></div>
  <div style="float:left; padding-right:20px; color:#ffffff; font-size:9pt;"><a style="color:#fff;" href="/start">Join Today!</a></div>
  <div style="float:left; padding-right:20px; color:#ffffff; font-size:9pt;"><a style="color:#fff;" href="/start">Sign In</a></div>
 </div>
<? } ?>
	</div>
</div>

<div style="clear: both;"></div>

<?php
/* old navbar below:


		showNavDrop('<span class="moresearch">more search</span><img src="/images/arrow_down.png" alt="" />', array(
			array("Account Settings", "/settings", "application_edit"),
			array("Manage My Photos", "/settings/photos.php", "images"),
			array("Manage My Videos", "/settings/videos.php", "television")
				), "top: 22px; left: 0px;");



<div class="nav">
	<div class="navlinks"<?=$hideHeaderLinks ? ' style="display: none;"' : ""?>>
		<a href="/invite.php" class="invite">Invite Friends</a>
		<?php
		$i = 0;
		foreach ($navLinks as $title => $item)
		{
			if ($i == count($navLinks) - 2)
			{
				echo '<div style="float: right;">';
				$isFloating = true;
			}
			else
				echo '<span class="separator"> | </span>';

			if (is_array($item)) //dropdown
			{
				$id = substr(md5($title), 0, 5);
				?>
				<span class="navdropcontain" onmouseover="javascript:showDropDown('dd-<?=$id?>');" onmouseout="javascript:hideDropDown('dd-<?=$id?>');">
					<?=$title?>
					<div id="dd-<?=$id?>" class="navdrop">
						<?php
						foreach ($item as $menuitem)
						{
							if (substr($menuitem[1], 0, 11) == "javascript:")
								$menuitem[1] = 'javascript:void(0);" onclick="' . $menuitem[1];
							echo '<div onmouseup="javascript:if (event.button == 0) actionClicked(this);" class="item"><a href="' . $menuitem[1] . '"><img src="/images/' . $menuitem[2] . '.png" alt="" />' . $menuitem[0] . '</a></div>';
						}
						?>
					</div>
				</span>
				<?php
			}
			else
				echo '<a ' . ($isFloating ? 'class="unbolded" ' : '') . 'href="' . $item . '">' . $title . '</a>';

			$i++;
		}
		unset($title);
		//close div from float right
		echo "</div>";?>
	</div>
</div>
*/
?>