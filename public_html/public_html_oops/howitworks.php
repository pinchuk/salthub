<?php

$standalone = empty($script);

if ($standalone) // called as a standalone page
{
	include "inc/inc.php";
	?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
	<link rel="stylesheet" href="/style.css" type="text/css" />
	<title>mediaBirdy - tweet more efficiently, using media!</title>

</head>

<body style="background: #fff; margin: 10px;">
	<?php
}

?>

<div class="tabcontent" id="tabcontent1"<?=$standalone ? " style=\"border: 0; display: inline;\"" : ""?>>
	<div class="bigtext" style="padding: 10px 10px 0 10px;">
		How does mediaBirdy work?
	</div>
	<ul style="padding-right: 5px;">
		<li>If you have a Facebook or Twitter account, then you already have a mediaBirdy account.</li>
		<li>Just login to mediaBirdy with one of them and start uploading.&nbsp; If you belong to Facebook and Twitter, you can merge them on mediaBirdy and manage both accounts from one place.</li>
		<li>Then, photos and videos you upload to mediaBirdy will automatically be broadcast to both accounts for all your followers, friends, and fans to see.&nbsp;
			You can even upload your favorite YouTube videos!<?php if (!$API->isLoggedIn()) { ?>&nbsp; <a href="javascript:getStarted();">Sign in</a> and give it a try.<?php } ?></li>
	</ul>
	
	<div style="text-align: center;">
		<img src="/images/howitworks.png" alt="" />
	</div>
</div>

<?php

if ($standalone) // called as a standalone page
	echo "</body></html>";

?>