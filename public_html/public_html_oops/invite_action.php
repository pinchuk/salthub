<?php

include "inc/inc.php";

include "inc/phpmailer.php";


$API->setToDoItem( TODO_INVITE_CONTACTS );

$mail->IsSMTP();


if( isset( $_REQUEST['cid']) )
  $_POST['cid'] = $_REQUEST['cid'];

$ssite = $_SESSION['fetchedsite'];
if( $ssite == "facebook" ) $ssite = 0;
elseif( $ssite == "twitter" ) $ssite = 1;

if (isset($_POST['cid']))
{

	//coming from /invite.php on splash - wanting to reinvite a user
	$x = mysql_query("select * from contacts where cid=" . intval($_POST['cid']) . " and uid={$API->uid}");

  if (mysql_num_rows($x) == 0 )
   	$x = mysql_query("select * from contacts where eid=" . intval($_POST['cid']) . " and site='$ssite' and uid={$API->uid}");

	if (mysql_num_rows($x) == 0)
  {
    $cid = $_POST['cid'];
    $addid = $cid;

    //Add to contacts if we haven't already
    if( $ssite == 0 || $ssite == 1 )
      $name = $_SESSION['fetchedcontacts'][$cid];
    else
      $name = $_SESSION['fetchedcontacts'][$cid]['name'];

    $q = "insert into contacts (invited,eid,name,uid,site) values (1,'" . $cid . "','" . addslashes($name) . "'," . $API->uid . "," . $ssite . ")";
  	mysql_query($q);
  }
  else
  {
    //We have the contact already in the database
  	$contact = mysql_fetch_array($x, MYSQL_ASSOC);

  	if ($contact['invited'] == 0)
  		mysql_query("update contacts set invited=1 where cid={$contact['cid']}");

  	$_SESSION['fetchedcontacts'][$contact['eid']] = $contact['name'];
  	$_SESSION['fetchedsite'] = convertWordSite($contact['site']);

    $addid = $contact['eid'];

  	$add = array(0);
  }

	$mail->SMTPAuth = false;
	$mail->Host = "localhost";
	$mail->Port = 25;
}
else
{
	//add all the fetched contacts to contact manager
	$addAll = true;

	ob_start();
	include "contacts_add.php";
	$ok = ob_get_contents();
	ob_end_clean();

	if ($ok != "OK")
		$fbFailed[] = "Some of your contacts could not be added to contact manager.";
}
	
//now to the actual inviting

$msgTemplate = generateInviteMsgTemplate();

$FBmsgTemplate = quickQuery("select content from static where id='fb_invite'");
$FBmsgTemplate = str_replace("{NAME_FROM}", $API->name, $FBmsgTemplate);
$FBmsgTemplate = str_replace("{REF_LINK}", $_SERVER['HTTP_HOST'] . "/?ref=" . $API->uid, $FBmsgTemplate);

$TWmsgTemplate = quickQuery("select content from static where id='tw_invite'");
$TWmsgTemplate = str_replace("{NAME_FROM}", $API->name, $TWmsgTemplate);
$TWmsgTemplate = str_replace("{REF_LINK}", $_SERVER['HTTP_HOST'] . "/?ref=" . $API->uid, $TWmsgTemplate);

$invites = 0;

if (!isset($_POST['cid']) && ($_SESSION['fetchedsite'] == "hotmail" || $_SESSION['fetchedsite'] == "msn" || $_SESSION['fetchedsite'] == "yahoo" || $_SESSION['fetchedsite'] == "gmail"))
{
	$mail->Username = $_SESSION['fetchedcredentials']['user'];
	$mail->Password = $_SESSION['fetchedcredentials']['pass'];
	$mail->SMTPAuth = true;
	
	if ($_SESSION['fetchedsite'] == "gmail")
	{
		$mail->Host = "smtp.gmail.com";
		$mail->SMTPSecure = "ssl";
		$mail->Port = 465;
	}
	elseif ($_SESSION['fetchedsite'] == "yahoo")
	{
		$mail->Host = "smtp.mail.yahoo.com";
		$mail->Port = 25;
	}
	else //hotmail or msn
	{
		$mail->Host = "smtp.live.com";
		$mail->SMTPSecure = "tls";
		$mail->Port = 25;
	}
}

if (!is_array($_SESSION['fetchedcontacts']))
{
	//should be a manual entry (email)

  $uid = quickQuery( "select uid from users where email='" . addslashes( $_REQUEST['email'] ) . "'" );
  if( $uid > 0 )
  {
    echo "OK|$uid";
    exit;
  }
  else
  {
  	$sendMail = true;
  	$invites++;
	  $emails[] = array("email" => $_REQUEST['email'], "name" => $_REQUEST['name']);
  }
}
else //from imported contacts
{
	// $add is set from contacts_add.php

	$i = 0;

	foreach ($_SESSION['fetchedcontacts'] as $email => $name)
	{
		if ($_SESSION['fetchedsite'] == "facebook")
    {
      if( $email == $addid ) //The variable "email" is actually the facebook ID, due to the format of the data from getfbcontacts.php
      {
				if (!isset($facebook))
					$facebook = $API->startFBSession();

        $name = $_SESSION['fetchedcontacts'][$addid];
				$msg = getAltMsg(str_replace("{NAME_TO}", $name, $FBmsgTemplate));

				//$result = @$facebook->api_client->call_method("facebook.stream.publish", array("target_id" => $_SESSION['fetchedcontacts'][$i]['uid'], "message" => $msg));
				$result = $facebook->api(array('method' => 'stream.publish', 'target_id' => $email, "message" => $msg));
				$success = current(explode("_", $result)) == $email;

				if ($success)
				{
					$invites++;
					mysql_query("insert into fbinvites (uid,fb_uid) values (" . $API->uid . "," . $email . ")");
				}
				else
					$fbFailed[] = $_SESSION['fetchedcontacts'][$addid];
			}
    }
    else if ($_SESSION['fetchedsite'] == "twitter")
    {
      if( $email == $addid ) //The variable "email" is actually the facebook ID, due to the format of the data from getfbcontacts.php
      {
        $name = $_SESSION['fetchedcontacts'][$addid];
    		$msg = getAltMsg(str_replace("{NAME_TO}", $name, $TWmsgTemplate));

        $result = $API->twSendMessage( $API->uid, $msg, $addid );
        //print_r( $result );
        if( !$result || isset( $result->error ) )
          $twFailed[] = $_SESSION['fetchedcontacts'][$addid];
      }
    }
    else if (in_array($i, $add))
		{
			$sendMail = true;
			$emails[] = array("email" => $email, "name" => $name);
			$invites++;
		}

		$i++;
	}
	
	if ($_REQUEST['otheraction'] == "1")
	{
		//nothin yet
	}
}

if ($sendMail)
{
	$mail->SetFrom("notifications@$siteName.com", $siteName);
	$mail->Subject = "Connect with " . $API->name;
	
	$mail->isHTML(true);
	
	foreach ($emails as $name => $email)
	{
		$mail->ClearAddresses();
		$mail->AddAddress($email['email']);
		
		$msg = str_replace("{NAME_TO}", getFirstName( $email['name'] ), $msgTemplate);

		$mail->Body = emailTemplate($msg, $email['email']);
		$mail->AltBody = getAltMsg($msg);

		$mail->Send();
	}
}

unset($_SESSION['fetchedcontacts']);
unset($_SESSION['fetchedsite']);
unset($_SESSION['fetchedacct']);

mysql_query("update users set invites=invites+$invites where uid=" . $API->uid);

function getAltMsg($msg)
{
	global $API;
  global $site;

  if( $site == "s" )
	  $msg = html_entity_decode(str_replace("&nbsp;", " ", str_replace("<br />", "\n", str_replace("\n", "", $msg))));
  else
  	$msg = html_entity_decode(str_replace("&nbsp;", " ", str_replace("<br />", "\n", str_replace("\n", "", $msg)))) . "\n\nHere is the link to sign up: " . $_SERVER['HTTP_HOST'] . "/?ref=" . $API->uid;
	$msg = str_replace("<p>", "\n\n", $msg);
	$msg = strip_tags($msg);
	
	return trim($msg);
}

echo "OK";
if (count($fbFailed) > 0)
	echo "|" . implode("<br/>", $fbFailed);
if (count($twFailed) > 0)
	echo "|" . implode("<br/>", $twFailed);
?>