<?php

include_once "inc/inc.php";
include_once "inc/mod_comments.php";

if (!$API->isLoggedIn()) die();

$type = addslashes(substr($_POST['type'], 0, 3));

if ($_POST['like'] == 0)
	$like = 0;
elseif ($_POST['like'] == 1)
	$like = 1;
else
	$like = -1;

$link = intval($_POST['link']);
$layout = intval($_POST['layout']);

if( $type == "" )
{
  $type = $_REQUEST['type'];
  $link = $_REQUEST['link'];
  $like = $_REQUEST['like'];
}

if ($like == -1)
{
	$lid = intval(quickQuery("select lid from likes where uid=" . $API->uid . " and type='$type' and link=$link"));
	if ($lid > 0)
	{
		sql_query("delete from likes where lid=$lid");
		sql_query("delete from feed where type='L' and link=$lid");
	}
}
else
{
	sql_query("insert into likes (uid,type,link,likes) values (" . $API->uid . ",'$type',$link,$like)");
  //echo mysql_error();

	$lid = mysql_insert_id();
}

//echo "$like|";

if ($like == -1)
{
	//showLikeLinks($type, $link, $layout);
}
else
{
	//showLikeStats($type, $link);

  $likeType = "L";

  switch ($type )
  {
    case "P":
	  	$x = sql_query("select photos.uid,aid,jmp,title,descr from photos join albums on albums.id=photos.aid where photos.id=$link");
  		$nType = NOTIFY_PHOTO_LIKE;
    break;

    case "V":
	  	$x = sql_query("select uid,title,descr,jmp from videos where id=$link");
  		$nType = NOTIFY_VIDEO_LIKE;
    break;

    case "rss":
    case "r":
      $x = sql_query( "select 0 as uid, gname as original_poster, jmp, rss_feed.title, rss_feed.preview AS descr, pages.pid AS pid from pages left join rss_feed on rss_feed.gid=pages.gid where rss_feed.id='$link'" );
      $link = quickQuery( "select fid from feed where link='$link' and type='rss'" );
  		$nType = NOTIFY_FEED_LIKE;
      $likeType = "rss"; //RSS Feed
    break;

    case "W":
      $x = sql_query( "select users.uid, name as original_poster, jmp from users left join wallposts on wallposts.uid=users.uid where wallposts.sid='$link'" );
      $link = quickQuery( "select fid from feed where link='$link' and type='W'" );
  		$nType = NOTIFY_FEED_LIKE;
      $likeType = "L"; //Wall Feed
    break;

    default:
      unset( $nType );
      unset( $likeType );
    break;
  }

	if (isset($x) && mysql_num_rows($x) == 1)
	{
		$info = mysql_fetch_array($x, MYSQL_ASSOC);
  }

  $info['like'] = $like;
  $info['id'] = $lid;
  $info['type'] = $likeType;

  if( isset( $likeType ) )
  {
		$API->feedAdd("L", $info['id']);
  }

	if (isset($x) && mysql_num_rows($x) == 1)
  {
    if( empty( $info['jmp'] ) || $info['jmp'] == NULL )
    {
      $url = $API->getMediaURL($type, $link);
      $jmp = shortenURL( "http://" . SERVER_HOST . $url );
      if( $type == "P" )
        sql_query( "update photos set jmp='$jmp' where photos.id='$link' limit 1" );
      else
        sql_query( "update videos set jmp='$jmp' where id='$link' limit 1" );

      $info['jmp'] = $jmp;
    }

    // For photo attachments in the FB like posts
    $info['type'] = $type;
    $info['id'] = $link;

    if( isset( $nType ) )
    {
  		$API->sendNotification($nType, $info);
    }
	}
}
?>