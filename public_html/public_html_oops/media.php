<?php
/*
This is the page that displays photo and video media.

Change History:
8/9/2011 - Removed "Report Media" link, duplicate with "Report" link.
11/4/2011 - Added support for facebook linter.
12/21/2012 - Made SSL requests to the media page forward to non-SSL pages to fix video playback problems.

*/

if( stristr( $_SERVER['HTTP_USER_AGENT'], "facebookexternal" ) !== false )
{
  $facebookCrawler = true;
}

session_start();

if( isset( $_REQUEST['ref'] ) )
{
	$_SESSION['ref'] = $_REQUEST['ref'];
  if( isset( $_REQUEST['cid'] ) )
    $_SESSION['customcid'] = $_REQUEST['cid'];
}


include "inc/inc.php";

if( $facebookCrawler )
{
  $API->uid = 91;
  $noLogin = true;
  $API->admin = true;
}



$id = intval($_GET['id']);

if ($_GET['t'] == "A")
{
	$id = quickQuery("select mainImage from albums where albums.id=$id");
	$_GET['t'] = "P";
}

if ($_GET['t'] == "P")
{
	$type = "P";
	$table = "photos";
  $css[] = "/lightbox/css/jquery.lightbox-0.5.css";
}
else
{
	$type = "V";
	$table = "videos";

	$scripts[] = "/flowplayer/flowplayer-min.js";
}

if( isSSL() && $type == "V")
{
  //If we're connected over SSL then we need to switch to http because
  // videos will not play over https
  $url = $API->getMediaURL( $type, $id );

  header( "Location: http://www." . $siteName . ".com" . $url );
  exit;
}


include "inc/mod_comments.php";

//max dimensions are 635x655
$maxWidth = 635;
$maxHeight = 655;

$error = 0;

if ($type == "V")
{
  if( $API->admin )
  	$x = sql_query("select @type:='$type',media.*,shares,hash,id,pic,username,name,width,height" . ($site == "s" ? ",catname" : "") . " from videos as media " . ($site == "s" ? "natural join categories" : "") . " inner join users on media.uid=users.uid where media.id=$id");
  else
  	$x = sql_query("select @type:='$type',media.*,shares,hash,id,pic,username,name,width,height" . ($site == "s" ? ",catname" : "") . " from videos as media " . ($site == "s" ? "natural join categories" : "") . " inner join users on media.uid=users.uid where" . $API->getPrivacyQuery() . "active=1 and media.id=$id");
}
else //photo
{
  if( $API->admin )
  	$x = sql_query("select @type:='$type',media.*,shares,hash,media.id,aid,pic,username,name,title,descr" . ($site == "s" ? ",albType,pdescr,ptitle,catname" : "") . " from photos as media " . ($site == "s" ? "natural join categories" : "") . " inner join users on media.uid=users.uid join albums on media.aid=albums.id where media.id=$id");
  else
  	$x = sql_query("select @type:='$type',media.*,shares,hash,media.id,aid,pic,username,name,title,descr" . ($site == "s" ? ",albType,pdescr,ptitle,catname" : "") . " from photos as media " . ($site == "s" ? "natural join categories" : "") . " inner join users on media.uid=users.uid join albums on media.aid=albums.id where" . $API->getPrivacyQuery() . "active=1 and media.id=$id");

  if( mysql_error() )
  {
    //echo "select @type:='$type',media.*,shares,hash,media.id,aid,pic,username,name,title,descr" . ($site == "s" ? ",albType,pdescr,ptitle,catname" : "") . " from photos as media " . ($site == "s" ? "natural join categories" : "") . " inner join users on media.uid=users.uid join albums on media.aid=albums.id where media.id=$id";
    echo  mysql_error();
  }

}

if (mysql_num_rows($x) == 0)
{
  //echo "select @type:='$type',media.*,shares,hash,media.id,aid,pic,username,name,title,descr" . ($site == "s" ? ",albType,pdescr,ptitle,catname" : "") . " from photos as media " . ($site == "s" ? "natural join categories" : "") . " inner join users on media.uid=users.uid join albums on media.aid=albums.id where media.id=$id";
	$error = 1;

  //exit;
}
else
{
	$media = mysql_fetch_array($x, MYSQL_ASSOC);
/*
	$nextId = sql_query("select @type:='$type',title,media.id,hash from $table as media" . ($type == "P" ? " inner join albums on albums.id=aid" : "") . " where" . $API->getPrivacyQuery() . "media.id > " . $media['id'] . " and " . ($type == "V" ? "ready=1" : "aid is not null") . " order by media.id limit 1");
	if (mysql_num_rows($nextId) == 0) //start from the first
		$nextId = sql_query("select @type:='$type',title,media.id,hash from $table as media" . ($type == "P" ? " inner join albums on albums.id=aid" : "") . " where" . $API->getPrivacyQuery() . "media.id != " . $media['id'] . " and " . ($type == "V" ? "ready=1" : "aid is not null") . " order by media.id limit 1");

	$nextMedia = mysql_fetch_array($nextId, MYSQL_ASSOC);

	$prevId = sql_query("select @type:='$type',title,media.id,hash from $table as media" . ($type == "P" ? " inner join albums on albums.id=aid" : "") . " where" . $API->getPrivacyQuery() . "media.id < " . $media['id'] . " and " . ($type == "V" ? "ready=1" : "aid is not null") . " order by media.id desc limit 1");
	if (mysql_num_rows($prevId) == 0) //start from the last
		$prevId = sql_query("select @type:='$type',title,media.id,hash from $table as media" . ($type == "P" ? " inner join albums on albums.id=aid" : "") . " where" . $API->getPrivacyQuery() . "media.id > " . $media['id'] . " and " . ($type == "V" ? "ready=1" : "aid is not null") . " order by media.id desc limit 1");
	$prevMedia = mysql_fetch_array($prevId, MYSQL_ASSOC);
*/

	if ($type == "V")
	{
		if (empty($media['width']))
			$error = 2;
		elseif (empty($media['title']))
			$error = 3;
		else
		{
			$ratio = $media['width'] / $media['height'];
			$thumb = "http://" . SERVER_HOST . $API->getThumbURL(0, 350, 350, "/videos/" . $media['id'] . "/" . $media['hash'] . ".jpg");
		}
	}
	elseif ($type == "P")
	{
		$albumCount = quickQuery("select count(*) from photos where aid=" . $media['aid']);

		if ($site == "m")
			$ratio = $media['ratio'];
		else
			$ratio = $media['width'] / $media['height'];

		//let's find where this baby falls in the album so we can find the right page on the carousel
		$c = quickQuery("select count(*) from photos where aid=" . $media['aid'] . " and id < " . $media['id']);
		$carouselPage = floor($c / 6);

		$imgPath = "/photos/" . $media['id'] . "/" . $media['hash'] . ".jpg";
/*
		if ($isDevServer && !file_exists(".$imgPath"))
		{
			sql_query("delete from photos where id=" . $media['id']);
			header("Location: " . $API->getMediaURL($type, $nextMedia['id'], $nextMedia['title']));
			die();
		}
*/
		$thumb = "http://" . SERVER_HOST . $API->getThumbURL(0, $maxWidth, $maxHeight, $imgPath);
	}
}

$encodedURL = urlencode("http://" . SERVER_HOST . $API->getMediaURL($type, $media['id'], $title));

/*
//get twitter share count
$tmp = @file_get_contents("http://urls.api.twitter.com/1/urls/count.json?url=$encodedURL%2F");
$i = strpos($tmp, '"count":');
if ($i > 0)
{
	$tmp = substr($tmp, $i + 8);
	$i = strpos($tmp, ",");
	$tmp = substr($tmp, 0, $i);
	$twCount = intval($tmp);
}

//get facebook share count
$tmp = @file_get_contents("http://api.ak.facebook.com/restserver.php?v=1.0&method=links.getStats&urls=[%22$encodedURL%22]&format=json&callback=fb_sharepro_render");
$i = strpos($tmp, '"share_count":');
if ($i > 0)
{
	$tmp = substr($tmp, $i + 14);
	$i = strpos($tmp, ",");
	$tmp = substr($tmp, 0, $i);
	$fbCount = intval($tmp);
}*/

if ($ratio > 1) //wider than tall
{
	$newWidth = $maxWidth;
	$newHeight = ceil($newWidth / $ratio);
	
	if ($newHeight > $maxHeight)
	{
		$newHeight = $maxHeight;
		$newWidth = ceil($newHeight * $ratio);
	}
}
else //taller than wide or square
{
	$newHeight = $maxHeight;
	$newWidth = ceil($newHeight * $ratio);
	
	if ($newWidth > $maxWidth)
	{
		$newWidth = $maxWidth;
		$newHeight = ceil($newWidth / $ratio);
	}
}

if ($error == 0)
{
	$updateViews = false;
	
	$ip = ip2long($_SERVER['REMOTE_ADDR']);
	sql_query("insert into media_views (id,type,ip) values (" . $media['id'] . ",'$type',$ip)");
	
	if (mysql_affected_rows() == 1) //user has not already viewed media
		$updateViews = true;
	else //user has already viewed media
	{
		//check to see if they viewed within past 5 min
		sql_query("update media_views set ts=now() where id=" . $media['id'] . " and type='$type' and ip=$ip and ts < now() - 300");
		$updateViews = mysql_affected_rows() == 1;
	}
	
	if ($updateViews)
		sql_query("update $table set lastviewed=current_timestamp,views=views+1 where id=" . $media['id']);

	if ($API->isLoggedIn())
		sql_query("insert into views (type,link,uid) values ('$type'," . $media['id'] . "," . $API->uid . ") on duplicate key update ts=current_timestamp");
	
	$title = $media['ptitle'] ? $media['ptitle'] : $media['title'];
	$descr = $media['pdescr'] ? $media['pdescr'] : $media['descr'];
}
else
{
	switch ($error)
	{
		case 1:
		$msg = "The requested media was not found.";
		break;

		case 2:
		case 3:
		$msg = "This video is not done being processed.&nbsp; Please try back later.";
		break;
	}
}

$meta = array();

$meta[] = array( "property" => "og:title", "content" => $title );
$meta[] = array( "property" => "og:url", "content" => "http://" . SERVER_HOST . $API->getMediaURL($type, $media['id'], $title) );
$meta[] = array( "property" => "og:image", "content" => $thumb );

$meta[] = array( "property" => "og:site_name", "content" => $siteName );
$meta[] = array( "property" => "og:type", "content" => "article" );
$meta[] = array( "property" => "fb:app_id", "content" => $fbAppId );

$scripts[] = "/comments.js";
$scripts[] = "/mod_media.js";
$scripts[] = "/showembedmedia.js";
//$scripts[] = "/yesnobox.js";
$scripts[] = "/email.js";
$scripts[] = "http://s7.addthis.com/js/250/addthis_widget.js#username=mediabirdy";

$background = "#fff";



include "header.php";

if ($msg)
{
	echo "<div class=\"error\">$msg</div>";
	include "footer.php";
	die();
}

$_SESSION['lastMediaViewed'] = array("type" => $type, "id" => $id);

if (!$API->isLoggedIn()) { ?>

<div style="padding: 5px;">
	<div style="float: left;">
		<div class="yellowinfo">
			<div class="title"><?=$siteName?> lets you add videos and photos to your Tweets and Facebook!</div>
			If you have a Facebook or Twitter account, then you already<br />have a <?=$siteName?> account.&nbsp; <a href="javascript:showLogin();">login now &gt;&gt;</a>
		</div>
	</div>
	<div style="float: right;">
 		<?php  if( $API->adv ) { showAd("greeninfo"); } ?>
	</div>
	<div style="clear: both;"></div>
</div>
<?php } ?>


<div style="float: left;">
	<div class="mediabox">
		<div style="float: left; width: 53px; text-align: center;">
			<a href="<?=$API->getProfileURL($media['uid'], $media['username'])?>"><img height="48" width="48" src="<?=$API->getThumbURL(1, 48, 48, $API->getUserPic($media['uid'], $media['pic']))?>" alt="" /></a>
		</div>
		<div style="float: left; padding-left: 8px;">
			<div class="mediatitle">
				<?=$title?>
			</div>
			<div class="mediasubtitle">
				<span>by: <a href="<?=$API->getProfileURL($media['uid'], $media['username'])?>"><?=$media['name']?></a><?php if ($site == "s" && $API->isLoggedIn() && $media['uid'] != $API->uid) { ?><?php echo " &nbsp;|&nbsp; " . friendLink($media['uid']); } ?></span>
				<span><?=formatDate($media['created'])?></span>
				<span style="margin: 0; margin-left:-5px;">View user's &#0133; <a href="<?=$API->getProfileURL($media['uid'], $media['username'])?>/videos">videos</a> | <a href="<?=$API->getProfileURL($media['uid'], $media['username'])?>/photos">photos</a><?php if ($site == "s") { ?> | <a href="javascript:void(0);" onclick="javascript:showFriendsPopup(<?=$media['uid']?>);"><? echo ($site=="m") ? 'friends' : 'connections'; ?></a><?php } ?></span>
			</div>
		</div>
		<div style="clear: both;"></div>
	</div>

	<?php
	$heightWithoutPadding = 170;
	$padding = round(($newHeight - $heightWithoutPadding) / 12);
	?>
	
	<div class="mediabox" style="border-top: 1px solid #ccc; padding: 3px 0; width: <?=$maxWidth?>px;">
		<div style="float: left; width: <?=$maxWidth?>; text-align: center;" id="mediacontainer">
			<?php
      $containerUrl = quickQuery( "select container_url from users where uid='" . $media['uid'] . "'" );


			if ($type == "V" && strlen($media['hash']) == 32) //video
			{
				showVideoPlayer($newWidth, $newHeight, $media);
			}
			elseif ($type == "V") //youtube
			{
				$size = array($maxWidth, round($maxWidth / (640 / 360)));

			?>
				<object width="<?=$size[0]?>" height="<?=$size[1]?>" style="z-index: 990;">
					<param name="movie" value="http://www.youtube.com/v/<?=$media['hash']?>&hl=en_US&fs=0&rel=0&"></param>
					<param name="allowFullScreen" value="true"></param>
					<param name="allowscriptaccess" value="always"></param>
					<param name="wmode" value="transparent" />
					<embed src="http://www.youtube.com/v/<?=$media['hash']?>&hl=en_US&fs=1&rel=0&" type="application/x-shockwave-flash" allowscriptaccess="always" wmode="transparent" allowfullscreen="true" width="<?=$size[0]?>" height="<?=$size[1]?>" style="z-index: 990;">
					</embed>
				</object>
			<?php
			}
			else
			{
			?>
			<table border="0" cellpadding="0" cellspacing="0" style="width: <?=$maxWidth?>px">
				<tr>
					<td>
						<?php // the logic to determine the width of the photo - for mbirdy we don't care - for splash we need to see if the image fills up the width of the container for tagging purposes, if not, make the container smaller ?>
						<div style="position: relative; margin: 0 auto; width: <?=$site == "m" ? $newWidth : ($newWidth > $media['width'] ? $media['width'] : $newWidth)?>px;" id="gallery">
<?
if( $newWidth > $media['width'] ) $newWidth = $media['width'];
if( $newHeight > $media['height'] ) $newHeight = $media['height'];
?>
							<!-- <a href="javascript:;" onclick="javascript:showMediaPopup(<?=$id;?>,'<?=$type?>');"><img width="<?=$newWidth?>" height="<?=$newHeight?>" src="<?=$thumb?>" onmousemove="javascript:if (site=='s') boxTagOnPhoto(this, event);" id="thephoto" alt="" /></a> -->
              <a href="javascript:void(0);" onclick="showMediaPopup(<?=$id;?>,'<?=$type?>');"><img width="<?=$newWidth?>" height="<?=$newHeight?>" src="<?=$thumb?>" onmousemove="javascript:if (site=='s') boxTagOnPhoto(this, event);" id="thephoto" alt="" /></a>
							<div id="usertag" class="usertag" style="display: none;" onclick="javascript:userTagBoxClicked();"><div>&nbsp;</div></div>
							<div id="nametag" class="nametag" style="display: none;"></div>
						</div>
					</td>
				</tr>
			</table>
			<?php } ?>
		</div>
		<div style="clear: both;"></div>
		<?php if ($type == "P" && $albumCount > 1) { ?>
		<div style="margin-top: 2px;">
			<div class="mediastat" style="float: left; padding: 13px 0 0 0; border: 0px;">
				<img src="/images/photos.png" alt="" /><br />
				<?php
				switch ($media['albType'])
				{
					case 2:
					echo "profile photos";
					break;

					case 1:
					echo "single photos";
					break;

					case 0:
					echo "album images";
					break;
				}
				?> &nbsp;<img src="/images/arrow_right.png" alt="" />
			</div>
			<div class="albumcarousel" id="albumcarousel">&nbsp;</div>
			<div style="clear: both;"></div>
		</div>
		<?php } ?>
	</div>
	<?
  if ($site == "s")
  {
    include( "media_tags_and_description.php" );
  }
  ?>

	<div class="mediainfo" style="display:none;">
		<div>Views:<br /><span><?=$media['views'] + ($updateViews ? 1 : 0)?></span></div>
		<div>Comments:<br /><span id="numcomments">&nbsp;</span></div>
		<div style="<?=$site == "m" ? "border-right: 0;" : ""?>">Shares:<br /><span id="numshares"><?=$media['shares'] + $fbCount + $twCount?></span></div>
		<?php if ($site == "s") { ?>
		<div style="width: 140px; border-right: 0;">Category:<br /><span><?=$media['catname']?></span></div>
		<?php } ?>
	</div>

<!--	<div style="padding-top: 10px; clear: both;">
		<table border="0" width="635" cellpadding="0" cellspacing="0" class="mediaactions">
			<tr>
        <td style="padding-right: 20px; font-size:9pt; padding-top:5px;">
          <a type="button_count" href="javascript:void(0);" onclick="javascript: openPopupWindow( 'http://www.facebook.com/sharer/sharer.php?u=<?= urlencode( "http://" . SERVER_HOST . $API->getMediaURL($type, $media['id'], $title) ); ?>&t=<?=urlencode($media['title']);?>', 'Share', 550, 320 );">
            <img src="/images/facebook_share.png" width="58" height="20" alt="" />
          </a>
        </td>
				<td><a href="http://twitter.com/share" class="twitter-share-button" data-text="<?=$media['descr']?>" data-count="horizontal" data-via="media_Birdy">Tweet</a></td>
				<td><div class="action" style="background-image: url(/images/email_go.png);" onclick="javascript:<?php if ($site == "m") echo "if (showEmail('$type', " . $media['id'] . "))"; else echo "loadShare('$type', 1, " . $media['id'] . ");";?> addShare();">share / e-mail</div></td>
				<?php if ($site == "s") { ?>
				<td><div class="action" style="background-image: url(/images/page_add.png);" onclick="javascript:shareInPage('<?=$type?>', <?=$media['id']?>, '<?=$media['hash']?>');">share in page</div></td>
				<td><div class="action" style="background-image: url(/images/layout_add.png);" onclick="javascript:addToLog('<?=$type?>', <?=$media['id']?>, '<?=typeToWord($type)?> added to log', this);">add to my log</div></td>
				<?php } ?>
			</tr>
		</table>
	</div>-->

	<div style="float: left; margin-top:20px;">
		<?php showCommentsArea($type, $media['id'], $media['jmp'], $API->uid == $media['uid']); ?>
	</div>
<!--
  <div style="float: right; padding: 15px; font-size: 9pt;">
		<a href="javascript:void(0);" onclick="javascript:showReport('<?=$type?>', <?=$media['id']?>);"><img src="/images/report.png" alt="" style="vertical-align: bottom;" />&nbsp; Report media</a>
	</div>
-->
	<div style="clear: both;"></div>
</div>

<div style="float: left; position: relative;">
	<div class="userpics alsoviewed" id="alsoviewedby"></div>
	<div style="padding: 15px 5px 15px 5px; font-size:8pt;">
<?
/*
		<div style="float: left;">
			<a href="<?=$API->getMediaURL($type, $prevMedia['id'], $prevMedia['title'])?>"><img src="<?=$API->getThumbURL(1, 80, 55, "/$table/" . $prevMedia['id'] . "/" . $prevMedia['hash'] . ".jpg")?>" alt="" /></a>
		</div>
		<div style="float: left; padding: 7px 9px 0 9px; width: 123px; font-size: 8pt; text-align: center;">

			<a href="<?=$API->getMediaURL($type, $prevMedia['id'], $prevMedia['title'])?>"><img src="/images/left.png" alt="" />&nbsp; previous <?=strtolower(typeToWord( $type ))?></a>
			<div style="margin-top: 5px; padding-bottom: 5px; border-top: 1px solid #D8DFEA; height: 1px;"></div>
			<a href="<?=$API->getMediaURL($type, $nextMedia['id'], $nextMedia['title'])?>">next <?=strtolower(typeToWord( $type ))?>&nbsp; <img src="/images/right.png" alt="" /></a>
		</div>
		<div style="float: left;">
			<a href="<?=$API->getMediaURL($type, $nextMedia['id'], $nextMedia['title'])?>"><img src="<?=$API->getThumbURL(1, 80, 55, "/$table/" . $nextMedia['id'] . "/" . $nextMedia['hash'] . ".jpg")?>" alt="" /></a>
		</div>
		<div style="clear: both; height: 5px;"></div>
*/
?>
      <div style="float:left;"><a href="/media-fwd.php?type=<?=$type?>&id=<?=$media['id']?>&prev=1"><img src="/images/left.png" alt="" />&nbsp; previous <?=strtolower(typeToWord( $type ))?></a></div>
			<div style="float:right;"><a href="/media-fwd.php?type=<?=$type?>&id=<?=$media['id']?>&prev=0">next <?=strtolower(typeToWord( $type ))?>&nbsp; <img src="/images/right.png" alt="" /></a></div>
      <div style="clear:both;"></div>
	</div>
	<div style="padding: 1px 0 0 5px;">
    <? if( $site != "m" && $API->adv ) { ?>
		<div class="subhead" style="padding-top:0px; margin: 0px 0 5px;"><div style="float:left;">Sponsors</div><div style="float:right;"><a href="http://www.salthub.com/adv/create.php" style="font-size:8pt; font-weight:300; color:rgb(0, 64, 128);">create an ad</a>&nbsp;</div><div style="clear:both;"></div></div>
    <?php showAd("companion"); } ?>
	</div>
	<div style="margin-top: 3px; padding-left: 5px;">
    <div class="subhead" style="margin-bottom:10px;">Recent and New</div>
		<?php
		include "inc/mod_media.php";
		showMediaModule();
		?>
	</div>
</div>

<script language="javascript" type="text/javascript">
<!--
var mediaOwner = <?=$media['uid']?>;
tagQueue = [<?=$media['id']?>];

<?php
if (is_array($tagJs))
{
	echo "var tags = new Array();\n";
	echo implode("\n", $tagJs);
}
?>

document.getElementById("numcomments").innerHTML = numComments;

function initPage()
{
	e = document.getElementById("txtcomment");
	
	if (e)
		if (e.value == "")
			e.value = defaultComment;

	moreAlsoViewedBy();
	<?php if ($type == "P" && $albumCount > 1) echo "moreCarousel(true);"; ?>
}

var avbPage = 0;
function moreAlsoViewedBy()
{
	getAjax("/alsoviewedby.php?type=<?=$type?>&link=<?=$media['id']?>&p=" + avbPage, "moreAlsoViewedByHandler");
	avbPage++;
}

function moreAlsoViewedByHandler(data)
{
	document.getElementById("alsoviewedby").innerHTML = data;
}

function addShare()
{
	postAjax("/addshare.php", "type=<?=$type?>&id=<?=$media['id']?>", "addShareHandler");
}

function addShareHandler()
{
	e = document.getElementById("numshares");
	e.innerHTML = parseInt(e.innerHTML) + 1;
}

<?php if ($type == "P") { ?>
var carPage = <?=$carouselPage - 1?>;
function moreCarousel(next)
{
	if (next)
		carPage++;
	else
		carPage--;
	
	getAjax("/albumcarousel.php?aid=<?=$media['aid']?>&p=" + carPage, "moreCarouselHandler");
}

function moreCarouselHandler(data)
{
	document.getElementById("albumcarousel").innerHTML = data;
}


<?
$album_id = $media['aid'];
$sql = "select id, if( $id=id, 1, 0 ) as matched from photos where aid='$album_id' order by matched desc, id";
$q2 = mysql_query( $sql );
?>
var max_images = <?=mysql_num_rows( $q2 );?>;
var album_images = new Array( <?
$c = 0;
while( $r2 = mysql_fetch_array($q2) )
{
  if( $c > 0 ) echo ",";
  echo '"' . $r2['id'] . '"';
  $c++;
}
?> );
<? } ?>

showMedia("browse", "new", "<?=$type?>", 0);


//-->
</script>

<?php
//$scripts[] = "/lightbox/js/jquery.lightbox-0.5.js";

include "inc/embedmedia.php";
include "footer.php";
/*

<script type="text/javascript">
$(function() {
    $('#gallery a').lightBox();
});
</script>
*/ ?>