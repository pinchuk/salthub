<?
/*
This script renders the information below a photo or video in media.php and media_popup.php.  This script
can be included in a file, or called via AJAX.
*/

include_once( "inc/inc.php" );
$from_popup = false;


if( empty( $type ) || empty( $id ) )
{
  $from_popup = true;

  $id = $_GET['id'];
  $type = $_GET['type'];
  $media = array();

  switch( $type )
  {
    case "P":
      $media = queryArray( "select id, uid, pdescr as descr, aid from photos where id='$id'" );
      if( $media['descr'] == "" )
        $media['descr'] = quickQuery( "select descr from albums where id='" . $media['aid'] . "'" );
    break;
    case "V": $media = queryArray( "select id, uid, descr from videos where id='$id'" ); break;
  }

  $descr = $media['descr'];
}


$wordType = typeToWord($type);
	?>
	<div class="tags">
		<div style="float: left; width: 475px;"><?php $tagJs = showTags($media, $type); ?></div>
		<div class="action" style="float: right; font-weight: normal; background-image: url(/images/tag_add.png); margin: 0;" onclick="javascript:showTagPopUp('<?=$type?>', <?=$media['id']?>, '<?=$API->getThumbURL(0, 250, 258, "/" . $table . "/" . $media['id'] . "/" . $media['hash'] . ".jpg")?>');">tag a person or item</div>
		<div style="clear: both;"></div>
	</div>

<?
$users = array();
$pages = array();

$q = mysql_query( "select gid from page_media where type='" . $type . "' and id='" . $id . "'" );
while( $r = mysql_fetch_array( $q ) )
{
  if( !in_array( $r['gid'], $pages ) )
    $pages[] = $r['gid'];
}

$q = mysql_query( "select uid, gid from feed where type='" . $type . "' and link='" . $id . "' and uid!='" . $media['uid'] . "'" );
while( $r = mysql_fetch_array( $q ) )
{
  if( $r['gid'] == 0 )
    if( !in_array( $r['uid'], $users ) )
      $users[] = $r['uid'];
  else
    if( !in_array( $r['gid'], $pages ) )
      $pages[] = $r['gid'];
}

$showingAlsoIn = false;

if( sizeof( $users ) > 0 || sizeof( $pages ) > 0 ) 
{
  $showingAlsoIn = true;
?>

	<div class="tags" style="border-top:0px; height:13px;">
		<div style="width: 600px;">
      <b>Also in:</b>
      <?
      $count = 0;

      for( $c = 0; $c < sizeof( $users ); $c++ ) {
        if( $count > 0 ) echo ", ";
        $count++;
        $info = $API->getUserInfo( $users[$c], "name" );
        $url = $API->getProfileURL( $users[$c] );
        echo '<a href="' . $url . '">' . $info['name'] . '</a>';
       }
      for( $c = 0; $c < sizeof( $pages ); $c++ ) {
        if( $count > 0 ) echo ", ";
        $count++;
        $info = $API->getPageInfo( $pages[$c], true );
        echo '<a href="' . $info['url'] . '">' . $info['gname'] . '</a>';
       } ?>
    </div>
	</div>
<?
   }

  if( strlen( $descr ) > 0 ) {
    $height="";
    
    if( $from_popup )
    { 
      $height = "height:80px;";
      if( $showingAlsoIn )
      {
        $height = "height:45px;";
      }
    }
?>
	<div class="tags" style="border-top:0px; <?=$height?> overflow-y:auto; overflow-x:hidden;">
<? 
  if( $from_popup )
  {
  ?>
		<div style="width: 815px; ">
<? } else { ?>
		<div style="width: 500px;">
<? } ?>
      <b>Description:</b>
<?
      $len = 180;
      if( strlen( $descr ) > $len && !$from_popup )
      {
        $text = cutOffText( $descr, $len );
        echo '<span id="cutoff_text">' . $text . '</span>';
        echo '<span style="display:none;" id="original_text">' . $descr . '</span>';
        echo '<div style="width:500px; text-align:center;"><a id="showmore" href="javascript: void(0);" onclick="javascript: alternateDisplay(\'cutoff_text\', \'original_text\', \'showmore\');">show more</a></div>';
      }
      else
        echo $descr;
?>
    </div>
	</div>
<? }

?>