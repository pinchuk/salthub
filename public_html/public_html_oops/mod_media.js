var ddTimer = null;
var showingVideos = true;

function showMedia(head1, head2, type, page)
{
	if (type == "")
	{
		if (showingVideos)
			type = "V";
		else
			type = "P";
	}
	
	if ((type == "V" && !showingVideos) || type == "P" && showingVideos)
		switchMediaShown();
	
	e = document.getElementById("mediamodulelinks");
	
	if (head1 == "ive")
	{
		e.innerHTML = "";
		document.getElementById("mmive").style.fontWeight = "bold";
		document.getElementById("mmbrowse").style.fontWeight = "normal";
	}
	else //browse
	{
		html =  "| <a href=\"javascript:void(0);\" onclick=\"javascript:showMedia('browse', 'new', '" + type + "', 0);\"><span id=\"mmnew\">New</span></a>";
		if (mediaOwner > 0)
			html += " | <a href=\"javascript:void(0);\" onclick=\"javascript:showMedia('browse', 'user', '" + type + "', 0);\"><span id=\"mmuser\">More from user</span></a>";
		html += " | <a href=\"javascript:void(0);\" onclick=\"javascript:showMedia('browse', 'recent', '" + type + "', 0);\"><span id=\"mmrecent\">Recently viewed</span></a>";
		e.innerHTML = html;
		document.getElementById("mmive").style.fontWeight = "normal";
		document.getElementById("mmbrowse").style.fontWeight = "bold";
		document.getElementById("mm" + head2).style.fontWeight = "bold";
	}
	
	getAjax("/getmedia.php?h1=" + head1 + "&h2=" + head2 + "&t=" + type + "&uid=" + mediaOwner + "&p=" + page, "showMediaHandler");
}

function showMediaHandler(data)
{
	document.getElementById("mediamodule").innerHTML = data;
}

function switchMediaShown()
{
	showingVideos = !showingVideos;
	
	e = document.getElementById("media-chosen");
	f = document.getElementById("media-dd");
	
	if (showingVideos)
	{
		e.innerHTML = "Videos";
		f.innerHTML = "Photos";
	}
	else
	{
		f.innerHTML = "Videos";
		e.innerHTML = "Photos";
	}
	
	document.getElementById('media-dd').style.display = "none";
}