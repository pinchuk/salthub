<?php
include "../inc/inc.php";
?>
<form action="/pages/page_create2.php" method="POST" id="page_create_form" autocomplete="off">
<input type="hidden" name="type_selection" id="type_selection" value="1"/>
<input type="hidden" name="l1_subcat" id="l1_subcat"/>

  <div style="width:490px; float:left;">

      <div id="company" class="dentry" style="padding:10px;">
        <div style="font-size:14pt; font-weight:bold; padding-left:10px;">Business or Service</div>
        <div style="font-size:9pt; padding-left:10px; padding-top:5px;">Choose a sector and category</div>

        <div>
          <div style="float:left;">
<!--
          <select name="l1_cat" style="width:150px;" onchange="javascript: refreshSubcats2( selectedValue( this ) );">
<?
$q = sql_query( "select * from categories where cattype='G' and industry='" . PAGE_TYPE_BUSINESS . "' order by catname desc" );
while( $r = mysql_fetch_array( $q ) )
{
?>
          <option value="<? echo $r['cat']; ?>"><? echo $r['catname']; ?></option>
<?
}
?>
          </select>
-->
            <div id="ind"></div>
          </div>


          <div style="float:left;" id="company_subcats2">
            <div id="PandS" style="font-size:8pt;"></div>

<!--
          <select name="l1_subcat_temp" id="l1_subcat_temp" style="width:250px;">
<?
$q = sql_query( "select * from categories where industry='111' order by catname" );
while( $r = mysql_fetch_array( $q ) )
{
?>
          <option value="<? echo $r['cat']; ?>"><? echo $r['catname']; ?></option>
<?
}
?>
          </select>
-->
          </div>
        </div>

        <div style="font-size:9pt; clear:both; margin: 10px; padding-top:10px; ">
          Select the primary sector and category for your business or service. Charges for additional categories and sectors apply.
        </div>

        <div style="margin-top:5px;">
          <div style="float:left; padding:5px; padding-top:10px; position:relative;">
            <input type="text" id="l1_gname" name="l1_gname" value="Page Name" style="width:402px;" onkeyup="javascript:searchKeypress(event,this,'1');" onfocus="javascript:if( this.value=='Page Name' ) this.value='';" onblur="javascript:if (this.value=='') this.value='Search <?=$siteName?> ...'; searchLostFocus(-1,'1');" />
            <div id="suggestionBox1" style="padding:0px; position: absolute; left:10px; top:42px; width:288px; height:205px; z-index:101; display:none;"></div>
          </div>
        </div>

        <div style="margin-top:5px;">
          <div style="float:left; padding:5px;"><input type="text" name="l1_website" value="Website (optional)" style="width:402px;" onclick="if( this.value=='Website (optional)' ) this.value='';"/></div>
        </div>

      </div>

      <div style="clear:both;"></div>

      <div id="common" class="dentry" style="padding:10px; padding-top:5px;">
        <div>
          <div style="float:left;">
          <select name="privacy">
            <option value="0">Accessible to:</option>
            <option value="<? echo PRIVACY_EVERYONE ?>">Everyone</option>
            <option value="<? echo PRIVACY_FRIENDS ?>">Connections</option>
            <!--<option value="<? echo PRIVACY_SELF ?>">Myself</option>-->
          </select>
          </div>

          <div style="float:left; margin-top:10px; font-size:9pt;">
            Privacy
          </div>
        </div>

        <div style="clear:both;"></div>

        <div style="font-size:9pt; margin-left:10px; margin-top:10px;">
          <div style="float:left;"><input type="checkbox" name="agree" id="agree" checked="checked" style="padding:0px; margin:0px; width:20px;"/></div>
          <div style="float:left; padding-top:0px;">I agree to the <a href="/tos.php">Terms of Service</a> and I am authorized to create this page.</div>
        </div>

      <div style="clear:both;"></div>

        <div style="margin-top:15px; margin-left:10px;">
          <input type="button" value="Next" style="color:#326798; font-size:12pt; width:80px; margin-left:10px; margin:0px; height:33px;" class="button" onclick="javascript: if( checkPageCreateForm() ) { params = escape( getFormVals2( document.getElementById('page_create_form') ) ); loadShare('newpage3',1, params ); } "/>
        </div>
      </div>
    </div>

  <div style="clear:both;"></div>
</form>
