/*
 * JavaScript Pretty Date
 * Copyright (c) 2008 John Resig (jquery.com)
 * Licensed under the MIT license.
 */

// Takes an ISO time and returns a string representing how
// long ago the date represents.
function prettyDate(time)
{
	diff = lastTime - time;
	day_diff = Math.floor(diff / 86400);
			
	if ( isNaN(day_diff) || day_diff < 0 || day_diff >= 31 )
		return;
			
	return day_diff == 0 && (
			diff < 60 && "Just now" ||
			diff < 120 && "1 min ago" ||
			diff < 3600 && Math.floor( diff / 60 ) + " mins ago" ||
			diff < 7200 && "1 hr ago" ||
			diff < 86400 && Math.floor( diff / 3600 ) + " hrs ago") ||
		day_diff == 1 && "Yesterday" ||
		day_diff < 7 && day_diff + " days ago" ||
		day_diff < 31 && Math.ceil( day_diff / 7 ) + " wks ago";
}

function doPrettyDate()
{
        var links = document.getElementsByTagName("span");       
        for ( var i = 0; i < links.length; i++ )
		{
                if ( links[i].id.substring(0, 8) == "timeago-" ) {
						x = links[i].id.split("-");
                        var d = prettyDate(parseInt(x[1]));
                        if ( d )
                                links[i].innerHTML = d;
                }
		}
}