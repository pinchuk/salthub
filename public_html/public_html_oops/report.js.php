<?php
header("Content-type: text/javascript");
include "inc/inc.php";
?>

function showReport(area, id)
{
	message = "Please select your reason for flagging this item as inappropriate from the dropdown below.&nbsp; We promise to review each and every submission within 24-48 hours.&nbsp; If you are the copyright owner of this item and believe it has been uploaded without your permission, please follow these directions to submit a copyright infringement notice.&nbsp; (<a href='javascript:void(0);' onclick='window.open(\"/dmca.php?1\", \"DMCA\",\"location=0,status=0,scrollbars=1,width=500,height=600\");'>DMCA Complaint</a>)";
	message += '<br /><br /><select id="report-reason">';
	<?php
	$x = mysql_query("select id,reason from report");
	while ($y = mysql_fetch_array($x, MYSQL_ASSOC))
	{
		?>
		message += '<option value="<?=$y['id']?>"><?=$y['reason']?></option>';
		<?php
	}
	?>
	message += '</select>';
	
	message = '<div style="font-size: 11pt; padding: 10px; text-align: center;">' + message + '<br /><br /><input type="button" onclick="javascript:sendReport(\'' + area + '\', \'' + id + '\');" class="button" value="Report" /> &nbsp; <input type="button" class="button" onclick="javascript:closePopUp();" value="Cancel" /></div>';
	showPopUp("Report this item", message, 550);
	//showYesNo("Report this item", message, new Array(new Array("Report", "sendReport('" + area + "', '" + id + "')"), new Array("Cancel", "")), 550, true);
}

function sendReport(area, id)
{
	closePopUp();
	data = "type=" + area + "&id=" + id + "&reason=" + document.getElementById("report-reason").value;
	postAjax("/sendreport.php", data, "sendReportHandler");
}

function sendReportHandler(data)
{
	if (data == "OK")
		showPopUp2("Report sent", "Thank you for reporting this content.", 0);
	else
  {
    if( admin )
      alert( data );
		showPopUp2("", "There was an error processing your request.  Please try again later.", 0);
  }
}