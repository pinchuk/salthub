<?php
/*
7/28/2012 - Added support for search window on the work profile

*/

include "inc/inc.php";

$divname = $_GET['divname'];
$query = $_GET['q'];
$compact = $_GET['c'];

$companies_only = ($_GET['co'] == '1'); //Companies only
$vessels_only = ($_GET['co'] == '2');

if( is_numeric( $query ) )
  $imoquery = " OR imo='$query' OR mmsi='$query'";
else
  $imoquery = "";

                                                                                      // or contactfor like '%$query%'
$sql = "select SEARCH_RESULTS.gid, pages.pid, pages.gname, pages.cat, pages.type, pages.subcat, pages.gname='$query' as exact, pages.gname like '%$query%' as secondary from
  (
    SELECT gid FROM pages where gname like '%$query%'
  UNION
    select gid from boats where exnames like '%$query%' $imoquery
  ) SEARCH_RESULTS, pages where pages.gid=SEARCH_RESULTS.gid and pages.privacy=" . PRIVACY_EVERYONE . " and pages.cat!=" . CAT_LICDEG;


if( $companies_only )
  $sql .= " and pages.type='" . PAGE_TYPE_BUSINESS . "'";
else if( $vessels_only )
  $sql .= " and pages.type='" . PAGE_TYPE_VESSEL . "'";

$sql .= " order by secondary desc, exact desc";

if( $compact )
  $sql .= " limit 7";
else
  $sql .= " limit 5";

$q = mysql_query( $sql );

//file_put_contents('/tmp/qqqq', $sql);

$total_results = mysql_num_rows( $q );
$results = 10 - $total_results;

$q2 = mysql_query( "select uid, pic, name, occupation, name='$query' as exact from users where (name like '%$query%' OR contactfor like '%$query%') and active=1 order by exact desc limit $results" );

$total_results += mysql_num_rows( $q2 );

if( $total_results == 0 ) { echo "0"; exit; }
?>

<div class="searchPreview">
<?
if( mysql_num_rows( $q ) )
{
  if( !$compact )
  {
?>
    <div class="searchHeading">
      <div style="float:left; height:20px;">Suggested Pages</div>
      <div class="viewall2"><a href="/search_s.php?q=<? echo $query ?>">view more</a></div>
    </div>
  <?
  }
  else
  {
?>
    <div class="searchHeading">
      <div style="float:left; height:20px;">Similar Pages</div>
      <div class="viewall2"><a href="/search_s.php?q=<? echo $query ?>">view more</a></div>
    </div>
<?
  }


  while( $r = mysql_fetch_array( $q ) )
  {
    switch( substr( $divname, 0, 1 ) )
    {
      case "W": //Support for Work Profile
        $wid = substr( $divname, 1 );

        $url = 'javascript:void(0);" onclick="javascript: changeEmployerSelection( \'' . $wid . '\', ' . $r['gid'] . ', \'' . addslashes( $r['gname'] ) . '\' ); searchPopupDisabled = true;';
        $onclick_url = 'changeEmployerSelection( \'' . $wid . '\', ' . $r['gid'] . ', \'' . addslashes( $r['gname'] ) . '\' ); searchPopupDisabled = true;';
      break;

      case "P": //Support for People Where You Work Module
        $url = 'javascript:void(0);" onclick="javascript: searchPWYW( ' . $r['gid'] . ' ); document.getElementById( \'suggestionBoxPWYW\' ).style.display=\'none\'; searchPopupDisabled = true;';
        $onclick_url = 'searchPWYW( ' . $r['gid'] . ' ); document.getElementById( \'suggestionBoxPWYW\' ).style.display=\'none\'; searchPopupDisabled = true; document.getElementById(\'search\').value=\'' . $r['gname'] . '\';';
      break;

      default:
        $url = $API->getPageUrl( $r['gid'], $r['gname'] );
        $onclick_url = 'location=\'' . $url . '\';';
      break;
    }

    if( strtolower( $r['gname'] ) == strtolower( $query ) && $r['type'] == PAGE_TYPE_BUSINESS ) { echo "<!--DUPE-->"; }
  ?>
    <div class="searchItem" onclick="javascript: <?=$onclick_url?>">
      <div class="image">
        <a href="<?=$url?>"><img width="48" height="48" src="<?=$API->getThumbURL(1, 48, 48, $API->getPageImage($r['gid'] ) )?>" /></a>
      </div>
      <div class="text">
        <?
        echo '<a href="' . $url . '">' . cutOffText($r['gname'], 40 ) . '</a><br />';

        if( $r['type'] == PAGE_TYPE_BUSINESS && $r['subcat'] != CAT_CLUB_ASSOC )
        {
          echo '' . quickQuery( "select catname from categories where cat='" . $r['type'] . "'" ) . "<br />";
        }
        elseif( $r['type'] == PAGE_TYPE_VESSEL )
        {
          $exnames = quickQuery( "select exnames from boats where gid='" . $r['gid'] . "'" );

          if( strlen( $exnames ) > 0 )
          {
            echo 'ex. ' . cutOffText( $exnames, 40 ) . "<br />";
          }
          else
            echo '' . quickQuery( "select catname from categories where cat='" . $r['type'] . "'" ) . "<br />";

          $len = quickQuery( "select length from boats where gid='". $r['gid'] . "'" );
          if( $len > 0 )
            echo '' . $len . ' ft / ' . round( $len * 0.3048, 2 ) . ' m';
        }
        else
        {
          echo '' . quickQuery( "select catname from categories where cat='" . $r['cat'] . "'" ) . "<br />";
          if( $r['subcat'] > 0 )
            echo '' . quickQuery( "select catname from categories where cat='" . $r['subcat'] . "'" ) . "";
        }
        ?>
      </div>

    </div>
  <?
  }
}

if( mysql_num_rows( $q2 ) > 0 && intval( $compact ) != 1 )
{
  ?>
    <div class="searchHeading">
      Suggested Connections
      <div class="viewall2"><a href="/search_m.php?q=<? echo $query ?>&t=U">view more</a></div>
    </div>

  <?
  while( $r = mysql_fetch_array( $q2 ) )
  {
    $url = $API->getProfileURL( $r['uid'] );
  ?>
    <div class="searchItem" onclick="javascript: window.location.href='<?=$url?>';">
      <div class="image"><a href="<?=$url?>"><img height="48" width="48" src="<?=$API->getThumbURL(1, 48, 48, $API->getProfilePic($r['uid'] ) )?>" /></a></div>
      <div class="text">
        <?
        echo '<a href="' . $url . '">' . cutOffText($r['name'], 40 ) . '</a><br />';
        if( $r['occupation'] != "" ) echo $r['occupation']; else echo "SaltHub Member";
        ?>
      </div>

    </div>
  <?
  }
}
?>

</div>