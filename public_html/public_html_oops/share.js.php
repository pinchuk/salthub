<?php
header("Content-type: text/javascript");
include "inc/inc.php";
?>

var defaultEmail = "Enter e-mails for people not in your contact list";
var defaultShareSearch = "Type a name here";

var usersSelected = new Array();
var actions = new Array();
var allContacts = new Array();

<?php
$highlight = array();
$page = $_GET['p'];

if (isset($_GET['json_params']))
	$params = json_decode($_GET['json_params'], true);
else
	$params[0] = $_GET['params'];

if (in_array($page, array("A", "A_", "P", "V")))
{
	//A_ - sharing an album but a photo id has been specified
	if ($page == "A_")
		$id = quickQuery("select aid from photos where id=" . intval($params[0]));
	else
		$id = intval($params[0]);

	$word = typeToWord($page);
	if ($page == "V")
	{
		$actions = array(array("Not Shared", "Shared"), array("Not in Video", "In Video"));
		$showSelf = true;
	}
	else
		$actions = array(array("Not Shared", "Shared"));

	$isOwner = quickQuery("select uid from {$word}s where id=$id") == $API->uid;
	$introText = "Select connections you want to share " . ($isOwner ? "your" : "this") . " $word with";
	$btnCaption = "Share";
	$msgSuccess = null;
	$msgFail = "There was an error sharing your $word.";
	
	$postJs = "
		if (skipped) //still need to set privacy settings
		{
			priv = getSelectedPrivacy();

			if (priv == " . PRIVACY_SELECTED . ")
				priv = " . PRIVACY_EVERYONE . ";
			
			data = 'type=" . substr($page, 0, 1) . "&id=$id&priv=' + priv;
			//callback the same function - do not execute this same code again
			postAjax('/updateprivacy.php', data, 'doSharePostJs(false); void');
			return;
		}
		
		if (script == 'signup/loggedin' || script == 'mediahome')
			window.location.href = '" . $API->getMediaURL(substr($page, 0, 1), $id) . "';
    else if( currentPage > 0 )
    {
      window.location.href = '/page/' + currentPage + '-a/logbook';
    }
		else
		{
  		resetUpload(true, true);
      if (typeof showNewLogEntries == 'function')
			  showNewLogEntries();
		}
    ";

	$showBtnSkip = true;
	$allowClose = false;
	$showPrivacy = $isOwner;

	$info = $API->getMediaInfo(substr($page, 0, 1), $id, "title,descr" . ($page != "V" ? ",ptitle,pdescr" : ""));
	$defaultSubj = $info['ptitle'] ? $info['ptitle'] : $info['title'];
	$defaultBody = $info['pdescr'] ? $info['pdescr'] : $info['descr'];

	$x = mysql_query("select cid, uid  from video_tags where vid='$id'");
	while ($y = mysql_fetch_array($x, MYSQL_ASSOC))
  {
    if( $y['cid'] == "0" && $API->uid != $y['uid'] ) continue;

    if( $y['uid'] != $API->uid )
    {
      $eid = quickQuery( "select eid from contacts where cid='" . $y['cid'] . "'" );
      if( isset( $eid ) )
      {
        $temp_cid = quickQuery( "select cid from contacts where eid='" . $eid . "' and uid='" . $API->uid . "'" );
        if( isset( $temp_cid ) ) $y['cid'] = $temp_cid;
      }
    }

		$highlight[] = $y['cid'];
    $usersSelected[1][] = $y['cid'];
  }
}
elseif ($page == "suggest")
{
	$actions = array(array("", "Suggested"));
	$introText = "Select connections of yours who know this user";
	$btnCaption = "Suggest";
	$msgSuccess = "Your connection suggestions have been made.";
	$msgFail = "There was an error suggesting connections.";
	$showBtnSkip = false;
	$allowClose = true;
	$siteUsersOnly = true;
	$defaultSubj = "Connection suggestion";
	$defaultBody = "I thought you might want to connect.";

	//highlight people already friends with this user (mutual friends)
	$capHighlight = "Connected";
	$uid = intval($params[0]);
	$x = mysql_query("select d.cid from contacts c inner join contacts d on c.eid=d.eid and c.site=d.site where c.uid=$uid and d.uid={$API->uid} and c.site=" . SITE_OURS);
	while ($y = mysql_fetch_array($x, MYSQL_ASSOC))
		$highlight[] = $y['cid'];
}
elseif ($page == "suggestpage")
{
	$actions = array(array("Add to Page", "Remove"));
	$introText = "Select connections you want to suggest this page";
	$btnCaption = "Add";
	$msgSuccess = "Your connections have been notified of your page suggestion.";
	$msgFail = "There was an error adding your connections to this page.";
	$showBtnSkip = true;
	$allowClose = true;
	$siteUsersOnly = false;

	$gid = intval($params[0]);

  $gname = quickQuery( "select gname from pages where gid='" . $gid . "'" );
	$defaultSubj = "$siteName Page - " . $gname;
	$defaultBody = "I thought you might want to join this page &quot;" . $gname . "&quot;. http://www.$siteName.com/" . $API->getPageUrl( $gid ) . "?join";

	//highlight people already in this page
	$capHighlight = "Already joined";
  $id = $gid;
	$x = mysql_query("select cid from page_members gm inner join contacts c on gm.uid=c.eid and c.uid={$API->uid} where gid=$gid");
	while ($y = mysql_fetch_array($x, MYSQL_ASSOC))
		$highlight[] = $y['cid'];
}
elseif ($page == "newpage")
{
	$actions = array(array("Add to Page", "Remove"));
	$introText = "Add connections to your page below";
	$btnCaption = "Create Page";
//	$msgFail = "There was an error creating your page.";
	$msgFail = "The name you have entered does not conform with the terms of use, please try again.";  //Assume the error is related to a bad name.
	$showBtnSkip = false;
	$allowClose = true;
	$capTo = "Added";
	$preText = "Enter page name";
	$hideSubj = true;
	$hideBody = true;
	$showPrivacy = true;
}
elseif ($page == "newpage2" || $page == "newpage3")
{
	$actions = array(array("Add to Page", "Remove"));
	$introText = "Please select some contacts to add to your new page";
	$btnCaption = "Create Page";
//	$msgFail = "There was an error creating your page.";
//	$msgFail = "The name you have entered does not conform with the terms of use, please try again.";  //Assume the error is related to a bad name.
	$showBtnSkip = false;
	$allowClose = true;
	$capTo = "Added";
//	$preText = "Enter page name";
	$hideSubj = true;
	$hideBody = true;
	$showPrivacy = true;
}
elseif ($page == "network")
{
	$actions = array(array("Add to Club", "Remove"));
	$introText = "Select connections you want to invite";
	$btnCaption = "Invite";
	$msgSuccess = "Your connections have been invited to this club or association.";
	$msgFail = "There was an error inviting your connections.";
	$showBtnSkip = true;
	$allowClose = true;
	$siteUsersOnly = false;
	$defaultSubj = "Clubs &amp; Associations - " . $params['name'];
	$defaultBody = "I invited you to the club &quot;" . $params['name'] . "&quot;.  Join the club to learn more and receive updates in your welcome feed.";

	//highlight people already in this page
	$capHighlight = "Already joined";
	$nid = intval($params['nid']);
	$x = mysql_query("select cid from network_users gm inner join contacts c on gm.uid=c.eid and c.uid={$API->uid} where nid=$nid");
	while ($y = mysql_fetch_array($x, MYSQL_ASSOC))
		$highlight[] = $y['cid'];
}
elseif ($page == "page")
{
	$actions = array(array("Add to Page", "Remove"));
	$introText = "Select connections you want to add to this page";
	$btnCaption = "Add";
	$msgSuccess = "Your connections have been added to this page.";
	$msgFail = "There was an error adding your connections to this page.";
	$showBtnSkip = true;
	$allowClose = true;
	$siteUsersOnly = false;
	$defaultSubj = "$siteName Page - " . $params['gname'];
	$defaultBody = "I added you to the page &quot;" . $params['gname'] . "&quot;. Join the page to learn more and receive updates in your welcome feed.";

	//highlight people already in this page
	$capHighlight = "Already joined";
	$gid = intval($params['gid']);
  $id = $gid;
	$x = mysql_query("select cid from page_members gm inner join contacts c on gm.uid=c.eid and c.uid={$API->uid} where gid=$gid");
	while ($y = mysql_fetch_array($x, MYSQL_ASSOC))
		$highlight[] = $y['cid'];
}
else //compose
{
	$actions = array(array("Not Shared", "Shared"));
	$introText = "Select connections you want to share your message with";
	$btnCaption = "Send";
	$msgSuccess = "Your message has been sent successfully.";
	$msgFail = "There was an error sending your message.";
	$page = "compose";
	$showBtnSkip = false;
	$allowClose = true;

  if( isset( $params['subj'] ) )
    $defaultSubj = urldecode($params['subj']);
  if( isset( $params['body'] ) )
    $defaultBody = urldecode($params['body']);
}

if (!$siteUsersOnly)
	$siteUsersOnly = $API->verify != 1;

$i = 0;

if ($showSelf)
	$prefix = "(select " . $API->uid . " as eid," . SITE_OURS . " as site,0 as cid,'" . addslashes($API->name ? $API->name : $API->username) . "' as name) union ";
else
	$prefix = "";

$x = mysql_query("$prefix (select eid,site,cid,name from contacts where uid=" . $API->uid . ($siteUsersOnly ? " and site=" . SITE_OURS : "") . " order by name)");

while ($contact = mysql_fetch_array($x, MYSQL_ASSOC))
{
	$contact['pic'] = $API->getContactPic($contact);
	echo "allContacts[" . $i++ . "] = [" . $contact['cid'] . ", '" . addslashes($contact['name']) . "', '" . $contact['pic'] . "', '" . $contact['site'] . "'];\n";
}

$i = 0;

foreach ($actions as $action)
{
	echo "actions[$i] = ['" . implode("', '", $action) . "'];\n";
//  if( isset( $usersSelected[$i] ) )
//  	echo "usersSelected[$i] =  '," . implode(",", $usersSelected[$i] ) . ",';\n";
//  else
  	echo "usersSelected[$i] =  \"\";\n";

	$i++;
}

  $defaultBody = str_replace( chr(10), "' + String.fromCharCode(10) + '", $defaultBody ); //This is to fix a 'feature' in IE where it assumes that the string is unterminated if there are a bunch of line breaks in the string.
  $defaultBody = str_replace( chr(13), "' + String.fromCharCode(10) + '", $defaultBody ); //This is to fix a 'feature' in IE where it assumes that the string is unterminated if there are a bunch of line breaks in the string.

?>

var shareHTML;
var highlightUsers = [<?=implode(",", $highlight)?>];

function refreshShareHTML()
{
  shareHTML  = '<div style="padding: 5px;">';
  <?php if ($preText) { ?>
  shareHTML += '	<div class="smtitle" style="float: left;"><?=addslashes($preText)?>:&nbsp; &nbsp; <input type="text" id="share-text" style="width: 285px;" alt="" /></div>';
  <?php } ?>
  shareHTML += '	<div class="smtitle" style="float: right; font-weight: normal;"><a href="/contacts.php"><img src="/images/vcard.png" alt="" />Contact Manager</a></div>';
  <?php if ($preText) { ?>
  shareHTML += '	<div style="clear: both; height: 9px; border-bottom: 1px solid #7f7f7f; margin-bottom: 7px;"></div>';
  <?php } ?>
  shareHTML += '	<div class="smtitle" style="float: left; padding-bottom: 7px;"><?=addslashes($introText)?></div>';
  shareHTML += '	<div style="clear: both;">';
  shareHTML += '		<div style="border: 1px solid #c0c0c0; float: left; font-size: 9pt; font-weight: bold; padding-right: 4px;">';
  shareHTML += '			<input class="txtsearch" id="share-search" onkeyup="javascript:shareSearch(this.value);" onfocus="javascript:if (this.value == defaultShareSearch) this.value = \'\';" onblur="javascript:if (this.value == \'\') this.value = defaultShareSearch;" style="width: 150px; border: 0;" value="' + defaultShareSearch + '" />&nbsp; <a href="javascript:void(0);" onclick="javascript:clearShareSearch();" style="color: #808080;">X</a>';
  shareHTML += '		</div>';

  //select all
  shareHTML += '		<div class="shareselall">';
  shareHTML += '			<a href="javascript:void(0);" onclick="javascript:shareSelAll();" /><input type="checkbox" id="shareselall" onchange="javascript:shareSelAll();" /> Select all</a>';
  shareHTML += '		</div>';

  //show dd
  divHover = '<div onmouseover="javascript:this.style.background = \'#d8dfea\';" onmouseout="javascript:this.style.background=\'\';">';

  shareHTML += '		<div class="showddcontainer" style="margin-right: 14px;">';
  shareHTML += '			<div style="cursor: default;" onmouseover="javascript:if (ddTimer) clearTimeout(ddTimer); document.getElementById(\'show-dd\').style.display = \'inline\';" onmouseout="javascript:ddTimer = setTimeout(\'document.getElementById(\\\'show-dd\\\').style.display=\\\'none\\\';\', 200);">';
  shareHTML += '				<div style="width: 75px; padding-left: 3px; float: left;" id="showsel">Show</div> &nbsp;<img src="/images/down.png" alt="" />';
  shareHTML += '				<div class="dropdown showdd" id="show-dd">';
  shareHTML += '					' + divHover + '<a href="javascript:void(0);" onclick="javascript:showShareSite(-1, \'All\'); shareSearch(\'\');"><img src="/images/page.png" alt="" />All Contacts</a></div>';
  shareHTML += '					' + divHover + '<a href="javascript:void(0);" onclick="javascript:showShareSite(<?=SITE_OURS?>, \'<?=$siteName?>\');"><img src="/images/splash.png" alt="" /><?=$siteName?></a></div>';
  shareHTML += '					' + divHover + '<a href="javascript:void(0);" onclick="javascript:showShareSite(<?=SITE_FACEBOOK?>, \'Facebook\');"><img src="/images/facebook.png" alt="" />Facebook</a></div>';
  shareHTML += '					' + divHover + '<a href="javascript:void(0);" onclick="javascript:showShareSite(<?=SITE_TWITTER?>, \'Twitter\');"><img src="/images/twitter.png" alt="" />Twitter</a></div>';
  shareHTML += '					' + divHover + '<a href="javascript:void(0);" onclick="javascript:showShareSite(<?=SITE_HOTMAIL?>, \'Hotmail\');"><img src="/images/hotmail.png" alt="" />Hotmail</a></div>';
  shareHTML += '					' + divHover + '<a href="javascript:void(0);" onclick="javascript:showShareSite(<?=SITE_HOTMAIL?>, \'MSN\');"><img src="/images/msn.png" alt="" />MSN</a></div>';
  shareHTML += '					' + divHover + '<a href="javascript:void(0);" onclick="javascript:showShareSite(<?=SITE_YAHOO?>, \'Yahoo\');"><img src="/images/yahoo.png" alt="" />Yahoo</a></div>';
  shareHTML += '					' + divHover + '<a href="javascript:void(0);" onclick="javascript:showShareSite(<?=SITE_GMAIL?>, \'GMail\');"><img src="/images/gmail.png" alt="" />GMail</a></div>';
  shareHTML += '				</div>';
  shareHTML += '			</div>';
  shareHTML += '		</div>'

  <?php if ($showPrivacy) { ?>
  //who can view this dd
  i = 0;
  divHover = '<div onclick="javascript:document.getElementById(\'share-priv-{i}\').checked = true; document.getElementById(\'priv-dd\').style.display = \'none\';" onmouseover="javascript:this.style.background = \'#d8dfea\';" onmouseout="javascript:this.style.background=\'\';">';

  shareHTML += '		<div class="showddcontainer">';
  shareHTML += '			<div style="cursor: default;" onmouseover="javascript:if (typeof ddTimerP != \'undefined\') if (ddTimerP) clearTimeout(ddTimerP); document.getElementById(\'priv-dd\').style.display = \'inline\';" onmouseout="javascript:ddTimerP = setTimeout(\'document.getElementById(\\\'priv-dd\\\').style.display=\\\'none\\\';\', 200);">';
  shareHTML += '				<div style="width: 107px; padding-left: 3px; float: left;">Who can view this</div> &nbsp;<img src="/images/down.png" alt="" />';
  shareHTML += '				<div class="dropdown showdd" style="width: 135px;" id="priv-dd">';
  shareHTML += '					' + divHover.replace('{i}', i) + '<input type="radio" checked name="privacy" value="<?=PRIVACY_EVERYONE?>" id="share-priv-' + i++ + '" />The world</div>';
  <?php if ($page == "page") { ?>
  shareHTML += '					' + divHover.replace('{i}', i) + '<input type="radio" name="privacy" value="<?=PRIVACY_FRIENDS?>" id="share-priv-' + i++ + '" />Only my friends</div>';
  shareHTML += '					' + divHover.replace('{i}', i) + '<input type="radio" name="privacy" value="<?=PRIVACY_SELECTED?>" id="share-priv-' + i++ + '" />Friends selected</div>';
  shareHTML += '					' + divHover.replace('{i}', i) + '<input type="radio" name="privacy" value="<?=PRIVACY_SELF?>" id="share-priv-' + i++ + '" />Only me</div>';
  <?php } else { ?>
  shareHTML += '					' + divHover.replace('{i}', i) + '<input type="radio" name="privacy" value="<?=PRIVACY_SELECTED?>" id="share-priv-' + i++ + '" />Only members</div>';
  <?php } ?>
  shareHTML += '				</div>';
  shareHTML += '			</div>';
  shareHTML += '		</div>';
  <?php } ?>

  shareHTML += '		<div style="clear: both;"></div>';
  shareHTML += '	</div>';
  shareHTML += '	<div style="margin-top: 5px; border: 1px solid #7494B2; padding: 3px 5px 3px 5px;">';
  shareHTML += '		<div id="share-contactscontainer" style="height: 174px; overflow: hidden; ' + (allContacts.length > 12 ? ' overflow-y: scroll; width: 554px;' : 'width: 536px;') + '">';
  if (allContacts.length == 0)
  shareHTML += '			<div style="height: 174px; font-size: 12pt; font-weight: bold; text-align: center; padding-top: 59px;">You have no <?=$siteUsersOnly ? "friends on $siteName" : "contacts"?>.<br/><br/>Add some using the <a href="/contacts.php">contact manager</a>.</div>';
  else
  for (i = 0; i < allContacts.length; i++)
  {
  	highlightUser = false;

  	for (var j in highlightUsers)
  	{
  		if (highlightUsers[j] == allContacts[i][0])
  		{
  			highlightUser = true;
  			break;
  		}
  	}
  shareHTML += '			<div style="float: left; width: 134px; padding-top: 2px; height: 56px;" id="share-user-' + i + '">';
  shareHTML += '				<div style="' + (highlightUser ? 'background: #fff9d7; ' : '') + 'width: 124px; padding: 2px 2px 0 2px; border: 1px solid #7F9DB9;' + (!highlightUser && actions.length == 1 ? " cursor: pointer;" : "") + '" ' + (!highlightUser && actions.length==1 ? "onclick='javascript:toggleAction(0, " + allContacts[i][0] + ");'" : "") + ' id="cid-' + allContacts[i][0] +'">';
  shareHTML += '					<img src="' + allContacts[i][2] + '" alt="" style="width: 48px; height: 48px;" />';
  shareHTML += '					<div style="float: right; width: 69px; overflow: hidden; font-size: 8pt; font-family: tahoma; height: 50px;">';
  shareHTML += '						<div style="height: ' + (actions.length == 1 ? 36 : 24) + 'px; line-height: 9pt; word-wrap: break-word; overflow: hidden;">' + allContacts[i][1] + '</div>';
  for (j = 0; j < actions.length; j++)
  {
  shareHTML += '						<div style="height: 12px;">';
  if (( (highlightUser && actions.length < 2) && j == actions.length - 1) )
  shareHTML += '							<?=$capHighlight?>';
  else if ( (!highlightUser || actions.length == 2) && !(allContacts[i][0] == 0 && j == 0))
  {
    shareHTML += '							<a href="javascript:void(0);"' + (actions.length == 1 ? '' : ' onclick="javascript:toggleAction(' + j + ', ' + allContacts[i][0] + ');"') + '><span id="cid-' + allContacts[i][0] + '-' + j + '">';
    if( actions.length == 2 && j == 1 && highlightUser )
    {
      shareHTML += '</span></a>';
    }
    else
    {
      if( allContacts[i][1].indexOf('@') != -1 && j == 1 )
      {
        shareHTML += '</a><a href="javascript:void(0);" title="Add a name in your contact manager to tag">' + actions[j][0] + '</span></a>';
      }
      else
      {
        shareHTML += actions[j][0] + '</span></a>';
      }
    }
  }
  shareHTML += '						</div>';
  }
  shareHTML += '					</div>';
  shareHTML += '					<div style="clear: both;"></div>';
  shareHTML += '				</div>';
  shareHTML += '			</div>';
  }
  shareHTML += '		</div>';
  shareHTML += '		<div style="clear: both; padding-left: 10px;" class="shareinput">';
  <?php if (!$siteUsersOnly) { ?>
  shareHTML += '			<div class="embedlabel"><?=$capTo ? $capTo : "To"?>:</div>';
  shareHTML += '				<div class="embedinput"><textarea id="share-to" onfocus="this.blur();" onkeypress="return false;"></textarea></div>';
  shareHTML += '			<div class="embedlabel">&nbsp;</div>';
  shareHTML += '				<div class="embedinput"><textarea id="share-emails" onblur="javascript:if (this.value == \'\') this.value = defaultEmail;" onfocus="javascript:if (this.value == defaultEmail) this.value = \'\';" style="height: 15px;">' + defaultEmail + '</textarea></div>';
  <?php } elseif ($API->verify == 0) { ?>
  shareHTML += '			<div class="embedlabel" style="width: auto; text-align: center; clear: both; float: none;"><a href="/signup/verify.php">Verify your e-mail address</a> to share with your friends via e-mail.</div>';
  <?php } ?>
  <?php if (!$hideSubj) { ?>
  shareHTML += '			<div class="embedlabel">Subject:</div>';
  shareHTML += '				<div class="embedinput"><textarea id="share-subj" style="height: 15px;"><?= str_replace("'", "\'", $defaultSubj); ?></textarea></div>';
  <?php } ?>
  <?php if (!$hideBody) { ?>
  shareHTML += '			<div class="embedlabel">Message:</div>';
  shareHTML += '				<div style="padding-top:10px; width:300px; clear:right; float:left;"><textarea id="share-body" onclick="resizeTextArea(this);" onkeyup="resizeTextArea(this);" rows="2" style="width:375px;">' + '<?=str_replace("'", "\'", $defaultBody);?></textarea></div>';
  <?php } ?>
  shareHTML += '			<div style="clear: both; padding: 10px; text-align: center; height: 15px; font-size: 9pt;" id="share-btn"><input type="button" class="button" onclick="javascript:doShare();" value="<?=$btnCaption?>" />';
  <?php if ($showBtnSkip) echo 'shareHTML += \'				 &nbsp; &nbsp; <input type="button" class="button" onclick="javascript:doSharePostJs(true);" value="Skip" />\';'; ?>
  shareHTML += '			</div>';
  shareHTML += '		</div>';
  shareHTML += '	</div>';
  shareHTML += '</div>';
}

function resizeTextArea(t) {
  a = t.value.split('\n');
  b=1;
  for (x=0;x < a.length; x++) {
    if (a[x].length >= t.cols) b+= Math.floor(a[x].length/t.cols);
  }
  b+= a.length;
  if (b > t.rows)
    t.rows = b;
}

function shareSelAll()
{
	e = document.getElementById("shareselall");
	chk = !e.checked;
	e.checked = chk;
	
	usersSelected[0] = "";
	
	//select all
	for (i = 0; i < allContacts.length; i++)
		toggleAction(0, allContacts[i][0]);
	
	//unselect all
	if (!chk)
		for (i = 0; i < allContacts.length; i++)
			toggleAction(0, allContacts[i][0]);
}

function clearShareSearch()
{
	shareSearch("");
	
	e = document.getElementById("share-search");
	e.value = "";
	e.focus();
}

function showShareSite(q, cap)
{
	for (i = 0; i < allContacts.length; i++)
		document.getElementById("share-user-" + i).style.display = allContacts[i][3] == q ? "" : "none";

	document.getElementById("show-dd").style.display = "none";
	
	document.getElementById("showsel").innerHTML = cap;
}

function shareSearch(q)
{
	q = q.toLowerCase();
	
	for (i = 0; i < allContacts.length; i++)
		document.getElementById("share-user-" + i).style.display = allContacts[i][1].toLowerCase().indexOf(q) == -1 ? "none" : "";
}

function doShare()
{
	data = "";
	hasSelected = false;
	
	for (var i in actions)
	{
		data += "&action" + i + "=" + usersSelected[i];
		if (usersSelected[i] != "")
			hasSelected = true;
	}
	
	e = document.getElementById("share-emails");
	
	if (e)
	{
		emails = e.value;
		if (emails != defaultEmail)
			data += "&emails=" + escape(emails.split(/[\s,\n]+/).join(","));
	}
	else
		emails = false;

	hasSelected = hasSelected || emails != defaultEmail;

	if (!hasSelected)
		return alert("You must select at least one user.");

	e = document.getElementById("share-subj");
	if (e)
		data += "&subj=" + escape(e.value);

	e = document.getElementById("share-body");
	if (e)
		data += "&body=" + escape(e.value);

	data += "&priv=" + getSelectedPrivacy();

//	alert(data);

	document.getElementById("share-btn").innerHTML = "Wait &#0133;";

	e = document.getElementById("share-contactscontainer");
	e.style.overflow = "hidden";
	e.innerHTML = '<div style="text-align: center; padding-top: 77px;"><img src="/images/barwait.gif" style=" alt="" /></div>';

	e = document.getElementById("share-text");
	if (e)
		shareText = escape(e.value);
	else
		shareText = '';

<? if( $page == "newpage2" ) { ?>
  sharedata = document.getElementById( 'sharedata' );
  sharedata.value = data;
  form = document.getElementById( 'page_create_form' );
  form.submit();
<? } else if( $page == "newpage3" ) { ?>
	postAjax("/pages/page_create2.php", "p=<?=$page?>&<?= str_replace( "&amp;", "%26", str_replace( "&amp;", "&", urldecode( $_GET['params'] ) ) ); ?>&sharedata=" + encodeURIComponent(data) + "&popup=1&id=<? echo $id ?>", "pageForward");
<? } else { ?>
	postAjax("/sharehandler.php", "p=<?=$page?>&json_params=" + escape("<?=addslashes(json_encode($params))?>") +  data + "&shareText=" + shareText + "&id=<? echo $id ?>&gid=" + currentPage, "doShareHandler");
<? } ?>
}

function getSelectedPrivacy()
{
	for (i = 0; i < 4; i++)
	{
		e = document.getElementById("share-priv-" + i);
		if (!e) break;

		if (e.checked)
			return e.value;
	}
}

function pageForward(data)
{
  window.location.href = data;
}

function doShareHandler(data)
{
	x = data.split("|");

	msgSuccess = "<?=$msgSuccess?>";
/*
  if( x[0] != "OK" && x[0] != "Dupe" && admin == 1 )
  {
 		showPopUp2("Error", data);
//      showPopUp2("", "<?=$msgFail?>");
  }
  else */if( x[0] == "Dupe" )
  {
    showPopUp2("", 'A page with this name already exists.  Please choose a different name, or <a href="http://<? echo $_SERVER['HTTP_HOST'] ?>/page/' + x[1] + '-Fwd">visit the existing page</a>.' );
  }
  else //Upload worked
	{
		if (msgSuccess == "")
		{
			if (x.length == 1)
				closePopUp();
			else
				window.location.href = x[1];
		}
		else
			showPopUp2("", msgSuccess);

  	if (script == "media") //we are on the photo page
    {
  		location.reload(true);
    }

		doSharePostJs(false);
	}

}

function doSharePostJs(skipped)
{
	closePopUp();
	<?=$postJs?>;
}

function doHighlightUser(cid, on)
{
	document.getElementById("cid-" + cid).style.background = on ? "#fff9d7" : "#fff";
}

function toggleAction(action, cid)
{

	if (usersSelected[action].indexOf("," + cid + ",") == -1) //selecting
	{
		document.getElementById("cid-" + cid + "-" + action).innerHTML = actions[action][1];

		usersSelected[action] += "," + cid + ",";
	}
	else
	{
		document.getElementById("cid-" + cid + "-" + action).innerHTML = actions[action][0];

		usersSelected[action] = usersSelected[action].replace("," + cid + ",", ",");
	}

  highlight = false;
  for( c = 0; c < actions.length; c ++ )
  {
    if( usersSelected[c].indexOf("," + cid + ",") > -1 )
    {
      highlight = true;
      break;
    }
  }

	doHighlightUser(cid, highlight);

	ids = usersSelected[0].split(",");
	names = "";

	for (var i in ids)
		if (ids[i] != "")
		{
			for (var j in allContacts)
				if (allContacts[j][0] == ids[i])
				{
					names += ", " + allContacts[j][1];
					break;
				}
		}

	e = document.getElementById("share-to");
	if (e) e.value = names.substring(2);
}

function showShare()
{
  refreshShareHTML();
  usersSelected[0] = "";

	showPopUp("", shareHTML, [611 + (allContacts.length > 12 ? 21 : 0), <?=$siteUsersOnly ? 414 : 491?> - <?=$page == "newpage" ? 35 : 0?> - <?= ($page == "newpage2" || $page == "newpage3") ? 75 : 0?>], 1, <?=$allowClose ? "false" : "true"?>);

}

<?php if ($_GET['show'] == "1") echo "showShare();\n"; ?>
