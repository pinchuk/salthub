<?php

include "inc/inc.php";

$file = $_GET['f'];

$ext = substr(strtolower($file), -3);

$thumb = array(ceil($_GET['w']), ceil($_GET['h']));

if ($thumb[0] > 1000 || $thumb[1] > 1000) die('error 0');

$crop = $_GET['crop'] != "0" && $_GET['crop'] != "N";

if (substr($file, 0, 1) == "/")
	$file = substr($file, 1);

if (strpos($file, "../")) die('error 1');
$x = split("/", $file);

switch ($x[0])
{
	case "videos":
		$parts = explode("/", $file);
		$parts[1] = getPathFromPhotoID($parts[1]);
		$file = implode("/", $parts);
		
		if ($crop)
			$allowedThumbs = array(array(24,24), array(80,55), array(100,75), array(119,95));
		else
			$allowedThumbs = array(array(350,350), array(75,75), array(55,75), array(130,195), array(250,258));
		break;

	case "photos":
		$parts = explode("/", $file);
		$parts[1] = getPathFromPhotoID($parts[1]);
		$file = implode("/", $parts);
		//die($file);
		
		if ($crop)
			$allowedThumbs = array(array(24,24), array(48,48), array(24, 24), array(75,75), array(45,35), array(80,55), array(75,50), array(100,75), array(119,95));
		else
			$allowedThumbs = array(array(178, 266), array(55,75), array(85, 128), array(75,75), array(55,75), array(130,195), array(635,655), array(250,258), array(100,75), array(119,95));
		break;

	case "userpics_$site":
		$noCache = true;
		if ($crop)
			$allowedThumbs = array(array(24,24), array(48,48), array(119,95));
		else
			$allowedThumbs = array(array(178, 266), array(55,75), array(85, 128),array(119,95));
		break;
	
	case "images":
		$noCache = true;
		$anyThumb = true;
		break;

	default:
	die('error 3');
	break;
}

if (!$anyThumb && !in_array($thumb, $allowedThumbs)) die('error 4');

$targetDir = dirname($file) . "/thumbs/" . ($crop ? "" : "N") . $thumb[0] . "x" . $thumb[1];

if (@filemtime($targetDir . "/" . basename($file)) > 1312206736)
{
	header("Content-Type: image/jpeg");
	readfile($_SERVER["DOCUMENT_ROOT"] . "/" . $targetDir . "/" . basename($file));
	die();
}

if (!$noCache)
	@mkdir($targetDir, 0777, true);

$basename = basename($file);
if (!file_exists($file)) $file = "images/noimage.jpg";

if (strlen($x[2]) == 15) //youtube request
{
	$file = "http://i.ytimg.com/vi/" . substr($x[2], 0, 11) . "/1.jpg";
	//die($file);
}

if ($ext == "png")
{
	$img = imagecreatefrompng($file);
}
elseif ($ext == "gif")
{
	$img = imagecreatefromgif($file);
}
else
{
	$img = @imagecreatefromjpeg($file);
	if (!$img)
		$img = @imagecreatefrompng($file);
	if (!$img)
		$img = @imagecreatefromgif($file);
		
}

$width = imageSX($img);
$height = imageSY($img);

if (!$width || !$height) {
	echo "ERROR:Invalid width or height";
	exit(0);
}

// Build the thumbnail
$result = scaleImage($width, $height, $thumb[0], $thumb[1], $crop);

$target_width = $result[0];
$target_height = $result[1];
$widthOffset = $result[2];
$heightOffset = $result[3];

$new_img = ImageCreateTrueColor($target_width, $target_height);

if (!@imagecopyresampled($new_img, $img, 0, 0, round($widthOffset / 2), round($heightOffset / 2), $target_width, $target_height, $width - $widthOffset, $height - $heightOffset)) {
	echo "ERROR:Could not resize image";
	exit(0);
}

header('Content-Type: image/jpeg');

$quality = 90;
if (!$noCache)
	imagejpeg($new_img, $targetDir . "/" . $basename, $quality);
imagejpeg($new_img, null, $quality);
imagedestroy($new_img);
imagedestroy($img);
?>