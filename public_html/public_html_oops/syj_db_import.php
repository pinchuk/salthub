<?php

include "inc/inc.php";

$fromEmail = true;
include "upload/photo.php";

// get categories
echo "Retrieving category list ... ";
$html = file_get_contents("http://www.superyachtjobs.com/companydirectory.asp");
$tmp = explode('<a href="directory.asp?a=', $html);

echo "done.\n";

foreach ($tmp as $x)
{
	$i = strpos($x, '"');

	if ($i > 5) continue;

	$id = substr($x, 0, $i);
	$j = strpos($x, "<");
	$name = substr($x, $i + 2, $j - $i - 2);

	$cats[$id] = $name;

  //mysql_query( "insert into categories (cat, catname, cattype) values ($id, '$name', 'C')" );
}



echo "Found " . count($cats) . " categories.\n";

// get directory for each category

foreach ($cats as $catid => $catname)
{
	for ($catpage = 1, $foundcat = true; $foundcat == true; $catpage++)
	{
		$foundcat = false;
		
		echo "Getting directory for category #{$catid} - page #{$catpage} - ({$catname}) ... ";
		
		$html = file_get_contents("http://www.superyachtjobs.com/directory.asp?a={$catid}&p={$catpage}");
		$tmp = explode('<table id="companyid', $html);
		
		echo "done.\n";
		
		foreach ($tmp as $x)
		{
			// get each company info
			
			$i = strpos($x, '<td class="companyname">');
			if ($i === false) continue;		

			$foundcat = true;
			
			$j = strpos($x, '</td>');
			$name = trim(substr($x, $i + 24, $j - $i - 24));
			
			$x = substr($x, $j + 5);
			$i = strpos($x, '<td>');
			$j = strpos($x, '</td>', $i);
			$city = trim(substr($x, $i + 4, $j - $i - 4));
			
			$x = substr($x, $j + 5);
			$i = strpos($x, '<td>');
			$j = strpos($x, '</td>');
			$country = trim(substr($x, $i + 4, $j - $i - 4));
			
			$i = strpos($x, 'CompanyDetails.asp?coid=');
			$j = strpos($x, '"', $i);
			$id = trim(substr($x, $i + 24, $j - $i - 24));

			$logo = "http://www.synfo.com/Directories/isadmin/images/CompanyLogos/{$id}x300.jpg";

			echo "- Getting information for company #{$id} ($name) ... ";

			$html = file_get_contents("http://www.superyachtjobs.com/CompanyDetails.asp?coid=$id");

			echo "done.\n";

			if (stripos($html, $logo) === false)    //Web server must be MS; not case sensitive.  Was exclusing some logos.
				unset($logo);
			
			$companies[$id] = array("catid" => $catid, "name" => $name, "city" => $city, "country" => $country, "logo" => $logo);

      if( quickQuery( "select count(*) from pages where gname='" . addslashes( $name ) . "'" ) > 0 ) continue;


      //Get Description
			$i = substr( $html, strpos($html, '<div id="panelaboutusdetail">') );
      $chunks = explode( "</div>", $i );
      $desc = trim( strip_tags( $chunks[0], "<br/>" ) );

      //Get Products
			$i = substr( $html, strpos($html, '<p>Category: <strong>') );
      $chunks = explode( "</strong></p>", $i );
      $products = trim( strip_tags( $chunks[0], "<br/>" ) );
      $products = str_replace( "Category: ", "", $products );

      //Get other crap
			$i = strpos($html, '<td width="470">');
			$j = strpos($html, '<td style="width:265px;">');
			$tmp = explode("<br/>", strip_tags(substr($html, $i, $j - $i), "<br/>"));

			for ($i = 0; $i < count($tmp); $i++)
			{
				$line = trim($tmp[$i]);
				
				if (strpos($line, ": ") === false)
					$companies[$id]['address'] .= "\n" . trim($line);
				else
				{
					$x = explode(": ", $line);
					$field = trim(strtolower($x[0]));
					if (!empty($field))
						$companies[$id][$field] = trim($x[1]);
				}
			}

      if( isset( $companies[$id]['tel'] ) )
        $companies[$id]['tel'] = str_replace( "+", "", $companies[$id]['tel'] );
      else
        $companies[$id]['tel'] = '';

      if( isset( $companies[$id]['fax'] ) )
        $companies[$id]['fax'] = str_replace( "+", "", $companies[$id]['fax'] );
      else
        $companies[$id]['fax'] = '';

			$companies[$id]['address'] = trim($companies[$id]['address']); //str_replace("\n", "|", trim($companies[$id]['address']));

      $pid = 0;
      if( isset( $logo ) )
      {
        try
        {
  				$contents = file_get_contents($logo);
	  			$source = "/tmp/temp.jpg";
  				file_put_contents($source, $contents);
        }
        catch( Exception $e)
        {
          echo "Error saving logo: $logo <br>";
        }

        $_GET['pc'] = 1;
        global $fromEmail;
        $fromEmail = true;

        $API->uid = -1;

        ob_start();
        processPhoto($source, true);
        $json = json_decode(ob_get_contents(), true);
        ob_end_clean();

        $pid = $json['id'];

      }

      $gname = $companies[$id]['name'];
      $subcat = $companies[$id]['catid'];
      $phone = $companies[$id]['tel'];
      $fax = $companies[$id]['fax'];
      $email = $companies[$id]['email'];
      $www = $companies[$id]['website'];
      $contact_person = $companies[$id]['contact'];

      //echo "<br>Descr: $desc <br> Products: $products<br />Logo: $logo<br />";
      $gname = addslashes( $gname );
      $desc = addslashes( $desc );
      $products = addslashes( $products );


      mysql_query( "insert into pages (gname, pid, cat, subcat, phone, fax, email, www, contact_person,created,active, descr, products) values ('$gname',$pid,111,'$subcat','$phone','$fax', '$email','$www','$contact_person', NOW(), 1, '$desc', '$products' )" );
//        echo mysql_error();
//        echo "Done";
//        exit;

		}
	}
}

//file_put_contents("/tmp/parsed", var_export($cats, true) . "\n\n" . var_export($companies, true));

echo "All done.  Output saved to /tmp/parsed\n\n";

?>