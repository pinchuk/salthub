var tagType;
var tagType;
var tagId;
var tagProfileURL;
var mediaType;

var tags = new Array();

function tagMessage(err, msg)
{
	e = document.getElementById("tagmessage");
	
	if (err)
		e.style.color = "#800000";
	else
		e.style.color = "#008000";

	e.innerHTML = msg;
}

function tagContact(e, i, isCustom)
{
	if (typeof isCustom == "undefined")
		isCustom = false;

	id = tagQueue[tagQueueIndex];

	if (typeof tags[id] == "undefined")
  {
		tags[id] = new Array();
  }

	box = document.getElementById("usertagmini");
	cid = tags[id][i][1];

	if (e.checked) //tagging
	{
		if (t == "P" && box && box.style.display == "none")
		{
			tagMessage(true, "You must first click the area in the picture you'd like to tag.");
			e.checked = false;
		}
		else
		{
      if( box )
  			box.style.display = "none";
      if( mediaType != "V" )
      {
  			tags[id][i][2] = box.style.left.substring(0, box.style.left.length - 2) - document.getElementById("imgmini").offsetLeft;
        tags[id][i][3] = box.style.top.substring(0, box.style.top.length - 2) - document.getElementById("imgmini").offsetTop;
      }
      else
      {
  			tags[id][i][2] = 0;
        tags[id][i][3] = 0;
      }
		}
	}
	else //untagging
	{
    tags[id][i][2] = -2;
    tags[id][i][3] = -2;
	}

  if( mediaType != "V" )
  {
  	placeTagBox(-1, -1, true, "");
    togglePopupWindow(false);
  }

  refreshNames();
}

function tagPicture(oevent) //User just clicked on the media
{
	evnt = new xEvent(oevent);

	pos = findPos(document.getElementById("imgmini"));
	pic = document.getElementById("imgmini");

	t = evnt.pageY - pos[1] - 19 + document.getElementById("imgmini").offsetTop;
	l = evnt.pageX - pos[0] - 19 + document.getElementById("imgmini").offsetLeft;

	if (t < 0) t = 0;
	if (l < 0) l = 0;

//	if (l > pic.width - 36) l = pic.width - 36;
//	if (t > pic.height - 36) t = pic.height - 36;

  popup = document.getElementById('contactspopup');
  if( popup )
  {
    var X = new Number(l + 40);
    var Y = new Number(t - 10 );

    popup.style.position = "absolute";
    popup.style.left = X.toString() + "px";
    popup.style.top = Y.toString() + "px";

    temp = document.getElementById('NotListedForm');
    if( temp ) temp.style.display = 'none';

    togglePopupWindow(true);
  }

	placeTagBox(l, t, true);
}

function placeTagBox(x, y, mini, name)
{
	if (mini)
		e = document.getElementById("usertagmini");
	else
		e = document.getElementById("usertag");

	if (!e) return;

	if (x == -1)
		e.style.display = "none";
	else
	{
		e.style.top = y + "px";
		e.style.left = x + "px";

		e.style.display = "";
	}

	if (typeof name != "undefined")
	{
		if (name == "")
			placeNameTag(-1);
		else
			placeNameTag(x, y + 90, name);
	}
}

function placeNameTag(x, y, username)
{
//  alert( x+","+y+username );
	var e = document.getElementById("nametag");

  if( !e ) return;

	if (x == -1)
  {
 		e.style.display = "none";
  }
	else
	{

  		e.style.top = y + "px";
	  	e.style.left = x + "px";

  		e.innerHTML = username;

	  	e.style.display = "";
	}
}

var tagQueue = new Array();
var tagQueueIndex = 0;
var defaultTagSearch = "Type a tag here";
//var tagPopUpShown = false;

function showTagPopUp(t, id, img)
{
	tagType = t;
	tagId = id;

  if( t == "V" )
  {
    loadShare(t, 1, id );
    return;
  }


	if (document.getElementById("tagpopupcontent"))
		tagPopUpShown = true;
	else
		tagPopUpShown = false;

  mediaType = t;

  if( t == "P" )
  {
    html = ' <div style="display:none; position:absolute; z-index:3; background-color:#EEEEEE; padding:5px;" id="contactspopup">';   //Begin popup

    html += ' <input type="text" name="name" id="tagNotListedName" onkeyup="javascript:renderPopupWindow(this.value);" value="Type a tag here" onclick="javascript: if( this.value == \'Type a tag here\' ) this.value = \'\';" style="font-size:10px; width:130px;"><br>';
    html += ' <input type="text" name="email" id="tagNotListedEmail" value="Enter an email address" onclick="javascript: if( this.value == \'Enter an email address\' ) this.value = \'\';" style="font-size:10px; width:130px;"><br>';
    html += ' <div style="float:left; display:inline;"><a href="javascript:void(0);" onclick="javascript: newContactTag();" class="addtag" id="tagaddnew">add tag from above</a></div>';
    html += ' <div style="clear:both;"></div>';

  	html += '	<div style="font-size: 8pt; padding: 3px 0;">or choose from below &#0133;</div>';
  	html += '	<div id="addtag" style="height: 125px; overflow-y: scroll; overflow-x: hidden; background: #FFF; border: 1px solid #ccc;">';
  	html += '	 <div style="line-height: 289px; height: 289px; text-align: center;"> &nbsp; <img src="/images/waiteee.gif" alt="" /></div>';
  	html += '	</div>';

    html += '</div>'; //End pupup
  }
  else
    html = "";

	if (tagPopUpShown)
		html += "";
	else
		html += '<div id="tagpopupcontent">';

	html += '<div style="padding-bottom: 5px;"><span class="smtitle">Who\'s in this?</span> &nbsp; <span style="font-size: 8pt;">';
	if (t == "P")
  	html += 'Click on someone or something and select who/what\'s in the box.';
	else
	  html += 'Select who/what\'s in the video.';
	html += '</span></div>';
	html += '<div class="smtitle" style="padding-bottom: 5px; color: #800000;" id="tagmessage">&nbsp;</div>';

//1
  if( t == "P" )
  	html += '<div style="text-align:center; width:100%;"><div id="usertag_container" class="usertag_container" style="margin-left:auto; margin-right:auto;">';
  else
    html += '<div style="width:400px;"><div id="usertag_container">';

	html += '	<div class="usertagmini" id="usertagmini" style="display: none;">&nbsp;</div>';
	html += '	<div class="usertag" id="usertag" style="display: none;">&nbsp;</div>';
	html += '	<div style="' + (t=="V" ? 'width:200px;' : '') + '"><img width="250" id="imgmini" src="' + img + '" alt="" style="' + (t == "V" ? "cursor: default; float:left; margin-left:40px;" : "") + ' margin-bottom:20px;" /></div>';

  if( t != "P" )
  {
    html += '<div style="float:right; margin-right:40px; text-align:right; width:160px;">';
    html += ' <input type="text" name="name" id="tagNotListedName" onkeyup="javascript:renderPopupWindow(this.value);" value="Type a tag here" onclick="javascript: if( this.value == \'Type a tag here\' ) this.value = \'\';" style="font-size:10px; width:145px;">';
    html += ' <input type="text" name="email" id="tagNotListedEmail" value="Enter an email address" onclick="javascript: if( this.value == \'Enter an email address\' ) this.value = \'\';" style="font-size:10px; width:145px; display:none;">';

    html += ' <div style="display:inline;"><a href="javascript:void(0);" onclick="javascript: newContactTag();" class="addtag" id="tagaddnew">add new</a></div>';

  	html += '	<div id="addtag" style="height: 125px; width:150px; overflow-y: scroll;overflow-x: hidden; background: #FFF; border: 1px solid #ccc; text-align:left; margin-bottom:20px; float:right;">';
  	html += '	 <div style="line-height: 289px; height: 289px; text-align: center;"> &nbsp; <img src="/images/waiteee.gif" alt="" /></div>';
  	html += '	</div><div>';
  }

	html += '</div></div>';
//1


	html += '<div style="clear: both;"></div>';

  html += '<div id="inthisphoto" style="margin-bottom:20px; margin-top:20px; font-size:12px; width:400px;"><b>Tagged Here: </b><span id="taggednames">(no tags)</span></div>';

	html += '	<div style="clear: both;"></div>';

	html += '	<div style="float: left; text-align: right; width: 250px; font-size: 9pt;" id="tagprevnext"></div>';
	html += '<div class="publish_it" style="clear:both;">';
	html += '	When done tagging this album, publish it! &nbsp; <input onclick="javascript:publishTags();" type="button" class="button" value="Publish" />';
	html += '</div>';


	if (tagPopUpShown)
	{
		document.getElementById("tagpopupcontent").innerHTML = html;
	}
	else
	{
		html += '</div>';
    tags = new Array();
		showPopUp("", html, 0, "no", false);
	}

	if (t == "P")
  {
		xAddEventListener(document.getElementById("imgmini"), "click", tagPicture, false);
    xAddEventListener(document.getElementsByTagName("body")[0], "click", closePopup, false);
  }

  getAjax("/gettagcontacts.php?type=" + t + "&id=" + id, getTagContactsHandler );

	showTagPrevNext();

	tagPopUpShown = true;
}

function showTagPrevNext()
{
	if (tagQueue.length < 2) return;
	
	html = "";

	if (tagQueueIndex > 0)
		html += '<a href="javascript:void(0);" onclick="javascript:tagNext(false);">previous <img src="/images/left.png" alt="" /></a>&nbsp;';

	if (tagQueueIndex < tagQueue.length - 1)
		html += '&nbsp;<a href="javascript:void(0);" onclick="javascript:tagNext(true);"><img src="/images/right.png" alt="" /> next</a>';
	
	document.getElementById("tagprevnext").innerHTML = html;
}

function tagNext(next)
{
	document.getElementById("tagprevnext").innerHTML = '&nbsp;<img src="/images/wait_sm.gif" alt="" />';
	
	if (next)
		tagQueueIndex++;
	else
		tagQueueIndex--;

	getAjax("/getphototagpreview.php?id=" + tagQueue[tagQueueIndex], "tagNextHandler");
}

function tagNextHandler(data)
{
	showTagPopUp("P", tagQueue[tagQueueIndex], data);
}

var tagContactsHTML = "";

function deleteTag(t, tid, hash, cid)
{
	document.getElementById("tagged-" + tid).style.display = "none";

  if( typeof highlightUsers != "undefined"  )
  {
    for( c = 0; c < highlightUsers.length; c++ )
    {
      if( highlightUsers[c] == cid )
      {
        highlightUsers.splice( c, 1 );
      }
    }
  }

	postAjax("/deletetag.php", "type=" + t + "&id=" + tid + "&hash=" + hash, "void");
}

function saveTags(t, id)
{
	data = "";
	customNames = "";

	for (i = 0; i < tags[id].length; i++)
  {
    if( tags[id][i][2] >= 0 || tags[id][i][2] == -2)
    {

  		data += String.fromCharCode(2) + tags[id][i].join(String.fromCharCode(1));
    }
  }

	postData = "type=" + t + "&id=" + id + "&data=" + encodeURIComponent( data.substring(1) );

  if (typeof currentPage != "undefined")
    postData += "&gid=" + currentPage;

  if( script == "home")
  	postData = postData + "&uploading=1";

	//alert(postData);
	postAjax("/savetags.php", postData, "saveTagsHandler");

  popup = document.getElementById('contactspopup');
  if( popup )
  {
    popup.style.display='none';
  }
}

function saveTagsHandler(data)
{
	if (data != "OK" )
  {
    if( data == "FB" )
      document.location = "/facebook_error.php";
/*
    else
      if( admin == 1)
	    	alert("Error: " + data);
*/
  }

	if (script == "media") //we are on the photo page
  {
		location.reload(true);
  }
//	else //we are uploading from elsewhere
//		tagMessage(false, "Your tags for this photo have been saved.");
}

function cancelTags()
{
	if (script == "media") //we are on the photo page
		closePopUp(true);
	else //we are uploading from elsewhere
	{
		if (tagType == "V")
			tt = "V";
		else if (tagQueue.length > 1) //album from main image
			tt = "A_";
		else
			tt = "P";

		loadShare(tt, 1, tagQueue[0]);
	}
}

function userTagBoxClicked()
{
	//location = tagProfileURL;
}

function boxTagOnPhoto(e, ev)
{
	if (typeof tags == "undefined") return;

	ipos = findPos(e);
	cpos = getCursorPosition(ev);

	pos = [cpos[0] - ipos[0], cpos[1] - ipos[1]];

	for (var i in tags)
	{
		if (pos[0] >= (tags[i][0] - 0) && pos[0] <= (tags[i][0] + 81) && pos[1] >= (tags[i][1] - 0) && pos[1] <= (tags[i][1] + 81))
		{
			placeTagBox(tags[i][0], tags[i][1], false, tags[i][2]);
			//placeNameTag(tags[i][0], tags[i][1] + 90, tags[i][2]);
			return;
		}
	}

	placeTagBox(-1, -1, false, "");
}

function togglePopupWindow( forceOpen )
{
  if( typeof forceOpen == "undefined" )
    forceOpen = false;

  popup = document.getElementById('contactspopup');
  if( popup )
  {
    if( popup.style.display == "none" || forceOpen )
    {
      text1 = document.getElementById('tagNotListedName');
      text2 = document.getElementById('tagNotListedEmail');

      if( text1 )
        text1.value = "Type a tag here";
      if( text2 )
        text2.value = "Enter an email address";

      renderPopupWindow();
      popup.style.display='';
    }
    else
    {
      popup.style.display='none';
    }
  }

  refreshNames();
}

function newContactTag()
{
	id = tagQueue[tagQueueIndex];

	if (typeof tags[id] == "undefined")
  {
		tags[id] = new Array();
  }

	box = document.getElementById("usertagmini");

  if ( mediaType=="P" && box && box.style.display == "none")
  {
  	tagMessage(true, "You must first click the area in the picture you'd like to tag.");
  }
  else
  {
    if( box )
    	box.style.display = "none";

    var name = document.getElementById('tagNotListedName').value;
    var email = document.getElementById('tagNotListedEmail').value;

    if( mediaType != "V" )
    	tags[id][tags[id].length] = [ name, "new", box.style.left.substring(0, box.style.left.length - 2) - document.getElementById("imgmini").offsetLeft, box.style.top.substring(0, box.style.top.length - 2) - document.getElementById("imgmini").offsetTop, 1, email];
    else
    	tags[id][tags[id].length] = [ name, "new", 0,0, 1, email];
  }

  if( mediaType != "V" )
  {
    togglePopupWindow();
	  placeTagBox(-1, -1, true, "");
  }
  else
  {
    var text1 = document.getElementById('tagNotListedName');
    var text2 = document.getElementById('tagNotListedEmail');

    if( text1 )
      text1.value = "Type a tag here";
    if( text2 )
      text2.value = "Enter an email address";

    renderPopupWindow();
    refreshNames();
  }
}

function cancelTag()
{
	placeTagBox(-1, -1, true, "");

  togglePopupWindow();
}

function removeTag( i )
{
	id = tagQueue[tagQueueIndex];

  tags[id][i][2] = -2;
  tags[id][i][3] = -2;

  refreshNames();

  if( mediaType == "V" )
    renderPopupWindow();
}

function getTagContactsHandler(data)
{
	id = tagQueue[tagQueueIndex];
  if( typeof tags[id] == "undefined" ) //We should only be loading names if this is the first time loading.
  {
    tags[id] = new Array();

    //store database in tag variable
    rows = data.split(String.fromCharCode(2));
  	for (i = 0; i < rows.length - 1; i++)
    {
      tags[id][i] = rows[i].split(String.fromCharCode(1));
    }

    //columns:
    // 0 = Name
    // 1 = cid
    // 2 = x;   -1 means that the contact is not tagged in this media
    // 3 = y;   -1 means that the contact is not tagged in this media
    // 4 = Custom
    // 5 - Page
  }
  refreshNames();

  if( mediaType == "V" )
    renderPopupWindow();
}

function refreshNames()
{
	id = tagQueue[tagQueueIndex];

  content = "";


  if( typeof tags[id] != "undefined" )
    for (i = 0; i < tags[id].length; i++)
    {
      if( tags[id][i][2] >= 0 )
        content += tags[id][i][0] + ' (<a href="javascript:void(0);" onclick="javascript:removeTag(' + i + ');">remove</a>), ';
    }

  if( content.length > 0 )
  {
    content = content.substr( 0, content.length - 2 );
  }
  else
  {
    content = "no tags";
  }

  var taggednames = document.getElementById('taggednames');
  if( taggednames )
    taggednames.innerHTML = content;
}

function renderPopupWindow( searchTerm )
{
  content = "";
  foundPages = false;

	id = tagQueue[tagQueueIndex];

  if( searchTerm != null )
    searchTerm = searchTerm.toUpperCase();

  if( typeof tags[id] != "undefined" )
  {
    for (i = 0; i < tags[id].length; i++)
    {
      cname = tags[id][i][0].toUpperCase();
      if( searchTerm == null || cname.indexOf( searchTerm ) >= 0 )
      {
        if( tags[id][i][5] > 0 && !foundPages )
        {
          foundPages = true;
          content += '<div style="padding-left:4px; padding-top:3px; font-weight:700; font-size:9pt;">Your Pages</div><div style="margin-left:5px; width:95%; border-bottom:1px solid #ddd; height:2px;"></div>'
        }

        content += '<div id="tagdiv-' + tags[id][i][1] + '" class="addtag"><input type="checkbox" value="1" onclick="tagContact(this, ' + i + ');" id="tagcb-' + tags[id][i][1] + '"';
        if( tags[id][i][2] >= 0 ) // this means that this person is already tagged somewhere;
          content += ' checked>';
        else
          content += '>';
        content += tags[id][i][0];
        content += '</div>';
      }
    }
  }

  var email = document.getElementById('tagNotListedEmail');

  if( content == "" )
  {
    if( email )
      email.style.display = '';
    content = "(No contacts)";
  }
  else
    email.style.display = 'none';

  content = '<div id="searchdiv"></div>' + content;

	document.getElementById("addtag").innerHTML = content;
}

function closePopup(e)
{
  if( typeof e == "undefind" || !e )
    var e = window.event;

  target = e.target || e.srcElement;

  if( target.id != "imgmini" && target.id != "contactspopup" && target.id.substring(0,3) != "tag" && target.id != "addtag" )
  {
    popup = document.getElementById('contactspopup');
    if( popup )
      if( popup.style.display == "" )
        togglePopupWindow();

   	placeTagBox(-1, -1, true, "");
  }
}

function publishTags()
{
	id = tagQueue[tagQueueIndex];


  for (j = 0; j < tagQueue.length; j++)
  {
    if( typeof tags[ tagQueue[j] ] != "undefined" )
    {
      saveTags(mediaType, tagQueue[j]);
    }
  }

  cancelTags();
/*
	if (script == "media") //we are on the photo page
		location.reload(true);
*/
}