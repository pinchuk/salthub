<?php
/*
Adds a new wall post to the database.  Called from /inc/tweetbox.php

Change Log
10/27/2011 - Removed post-to-wall permission setting from facebook and twitter, only using checkbox under upload window now.
11/11/2012 - Changed to use session variables to determine if we're going to post to twitter and/or facebook
*/

include "inc/inc.php";

if (!$API->isLoggedIn()) die();

$to = intval($_POST['target']);
$gid = intval($_POST['gid']);

$fbtarget = null;
if( isset( $_POST['fbtarget'] ) )
{
  $fbtarget = $_POST['fbtarget'];
  $_SESSION['fbpages']['selection'] = $fbtarget;
  if( $fbtarget == 0 )
    $fbtarget = null;
}

if ($gid > 0)
	$to = 0;
elseif ($to == 0)
	$to = $API->uid;

sql_query("insert into wallposts (uid,uid_to,post) values (" . $API->uid . ",$to,'" . addslashes( $_POST['post'] ) . "')");

$id = mysql_insert_id();

$from = null;
if( $gid > 0 )
{
  $postAsPage = quickQuery( "select postAsPage from page_members where admin=1 and gid=$gid and uid=" . $API->uid );

  if( $postAsPage == 1 )
  {
    $to = -1;
    $from = -1;
  }

  $API->sendNotificationToPage( NOTIFY_GROUP_COMMENT, array( "from" => ($from == -1 ? 0 : $API->uid), "gid" => $gid ) );


  $jmp = quickQuery( "select jmp from pages where gid='" . $gid . "'" );
  if( $jmp == "" )
  {
    $jmp = shortenURL( "http://" . SERVER_HOST . $API->getPageURL($gid) . "/logbook");
    sql_query( "update pages set jmp='$jmp' where gid='" . $gid . "'" );
  }


}
else
{
//  $uname = quickQuery( "select username from users where uid='" . $API->uid . "'" );
  $jmp = quickQuery( "select jmp from users where uid='" . $API->uid . "'" );
  if( $jmp == "" )
  {
    $jmp = shortenURL( "http://" . SERVER_HOST . $API->getProfileURL($API->uid) . "/logbook");
    sql_query( "update users set jmp='$jmp' where uid='" . $API->uid . "'" );
  }
}


$fid = $API->feedAdd("W", $id, $to, $from, $gid);

$url = getFeedJMPLink( $fid );

$msg = $_POST['post'];


if( strlen($msg) < (130 - strlen(SITE_VIA)) ) //Check to see if we have room for "via XXXX"
  $twmsg .= $msg . " via @" . SITE_VIA;
else
{
  $twmsg = substr( $msg, 0, (110 - strlen(SITE_VIA)) );
  $twmsg .= " $url via @" . SITE_VIA;
}

$msg .= " $url via $siteName";

//Checking if we're going to post to twitter is determined in twUpdateStatus
$API->twUpdateStatus(null, substr($twmsg, 0, 139), true);

if( !isset( $_SESSION['postas'][SM_FACEBOOK][0] )
      || $_SESSION['postas'][SM_FACEBOOK][0] > 0 )
{
  if( !$API->fbStreamPublish(null, $msg, null, $fbtarget) )
  {
    $err = "FB";
  }
}

if( strlen( $err ) > 0 )
{
  echo $err;
}
else
{
  echo "OK";
}
?>