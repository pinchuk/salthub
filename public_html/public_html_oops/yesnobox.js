document.write('<div id="yesno-graytrans" style="display: none; -moz-opacity: .50; filter:alpha(opacity: 50); opacity: .50; background: #C8C8C8; position: absolute; top: 0; left: 0; width: 100%; height: 100%;">&nbsp;</div>');
document.write('<div id="yesnobox" style="visibility: hidden; border: 1px solid black; padding: 20px; position: absolute; top: 0px; left: 0px; z-index: 999; background: white; text-align: center;">');
document.write('	<div class="attention" style="margin-bottom: 15px;" id="yesnotitle">Title</div>');
document.write('	<div style="font-size: 12pt;" id="yesnomessage">');
document.write('	</div>');
document.write('	<div style="margin-top: 15px;" id="yesnoboxbuttons"></div>');
document.write('</div>');

function showYesNo(title, message, buttons, width, grayback)
{
	scrollY = window.pageYOffset || document.documentElement.scrollTop || 0;
	
	e = document.getElementById("yesno-graytrans");
	if (typeof grayback == "boolean" && grayback)
	{
		e.style.top = scrollY + "px";
		e.style.display = "";
		document.body.style.overflow = "hidden";
	}
	else
		e.style.display = "none";
	
	e = document.getElementById("yesnotitle");
	if (typeof title == "string")
	{
		e.innerHTML = title;
		e.style.display = "";
	}
	else
		e.style.display = "none";
	
	document.getElementById("yesnomessage").innerHTML = message;

	e = document.getElementById("yesnobox");
	if (typeof width !== "undefined")
		if (width !== null)
			e.style.width = width + "px";
	
	html = '<div style="padding-left: 15px;">';
	for (i = 0; i < buttons.length; i++)
		html += '<input type="button" id="yesnobtn' + i + '" class="button" value="' + buttons[i][0] + '" onclick="javascript:yesNoHide();' + buttons[i][1] + '" style="margin-right: 15px;" />';
	html += '</div>';
	
	document.getElementById("yesnoboxbuttons").innerHTML = html;
	
	dim = getWindowSize();
	e.style.left = ((dim[0] - e.offsetWidth) / 2) + "px";
	e.style.top = (scrollY + ((dim[1] - e.offsetHeight) / 2)) + "px";
	e.style.visibility = "";
	
	document.getElementById("yesnobtn0").focus();
}

function yesNoHide()
{
	document.getElementById("yesnobox").style.visibility = "hidden";
	document.getElementById("yesno-graytrans").style.display = "none";
	document.body.style.overflow = "";
}

function getWindowSize()
{
	var myWidth = 0;
	var myHeight = 0;
	
	if (typeof window.innerWidth == "number")
	{
		//Non-IE
		myWidth = window.innerWidth;
		myHeight = window.innerHeight;
	}
	else if (document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight))
	{
		//IE 6+ in 'standards compliant mode'
		myWidth = document.documentElement.clientWidth;
		myHeight = document.documentElement.clientHeight;
	}
	else if(document.body && (document.body.clientWidth || document.body.clientHeight))
	{
		//IE 4 compatible
		myWidth = document.body.clientWidth;
		myHeight = document.body.clientHeight;
	}
	
	return new Array(myWidth, myHeight);
}