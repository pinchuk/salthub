<?php

	DEFINE ("DEBUG_MODE",			true);
	DEFINE ("RACKSPACE_USERNAME",	"tweider");
	DEFINE ("RACKSPACE_KEY",		"2eafbeef6d3c8e96728ebd33e031c533");
	DEFINE ("SAMPLE_DOWNLOAD_FILE",	"http://www.salthub.com/images/salt_badge100.png");
	DEFINE ("PHOTO_CDN_CONTAINER_BASE_URL",	"http://c314840.r40.cf1.rackcdn.com");
	DEFINE ("PHOTO_CDN_CONTAINER_BASE",	"sh_photos");
	
	
	if (!strlen(RACKSPACE_USERNAME)) {
		throw Exception ("Missing or Invalid Rackspace Username");
	}
	
	if (!strlen(RACKSPACE_KEY)) {
		throw Exception ("Missing or Invalid Rackspace Key");
	}
	

	function __get_member_folder ($x_member_id=0, $x_range_step=10000, $x_member_count=1000000, $x_force_path=null) {
		if (strlen($x_force_path)) {
			return ($x_force_path);
		}
		
		if ($x_member_id > 0) {
			$previous_range_id = 0;
			
			for ($i = 1; $i <= $x_member_count; $i+=$x_range_step) {
								
				if ($x_member_id >= $previous_range_id && $x_member_id < $i) {
					return ($previous_range_id);
				}
				
				$previous_range_id++;
			}
		}
	}

	function __download_file ($x_url) {
		if (strlen(trim($x_url))) {
			$tmp_filename = tempnam ("/tmp", "downloaded_file_test__") . "." . pathinfo($x_url, PATHINFO_EXTENSION);
			
			if (DEBUG_MODE) {
				echo ("\$tmp_filename = " . $tmp_filename . "<br />");
			}
						
			if (file_put_contents($tmp_filename, file_get_contents($x_url), FILE_BINARY) !== false) {
				return ($tmp_filename);
			} else {
				return ("Download Failure");
			}
		}
	}
	
?>