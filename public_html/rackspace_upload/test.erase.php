<?php
	require_once ("./configuration.php");
	require_once ("./rackspace_classes/cloudfiles.php")
	
	
	if (DEBUG_MODE) echo ("Trying Delete For Record Number " . $i . "<br />");
		try {
			if (DEBUG_MODE) echo ("Switching Record Number $i to Container Name " . $x_file_ids[$arrKeys[$i]] -> GetRemoteContainerName() . "<br />");
			
			$FileContainer = $this -> PROVIDER_CONNECTION -> get_container ($x_file_ids[$arrKeys[$i]] -> GetRemoteContainerName());

			
			if (DEBUG_MODE) echo ("Got File Container For Record Number" . $i . "<br />");
			
			try {
				/* cloudfiles needs the object folder and the object id in order to delete it */ 
				if ($FileContainer -> delete_object ($x_file_ids[$arrKeys[$i]] -> GetFileName(), $x_file_ids[$arrKeys[$i]] -> GetRemoteContainerName())) {
					if (DEBUG_MODE) echo ("Deleted Object Number " . $i . "<br />");
					$intRemovedCount++;
				}
			} catch (Exception $e) {
				if (DEBUG_MODE) throw new Exception ($e);
			}
		} catch (Exception $e) {
			if (DEBUG_MODE) throw new Exception ($e);


?>