<?php
	require_once ("./configuration.php");
	require_once ("./rackspace_classes/cloudfiles.php");
	
	if (DEBUG_MODE) {
		echo ('<div>RACKSPACE_USERNAME = ' . RACKSPACE_USERNAME . "</div>");
		echo ('<div>RACKSPACE_KEY = ' . RACKSPACE_KEY . "</div>");
		echo ('<div>SAMPLE_DOWNLOAD_FILE = ' . SAMPLE_DOWNLOAD_FILE . "</div>");
		echo ('<div>PHOTO_CDN_CONTAINER_BASE = ' . PHOTO_CDN_CONTAINER_BASE . "</div>");
		
		
	}
	
	$objRackspaceAuthentication = new CF_Authentication(RACKSPACE_USERNAME, RACKSPACE_KEY);
	
	$blAuthenticated = $objRackspaceAuthentication ->	authenticate();
	
	$objRackspaceConnection = new CF_Connection ($objRackspaceAuthentication);
			
	$objRackspaceConnection ->	setDebug (false);
	$objRackspaceConnection ->	ssl_use_cabundle(); 
 			
	
	//put path from $UPLOADED_FILES ARRAY VARIABLE here, probably tmp_name
	$blDownloadedFilename = __download_file(SAMPLE_DOWNLOAD_FILE);

	
	echo ("\$blDownloadedFilename = " . $blDownloadedFilename . "<br />");
	
	echo ("<table border=\"1\">");
	echo ("<tr><th>User ID</th><th>Member Target Folder ID</th><th>Upload Status</th><th>Uploaded URL</th></tr>");
	
	$arrFilename = pathinfo($blDownloadedFilename);	//
	
	
	$strUploadPath = "member_uploads_" . $intMemberID;
	
	try {
		//create the folder for this user
		// $strSQL = "SELECT remote_container_name FROM salthub.members WHERE member_id = 5";
		
		$objRS = mysql_query ($strSQL, $objDB);
		
		$strContainerName = mysql_result ($objRS, "remote_container_name", 0);
		
		if (!strlen(trim($strContainerName))) { //check to see if the user doesn't have a folder created already, make new folder
			if ($DESTINATION_CONTAINER = $objRackspaceConnection -> create_container($strUploadPath) !== false) {
				//if folder was created properly, get a handle on the folder
						
				$DESTINATION_CONTAINER = $objRackspaceConnection -> get_container($strUploadPath);
			
			}
		} else {
		
			$DESTINATION_CONTAINER = $objRackspaceConnection -> get_container($strUploadPath);
		}
	
	} catch (Exception $e) {
	
	}
	
	
	
	
	$objRemoteFile = $DESTINATION_CONTAINER -> create_object (
		$arrFilename["basename"] /* the name you want on the remote server, you refer to it as hash.jpg */
	);

	$blUploadStatus = "false";
	
	if ($objRemoteFile) {
		//try to upload the file by opening it from the local upload file system
		//$blDownloadedFilename = the local path the uploaded file, probably tmp_name in uploaded files
		
		if ($objRemoteFile -> write (fopen($blDownloadedFilename, "r") /* rackspace wants a file handle as a resource */, 
			filesize($blDownloadedFileName) /* size of the object being uploaded */) {

			if (DEBUG_MODE) {
				echo ("Remote File Created<br />");
			} //returns status update if file was updated
			
			//enable cdn for this file
			
			$blUploadStatus = "true"; //updated pseuo-boolean flag if it was updated
		}
	}
	 
	
	//if it was uploaded, it will create a link to the file so you can test view it
	if ($blUploadStatus === "true") {
		$strUploadedURL = "<a href=\"" . PHOTO_CDN_CONTAINER_BASE . "/" . $strUploadPath . $arrFilename["basename"] . "\">View File</a>";
	} else {
		$strUploadedURL = "Upload Failure";
	}
	
	
		
	exit;
	

?>