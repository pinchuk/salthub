<?php
/*
Saves tag information from photos and videos.  Also sends out notifications to those who own the media and others who have previously been tagged.

1/13/2012 - Added support for pages as custom contacts
3/12/2012 - Added fix for email notification to media owner.  Was not sending notification to the correct email address.
5/5/2012  - Added Facebook error checking
8/22/2012 - Change custom tag name and email to use addslashes().
*/


include_once "inc/inc.php";

$API->requireLogin();
$API->requireSite("s");

$err = "OK";

$tags = explode(chr(2), $_POST['data']);

$uploading = $_POST['uploading'];
$postAsGid = intval( $_POST['gid'] );

if( $postAsGid > 0  )
{
  $postAsPage = quickQuery( "select postAsPage from page_members where admin=1 and gid=$postAsGid and uid=" . $API->uid );
  if( $postAsPage != 1 )
    $postAsGid = 0;
}

if ($_POST['type'] == "P")
	$type = "P";
else
	$type = "V";

$id = intval($_POST['id']);

$customNames = explode(chr(1), $_POST['customNames']);
$nCustomName = 0; //keeps track of the number of custom names that have been processed

$namesTagged = array();
$addedToFeed = false;

foreach ($tags as $tag)
{
	$tag = explode(chr(1), $tag);

  if( sizeof( $tag ) < 5 )
    continue;

    if (intval($tag[4]) == 1)
			$name = $tag[0];
		else if ($tag[1]== 0) //tagged self
			$name = $API->name;
		else
			$name = quickQuery("select name from contacts where cid=" . $tag[1] );

  if( $name != "" )
  	$namesTagged[] = $name;
}
sort($namesTagged);

$info = array();

foreach ($tags as $tag)
{
	$tag = explode(chr(1), $tag);

  if( sizeof( $tag ) < 5 )
    continue;

  $name = $tag[0];
  $cid = intval($tag[1]);
  $x = intval($tag[2]);
  $y = intval($tag[3]);
  $custom = intval($tag[4]);
  $gid = 0;

  $pageContact = false;
  if($custom==2) {
    $custom=1;
    $pageContact=true;
  }

  $email = $tag[5];
  $sendEmailNotification = false;
  $sendNotification = false;

  $name = addslashes( $name );
  $email = addslashes( $email );

	if ($tag[1] == "new" && $x >= 0)
	{
		sql_query("insert into custom_tags (name, email) values ('" . $name . "', '" . $email . "')");
		$cid = mysql_insert_id();
    $sendEmailNotification = true;
	}
  elseif ( $pageContact && $x >= 0)
  {
    $gid=intval($tag[1]);
    $cid = quickQuery( "select id from custom_tags where gid='$gid'" );
    if(intval($cid)<=0)
    {
      sql_query("insert into custom_tags (name, email, gid) values ('" . $name . "', '', '$gid')");
      $cid = mysql_insert_id();

      sql_query( "insert into page_media (type,id,gid,uid) values ( '$type', '$id', '$gid', '" . $API->uid . "' );" );
    }
  }

  $name = stripslashes( $name );
  $email = stripslashes( $email );

	if ($x == "-2") //user untagged something
	{
		sql_query("delete from " . typeToWord($type) . "_tags where uid=" . $API->uid . " and cid=$cid");
		continue;
	}

	if ($type == "P")
	{
		$xq = sql_query("select width,height from photos where id=$id");
		if (mysql_num_rows($xq) == 0) die();

		$dims = mysql_fetch_array($xq, MYSQL_ASSOC);

		$full = scaleImage($dims['width'], $dims['height'], 635, 655);
		$prev = scaleImage($dims['width'], $dims['height'], 250, 258);

		$ratio = $full[0] / $prev[0];

		$x = $x * $ratio;
		$y = $y * $ratio;


    $tid = quickQuery( "select tid from photo_tags where pid='$id' and cid='$cid' and uid='" . $API->uid . "'" );
    if( empty( $tid ) )
    {
  		sql_query("insert into photo_tags (custom,pid,cid,x,y,uid) values ($custom,$id,$cid," . $x . "," . $y . "," . $API->uid . ")");
  		$tid = mysql_insert_id();
      $sendNotification = true;

      $eid = quickQuery( "select eid from contacts where cid=$cid" );
      if( isset( $eid ) && stristr( $eid, "@" ) != false )
        $sendEmailNotification = true;
    }
    else
      sql_query( "update photo_tags set x='$x', y='$y' where pid='$id' and cid='$cid' and uid='" . $API->uid . "'" );


		if ($cid == 0) //tagged self
		{
			$user['eid'] = $API->uid;
			$user['name'] = $API->name;
			$user['site'] = SITE_OURS;
		}
		elseif ($custom == 1 )
		{
			$user['site'] = SITE_CUSTOM;
			$user['name'] = $customName;
		}
		else
		{
			$xq = sql_query("select site,eid,name from contacts where cid=$cid");
			$user = mysql_fetch_array($xq, MYSQL_ASSOC);
		}

		if (empty($info))
		{
  		$x = sql_query("select photos.uid,aid,jmp,title,descr from photos join albums on albums.id=photos.aid where photos.id=$id");
			$info = mysql_fetch_array($x, MYSQL_ASSOC);
		}

		if (($user['site'] == SITE_OURS || $user['site'] == SITE_CUSTOM) )
		{
      if( $gid > 0 )
      {
        if( !$addedToFeed ) {
          $API->feedAdd( ($type == "P" ? "t" : "T"), $tid, -1, $API->uid, $gid );
          $addedToFeed = true;
        }
      }
      else
      {
        if( !$addedToFeed ) {
          $to = $user['eid'];
          $from = null;
          if( $postAsGid > 0 )
          {
            $to = -1;
            $from = -1;
          }

          $API->feedAdd("t", $tid, $to, $from, $postAsGid );
          $addedToFeed = true;
        }

        $info['tid'] = $tid;
        $info['uid'] = $user['eid'];
        $info['type'] = $type;
        $info['id'] = $id;
      	$info['namesTagged'] = array_unique($namesTagged);

        if( !is_null($info['uid']) && !is_null($info['tid']) && $info['uid'] != $API->uid)
  			  $err = $API->sendNotification(NOTIFY_PHOTO_TAGGED, $info );
      }
		}
	}
	else
	{

    $tid = quickQuery( "select tid from video_tags where vid='$id' and cid='$cid' and uid='" . $API->uid . "'" );
    if( empty( $tid ) )
    {
		  sql_query("insert into video_tags (custom,vid,cid,uid) values ($custom,$id,$cid," . $API->uid . ")");
    	$tid = mysql_insert_id();
      $sendNotification = true;

      $eid = quickQuery( "select eid from contacts where cid=$cid" );
      if( isset( $eid ) && stristr( $eid, "@" ) != false )
        $sendEmailNotification = true;
    }

		if ($cid == 0) //tagged self
		{
			$user['eid'] = $API->uid;
			$user['name'] = $API->name;
			$user['site'] = SITE_OURS;
		}
		elseif ($custom == 1)
		{
			$user['site'] = SITE_CUSTOM;
			$user['name'] = $customName;
		}
		else
		{
			$xq = sql_query("select site,eid,name from contacts where cid=$cid");
			$user = mysql_fetch_array($xq, MYSQL_ASSOC);
		}

		if (empty($info))
		{
  		$x = sql_query("select uid,title,descr,jmp from videos where id=$id");
			$info = mysql_fetch_array($x, MYSQL_ASSOC);
		}

		if ($user['site'] == SITE_OURS && $sendNotification )
		{
      if( $gid > 0 )
      {
        if( !$addedToFeed ) {
          $API->feedAdd( ($type == "P" ? "t" : "T"), $tid, -1, $API->uid, $gid );
          $addedToFeed = true;
        }
      }
      else
      {
        if( !$addedToFeed ) {
          $to =  $user['eid'];
          $from = null;
          if( $postAsGid > 0 )
          {
            $to = -1;
            $from = -1;
          }

    			$API->feedAdd("T", $tid, $to, $from, $postAsGid );
          $addedToFeed = true;
        }

        $info['tid'] = $tid;
        $info['uid'] = $user['eid'];
        $info['type'] = $type;
        $info['id'] = $id;
      	$info['namesTagged'] = array_unique($namesTagged);

        if( !is_null($info['uid']) && !is_null($info['tid']))
    			$err = $API->sendNotification(NOTIFY_VIDEO_TAGGED, $info );
      }
		}
	}


	//notify media owner
	if ($API->getMediaOwner( $id, $type ) != $API->uid)
	{
    $info['uid'] = $API->getMediaOwner( $id, $type );
		$info['type'] = $type;
    $info['id'] = $id;

		$err = $API->sendNotification(NOTIFY_ADDTAG, $info);
	}

  if( $sendEmailNotification && isset( $email ) && strpos( $email, "@" ) > 0 )
  {
    $title = current($API->getMediaInfo($type, $id, "title") );

  	$msgTemplate = quickQuery("select content from static where id='new_usr_tagged'");
  	$msgTemplate = str_replace("{NAME_FROM}", $API->name, $msgTemplate);
  	$msgTemplate = str_replace("{REF_LINK}", findLinks("http://" . $_SERVER['HTTP_HOST'] . "/?ref=" . $API->uid . "&cid=" . $cid ), $msgTemplate);
  	$msgTemplate = nl2br($msgTemplate); //str_replace("\n", "<br />", $msgTemplate);
  	$msgTemplate = str_replace(".  ", ".&nbsp; ", $msgTemplate);

  	$msgTemplate = str_replace("{NAME_TO}", $name, $msgTemplate);
  	$msgTemplate = str_replace("{MEDIA_TITLE}", $title, $msgTemplate);
  	$msgTemplate = str_replace("{MEDIA_TYPE}", typeToWord($type), $msgTemplate);
  	$msgTemplate = str_replace("{MEDIA_LINK}", "http://" . $_SERVER['HTTP_HOST'] . $API->getMediaURL($type, $id, $title) . "?ref=" . $API->uid . "&cid=" . $cid, $msgTemplate);

    emailAddress( $email, "You were tagged in a " . typeToWord($type) . " on " . $siteName . "!", $msgTemplate );

    //Add new user to contacts
    $temp_uid = quickQuery( "select uid from users where email='$email'" );
    if( empty( $temp_uid ) )
      sql_query( "insert into contacts (eid, name, uid, site, invited) values ('$email','$name'," . $API->uid . ", 6,1)" );
    else
    {
      $temp_name = quickQuery( "select name from users where uid='$temp_uid'" );
      $q = sql_query( "select cid from contacts where uid='" . $API->uid . "' and eid='$temp_uid'" );
      if( mysql_num_rows( $q ) == 0 )
        sql_query( "insert into contacts (eid, name, uid, site) values ($temp_uid,'$temp_name'," . $API->uid . ", 2)" );
    }
  }
}

if (count($namesTagged) > 0)
{
	//Show when I tag a user in a photo or video - on social sites

  if( $type == "P" && empty($info) )
  {
		$x = sql_query("select photos.uid,aid,jmp,title,descr from photos join albums on albums.id=photos.aid where photos.id=$id");
		$info = mysql_fetch_array($x, MYSQL_ASSOC);
    $table = "photos";
  }
  else
  {
 		$x = sql_query("select uid,title,descr,jmp from videos where id=$id");
  	$info = mysql_fetch_array($x, MYSQL_ASSOC);
    $table = "videos";
  }

  if( $info['jmp'] == "" )
  {
    $jmp = shortenMediaURL($type, $id);
    sql_query( "update $table set jmp='$jmp' where id=$id" );
    $info['jmp'] = $jmp;
  }

  $info['id'] = $id;


	$info['type'] = $type;
	$info['namesTagged'] = array_unique($namesTagged);
	$err = $API->sendNotification(NOTIFY_ITAG, $info);

	//Show when a user adds a tag to my media - on social sites
  $info['uid'] = $API->getMediaOwner( $id, $type );

	if ($info['uid'] != $API->uid) //If the owner of the media is not the person who made the post...
	{
//  	$API->sendNotification(NOTIFY_ADDTAG_SOC, $info);
  }
}

if( $err == "" ) $err = "OK";
echo $err;
?>