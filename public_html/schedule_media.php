<?php

include "inc/inc.php";

if ($API->admin != 1) die();

if ($_POST['update'] == "1")
{
	foreach ($_POST as $k => $v)
	{
		$k = explode("-", $k);
		if (count($k) != 3) continue;
		
		$update[$k[0] . $k[1]][$k[2]] = $v;
	}
	
	foreach ($update as $id => $arr)
	{
		$type = addslashes(substr($id, 0, 1));
		$id = intval(substr($id, 1));
		$date = intval($arr['y']) . "-" . intval($arr['m']) . "-" . intval($arr['d']) . " " . intval($arr['h']) . ":00:00";
		mysql_query("update scheduled set ts='$date' where type='$type' and id='$id'");
	}
	
	header("Location: /schedule_media.php"); die();
}
elseif (isset($_GET['id']))
{
	$id = intval($_GET['id']);
	
	if ($_GET['type'] == "V")
		$type = "V";
	else
		$type = "P";
	
	if ($_GET['unschedule'] == "1")
	{
		mysql_query("delete from scheduled where type='$type' and id=$id");
		if ($type == "V")
			mysql_query("update videos set privacy=" . PRIVACY_EVERYONE . " where id=$id");
		else
			mysql_query("update albums set privacy=" . PRIVACY_EVERYONE . " where id=" . quickQuery("select aid from photos where id=$id"));
	}
	else
	{
		mysql_query("insert into scheduled (type,id,ts) values ('$type',$id,'2013-" . date("m-d") . "')");
		if ($type == "V")
			mysql_query("update videos set privacy=" . PRIVACY_SELF . " where id=$id");
		else
			mysql_query("update albums set privacy=" . PRIVACY_SELF . " where id=" . quickQuery("select aid from photos where id=$id"));
	}
	
	header("Location: /schedule_media.php"); die();
}

include "header.php";

$q1 = "select 'V' as type,0 as aid,ts,videos.created,scheduled.id,hash,title,descr from videos inner join scheduled where scheduled.id=videos.id";
$q2 = "select 'P' as type,aid,ts,photos.created,scheduled.id,hash,title,descr from photos inner join albums on albums.id=photos.aid inner join scheduled where scheduled.id=photos.id";

$x = mysql_query("select * from ($q1) as t1 union select * from ($q2) as t2 order by ts");

?>

<form action="/schedule_media.php?<?=$hash?>" method="post">

<input type="hidden" name="update" value="1" />

<?php

while ($media = mysql_fetch_array($x, MYSQL_ASSOC))
{
	$d = explode("-", date("m-d-Y-H", strtotime($media['ts'])));
	?>
	<b>Schedule the following for:</b> &nbsp;
	<select name="<?=$media['type']?>-<?=$media['id']?>-m"><?php for ($i = 1; $i <= 12; $i++) echo '<option ' . (intval($d[0]) == $i ? "selected" : "") . ' value="' . $i . '">' . $i . '</option>'; ?></select> /
	<select name="<?=$media['type']?>-<?=$media['id']?>-d"><?php for ($i = 1; $i <= 31; $i++) echo '<option ' . (intval($d[1]) == $i ? "selected" : "") . ' value="' . $i . '">' . $i . '</option>'; ?></select> /
	<select name="<?=$media['type']?>-<?=$media['id']?>-y"><?php for ($i = date("Y"); $i <= date("Y") + 3; $i++) echo '<option ' . (intval($d[2]) == $i ? "selected" : "") . ' value="' . $i . '">' . $i . '</option>'; ?></select>
	&nbsp;at&nbsp;
	<select name="<?=$media['type']?>-<?=$media['id']?>-h"><?php for ($i = 0; $i <= 23; $i++) echo '<option ' . (intval($d[3]) == $i ? "selected" : "") . ' value="' . $i . '">' . $i . '</option>'; ?></select>:00<br />
	or <a href="/schedule_media.php?unschedule=1&type=<?=$media['type']?>&id=<?=$media['id']?>">unschedule</a>
	<?php
	showMediaPreview($media['type'], $media);
}

?>

<input type="submit" value="Update schedule" class="button" />

</form>

<?php include "footer.php"; ?>