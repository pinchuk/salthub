<?php
/*
This is a more specific search than search_s.php.  Searches through a certain type of content based on given search parameters.
Displays a verbose set of results.
*/

$scripts[] = "/mod_media.js";
$background = "#fff";
include_once "inc/inc.php";

$pg = intval($_GET['p']);
$q = trim($_GET['q']);

if (isset($_GET['cat']) && $_GET['cat'] != "")
	$cat = intval($_GET['cat']);

if ($site == "m")
	$types = array("V", "P", "U");
else
	$types = array("V", "P", "U", "B", "c", "C", "g", "G");

if (in_array($_GET['t'], $types))
	$type = $_GET['t'];
else
	$type = "V";

$order = "media.created desc";

if (isset($_GET['o']))
{
	$showPic = true; //show output as picture vs entire album (does not affect video)
	$o = $_GET['o'];

	switch ($o)
	{
		case "v":
		$order = "views desc";
		break;

		case "of":
    $uids = $API->getFriendsUids();
    $friends = "";
    for( $c = 0; $c < sizeof( $uids ); $c++ )
    {
      if( $c > 0 ) $friends .= ",";
      $friends .= $uids[$c];
    }

		$join = "inner join (select {$type}id as mtagid,group_concat(if(eid is null,uid,eid)) as uids_tagged from " . typeToWord($type) . "_tags natural left join contacts where if(cid=0," . SITE_OURS . ",site)=" . SITE_OURS . " and if(eid is null,uid,eid) in ( $friends ) group by mtagid) as tmp on media.id=mtagid";
		$page = "group by media.id";
		$title = ucwords(typeToWord($type)) . "s of Friends";
		break;

		case "bf":
      $uids = $API->getFriendsUids();

  		$where = "media.uid in (";
      for( $c = 0; $c < sizeof( $uids ); $c++ )
      {
        if( $c > 0 ) $where .= ",";
        $where .= $uids[$c];
      }

      if( sizeof( $uids ) == 0 )
        $where .= "0";

      $where .= ") and";


		$title = ucwords(typeToWord($type)) . "s by Friends";
		break;

		default:
		case "r":
		$o = "r";
		$order = "media.created desc";
		break;
	}
}
else
	$showPic = false;
$params = explode(' ', $_GET['q']);
if ($type == "P")
{ 
	$perPage = 10;
  if( $site != "m" )
  {
      $photo_ar = explode(' ', $_GET['q']);
      foreach ($photo_ar as $value){
          $photos[] = "(MEDIA2.ptitle like '%$value%' or MEDIA2.pdescr like '%$value%')";
      }

      $album_ar = explode(' ', $_GET['q']);
      foreach ($album_ar as $value){
          $albums[] = "(ALBUMS.title like '%$value%' or ALBUMS.descr like '%$value%')";
      }

      $contacts_ar = explode(' ', $_GET['q']);
      foreach ($contacts_ar as $value){
          $contacts[] = "(contacts.name like '%$value%')";
      }

      $users_ar = explode(' ', $_GET['q']);
      foreach ($users_ar as $value){
          $users[] = "(users.name like '%$value%')";
      }

      //$where .= "(ptitle like '%$q%' OR title like '%$q%' OR descr like '%$q%') and";
//      $where .= "(title like '%$q%') and";
/*
		$mediaQuery = "select SQL_CALC_FOUND_ROWS media.id,ifnull(ptitle,title) as title,ifnull(pdescr,descr) as descr,hash,aid,media.created from photos as media inner join albums on albums.id=media.aid $join
                    where $where privacy=0 and reported=0 and " . (isset($cat) ? "media.cat='$cat' and " : "") . " media.uid!=832";

*/
    $innerQuery = "";

    if( $q != "" )
    {
//      $innerQuery = "(
//    SELECT
//        MEDIA2.id
//    FROM
//    photos as MEDIA2
//        INNER JOIN
//    albums AS ALBUMS ON MEDIA2.aid = ALBUMS.id AND MEDIA2.uid != 832 AND MEDIA2.privacy = 0 AND MEDIA2.reported = 0
//        INNER JOIN
//    (SELECT
//        MEDIA2.id
//    FROM
//        photos AS MEDIA2
//    WHERE
//        ( MEDIA2.ptitle LIKE '%$q%' OR MEDIA2.pdescr LIKE '%$q%') and MEDIA2.uid != 832 and MEDIA2.aid != 884372 AND MEDIA2.aid!=70267 ) MEDIA2_TEXT_SEARCH ON MEDIA2.id = MEDIA2_TEXT_SEARCH.id
//UNION
//    SELECT
//        MEDIA2.id
//    FROM
//    photos as MEDIA2
//        INNER JOIN
//    albums AS ALBUMS ON MEDIA2.aid = ALBUMS.id AND MEDIA2.uid != 832 AND MEDIA2.privacy = 0 AND MEDIA2.reported = 0
//        INNER JOIN
//    (SELECT
//        ALBUMS.id
//    FROM
//        albums AS ALBUMS
//    WHERE
//        ALBUMS.uid != 832 and ALBUMS.id != 884372 AND ALBUMS.id!=70267 AND (ALBUMS.title LIKE '%$q%' OR ALBUMS.descr LIKE '%$q%')) ALBUM_TEXT_SEARCH ON MEDIA2.aid = ALBUM_TEXT_SEARCH.id
//UNION
//  SELECT
//      pid as id
//  FROM
//      photo_tags
//          INNER JOIN
//      (SELECT
//          contacts.cid
//      FROM
//          contacts
//      WHERE
//          contacts.site = 2 AND contacts.name LIKE '%$q%') USERS ON USERS.cid = photo_tags.cid
//UNION
//  SELECT
//      pid as id
//  FROM
//      photo_tags
//          INNER JOIN
//      (SELECT
//          users.uid
//      FROM
//          users
//      WHERE
//          users.name LIKE '%$q%') USERS2 ON USERS2.uid = photo_tags.uid WHERE photo_tags.cid=0
//) SEARCH_RESULTS,";
//      $where .= "media.id = SEARCH_RESULTS.id AND ";
        $innerQuery = "(
    SELECT
        MEDIA2.id
    FROM
    photos as MEDIA2
        INNER JOIN
    albums AS ALBUMS ON MEDIA2.aid = ALBUMS.id AND MEDIA2.uid != 832 AND MEDIA2.privacy = 0 AND MEDIA2.reported = 0
        INNER JOIN
    (SELECT
        MEDIA2.id
    FROM
        photos AS MEDIA2
    WHERE
        (".implode(' and', $photos).") and MEDIA2.uid != 832 and MEDIA2.aid != 884372 AND MEDIA2.aid!=70267 ) MEDIA2_TEXT_SEARCH ON MEDIA2.id = MEDIA2_TEXT_SEARCH.id
UNION
    SELECT
        MEDIA2.id
    FROM
    photos as MEDIA2
        INNER JOIN
    albums AS ALBUMS ON MEDIA2.aid = ALBUMS.id AND MEDIA2.uid != 832 AND MEDIA2.privacy = 0 AND MEDIA2.reported = 0
        INNER JOIN
    (SELECT
        ALBUMS.id
    FROM
        albums AS ALBUMS
    WHERE
        ALBUMS.uid != 832 and ALBUMS.id != 884372 AND ALBUMS.id!=70267 AND (".implode(' and', $albums).")) ALBUM_TEXT_SEARCH ON MEDIA2.aid = ALBUM_TEXT_SEARCH.id
UNION
  SELECT
      pid as id
  FROM
      photo_tags
          INNER JOIN
      (SELECT
          contacts.cid
      FROM
          contacts
      WHERE
          contacts.site = 2 AND (".implode(' and', $contacts).")) USERS ON USERS.cid = photo_tags.cid
UNION
  SELECT
      pid as id
  FROM
      photo_tags
          INNER JOIN
      (SELECT
          users.uid
      FROM
          users
      WHERE
          (".implode(' and', $users).")) USERS2 ON USERS2.uid = photo_tags.uid WHERE photo_tags.cid=0
) SEARCH_RESULTS,";
        $where .= "media.id = SEARCH_RESULTS.id AND ";
    }


    //If you do not specify a search string here, the query can take a long time as it must sort the entire photo database first. 

    $mediaQuery = "SELECT SQL_CALC_FOUND_ROWS SQL_NO_CACHE IFNULL(media.ptitle, albums.title) as title, IFNULL(media.pdescr, albums.descr) as descr, media.hash, media.aid, media.id, media.created FROM " .
      $innerQuery . " photos as media $join, albums WHERE $where media.aid!=884372 AND media.aid!=70267 and albums.id = media.aid";

    if( $cat != "" ) $mediaQuery .= " AND media.cat=$cat";

    $mediaQuery .= " $page order by $order limit " . ($pg * $perPage) . ",$perPage";

    
    $mediaQuery = sql_query( $mediaQuery );

    $q2 = mysql_query( "SELECT FOUND_ROWS();" );
    $rows = current( mysql_fetch_array( $q2 ) );
    $pages = ceil( $rows / $perPage);

  }
  else
  {
      $photos = array();
      foreach ($params as $value){
          $photos[] = "(title like '%$value%' or descr like '%$value%')";
      }
  	$perPage = 10;
  	$mediaQuery = sql_query("select @type:='$type',views,pic,twusername,fbid,users.uid,name,username,albums.id as aid,albums.title,albums.descr,media.created,hash,media.id,mainImage" . ($site == "s" ? ",ptitle,pdescr" : "") . " from albums inner join users on albums.uid=users.uid inner join photos as media on " . ($showPic ? "albums.id=media.aid" : "mainImage=media.id") . " $join where " . $API->getPrivacyQuery() . " $where active=1 and " . (isset($cat) ? "$cat in (select cat from photos p where p.aid=albums.id) and " : "") . "(" . implode(" and ", $photos) . ") $page order by $order limit " . ($pg * $perPage) . ",$perPage");
  	$pages = ceil(mysql_num_rows($mediaQuery) / $perPage);

  	$title = empty($title) ? empty($q) ? "All Photos" : "Photo Results for &quot;$q&quot;" : $title;
  }

//  echo "select @type:='$type',views,pic,twusername,fbid,users.uid,name,username,albums.id as aid,albums.title,albums.descr,media.created,hash,media.id,mainImage" . ($site == "s" ? ",ptitle,pdescr" : "") . " from albums inner join users on albums.uid=users.uid inner join photos as media on " . ($showPic ? "albums.id=media.aid" : "mainImage=media.id") . " $join where " . $API->getPrivacyQuery() . " $where active=1 and " . (isset($cat) ? "$cat in (select cat from photos p where p.aid=albums.id) and " : "") . "(title like '%$q%' or descr like '%$q%') $page order by $order limit " . ($pg * $perPage) . ",$perPage";

	$title = empty($title) ? empty($q) ? "All Photos" : "Photo Results for &quot;$q&quot;" : $title;
}
elseif ($type == "U")
{
	$perPage = 10;
	$pages = ceil(quickQuery("select @type:='$type',count(*) from users where active=1 and (username like '%$q%' or name like '%$q%' or contactfor like '%$q%') and uid!=832") / $perPage);
	//$mediaQuery = sql_query("select @type:='$type',uid,name,username,pic,twusername,fbid,contactfor,occupation,sector from users where active=1 and (username like '%$q%' or name like '%$q%' or contactfor like '%$q%') and uid!=832 order by contactfor desc, joined desc limit " . ($pg * $perPage) . ",$perPage");
//    $mediaQuery = sql_query("SELECT users.uid, users.name, users.username, users.pic, users.twusername, users.fbid, users.contactfor, users.occupation, users.sector FROM users
//                             LEFT JOIN `work` ON `work`.uid = users.uid
//                             LEFT JOIN pages ON pages.gid = work.occupation
//                             WHERE (pages.gname LIKE '%$q%' OR users.name LIKE '%$q%' OR users.contactfor LIKE '%$q%' OR users.username LIKE '%$q%') AND users.active=1 and users.uid not like $API->uid
//                             GROUP BY users.uid ORDER BY lastlogin DESC LIMIT " . ($pg * $perPage) . ",$perPage");
    foreach ($params as $value){
        $users[] = "(pages.gname like '%$value%' or users.name like '%$value%' or users.company like '%$value%' or users.occupation like '%$value%' or users.contactfor like '%$value%' or users.username like '%$value%')";
    }

    $mediaQuery = sql_query("SELECT users.uid, users.name, users.username, users.pic, users.twusername, users.fbid, users.contactfor, users.occupation, users.sector FROM users
                             LEFT JOIN `work` ON `work`.uid = users.uid
                             LEFT JOIN pages ON pages.gid = work.occupation
                             WHERE (" . implode(" and ", $users) . ") AND users.active=1 and users.uid not like $API->uid
                             GROUP BY users.uid ORDER BY lastlogin DESC LIMIT " . ($pg * $perPage) . ",$perPage");
	$title = empty($title) ? empty($q) ? "All Users" : "User Results for &quot;$q&quot;" : $title;
}
elseif ($type == "B") // boats/vessels
{
	$perPage = 10;

//	$fields = array("shipyard", "shiptype", "country", "navalarchitect", "stylist", "decorator", "hullid", "engine", "speedcruise", "ex", "descr", "gname");
//	$fields = array("flag", "callsign", "descr", "gname", "name", "exnames","imo");
//	foreach ($fields as $f)
//		$wheres[] = "$f like '%$q%'";

    foreach ($params as $value){
        $vessels[] = "(exnames like '%$value%' or gname like '%$value%' or products like '%$value%' or contact_person like '%$value%' or contactfor like '%$value%')";
    }

    $where = "g.type=" . PAGE_TYPE_VESSEL . " and active=1 and g.privacy=" . PRIVACY_EVERYONE . " and (" . implode(" and ", $vessels) . ")";
//    $pages = ceil(quickQuery("select count(*) from pages left join boats_ex ex on glink=ex.id inner join boats b on glink=b.id where $where") / $perPage);
//    $query = "select if(gname = '$q',1,0) as exactMatch,pages.gid,gname,pages.pid,hash,length,group_concat(ex separator ', ') as ex,imo, flag as country, exnames from pages left join boats_ex on boats_ex.id=pages.glink left join photos on photos.id=pages.pid inner join boats on boats.id=glink where $where group by glink order by exactMatch desc,gname limit " . ($pg * $perPage) . ",$perPage";
    $pages = ceil(quickQuery("select count(*) from pages g left join boats_ex ex on ex.id=glink left join photos on photos.id=g.pid inner join boats as boats on boats.id=glink where $where") / $perPage);
    $query = "select g.gid,gname,g.pid,hash,length,exnames from pages g left join boats_ex ex on ex.id=glink left join photos on photos.id=g.pid inner join boats as boats on boats.id=glink where $where limit " . ($pg * $perPage) . ", $perPage";

    $mediaQuery = sql_query($query);

  if ($isDevServer) echo mysql_error();
	$title = empty($title) ? empty($q) ? "All Vessels" : "Vessel Results for &quot;$q&quot;" : $title;
}
elseif ($type == "C") // comments/log entries
{
	$perPage = 10;
	$pages = ceil(quickQuery("select count(*) from (" . getLogEntrySearchQuery($q) . ") as tmp") / $perPage);
	$mediaQuery = sql_query(getLogEntrySearchQuery($q) . "limit " . ($pg * $perPage) . ",$perPage");

	$title = empty($title) ? empty($q) ? "All Comments and Log Entries" : "Comments and Log Entries for &quot;$q&quot;" : $title;
}
elseif ($type == "c") // work pages
{
    $works = array();
    foreach ($params as $value){
        $works[] = "(gname like '%$value%' or products like '%$value%' or catname LIKE '%$value%' or contact_person like '%$value%' or g.contactfor like '%$value%')";
    }

	$perPage = 10;
//	$where = "g.type=" . PAGE_TYPE_BUSINESS . " and g.active=1 and g.privacy=" . PRIVACY_EVERYONE . " and (gname like '%$q%' or products like '%$q%' or contact_person like '%$q%' or catname like '%$q%' or g.contactfor like '%$q%')";
	$where = "g.type=" . PAGE_TYPE_BUSINESS . " and g.active=1 and g.privacy=" . PRIVACY_EVERYONE . " and (" . implode(" and ", $works) . ")";
	$pages = ceil(quickQuery("select count(*) from pages g left join categories on categories.cat=g.subcat where $where") / $perPage);

	$mediaQuery = sql_query("select g.gid,gname,g.pid,hash,g.descr,if(m.uid={$API->uid},1,0) as ismember from pages g left join categories on categories.cat=g.subcat left join page_members m on g.gid=m.gid and m.uid={$API->uid} left join photos on photos.id=g.pid where g.privacy=" . PRIVACY_EVERYONE . " and $where limit " . ($pg * $perPage) . ",$perPage");

	$title = empty($title) ? empty($q) ? "All Work Pages" : "Work Page Results for &quot;$q&quot;" : $title;
}
elseif ($type == "g") // other pages
{
    $pages = array();
    foreach ($params as $value){
        $pages[] = "(g.gname like '%$value%' or g.descr like '%$value%' or g.contactfor like '%$value%' or products like '%$value%' or contact_person like '%$value%')";
    }

	$perPage = 10;
//	$where = "g.type!=" . PAGE_TYPE_BUSINESS. " and g.type != " . PAGE_TYPE_VESSEL . " and g.active=1 and (g.gname like '%$q%' or g.descr like '%$q%' or g.contactfor like '%$q%')";
	$where = "g.type!=" . PAGE_TYPE_BUSINESS. " and g.type != " . PAGE_TYPE_VESSEL . " and g.active=1 and (" . implode(" and ", $pages) . ")";
	$pages = ceil(quickQuery("select count(*) from pages g where $where") / $perPage);

	$mediaQuery = sql_query("select g.gid,gname,g.pid,hash,g.descr,if(m.uid={$API->uid},1,0) as ismember from pages g left join page_members m on g.gid=m.gid and m.uid={$API->uid} left join photos on photos.id=g.pid where g.privacy=" . PRIVACY_EVERYONE . " and $where limit " . ($pg * $perPage) . ",$perPage");

	$title = empty($title) ? empty($q) ? "All Pages" : "Page Results for &quot;$q&quot;" : $title;
}
elseif ($type == "G") // All pages
{
	$perPage = 10;
    $pages = array();
    foreach ($params as $value){
        $pages[] = "(g.gname like '%$value%' or g.descr like '%$value%' or g.contactfor like '%$value%')";
    }
	$where = "g.active=1 and (" . implode(" and ", $pages) . ") and g.type!=" . PAGE_TYPE_VESSEL;
	$pages = ceil(quickQuery("select count(*) from pages g where $where") / $perPage);

	$mediaQuery = sql_query("select g.gid,gname,g.pid,hash,g.descr from pages g left join photos on photos.id=g.pid where $where order by gname limit " . ($pg * $perPage) . ",$perPage");

	$title = empty($title) ? empty($q) ? "All Pages" : "Page Results for &quot;$q&quot;" : $title;
}
else // type == "V"
{
	$perPage = 10;

  if( $site != "m" )
  {
      $ar = explode(' ', $q);
      $videos = array();
      foreach ($ar as $value){
          $videos[] = "(media.title like '%$value%' or media.descr like '%$value%')";
      }
//	  $pages = ceil(quickQuery("select count(*) from videos as media $join inner join users on media.uid=users.uid where" . $API->getPrivacyQuery() . (isset($cat) ? "cat=$cat and " : "") . "$where active=1 and ready=1  and media.uid!=832 and (title like '%$q%' or descr like '%$q%' or id IN (select distinct videos.id from videos left join video_tags on video_tags.vid=videos.id left join contacts on contacts.cid=video_tags.cid left join users on IF(video_tags.cid=0,video_tags.uid,contacts.uid) = users.uid where users.name like '%$q%')) $page") / $perPage);
//  	$mediaQuery = sql_query("select pic,twusername,fbid,users.uid,name,username,id,hash,views,title,descr,created,media.jmp from videos as media $join inner join users on media.uid=users.uid where" . $API->getPrivacyQuery() . (isset($cat) ? "cat=$cat and " : "") . "$where active=1 and ready=1 and (title like '%$q%' or descr like '%$q%' or id IN (select distinct videos.id from videos left join video_tags on video_tags.vid=videos.id left join contacts on contacts.cid=video_tags.cid left join users on IF(video_tags.cid=0,video_tags.uid,contacts.uid) = users.uid where users.name like '%$q%')) $page order by $order limit " . ($pg * $perPage) . ",$perPage");
    if( $q != '' )
//      $where .= "media.title like '%$q%' and";
      $where .= "(".implode(' and', $videos).") and";

//      $pages = ceil(quickQuery("select count(*) from videos as media $join inner join users on media.uid=users.uid where" . $API->getPrivacyQuery(null, true, "", false) . (isset($cat) ? "cat=$cat and " : "") . "$where active=1 and ready=1 $page") / $perPage);
//      $mediaQuery = sql_query("select pic,twusername,fbid,users.uid,name,username,id,hash,views,title,descr,created,media.jmp from videos as media $join inner join users on media.uid=users.uid where" . $API->getPrivacyQuery(null, true, "", false) . (isset($cat) ? "cat=$cat and " : "") . "$where active=1 and ready=1 $page group by media.hash order by $order limit " . ($pg * $perPage) . ",$perPage");
      $pages = ceil(quickQuery("select count(*) from videos as media where privacy=0 and reported=0 and (".implode(' and', $videos).")") / $perPage);
      $video_query = "select id,title,descr,hash from videos as media where privacy=0 and reported=0 and (".implode(' and', $videos).") limit ".($pg*$perPage).", $perPage";
      $mediaQuery = sql_query($video_query);
  }
  else
  {
  	$pages = ceil(quickQuery("select count(*) from videos as media $join inner join users on media.uid=users.uid where active=1 and ready=1 and (title like '%$q%' or descr like '%$q%')") / $perPage);
  	$mediaQuery = sql_query("select pic,twusername,fbid,users.uid,name,username,id,hash,views,title,descr,created,media.jmp from videos as media inner join users on media.uid=users.uid where active=1 and ready=1 and (title like '%$q%' or descr like '%$q%') limit " . ($pg * $perPage) . ",$perPage");
  }


if( mysql_error() ) echo mysql_error();

	$title = empty($title) ? empty($q) ? "All Videos" : "Video Results for &quot;$q&quot;" : $title;
}

if ($type != "U")
{
	$scripts[] = "/showembedmedia.js";
	$scripts[] = "http://s7.addthis.com/js/250/addthis_widget.js#username=mediabirdy";
	$scripts[] = "/email.js";
}

$tmp = $title;
include "header.php";
$title = $tmp . ($cat ? " in " . quickQuery("select catname from categories where cat=$cat") : "");

?>

<?php if ($site == "s") { ?>
<div class="bigtext2" style="clear: both; padding: 0 0 5px 5px;"><?=$title?></div>
<div class="contentborder">
<?php } ?>
<div class="boxleftNoPadding" style="float: left;">
	<?php if ($site == "m") showDidYouKnow(); ?>
	<div style="padding-left: <?=$site == "m" ? 7 : 0?>px;">
		<?php if ($site == "m") { ?>
		<div class="biggertext" style="color: #555;">
			<?=$title?>
		</div>
		<?php } ?>
		<div style="padding: <?=$site == "m" ? 5 : 0?>px 0 5px 0; width: 556px; border-bottom: 1px solid #d8dfea;">
			<?php if ($site == "s" && in_array($type, array("V", "P"))) { ?>
			<div style="float: left;"><img src="/images/application_view_list.png" alt="" /></div>
			<div style="float: left; font-size: 9pt; position: relative; padding-right: 15px; margin-right: 10px; border-right: 1px solid #d8dfea;">
				<div style="cursor: default;" onmouseover="javascript:if (ddTimer) clearTimeout(ddTimer); document.getElementById('view-dd').style.display = '';" onmouseout="javascript:ddTimer = setTimeout('document.getElementById(\'view-dd\').style.display=\'none\';', 200);">
					<div style="width: 45px; padding-left: 3px; float: left;"><span id="view-chosen">View &#0133;</span></div> &nbsp;<img src="/images/down.png" alt="" />
					<div style="display: none;" class="dropdown viewdd" id="view-dd">
						<a href="/search_m.php?q=<?=urlencode($q)?>&t=<?=$type?>&cat=<?=$cat?>&o=r">Most Recent</a><br />
						<a href="/search_m.php?q=<?=urlencode($q)?>&t=<?=$type?>&cat=<?=$cat?>&o=v">Most Viewed</a><br />
						<a href="/search_m.php?q=<?=urlencode($q)?>&t=<?=$type?>&cat=<?=$cat?>&o=of">Of Friends</a><br />
						<a href="/search_m.php?q=<?=urlencode($q)?>&t=<?=$type?>&cat=<?=$cat?>&o=bf">By Friends</a>
					</div>
				</div>
			</div>
			<?php
			}
			foreach (array(array("V", "video", "television"), array("P", "photo", "images") /*, array("U", "user", "page") */) as $opt)
			{
				if ($opt[0] == $type) continue;
			?>
			<div class="subtitlepic smtitle" style="float: left; padding-right: 20px; background-image: url(/images/<?=$opt[2]?>.png); font-weight: normal;">
				<a href="/search_m.php?q=<?=urlencode($q)?>&t=<?=$opt[0]?>&cat=<?=$cat?>&o=<?=$o?>">see <?=$opt[1]?> results</a>
			</div>
			<?php
			}
			?>
			<div style="clear: both;"></div>
		</div>
	</div>
	
	<div style="padding: 20px 0 0 3px;">
<?php

if (mysql_num_rows($mediaQuery) == 0)
{
	?>
	<div class="noresults">
		No results found.
	</div>
	<?php
}
else
{
	switch ($type)
	{
		case "C":
		while ($result = mysql_fetch_array($mediaQuery, MYSQL_ASSOC))
			showLogEntrySearchResult($result, $q);
		break;

    case "G":
    case "g":
		case "c":
		while ($page = mysql_fetch_array($mediaQuery, MYSQL_ASSOC))
			showPageResult($page);
		break;
		
		case "B":
		while ($boat = mysql_fetch_array($mediaQuery, MYSQL_ASSOC))
			showBoatResult($boat);
		break;

		case "U":
		while ($user = mysql_fetch_array($mediaQuery, MYSQL_ASSOC))
			showUserResult($user);
		break;
		
		default:
		while ($media = mysql_fetch_array($mediaQuery, MYSQL_ASSOC))
			showMediaPreview($type, $media, false, !$showPic);
		break;
	}
}
?>
	</div>

    <? if ($pages > 1){ ?>
	<div style="padding-right: 30px; padding-top:30px;">
		<?php $page = $pg; include "inc/pagination.php"; ?>
	</div>
    <? } ?>

<?php
if ($site == "s")
{
	?>
		</div>
		<div style="float: right; width: 300px;">
			<?php showCompleteProfileInformationWide() ?>
<? if( $API->adv ) { ?>
			<div class="subhead parent_create_ad" style="margin-top: 10px;">
                <div style="float:left;">Sponsors</div>
                <div class="create_ad link" style="float:right;"><a href="http://www.salthub.com/adv/create.php" style="font-size:8pt; font-weight:300; color:rgb(0, 64, 128);">create an ad</a>&nbsp;</div>
                <div style="clear:both;"></div>
            </div>
			<div style="margin: 5px 0 10px;"><? showAd("companion"); ?></div>
<? } ?>
			<?php
			if ($type == "c" || $type == "G" || $type=="g" )
			{
				unset($type);
				include "pages/gyml.php";
				echo '<div style="clear: both; height: 10px;"></div>';
			}
			else
				include "inc/connect.php";
			?>
<?  if( $API->adv ) { ?>
			<div class="subhead parent_create_ad">
                <div style="float:left;">Sponsors</div>
                <div class="create_ad link" style="float:right;"><a href="http://www.salthub.com/adv/create.php" style="font-size:8pt; font-weight:300; color:rgb(0, 64, 128);">create an ad</a>&nbsp;</div>
                <div style="clear:both;"></div>
            </div>
			<div style="margin: 5px 0 0;"><? showAd("companion"); ?></div>
<? } ?>
			<?php include "inc/pymk.php"; ?>
		</div>
		<div style="clear: both;"></div>
	</div>
<?php
}
?>

<script language="javascript" type="text/javascript">
<!--
var searchType = "<?=$type?>";
//-->
</script>

<?php

include "inc/embedmedia.php";

if ($site == "m")
	include "settings/footer.php";
else
	include "footer.php";

?>
