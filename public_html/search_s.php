<?php

/*
Change Log

8/24/2011 - Removed the search of tags from the video and photo searches to improve speed.  Old code is commented out.


*/

include "header.php";

$params['q'] = $_GET['q'];
$params['t'] = substr($_GET['t'], 0, 1);

?>

<div class="bigtext2" style="clear: both; padding: 0 0 5px 5px;">
	Results for &quot;<?=$params['q']?>&quot;
</div>

<div class="contentborder">
	<div style="float: left; width: 634px;">
		<?php

		foreach (array("V", "P") as $type)
		{
			?>
			<div class="searchhead">
				<a href="/search_m.php?<?=getParams(array("t" => $type))?>"><?=ucwords(typeToWord($type))?>s for &quot;<?=$params['q']?>&quot;</a>
			</div>

				<?php

				if ($type == "V"){
                    $ar = explode(' ', $_GET['q']);
                    foreach ($ar as $value){
                        $videos[] = "(title like '%$value%' or descr like '%$value%')";
                    }
//					$q = "select id,title,descr,hash from videos as media where privacy=0 and reported=0 and (title like '%{$params['q']}%' or descr like '%{$params['q']}%') limit 5";
					$q = "select id,title,descr,hash from videos as media where privacy=0 and reported=0 and (".implode(' and', $videos).") limit 5";
                }
				else
        {
//					$q = "SELECT SQL_NO_CACHE media.id, SEARCH_RESULTS.*, IFNULL(media.ptitle, albums.title) as title, IFNULL(media.pdescr, albums.descr) as descr, media.hash, media.aid, media.created FROM (
//SELECT
//        MEDIA2.id
//    FROM
//    photos as MEDIA2
//        INNER JOIN
//    albums AS ALBUMS ON MEDIA2.aid = ALBUMS.id AND MEDIA2.uid != 832 AND MEDIA2.privacy = 0 AND MEDIA2.reported = 0
//        INNER JOIN
//    (SELECT
//        MEDIA2.id
//    FROM
//        photos AS MEDIA2
//    WHERE
//        MEDIA2.ptitle LIKE '%{$params['q']}%' OR MEDIA2.pdescr LIKE '%{$params['q']}%') MEDIA2_TEXT_SEARCH ON MEDIA2.id = MEDIA2_TEXT_SEARCH.id
//UNION
//
//    SELECT
//        MEDIA2.id
//    FROM
//    photos as MEDIA2
//        INNER JOIN
//    albums AS ALBUMS ON MEDIA2.aid = ALBUMS.id AND MEDIA2.uid != 832 AND MEDIA2.privacy = 0 AND MEDIA2.reported = 0
//        INNER JOIN
//    (SELECT
//        ALBUMS.id
//    FROM
//        albums AS ALBUMS
//    WHERE
//        ALBUMS.title LIKE '%{$params['q']}%' OR ALBUMS.descr LIKE '%{$params['q']}%') ALBUM_TEXT_SEARCH ON MEDIA2.aid = ALBUM_TEXT_SEARCH.id
//UNION
//  SELECT
//      pid as id
//  FROM
//      photo_tags
//          INNER JOIN
//      (SELECT
//          contacts.cid
//      FROM
//          contacts
//      WHERE
//          contacts.site = 2 AND contacts.name LIKE '%{$params['q']}%') USERS ON USERS.cid = photo_tags.cid
//UNION
//  SELECT
//      pid as id
//  FROM
//      photo_tags
//          INNER JOIN
//      (SELECT
//          users.uid
//      FROM
//          users
//      WHERE
//          users.name LIKE '%{$params['q']}%') USERS2 ON USERS2.uid = photo_tags.uid WHERE photo_tags.cid=0
//
//
//        ) SEARCH_RESULTS, photos as media, albums WHERE media.id = SEARCH_RESULTS.id AND albums.id = media.aid order by media.created desc limit 5";

//select media.id,ifnull(ptitle,title) as title,ifnull(pdescr,descr) as descr,hash from photos as media inner join albums on albums.id=media.aid where privacy=0 and reported=0 and
//          (ptitle like '%{$params['q']}%' OR title like '%{$params['q']}%' OR descr like '%{$params['q']}%')";
$photo_ar = explode(' ', $_GET['q']);
foreach ($photo_ar as $value){
    $photos[] = "(MEDIA2.ptitle like '%$value%' or MEDIA2.pdescr like '%$value%')";
}

$album_ar = explode(' ', $_GET['q']);
foreach ($album_ar as $value){
    $albums[] = "(ALBUMS.title like '%$value%' or ALBUMS.descr like '%$value%')";
}

$contacts_ar = explode(' ', $_GET['q']);
foreach ($contacts_ar as $value){
    $contacts[] = "(contacts.name like '%$value%')";
}

$users_ar = explode(' ', $_GET['q']);
foreach ($users_ar as $value){
    $users[] = "(users.name like '%$value%')";
}
                    $q = "SELECT SQL_NO_CACHE media.id, SEARCH_RESULTS.*, IFNULL(media.ptitle, albums.title) as title, IFNULL(media.pdescr, albums.descr) as descr, media.hash, media.aid, media.created FROM (
SELECT
        MEDIA2.id
    FROM
    photos as MEDIA2
        INNER JOIN
    albums AS ALBUMS ON MEDIA2.aid = ALBUMS.id AND MEDIA2.uid != 832 AND MEDIA2.privacy = 0 AND MEDIA2.reported = 0
        INNER JOIN
    (SELECT
        MEDIA2.id
    FROM
        photos AS MEDIA2
    WHERE
        (".implode(' and', $photos).")) MEDIA2_TEXT_SEARCH ON MEDIA2.id = MEDIA2_TEXT_SEARCH.id
UNION

    SELECT
        MEDIA2.id
    FROM
    photos as MEDIA2
        INNER JOIN
    albums AS ALBUMS ON MEDIA2.aid = ALBUMS.id AND MEDIA2.uid != 832 AND MEDIA2.privacy = 0 AND MEDIA2.reported = 0
        INNER JOIN
    (SELECT
        ALBUMS.id
    FROM
        albums AS ALBUMS
    WHERE
        (".implode(' and', $albums).")) ALBUM_TEXT_SEARCH ON MEDIA2.aid = ALBUM_TEXT_SEARCH.id
UNION
  SELECT
      pid as id
  FROM
      photo_tags
          INNER JOIN
      (SELECT
          contacts.cid
      FROM
          contacts
      WHERE
          contacts.site = 2 AND (".implode(' and', $contacts).")) USERS ON USERS.cid = photo_tags.cid
UNION
  SELECT
      pid as id
  FROM
      photo_tags
          INNER JOIN
      (SELECT
          users.uid
      FROM
          users
      WHERE
          (".implode(' and', $users).")) USERS2 ON USERS2.uid = photo_tags.uid WHERE photo_tags.cid=0


        ) SEARCH_RESULTS, photos as media, albums WHERE media.id = SEARCH_RESULTS.id AND albums.id = media.aid order by media.created desc limit 5";
                }

				$x = sql_query($q);

				if (mysql_num_rows($x) == 0)
					echo '<div class="noresults2">No results found.</div>';
				else
				{
					echo '<div class="searchimages">';
					while ($media = mysql_fetch_array($x, MYSQL_ASSOC))
					{
						echo '<div>';
						echo '<a href="' . $API->getMediaURL($type, $media['id'], $media['title']) . '">';
						echo '<img src="' . $API->getThumbURL(1, 100, 75, "/" . typeToWord($type) . "s/{$media['id']}/{$media['hash']}.jpg") . '" alt="" /><br />';
						echo "<span>{$media['title']}</span>";
						echo '</a>';
						echo '</div>';
						echo '</a>';
					}
					echo '<div style="clear: both;"></div></div>';
				}

				?>

			<?php if (mysql_num_rows($x) > 0) { ?><div class="searchmore"><a href="/search_m.php?<?=getParams(array("t" => $type))?>"><img src="/images/add.png" alt="" />show more <?=typeToWord($type)?>s</a></div><?php } ?>
			<div class="searchdiv"></div>
			<?php
		}
		?>


		<div class="searchhead">
			<a href="/search_m.php?<?=getParams(array("t" => "U"))?>">Connections For &quot;<?=$params['q']?>&quot;</a>
		</div>
		<div style="margin-top: -10px;">
			<?php
			$limit = 8;
			$like = $params['q'];
			ob_start();
			include_once "getpymk.php";
			$html = ob_get_contents();
			ob_end_clean();

			//if ($num_results > 0){
				echo $html;
            //}

			echo '<div style="clear: both; height: 5px;"></div>';

			if ($html == '')
				echo '<div class="noresults2" style="margin-top: 3px;">No results found.</div>';
			?>
		</div>

		<?php if (mysql_num_rows($x) > 0) { ?><div class="searchmore"><a href="/search_m.php?<?=getParams(array("t" => "U"))?>"><img src="/images/add.png" alt="" />show more connections</a></div><?php } ?>
		<div class="searchdiv"></div>

		<div class="searchhead">
			<a href="/search_m.php?<?=getParams(array("t" => "B"))?>">Vessels for &quot;<?=$params['q']?>&quot;</a>
		</div>
		<?php
// Changed by AppDragon
//		$fields = array("shiptype", "flag", "callsign", "imo", "name", "descr", "gname", "exnames");
		$fields = array("shiptype", "flag", "name", "descr", "gname", "exnames");
//		foreach ($fields as $f)
//			$wheres[] = "$f like '%{$params['q']}%'";
        $pages = explode(' ', $_GET['q']);
        $sql2 = array();
        foreach ($pages as $value){
            $sql2[] = "(exnames like '%$value%' or gname like '%$value%' or products like '%$value%' or contact_person like '%$value%' or contactfor like '%$value%')";
        }


// Changed by AppDragon
//		$q = "select g.gid,gname,g.pid,hash,length,exnames from pages g left join boats_ex ex on ex.id=glink left join photos on photos.id=g.pid inner join boats as boats on boats.id=glink where g.privacy=" . PRIVACY_EVERYONE . " and g.active=1 and g.type=" . PAGE_TYPE_VESSEL . " and (" . implode(" or ", $wheres) . ") group by glink limit 5";
		$q = "select g.gid,gname,g.pid,hash,length,exnames from pages g left join boats_ex ex on ex.id=glink left join photos on photos.id=g.pid inner join boats as boats on boats.id=glink where g.privacy=" . PRIVACY_EVERYONE . " and g.active=1 and g.type=" . PAGE_TYPE_VESSEL . " and (" . implode(" and ", $sql2) . ") group by glink limit 5";

    $x = sql_query($q);

		if (mysql_num_rows($x) > 0)
		{
		?>
		<div class="searchimages">
			<?php
			while ($page = mysql_fetch_array($x, MYSQL_ASSOC))
			{
        $image = $API->getPageImage( $page['gid'] );
				echo '<div>';
				echo '<a href="' . $API->getPageURL($page['gid'], $page['gname']) . '">';
				echo '<img src="' . $API->getThumbURL(1, 100, 75, $image) . '" alt="" /><br />';
				echo "<span><b>{$page['gname']}</b><br />{$page['length']} ft. / " . round($page['length'] / 3.2808399, 2) . " m.</span>";
        if( $page['exnames'] != '' )
  				echo "<br /><span>(ex. " . $page['exnames'] . ")</span>";
				echo '</a>';
				echo '</div>';
				echo '</a>';
			}

			?>
			<div style="clear: both;"></div>
		</div>
		<?php
		}
		else
			echo '<div class="noresults2">No results found.</div>';
		?>

		<?php if (mysql_num_rows($x) > 0) { ?><div class="searchmore"><a href="/search_m.php?<?=getParams(array("t" => "B"))?>"><img src="/images/add.png" alt="" />show more vessels</a></div><?php } ?>
		<div class="searchdiv"></div>

		<div class="searchhead">
			<a href="/search_m.php?<?=getParams(array("t" => "C"))?>">Comments and Log Entries for &quot;<?=$params['q']?>&quot;</a>
		</div>

		<?php
		$x = sql_query(getLogEntrySearchQuery($params['q']) . "limit 4");

		if (mysql_num_rows($x) > 0)
			while ($result = mysql_fetch_array($x, MYSQL_ASSOC))
				showLogEntrySearchResult($result, $params['q']);
		else
			echo '<div class="noresults2">No results found.</div>';
		?>

		<?php if (mysql_num_rows($x) > 0) { ?><div class="searchmore"><a href="/search_m.php?<?=getParams(array("t" => "C"))?>"><img src="/images/add.png" alt="" />show more comments and log entries</a></div><?php } ?>
		<div class="searchdiv"></div>

		<div class="searchhead">
			<a href="/search_m.php?<?=getParams(array("t" => "c"))?>">Companies for &quot;<?=$params['q']?>&quot;</a>
		</div>
    <div style="font-size:8pt; padding-bottom:3px;">
      <div style="float:left; margin-right:3px;"><img src="images/exclamation2.png" width="16" height="16" alt="" /></div>
      <div style="float:left;">when searching for a service or specialty, use the <a href="/directory.php">Business Directory</a> for a better experience.</div>
    </div>
    <div style="clear:both; padding-top:3px;"></div>
		<?php
        $pages = explode(' ', $_GET['q']);
        $companies = array();
        foreach ($pages as $value){
            $companies[] = "(gname like '%$value%' or products like '%$value%' or catname like '%$value%' or contact_person like '%$value%' or contactfor like '%$value%')";
        }
//		$q = "select gid,gname,g.pid,hash from pages g left join photos on photos.id=g.pid left join categories on categories.cat=g.subcat where g.privacy=" . PRIVACY_EVERYONE . " and g.active=1 and g.privacy=" . PRIVACY_EVERYONE . " and g.type=" . PAGE_TYPE_BUSINESS . " and (gname like '%{$params['q']}%' or products like '%{$params['q']}%' or contact_person like '%{$params['q']}%' or catname like '%{$params['q']}%' or contactfor like '%{$params['q']}%') limit 5";
		$q = "select gid,gname,g.pid,hash from pages g left join photos on photos.id=g.pid left join categories on categories.cat=g.subcat where g.privacy=" . PRIVACY_EVERYONE . " and g.active=1 and g.privacy=" . PRIVACY_EVERYONE . " and g.type=" . PAGE_TYPE_BUSINESS . " and (" . implode(" and ", $companies) . ") limit 5";

		$x = sql_query($q);
    echo mysql_error();

		if (mysql_num_rows($x) > 0)
		{
		?>
		<div class="searchimages">
			<?php
			while ($page = mysql_fetch_array($x, MYSQL_ASSOC))
			{
				echo '<div>';
				echo '<a href="' . $API->getPageURL($page['gid'], $page['gname']) . '">';
				echo '<img src="' . $API->getThumbURL(1, 100, 75, $API->getPageImage($page['gid'], $page['pid'] ) ) . '" alt="" /><br />';
				echo "<span><b>{$page['gname']}</b></span>";
				echo '</a>';
				echo '</div>';
				echo '</a>';
			}

			?>
			<div style="clear: both;"></div>
		</div>
		<?php
		}
		else
			echo '<div class="noresults2">No results found.</div>';
		?>

		<?php if (mysql_num_rows($x) > 0) { ?><div class="searchmore"><a href="/search_m.php?<?=getParams(array("t" => "c"))?>"><img src="/images/add.png" alt="" />show more work pages</a></div><?php } ?>
		<div class="searchdiv"></div>

		<div class="searchhead">
			<a href="/search_m.php?<?=getParams(array("t" => "g"))?>">Pages for &quot;<?=$params['q']?>&quot;</a>
		</div>
		<?php
        $pages = explode(' ', $_GET['q']);
        $pg = array();
        foreach ($pages as $value){
            $pg[] = "(g.gname like '%$value%' or g.descr like '%$value%' or g.contactfor like '%$value%' or products like '%$value%' or contact_person like '%$value%')";
        }
		$q = "select gid,gname,g.pid,hash from pages g left join photos on photos.id=g.pid where g.active=1 and g.privacy=" . PRIVACY_EVERYONE . " and g.type!=" . PAGE_TYPE_BUSINESS . " and g.type!=" . PAGE_TYPE_VESSEL . " and (" . implode(" and ", $pg) . ") limit 5";
        $x = sql_query($q);

		if (mysql_num_rows($x) > 0)
		{
		?>
		<div class="searchimages">
			<?php
			while ($page = mysql_fetch_array($x, MYSQL_ASSOC))
			{
				echo '<div>';
				echo '<a href="' . $API->getPageURL($page['gid'], $page['gname']) . '">';
				echo '<img src="' . $API->getThumbURL(1, 100, 75, $API->getPageImage($page['gid'], $page['pid'] ) ) . '" alt="" /><br />';
				echo "<span><b>{$page['gname']}</b></span>";
				echo '</a>';
				echo '</div>';
				echo '</a>';
			}

			?>
			<div style="clear: both;"></div>
		</div>
		<?php
		}
		else
			echo '<div class="noresults2">No results found.</div>';
		?>

		<?php if (mysql_num_rows($x) > 0) { ?><div class="searchmore"><a href="/search_m.php?<?=getParams(array("t" => "g"))?>"><img src="/images/add.png" alt="" />show more pages</a></div><?php } ?>
		<div class="searchdiv"></div>

	</div>



	<div style="float: right; width: 300px;">
		<?php showCompleteProfileInformationWide() ?>
<? if( $API->adv ) { ?>
		<div class="subhead parent_create_ad" style="margin-top: 10px;">
            <div style="float:left;">Sponsors</div>
            <div class="create_ad link" style="float:right;"><a href="http://www.salthub.com/adv/create.php" style="font-size:8pt; font-weight:300; color:rgb(0, 64, 128);">create an ad</a>&nbsp;</div>
            <div style="clear:both;"></div>
        </div>
		<div style="margin: 5px 0 10px;"><?php showAd("companion"); ?></div>
<? } ?>
		<?php include "inc/connect.php"; ?>
<? if( $API->adv ) { ?>
		<div class="subhead parent_create_ad">
            <div style="float:left;">Sponsors</div>
            <div class="create_ad link" style="float:right;"><a href="http://www.salthub.com/adv/create.php" style="font-size:8pt; font-weight:300; color:rgb(0, 64, 128);">create an ad</a>&nbsp;</div>
            <div style="clear:both;"></div></div>
		<div style="margin: 5px 0 0;"><?php showAd("companion"); ?></div>
<? } ?>
		<?php include_once "inc/pymk.php"; ?>
	</div>
	<div style="clear: both;"></div>
</div>

<?php

function getParams($change)
{
	global $params;
	
	$params2 = $params;
	
	foreach ($change as $k => $v)
		$params2[$k] = $v;
	
	foreach ($params2 as $k => $v)
		$res .= "&$k=$v";
	
	return substr($res, 1);
}

include "footer.php";

?>