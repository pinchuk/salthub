<?php
/*
Submits a report of content from a user who believes that the content is violating the TOS.

Change Log
8/20/2011 - Added "reported" flag to photos and videos

*/

include "inc/inc.php";

/*
mysql> describe reported;
+--------+------------+------+-----+---------+----------------+
| Field  | Type       | Null | Key | Default | Extra          |
+--------+------------+------+-----+---------+----------------+
| rid    | int(11)    | NO   | PRI | NULL    | auto_increment |
| reason | int(11)    | YES  |     | NULL    |                |
| uid    | int(11)    | YES  |     | NULL    |                |
| type   | varchar(1) | YES  |     | NULL    |                |
| id     | int(11)    | YES  |     | NULL    |                |
+--------+------------+------+-----+---------+----------------+
5 rows in set (0.00 sec)

*/

$vals = implode(",", array(intval($_POST['reason']), $API->isLoggedIn() ? $API->uid : 0, "'" . substr($_POST['type'], 0, 1) . "'", intval($_POST['id'])));

sql_query("insert into reported (reason,uid,type,id) values ($vals)");

echo mysql_error();


$type = substr($_POST['type'], 0, 1);
$id = $_POST['id'];
if( $type == 'P' || $type == 'V' )
{
  sql_query( "update " . ($type=='P' ? "photos" : "videos") . " set reported='1' where id='$id'" );

  if( $type == "P" )
  {
    //Set the user back to the "default" image if a user's image was reported
    sql_query( "update users set pic=0 where pic='$id' limit 5" );
  }
  
  //Send notification of report
  $msg = quickQuery( "select content from static where id='report_email_text'" );
  $email = quickQuery( "select content from static where id='report_email_address'" );
  $from = "admin@" . $siteName . ".com";

	$msg = str_replace("{TYPE}", ($type=='P' ? "photo" : "video"), $msg);
	$msg = str_replace("{REASON}", quickQuery( "select reason from report where ID='" . $_POST['reason'] . "'" ), $msg);
  $msg = nl2br( $msg );

  emailAddress($email, "Reported " . ($type=='P' ? "photo" : "video"), $msg, $from);
}
elseif( $type == 'A' )
{
  //Send notification of report
  $msg = quickQuery( "select content from static where id='report_email_text'" );
  $email = quickQuery( "select content from static where id='report_email_address'" );
  $from = "admin@" . $siteName . ".com";

	$msg = str_replace("{TYPE}", "Advertisement: " . quickQuery( "select title from ads2 where id='$id'" ), $msg);
	$msg = str_replace("{REASON}", quickQuery( "select reason from report where ID='" . $_POST['reason'] . "'" ), $msg);
  $msg = nl2br( $msg );

  emailAddress($email, "Reported Ad", $msg, $from);
}
elseif( $type == 'j' )
{
  //Send notification of report
  $msg = quickQuery( "select content from static where id='report_email_text'" );
  $email = quickQuery( "select content from static where id='report_email_address'" );
  $from = "admin@" . $siteName . ".com";

	$msg = str_replace("{TYPE}", "Job Posting", $msg);
	$msg = str_replace("{REASON}", quickQuery( "select reason from report where ID='" . $_POST['reason'] . "'" ), $msg);
  $msg = nl2br( $msg );

  emailAddress($email, "Reported Job", $msg, $from);
}

?>OK