<?php

/*

every page should either include header.php or inc.php

inc.php contains all the backend code needed to set up the session,
includes the various functions we use, the api, etc.

header.php has an include statement for inc.php but also displays
the splashvision/mediabirdy header seen at the top of the page

so, use header and footer for html pages and just inc.php for
backend/ajax pages

i realize it's probably self-explanatory, but just wanted to make
sure we are on the right page.

also see sample_ajax.php

thanks
mike

*/

include "../header.php";

echo "blah blah blah";

include "../footer.php";

?>
