<?php

/*

some of the site is based on old code that does not use json with ajax.
i have been trying to implement json in new code and old code that i
have revised, but old code still exists.  i would prefer to see json
used in the future, but i realize all coders have different styles, so
feel free to do what you please.

javascript ajax functions are found in /simpleajax.js, the ones i
typically use are getAjax and postAjax - their use should be
fairly simple to pick up.  let me know if you have any questions.

*/

include "../inc/inc.php";

$arr = array("fruits" => array("lemon", "orange"),
	"vegetables" => array("corn", "broccoli"),
	"uid" => $API->uid
	);

echo json_encode($arr);

?>
