<?php
/*
Updates a user's primary photo associated with their account.
*/

include_once "../inc/inc.php";

if (empty($API->uid)) die();

$pic = intval($_POST['pic']);

if ($pic == 0)
{
	if (empty($API->fbid) && intval(quickQuery("select fbid from users where uid=" . $API->uid)) == 0)
		die();
}
elseif ($pic == 1)
{
	if (empty($API->twid) && intval(quickQuery("select twid from users where uid=" . $API->uid)) == 0)
		die();
}
elseif ($site == "m")
{
	$pic = 2;
	if ($API->getUserPic(null, 2) == "") die();
}
elseif ($site == "s")
{
	mysql_query("update albums set mainImage=$pic where albType=2 and uid=" . $API->uid);
	mysql_query("update photos set aid=" . quickQuery("select id from albums where albType=2 and uid={$API->uid}") . " where id=$pic and uid=" . $API->uid);
}

mysql_query("update users set pic=$pic where uid=" . $API->uid);
$_SESSION['pic'] = $pic;

$API->feedAdd('o',0);
?>