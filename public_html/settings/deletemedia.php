<?php
/*
Deletes a user's owned media.  Called from /footer.php (admins only) or from
/settings/photos.php
/settings/videos.php
*/

include "../inc/inc.php";

if (!$API->isLoggedIn()) die();

if ($_POST['type'] == "V")
	$type = "V";
else
	$type = "P";

if ($type == "V")
{
	foreach ($_POST as $k => $v)
	{
		if ($v == "on")
		{
			$id = intval(substr($k, 3));
			$hash = quickQuery("select hash from videos where id=$id" . ($API->admin == 1 ? "" : " and uid=" . $API->uid));
			
			if (empty($hash)) continue;
			
			deleteMediaFromDB($type, $id);
			mysql_query("delete from videos where id=$id");
			if (strlen($hash) == 32) //our video, not youtube
				exec("rm -rf " . $_SERVER["DOCUMENT_ROOT"] . "/videos/$id");
		}
	}
}
else
{
	foreach ($_POST as $k => $v)
	{
		if ($v == "on")
		{
			$id = intval(substr($k, 3));
			
			if ($_POST['admindelete'] == "1" && $API->admin == 1) //deleting a photo from the photo page
			{
				$type = "P";
				$x = mysql_query("select id,hash from photos where id=$id");
			}
			elseif ($_POST['single'] == "1") //deleting a photo from the profile photo page (/settings/index.php)
			{
				$type = "P";
				$x = mysql_query("select id,hash from photos where id=$id and uid={$API->uid}");
			}
			else //deleting an album from the settings page
			{
				$type = "A";
				$x = mysql_query("select id,hash from photos where aid=$id and uid=" . $API->uid);
				if (mysql_num_rows($x) > 0)
					deleteMediaFromDB($type, $id);
			}
			
			if (mysql_num_rows($x) > 0)
			{
				deleteMediaFromDB($type, $id);
				
				while ($photo = mysql_fetch_array($x, MYSQL_ASSOC))
				{
					mysql_query("delete from photos where id=" . $photo['id']);
					exec("rm -rf " . $_SERVER["DOCUMENT_ROOT"] . "/photos/" . getPathFromPhotoID(intval($photo['id'])));
				}
			}
		}
	}
}

?>