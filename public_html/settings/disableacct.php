<?php
/*
Enables a user to disable their own account.

11/10/2012 - Added support for secondary social media accounts
*/

include "../inc/inc.php";

if (!$API->isLoggedIn()) die("ERROR");

$a = intval($_POST['a']);
$sid = intval($_POST['sid']);

// we need to check to make sure they have the other account enabled before disabling this account

if( $sid > 0 && $a == 0 ) //This is a secondary account
{
  mysql_query( "delete from social_media_accounts where id='" . $sid . "' and uid='" . $API->uid . "'" );
}
else if ($a == 1) //twitter
{
	mysql_query("update users set twtoken='',twsecret='',twusername='',twid=null where uid=" . $API->uid );
	if (mysql_affected_rows() == 0) die("ERROR");

	$_SESSION['access_token'] = '';

	if ($_SESSION['pic'] == 1)
		$_SESSION['pic'] = 0;

	if ($API->pic == 1) //we were using the twitter picture, now we need to use the fb picture
		mysql_query("update users set pic=0 where uid=" . $API->uid);
}
else if ($a == 2){
    mysql_query("update users set linkedin_id = '' where uid = ".$API->uid);
    unset($_SESSION['linkedin_access_token']);
    unset($_SESSION['linkedin_auth']);
    unset($_SESSION['li_pic']);
}
else //facebook
{
	mysql_query("update users set fbsess='',fbid='',fbusername='' where uid=" . $API->uid);
	if (mysql_affected_rows() == 0) die("ERROR");

	if ($_SESSION['pic'] == 0)
		$_SESSION['pic'] = 1;

	unset($_SESSION['fbsess']);

	if ($API->pic == 0) //we were using the fb picture, now we need to use the tw picture
		mysql_query("update users set pic=1 where uid=" . $API->uid);
}

echo "OK";

?>