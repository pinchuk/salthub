<?php

include_once "../inc/inc.php";
if (!$API->isLoggedIn())
{
	header("Location: /?login");
	die();
}

if (!$hideRight && $site == "m") $scripts[] = "/mod_media.js";
$title = "Settings";
include "../header.php";

?>


<div class="bigtext" style="color: #555; margin-bottom:-23px;">
	<div class="contentleft piconly">
		<div class="profilepic_container">
			<div class="profilepic" style="text-align: center;">
				<div class="borderhider">&nbsp;</div><div class="borderhider2">&nbsp;</div>
				<a href="<?=$API->getProfileURL()?>"><img width="48" height="48" src="<?=$API->getThumbURL(1, 48, 48, $API->getUserPic())?>" alt="" /></a>
			</div>
		</div>
	</div>

<div style="padding-top: 3px;">
  <?php
	$user = array("uid" => $API->uid, "username" => $API->username, "name" => $API->name);

  $links = array();
  $links[] = array("/images/application_edit.png", "General Details", "index.php");
  $links[] = array("/images/bell_add.png", "Notifications", "notifications.php");
  $links[] = array("/images/television.png", "Videos", "videos.php");
  $links[] = array("/images/images.png", "Photos", "photos.php");
  $links[] = array("/images/layout_delete.png", "Privacy", "privacy.php");

	showUserWithItems($user, $links);
  ?>
</div>

</div>

<div class="contentborder" >
<div style="clear:both; margin-bottom:10px;"></div>


<!--
<div class="boxleftNoPadding" style="float: left;<?=$hideRight || $site == "s" ? " width: 100%;" : ""?>">
	<div class="contentlinks" style="font-size: 10pt;">
		<?php
		$links[] = array("index", "General Details");
		$links[] = array("notifications", "Notifications");
		$links[] = array("videos", "Videos");
		$links[] = array("photos", "Photos");
		$links[] = array("privacy", "Privacy");

		for ($i = 0; $i < count($links); $i++)
		{
			?>
			<a href="/settings/<?=$links[$i][0]?>.php"<?=$script == "settings/" . $links[$i][0] ? " style=\"font-weight: bold;\"" : ""?>><?=$links[$i][1]?></a>
			<?php
			if ($i < count($links) - 1)
				echo " |";
		}
		?>
	</div>
-->