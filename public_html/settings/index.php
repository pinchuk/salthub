<?php
/*
Main screen for user settings
*/

$scripts[] = "/likeus.js.php";
$scripts[] = "/checkavail.js";
include "header.php";
require_once('../twitter/twitteroauth/twitteroauth.php');

$x = sql_query("select linkedin_id, fbid,twid,name,email,username,phone,twusername,twid,fbusername,fbid from users where uid=" . $API->uid);
$user = mysql_fetch_array($x, MYSQL_ASSOC);

if (isset($_GET['invalid_image']))
	echo "<div class=\"error\">You have uploaded an invalid image.</div>";
elseif (isset($_GET['success']))
	echo "<div class=\"success\">Your profile image has been changed.</div>";

$_SESSION['addacct'] = true;
?>

<div class="strong" style="font-size:13pt;">General Details</div>

<div class="thead">My Name:</div>
<div class="tcontent" id="name-content">
  <?
  $num_changes = quickQuery( "select number_of_name_changes from users where uid='" . $API->uid . "'" );

  if( $num_changes == 0 )
  {
  ?>
	  <input type="text" id="name" onkeyup="javascript:checkAvail(this);" maxlength="65" value="<?=$user['name']?>" />&nbsp; &nbsp; <span id="name-avail"></span>
  	<div style="margin-top: 2px;">
        Please use your real name. This is the name people will use to find you. If you would like to use your company name, please <a href="/pages/page_create.php">create a page</a>.
  	</div>
  <?
  }
  else
  {
    echo '<div style="padding-top:5px;">' . $user['name'] . '</div>';
  }
  ?>
</div>

<?
$hasPassword = ($site == "s" && quickQuery("select count(*) from users where uid=" . $API->uid . " and password is not null") == "1");
?>
<div class="thead" style="padding-top: 3px;">Password:</div>
<div class="tcontent">
  <a href="javascript:void(0);" onclick='javascript: showChangePassword();'><? if( $hasPassword ) {?>change<? } else { ?>add<? } ?> password</a> <span id="changepwtext"></span>
<? if( !$hasPassword ) {?>
	<div style="margin-top: 2px;">
      Adding a password to your account enables you to login to <?=$siteName?> without a Facebook or Twitter account.
	</div>
<? } ?>
</div>

<div id="changePassword" style="display:none; background-color:#EEEEEE;">
<? if( $hasPassword ) { ?>
  <div class="thead">Old Password:</div>
  <div class="tcontent"><input type="password" id="oldpw" maxlength="25" value="" style="width: 150px;" /></div>
<? } ?>
  <div class="thead">New Password:</div>
  <div class="tcontent"><input type="password" id="newpw1" maxlength="25" value="" style="width: 150px;" /></div>
  <div class="thead">Confirm Password:</div>
  <div class="tcontent"><input type="password" id="newpw2" maxlength="25" value="" style="width: 150px;" /></div>
  <div class="tcontent" style="text-align:center; margin-bottom:20px;"><a href="javascript:void(0);" onclick="javascript:submitChangePassword();">save password</a> &nbsp;&nbsp;&nbsp; <a href="javascript:void(0);" onclick="javascript: showChangePassword();">cancel</a></div>
</div>

<div class="thead">My E-mail:</div>
<div class="tcontent">
	<input type="text" id="email" onkeyup="javascript:if (this.value.indexOf('@') > 0) checkAvail(this);" maxlength="65" value="<?=$user['email']?>" />&nbsp; &nbsp; <span id="email-avail"></span>
	<div style="margin-top: 2px;">
		This is the e-mail address we will use to contact you. If you do do not login with Twitter or Facebook, it will also be used for logging in.
	</div>
</div>

<div class="thead">My Username:</div>
<div class="tcontent">
	<input type="text" id="username" onkeyup="javascript:checkAvail(this);" maxlength="25" value="<?=$user['username']?>" style="width: 125px;" />&nbsp; &nbsp; <span id="username-avail"></span>
	<div style="margin-top: 2px;">
	Your username makes it easier to find your profile. It may only contain letters and numbers.
	</div>
</div>

<div class="thead">My Profile URL:</div>
<div class="tcontent">
	<span style="color: #000;">http://<?=$_SERVER['HTTP_HOST']?>/user/<span id="userurl"><?=str_replace( " ", ".", $user['username'] );?></span></span>
</div>

<?
if( $site != "m" )
{
$uname = quickQuery( "select username from users where uid='" . $API->uid . "'" );
$jmp = quickQuery( "select jmp from users where uid='" . $API->uid . "'" );
if( $jmp == "" )
{
  $jmp = shortenURL( "http://" . SERVER_HOST . $API->getProfileURL($API->uid) . "/logbook");
  sql_query( "update users set jmp='$jmp' where uid='" . $API->uid . "'" );
}
?>
<div class="thead">My Profile's shb.me URL:</div>
<div class="tcontent" style="color: #000; margin-top:5px;">
  http://shb.me/<span id="jmp-url"><?=$jmp?></span>
</div>

<?php
}
?>

<div class="thead">Mobile phone upload:</div>
<div class="tcontent">
	<div style="float: left; width: 350px;">
		<input type="text" id="phone" onkeyup="javascript:checkAvail(this);" maxlength="25" value="<?=$user['phone']?>" style="width: 125px;" /><span style="color: #000;">@<?=$siteName?>.com</span>&nbsp; &nbsp; <span id="phone-avail"></span>
	</div>
	<div style="float: left; background-image: url(/images/help.png); font-size: 8pt;" class="subtitlepic" onclick="javascript:showRecommendation();"">
		recommendation
	</div>
	<div style="padding-top: 2px; clear: both;">
	Create your own unique email address to post your media directly into your <?=$siteName?> account from your mobile phone.&nbsp; Now you can capture your moments and share them with your <? echo ($site=="m") ? 'friends' : 'connections'; ?> as they happen.&nbsp; <a href="/email.php">Learn how &#0133;</a>
	</div>
</div>


<div class="thead" style="margin-top:20px; font-size:12pt; text-align:left;">Social Accounts &amp; Photo Management</div>


<div class="thead">Social Distribution:<br /><span style="font-size:8pt; font-weight:300;">(recommended)</span></div>
<div class="tcontent" style="padding-top:10px;">
  Enabling your social accounts on <?=$siteName?> has benefits. Once enabled, you can share company or personal news, product releases, status updates, photos + videos and more.  Your information will then be sent directly to multiple networks simultaneously without leaving <?=$siteName?>.
</div>

<div style="clear:both; padding-top:10px;"></div>

<?
$hasPassword = ($site == "s" && quickQuery("select count(*) from users where uid=" . $API->uid . " and password is not null") == "1");

if (($user['twid'] && $user['fbid']/* && !empty($user['linkedin_id'])*/) || $hasPassword)
	$disableOK = true;

for ($i = 0; $i <= 1; $i++)
{
	if ($i == 1)
	{
		$prefix = "tw";
		$ssite = "Twitter";
		$enabled = $user['twid'] > 0;
	}
	elseif ($i == 0)
	{
		$prefix = "fb";
		$ssite = "Facebook";
		$enabled = $user['fbid'] > 0;
	}
//    else if ($i == 2){
//        $prefix = "li";
//        $ssite = "LinkedIn";
//        $enabled = isset($_SESSION['li_pic']);
//    }

?>
	<div class="thead"><?=$ssite?> Account:</div>
	<div class="tcontent" style="color: #000;">
		<?php
		if (!$enabled)
		{
		?>
    <div style="padding-top:5px;">
  		<a href="javascript:void(0);" onclick="javascript:showEnableAcct(<?=$i?>);">enable</a>
    </div>
		<?php
		} else {
		?>

    <div style="float:left; padding-top:5px;">
      <div style="height:25px; color:rgb(119,119,119);">Primary Account</div>
    <? if ($ssite == "LinkedIn"){ ?>
        <img class="img" src="<?=$_SESSION['li_pic']?>" alt="" />
                <?=$user[$prefix . 'username']?><br />
            <a href="javascript:void(0);" onclick="javascript:disableAcct(<?=$i?>);">disable</a>
    <? }else{ ?>
  		<img class="img" src="https://graph.facebook.com/<?=$user['fbid']?>/picture?width=50&height=50" alt="" />
	  	<?=$user[$prefix . 'username']?><br />
		  <?php }if ($i != 2 && $disableOK) { ?><a href="javascript:void(0);" onclick="javascript:disableAcct(<?=$i?>);">disable</a><?php } ?>
    </div>

<?
if( $i == SITE_TWITTER ) //Secondary accounts are only supported via Twitter currently
{
  $sql = "select * from social_media_accounts where uid='" . $API->uid . "' and site='" . SITE_TWITTER . "'";
  $q = mysql_query( $sql );

  while( $r = mysql_fetch_array( $q ) )
  {
    $connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $r['token'], $r['secret']);
    $content = $connection->get('account/verify_credentials');
?>
  <div style="margin-left:10px; width:150px; float:left; padding-top:5px;">
    <div style="height:25px; color:rgb(119,119,119);">Broadcast Account</div>
		<img class="img" width="48" height="48" src="<?= $content->profile_image_url; ?>" alt="" />
 	  <?=$r['username'];?><br />
    <a href="javascript:void(0);" onclick="javascript:disableAcct(0,<?=$r['id']?>);">disable</a>
  </div>
<?
  }
?>
  <div style="margin-left:10px; width:150px; float:left; padding-top:5px;">
    <div style="height:25px;"></div>
    <a href="javascript:void(0);" onclick="javascript:showEnableAcct(<?=$i?>,'Add an account to broadcast to', 'secondary=1');">
    <img class="img" src="/images/add_account.png" width="48" height="48" alt="" />
    Add Account</a>
  </div>
<?
}
?>

		<? } ?>
	</div>
<?php
}
?>

<div class="thead" style="padding-top: 5px;">My photo:</div>
<?php
if ($site == "m")
{
?>
<div class="tcontent">
	<div style="clear: left; padding-bottom: 5px;">
		<div style="height: 20px;">
			<a href="javascript:void(0);" onclick="javascript:showUploadPhoto();">Upload my own photo</a>
			<div id="uploadownphoto" style="display: none; position: relative;">
				<form action="/upload/photo.php?userpic" enctype="multipart/form-data" method="post">
				<input type="file" name="Filedata" onchange="javascript:fakefile.value = this.value;" style="position: absolute; left: 90px; z-index: 2; -moz-opacity: 0; filter:alpha(opacity: 0); opacity: 0;">
				<div style="top: 0px; left: 0px; position: absolute;">
					<input id="fakefile" type="text" disabled style="left: 0px; top: 0px; font-family: arial; font-size: 9pt; color: black; background: white; border: 1px solid #7F9DB9; width: 227px; height: 19px;">
					<div style="position: absolute; left: 230px; top: 1px; width: 75px; height: 20px; overflow: hidden;">
						<img src="/images/btnbrowse.png" style="padding-left: 4px;" />
					</div>
				</div>
				<div style="top: -1px; left: 310px; position: absolute;">
					<input type="submit" value="&nbsp;Upload&nbsp;" class="button" style="width: 80px; font-size: 9pt;" />
				</div>
			</div>
		</div>
	</div>
	<?php
	$pics = array(0 => $API->getThumbURL(1, 48, 48, $API->getUserPic(null, 0, false)), 1 => $API->getThumbURL(1, 48, 48, $API->getUserPic(null, 1, false)));
	
	foreach ($pics as $i => $url)
	{
		if (!empty($url))
		{
			?>
			<a href="javascript:void(0);" onclick="javascript:changeUserPic(<?=$i?>);"><img class="img" src="<?=$url?>" alt="" id="userpic<?=$i?>" style="<?=$site == "m" ? "border: 2px solid " . ($API->pic == $i ? "#326798" : "#E5ECF3") : ""?>;" /></a>
			<?php
		}
	}
	?>
</div>
<?php
}
elseif ($site == "s")
{
	?>
	<div class="tcontent">
		<div style="clear: left; float: left; width: 57px;">
			<img id="curuserpic" class="img curuserpic" alt="" src="<?=$API->getThumbURL(1, 48, 48, $API->getUserPic())?>" />
		</div>

    <div id="pccontainer_show" style="float:left; clear:left;">
      <a href="javascript:void(0);" onclick="javascript:document.getElementById('pccontainer').style.display=''; javascript:document.getElementById('pccontainer_show').style.display='none'; slidedown('photochanger');">change profile photo</a>
    </div>

	</div>

	<div id="pccontainer" style="padding-left: 150px; float: left; display:none;"></div>
	<?php
}
?>

<script language="javascript" type="text/javascript">
<!--
var twToken = "<?=$user['twtoken']?>";

function showRecommendation()
{
	showPopUp("E-mail recommendation", "<div style='padding: 10px; font-size: 9pt;'>When creating an e-mail address for posting to your <?=$siteName?> account, we recommend using a combination of numbers and letters to prevent others from also posting to your account.</div>", 350);
}

function changeUserPicHandler(data)
{
	//alert(data);
}

function disableAcct(a, sid)
{
  if( typeof sid == "undefined" )
    sid = 0;

	if (confirm("Are you sure you want to disable this account?"))
		postAjax("disableacct.php", "a=" + a + "&tt=" + escape(twToken) + "&sid=" + sid, "disableAcctHandler");
}

function disableAcctHandler(data)
{
	if (data == "OK")
		location.reload(true);
	else
		alert("There was an error disabling this account.  Please try again later.");
}

function showChangePassword()
{
  if( document.getElementById("changePassword").style.display == "" )
    document.getElementById("changePassword").style.display = "none";
  else
    document.getElementById("changePassword").style.display = "";
}

function submitChangePassword()
{
  if( document.getElementById("oldpw") )
  {
    old = document.getElementById("oldpw").value;
  }
  else
  {
    old = "";
  }

  new1 = document.getElementById("newpw1").value;
  new2 = document.getElementById("newpw2").value;

  if( new1 != new2 )
  {
    alert( '"New password" does not match "Confirm password", please check your passwords.' );

  }
  else
  {
    postAjax("changepw.php", "old=" + old + "&new=" + new1, "changePwHandler");
  }
}

function changePwHandler( data )
{
  if( data == "BadOldPassword" )
  {
    alert( 'Your old password does not match, your password was not saved.' );
  }
  else if( data == "OK" )
  {
    document.getElementById("changePassword").style.display = "none";
    document.getElementById("changepwtext").innerHTML = '&nbsp;&nbsp;&nbsp;<img src="/images/check.png"/> saved';
  }
}

<?
$firstLogin = quickQuery( "select first_login from users where uid='" . $API->uid . "'" );

if( isset( $_GET['n'] ) || $firstLogin == 1 ) {
?>
  showLikeUs();
<?
  sql_query( "update users set first_login=0 where uid='" . $API->uid . "' limit 1" );
}
?>

<? if( isset( $_GET['merged'] ) ) { ?>
  showPopUp("Thank you!", "<div style='padding: 10px; font-size: 9pt;'>Your accounts have been successfully merged.</div>", 350);
<? } ?>

<? if( isset( $_GET['dup'] ) && $_GET['dup'] == 1 ) { ?>
  showPopUp("Notice", "<div style='padding: 10px; font-size: 9pt;'>This social media account has already been added to your profile.</div>", 350);
<? } ?>

//-->

</script>

<?php
$scripts[] = "/photochanger.js";
include "footer.php";
?>
