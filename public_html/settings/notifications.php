<?php
/*
This page enables the user to change their notification settings.
*/

$hideRight = true;
include "header.php";

$x = mysql_query("select email,notifications" . ($site == "s" ? ",verify" : "") . " from users where uid=" . $API->uid);
$info = mysql_fetch_array($x, MYSQL_ASSOC);
$enabled = $info['notifications'];

?>

<div class="strong" style="font-size:13pt;">Notifications</div>

<form name="frmnotify">

<?php

//$notDone = '&nbsp; <img src="/images/x.png" alt="" />';

showHead("Send Notifications To");
showOption(NOTIFY_PREF_FB, "Send notifications to my Facebook account");
showOption(NOTIFY_PREF_TW, "Send notifications to my Twitter account");
if ($info['email'] && $info['verify'] == 1) showOption(NOTIFY_PREF_EMAIL, "Send notifications to my e-mail address");

showHead("Weekly Summaries");
showOption(NOTIFY_PREF_WEEKLY_PAGES_SUMMARY, "Your Pages");
showOption(NOTIFY_PREF_WEEKLY_PROFILE_SUMMARY, "Your Profile");
showOption(NOTIFY_PREF_WEEKLY_CONNECTIONS_SUMMARY, "Your Vessels and Connections");

showHead("Notifications");
//showOption(NOTIFY_PREF_MB_SHARE, "Notify me when a user shares my media");
showOption(NOTIFY_PREF_MB_LIKE, "Notify me when a user likes or dislikes my media");
showOption(NOTIFY_PREF_MB_COMMENT, "Notify me when a user posts a comment about my media");
if ($site == "s")
{
	showOption(NOTIFY_PREF_MB_ADDTAG, "Notify me when a user adds a tag to my media");
	showOption(NOTIFY_PREF_MB_TAGGED, "Notify me when I am tagged in media");
	showOption(NOTIFY_PREF_MB_ADDFRIEND, "Notify me when a user sends me a friend request");
	showOption(NOTIFY_PREF_MB_MESSAGE, "Notify me when a user sends me a message");
	showOption(NOTIFY_PREF_MB_CMTSAME, "Notify me when a user comments where I comment");
	showOption(NOTIFY_PREF_MB_WALLPOST, "Notify me when a user writes on my log book$notDone");
	showOption(NOTIFY_PREF_MB_GROUP, "Notify me when I am added to a page$notDone");
	showOption(NOTIFY_PREF_MB_PAGE_UPDATE, "Notify me when I my pages are updated");
}

foreach (array("FB" => "Facebook", "TW" => "Twitter") as $s => $ssite)
{
?>

<div style="float: left; width: 50%;">
<?php
showHead("Show the following activities on $ssite &#0133;");
//eval('showOption(NOTIFY_PREF_' . $s . '_UPLOAD, "Show when I upload media");');
eval('showOption(NOTIFY_PREF_' . $s . '_LIKE, "Show my likes and dislikes");');
//eval('showOption(NOTIFY_PREF_' . $s . '_COMMENT, "Show my comments");');
if ($site == "s")
{
	eval('showOption(NOTIFY_PREF_' . $s . '_ITAG, "Show when I tag a user in a photo or video");');
	eval('showOption(NOTIFY_PREF_' . $s . '_ADDTAG, "Show when a user adds a tag to my media");');
	eval('showOption(NOTIFY_PREF_' . $s . '_TAGGED, "Show when I am tagged in media");');
  eval('showOption(NOTIFY_PREF_' . $s . '_PAGE_MEDIA_SHARED, "Show when I share media in pages");');
}  
?>
</div>

<?php } ?>

</form>

<div style="clear: both; height: 15px;"></div>

<input type="button" class="button" value="Save Settings" onclick="javascript:saveNotifications();" />

<script language="javascript" type="text/javascript">
function saveNotifications()
{
	notifications = "";
	checks = getFormVals(document.forms.frmnotify).split("&");
	
	for (i in checks)
	{
		x = checks[i].split("=");
		if (x[1] == "on")
			notifications += "," + x[0].substring(1);
	}
	
	notifications += ",";

//	alert(notifications);
	
	postAjax("savenotifications.php", "n=" + notifications, "saveNotificationsHandler");
}

function saveNotificationsHandler(data)
{
	if (data == "OK")
		showPopUp2("", "Your notification preferences have been saved<br />and will take effect immediately.");
	else
		showPopUp2("", "There was an error saving your notification preferences.<br />Please try again later.");
}
</script>

<?php
$odd = true;

function showHead($x)
{
	global $odd;
	$odd = true;
	echo "<div class=\"notify_head\">$x</div>";
}

function showOption($val, $opt)
{
	global $odd, $enabled;
	echo "<div class=\"notify_" . ($odd ? "odd" : "even") . "\"><input type=\"checkbox\" name=\"n$val\"" . ($enabled == "*" || strpos($enabled, ",$val,") !== false ? " checked" : "") . " />$opt</div>";
	$odd = !$odd;
}

include "footer.php";

?>