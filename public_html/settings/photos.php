<?php
/*
Enables a user to browse and delete their photos and albums.
*/

$type = "P";

include "../inc/inc.php";

if (!$API->isLoggedIn())
{
	header("Location: /");
	die();
}

$perPage = 10;
$page = intval($_GET['p']);

$x = mysql_query("select SQL_CALC_FOUND_ROWS albums.id as aid,albums.title,albums.descr,albums.created,hash,photos.id,mainImage,jmp from albums join photos on mainImage=photos.id where albums.uid=" . $API->uid . " order by albums.albType desc limit " . ($page * $perPage) . ",$perPage");

if (mysql_num_rows($x) == 0 && $page > 0)
{
	header("Location: /$script.php");
	die();
}

$q = mysql_query( "select found_rows()" );
$r = mysql_fetch_array( $q );
$count = $r[0];
$pages = ceil($count / $perPage);

$scripts[] = "http://s7.addthis.com/js/250/addthis_widget.js#username=mediabirdy";
$scripts[] = "/showembedmedia.js";
$scripts[] = "/email.js";
include "header.php";

?>

<div class="strong" style="font-size:13pt;">Photos</div>

<form name="media">
<?php
while ($media = mysql_fetch_array($x, MYSQL_ASSOC))
{
    $isOnPage = quickQuery("select photos.on_page from photos inner join albums on albums.id = photos.aid where photos.id = ".$media['mainImage']);
    if ($isOnPage == 0)
	    showMediaEdit($type, $media);

	$mainImage[$media['aid']] = $media['mainImage'];
	$y = mysql_query("select aid,id,hash from photos where on_page = 0 and aid=" . $media['aid'] . " limit 14");
	while ($photo = mysql_fetch_array($y, MYSQL_ASSOC))
		$photos[] = $photo;
}
?>
</form>

<div style="float: left;" class="smtitle">
	<a href="javascript:void(0);" onclick="javascript:deleteMedia();">Delete selected albums</a>
</div>
<div style="padding-right: 10px;">
	<?php include "../inc/pagination.php"; ?>
</div>

<script language="javascript" type="text/javascript">
<!--
var mediaType = "<?=$type?>";
var page = 1;

function showMoreMedia( album )
{
  postAjax("getMorePhotos.php", "album=" + album + "&p=" + page, "showMoreMediaHandler");
  page++;
}

function showMoreMediaHandler( data )
{
  result = data.split(String.fromCharCode(1));
  album = result[0];
  count = result[1];
  html = result[2];


  document.getElementById("albumcontainer-" + album).innerHTML += html;

  if( count < 14 )
  {
  	document.getElementById("albumshowmore-"  + album).style.display="none";
  }
}

function initPage2()
{
	photos = [<?php
	$i = 0; //taking the contents of the albums and making them into an array [aid,pid,hash] for parsing by javascript
	for ($i = 0; $i < count($photos); $i++)
	{
		echo "[" . $photos[$i]['aid'] . "," . $photos[$i]['id'] . ",\"" . $photos[$i]['hash'] . "\"]";
		if ($i < count($photos))
			echo ", ";
	}
	?>];
	
	var lastAid = -1;

  count = 0;
	for (var i in photos)
	{
		if (lastAid != photos[i][0])
		{
			if (lastAid > -1)
				document.getElementById("albumcontainer-" + lastAid).innerHTML = add;
			add = "";
			lastAid = photos[i][0];
		}
		add += getPhotoHTML([photos[i][1], photos[i][2], photos[i][0]]);

    count++;
	}

  if( count >= 13 )
  {
  	document.getElementById("albumshowmore-" + photos[i][0]).style.display="";
  }

	document.getElementById("albumcontainer-" + photos[i][0]).innerHTML = add;

	delete add;
	delete photos;
	
	<?php
	foreach ($mainImage as $aid => $id)
		echo "phMainImage[$aid] = $id;\n";
	?>
}
//-->
</script>

<?php

include "../inc/embedmedia.php";
include "footer.php";

?>