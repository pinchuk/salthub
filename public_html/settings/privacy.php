<?php
/*
This page enables the user to change their privacy settings.
*/

include "header.php";

if( $site != "m" )
{
$x = mysql_query("select privacy from users where uid=" . $API->uid);
$info = mysql_fetch_array($x, MYSQL_ASSOC);
$enabled = $info['privacy'];

?>

<div class="strong" style="font-size:13pt;">Privacy</div>

<form name="frmprivacy">

<div class="thead" style="padding-top:3px;">Profile Privacy</div>
<div class="tcontent">
  Select the items below you would like to make private.
</div>

<div class="thead" style="padding-top:3px;">Maximum Privacy</div>
<div class="tcontent">
  <table cellpadding="0" cellspacing="0">
  <tr>
    <td valign="top"><? showOption(PRIVACY_MAX); ?></td>
    <td>Your name, profile photo, where you're from, company / vessel and profession are visible to everyone.  Note: All members can request a connection or send you a private message, regardless of privacy settings.</td>
  </tr>
  </table>
</div>

<div class="thead" style="padding-top:3px;">Custom Privacy</div>

<div class="thead" style="padding-top:3px;">About:</div>
<div class="tcontent" style="width:600px;">
  <table cellpadding="0" cellspacing="0">
  <tr>
    <td valign="top"><? showOption(PRIVACY_BASICS); ?></td>
    <td width="80" valign="top">the basics</td>

    <td valign="top"><? showOption(PRIVACY_EDUCATION); ?></td>
    <td width="80" valign="top">education</td>

    <td valign="top"><? showOption(PRIVACY_WORK); ?></td>
    <td width="80" valign="top">work</td>

    <td valign="top"><? showOption(PRIVACY_NETWORKS); ?></td>
    <td width="100" valign="top">clubs and<br /> associations</td>

    <td valign="top"><? showOption(PRIVACY_LIKES); ?></td>
    <td width="100" valign="top">things I like</td>

    <td valign="top"><? showOption(PRIVACY_EMPLOYMENT); ?></td>
    <td width="100" valign="top">employment</td>
  </tr>
  </table>
</div>

<div class="thead" style="padding-top:3px;">Log Book:</div>
<div class="tcontent">
  <? showOption(PRIVACY_LOG); ?>
</div>

<div class="thead" style="padding-top:3px;">Post to my log:</div>
<div class="tcontent">
  <? showOption(PRIVACY_LOG_POSTING); ?>
</div>

<div class="thead" style="padding-top:3px;">Videos:</div>
<div class="tcontent">
  <? showOption(PRIVACY_VIDEOS); ?>
</div>

<div class="thead" style="padding-top:3px;">Photos:</div>
<div class="tcontent">
  <? showOption(PRIVACY_PHOTOS); ?>
</div>

<div class="thead" style="padding-top:3px;">Connections:</div>
<div class="tcontent">
  <? showOption(PRIVACY_CONNECTIONS); ?>
</div>

<div class="thead" style="padding-top:3px;">Follow Me:</div>
<div class="tcontent">
  <? showOption(PRIVACY_FOLLOWME); ?>
</div>


</form>

<div style="clear:both;"></div>
<input type="button" class="button" value="Save Settings" style="margin-left:150px; margin-top:20px;" onclick="javascript:savePrivacy();" />

<div style="clear:both;"></div>

<div style="width:80%; height:1px; border:0px; border-top: 1px; border-style:dotted; margin:15px;"></div>
<?
} // end site != m
?>
<div class="thead" style="padding-top: 5px;">Deactivate account:</div>
<div class="tcontent">
  <table>
  <tr>
    <td valign="top"><input type="checkbox" style="width: auto; margin: 0; padding: 0; margin-right:5px;" onchange="javascript:showDeactivateButton(this.checked);" /></td>
    <td>Once you deactivate your account, it will no longer be shown publicly; however, if you log back into your account normally, it will be automatically restored.</td>
  </tr>
  </table>
</div>

<div style="clear: both; text-align: center; padding-top: 10px;">
	<input type="button" value="Deactivate Account" id="btndeactivate" class="button" style="display: none;" onclick="javascript:deactivateAccount();" />
</div>

<script language="javascript" type="text/javascript">
<!--

function savePrivacy()
{
	privacy = "";
	checks = getFormVals(document.forms.frmprivacy).split("&");

	for (i in checks)
	{
		x = checks[i].split("=");
		if (x[1] == "on")
			privacy += "," + x[0].substring(1);
	}

	privacy += ",";

	//alert(notifications);

	postAjax("saveprivacy.php", "p=" + privacy, "savePrivacyHandler");
}

function savePrivacyHandler(data)
{
  window.location.href = "privacy.php?s";
/*
	if (data == "OK")
		showPopUp2("", "Your privacy preferences have been saved<br />and will take effect immediately.");
	else
		showPopUp2("", "There was an error saving your privacy preferences.<br />Please try again later.");
*/
}


function deactivateAccount()
{
	if (confirm("Are you sure you want to deactivate your account?"))
		postAjax("/settings/setactive.php", "active=0", "deactivateAccountHandler");
}

function deactivateAccountHandler(data)
{
  window.location.href = "/logout.php";
}

function showDeactivateButton(show)
{
	document.getElementById("btndeactivate").style.display = show ? "" : "none";
}

<? if( isset( $_GET['s'] ) ) { ?>
showPopUp2("", "Your privacy preferences have been saved<br />and will take effect immediately.");
<? } ?>

//-->
</script>

<?php

function showOption($val)
{
	global  $enabled;
	echo "<input style=\"width: auto; margin: 0; padding: 0; margin-right:5px;\" type=\"checkbox\" name=\"p$val\"" . (strpos($enabled, ",1,") !== false || strpos($enabled, ",$val,") !== false ? " checked" : "") . " />";
}

include "footer.php"; ?>