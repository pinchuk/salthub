<?php
/*
Saves a user's privacy settings.  Called from /settings/privacy.php
*/

include_once "../inc/inc.php";

if (!$API->isLoggedIn()) die();

if( strpos($_POST['p'], ",1,") !== false ) $_POST['p'] = ",1,";

$foundAll = true;
for( $c = PRIVACY_MAX + 1; $c < PRIVACY_NUM_ITEMS; $c++ )   //This is to check to see if we check all of the privacy settings except max for some reason
{
  if( strpos($_POST['p'], "," . $c . ",") === false )
  {
    $foundAll = false;
    break;
  }
}

if( $foundAll ) $_POST['p'] = ",1,";

mysql_query("update users set privacy='" . $_POST['p'] . "' where uid=" . $API->uid);

?>OK