<?php
/*
Disables or enables a user's account.  Disabled accounts will have their photos and videos removed from the site immediately.  Users
can re-enable their account by logging in again.
*/

include "../inc/inc.php";

if (!$API->isLoggedIn()) die();

$active = intval($_POST['active']);

if ($API->admin == 1)
{
	$uid = intval($_POST['uid']);
	if ($uid == 0)
		$uid = $API->uid;

  if( $active == -1 )
  {
    $user_data = queryArray( "select name, pic, username from users where uid='" . $_POST['uid'] . "'" );

  	$API->name = $user_data['name'];
  	$API->pic = $user_data['pic'];
  	$API->username = $user_data['username'];

    $msg = quickQuery( "select content from static where id='banned_email'" );

    $msg = str_replace( "{SITENAME}", $siteName, $msg );
    $msg = nl2br( $msg );

    $API->emailUser($_POST['uid'], "$siteName Account Banned", $msg);


  }
}
else
{
	$active = 0;
	$uid = $API->uid;
}

mysql_query("update users set active=$active where uid=$uid");

if( $active == 0 )
{
  sql_query( "update photos set reported=2 where uid='" . $API->uid . "' and reported=0" ); //Inactive users will have their content marked as reported to prevent them from showing in search results.
  sql_query( "update videos set reported=2 where uid='" . $API->uid . "' and reported=0" );
}

?>