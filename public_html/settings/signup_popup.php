

<? if( !$API->isLoggedIn() ) { ?>
<div id="getstarted" style="width: 1053px; display: none; margin:0 auto; z-index:100;">
	<div style="width: 355px; margin: 0 auto; position: relative;">
		<div style="position: absolute; top: 300px; left: 0; border: 1px solid black; background: white; padding: 10px; z-index:101;">
			<div style="padding-left: 0px;">
				<div style="font-family: arial; color: #555555; font-size: 16pt; font-weight: bold;">Get Started!</div>
				<div style="font-family: arial; color: #555555; font-size: 8pt; margin-top: 5px;">
					Use your real name to create an account.  If you are here to represent your business or a product, you can create or claim your professional page after you join.
				</div>
			</div>
			<table border=0 cellpadding=0 cellspacing=0 style="margin-top: 13px;">
				<form method="post" action="/signup/entry.php" id="frmsignup">
				<input type="hidden" name="recaptcha_challenge_field" value="" />
				<input type="hidden" name="recaptcha_response_field" value="" />
				<tr>
					<td style="text-align: right; padding-bottom: 5px; padding-right: 5px; font-family: arial; font-weight: bold; font-size: 8pt; color: #555555;">First &amp; Last Name:</td>
					<td style="padding-bottom: 5px;"><input type="text" maxlength="25" name="uname" id="newuname" value="<?=htmlentities($_POST['uname'])?>" style="border: 1px solid #7F9DB9; width: 170px;"></td>
				</tr>
				<tr>
					<td style="padding-right: 5px; padding-bottom: 5px; text-align: right; font-family: arial; font-weight: bold; font-size: 8pt; color: #555555;">Password:</td>
					<td style="padding-bottom: 5px;"><input type="password" maxlength="25" name="password" style="border: 1px solid #7F9DB9; width: 170px;"></td>
				</tr>
				<tr>
					<td style="text-align: right; padding-bottom: 5px; padding-right: 5px; font-family: arial; font-weight: bold; font-size: 8pt; color: #555555;">E-mail Address:</td>
					<td style="padding-bottom: 5px;"><input type="text" name="email" value="<?=$emailEntered?>" style="border: 1px solid #7F9DB9; width: 170px;"></td>
				</tr>
				<tr>
					<td style="padding-right: 5px; text-align: right; font-family: arial; font-weight: bold; font-size: 8pt; color: #555555;">Birthday:</td>
					<td>
						<select style="width: 62px; height: 21px; border: 1px solid #7F9DB9;" name="month">
							<option>Month</option>
							<option value="1"<? if( $_POST['month'] == 1 ) echo " SELECTED"; ?>>Jan</option>
							<option value="2"<? if( $_POST['month'] == 2 ) echo " SELECTED"; ?>>Feb</option>
							<option value="3"<? if( $_POST['month'] == 3 ) echo " SELECTED"; ?>>Mar</option>
							<option value="4"<? if( $_POST['month'] == 4 ) echo " SELECTED"; ?>>Apr</option>
							<option value="5"<? if( $_POST['month'] == 5 ) echo " SELECTED"; ?>>May</option>
							<option value="6"<? if( $_POST['month'] == 6 ) echo " SELECTED"; ?>>Jun</option>
							<option value="7"<? if( $_POST['month'] == 7 ) echo " SELECTED"; ?>>Jul</option>
							<option value="8"<? if( $_POST['month'] == 8 ) echo " SELECTED"; ?>>Aug</option>
							<option value="9"<? if( $_POST['month'] == 9 ) echo " SELECTED"; ?>>Sep</option>
							<option value="10"<? if( $_POST['month'] == 10 ) echo " SELECTED"; ?>>Oct</option>
							<option value="11"<? if( $_POST['month'] == 11 ) echo " SELECTED"; ?>>Nov</option>
							<option value="12"<? if( $_POST['month'] == 12 ) echo " SELECTED"; ?>>Dec</option>
						</select>
						<select style="width: 50px; height: 21px; border: 1px solid #7F9DB9;" name="day">
							<option>Day</option>
							<?php
							for ($i = 1; $i <= 31; $i++)
		if( $i == $_POST['day'] )
								echo "<option value=$i SELECTED>$i</option>";
		else
								echo "<option value=$i>$i</option>";
							?>
						</select>
						<select style="width: 54px; height: 21px; border: 1px solid #7F9DB9;" name="year">
							<option>Year</option>
							<?php
							for ($i = date("Y") - 18; $i > date("Y") - 100; $i--)
		if( $i == $_POST['year'] )
								echo "<option value=$i SELECTED>$i</option>";
		else
								echo "<option value=$i>$i</option>";
							?>
						</select>
					</td>
				</tr>

				<tr>
					<td colspan=3 style="padding-top: 15px; padding-left: 15px; padding-right:15px; color: #808080; font-family: arial; font-size: 8pt;">
						<div style="margin-top: 10px; text-align: center; border: 1px solid #e2c822; background-color:#fff9d7; padding:5px;">
							* <?= $siteName ?> is a professional network. False or spam accounts will be removed and or blocked.
						</div>
					</td>
				</tr>

				<tr>
					<td colspan=3 style="padding-top: 15px; padding-left: 30px; color: #808080; font-family: arial; font-size: 8pt;">
						By clicking sign up, you agree to the <a href="/privacy.php" style="color: #326798; text-decoration: none;">Privacy Policy</a><br>
							and <a href="/tos.php" style="color: #326798; text-decoration: none;">Terms of Use</a>.
						<div style="margin-top: 10px; text-align: center;">
							<input type="button" onclick="javascript:doCaptcha();" style="height: 23px; width: 62px; background: #FFF8CC; border: 1px solid #FF9A66; color: #3B5998; font-weight: bold; font-size: 9pt; font-family: arial;" value="Sign Up">
						</div>
					</td>
				</tr>

				</form>
			</table>
		</div>
	</div>
</div>

<? } ?>