function showUploadPhoto()
{
	if (site == "s")
	{
		showPhotoChanger();
	}
	else
	{
		document.getElementById("uploadownphoto").style.display = "";
		this.style.display= "none";
	}
}

function changeUserPic(pic)
{
	if (site == "m")
		for (i = 0; i < 3; i++)
		{
			e = document.getElementById("userpic" + i);

			if (e)
			{
				if (i == pic)
				{
					e.style.border = "2px solid #326798";
					cup = document.getElementById("curuserpic");
					if (cup) cup.src = e.src;
				}
				else
					e.style.border = "2px solid #e5ecf3";
			}
		}
	else
	{
		e = document.getElementById("userpic" + pic);
		cup = document.getElementById("curuserpic");
		if (cup) cup.src = e.src;
	}
	
	postAjax("changeuserpic.php", "pic=" + pic, "changeUserPicHandler");
}

function deleteProfilePic(id)
{
	if (confirm("Are you sure you want to delete this profile photo?"))
		postAjax("/settings/deletemedia.php", "type=P&ids" + id + "=on&single=1", function(x)
			{
				document.getElementById("pp" + id).style.display = "none";
			}
		);
}

function deleteMedia()
{
	if (confirm("Are you sure you want to delete the selected media?"))
  {
		postAjax("/settings/deletemedia.php", "type=" + mediaType + "&" + getFormVals(document.forms.media), "deleteMediaHandler");
  }
}

function deleteMediaHandler(data)
{
	//alert(data);
	location.reload(true);
}

function updateMedia(id)
{
	ttitle = document.getElementById("title-" + id).value;
	descr = document.getElementById("descr-" + id).value;

  cat = document.getElementById("cat-" + id).value;
  privacy = document.getElementById("privacy-" + id).value;
	postAjax("updatemedia.php", "id=" + id + "&type=" + mediaType + "&title=" + escape(ttitle) + "&descr=" + escape(descr)  + "&privacy=" + escape(privacy) + "&cat=" + escape(cat), "updateMediaHandler");

	document.getElementById("save-" + id).style.display = "none";
	document.getElementById("wait-" + id).style.visibility = "";
}

function updateMediaHandler(data)
{
	document.getElementById("save-" + data).innerHTML = "saved";
	document.getElementById("save-" + data).style.display = "";
	document.getElementById("wait-" + data).style.visibility = "hidden";
}

function showCharsLeft(id, show)
{
	if (show)
    if( document.getElementById("save-" + id) )
  		document.getElementById("save-" + id).innerHTML = '&nbsp;&nbsp;<a href="javascript:void(0);" onclick="javascript:updateMedia(' + id + ');">save</a>';
	document.getElementById("chars-" + id).style.visibility = show ? "" : "hidden";
}

function charsLeft(id, val)
{
/*
	e = document.getElementById("chars-" + id);
	e.style.visibility = "";

	diff = (104 - val.length);
	if (diff < 0)
		e.innerHTML = (diff * -1) + " characters too long (not twitter compliant)";
	else
		e.innerHTML = diff + " characters remaining";
*/
}