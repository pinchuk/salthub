<?php
/*
Updates a user's album or video title and descriptions.  Called from /settings/photos.php and /settings/videos.php.

*/

include "../inc/inc.php";

if (!$API->isLoggedIn()) die();

$id = intval($_POST['id']);

//$_POST['descr'] = addslashes( str_replace("\n", "", $_POST['descr']) );
$_POST['title'] = addslashes( str_replace("\n", " ", $_POST['title']) );
$cat = intval($_POST['cat']);
$privacy = intval($_POST['privacy']);
if( empty( $cat ) || $cat == 0 ) $cat = 1;

if( $_POST['type'] == "V" )
  mysql_query("update videos set title='" . $_POST['title'] . "',descr='" . $_POST['descr'] . "',privacy='" . $_POST['privacy'] . "'" . ($site == "s" && $cat > 0 ? ",cat=$cat" : "") . " where uid=" . $API->uid . " and id=$id");
else
{
  mysql_query("update albums set title='" . $_POST['title'] . "',descr='" . $_POST['descr'] . "' where uid=" . $API->uid . " and id=$id");

  if( isset( $_POST['privacy'] ) && isset( $_POST['cat'] ) )
  {
    mysql_query("update photos set cat=$cat, privacy=$privacy where uid=" . $API->uid . " and aid=$id");
  }
}
if ($_POST['isyt'] == 1){
    $to = $target > 0 ? $target : $API->uid;
    $API->feedAdd("V", $id, $to, $API->uid, $gid);
}
echo $id;

if ($_POST['isyt'] == "1") //youtube videos get notifications sent here
{
  $err = "OK";

	$x = mysql_query("select title,descr,jmp,id from videos where id=$id");
	if (mysql_num_rows($x) == 0) die();
	
	$vid = mysql_fetch_array($x, MYSQL_ASSOC);
	$vid['type'] = "V";

  $fbtarget = null;
  if( isset( $_POST['fbtarget'] ) )
  {
    $fbtarget = $_POST['fbtarget'];
    $_SESSION['fbpages']['selection'] = $fbtarget;
    if( $fbtarget == 0 )
      $fbtarget = null;
  }
  $vid['fbtarget'] = $fbtarget;

	$err = $API->sendNotification(NOTIFY_VIDEO_UPLOADED, $vid);

	//see if the video was meant to be uploaded to someone else's profile
	$target = intval($_POST['target']);
	if ($target > 0)
		$API->feedAdd("V", $id, $target, $API->uid);

  echo $err;

}


?>