<?php
/*
Step 3 of the signup process, forces the user to add a photo to their profile before gaining access
to the site.
*/

$step=3;
$page_header = "Your Headshot";
include "step-header.php";
?>

<div class="newusercontent">

<div style="float:left; margin-left:5px;"><h1>Upload your headshot</h1></div>

<div style="clear:left; float:left;">

<table border=0 cellpadding=0 cellspacing=0 style="margin-left:5px;">
	<tr valign="top">
		<td width="555">
			<div style="color #555555; width: 450px; font-size: 10pt;">
				<div style="clear: both;">Help people find you and brighten up your profile with a photo of yourself.</div>
				<div style="padding: 15px 0 25px 0; float: left;">
					<img src="<?=$API->getThumbURL(0, 178, 266, $API->getUserPic())?>" alt="">
				</div>
				<div style="float: left; padding-top: 5px; width: 265px; font-size: 9pt;">
					<ul>
					<li>Let's start with your main profile photo.&nbsp; You can add more photos later.</li>
					<li>All you need to do is click &quot;Browse&quot; to locate the photo on your computer and then click on the &quot;Upload Picture&quot; button.</li>
					<? if (quickQuery("select count(*) from users where uid=" . $API->uid . " and password is not null") == 0) { ?><li>Alternatively, you can <a href="/signup/verify.php?resend=1">use your <?=$API->fbid ? "Facebook" : "Twitter"?> profile photo</a>.</li><? } ?>
          <li><a href="/signup/verify.php?resend=1">Use current image</a></li>
					</ul>
				</div>
				<div style="clear: both;"></div>
			</div>

			<form method="post" action="/upload/photo.php?userpic" enctype="multipart/form-data">
			<input type="hidden" name="from" value="/signup/addphoto.php" />
			<input type="hidden" name="next" value="/signup/verify.php?resend=1" />
			<div style="border: 1px solid #808080; width: 425px; padding: 10px;">
				<table border=0 cellpadding=0 cellspacing=0>
					<tr>
						<td style="color: #808080; font-weight: bold; font-size: 9pt; padding-right: 5px;">
							upload image
						</td>
						<td>
							<div style="position: relative; padding-bottom: 20px;">
								<input type="file" name="Filedata" onchange="javascript:fakefile.value = this.value;" style="position: absolute; left: 0px; z-index: 2;" size="40" accept="image/*">
							</div>
						</td>
					</tr>
				</table>
				<div style="text-align: center; margin-top: 15px; color: #808080; font-size: 9pt; width: 370px;">
					You can upload GIF, JPEG, and PNG images
				</div>
				<div style="text-align: center; margin-top: 15px; width: 370px;">
						<input class="button" type="submit" name="button" value="Upload Picture">
				</div>
			</div>
			</form>
		</td>
		<td>
			<div style="background: #f7f7f7; width: 300px;">
				<div style="padding: 20px 15px;">
					<div style="font-family: tahoma; font-size: 11pt; color: #555555; font-weight: bold;">Photo Guidelines</div>
					<div style="margin-bottom: 5px; margin-top: 15px; font-family: tahoma; color: #555555; font-size: 8pt;">
						<b>&#149;</b>&nbsp;&nbsp;&nbsp; We accept images in JPG, GIF, and PNG formats.
					</div>
					<div style="margin-bottom: 5px; font-family: tahoma; color: #555555; font-size: 8pt;">
						<b>&#149;</b>&nbsp;&nbsp;&nbsp; Please do not upload photos that contain nudity, violate copyright laws, or depict hate or gore.
					</div>
					<div style="margin-bottom: 5px; font-family: tahoma; color: #555555; font-size: 8pt;">
						<b>&#149;</b>&nbsp;&nbsp;&nbsp; <a href="javascript:void(0);" onclick="window.open('/tos.php?1', 'Terms of use','location=0,status=0,scrollbars=1,width=580,height=600');">terms of use</a>
					</div>
				</div>
			</div>

		</td>
	</tr>
</table>

</div>

  <div style="clear:both;"></div>
</div>

<?php

if (isset($_GET['invalid_image']))
{
	echo '<script language="javascript" type="text/javascript">showPopUp2("", "You uploaded an invalid photo.<p/>Please try again.", 400);</script>';
}
?>

</body></html>