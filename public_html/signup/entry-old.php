<?php
include "../inc/inc.php";

include "../inc/recaptchalib.php";
$publickey = "6LciL78SAAAAANVcbxyOQtkOkEyJN1YaGs_HUe4M";
$privatekey = "6LciL78SAAAAAKjsEQNYz2kD7Dm__mk4jshxleAk";


$scripts[] = "/index.js";

//Store referral ID and custom tag ID if coming from a tag email/invite
session_start();

if( isset( $_GET['ref'] ) )
{
	$_SESSION['ref'] = $_GET['ref'];
  if( isset( $_GET['cid'] ) )
    $_SESSION['customcid'] = $_GET['cid'];
}

if( isset( $_GET['joingid'] ) )
{
	$_SESSION['joingid'] = $_GET['joingid'];
	$_SESSION['friend_uid'] = $_GET['uid'];
}

$API->requireSite("s");
if ($API->isLoggedIn())
{
	header("Location: /");
	die();
}

if (isset($_POST['email']))
{

	$uname = str_replace(".", "", $_POST['uname']);
	$invalid['uname'] = strlen($uname) < 3 || strlen($uname) > 16 || preg_replace("/[^a-zA-Z0-9. ]/", "", $uname) != $uname;

	$password = $_POST['password'];
	$invalid['password'] = strlen($password) < 5 || strlen($password) > 20;
	
	$email = addslashes(stripslashes($_POST['email']));
	$invalid['email'] = strlen($email) < 3 || strpos($email, "@") === false || strpos($email, ".") === false;
	
	$invalid['dob'] = $_POST['year'] < 1900 || $_POST['year'] > date("Y") || $_POST['month'] < 1 || $_POST['month'] > 12 || $_POST['day'] < 1 || $_POST['day'] > 31;
	$dob = ceil($_POST['year']) . "-" . ceil($_POST['month']) . "-" . ceil($_POST['day']);
	
	$resp = recaptcha_check_answer($privatekey, SERVER_HOST, $_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"]);
	$invalid['captcha'] = !$resp->is_valid;

  $invalid['company'] = quickQuery( "select count(*) from pages where gname='" . $_POST['uname'] . "'" );


	if (!$invalid['uname'])
		$invalid['uname-taken'] = quickQuery("select count(*) from users where username='$uname'") > 0;
	
	if (!$invalid['email'])
	{
		$x = sql_query("select username,uid from users where email='$email'");

		if (mysql_num_rows($x) > 0)
		{
			$y = mysql_fetch_array($x, MYSQL_ASSOC);
			if ($y['username'] == null)
			{
				$_SESSION['uidpending'] = $y['uid'];
				$invalid['email-taken'] = false;
			}
			else
				$invalid['email-taken'] = true;
		}
	}

	foreach ($invalid as $cat => $b)
		if ($b)
			switch ($cat)
			{
				case "uname-taken":
					$error[] = "The user name you have chosen is already in use.";
					break;

				case "email-taken":
					$error[] = "The e-mail address you have entered is already in use.";
					break;
				
				case "uname":
					$error[] = "User names must between 3 and 16 characters and contain only letters, numbers, and a period.";
					break;

				case "password":
					$error[] = "Your password must be between 5 and 20 characters.";
					break;

				case "email":
					$error[] = "The e-mail address you have entered is invalid.";
					break;

				case "dob":
					$error[] = "You have chosen an invalid date of birth.";
					break;
					
				case "captcha":
					$error[] = "You failed the reCAPTCHA challenge.";
					break;

        case "company":
          $error[] = "The name that you've chosen does not conform to our terms of use. Please use your real name to register. You will be able to add your company, vessel, or other interest after joining $siteName";
          break;
			}
			
	if (!isset($error))
	{
		unset($query);
		
		if (isset($_SESSION['uidpending']))
		{
			$xx = sql_query("select username from users where uid=" . intval($_SESSION['uidpending']));
			if (mysql_num_rows($xx) == 1)
			{
				if (end(mysql_fetch_array($xx, MYSQL_ASSOC)) == null)
				{
					$_SESSION['uid'] = intval($_SESSION['uidpending']);
					$API->uid = $_SESSION['uid'];
					$query = "update users set username='$uname',name='" . str_replace(".", " ", $uname) . "',password=md5('$password'),email='$email',active=2,dob='$dob' where uid=" . $_SESSION['uid'];
				}
			}
		}

		if (!isset($query))
			$query = "insert into users (lastlogin,joined,username,name,password,email,active,dob) values (now(),now(), \"" . join("\", \"", array($uname, $uname, md5($password), $email, 0, $dob)) . "\")";

		sql_query($query);

		if (empty($_SESSION['uid']))
		{
			$_SESSION['uid'] = mysql_insert_id();
			$API->uid = $_SESSION['uid'];
		}

		$_SESSION['username'] = $uname;
		$_SESSION['name'] = str_replace(".", " ", $uname);
		$_SESSION['pic'] = SITE_OURS;
		$_SESSION['needsVerified'] = 1;
		$_SESSION['verify'] = 0;
		
		$API->username = $_SESSION['username'];
		$API->name = $_SESSION['name'];
		$API->pic = $_SESSION['pic'];

		$API->uid = $_SESSION['uid'];
		$API->createSpecialAlbums();

		$ref = $_SESSION['ref'];

		if( isset( $_SESSION['joingid'] ) )
    {
  		$gid = intval($_SESSION['joingid']);
  		sql_query("insert into page_members (gid,uid) values ({$gid},$API->uid)");
      setcookie( "joingid", $gid, time()+60*60*24*30 );
      setcookie( "friend_uid", $_SESSION['friend_uid'], time()+60*60*24*30 );
    }

//    This is done in addReferral()
//		if ($ref > 0)
//			sql_query("insert into friends (id1,id2,status) values (" . $_SESSION['uid'] . "," . $ref . ",0)");
    $API->addReferral();
    $API->prepWelcomeEmail();


		//$API->sendConfirmationEmail($email);

		header("Location: /signup/invite-ps.php");
	}
}

if (empty($_SESSION['uidpending']))
	$emailEntered = htmlentities($_POST['email']);
else
	$emailEntered = quickQuery("select email from users where uid=" . $_SESSION['uidpending']);

include "newheader.php";


//Banners code from mediaBirdy
$banners = array();
$x = sql_query("select * from banners order by id");
while ($y = mysql_fetch_array($x, MYSQL_ASSOC))
{
	if (file_exists("../images/banners/" . $y['id'] . ".png"))
		$banners[$y['id']] = $y['link'];
}

?>

<div style="height: 27px; background: #FFF7C2; padding-top: 6px; font-size: 11pt; font-family: arial; margin-bottom: 17px;">
	<div style="width: 902px; margin: 0 auto;">
		<div class="logintxt">
			Sign in with <a href="/fbconnect/login.php"><img src="/images/facebook16.png" /></a> <a href="/twitter/login.php"><img src="/images/twitter16.png" /></a> or <a href="javascript:void(0);" onclick="javascript:getStarted();">Create a New Account</a>.
		</div>
		<form method="post" id="frmlogin" action="/signup/login.php">
			<div class="divlogin">
				Returning user&nbsp;
				<input type="text" name="uname" value="your username" onfocus="javascript:if (this.value == 'your username') this.value = '';" onblur="javascript:if (this.value == '') this.value = 'your username';" />
				<span id="txtpassword_container"><input type="text" value="your password" onfocus="javascript:showPasswordBox();" /></span>&nbsp;
				<a href="javascript:void(0);" onclick="javascript:document.getElementById('frmlogin').submit();">Login</a>&nbsp;
				<a href="/signup/resetpw.php"><img src="/images/help.png" alt="" /></a>
			</div>
		</form>
	</div>
</div>

<div style="width: 906px; margin: 0 auto;">
	<div style="width: 552px; border: 2px solid #C0C0C0; border-right: 0; float: left; background: #F7F7F7; height: 254px; padding: 7px 0;">
		<div style="padding-left: 4px;" class="frontuserpics">
			<?php
			$x = sql_query("select uid,pic from users where active=1 and pic > 2 order by rand() limit 10");
			while ($user = mysql_fetch_array($x, MYSQL_ASSOC))
			{
				?>
				<div><img src="<?=$API->getThumbURL(1, 48, 48, $API->getUserPic($user['uid'], $user['pic']))?>" /></div>
				<?php
			}
			?>
		</div>
		<div style="clear: both; padding: 25px 0 0 17px;" class="frontoverview">
			<div class="section">
				<div class="title">Businesses</div>
				<div class="text">
					Post news and media to Facebook and Twitter.<p />
					Claim or create your company page.<p />
					Connect with new and existing customers.
				</div>
			</div>
			<div class="section">
				<div class="title">Enthusiasts</div>
				<div class="text">
					Share photos and videos with your friends.<p />
					Connect with others who have the same interests.<p />
					Follow your favorite vessels and companies.
				</div>
			</div>
			<div class="section">
				<div class="title">Professionals</div>
				<div class="text">
					Connect with old and new colleagues.<p />
					Post your resume and find work.<p />
					Exchange ideas and knowledge with others.
				</div>
			</div>
		</div>
	</div>
	<div style="border: 2px solid #C0C0C0; border-left: 0; background: #f7f7f7; width: 348px; float: left; padding: 5px 0;">
<!--
		<div style="margin: 0 auto; width: 335px;">
			<?php showVideoPlayer(335, 258, array("id" => -1, "hash" =>"/videos/" . $site . "/entry/" . quickQuery("select id from entry_videos where display=1"))); ?>
		</div>
-->
  	<?php
  	$i = 0;
  	foreach ($banners as $id => $link)
  	{
  		?>
  		<div id="banner-<?=$i?>" style="text-align: center; height:232; padding: 5px 0 10px 0; <?=$i == 0 ? "" : " display: none;"?>">
  			<a href="<?=$link?>"><img src="/images/banners/<?=$id?>.png" alt="" width="335" height="232"/></a>
  		</div>
  		<?php
  		$i++;
  	}
  	?>
  	<div style="text-align: center; padding: 0 0 5px 10px;">
  		<?php
  		$i = 0;
  		foreach ($banners as $id => $link)
  		{
  		?><img id="banner-sq-<?=$i?>" src="/images/square<?=$i == 0 ? "2" : ""?>.png" onmouseout="javascript:bannerOut();" onmouseover="javascript:bannerOver(<?=$i?>);" style="padding-right: 10px;" alt="" /><?php
  		$i++;
  		}
  		?>
  	</div>

    <div style="clear:both;"></div>
	</div>
	
	<div style="height: 10px; clear: both;"></div>

	<div style="border: 1px solid #C0C0C0; background: #F7F7F7;">
		<div style="float: left; font-size: 12pt; font-weight: bold; color: #555; padding: 12px 5px;">
			Trending<br />Now
		</div>
		<div style="padding: 5px; margin: 5px; background: white; float: left;">
			<table border="0" cellpadding="0" cellspacing="0" width="470">
				<tr>
					<td width="1"><div class="searchterm" style="font-size: 14pt; font-weight: bold;">yachts</div></td>
					<td></td><td width="1"><div class="searchterm" style="font-size: 8pt;">kite boarding</div></td>
					<td></td><td width="1"><div class="searchterm" style="font-size: 12pt; font-weight: bold;">sharks</div></td>
					<td></td><td width="1"><div class="searchterm" style="font-size: 9pt;">offshore racing</div></td>
					<td></td><td width="1"><div class="searchterm" style="font-size: 14pt; font-weight: bold;">brokers</div></td>
					<td></td><td width="1"><div class="searchterm" style="font-size: 9pt;">caribbean</div></td>
					<td></td><td width="1"><div class="searchterm" style="font-size: 12pt; font-weight: bold;">crew</div></td>
				</tr>
			</table>
			<table border="0" cellpadding="0" cellspacing="0" width="470">
				<tr>
					<td width="1"><div class="searchterm" style="font-size: 12pt; font-weight: bold;">ships</div></td>
					<td></td><td width="1"><div class="searchterm" style="font-size: 10pt;">webcams</div></td>
					<td></td><td width="1"><div class="searchterm" style="font-size: 14pt; font-weight: bold;">surfing</div></td>
					<td></td><td width="1"><div class="searchterm" style="font-size: 9pt; font-weight: bold;">beach resorts</div></td>
					<td></td><td width="1"><div class="searchterm" style="font-size: 12pt;">us navy</div></td>
					<td></td><td width="1"><div class="searchterm" style="font-size: 14pt;">boating</div></td>
					<td></td><td width="1"><div class="searchterm" style="font-size: 11pt;">jobs</div></td>
				</tr>
			</table>
			<table border="0" cellpadding="0" cellspacing="0" width="470">
				<tr>
					<td width="1"><div class="searchterm" style="font-size: 10pt;">cruise ships</div></td>
					<td></td><td width="1"><div class="searchterm" style="font-size: 12pt; font-weight: bold;">boats for sale</div></td>
					<td></td><td width="1"><div class="searchterm" style="font-size: 12pt;">USCG</div></td>
					<td></td><td width="1"><div class="searchterm" style="font-size: 14pt; font-weight: bold;">fishing</div></td>
					<td></td><td width="1"><div class="searchterm" style="font-size: 8pt; font-weight: bold;">charter boats</div></td>
					<td></td><td width="1"><div class="searchterm" style="font-size: 14pt; font-weight: bold;">scuba</div></td>
				</tr>
			</table>
		</div>
		<div style="float: left; width: 328px; padding-top: 10px;">
			<div style="height: 35px; line-height: 35px; font-weight: bold; font-size: 14pt; text-align: center;">
				Videos Viewed on <?=$siteName?>!
			</div>
			<div id="divodometer" style="width: 200px; margin: 0 auto;"></div>
		</div>
		<div style="clear: both;"></div>
	</div>

  <? include "newfooter.php"; ?>
</div>


</div>

<form id="frmrecaptcha">
	<div id="recaptchaoldparent" style="display: none;">
		<div id="recaptcha" style="display: none;">
		<?=recaptcha_get_html($publickey)?>
		</div>
	</div>
</form>

<div id="getstarted" style="width: 902px; margin: 0 auto; display: none;">
	<div style="width: 355px; margin: 0 auto; position: relative;">
		<div style="position: absolute; top: -352px; left: 0; border: 1px solid black; background: white; padding: 10px;">
			<div style="padding-left: 0px;">
				<div style="font-family: arial; color: #555555; font-size: 16pt; font-weight: bold;">Get Started!</div>
				<div style="font-family: arial; color: #555555; font-size: 8pt; margin-top: 5px;">
					Use your real name to create an account.  If you are here to represent your business or a product, you can create or claim your professional page after you join.
				</div>
			</div>
			<table border=0 cellpadding=0 cellspacing=0 style="margin-top: 13px;">
				<form method="post" action="/signup/entry.php" id="frmsignup">
				<input type="hidden" name="recaptcha_challenge_field" value="" />
				<input type="hidden" name="recaptcha_response_field" value="" />
				<tr>
					<td style="text-align: right; padding-bottom: 5px; padding-right: 5px; font-family: arial; font-weight: bold; font-size: 8pt; color: #555555;">First &amp; Last Name:</td>
					<td style="padding-bottom: 5px;"><input type="text" maxlength="16" name="uname" id="newuname" value="<?=htmlentities($_POST['uname'])?>" style="border: 1px solid #7F9DB9; width: 170px;"></td>
				</tr>
				<tr>
					<td style="padding-right: 5px; padding-bottom: 5px; text-align: right; font-family: arial; font-weight: bold; font-size: 8pt; color: #555555;">Password:</td>
					<td style="padding-bottom: 5px;"><input type="password" maxlength="16" name="password" style="border: 1px solid #7F9DB9; width: 170px;"></td>
				</tr>
				<tr>
					<td style="text-align: right; padding-bottom: 5px; padding-right: 5px; font-family: arial; font-weight: bold; font-size: 8pt; color: #555555;">E-mail Address:</td>
					<td style="padding-bottom: 5px;"><input type="text" name="email" value="<?=$emailEntered?>" style="border: 1px solid #7F9DB9; width: 170px;"></td>
				</tr>
				<tr>
					<td style="padding-right: 5px; text-align: right; font-family: arial; font-weight: bold; font-size: 8pt; color: #555555;">Birthday:</td>
					<td>
						<select style="width: 62px; height: 21px; border: 1px solid #7F9DB9;" name="month">
							<option>Month</option>
							<option value="1"<? if( $_POST['month'] == 1 ) echo " SELECTED"; ?>>Jan</option>
							<option value="2"<? if( $_POST['month'] == 2 ) echo " SELECTED"; ?>>Feb</option>
							<option value="3"<? if( $_POST['month'] == 3 ) echo " SELECTED"; ?>>Mar</option>
							<option value="4"<? if( $_POST['month'] == 4 ) echo " SELECTED"; ?>>Apr</option>
							<option value="5"<? if( $_POST['month'] == 5 ) echo " SELECTED"; ?>>May</option>
							<option value="6"<? if( $_POST['month'] == 6 ) echo " SELECTED"; ?>>Jun</option>
							<option value="7"<? if( $_POST['month'] == 7 ) echo " SELECTED"; ?>>Jul</option>
							<option value="8"<? if( $_POST['month'] == 8 ) echo " SELECTED"; ?>>Aug</option>
							<option value="9"<? if( $_POST['month'] == 9 ) echo " SELECTED"; ?>>Sep</option>
							<option value="10"<? if( $_POST['month'] == 10 ) echo " SELECTED"; ?>>Oct</option>
							<option value="11"<? if( $_POST['month'] == 11 ) echo " SELECTED"; ?>>Nov</option>
							<option value="12"<? if( $_POST['month'] == 12 ) echo " SELECTED"; ?>>Dec</option>
						</select>
						<select style="width: 50px; height: 21px; border: 1px solid #7F9DB9;" name="day">
							<option>Day</option>
							<?php
							for ($i = 1; $i <= 31; $i++)
		if( $i == $_POST['day'] )
								echo "<option value=$i SELECTED>$i</option>";
		else
								echo "<option value=$i>$i</option>";
							?>
						</select>
						<select style="width: 54px; height: 21px; border: 1px solid #7F9DB9;" name="year">
							<option>Year</option>
							<?php
							for ($i = date("Y"); $i > date("Y") - 100; $i--)
		if( $i == $_POST['year'] )
								echo "<option value=$i SELECTED>$i</option>";
		else
								echo "<option value=$i>$i</option>";
							?>
						</select>
					</td>
				</tr>

				<tr>
					<td colspan=3 style="padding-top: 15px; padding-left: 15px; padding-right:15px; color: #808080; font-family: arial; font-size: 8pt;">
						<div style="margin-top: 10px; text-align: center; border: 1px solid #e2c822; background-color:#fff9d7; padding:5px;">
							* <?= $siteName ?> is a professional network. False or spam accounts will be removed and or blocked.
						</div>
					</td>
				</tr>

				<tr>
					<td colspan=3 style="padding-top: 15px; padding-left: 30px; color: #808080; font-family: arial; font-size: 8pt;">
						By clicking sign up, you agree to the <a href="/privacy.php" style="color: #326798; text-decoration: none;">Privacy Policy</a><br>
							and <a href="/tos.php" style="color: #326798; text-decoration: none;">Terms of Use</a>.
						<div style="margin-top: 10px; text-align: center;">
							<input type="button" onclick="javascript:doCaptcha();" style="height: 23px; width: 62px; background: #FFF8CC; border: 1px solid #FF9A66; color: #3B5998; font-weight: bold; font-size: 9pt; font-family: arial;" value="Sign Up">
						</div>
					</td>
				</tr>

				</form>
			</table>
		</div>
	</div>
</div>

<script language="javascript">
<!--

var banners = new Array();
<?php
$i = 0;
foreach ($banners as $id => $link)
{
	if ($i == 0)
		echo "var currentBanner = $i;\n";
	echo "banners[" . $i++ . "] = [$id, '$link'];\n";
}
?>

setInterval("nextBanner()", 7000);


function showPasswordBox()
{
	document.getElementById("txtpassword_container").innerHTML = '<input type="password" id="txtpassword" name="password" onkeypress="javascript:return trySubmit(this,event);" />';
	document.getElementById("txtpassword").focus();
}

function doCaptcha()
{
  var pswd = new String( document.forms['frmsignup'].elements['password'].value );
  var uname = new String( document.forms['frmsignup'].elements['uname'].value );
  var email = new String( document.forms['frmsignup'].elements['email'].value );

  if( uname.length < 3 | uname.length > 16 )
  {
    showPopUp2( "Whoops!",  "<p>User names must between 3 and 16 characters and contain only letters, spaces, numbers, and a period.", 500);
    return false;
  }

  if( uname.length == uname.replace(" ", "").length )
  {
    showPopUp2( "Whoops!",  "<p>Please enter your first and last name.", 500);
    return false;
  }

  if( pswd.length < 5 | pswd.length > 20 )
  {
    showPopUp2( "Whoops!",  "<p>Your password must be between 5 and 20 characters.", 500);
    return false;
  }

  if( document.forms['frmsignup'].elements['year'][0].selected == true | document.forms['frmsignup'].elements['month'][0].selected == true | document.forms['frmsignup'].elements['day'][0].selected == true )
  {
    showPopUp2( "Whoops!",  "<p>You have chosen an invalid date of birth.", 500);
    return false;
  }

  if( email.length < 4 )
  {
    showPopUp2( "Whoops!",  "<p>The e-mail address you have entered is invalid.", 500);
    return false;
  }

	showPopUp("Please verify that you\'re human", '<center><div id="recaptchaparent" style="padding: 5px 0;"></div><input type="button" class="button" value="Sign Up" onclick="javascript:submitSignUp();" /> &nbsp;&nbsp; <input type="button" class="button" onclick="javascript:closeCaptcha();" value="Cancel" /></center>', 0, "no", true);

	e = document.getElementById("recaptcha");
	document.getElementById("recaptchaparent").appendChild(e);
	e.style.display = "";
}

function submitSignUp()
{
	closeCaptcha();

	vals = getFormVals(document.forms['frmrecaptcha']).split("&");
	for (var i in vals)
	{
		x = vals[i].split("=");
		eval("document.forms['frmsignup']." + x[0] + ".value = '" + x[1] + "';");
	}

	document.forms['frmsignup'].submit();
}

function closeCaptcha()
{
	e = document.getElementById("recaptcha");
	document.getElementById("recaptchaoldparent").appendChild(e);
	e.style.display = "";
	
	closePopUp();
}

var views = <?=quickQuery("select sum(views) from videos")?>;
var new_views = 0;

var odometer = new Odometer(document.getElementById("divodometer"), {digits: 10, bustedness: 1, value: views, digitWidth: 20, digitHeight: 25, fontStyle: 'font-family: Courier New, Courier, monospace; font-weight: 900; font-size: 18pt;'});

function scrollOdometer()
{
	views += 0.0025;
	odometer.set(views);

	if (views < new_views)
		setTimeout("scrollOdometer();", 0);
}

function updateOdometer()
{
	getAjax("/signup/gettotviews.php?", function (data)
		{
			new_views = parseInt(data);

			if (views == 0)
			{
				odometer.set(new_views);
				views = new_views;
			}
			else
			{
				scrollOdometer();
			}
		}
	);
}

function trySubmit(txt, e)
{
	var keycode;

	if (window.event)
		keycode = window.event.keyCode;
	else if (e)
		keycode = e.which;
	else
		return true;

	if (keycode == 13)
	{
		txt.form.submit();
		return false;
	}
	else
		return true;
}

function getStarted()
{
	document.getElementById("getstarted").style.display = "";
	document.getElementById("newuname").focus();
}

setInterval("updateOdometer()", 5000);

<?php
if ($_GET['login'] == "0")
	echo 'showPopUp2("Whoops!", "Either your password or user name is incorrect.", 500);';
elseif (isset($error))
{
  echo 'showPopUp("Whoops!", "' . implode("<p>", $error) . '", 500, "no", false, getStarted );';
}

if ($_GET['fb'] == "1")
	echo 'FB.Connect.requireSession(fbloggedin);';
?>

//-->
</script>

</body></html>
