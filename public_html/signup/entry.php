<?php

include "../inc/inc.php";

/*include "../inc/recaptchalib.php";

$publickey = "6LciL78SAAAAANVcbxyOQtkOkEyJN1YaGs_HUe4M";
$privatekey = "6LciL78SAAAAAKjsEQNYz2kD7Dm__mk4jshxleAk";*/

//Store referral ID and custom tag ID if coming from a tag email/invite
//session_start();

if( isset( $_GET['ref'] ) )
{
	$_SESSION['ref'] = $_GET['ref'];
  if( isset( $_GET['cid'] ) )
    $_SESSION['customcid'] = $_GET['cid'];
}

if( isset( $_GET['joingid'] ) )
{
	$_SESSION['joingid'] = $_GET['joingid'];
	$_SESSION['friend_uid'] = $_GET['uid'];
}

$API->requireSite("s");
if ($API->isLoggedIn())
{
	header("Location: /");
	die();
}

if (isset($_POST['email']))
{

	$uname = str_replace(".", "", $_POST['uname']);
    $uname = str_replace("-", " ", $_POST['uname']);
	$invalid['uname'] = strlen($uname) < 3 || strlen($uname) > 25 || preg_replace("/[^a-zA-Z0-9. ]/", "", $uname) != $uname;

	$password = $_POST['password'];
	$invalid['password'] = strlen($password) < 5 || strlen($password) > 20;

	$email = addslashes(stripslashes($_POST['email']));
	$invalid['email'] = strlen($email) < 3 || strpos($email, "@") === false || strpos($email, ".") === false;

	$invalid['dob'] = $_POST['year'] < 1900 || $_POST['year'] > date("Y") || $_POST['month'] < 1 || $_POST['month'] > 12 || $_POST['day'] < 1 || $_POST['day'] > 31;
	$dob = ceil($_POST['year']) . "-" . ceil($_POST['month']) . "-" . ceil($_POST['day']);
	
	//$resp = recaptcha_check_answer($privatekey, SERVER_HOST, $_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"]);
	
	$word_ok = false;
	if(!empty($_SESSION['freecap_word_hash']) && !empty($_POST['freecap']))
	{
		if ($_SESSION['hash_func'](strtolower($_POST['freecap'])) == $_SESSION['freecap_word_hash'])
		{
			$_SESSION['freecap_attempts'] = 0;
			$_SESSION['freecap_word_hash'] = false;

			$word_ok = true;
		}
	}
	
	$invalid['captcha'] = !$word_ok;

    $invalid['company'] = quickQuery( "select count(*) from pages where gname='" . $_POST['uname'] . "'" );


	//if (!$invalid['uname'])
		//$invalid['uname-taken'] = quickQuery("select count(*) from users where replace(username,' ', '.')='" . str_replace(' ', '.', $val) . "'");

    if (!$invalid['uname']){
        $x = sql_query("SELECT username FROM users WHERE username = '".$_POST['uname']."'");
        if (mysql_num_rows($x) > 0)
            $invalid['uname-taken'] = true;
    }

	if (!$invalid['email'])
	{
		$x = sql_query("select username,uid from users where email='$email'");

		if (mysql_num_rows($x) > 0)
		{
			$y = mysql_fetch_array($x, MYSQL_ASSOC);
			if ($y['username'] == null)
			{
				$_SESSION['uidpending'] = $y['uid'];
				$invalid['email-taken'] = false;
			}
			else
				$invalid['email-taken'] = true;
		}
	}

	foreach ($invalid as $cat => $b)
		if ($b)
			switch ($cat)
			{
				case "uname-taken":
					$error[] = "The user name you have chosen is already in use.";
					break;

				case "email-taken":
					$error[] = "The e-mail address you have entered is already in use.";
					break;

				case "uname":
					$error[] = "User names must between 3 and 25 characters and contain only letters, numbers, and a period.";
					break;

				case "password":
					$error[] = "Your password must be between 5 and 20 characters.";
					break;

				case "email":
					$error[] = "The e-mail address you have entered is invalid.";
					break;

				case "dob":
					$error[] = "You have chosen an invalid date of birth.";
					break;
					
				case "captcha":
					$error[] = "The word you typed from the<br>image (captcha) was not correct.";
					break;

        case "company":
          $error[] = "The name that you've chosen does not conform to our terms of use. Please use your real name to register. You will be able to add your company, vessel, or other interest after joining $siteName";
          break;
			}

	if (!isset($error))
	{
		unset($query);
		
		if (isset($_SESSION['uidpending']))
		{
			$xx = sql_query("select username from users where uid=" . intval($_SESSION['uidpending']));
			if (mysql_num_rows($xx) == 1)
			{
				if (end(mysql_fetch_array($xx, MYSQL_ASSOC)) == null)
				{
					$_SESSION['uid'] = intval($_SESSION['uidpending']);
					$API->uid = $_SESSION['uid'];
					$query = "update users set username='$uname',name='" . str_replace(".", " ", $uname) . "',password=md5('$password'),email='$email',active=2,dob='$dob' where uid=" . $_SESSION['uid'];
				}
			}
		}

		if (!isset($query))
			$query = "insert into users (lastlogin,joined,username,name,password,email,active,dob) values (now(),now(), \"" . join("\", \"", array($uname, $uname, md5($password), $email, 0, $dob)) . "\")";

		sql_query($query);

		if (empty($_SESSION['uid']))
		{
			$_SESSION['uid'] = mysql_insert_id();
			$API->uid = $_SESSION['uid'];
		}

        $uname = str_replace("-", " ", $_POST['uname']);
		$_SESSION['username'] = $uname;
		$_SESSION['name'] = str_replace(".", " ", $uname);
		$_SESSION['pic'] = SITE_OURS;
		$_SESSION['needsVerified'] = 1;
		$_SESSION['verify'] = 0;
		
		$API->username = $_SESSION['username'];
		$API->name = $_SESSION['name'];
		$API->pic = $_SESSION['pic'];

		$API->uid = $_SESSION['uid'];
		$API->createSpecialAlbums();

		$ref = $_SESSION['ref'];

		if( isset( $_SESSION['joingid'] ) )
    {
  		$gid = intval($_SESSION['joingid']);
  		sql_query("insert into page_members (gid,uid) values ({$gid},$API->uid)");
      setcookie( "joingid", $gid, time()+60*60*24*30 );
      setcookie( "friend_uid", $_SESSION['friend_uid'], time()+60*60*24*30 );
    }

//    This is done in addReferral()
//		if ($ref > 0)
//			sql_query("insert into friends (id1,id2,status) values (" . $_SESSION['uid'] . "," . $ref . ",0)");
    $API->addReferral();
    $API->prepWelcomeEmail();

    $uid = $API->uid;
    include( "../inc/create_you_have_connections_email.php" );

		$API->sendConfirmationEmail($email);

		header("Location: /welcome");
    exit;

//As of 10-31-2012, the user is forwarded to the welcome page
//Where the remainder of the sign up process happens.
//		header("Location: /signup/update_profile.php");
	}
}

if (empty($_SESSION['uidpending']))
	$emailEntered = htmlentities($_POST['email']);
else
	$emailEntered = quickQuery("select email from users where uid=" . $_SESSION['uidpending']);

include "newheader.php";

//Banners code from mediaBirdy
$banners = array();
$x = sql_query("select * from banners order by page,id");
while ($y = mysql_fetch_array($x, MYSQL_ASSOC))
	$banners[$y['page']][$y['id']] = $y['link'];


include "../header_gradient.php";

?>

<div class="graygradient">
	<div class="headcontainer">
		<div style="width: 3210px; position: relative;"><!-- width = 1070 x #slides -->
			<div id="slide1" class="entryslide" style="left: 1070px;">
				<?=$htmlSignIn?>
				<div class="slidecontent">
					<span class="title">Inside <?=$siteName?></span>
					<div class="text" style="width: 525px;">
						<?=$siteName?> is an easy to use platform for Professional Mariners and Maritime Businesses. The site has been designed to bring like-minded professionals together and allow users to freely navigate across 9 maritime industries.
						<p>
						<?=$siteName?> allows its users to drill down and find information, Vessels, Businesses, and Professionals specific to the industry they work in. Users may explore and interact with Businesses and Professionals in parallel maritime industries. This is done through profiles, professional pages, our maritime directory (5,000+ companies), job postings, the employee directory, vessel directory (100,000+ vessels), our advertising platform, photos, videos and more.
					</div>
				</div>
				<div style="float: left; padding: 30px 0 0 30px;">
					<div style="position: relative;">
						<div style="padding: 3px 0 0 40px; left: 40px; position: relative; width: 430px; height: 370px; overflow: hidden;">
							<?php
							$i = 0;
							foreach ($banners['inside'] as $id => $link)
							{
								?>
								<div id="banner0-<?=$i?>" style="position: absolute; text-align: center; left: <?=470 * $i?>px; padding: 5px 0 10px 0;">
									<a href="<?=$link?>"><img src="/images/banners/inside<?=$id?>.jpg" alt="" style="width: 470px; height: 285px;" /></a>
								</div>
								<?php
								$i++;
							}
							?>
							<div style="text-align: center; position: absolute; top: 310px; width: 390px; padding: 35px 0 0px 10px;">
								<?php
								$i = 0;
								foreach ($banners['inside'] as $id => $link)
								{
								?><img id="banner0-sq-<?=$i?>" src="/images/square<?=$i == 0 ? "2" : ""?>.png" onmouseout="javascript:bannerOut(0);" onmouseover="javascript:bannerOver(0, <?=$i?>);" style="padding-right: 10px;" alt="" /><?php
								$i++;
								}
								?>
							</div>
						</div>
						<div style="position: absolute; top: 0; left: 0;">
							<img src="/images/compy.png" alt="" />
						</div>
					</div>
				</div>
			</div>
			<div id="slide2" class="entryslide" style="left: 2140px;">

				<div class="slidecontent">
				<?=$htmlSignIn?>

					<span class="title">Business Benefits</span>
					<div class="text" style="width: 480px;">
            The greatest assets of your business are your customers, people and knowledge. The <?=$siteName?> platform brings them together in one central location and enables your business to transform. Work Smarter, Faster, Better. Outpace the competition by extending frontline visibility across your industry to achieve efficiency and align yourself with decision makers. Drive your B2B + B2C relations forward with the <?=$siteName?> platform.

					</div>

						<div class="quotecontainer">
							<div class="quote">
								<div class="open"></div>
								<div class="text" style="width:440px;">We use SaltHub to differentiate ourselves and to connect with professionals and businesess that matter most to our success.</div>
								<div class="close"></div>
								<div class="person">&mdash; Chris, <?=$siteName?> member since 2011</div>
							</div>
						</div>

				</div>


				<div style="float: left; padding: 30px 0 0 45px;">
          <img src="/images/business_benefits.png" width="521" height="320" alt="" />
        </div>

			</div>

			<div id="slide3" class="entryslide" style="left: 3210px;">

				<div class="slidecontent">
				<?=$htmlSignIn?>

					<span class="title">Career Benefits</span>
					<div class="text" style="width: 480px;">
            In today's world there are two required components to elevate your career. The first is a positive online presence, and the second is a professional social platform where you can leverage your relationships and knowledge. The <?=$siteName?> network provides these components and enables professionals in the Maritime Industries to take their careers to the next level. Use <?=$siteName?> to build professional relationships and be discovered.
					</div>

						<div class="quotecontainer">
							<div class="quote">
								<div class="open"></div>
								<div class="text" style="width:440px;">SaltHub has elevated my career by keeping me informed about industry news and new job openings</div>
								<div class="close"></div>
								<div class="person">&mdash; Whitney, <?=$siteName?> member since 2011</div>
							</div>
						</div>

				</div>


				<div style="float: left; padding: 30px 0 0 45px;">
          <img src="/images/career_benefits.png" width="521" height="320" alt="" />
        </div>

			</div>
<!--
      <div id="slide4" class="entryslide" style="left: 4212px;">
				<img src="http://i.huffpost.com/gen/597150/thumbs/a-JET-PLOT-386x217.jpg">
			</div>
-->
			<div id="slide0" class="entryslide">
				<?=$htmlSignIn?>
				<div style="width: 507px; float: left;">
					<div class="slidecontent">
						<span class="title">What is <?=$siteName?>?</span>

						<div class="text">
                            A Career Management Utility and Business Marketing Platform for those in the Maritime Industry. Create your free account today to connect with professionals, businesses and explore new opportunities.
						</div>

						<div class="quotecontainer">
							<div class="quote">
								<div class="open"></div>
								<div class="text">A must have tool for reaching decision makers and<br />your peers.&nbsp; Build your career - build your business.</div>
								<div class="close"></div>
								<div class="person">&mdash; Andy, <?=$siteName?> member since 2012</div>
							</div>
						</div>
					</div>
					<!--
					<div style="background: #FFFEEA; border: 1px solid #7f7f7f; padding: 5px; width: 480px; margin-top: 20px;">
						<div style="color: #000; font-weight: bold; font-size: 10pt;">Used by professionals and businesses from:</div>
						<div style="color: #326798; font-size: 10pt; padding: 4px 0 10px 0;">
							Yachting - Commercial Shipping - Cruise Shipping - Commercial Fishing<br />
							Oil &amp; Gas Drilling - Port &amp; Marina Management - Rec Fishing &amp; Boating
						</div>
						-->

						<div class="frontuserpics" style="margin-left: -4px; margin-top: -10px;">
						<?php
						$used_images = array();
						for( $c = 0; $c < 9; $c++ )
								{
						  do {
							$image = rand( 1, 87 );
						  } while( in_array( $image, $used_images ) );
						  $used_images[] = $image;
									?>
									<!--<div><img style="height: 48px; width: 48px;" src="/images/signup/<? echo $image ?>.jpg" alt></div>-->
									<div style="background-position: <?=$image * 48?>px 0;"></div>
									<?php
								}
						?>
						</div>
						<div style="clear: both;"></div>
					<!--
					</div>
					-->
				</div>
				<div style="float: left; padding-top: 25px;">
					<div style="position: relative;">
						<div style="left: 20px; position: relative; width: 520px; height: 370px; overflow: hidden;">
							<?php
							$i = 0;
							foreach ($banners['whatis'] as $id => $link)
							{
								?>
								<div id="banner1-<?=$i?>" style="position: absolute; text-align: center; left: <?=520 * $i?>px; padding: 5px 0 10px 0;">
									<a href="<?=$link?>"><img src="/images/banners/whatis<?=$id?>.jpg" alt="" style="width: 520px; height: 320px;" /></a>
								</div>
								<?php
								$i++;
							}
							?>
							<div style="text-align: center; position: absolute; top: 310px; width: 520px; padding: 35px 0 0px 10px;">
								<?php
								$i = 0;
								foreach ($banners['whatis'] as $id => $link)
								{
								?><img id="banner1-sq-<?=$i?>" src="/images/square<?=$i == 0 ? "2" : ""?>.png" onmouseout="javascript:bannerOut(1);" onmouseover="javascript:bannerOver(1, <?=$i?>);" style="padding-right: 10px;" alt="" /><?php
								$i++;
								}
								?>
							</div>
						</div>
						<!--<div style="position: absolute; top: 0; left: 0;">
							<img src="/images/compy.png" alt="" />
						</div>-->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div style="width: 1053px; margin: 0 auto; padding: 20px 0 30px 0; border-bottom: 1px solid #c0c0c0;">
	<div style="font-family: tahoma; font-size: 20pt; color: #7f7f7f; padding: 0 0 20px 20px;">Top Tools on <?=$siteName?>.</div>
	<div class="toptool" style="cursor:pointer;" onclick="window.document.location='about-advertising.php';">
		<h3>Promote your Company</h3>
		<img src="/images/mega.png" alt="" />
		<div>Go ahead and create your ad in minutes.&nbsp; Start targeting your services and products on <?=$siteName?>.</div>
	</div>
	<div class="toptool" style="cursor:pointer;" onclick="window.document.location='/employment/jobs.php?q=';">
		<h3>Find Employment</h3>
		<img src="/images/career.png" alt="" />
		<div>Find shore-based and seagoing positions associated with maritime and water-related sectors.</div>
	</div>
	<div class="toptool" style="cursor:pointer;" onclick="window.document.location='/employment/emp_create.php';">
		<h3>Post a Job Opening</h3>
		<img src="/images/help_wanted.png" alt="" />
		<div>Get your job posting in front of professionals, and connect with qualified candidiates immediately.</div>
	</div>
	<div class="toptool" style="cursor:pointer;" onclick="window.document.location='about-directory.php';">
		<h3>Add your Business</h3>
		<img src="/images/business.png" alt="" />
		<div>Add your business or service to the <?=$siteName?> Directory and get discovered.</div>
	</div>
	<div class="toptool" style="cursor:pointer;" onclick="javascript: window.location='/pages/claim_company.php';">
		<h3>Claim your Company</h3>
		<img src="/images/claim.png" alt="" />
		<div>Claim your company to connect with consumers and for special opportunities from <?=$siteName?>.</div>
	</div>
	<div style="clear: both;"></div>
</div>

<div style="width: 1053px; margin: 0 auto; padding: 20px 0 30px 0; border-bottom: 1px solid #c0c0c0; overflow: hidden;">
	<div style="font-family: tahoma; font-size: 20pt; color: #7f7f7f; padding: 0 0 30px 20px;">How <?=$siteName?> is used by Businesses, Professionals, and Enthusiasts around the globe.</div>
	
	<div style="width: 1200px;">
		<div class="howused">
			<h2>Businesses</h2>
			<ul>
				<li><span class="badge"></span>Claim or create your Company Page to be heard, discovered, and so much more.</li>
				<li><span class="activity"></span>Share announcements, product releases, promotions, or news right from your Company Page.</li>
				<li><span class="orgchart"></span>Connect with new and existing customers via coworkers and social breadcrumbs.</li>
				<li><span class="twitface"></span>Status updates are posted directly to the Business Directory News Feed and your Facebook and Twitter pages.</li>
			</ul>
		</div>
		<div class="howused">
			<h2>Professionals</h2>
			<ul>
				<li><span class="tool"></span>Browse thousands of job postings, and see which of your connections can help you land your next gig.</li>
				<li><span class="profile"></span>Build your r&#0233;sum&#0233; and activate the Job Seeking setting to be discovered.</li>
				<li><span class="orgchart"></span>Connect and get reconnected with new and old colleagues.</li>
				<li><span class="securemail"></span>Choose what to be contacted for, then allow users to contact you directly or through SaltMail.</li>
			</ul>
		</div>
		<div class="howused">
			<h2>Enthusiasts</h2>
			<ul>
				<li><span class="quote"></span>Exchange ideas and knowledge with professionals and businesses around the globe.</li>
				<li><span class="activity"></span>Browse photos and specs for tens of thousands of your favorite yachts, ships, and companies.</li>
				<li><span class="tv"></span>Share photos and videos with your connections and tag who's in them.</li>
				<li><span class="orgchart"></span>Connect with others who share the same interests and passions.</li>
			</ul>
		</div>
	</div>

	<div style="clear: both;"></div>
</div>

<script type="text/javascript">
<!--


var banners = new Array();
<?php
$i = 0;
foreach ($banners as $page => $ids)
{
	$j = 0;
	echo "banners[$i] = new Array();";
	foreach ($ids as $id => $link)
		echo "banners[$i][" . $j++ . "] = ['$page$id', '$link'];\n";
		
	$i++;
}
?>

function nextBannerTimer(n)
{
	setTimeout("nextBanner(" + n + "); nextBannerTimer(" + n + ");", 6000);
}

nextBannerTimer(1);
nextBannerTimer(0);

var isLoggedIn = <? echo ($API->isLoggedIn() ? "1" : "0"); ?>;

<? if( isset( $_GET['ia'] ) ) { ?>
  showPopupUrl( "/signup/login_redirect_popup.php?" );
/*
html = '<div style=" width: 250px; margin-left:auto; margin-right:auto; padding:20px; font-size:8pt;"><div style="font-size:14pt;">Want to access this page?</div><div style="font-size:9pt;">Sign in or create an account.</div><div style="margin-top:20px;"><div style="margin-top: 10px; text-align: center;">';
html += '<input type="button" style="height: 23px; width: 62px; background: #FFF8CC; border: 1px solid #FF9A66; color: #3B5998; font-weight: bold; font-size: 9pt; font-family: arial;" value="Sign Up" onclick="closePopUp(); getStarted2();"></div>';
html += '<div style="text-align:center; color: #808080; padding-top:3px;">or login with</div><div style="text-align:center; color: #808080; padding-top:5px;">';
html += '<a onclick="javascript:closePopUp();" href="javascript:void(0);"><img src="/images/s_badge 20x20.png" /></a> <a href="/fbconnect/login.php"><img src="/images/facebook16.png" /></a> <a href="/twitter/login.php"><img src="/images/twitter16.png" /></a></div></div></div>';
showPopUp( 'Whoops!', html );
*/
<? } else if (isset($_GET['empty'])) {?>
showPopupUrl("/signup/linkedin_error.php?");
<? } ?>
-->
</script>

<div style="padding-top: 20px;">
	<?php include "newfooter.php"; ?>
<?
    if (isset($_GET['debug']))
    echo session_id();
?>
</body></html>
