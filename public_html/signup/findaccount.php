<?php
/*
This page allows the user to reset their password with either a username or email address.
*/

include_once "../inc/inc.php";
include_once "../inc/recaptchalib.php";
$publickey = "6LciL78SAAAAANVcbxyOQtkOkEyJN1YaGs_HUe4M";
$privatekey = "6LciL78SAAAAAKjsEQNYz2kD7Dm__mk4jshxleAk";

$title = "Password | $siteName";

$page_header = "Log in Assistance";
  $sub_header = $siteName . ' is the leading social network for professionals and businesses who make a living on and around the water. What did you forget?';

$scripts[] = "/index.js";
$noheader = true;
include "../signup/newheader.php";
include "../header_gradient.php";

?>

<div class="graygradient">
	<div class="headcontainer">
		<div class="entryslide">
			<?=$htmlSignIn?>
			<div class="slidecontent">
				<span class="title">Log in Assistance</span>
			
				<div class="text">
					What did you forget?&nbsp; If you are having trouble logging into your account, try using the tools below.&nbsp;
					If this does not work, send the <?=$siteName?> team an e-mail at <?=mungeemail("cr@salthub.com")?>.
				</div>
				
			</div>
			
			<img src="/images/resetpw.png" class="bigimage" alt="" />
		</div>
	</div>
</div>

<div style="margin: 10px auto; font-size: 9pt; width: 1050px;">
	<h3 class="bigtext" style="color: #555; padding: 20px 0 10px; margin: 0;">Find Your Account</h3>
	<div style="min-height: 175px;">
		<div id="forgot_content" style="display: block;">
			<ul style="margin: 0; padding: 0;" id="frmforgot">
				<li style="padding-left: 40px; line-height: 35px;">E-mail address, username, or full name:</li>
				<li style="padding-left: 40px; background: url(/images/email32.png) no-repeat 0 -5px;"><input id="forgot_search" type="text" style="width: 300px; height: 12px; padding: 3px;"></li>
				<li style="padding-left: 38px; line-height: 35px;"><div style="color: red; display: none;" class="message"></div><input class="button" type="button" value="Search" id="btnforgot"></li>
			</ul>
		</div>
	</div>
</div>

<script type="text/javascript">
function wait(w)
{
	if (w)
		$('#forgot_content').css('opacity', 0.5).find('input').attr('disabled', true);
	else
		$('#forgot_content').css('opacity', 1).find('input').attr('disabled', false);
}

$('#forgot_search').keypress(
	function (e)
	{
		$('#forgot_content .message').stop(true, true).slideUp();
		
		if (e.which == 13)
		{
			e.preventDefault();
			$('#btnforgot').click();
		}
	}
);

function reset()
{
	$('#forgot_content div.results').slideUp(1000, function () { $('#frmforgot').fadeIn(); });
}

function reset_pw(uid)
{
	wait(true);
	
	$.ajax({
		url: '/ajax/send_pw_reset.php?uid=' + uid,
		success: function (json)
		{
			wait(false);
			
			if (typeof json.error == 'string')
			{
				reset();
				$('#forgot_content .message').html(json.error).stop(true, true).slideDown();
			}
			else
			{
				$('#forgot_content').html('A password reset request has been sent to your e-mail address, ' + json.email);
			}
		}
	});
}

$('#btnforgot').click(
	function ()
	{
		isDevServer = true;
		
		wait(true);
		
		url = '/ajax/forgot_search.php?q=' + escape($('#forgot_search').val());
		
		$.ajax({
			url: url,
			success: function (json)
			{
				if (json.length == 0)
				{
					wait(false);
					
					$('#forgot_content .message').html('No users were found that matched your search. Please try again.').stop(true, true).slideDown();
				}
				else
				{
					ul = $('<ul class="results"></ul>');
					for (i in json)
					{
						li = $('<li data-uid="' + json[i].uid + '" style="line-height: 16px; width: 375px; float: left; padding: 10px; cursor: pointer; margin: 0 10px 10px 0; height: 48px; border: 1px solid #d8dfea;"></li>');
						$(li).append('<img src="' + json[i].img + '" style="width: 48px; height: 48px; float: left; margin: 0 10px 10px 0;" alt>');
						$(li).append('<b>' + json[i].name + '</b><br>');
						
						if (json[i].occupation)
						{
							$(li).append(json[i].occupation);
							if (json[i].company)
								$(li).append(' at ');
						}
						
						if (json[i].company)
							$(li).append(json[i].company);
						
						$(ul).append(li);
					}
					
					div = $('<div class="results"></div>');
					$(div).append(ul);
					$(div).append('<div class="caption" style="clear: both; float: left; padding-bottom: 10px;"><img src="/images/arrow_rotate_clockwise.png" alt=""/><a href="javascript:void(0);" onclick="javascript:reset();">Go back</a></div>');
					
					$('#frmforgot').fadeOut(250,
						function ()
						{
							wait(false);
							
							$('#forgot_content').slideUp().append(div).slideDown()
								.find('ul.results li')
									.mouseenter(
										function ()
										{
											$(this).css('background', '#d8dfea');
										}
									).mouseleave(
										function ()
										{
											$(this).css('background', '');
										}
									).click(
										function ()
										{
											reset_pw($(this).attr('data-uid'));
										}
									);
;
						}
					);
				}
			}
		});
	}
);
</script>

<?php
include 'newfooter.php';
?>