<table border=0 cellpadding=0 cellspacing=0 width=670>
	<tr>
		<td style="padding-right: 25px;">
			<img src="/images/splashlogo.png">
		</td>
		<td width=450>
			<div style="font-family: arial; font-size: 14pt; color: #555555;">
				Connecting people who live, work, and play in and around the water!
			</div>
			<div style="font-family: arial; color: #326798; font-size: 10pt; margin-top: 5px;">
				... used by enthusiasts, businesses, and professionals around the globe ...
			</div>
		</td>
	</tr>
</table>
