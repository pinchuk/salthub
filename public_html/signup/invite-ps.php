<?php
/*
Step 1 of the account creation process; this page enables the user to pull contacts from other websites
and invite them to SaltHub.
*/

$step=1;
include "step-header.php";
include_once "../inc/contacts.php";
?>

<script language="javascript" type="text/javascript">
<!--
var invites = 0;

-->
</script>

<div class="newusercontent">

<div style="float:left;"><h1>Welcome to <?=$siteName?>, <?=$API->name?>!</h1></div>

<div style="float: left; clear:left;">
<div style="font-size: 10pt;">
  Your personal and professional relationships are the single most effective way to find new opportunities.<br /> Stay in the loop and open new doors through your contacts.
</div>

<div style="width: 625px;">
	<div style="font-weight: bold; color: #326798; font-size: 12pt; margin-top: 15px;">Find contacts already on <?=$siteName?></div>
	<div style="padding-top: 10px;">
		<div style="border: 1px solid #d8dfea; background: #F3F8FB; padding: 5px;">
			<div class="smtitle"><img src="/images/page_add.png" />Connecting with people you already know on <? echo $siteName ?> is a good idea. <span class="small">(recommended)</span>  <br />
      </div>
			<?php showContactsTop(); ?>
		</div>
	</div>

	<div style="padding-top: 5px; font-size: 9pt;" id="divskip">
		<a style="text-decoration: none;" href="update_profile.php" onclick="javascript: if( invites == 0 ) { return confirm( ' <? echo $siteName ?> is about connecting through our real world connections. It is highly recommended to add your connections to get the most out of <? echo $siteName ?>. Are you sure you want to continue?' ); }" >skip &gt;</a>
	</div>
</div>
</div>

<div style="float:left; margin-left:40px;">
  <img src="/images/graph.jpg" alt="" />
</div>

<div style="clear: both;"></div>

<?php if ($script == "signup/invite-ps") echo '<script language="javascript" type="text/javascript" src="/invite.js?' . $hash . '"></script>'; ?>

</div>

</body></html>