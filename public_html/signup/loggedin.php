<?php
include "../header.php";
?>

<div style="font-size: 16pt; font-weight: bold; padding: 10px;">
	Welcome to <?=$siteName?>, <?=$API->name?>!
</div>

<div style="width: 944px; padding: 5px; border: 1px solid #D8DFEA; margin: 0 auto 15px;">
	<div style="width: 178px; float: left; text-align: center;">
		<a href="<?=$API->getProfileURL()?>"><img src="<?=$API->getThumbURL(0, 178, 266, $API->getUserPic())?>" alt="" /></a>
		
		<div style="text-align: left; padding-top: 10px;">
		<?php include "../messaging/notifications.php"; ?>
		</div>
	</div>
	<div style="padding-left: 5px; float: left; width: 456px;">
		<div style="width: 454px; border: 1px solid #c0c0c0; background: #EDEFF4; padding: 5px 0;">
			<div style="font-size: 16pt; font-weight: bold; padding: 0 5px;">
				Get started!
			</div>
			<div style="padding-left: 7px;">
				<script language="javascript" type="text/javascript">
				<!--
				currentLog = <?=$API->uid?>;
				tbPlace();
				//-->
				</script>
			</div>
			<div style="margin-top: 10px; padding: 10px 0 0 5px; font-size: 9pt; border-top: 1px solid #c0c0c0;">
				<div style="float: left;">
					<a href="/findpeople.php"><img src="/images/find.png" alt="" style="vertical-align: bottom;" />&nbsp; Find people you e-mail</a>
				</div>
				<div style="float: left; padding: 2px 0 0 10px; font-size: 8pt; color: #555;">(recommended)</div>
				<div style="clear: both; font-size: 9pt; padding-top: 2px;">Searching your e-mail address book and social networks is the fastest and most effective way to find your friends on <?=$siteName?>.</div>
				
				<div style="width: 200px;">
					<div style="padding-top: 10px;"><a href="javascript:void(0);" onclick="javascript:showCreatePage();"><img src="/images/page_add.png" alt="" style="vertical-align: bottom;" />&nbsp; Create a page</a></div>
					<div style="padding-top: 10px;"><a href="<?=$API->getProfileURL()?>"><img src="/images/comment_edit.png" alt="" style="vertical-align: bottom;" />&nbsp; View my log book</a></div>
					<div style="padding-top: 10px;"><a href="<?=$API->getProfileURL()?>/about"><img src="/images/book_open.png" alt="" style="vertical-align: bottom;" />&nbsp; Enhance my profile</a></div>
					<?php if (quickQuery("select verify from users where uid=". $API->uid) == "0") { ?><div style="padding-top: 10px;"><a href="/signup/verify.php"><img src="/images/email_go.png" alt="" style="vertical-align: bottom;" />&nbsp; Verify your e-mail address</a></div><?php } ?>
				</div>
			</div>
		</div>
	</div>
	<div style="padding-left: 5px; width: 300px; float: left;">
  <? if( $API->adv ) {
    showAd("companion");
  } ?>
	</div>
	<div style="clear: both;"></div>
</div>

<?php
include "../footer.php";
?>