<?php
/*
This is called by entry.php when a user signs in using the normal user/pass process (i.e., not Facebook or Twitter)

For Facebook and Twitter login pages, see:

/fbconnect/loggedin.php
/twitter/index.php

*/

include "../inc/inc.php";

if( $isDevServer )
  $url = 'http://dev.' . $siteName . '.com';
else
  $url = 'http://www.' . $siteName . '.com';

if ($_SESSION['bypassLogin'] == 1) //user has just verified email
{
	unset($_SESSION['bypassLogin']);
	$where = "uid={$_SESSION['uid']}";
}
else
{
	if (!empty($_POST['uname']))
	{
		$uname = addslashes($_POST['uname']);
		$where = "username='$uname' and password='" . md5($_POST['password']) . "'";
	}
	elseif (!empty($_POST['email']))
	{
		if (strpos($_POST['email'], '@') === false)
		{
			$uname = addslashes($_POST['email']);
			$where = "username='$uname' and password='" . md5($_POST['password']) . "'";
		}
		else
		{
			$email = addslashes($_POST['email']);
			$where = "email='$email' and password='" . md5($_POST['password']) . "'";
		}
	}
}

$x = sql_query("select uid,admin,fbid,twid,fbsess,name,pic,username,verify,email from users where $where");

if (mysql_num_rows($x) == 0)
{
	header("Location: $url/signup/entry.php?login=0");
	die();
}

$user = mysql_fetch_array($x, MYSQL_ASSOC);

sql_query("update users set lastlogin=now() where uid='" . $user['uid'] . "' limit 1" );

if( strlen( $user['fbid'] ) > 0 )
{
  $API->uid = $user['uid'];
  $API->restoreSessionVariables($user['uid']);
  if( $API->startFBSession() == null ) echo "null";
  $API->fbGetPages();
}

foreach ($user as $k => $v)
	$_SESSION[$k] = $v;

if (empty($user['fbid']) && empty($user['twid']) && $user['verify'] == 0)
	$_SESSION['needsVerified'] = 1;
else
	$_SESSION['needsVerified'] = 0;

if ($user['verify'] == 0)
{
	header("Location: $url/signup/verify.php");
}
else
{
  if( isset( $_SESSION['login_redirect'] ) && $_SESSION['login_redirect'] != '' ){
      header( "Location: ".$_SESSION['login_redirect']);// . $_SESSION['login_redirect'] );
  }
  else{
      header("Location: $url/welcome");
  }
  $_SESSION['login_redirect'] = '';
}


?>
