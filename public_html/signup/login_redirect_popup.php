<? $siteName = "SaltHub"; ?>
<div style="width: 600px; margin:0 auto;">

  <div style="float:left; width:280px; margin-left:15px;">
  	<div style="clear:both; font-family: arial; color: rgb(50, 103, 152); font-size: 10pt; padding-top: 30px; font-weight:bold; margin-top:-20px;">
      Welcome!
  	</div>

    <div style="margin-top:10px;">
      <div style="float:left;"><img src="/images/lock.png" width="16" height="16" alt="" /></div>
      <div style="float:left; font-weight:bold; font-size:9pt; padding-top:2px;">Login or Signup to access this page</div>
      <div style="clear:both;"></div>
    </div>

    <div style="width:100%; margin-top:15px; margin-bottom:15px; border-bottom: 1px solid rgb(220,220,220);"></div>

    <div style="font-size:9pt;">
      Login with facebook or twitter &nbsp;<a href="/fbconnect/login.php"><img src="/images/facebook16.png" /></a> <a href="/twitter/login.php"><img src="/images/twitter16.png" /></a> <!--<a href="/linkedin/login.php?clear"><img src="/images/linkedin20.jpg" style="width: 20px; height: 20px" /></a>-->
    </div>

    <div style=" width:100%; margin-top:15px; margin-bottom:15px; border-bottom: 1px solid rgb(220,220,220);"></div>

    <div style="font-size:9pt; line-height:170%;">

      Don't have a facebook or twitter account?<br />

      <a href="javascript:void(0);" onclick="javascript: closePopUp(); getStarted2(); "><b>Sign up for <?=$siteName?> &hellip;</b></a>
    </div>


    <div style="width:100%; margin-top:15px; margin-bottom:15px; border-bottom: 1px solid rgb(220,220,220);"></div>

    <div>
      <div style="float:left;"><img src="/images/s_badge 20x20.png" /></div>
      <div style="float:left;font-size:9pt; padding-top:8px; margin-left:5px;"> Have a <?=$siteName?> account? Login below.</div>
    </div>

		<form method="post" action="/signup/login.php">
    <div  class="divlogin" style="clear:both; float:none; padding-top:10px;">
  		<input type="text" name="email" value="your email" onfocus="javascript:if (this.value == 'your email') this.value = '';" onblur="javascript:if (this.value == '') this.value = 'your email';" />
    </div>

    <div class="divlogin" style="float:none; margin-top:5px;">
			<span id="txtpassword_container"><input type="text" name="password" onkeypress="javascript:return submitenter(this, event);" value="your password" onfocus="javascript:this.type = 'password'; this.value = '';" onblur="javascript:if (this.value == '') { this.type = 'text'; this.value = 'your password'; }" /></span>&nbsp;
      <a href="javascript:void(0);" onclick="javascript:this.parentElement.parentElement.submit();">Login</a>
    </div>

    <div style="font-size:8pt; clear:both; margin-top:5px;">
      <a href="/signup/resetpw.php">Forgot e-mail or password?</a>
    </div>
    </form>


  </div>

  <div style="float:left; width: 8px; margin-left:15px; margin-right:15px; padding-top:20px;">
    <img src="/images/skinny_long_arrow.png" width="8" height="291" alt="" />
  </div>

  <div style="float:left; width: 250px;">
    <div style="clear:both; font-family: arial; color: rgb(50, 103, 152); font-size: 9pt; padding-top: 30px; font-weight:bold;">Add your Business</div>
    <div>
      <div style="float:left; margin-right:10px; padding-top:5px;"><img src="/images/business.png" width="100" height="75" /></div>
      <div style="float:left; font-family: arial; color: #555555; font-size: 8pt;  width:140px;">After you have joined with your real name, you can add your company page, which can also be submitted to the <?=$siteName?> directory for even more exposure.</div>
    </div>

    <div style="clear:both; font-family: arial; color: rgb(50, 103, 152); font-size: 9pt; padding-top: 30px; font-weight:bold;">Get Discovered</div>
    <div>
      <div style="float:left; margin-right:10px; padding-top:5px;"><img src="/images/connect_map.png" width="100" height="75" /></div>
      <div style="float:left; font-family: arial; color: #555555; font-size: 8pt;  width:140px;">Once your profile and company page are set up, you will be able to connect with industry experts, coworkers (past and present), employers and decision makers.</div>
    </div>
  </div>


  <div style="clear:both;"></div>
</div>

