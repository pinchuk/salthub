<?php
include_once $_SERVER['DOCUMENT_ROOT'] . "/inc/inc.php";

if( !isSSL() )
  transitionToSSL();

//Lookup to see if we have have new meta data set in from admin
$temp = end(explode("/", $script));
$meta_tags = queryArray( "select title, keywords, descr from meta_tags where script='$temp'" );
unset( $temp );
if( $meta_tags != NULL )
{
  $title = $meta_tags['title'];
  $descr = $meta_tags['descr'];
  $keywords = $meta_tags['keywords'];
}

?><!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
	<link rel="shortcut icon" href="/favicon_<?=$site?>.ico">
	<link rel="stylesheet" href="/style_<?=$site?>.css?ver=<?=filemtime($_SERVER['DOCUMENT_ROOT'] . "/style_$site.css")?>" type="text/css">
	<link rel="stylesheet" href="/style.css?ver=<?=filemtime($_SERVER['DOCUMENT_ROOT'] . "/style.css")?>" type="text/css">

<?
  if( !isset($title) ) $title = $defaultPageTitle;
  if (!isset($descr ) && $site == "s" ) $descr = $defaultMetaDescription;

  if( $isDevServer ) $title = "SH DEV - " . $title;
?>

	<title><?=$title?></title>

<? if( isset( $descr ) ) { ?>
  <meta name="description" content="<?=$descr?>">
<? } else { ?>
  <meta name="description" content="<?=$sitename?> is the leading social network for professionals and businesses who make a living on and around the water. Connect, access knowledge, insights and be discovered.">
<? } ?>

<? if( isset( $keywords ) ) { ?>
  <meta name="keywords" content="<?=$keywords?>">
<? } ?>
	<script type="text/javascript" src="/jquery/jquery-1.4.4.min.js"></script>
	<script type="text/javascript">var script = "<?=end(explode("/", $script))?>";</script>
	<script type="text/javascript" src="/odometer.js"></script>
	<script type="text/javascript" src="/simpleajax.js"></script>
	<script type="text/javascript" src="http://static.ak.connect.facebook.com/js/api_lib/v0.4/FeatureLoader.js.php/en_US"></script>
	<script type="text/javascript" src="/common.js"></script>
	<script type="text/javascript" src="/index.js"></script>
	<script type="text/javascript" src="/tinybox/tinybox.js"></script>
	<script type="text/javascript" src="/flowplayer/flowplayer-min.js"></script>
	<?php
	foreach ($scripts as $script)
		echo '<script type="text/javascript" src="' . $script . '"></script>';
	?>
</head>


<body>
<?
include( "signup_popup.php" );

if (($script != "signup/entry" && $script != "signup/entry2") && !isset($noheader)) {
?>

<div class="graygradient">
	<div style="width: 1053px; margin: 0 auto; padding-top: 22px;">

		<div style="width: 507px; float: left;">
			<div style="padding-top: 30px;">
				<img src="/images/salt_badge100.png" style="margin: 0 15px -3px 0;" alt="">
				<span style="color: #717171; font-family: tahoma; font-size: 28pt;"><? if( isset( $page_header ) ) echo $page_header; else echo "What is " . $siteName . "?"; ?></span>
			</div>
			<div style="padding-top: 14px; font-size: 11pt;">
				<? if( isset( $sub_header ) ) echo $sub_header; else echo $siteName . ' is a social network that connects professionals and businesses<br>who make a living in and around the water.'; ?>
			</div>

			<div style="padding: 40px 0 0;" class="frontuserpics">
				<?php
        if( $script == "signup/entry" ) {
  				$x = sql_query("select uid,pic from users where active=1 and pic > 2 order by rand() limit 9");
  				while ($user = mysql_fetch_array($x, MYSQL_ASSOC))
  				{
  					?>
  					<div><img src="<?=$API->getThumbURL(1, 48, 48, $API->getUserPic($user['uid'], $user['pic']))?>"></div>
  					<?php
  				}
        }
				?>
			</div>

			<div style="font-size: 12pt; clear: both; padding-top: 30px;">
				Sign in with&nbsp;
				<a href="/fbconnect/login.php"><img src="/images/facebook16.png" style="vertical-align: bottom;" alt=""></a>&nbsp;
				<a href="/twitter/login.php"><img src="/images/twitter16.png" style="vertical-align: bottom;" alt=""></a>&nbsp;
				or <a href="javascript:void(0);" onclick="javascript:getStarted2();">Create a New Account</a>.
			</div>

   		<form method="post" id="frmlogin" action="/signup/login.php?2">
			<div class="divlogin" style="float: left; padding-top: 20px;">
				Returning user&nbsp;
				<input type="text" name="email" data-default="your e-mail address">
				<span id="txtpassword_container"><input type="text" value="your password" onfocus="javascript:showPasswordBox();"></span>&nbsp;
				<a href="javascript:void(0);" onclick="javascript:document.getElementById('frmlogin').submit();">Login</a>&nbsp;
				<a href="/signup/resetpw.php"><img src="/images/help.png" alt=""></a>
			</div>
       </form>
		</div>


		<div style="float: right; padding-right: 37px; font-size: 10pt; color: #326798;">
			Connect with us on&nbsp;
			<a href="http://www.facebook.com/pages/SaltHub/196023203785837"><img src="/images/facebook16.png" style="vertical-align: bottom;" alt=""></a>
			<a href="http://twitter.com/#!/<?=SITE_TWITTER_SN?>"><img src="/images/twitter16.png" style="vertical-align: bottom;" alt=""></a>
		</div>



		<div style="float: left; padding-top: 30px;">
			<div style="position: relative;">
        <? if( $script == "signup/entry" ) { ?>
				<div style="padding: 3px 0 0 40px;">
					<?php
					$i = 0;
					foreach ($banners as $id => $link)
					{
						?>
						<div id="banner-<?=$i?>" style="text-align: center; height:232; padding: 5px 0 10px 0; <?=$i == 0 ? "" : " display: none;"?>">
							<a href="<?=$link?>"><img src="/images/banners/<?=$id?>.png" alt="" style="width: 470px; height: 285px;"></a>
						</div>
						<?php
						$i++;
					}
					?>
					<div style="text-align: center; padding: 35px 0 5px 10px;">
						<?php
						$i = 0;
						foreach ($banners as $id => $link)
						{
						?><img id="banner-sq-<?=$i?>" src="/images/square<?=$i == 0 ? "2" : ""?>.png" onmouseout="javascript:bannerOut();" onmouseover="javascript:bannerOver(<?=$i?>);" style="padding-right: 10px;" alt=""><?php
						$i++;
						}
						?>
					</div>
				</div>
				<div style="position: absolute; top: 0; left: 0;">
					<img src="/images/compy.png" alt="">
				</div>
      <? } ?>
			</div>
		</div>
	</div>
</div>
<? } ?>


