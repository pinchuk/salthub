<?
/*
This popup is shown to new users when they load the welcome page for the first time.  It checks to see if the user has filled out the sector information
and if not, it forces them to select the sever. The script also checks to see if the user has verified their account, and if not, it will show the popup until they've
verified.
*/

require( "../inc/inc.php" );

$showUpdateProfile = false;
$showActivate = false;

$user_data = queryArray( "select sector, occupation, verify from users where uid='" . $API->uid . "'" );

if( $user_data['sector'] == 0 || $user_data['occupation'] == "" )
  $showUpdateProfile = true;

if( $user_data['verify'] == 0 )
  $showActivate = true;


if( $showActivate )
{
	$info = $API->getUserInfo(null, "email");
  if( isset( $info['email'] ) )
  {
    $email = $info['email'];
  }
  $hasEmail = isset($email);
?>
	<div style="width: 430px; border: 1px solid #c0c0c0; background: #EDEFF4; padding: 10px 5px;" id="activatediv">
    <div class="strong" style="margin: 5px 0 0 5px;">
      Activate full access to <?=$siteName?>
    </div>

		<div style="padding-top: 5px;">
			<table border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td width=30>
						<img src="/images/email_go.png">
					</td>
					<td style="font-weight: bold; color: #326798; font-size: 9pt;">
						<?
            if( $hasEmail )
            {
              echo 'We just sent you a message at <span style="color: #800080;" id="em_addr">' . $email . '</span>.';
            }
            else
            {
              echo 'Please provide us with your e-mail address.';
            }
            ?>
					</td>
				</tr>
			</table>
		</div>
		<script language="javascript">
		email2 = "s@";
		email3 = "<?=$siteName?>.com";
		email1 = "notification";

		email = email1 + email2 + email3;
      email2 = "signup@salthub.com";

		</script>
		<form name="frm" <?=$hasEmail ? "" : 'onsubmit="javascript:return checkForm();"'?> method="post" action="/signup/verify.php">
		<input type="hidden" name="resend" value="2" />

		 <!--	<p />
			If you did not get our e-mail, add
			<script language="javascript">
			document.write("&quot;" + email + "&quot;");
			</script>
			to your safe senders list and click below to send our message again, or use a different email address and try again. Please also check your junk/spam folders.-->
		<div style="padding: 10px 50px 0 0; font-size: 9pt; width:100%;">
			<?
      if( $hasEmail ) {
      ?>
  			For full access and to connect with other members, simply follow the link provided in our e-mail, and you're good to go!

        <div style="text-align:center; margin-top:5px; width:100%;">
				  <input type="submit" value="Resend Message" class="button">
        </div>

        <div style="width:100%; text-align:center; margin-top:5px;">
          <a href="javascript:void(0);" onclick="javascript:document.getElementById('changeemail').style.display='';">use a different email</a>
          or login with
          <a href="/fbconnect/login.php"><img src="/images/facebook16.png" style="vertical-align: bottom;" alt="" /></a>&nbsp;
	  			<a href="/twitter/login.php"><img src="/images/twitter16.png" style="vertical-align: bottom;" alt="" /></a>&nbsp;
        </div>
			<?php } else { ?>
        Please enter your email address below to receive the account activation link.
      <? } ?>
		</div>

    <div id="changeemail" style="<? if ($hasEmail) { ?> display:none; font-size:9pt;<? } ?> margin-top:10px; margin-bottom:10px; border: 1px solid #c0c0c0; padding: 5px;">
			E-mail address: &nbsp; &nbsp; <input type="text" name="email" id="email" style="width: 260px;" />
      <div style="text-align:center; margin-top:5px;">
			  <input type="submit" value="Update E-mail Address" class="button" />
      </div>
    </div>

		</form>

		<div style="margin-top:5px;">
			<table border=0 cellpadding=0 cellspacing=0 width="100%">
				<tr>
					<td width=30>
						<img src="/images/information.png">
					</td>
					<td style="font-weight: bold; font-size: 9pt;">
						<a href="javascript:void(0);" onclick="javascript: document.getElementById('havingtrouble').style.display='';">Having trouble?</a>
					</td>
          <td align="right" style="font-weight: bold; font-size: 9pt;">
            <a href="/logout.php">log out</a> &nbsp;
          </td>
				</tr>
				<tr>
					<td style="font-size: 9pt; padding-top: 2px;" colspan="2">
            <div id="havingtrouble" style="display:none; border: 1px solid #c0c0c0; padding: 5px;">
  						If you are not able to verify your account, you can log in at any time to return to this page.&nbsp; You can also e-mail us with your username and the e-mail you registered with at
						  <a style="text-decoration: none; color: #326798;" href="javascript:void(0);" onclick="javascript: sendEmail( email2, '' );">signup@<? echo $siteName ?>.com</a>
            </div>
					</td>
				</tr>
			</table>
		</div>

	</div>

<?
}

if( $showUpdateProfile )
{
  $user = queryArray( "select occupation, sector, company from users where uid='" . $API->uid . "'" );
?>
<div style="width: 430px; border: 1px solid #c0c0c0; background: #EDEFF4; <? if( $showActivate ) { ?>margin-top:5px;<?}?> padding: 10px 5px; font-size:9pt;" id="profilediv">
  <div class="strong" style="margin: 5px 0 0 5px;">
    <img src="/images/add.png" width="16" height="16" alt="" /> Add your profile information
  </div>

  <div style="margin-top:5px;">
      Work Place:
  </div>
  <div>
    <input id="company" type="text" style="width:300px;" value="<?=$user['company'];?>" onkeyup="javascript: CheckProfileForm();"/>
  </div>

  <div style="margin-top:5px;">
    Sector:
  </div>
  <div>
    <select id="sector" style="width:300px;" id="sector" onchange="javascript: CheckProfileForm();">
    <option value="0">(None Selected)</option>
    <?
    $q2 = sql_query( "select * from categories where cattype='G' and industry='" . PAGE_TYPE_BUSINESS . "' order by catname" );
    while( $r2 = mysql_fetch_array( $q2 ) )
    {
    ?>
    <option value="<? echo $r2['cat']; ?>"<? if( $r2['cat'] == $user['sector'] ) echo " SELECTED"; ?>><? echo $r2['catname']; ?></option>
    <?
    }
    ?>
    </select>
  </div>

  <div style="margin-top:5px;">
    Occupation or Title:
  </div>

  <div>
    <input id="occupation" type="text" style="width:300px;" value="<?=$user['occupation'];?>" onkeyup="javascript: CheckProfileForm();"/>
  </div>


  <div style="width:100%; height: 20px;">
    <div style="float:right; padding-top:5px; font-weight:bold;">
      <a href="/logout.php">log out</a> &nbsp;
    </div>

    <div style="float:left; padding-top:5px;">
      <input class="button" type="button" name="Done" value="Done" onclick="javascript: UpdateProfileInformation();" style="display:none;" id="profiledonebutton"/>
    </div>
  </div>
</div>


<?
}
?>