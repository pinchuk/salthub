<?php

include "../inc/inc.php";

$uid = intval($_GET['uid']);

if ($API->generateDeleteCommentHash($uid) != $_GET['hash'])
{
	header("Location: /");
	die();
}

if ($API->isLoggedIn())
	header("Location: /" . $_GET['type'] . "/" . $_GET['id']);
else
{
	$_SESSION['uidpending'] = $uid;
	header("Location: /");
}

?>