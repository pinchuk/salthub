<?php

if (empty($_REQUEST['uid']))
	header('Location: findaccount.php');

/*
This page allows the user to reset their password with either a username or email address.
*/

include_once "../inc/inc.php";
include_once "../inc/recaptchalib.php";
$publickey = "6LciL78SAAAAANVcbxyOQtkOkEyJN1YaGs_HUe4M";
$privatekey = "6LciL78SAAAAAKjsEQNYz2kD7Dm__mk4jshxleAk";

$title = "Password | $siteName";

$page_header = "Log in Assistance";
  $sub_header = $siteName . ' is the leading social network for professionals and businesses who make a living on and around the water. What did you forget?';

$scripts[] = "/index.js";
$noheader = true;
include "../signup/newheader.php";
include "../header_gradient.php";

?>

<div class="graygradient">
	<div class="headcontainer">
		<div class="entryslide">
			<?=$htmlSignIn?>
			<div class="slidecontent">
				<span class="title">Log in Assistance</span>
			
				<div class="text">
					What did you forget?&nbsp; If you are having trouble logging into your account, try using the tools below.&nbsp;
					If this does not work, send the <?=$siteName?> team an e-mail at <?=mungeemail("cr@salthub.com")?>.
				</div>
				
			</div>
			
			<img src="/images/resetpw.png" class="bigimage" alt="" />
		</div>
	</div>
</div>

<div style="margin: 28px auto 25px; font-size: 9pt; width: 1050px;">
	<div style="float: left; width: 546px;">
		<div class="pheaditem" style="width: auto;"><div class="caption"><img src="/images/arrow_rotate_clockwise.png" alt=""/><a href="/">Return to home page</a></div></div>
		<?php
		if (isset($_GET['r'])) //clicked email link to reset
		{
			echo '<div class="bigtext" style="color: #555; padding-bottom: 10px;">Reset Password</div>';
			$uid = intval($_GET['uid']);
			if ($_GET['r'] == $API->generateDeleteCommentHash("resetpw", $uid))
			{
				?>
				<form action="/signup/resetpw.php" method="post" onsubmit="javascript:if (document.getElementById('pass1').value != document.getElementById('pass2').value) { alert('New passwords do not match.'); return false; } else return true;">
				<input type="hidden" name="r" value="<?=$_GET['r']?>" />
				<input type="hidden" name="uid" value="<?=$uid?>" />
				<div style="width: 175px; float: left;">Enter your new password:</div>
				<div style="float: left;"><input type="password" id="pass1" name="password" alt="" style="width: 200px;" /></div>

				<div style="clear: both; height: 10px;"></div>

				<div style="width: 175px; float: left;">Re-enter your new password:</div>
				<div style="float: left;"><input type="password" id="pass2" alt="" style="width: 200px;" /> &nbsp; &nbsp; <input type="submit" name="reset" class="button" value="Reset Password" style="height: 23px;" /></div>
				</form>
				<?php
			}
			else
				echo 'Could not reset your password.&nbsp; Please check the e-mail link and try again.';
		}
		elseif (isset($_POST['r']))
		{
			echo '<div class="bigtext" style="color: #555; padding-bottom: 10px;">Forgotten Password</div>';
			$uid = intval($_POST['uid']);
			if ($_POST['r'] == $API->generateDeleteCommentHash("resetpw", $uid))
			{
				sql_query("update users set password='" . md5($_POST['password']) . "' where uid=$uid");
				echo 'Your password was reset successfully.&nbsp; You can now <a href="/">log in</a> using the new password.&nbsp; Thank you for using ' . $siteName . '.';
			}
			else
				echo 'Could not reset your password.&nbsp; Please check the e-mail link and try again.';
		}
		elseif (isset($_POST['reset']))
		{
			if (!empty($_POST['uid']))
				$x = sql_query("select email,uid,name,username from users where uid=" . intval($_POST['uid']));
			else
				$x = sql_query("select email,uid,name,username from users where username='" . $_POST['username'] . "'");
			
			echo '<div class="bigtext" style="color: #555; padding-bottom: 10px;">Forgotten Password</div>';
			if (mysql_num_rows($x) == 0)
				echo 'The user name you entered was not found.&nbsp; Please <a href="resetpw.php">try again</a> with a different user name, or <a href="resetpw.php">look up your user name</a> if you have forgotten it.';
			else
			{
				$user = mysql_fetch_array($x, MYSQL_ASSOC);
				$API->uid = $user['uid']; //Required to fix bug.  The API doesn't know what the uid is at this point, since the user is not logged in.
				emailAddress($user['email'], $siteName . " - Reset Password", $user['name'] . ', someone has initiated a password reset request.&nbsp; If this was you, you can reset your password by following <a href="http://' . $_SERVER['HTTP_HOST'] . '/signup/resetpw.php?uid=' . $user['uid'] . '&r=' . $API->generateDeleteCommentHash("resetpw", $user['uid']) . '">this link</a>.<p />If this was not you, please simply disregard this e-mail.', 'accounts');
				echo 'An e-mail has been sent to the e-mail address on record for this account with instructions for resetting the password.';
			}
		}
		elseif (isset($_POST['getuser']))
		{
			$x = sql_query("select email,uid,name,username from users where email='" . $_POST['email'] . "'");
			echo '<div class="bigtext" style="color: #555; padding-bottom: 10px;">Forgotten User Name</div>';
			if (mysql_num_rows($x) == 0)
				echo 'The e-mail address you entered was not found.&nbsp; Please <a href="resetpw.php">try again</a> with a different e-mail address.';
			else
			{
				$user = mysql_fetch_array($x, MYSQL_ASSOC);
	  $API->uid = $user['uid']; //Required to fix bug.  The API doesn't know what the uid is at this point, since the user is not logged in.
				emailAddress($user['email'], $siteName . " - Forgotten User Name", $user['name'] . ', your user name is &quot;' . $user['username'] . '&quot; (no quotations).&nbsp; You may now log in at http://' . $_SERVER['HTTP_HOST'], 'accounts');
				echo 'An e-mail has been sent to the e-mail address on record for this account with instructions for resetting the password.';
			}
		}
		else
		{
		?>
		<form action="/signup/resetpw.php" method="post">
		<div class="bigtext" style="color: #555; padding-bottom: 10px;"><? echo ( isset($_REQUEST['r'] ) ) ? "Reset" : "Forgotten"; ?> Password</div>
		<div style="width: 175px; float: left;">Enter your user name:</div>
		<div style="float: left;"><input type="text" name="username" alt="" style="width: 200px;" /> &nbsp; &nbsp; <input type="submit" name="reset" class="button" value="Reset Password" style="height: 23px;" /></div>
		</form>

		<div style="clear: both; height: 20px;"></div>

		<div style="border-top: 1px solid #000; padding-bottom: 15px;"></div>

		<form action="/signup/resetpw.php" method="post">
		<div class="bigtext" style="color: #555; padding-bottom: 10px;">Forgotten User Name</div>
		<div style="width: 175px; float: left;">Enter your e-mail address:</div>
		<div style="float: left;"><input type="text" name="email" alt="" style="width: 200px;" /> &nbsp; &nbsp; <input type="submit" name="getuser" class="button" value="Find User Name" style="height: 23px;" /></div>
		</form>

		<div style="clear: both; height: 20px;"></div>
		<?php
		}
		?>
	</div>

	<div style="clear: both;"></div>
</div>

<?
include( "newfooter.php" );
?>