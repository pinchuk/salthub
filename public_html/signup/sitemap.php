<?
/*
This is a sitemap used to enable a search engine spider to roam through the pages on site easily.
*/

include "../inc/inc.php";

$noheader = true;
include "newheader.php";
include "../header_gradient.php";
?>

<div class="graygradient">
	<div class="headcontainer">
		<div class="entryslide">
			<?=$htmlSignIn?>
			<div class="slidecontent">
				<span class="title" style="font-size:30pt;"><?=$siteName?> Site Map</span>

				<div class="text">

				</div>




			</div>

			<img src="/images/520x320 megaphone.jpg" class="bigimage" alt="" />
		</div>
	</div>
</div>


<style>
.sitemap_column
{
  float:left;
  width:200px;
}

.sml a
{
  font-size: 8px;
}

.sitemap_column .column_title
{
  font-weight:700;
}

</style>

<div style="padding: 10px 5px 5px 5px; font-family: arial; font-size: 11pt; width:1020px; margin-left: auto; margin-right:auto;">

  <div class="sitemap_column">
    <div class="column_title"><?=$siteName?> Navigation</div>
    <div class="sml"><a href="/about-directory.php">About Advertising</a></div>
    <div class="sml"><a href="/about-directory.php">About Business Listings</a></div>
    <div class="sml"><a href="/about-claim_page.php">About Claiming Pages</a></div>
    <div class="sml"><a href="/about-pages.php">About Pages</a></div>
    <div class="sml"><a href="/about.php">About <?=$siteName?></a></div>
    <div class="sml"><a href="/directory.php">Business Directory</a></div>
    <div class="sml"><a href="/signup/resetpw.php">Reset Password</a></div>
    <div class="sml"><a href="/page/15528-SaltHub/logbook">SaltHub News</a></div>
    <div class="sml"><a href="/vessels/vessel_live_tracking.php">Vessel Heat Map</a></div>
    <div class="sml"><a href="/signup">Welcome &amp; Login</a></div>
    <br />
    <div class="column_title">Clubs &amp; Associations</div>
    <? include "sitemap_clubs_directory.php"; ?>

    <br />
    <div class="column_title">Employment Directory</div>
    <?
    $sql = "select title, id from jobs where approved=1 and complete=2 and funded=1 order by title";
    $q = sql_query( $sql );

    while( $r = mysql_fetch_array( $q ) )
    {
    ?>
      <div class="sml"><a href="/employment/jobs_detail.php?d=<?=$r['id']?>"><?=$r['title'];?></a></div>
    <?
    }
    ?>
  </div>

  <div class="sitemap_column">
    <div class="column_title">Business Directory</div>
    <? include "sitemap_business_directory.php"; ?>
  </div>

  <div class="sitemap_column">
    <div class="column_title">Member Directory</div>
    <? include "sitemap_member_directory.php"; ?>
  </div>

  <div class="sitemap_column">
    <div class="column_title">Occupation Directory</div>
    <? include "sitemap_occupation_directory.php"; ?>
  </div>

  <div class="sitemap_column">
    <div class="column_title">Vessel Directory</div>
    <? include "sitemap_vessel_directory.php"; ?>
  </div>



</div>


<?
include "newfooter.php";
?>
