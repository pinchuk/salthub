<?php
/*
This is the header used during the signup process.  It shows what step they're in during the signup process.
*/

include_once $_SERVER['DOCUMENT_ROOT'] . "/inc/inc.php";
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="shortcut icon" href="/favicon_<?=$site?>.ico" />
	<link rel="stylesheet" href="/style_<?=$site?>.css?ver=<?=filemtime($_SERVER['DOCUMENT_ROOT'] . "/style_$site.css")?>" type="text/css" />
	<link rel="stylesheet" href="/style.css?ver=<?=filemtime($_SERVER['DOCUMENT_ROOT'] . "/style.css")?>" type="text/css" />
	<title><?= $defaultPageTitle ?></title>
<?
$scripts[] = "/jquery/jquery-1.4.4.min.js";
$scripts[] = "/jquery/jquery.tabSlideOut.v1.3.js";

if( is_array( $scripts ) )
{
  loadJS($scripts);
  unset($scripts);
}
?>
	<script language="javascript" type="text/javascript">var script = "<?=end(explode("/", $script))?>";</script>
	<script language="javascript" type="text/javascript" src="/odometer.js"></script>
	<script language="javascript" type="text/javascript" src="/simpleajax.js"></script>
	<script language="javascript" type="text/javascript" src="http://static.ak.connect.facebook.com/js/api_lib/v0.4/FeatureLoader.js.php/en_US"></script>
	<script language="javascript" type="text/javascript" src="/common.js"></script>
	<script language="javascript" type="text/javascript" src="/tinybox/tinybox.js"></script>
	<script language="javascript" type="text/javascript" src="/flowplayer/flowplayer-min.js"></script>
	<link rel="stylesheet" href="/style.css" type="text/css" />
	<link rel="stylesheet" href="/style_<?=$site?>.css" type="text/css" />
</head>


<div style="height: 200px; background-image: url(/images/entrygradient_sm.png);">
	<div style="width: 1053px; margin: 0 auto; padding-top: 22px;">
		<div style="width: 800px; float: left;">
			<div style="padding-top: 30px;">
				<img src="/images/salt_badge100.png" style="margin: 0 15px -3px 0;" alt="" />
				<span style="color: #717171; font-family: tahoma; font-size: 28pt;"><? if( isset( $page_header ) ) echo $page_header; else echo 'Getting Started'; ?></span>
				<? if( isset( $step ) ) { ?><span style="color: #717171; padding-left:20px;">Step <? echo $step ?> of 3 <img src="/images/<? echo $step ?>of3.png" width="102" height="14" alt="" /></span><? } ?>
			</div>
		</div>
	</div>
</div>

<div class="slide-out-div">
    <a class="handle" href="http://www.<? echo $siteName ?>.com"><? echo $siteName ?></a>
    <h3>Feedback</h3>
    <p>Are you having trouble with something or would you like to see a new feature?
    </p>

    <div id="feedbackContainer" style="text-align:center;">
      <textarea name="" rows="5" cols="35" id="feedbackText"></textarea>

      <div style="text-align:right;">
      <input type="button" name="" value="Send Feedback" onclick="javascript: sendFeedback();" class="button" style="margin-top:15px;"/>
      </div>
    </div>

</div>


<script type="text/javascript" language="javascript">
<!--

$('.slide-out-div').tabSlideOut({
    tabHandle: '.handle',                              //class of the element that will be your tab
    pathToTabImage: '/images/feedback.png',          //path to the image for the tab *required*
    imageHeight: '76px',                               //height of tab image *required*
    imageWidth: '22px',                               //width of tab image *required*
    tabLocation: 'left',                               //side of screen where tab lives, top, right, bottom, or left
    speed: 300,                                        //speed of animation
    action: 'click',                                   //options: 'click' or 'hover', action to trigger animation
    topPos: '250px',                                   //position from the top
    fixedPosition: true                               //options: true makes it stick(fixed position) on scroll
});

-->

</script>
