<?php
/*
Step 2 of the signup process.  This page asks the user to add their work and school experience, as well as their
"headline" and sector descriptions.
*/

$script[] = "/profile/profilemgmt.js.php";

$step=2;
$page_header = "About You";
include "step-header.php";



if (empty($user['uid']))
	$user['uid'] = $API->uid;

$x = sql_query(
	"select * from
		(select wid,unix_timestamp(start) as start,
			unix_timestamp(stop) as stop,
			if(stop=0,1,0) as present,
			w.descr,
			employer,
			occupation,
			w.www,g1.gname as employer_name,
			g2.gname as occupation_name
		from work w
			left join pages g1 on g1.gid=employer
			left join pages g2 on g2.gid=occupation
		where w.uid={$user['uid']})
	as tmp order by present desc,start desc"
	);

$works = array();
while ($y = mysql_fetch_array($x, MYSQL_ASSOC))
	$works[$y['wid']] = $y;

$edus = array();
$x = sql_query("select start,stop,school,gname as school_name from education inner join pages on gid=school where uid=" . $user['uid'] . " order by start");
while ($y = mysql_fetch_array($x, MYSQL_ASSOC))
	$edus[] = $y;

?>

<div class="newusercontent">

<div style="float:left;"><h1>Get started with your profile</h1></div>


<div style="float: left; clear:left;">
<div style="font-size: 10pt;">
   Users with complete profiles are <b>35 times more likely</b> to connect with the users that matter most to them on <? echo $siteName ?>.
</div>

<div style="width: 625px;">
	<div style="font-weight: bold; color: #326798; font-size: 12pt; margin-top: 15px;">Sector and Headline</div>

	<div style="padding-top: 10px;">
		<div style="border: 1px solid #d8dfea; background: #F3F8FB; padding: 20px;">


      <div id="educationedit" style="padding-top: 10px;">
      	<div class="education">
      		<div class="period" style="float:left; width: 210px;">
      			<div class="smtitle2">Sector: </div>
      		</div>
      		<div style="float: left;">
      			<div class="smtitle2">Headline: </div>
      		</div>
      	</div>

<?
$user = queryArray( "select occupation, sector from users where uid='" . $API->uid . "'" );
?>

      	<div style="clear:both;">
      		<div class="period" style="float:left; width: 210px;">
            <select id="sector" style="width:180px;" id="sector" onchange="javascript: showSurpriseForm();">
            <option value="0">(None Selected)</option>
            <?
            $q2 = sql_query( "select * from categories where cattype='G' and industry='" . PAGE_TYPE_BUSINESS . "' order by catname" );
            while( $r2 = mysql_fetch_array( $q2 ) )
            {
            ?>
            <option value="<? echo $r2['cat']; ?>"<? if( $r2['cat'] == $user['sector'] ) echo " SELECTED"; ?>><? echo $r2['catname']; ?></option>
            <?
            }
            ?>
            </select>
      		</div>

      		<div style="float: left;">
            <div style="float:left;"><input id="occupation" type="text" style="width:180px;" value="<?=$user['occupation'];?>" onkeydown="javascript: showSurpriseForm();"/></div>
            <div style="float:left; padding-left:10px;"><img src="/images/help.png" width="16" height="16" alt="" onclick="javascript: showHeadlinePopup();" style="cursor:pointer;"/></div>
      		</div>
        </div>

      	<div style="clear: both; height: 20px;"></div>
      </div>

<div style="display:none;" id="surprise_form">
			<div class="smtitle">Bragging Rights</div>

      <div style="padding-top: 10px;">
        <textarea id='about' style="width:483px; height:35px;" onclick="if( this.value=='Add something about yourself or do some bragging here. Examples: have 3 kids, own a fishing boat, surf and work in the yacht charter biz.' ) this.value='';">Add something about yourself or do some bragging here. Examples: have 3 kids, own a fishing boat, surf and work in the yacht charter biz.</textarea>
      	<div style="clear: both; height: 20px;"></div>
      </div>

			<div class="smtitle">Education <span style="font-wise:8pt; color:#555;">(recommended)</span></div>

      <div id="educationedit" style="padding-top: 10px;">
      	<div class="education">
      		<div class="period">
      			<div class="smtitle2">Period:</div>
      		</div>
      		<div style="float: left;">
      			<div class="smtitle2">School:</div>
      		</div>
      	</div>

      	<div id="educontainer"></div>

      	<div style="clear: both; height: 20px;"></div>
      </div>


			<div class="smtitle">Work <span style="font-wise:8pt; color:#555;">(recommended)</span> &nbsp;&nbsp;<input type="button" class="button" value="Add Job" onclick="javascript:selectWorkType();" /></div>

      <div id="workedit" style="padding-top: 10px;">

      	<?php
      	$wids = array();
      	foreach ($works as $work)
      	{
      		showEditWork($work);
      		$wids[] = $work['wid'];
      	}
      	?>

      	<div id="newworks"></div>



      </div>
</div>

      	<div style="clear: both; padding-left: 45px;">
      		<input type="button" class="button" value="Next &gt;" onclick="javascript:saveWorkAndEducation();" /> &nbsp;

      	</div>

</div>
	</div>
<!--
	<div style="padding-top: 5px; font-size: 9pt;" id="divskip">
		<a style="text-decoration: none;" href="addphoto.php">skip &gt;</a>
	</div>-->
</div>
</div>

<div style="clear: both;"></div>



<script language="javascript" type="text/javascript" src="/profile/profilemgmt.js.php?r=<? echo rand(); ?>"></script>
<script language="javascript" type="text/javascript" src="/actb.js"></script>

<script language="javascript" type="text/javascript">
<!--
var schools = [];
var edus = <?=json_encode($edus)?>;
var eduShown = 0;
var eduMax = 0;

for (var i in edus)
	eduAdd(edus[i]);

eduCheckAdd(); //add a blank entry
showSurpriseForm();

function showSurpriseForm()
{
  if( selectedValue( document.getElementById('sector') ) > 0 && document.getElementById('occupation').value != '' )
  {
    //document.getElementById('surprise_form').style.display='';

  }
}

function showHeadlinePopup()
{
  showPopUp("", "<div style=\"font-family: Arial; font-weight:200; text-align:left; font-size:9pt;\"><b style=\"font-weight:bold\">Your Headline</b><br /><br />This will be displayed next to your name throughout <? echo $siteName ?>. Your headline allows you to provide an \"identity\" showing descriptive text. This will allow others to quickly understand what you do.<br /><br /><b style=\"font-weight:bold\">Examples might be:</b><ul><li>Maritime Consultant / Expert</li><li>Hands-on Yacht Manager</li><li>Yacht Build Engineer</li><li>Superyacht Broker</li></ul></div>", [500]);
}

function eduAdd(edu)
{
	html  = '<div class="education" id="edu-' + eduMax + '">';
	html += '	<div class="period">';
	for (j = 0; j < 2; j++)
	{
		if (j == 1) html += " to ";
		html += '<select id="edu-year' + j + '-' + eduMax + '"><option></option>';
		for (y = <?=date("Y") - 50?>; y <= <?=date("Y") + 8?>; y++)
		html += '<option value="' + y + '" ' + (y == (j == 0 ? edu.start : edu.stop) ? "selected" : "") + '>' + y + '</option>';
		html += '</select>';
	}
	html += '	</div>';
	html += '	<div style="float: left;">';
	html += '		<input onkeypress="javascript:eduCheckAdd();" type="text" id="edu-school-' + eduMax + '" style="width: 275px;" value="' + (edu.school_name ? edu.school_name : "") + '" />';
	html += '		&nbsp; &nbsp; &nbsp; <a href="javascript:void(0);" onclick="javascript:eduRemove(' + eduMax + ');">X</a>';
	html += '	</div>';
	html += '</div>';

	newdiv = document.createElement("div");
	newdiv.innerHTML = html;

	document.getElementById("educontainer").appendChild(newdiv);

	schools[eduMax] = new actb(document.getElementById("edu-school-" + eduMax), "school");
	if (edu.school) schools[eduMax].actb_val = edu.school;

	eduShown++;
	eduMax++;
}

function eduRemove(i)
{
	if (eduShown == 1)
	{
		document.getElementById("edu-year0-" + i).selectedIndex = 0;
		document.getElementById("edu-year1-" + i).selectedIndex = 0;
		document.getElementById("edu-school-" + i).value = "";
	}
	else
	{
		document.getElementById("edu-" + i).style.display = "none";
		eduShown--;
	}
}

function eduCheckAdd()
{
	for (i = 0; i < eduMax; i++)
	{
		if (document.getElementById("edu-" + i).style.display == "" && document.getElementById("edu-school-" + i).value == "")
			return;
	}

	eduAdd({'start':'0', 'stop':'0', 'school':''});
}

var works = <?=json_encode($works)?>;
var wids = ['<?=implode("', '", $wids)?>'];
var employers = [];
var occupations = [];

// autocomplete
for (var i in works)
{
	employers[i] = new actb(document.getElementById("employer-" + i), "employer");
	employers[i].actb_val = works.employer;
	occupations[i] = new actb(document.getElementById("occupation-" + i), "occupation");
	occupations[i].actb_val = works.occupation;
}

<? if( !$isDevServer ) {
  include_once( "../inc/tracking_code.php" );
} ?>
//-->

</script>

</div>

</body></html>