<?php
/*
This pas is where the user verifies their email address.  An email is sent to the user (from this page)
in which they respond and are directed here. When they confirm their email address, their account is
flagged (in the user table, field "verify").


10/31/2012 - This URL still performs the verification function, but forwards to the welcome page now by default.
*/

if( empty( $_SESSION['uid'] ) && isset( $_GET['cc'] ) )
{
  session_start();
  session_destroy();
  session_start();
  $_SESSION['uid'] = $_GET['uid'];
}


include "../inc/inc.php";

include_once "../inc/recaptchalib.php";
$publickey = "6LciL78SAAAAANVcbxyOQtkOkEyJN1YaGs_HUe4M";
$privatekey = "6LciL78SAAAAAKjsEQNYz2kD7Dm__mk4jshxleAk";

/*
$headline = quickQuery( "select occupation from users where uid='" . $API->uid . "'" );
if( $headline == "" )
{
  header( "Location: /signup/update_profile.php" );
  die();
}
*/

/*
$pid = quickQuery( "select pic from users where uid='" . $API->uid . "'" );
if( $pid == 0 || $pid == "" )
{
  header( "Location: /signup/addphoto.php" );
  die();
}
*/


/*
if ($API->verify == 1)
{
  if( isset( $_SESSION['login_redirect'] ) && $_SESSION['login_redirect'] != "" )
    header( "Location: " . $_SESSION['login_redirect'] );
  else
  	header("Location: /welcome?n");
  unset( $_SESSION['login_redirect'] );
	die();
}
*/

if (isset($_POST['email']) && $_POST['email'] != "" )
{
	$email = $_POST['email'];
	sql_query("update users set email='$email' where uid=" . $API->uid);
}
else
	$email = null;


$resend = 0;

if ( isset($_POST['resend'] ) )
  $resend = intval( $_POST['resend'] );
else if( isset( $_REQUEST['resend'] ) )
  $resend = intval( $_REQUEST['resend'] );

if( ( $resend == 1 && empty( $_COOKIE["SEND_VERIFY_EMAIL"] ) ) || $resend > 1 )
{
 	$email = $API->sendConfirmationEmail();
  setcookie( "SEND_VERIFY_EMAIL", 1 );
}
else
{
	$info = $API->getUserInfo(null, "email");

  if( isset( $info['email'] ) )
  {
    $email = $info['email'];
  }
}

$hasEmail = isset($email);
$hasSocial = quickQuery("select count(*) from users where uid=" . $API->uid . " and (fbid is not null or twid is not null)") == 1;

if ($_GET['cc'])
{
	if ($_GET['cc'] == $API->generateDeleteCommentHash($_SESSION['uid'] . chr(2) . $email))
	{
    $active = quickQuery( "select verify from users where uid=" . $API->uid );
    if( $active == 0 ) //In this case, we signed up without an FB or TW account.
  		$API->feedAdd("J", SITE_OURS, $_SESSION['uid'], $_SESSION['uid']);

    if( isset( $_COOKIE['joingid'] ) )
    {
      $gid = $_COOKIE['joingid'];

      if( isset( $_COOKIE['friend_uid'] ) )
      {
        sql_query( "insert into friends (id1, id2, status) values (" . $API->uid . "," . $_COOKIE['friend_uid'] . ", 0)" );
    		$API->feedAdd("G", $gid, $API->uid, $_COOKIE['friend_uid'], $gid );
      }
      else
     		$API->feedAdd("G", $gid, $API->uid, $API->uid, $gid );
    }

    $_SESSION['verify'] = 1;

    $API->convertCustomContactsToUserContacts();

		sql_query("update users set verify=1 where verify=0 and uid=" . $API->uid);
        sql_query("INSERT INTO page_members(uid, gid) VALUES(".$API->uid.", 15528)");

		if (mysql_affected_rows() == 1)
		{
      sql_query( "update photos set reported=0 where uid='" . $API->uid . "' and reported=2" );
      sql_query( "update videos set reported=0 where uid='" . $API->uid . "' and reported=2" );

			foreach ($_SESSION as $k => $v)
				unset($_SESSION[$k]);

			$_SESSION['uid'] = $API->uid;
			$_SESSION['bypassLogin'] = 1;
			header("Location: /signup/login.php");
		}
		else //user was already verified
			header("Location: /welcome?n");

		die();
	}
	else
		$confirmFail = true;
}

$scripts[] = "/checkavail.js";


// This was implemented on Oct 31, 2012.  This functionality has been moved to the welcome page.
header( "Location: /welcome" );
exit;

$page_header = "Email Verification";
include "step-header.php";

?>

<div class="newusercontent">
	<h1>Welcome to <?=$siteName?>, <?=$API->name?>!</h1>

<table border=0 cellpadding=0 cellspacing=0 width="95%" align="center">
	<tr>
		<td style="padding: 5px;">

<table border=0 cellpadding=0 cellspacing=0>
	<tr valign="top">
		<td width=178 style="padding-right: 5px; text-align: center;">
			<img src="<?=$API->getThumbURL(0, 178, 266, $API->getUserPic())?>" alt="" />
		</td>
		<td style="padding-left: 15px;">
			<div style="width: 430px; border: 1px solid #c0c0c0; background: #EDEFF4; padding: 10px 5px;">

				<div style="padding-top: 20px;">
					<table border=0 cellpadding=0 cellspacing=0>
						<tr>
							<td width=30>
								<img src="/images/email_go.png">
							</td>
							<td style="font-weight: bold; color: #326798; font-size: 9pt;">
								<?=$hasEmail ? ($_GET['a'] == '1' ? 'When you signed up, we' : 'We just') . ' sent you a message at <span style="color: #800080;" id="em_addr">' . $email . '</span>.' : 'Please provide us with your e-mail address.' ?>
							</td>
						</tr>
					</table>
				</div>
				<script language="javascript">
				email2 = "s@";
				email3 = "<?=$siteName?>.com";
				email1 = "notification";

				email = email1 + email2 + email3;
        email2 = "signup@salthub.com";

				</script>
				<form name="frm" <?=$hasEmail ? "" : 'onsubmit="javascript:return checkForm();"'?> method="post" action="/signup/verify.php">
				<input type="hidden" name="resend" value="2" />
				<div style="padding-top: 10px; font-size: 9pt; padding-right: 50px;">
					Confirming your account will give you full access to SaltHub, including professionals and businesses. Simply follow the link provided in our e-mail, and you're good to go!
					<p />
					If you did not get our e-mail, add
					<script language="javascript">
					document.write("&quot;" + email + "&quot;");
					</script>
					to your safe senders list and click below to send our message again, or use a different email address and try again. Please also check your junk/spam folders.
					<? if (!$hasEmail) { ?>
					<br /><br />E-mail address: &nbsp; &nbsp; <input type="text" name="email" id="email" style="width: 260px;" />
					<?php } ?>
				</div>
				<div style="padding: 10px 50px 0 0; text-align: center; font-size: 9pt; line-height:180%;">
					<?php
					if (!$hasEmail)
					{
						?>
						<input type="submit" value="Add E-mail Address" class="button" />
						<? //&nbsp; &nbsp; <input type="button" value="Remind Me Later" class="button" onclick="javascript:location='/welcome';" /> ?>
						<?php
					}
					elseif ($_POST['resend'] == "1") { ?>
						Your confirmation e-mail has been <?=isset($_POST['email']) ? "" : "re"?>sent.
						<div style="padding-top: 10px;">
							<input type="submit" value="Send Again" class="button">
							<?php if ($hasSocial) { ?>&nbsp; &nbsp; <input type="button" value="Remind Me Later" class="button" onclick="javascript:location='/welcome';" /><?php } ?>
						</div>
					<?php } else { ?>
						<input type="submit" value="Resend Message" class="button">
						<?php if ($hasSocial ) { ?>&nbsp; &nbsp; <input type="button" value="Remind Me Later" class="button" onclick="javascript:location='/welcome';" /><?php } ?>
            <br />
            <div style="width:100%; text-align:center;"><br /><a href="javascript:void(0);" onclick="javascript:changeEmailAddr();">use a different email</a>
            or login with
            <a href="/fbconnect/login.php"><img src="/images/facebook16.png" style="vertical-align: bottom;" alt="" /></a>&nbsp;
						<a href="/twitter/login.php"><img src="/images/twitter16.png" style="vertical-align: bottom;" alt="" /></a>&nbsp;
            </div>

					<?php } ?>
				</div>
				</form>

				<div style="margin-top: 30px; border: 1px solid #c0c0c0; padding: 5px;">
					<table border=0 cellpadding=0 cellspacing=0>
						<tr>
							<td width=30>
								<img src="/images/information.png">
							</td>
							<td style="font-weight: bold; font-size: 9pt;">
								Having trouble?
							</td>
						</tr>
						<tr>
							<td></td>
							<td style="font-size: 9pt; padding-top: 2px;">
								If you are not able to verify your account, you can log in at any time to return to this page.&nbsp; You can also e-mail us with your username and the e-mail you registered with at
								<a style="text-decoration: none; color: #326798;" href="javascript:void(0);" onclick="javascript: sendEmail( email2, '' );">signup@<? echo $siteName ?>.com</a>
							</td>
						</tr>
					</table>
				</div>

			</div>
		</td>
		<td style="padding-left: 15px;">
      <div style="float:left; width: 250px;">
        <div>
    		  <div style="float:left; font-family: arial; color: #555555; font-size: 16pt; font-weight: bold;">After Verification</div>
        </div>

        <div style="clear:both; font-family: arial; color: rgb(50, 103, 152); font-size: 9pt; padding-top: 10px; font-weight:bold;">Add your Business</div>
        <div>
          <div style="float:left; margin-right:10px; padding-top:5px;"><img src="/images/business.png" width="100" height="75" /></div>
          <div style="float:left; font-family: arial; color: #555555; font-size: 8pt;  width:140px;">After you have joined with your real name, you can add your company page, which can also be submitted to the <?=$siteName?> directory for even more exposure.</div>
        </div>

        <div style="clear:both; font-family: arial; color: rgb(50, 103, 152); font-size: 9pt; padding-top: 30px; font-weight:bold;">Get Discovered</div>
        <div>
          <div style="float:left; margin-right:10px; padding-top:5px;"><img src="/images/connect_map.png" width="100" height="75" /></div>
          <div style="float:left; font-family: arial; color: #555555; font-size: 8pt;  width:140px;">Once your profile and company page are set up, you will be able to connect with industry experts, coworkers (past and present), employers and decision makers.</div>
        </div>


      </div>
		</td>
	</tr>
</table>

</div>

<script language="javascript" type="text/javascript">
<!--
function checkForm()
{
	valid = checkEmail(document.getElementById("email").value);

	if (valid)
		return true;
	else
	{
		showPopUp2("", "Please enter a valid e-mail address.");
		return false;
	}
}

function refreshEmailAddr( email )
{
  e1 = document.getElementById('em_addr');
  if( e1 )
  {
    e1.innerHTML = email;
  }
  closePopUp();
}

function changeEmailAddr()
{
  showPopUp2("Change Email Address", '<div style="width:450px;" id="verifypage">Email: 	<input type="text" id="email" onkeyup="javascript:if (this.value.indexOf(\'@\') > 0) checkAvail(this,\'refreshEmailAddr\');" maxlength="65" value="" size="35"/><br /><span id="email-avail" style="height:30px;">&nbsp;</span></div>');

}

<?php if ($confirmFail) echo 'showPopUp2("", "Your confirmation code was incorrect.<p />Please check the link carefully and try again.", 500);' . "\n"; ?>

<? if( !$isDevServer ) {
  include_once( "../inc/tracking_code.php" );
} ?>
//-->
</script>