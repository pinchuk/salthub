function parseScript(_source) {
	var source = _source;
	var scripts = new Array;
	while (source.indexOf("<script") > -1 || source.indexOf("</script") > -1) {
		var s = source.indexOf("<script");
		var s_e = source.indexOf(">", s);
		var e = source.indexOf("</script", s);
		var e_e = source.indexOf(">", e);
		scripts.push(source.substring(s_e + 1, e));
		source = source.substring(0, s) + source.substring(e_e + 1);
	}
	for (var i = 0; i < scripts.length; i++) {
		try {
			eval(scripts[i]);
		} catch (ex) {}
	}
	return source;
}

function postAjax(target, contents, func) {
    var ajax;
	try {
		ajax = new XMLHttpRequest;
	} catch (e) {
		try {
			ajax = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try {
				ajax = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e) {
				if (!silent) {
					alert("Please update your web browser before using this function.");
				}
				return;
			}
		}
	}

	ajax.onreadystatechange = function () {
		if (ajax.readyState == 4) {
			if (func !== null) {
				if (typeof func == "function")
					func(ajax.responseText);
				else
					eval(func + "('" + ajax.responseText.replace(/\n/g, "\\n").replace(/\r/g, "").replace(/'/g, "\\'") + "')");
			}
			ajaxBusy = false;
		}
	};
	ajax.open("POST", target, true);
	ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=iso-8859-1");
	ajax.send(contents + "&rand=" + Math.random());
}

function encodeLatin1URIComponent(str) {
	var bytes = '';
	for (var i = 0; i < str.length; i++)
	bytes += str.charCodeAt(i) < 256 ? str.charAt(i) : '?';
	return escape(bytes).split('+').join('%2B');
}

function getAjax(target, func)
{
	//log("\nGET      >> " + target);
	
	var ajax;
	ajaxBusy = true;
	try {
		ajax = new XMLHttpRequest;
	} catch (e) {
		try {
			ajax = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try {
				ajax = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e) {
				if (!silent) {
					alert("Please update your web browser before using this function.");
				}
				return;
			}
		}
	}
	ajax.onreadystatechange = function () {
		if (ajax.readyState == 4) {
			if (func !== null)
			{
				//log("RESPONSE << " + target);
			
				if (typeof func == "function")
					func(ajax.responseText);
				else
					eval(func + "('" + ajax.responseText.replace(/\n/g, "\\n").replace(/\r/g, "").replace(/'/g, "\\'") + "')");
			}
			ajaxBusy = false;
		}
	};
    ajax.open("GET", target + "&rand=" + Math.random(), true);
	ajax.send(null);
}

function getFormVals(frm) {
	vals = "";
	for (i = 0; i < frm.elements.length; i++) {
		if (frm.elements[i].name == "") {
			continue;
		}
		vals += "&" + frm.elements[i].name + "=";
		if (frm.elements[i].type == "hidden" || frm.elements[i].type == "text" || frm.elements[i].type == "textarea") {
			vals += frm.elements[i].value;
		} else if (frm.elements[i].type == "checkbox") {
			if (frm.elements[i].checked) {
				vals += "on";
			} else {
				vals += "off";
			}
		} else if (frm.elements[i].type == "select-one") {
			vals += escape(frm.elements[i].options[frm.elements[i].selectedIndex].value);
		}
	}
	vals = vals.substring(1);
	return vals;
}