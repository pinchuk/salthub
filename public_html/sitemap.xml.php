<?php

if ($_GET['xmp'] == '1')
	echo '<xmp>';

include 'inc/inc.php';

$limit = 100;

$nav = array(
		array('title' => 'About Advertising', 'url' => 'about-directory.php'),
		array('title' => 'About Business Listings', 'url' => 'about-directory.php'),
		array('title' => 'About Claiming Pages', 'url' => 'about-claim_page.php'),
		array('title' => 'About Pages', 'url' => 'about-pages.php'),
		array('title' => 'About SaltHub', 'url' => 'about.php'),
		array('title' => 'Business Directory', 'url' => 'directory.php'),
		array('title' => 'Reset Password', 'url' => 'signup/resetpw.php'),
		array('title' => 'SaltHub News', 'url' => 'page/15528-SaltHub/logbook'),
		array('title' => 'Vessel Heat Map', 'url' => 'vessels/vessel_live_tracking.php'),
		array('title' => 'Log-in and Welcome', 'url' => 'signup')
	);

echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n";
echo '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">' . "\n";

foreach ($nav as $n)
	print_xml($n);

$wheres = array("p.type=" . PAGE_TYPE_BUSINESS, "p.type=" . PAGE_TYPE_VESSEL, "p.subcat=" . CAT_CLUB_ASSOC, "p.cat=" . OCCUPATION_GRP);

foreach ($wheres as $where)
{
	$x = mysql_query(get_query_for_page($where));
	while ($y = mysql_fetch_array($x, MYSQL_ASSOC))
		print_xml($y);
}

// users

$q = "select if(isnull(name), username, name) as title, concat('user/', replace(if(isnull(username), concat('_', users.uid), username), ' ', '.')) as url, concat(container_url, '/', hash, '_square.jpg') as image
	from users
	left join photos on photos.id=users.pic
	where dummy=0
	order by users.uid
	limit $limit";

$x = mysql_query($q);
while ($y = mysql_fetch_array($x, MYSQL_ASSOC))
	print_xml($y);

echo '</urlset>';

function get_query_for_page($where)
{
	global $limit;
	
	$q = "select gname as title,concat(container_url, '/', hash, '_square.jpg') as image,concat('page/', gid, '-', replace(gname, ' ', '-')) as url from pages p
		natural join page_members pm
		left join photos on p.pid=photos.id
		left join users on photos.uid=users.uid
		where $where
		group by pm.gid
		order by count(pm.gid) desc
		limit $limit";
	
	//die($q);
	
	return $q;
}

function print_xml($opts)
{
	global $siteName;
	
	$base_url = 'http://' . $_SERVER['HTTP_HOST'] . '/';
	echo '<url>' . "\n";
	echo '	<loc>' . $base_url . $opts['url'] . '</loc>' . "\n";
	echo '	<image:image>' . "\n";
	echo '	   <image:loc>' . htmlentities($opts['image'] ? $opts['image'] : $base_url . 'images/salt_badge100.png') . '</image:loc>' . "\n";
	echo '	   <image:title>' . htmlentities($opts['title']) . '</image:title>' . "\n";
	
	if ($opts['descr'])
	echo '	   <image:caption>' . htmlentities($opts['descr']) . '</image:caption>' . "\n";
	
	echo '	</image:image>' . "\n";
	
	if ($opts['priority'])
	echo '	<priority>' . $opts['priority'] . '</priority>' . "\n";
	
	echo '</url>' . "\n";
}

/*

<url>
	<loc>http://www.example.com/</loc>
	<image:image>
	   <image:loc>http://example.com/image.jpg</image:loc>
	   <image:title></image:title>
	   <image:caption></image:caption>
	</image:image>
	<priority>0.5</priority>
</url>

*/

?>