<?
//require( "inc/api.php" );

//echo "blah";

$name = '_м _м" ';
$name = cleanName($name);
if(!$name){
    echo 'No';
}else{
    echo $name;
}


/* Checks is name correct
 * @param: $name(string)
 * @param: $charlist(string)
 * @return: true | false
 */
function isAllowed($name, $charlist = '!@#$%^*+=<>?~`:;'){    
    $is_denied = strpbrk($name, $charlist);
    return !$is_denied;   
}

/* Cleans the name from bad symbols after checks it
 * @param: $name(string)
 * @return: $clean_name(string) | false 
 */
function cleanName($name){
    if(!isAllowed($name)){
        return false;
    } 
    $bad_list = array('"', '_');
    $replace_list = array('', ' ');
    $name = str_replace($bad_list, $replace_list, $name);
    $name = trim($name);
    if(!empty($name)){
        return $name;
    }else{
        return false;
    } 
}
?>