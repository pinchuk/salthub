<?php
include_once "inc/inc.php";
include_once "inc/recaptchalib.php";

$title = "Terms of Use | $siteName";

$page_header = "Terms of Use $siteName";
  $sub_header = $siteName . ' is the leading social network for professionals and businesses who make a living on and around the water.';

$scripts[] = "/index.js";
$noheader = true;

if( sizeof( $_REQUEST ) > 0 )
{
  echo '<html><title></title><link rel="stylesheet" href="/style_s.css" type="text/css" /><link rel="stylesheet" href="/style.css" type="text/css" /><body>';
}
else
{
  if( $API->isLoggedIn() )
  {
    $background = "url('images/bggraygrad2.png'); background-repeat:repeat-x; margin-top:-5px; width:100%;";
    include "header.php";
  }
  else
  {
    include "signup/newheader.php";
    include "header_gradient.php";
  }
}

if( sizeof( $_REQUEST ) == 0 )
{
?>
<div class="graygradient">
	<div class="headcontainer">
		<div class="entryslide">
			<?=$htmlSignIn?>
			<div class="slidecontent">
				<span class="title">Terms of Use</span>

				<div class="text">
We have a few rules. The terms of service below will provide you information about using SaltHub and how to get the most out of our service.
				</div>

			</div>

			<img src="/images/TOS.jpg" class="bigimage" alt="" />
		</div>
	</div>
</div>
<div style="width:1050px; margin-left: auto; margin-right:auto;">
<? } else { ?>
<div style="width:550px; margin-left: 0;">
<? } ?>
  <div style="font-size: 11pt; padding: 5px;">
  <?=quickQuery("select content from static where id='tos'")?>
  </div>
</div>

<?php
if( sizeof( $_REQUEST ) > 0 )
  echo "</body></html>";
else
  include "signup/newfooter.php";
?>