<?php
/*
11/10/2012 - Added support for secondary accounts.

*/

include "../inc/inc.php";

require_once('twitteroauth/twitteroauth.php');
require_once('config.php');

if( isset( $_REQUEST['invite']) )
{
  $_SESSION['twReturnToInvite'] = $_REQUEST['invite'];
}

/* Build TwitterOAuth object with client credentials. */
$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET);
 
/* Get temporary credentials. */
$request_token = $connection->getRequestToken(OAUTH_CALLBACK);

/* Save temporary credentials to session. */
$_SESSION['oauth_token'] = $token = $request_token['oauth_token'];
$_SESSION['oauth_token_secret'] = $request_token['oauth_token_secret'];


if( isset( $_GET['secondary'] ) )
{
  $_SESSION['enabling_secondary_account'] = 1;
}

/* If last connection failed don't display authorization link. */
switch ($connection->http_code) {
  case 200:
    /* Build authorize URL and redirect user to Twitter. */
    $url = $connection->getAuthorizeURL($token);
    header('Location: ' . $url); 
    break;
  default:
    /* Show notification if something went wrong. */
	$_SESSION['error'] = "Twitter's servers are currently experiencing some login issues.&nbsp; Please try logging in later, or login with your Facebook account.";
	header("Location: /");
}
