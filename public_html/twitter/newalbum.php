<?php
/*
Creates an album and adds newly uploaded photos to the new album.
*/

include_once $_SERVER["DOCUMENT_ROOT"] . "/inc/inc.php";

if (!$fromEmail && !$API->isLoggedIn())
	die("BAD_SESSION");

if (!$fromEmail)
	createAlbum();

function createAlbum()
{
	global $API, $site, $aid;
	
	$target = intval($_POST['target']);

	$ids = split(",", $_POST['ids']);

	if (intval($ids[0]) == 0) die();

	$mainImage = intval($_POST['m']);
  if( $mainImage == 0 ) $mainImage = $ids[0];
	$title = str_replace("\n", "", $_POST['title']);
	$descr = str_replace("\n", "", $_POST['descr']);
	$cat = intval($_POST['cat']);
	$gid = intval($_POST['gid']);

	if (empty($title) || empty($descr)) return;

	if (!in_array($mainImage, $ids)) die();

  $fbtarget = null;
  if( isset( $_POST['fbtarget'] ) )
  {
    $fbtarget = $_POST['fbtarget'];
    if( $fbtarget == 0 )
      $fbtarget = null;
  }

  //Are we going to post to Twitter and Facebook?
  $posttw = 1;
  $postfb = 1;

  if( isset( $_POST['postfb'] ) )
    $postfb = intval( $_POST['postfb'] );
  if( isset( $_POST['posttw'] ) )
    $posttw = intval( $_POST['posttw'] );
  //

	$x = mysql_query("select jmp,hash,id from photos where id=$mainImage");
	$mainImageData = mysql_fetch_array($x, MYSQL_ASSOC);

  $err = "OK";

	if ($site == "s" && count($ids) == 1) //single image upload
	{
		$aid = quickQuery("select id from albums where uid=" . $API->uid . " and albType=1");

		mysql_query("update photos set cat=$cat,aid=$aid,ptitle='$title',pdescr='$descr' where id=" . intval($ids[0]) . " and uid=" . $API->uid);
		mysql_query("update albums set mainImage=$mainImage where id=$aid and uid=" . $API->uid);
		if ($gid > 0 && $site=='s')
			mysql_query("insert into page_media (type,id,gid) values ('P'," . intval($ids[0]) . ",$gid)");

  	$err = $API->sendNotification(NOTIFY_ALBUM_UPLOADED, array("uid" => $API->uid, "type" => "P", "title" => $title, "descr" => $descr, "id" => intval($ids[0]), "jmp" => $mainImageData['jmp'], "fbtarget" => $fbtarget, "skipfb" => !$postfb, "skiptw" => !$posttw, "gid" => $gid ));
	}
	else
	{
		mysql_query("insert into albums (title,descr,mainimage,uid) values ('$title','$descr',$mainImage," . $API->uid . ")");
		$aid = mysql_insert_id();

		foreach ($ids as $id)
		{
			mysql_query("update photos set created=now(),aid=$aid" . ($site == "s" ? ",cat=$cat" : "") . " where id=" . intval($id) . " and uid=" . $API->uid);
			if ($gid > 0 && $site=='s')
      {
				mysql_query("insert into page_media (type,id,gid) values ('P'," . intval($id) . ",$gid)");
      }
		}

  	$err = $API->sendNotification(NOTIFY_ALBUM_UPLOADED, array("uid" => $API->uid, "type" => "A", "title" => $title, "descr" => $descr, "mainimage" => $mainImageData['id'], "id" => $aid, "jmp" => $mainImageData['jmp'], "fbtarget" => $fbtarget, "skipfb" => !$postfb, "skiptw" => !$posttw, "gid" => $gid ));
	}

	$target = intval($_POST['target']);

	//see if pics were meant to be uploaded to someone else's profile and update their feeds

  $to = $target;
  $from = $API->uid;

  if( $gid > 0 )
  {
    $postAsPage = quickQuery( "select postAsPage from page_members where admin=1 and gid=$gid and uid=" . $API->uid );

    if( $postAsPage == 1 )
    {
      $to = -1;
      $from = -1;
    }
  }

	if (count($ids) == 1) //single pic
		$API->feedAdd("P", $mainImage, $to, $from, $gid);
	else //album
		$API->feedAdd("A", $aid, $to, $from, $gid);

	echo implode(":", array( $err, $aid, implode(",", $ids), $API->getThumbURL(0, 250, 258, "/photos/" . $ids[0] . "/" . quickQuery("select hash from photos where id=" . $ids[0]) . ".jpg")));
}

?>