<?php
/*
Contains javascript used throughout the site to handle file uploads.
*/

header("Content-type: text/javascript");
include "../inc/inc.php";
?>var vidswfu;
var swfuPhotos;
var catSelected = 0;
var uploadingType;
var currentPage = 0;
var defaultTxtStatus = "What's on your mind?";
var totalCharsAllowed = <?=115 - strlen(SITE_VIA)?>;

var uploadCategoryHTML = '<div style="width: 350px; margin: 0 auto;" class="smtitle2" style="color: #808080;"><div class="smtitle" style="text-align: center; padding-bottom: 15px;">Select a category</div><?php
if ($site == "s")
{
	$left = true;
	$x = mysql_query("select * from categories where cat != 2 and cattype='M' order by industry,catname");
	while ($cat = mysql_fetch_array($x, MYSQL_ASSOC))
	{
		echo '<div style="' . ($left ? '' : 'float: right; ') . 'width: ' . (150 - ($left ? 0 : 20)) . 'px;" class="catchoice">';
		echo '	<a class="smtitle2" onclick="javascript:uploadCategorySelected(' . $cat['cat'] . ', \\\'' . addslashes($cat['catname']) . '\\\');" href="javascript:void(0);"><input type="radio" id="uploadcat-' . $cat['cat'] . '" name="category" value="' . $cat['cat'] . '" />&nbsp;' . addslashes($cat['catname']) . '</a>';
		echo '</div>';

		$left = !$left;
	}
}
?><div style="clear: both; padding-top: 15px; text-align: center;"><input type="button" class="button" value="&nbsp; OK &nbsp;" onclick="javascript:doneCategorySelected();" /></div></div>';

function showTweetBox()
{
	if (document.getElementById("divtb1"))
	{
		showPopUp("", '<div id="tbpopup"></div>', 0, "no", 0, tbReplace);
		setTimeout('document.getElementById("tbpopup").appendChild(document.getElementById("divtb1"));', 100);
	}
	else
	{
		showPopUp("", tbHTML, 0, "no", 0);
		tbInitialize();
	}
}

function tbReplace()
{
	document.getElementById("divtb1container").appendChild(document.getElementById("divtb1"));
}

function txtStatusFocused(e, isFocused)
{
	if (isFocused)
	{
		if (e.value == defaultTxtStatus)
			e.value = "";
	}
	else
	{
		if (e.value == "")
			e.value = defaultTxtStatus;
	}
}

var disableWallPosts = false;

function txtStatusChanged(e,event)
{
/*
	l = e.value.length;

	charsleft = totalCharsAllowed - l;

	if (l >= totalCharsAllowed)
		e.value = e.value.substring(0, totalCharsAllowed);

	if (charsleft < 0)
		return false;

	document.getElementById("wpcharsremain").innerHTML = charsleft + " characters remaining";
*/

  FitToContent( "txtstatus", 150 );

  if( typeof event != "undefined" )
  {
    var charCode = event.keyCode || event.which;
  }

  document.getElementById("wpcharsremain").innerHTML = "";
}

function FitToContent(id, maxHeight)
{
  var text = id && id.style ? id : document.getElementById(id);
  if ( !text )
    return;

  if (text.clientHeight == text.scrollHeight) text.style.height = "20px";

  var adjustedHeight = text.clientHeight;
  if ( !maxHeight || maxHeight > adjustedHeight )
  {
    adjustedHeight = Math.max(text.scrollHeight, adjustedHeight);
    if ( maxHeight )
       adjustedHeight = Math.min(maxHeight, adjustedHeight);
    if ( adjustedHeight > text.clientHeight )
       text.style.height = adjustedHeight + "px";
  }
}

function wallPost()
{
  if( disableWallPosts ) return; //This is to prevent someone from pressing enter a bunch of times and getting many posts.

	post = document.getElementById("txtstatus").value;
	if (post == defaultTxtStatus) return;

  disableWallPosts = true;

  //For posting to FB pages
  e = document.getElementById("target_id");
  fbtarget = 0;
  if( e )
    fbtarget = e.value;

  e = document.getElementById("postfb");
  postfb = 0;
  if( e )
    postfb = (e.checked ? 1 : 0);

  e = document.getElementById("posttw");
  posttw = 0;
  if( e )
    posttw = (e.checked ? 1 : 0);

	if (site == "m")
  {
		postAjax("/updatesocialstatus.php", "target=" + tbTarget + "&fbtarget=" + fbtarget + "&post=" + post + "&postfb=" + postfb + "&posttw=" + posttw, "updateStatusHandler");
  }
	else
  {
		postAjax("/wallpost.php", "gid=" + currentPage + "&target=" + tbTarget + "&fbtarget=" + fbtarget + "&post=" + post + "&postfb=" + postfb + "&posttw=" + posttw, "wallPostHandler");
  }

	document.getElementById("txtstatus").value = defaultTxtStatus;
}

function updateStatusHandler(data)
{
	x = data.split(":");
	if (x[0] == "OK")
		location = x[1];
	else
	{
		if (admin == 1)
			alert("Error: " + data);
		else
			alert("An unknown error has occurred.  Please try again later.");
	}
}

function wallPostHandler(data)
{
  disableWallPosts = false;
	if (data == "OK")
	{
		if (typeof showNewLogEntries == "function")
		{
			showNewLogEntries();
			resetUpload(true);
		}
		else
			location.reload(true);
	}
	else if( data == "FB" ) //Facebook Error
  {
    document.location = "/facebook_error.php";
  }
	else
		alert("An unknown error has occurred.  Please try again later.");
}

function tbInitialize()
{
	vidswfu = new SWFUpload({
		// Backend Settings

		// File Upload Settings
	file_upload_limit : 0,
	file_queue_limit : 1,

		upload_url: "/upload/video.php?s=" + session_id,
		flash_url: '/upload/swfupload.swf',
		flash9_url: '/upload/swfupload_fp9.swf',
		button_image_url: '/images/btnbrowse.png',
		button_text: '',
		button_width: 71,
		button_height: 20,
		file_size_limit: "100 MB",
		disableDuringUpload: true,
		button_action: SWFUpload.BUTTON_ACTION.SELECT_FILE,
		
		// Button Settings
		button_placeholder_id : "placeholderVid",
		button_text_style : '',
		button_text_top_padding: 6,
		button_text_left_padding: 7,
		button_window_mode: SWFUpload.WINDOW_MODE.TRANSPARENT,
		button_cursor: SWFUpload.CURSOR.HAND,
		
		file_queued_handler: vidfileQueued,
		file_queue_error_handler : fileQueueError,
		upload_progress_handler : viduploadProgress,
		upload_success_handler : vidsuccess,
		upload_complete_handler : vidsuccess,
		
		// Debug Settings
		debug: false	});

	swfuPhotos = new SWFUpload({
			// Backend Settings

			// File Upload Settings
			file_upload_limit : 0,

			upload_url: "/upload/photo.php?s=" + session_id,
			flash_url: '/upload/swfupload.swf',
			flash9_url: '/upload/swfupload_fp9.swf',
			button_image_url: '/images/btnuploadimages.png',
			button_text: '',
			button_width: 105,
			button_height: 30,
			file_size_limit: "7 MB",
			disableDuringUpload: true,
			
			// Button Settings
			button_placeholder_id : "placeholderPhotos",
			button_text_style : '',
			button_text_top_padding: 0,
			button_text_left_padding: 0,
			button_window_mode: SWFUpload.WINDOW_MODE.TRANSPARENT,
			button_cursor: SWFUpload.CURSOR.HAND,
			file_types: "*.jpg;*.jpeg;*.png;*.gif",
			file_types_description: "Images",

			//file_queued_handler: fileQueuedPh,
			file_queue_error_handler : fileQueueError,
			//upload_start_handler : showProgressBarPh,
			file_dialog_complete_handler : phReadyForUpload,
			upload_progress_handler : phUploadProgress,
			//upload_error_handler : uploadError,
			upload_success_handler : phUploadSuccess,
			upload_complete_handler : phUploadComplete,

			// Debug Settings
			debug: false });

	if (typeof page == "undefined")
		document.getElementById("tbtitle").innerHTML = "<?=$site == "m" ? "Tweet something from $siteName" : "Add something to my log book"?> &#0133;";
	else
		document.getElementById("tbtitle").innerHTML = "Add something to the page log book &#0133;";

	resetUpload(true);

	e.value = "";
	e = document.getElementById("txtstatus");
	txtStatusChanged(e);
	e.value = defaultTxtStatus;

<?
if( isset( $_SESSION['fbpages'] ) && sizeof( $_SESSION['fbpages'] ) > 0 )
{
?>
	e = document.getElementById("postas");

  if( e )
  {
    html =  'Posting as <span id="targetname">';
<?
    if( empty( $_SESSION['fbpages']['selection'] ) || $_SESSION['fbpages']['selection'] == '0' )
    {
      echo "html += '" . $API->name . "';";
    }
    else
    {
      $found = false;
      for( $i = 0; $i < sizeof( $_SESSION['fbpages'] ); $i++ )
        if( $_SESSION['fbpages']['selection'] == $_SESSION['fbpages'][$i]['page_id'] )
        {
          echo "html += '" . $_SESSION['fbpages'][ $i ]['name'] . "';";
          $found = true;
          break;
        }
      if( !$found  )
      {
        echo "html += '" . $API->name . "';";
      }
    }
?>
    html += ' (<a href="javascript:void(0);" onclick="javascript:document.getElementById(\'targetname\').style.display=\'none\'; document.getElementById(\'targetselection\').style.display=\'\';">Change</a>)</span> ';
    html += '<span style="display:none;" id="targetselection">'
    html += '<select id="target_id" name="target_id" style="width:95px;">';
    html += '<option value="0"><? echo $API->name; ?></option>';
<? for( $i = 0; $i <  sizeof( $_SESSION['fbpages'] ); $i++ ) { if($_SESSION['fbpages'][$i]['page_id'] == "" ) continue; ?>
    html += '<option value="<? echo $_SESSION['fbpages'][$i]['page_id'] ?>"<? if( $_SESSION['fbpages']['selection'] == $_SESSION['fbpages'][$i]['page_id'] ) echo " SELECTED"; ?>><? echo $_SESSION['fbpages'][$i]['name']; ?></option>';
<? } ?>
    html += '</select></span>';
    e.innerHTML = html;
  }
<?
}
?>
}

var phUploaded = [];
var phQueueTotal;
var phUploadingNo;

function savePh()
{
	ttitle = document.forms.phform.phtitle.value;	
	desc = document.forms.phform.phdesc.value;
	
	if (ttitle == "" || desc == "")
	{
		alert("You must enter both a title and a description for the album.");
	}
	else
	{
		document.getElementById("phcontinue").style.display = "none";
		document.getElementById("phwait").style.display = "";
		
		photos = "";

		for (var k in phUploaded)
			if (phUploaded[k] == 1)
				photos += "," + k.substring(2);


    //For posting to FB pages
    e = document.getElementById("target_id");
    fbtarget = 0;
    if( e )
      fbtarget = e.value;

    e = document.getElementById("postfb");
    postfb = 0;
    if( e )
      postfb = (e.checked ? 1 : 0);

    e = document.getElementById("posttw");
    posttw = 0;
    if( e )
      posttw = (e.checked ? 1 : 0);

		post = "gid=" + currentPage + "&cat=" + catSelected + "&target=" + tbTarget + "&fbtarget=" + fbtarget + "&ids=" + photos.substring(1) + "&m=" + phMainImage[0] + "&title=" + escape(ttitle) + "&descr=" + escape(desc) + "&postfb=" + postfb + "&posttw=" + posttw;

		postAjax("/upload/newalbum.php", post, "savePhHandler");
	}
}

function saveVid()
{
	ttitle = document.forms.vidform.vidtitle.value;
	desc = document.forms.vidform.viddesc.value;

	if (ttitle == "" || desc == "")
	{
		alert("You must enter both a title and a description for the video.");
	}
	else
	{
		document.getElementById("vidcontinue").style.display = "none";
		document.getElementById("vidwait").style.display = "";

    //For posting to FB pages
    e = document.getElementById("target_id");
    fbtarget = 0;
    if( e )
      fbtarget = e.value;

    e = document.getElementById("postfb");
    postfb = 0;
    if( e )
      postfb = (e.checked ? 1 : 0);

    e = document.getElementById("posttw");
    posttw = 0;
    if( e )
      posttw = (e.checked ? 1 : 0);

		post = "gid=" + currentPage + "&cat=" + catSelected + "&target=" + tbTarget + "&fbtarget=" + fbtarget + "&title=" + escape(ttitle) + "&descr=" + escape(desc) + "&id=" + vidnewid + "&postfb=" + postfb + "&posttw=" + posttw;

		postAjax("/upload/savevid.php", post, "saveVidHandler");
	}
}

function saveYTVid()
{
	if (ytId == 0) //step 1 get video info
	{
		url = document.forms.vidytform.vidyturl.value;
		
		if (url == "")
		{
			alert("You must enter a valid YouTube address.");
		}
		else
		{
			document.getElementById("vidytwait").style.display = "";
			document.getElementById("btnsaveyt").style.display = "none";

			postAjax("/upload/saveytvid.php", "gid=" + currentPage + "&target=" + tbTarget + "&url=" + escape(url), "saveYTVidHandler");
		}
	}
	else //step 2 update vid info
	{
		{
			ttitle = document.forms.vidytform.vidyttitle.value;
			desc = document.forms.vidytform.vidytdesc.value;

			if (ttitle == "" || desc == "")
				return alert("You must enter both a title and a description for the video.");

			document.getElementById("vidytwait").style.display = "";
			document.getElementById("btnsaveyt").style.display = "none";

      //For posting to FB pages
      e = document.getElementById("target_id");
      fbtarget = 0;
      if( e )
        fbtarget = e.value;

      e = document.getElementById("postfb");
      postfb = 0;
      if( e )
        postfb = (e.checked ? 1 : 0);

      e = document.getElementById("posttw");
      posttw = 0;
      if( e )
        posttw = (e.checked ? 1 : 0);

			postAjax("/settings/updatemedia.php", "cat=" + catSelected + "&target=" + tbTarget + "&fbtarget=" + fbtarget + "&isyt=1&type=V&id=" + ytId + "&title=" + escape(ttitle) + "&descr=" + escape(desc) + "&postfb=" + postfb + "&posttw=" + posttw, "updateYTVidHandler");
		}
	}
}

function updateYTVidHandler(data)
{
  resetUpload(true);

  if (site != "m")
  {
    loadShare("V", 1, ytId, currentPage);
  }
  else
  {
    if( data == "OK" )
    	document.location = ytLocation;
    else
      document.location = "/facebook_error.php";
  }
}
var ytId = 0;
var ytLocation = "";

function saveYTVidHandler(data)
{
	uploadingType = "y";

	document.getElementById("btnsaveyt").style.display = "";
	document.getElementById("btnsaveyt").blur();
	document.getElementById("vidytwait").style.display = "none";

	x = data.split(String.fromCharCode(1));

	if (x[0] == "OK")
	{
		document.getElementById("vidyterror").style.display = "none";
		//document.getElementById("vidyterror").style.color = "#008000";
		//document.getElementById("vidyterror").innerHTML = "Your video has been added to mediaBirdy.";

		document.getElementById("vidytaddress").style.display = "none";
		document.getElementById("vidytinput").style.display = "";

		if (site == "m")
			document.getElementById("vidytcategory").style.display = "none";

		document.getElementById("vidytdesc_container").style.display = "";

		document.forms.vidytform.vidyttitle.value = x[3];
		document.forms.vidytform.vidytdesc.value = x[4];

		ytId = x[1];
		ytLocation = x[2];
	}
  else if( x[0] == "FB" )
  {
    document.location = "/facebook_error.php";
  }
	else
	{
		document.getElementById("vidyterror").style.display = "";
		document.getElementById("vidyterror").innerHTML = data;
	}
}

function saveVidHandler(data)
{
  resetUpload(true);

	x = data.split(":");
	
	if (x[0] == "OK")
	{
		if (site == "m")
			window.location.href = '/video.php?id=' + vidnewid;
		else
		{
			//tagQueue = [vidnewid];
			//showTagPopUp("V", tagQueue[0], x[1]);
			loadShare("V", 1, vidnewid, currentPage);
		}
	}
	else if( x[0] == "FB" )
  {
    document.location = "/facebook_error.php";
  }
  else if (admin == 1)
		alert("Error: " + data);
	else
		alert("An unknown error has occurred.");
}

function savePhHandler(data)
{
  resetUpload(true);

	x = data.split(":");

	if (x[0] == "OK")
	{
		if (site == "m")
			window.location.href = '/photo.php?id=' + phMainImage[0];
		else
		{
			tagQueue = x[2].split(",");
			showTagPopUp("P", tagQueue[0], x[3]);
		}
	}
	else if( x[0] == "FB" )
  {
    document.location = "/facebook_error.php";
  }
  else if (admin == 1)
		alert("Error: " + data);
	else
		alert("An unknown error has occurred.");
}

var numPhotosUploaded = 0;

function phUploadSuccess(obj, data, resp)
{
	info = data.split(":");

//  alert( info );

	if (info.length == 1)
		alert("ERROR:  " + info[0]);
	else
	{
		if (phMainImage[0] == 0)
		{
			changeMainImage(info[0], info[1], 0);
			document.getElementById("phuploadedcontainer").style.display = "";
			document.getElementById("phmainimagetitle").style.visibility = "visible";
		}
		
		phUploaded['id' + info[0]] = 1;
		
		newdiv = document.createElement("div");
		newdiv.innerHTML = getPhotoHTML(info);
		
		document.getElementById("phuploadedcontainer").appendChild(newdiv);
		numPhotosUploaded++;
	}
}

function phUploadComplete(obj)
{
	phUploadingNo++;

	stats = swfuPhotos.getStats();
	if (stats.files_queued > 0)
		swfuPhotos.startUpload();
	else
	{
		document.getElementById("phcontinue").style.display = "";
		document.getElementById("phbrowse").style.height = "";
		document.getElementById("phprogressbarcontainer").style.display = "none";
	}
}
	
function phUploadProgress(obj, done, total)
{
	document.getElementById("phprogressbar").style.width = Math.round(done / total * 175) + "px";
	percent = Math.round(1000 * done / total) / 10;
	document.getElementById("divuploadstatusph").innerHTML = "Uploading " + phUploadingNo + " of " + phQueueTotal + " (" + percent + "%)";
}

function doneCategorySelected()
{
	document.getElementById("upload-category").style.display = "none";

	if (uploadingType == "v")
		document.getElementById("vidinfo").style.display = "";
	else if (uploadingType == "p")
		document.getElementById("phinfo").style.display = "";
	else if (uploadingType == "y")
		document.getElementById("vidytinput").style.display = "";
}

function uploadCategorySelected(cat, catname)
{
	catSelected = cat;
	
	if (uploadingType == "v")
		document.getElementById("vidcategoryselected").innerHTML = catname;
	else if (uploadingType == "p")
		document.getElementById("phcategoryselected").innerHTML = catname;
	else if (uploadingType == "y")
		document.getElementById("vidytcategoryselected").innerHTML = catname;
	
	document.getElementById("uploadcat-" + cat).checked = true;
}

function showCategorySelect()
{
	e = document.getElementById("upload-category");

	e.innerHTML = uploadCategoryHTML;
	e.style.display = "";

	document.getElementById("phinfo").style.display = "none";
	document.getElementById("vidinfo").style.display = "none";
	document.getElementById("vidytinput").style.display = "none";
}
	
function phReadyForUpload(a, b, c)
{
	uploadingType = "p";

	stats = swfuPhotos.getStats();
	
	if (stats.files_queued > 0)
	{
		phQueueTotal = stats.files_queued;
		phUploadingNo = 1;
		document.getElementById("phbrowse").style.height = "0px";
		document.getElementById("phprogressbarcontainer").style.display = "";
		if (site == "m")
			document.getElementById("phcategory").style.display = "none";
		document.getElementById("phinfo").style.display = "";
		document.getElementById("phdesc_container").style.display = "";
		document.getElementById("phdisclaimer").style.display = "none";
		swfuPhotos.startUpload();
	}
}

function fileQueueError(obj, x, msg)
{
	alert(obj.name + ":  " + msg);
}

var vidnewid;
function vidsuccess(obj, data)
{
	//alert(data);
	if (typeof data !== "undefined")
	{
		document.getElementById("vidfile").style.display = "none";
		document.getElementById("vidbrowse").style.display = "none";
		document.getElementById("vidsuccess").style.display = "";

		x = data.split(":");
		if (x.length == 1)
		{
			document.getElementById("vidinfo").style.display = "none";
			e = document.getElementById("viderror");
			e.innerHTML = x[0];
			e.style.display = "";
		}
		else
		{
			vidnewid = x[0];
			//alert("SUCCESS:  " + data);
			//document.getElementById("vidthumb").style.display = "";
			//document.getElementById("vidthumb").innerHTML = "<img src='" + x[1] + "' alt='' />";
			document.getElementById("vidcontinue").style.display = "";
		}
	}
}

function vidfileQueued(obj)
{
	uploadingType = "v";

	document.getElementById("vidfile_file").innerHTML = obj.name;
	document.forms.vidform.vidtitle.value = obj.name;
	document.getElementById("vidprogressbarcontainer").style.display = "";
	document.getElementById("vidinfo").style.display = "";
	if (site == "m")
		document.getElementById("vidcategory").style.display = "none";
	document.getElementById("viddesc_container").style.display = "";
	document.getElementById("viddisclaimer").style.display = "none";
	vidswfu.startUpload();
}

function viduploadProgress(obj, done, total)
{
	document.getElementById("vidprogressbar").style.width = Math.round(done / total * 300) + "px";
	percent = Math.round(1000 * done / total) / 10;
}

function resetUpload(showPlaceholder, resetStatus)
{
	if (typeof resetStatus == "undefined")
		resetStatus = false;

	catSelected = 0;

  if( document.getElementById("btnprocess") == null )
  {
//    location.reload();
    return;
  }

	document.getElementById("btnprocess").style.display = "";
	document.getElementById("upload-category").style.display = "none";
	document.getElementById("mediaselection").style.display = "none";
	document.getElementById("phadd").style.display = "none";
	document.getElementById("phchoices").style.display = "none";
	document.getElementById("phcategory").style.display = "";
	document.getElementById("phcategoryselected").innerHTML = "Select a category";
	document.getElementById("phcomputer").style.display = "none";
	document.getElementById("phdisclaimer").style.display = "";
	document.getElementById("phuploadedcontainer").style.display = "none";
	document.getElementById("phmainimagetitle").style.visibility = "hidden";
	document.getElementById("phcontinue").style.display = "none";
	document.getElementById("phwait").style.display = "none";
	document.getElementById("phbrowse").style.height = "";
	document.getElementById("phprogressbarcontainer").style.display = "none";
	document.getElementById("phprogressbar").style.width = "0px";
	document.getElementById("phinfo").style.display = "none";
	document.getElementById("phuploaded").innerHTML = '<div class="phnewparent" id="phnewparent-0"></div>';
	document.getElementById("phuploadedcontainer").innerHTML = '<div style="font-weight: bold; margin-bottom: 2px;">Manage Your Images</div><div style="font-size: 8pt;">Set your main image by selecting a photo, or delete an image by placing your cursor on it and selecting the &quot;X&quot;.</div>					<div id="phuploaded" style="margin-top: 5px; width:300px;"></div><div style="clear: both;"></div>';
	document.getElementById("mainimage-0").src = "/images/albumempty.png";
	document.forms.phform.phtitle.value = "";
	document.forms.phform.phdesc.value = "";


	phMainImage[0] = 0;
	phUploaded = new Array();
	numPhotosUploaded = 0;

	document.getElementById("vidadd").style.display = "none";
	document.getElementById("vidchoices").style.display = "none";
	document.getElementById("vidcomputer").style.display = "none";
	document.getElementById("vidyt").style.display = "none";
	document.getElementById("vidcategory").style.display = "";
	document.getElementById("vidcategoryselected").innerHTML = "Select a category";
	document.getElementById("vidbrowse").style.display = "";
	document.getElementById("vidfile").style.display = "";
	document.getElementById("viddisclaimer").style.display = "";
	document.getElementById("vidsuccess").style.display = "none";
	document.getElementById("vidthumb").style.display = "none";
	document.getElementById("viderror").style.display = "none";
	document.getElementById("vidinfo").style.display = "none";
	document.getElementById("vidcontinue").style.display = "none";
	document.getElementById("vidwait").style.display = "none";
	document.getElementById("vidprogressbar").style.width = "0";
	document.getElementById("vidprogressbarcontainer").style.display = "none";
	document.getElementById("vidfile_file").innerHTML = "";

	document.forms.vidform.vidtitle.value = "";
	document.forms.vidform.viddesc.value = "";

	document.forms.vidytform.vidyturl.value = "";
  document.forms.vidytform.vidyttitle.value = '';
	document.forms.vidytform.vidytdesc.value = '';

	document.getElementById("vidytaddress").style.display = "";
	document.getElementById("vidytinput").style.display = "none";
  document.getElementById("btnsaveyt").style.display = "";
  document.getElementById("vidytwait").style.display = "none";

	document.getElementById("vidytcategory").style.display = "";
	document.getElementById("vidytcategoryselected").innerHTML = "Select a category";
	
	document.getElementById("logbubble").style.display = "none";
	
	e = document.getElementById("txtstatus");

	if (e.value == "" || resetStatus)
	{
		e.value = "";
		txtStatusChanged(e);
		e.value = defaultTxtStatus;
	}

	document.getElementById("tbstatus").style.display = "";
	document.getElementById("btnprocess").style.display = "";
	document.getElementById("wpcharsremain").style.display = "";
}

function showUpload(showDivs)
{
	resetUpload(false);

	if (document.getElementById("txtstatus").value == defaultTxtStatus)
		document.getElementById("txtstatus").value = "";
	
	document.getElementById("btnprocess").style.display = "none";
	document.getElementById("tbstatus").style.display = "none";
	document.getElementById("wpcharsremain").style.display = "none";

	document.getElementById("logbubble").style.display = "";
	
	for (i = 0; i < showDivs.length; i++)
	{
		document.getElementById(showDivs[i]).style.display = "";
		
		if (showDivs[i] == "phadd")
			document.getElementById("logbubble").style.backgroundPosition = "217px 0";
		else if (showDivs[i] == "vidadd")
			document.getElementById("logbubble").style.backgroundPosition = "136px 0";
	}
}

function showPhonePopup()
{
	window.open("/email.php?1", "Phone", "location=0,status=0,scrollbars=1,width=591,height=700");
}

function showTOSPopUp()
{
window.open('/tos.php?1', 'TOS','location=0,status=0,scrollbars=1,width=500,height=600');
}

function showPrivacyPopUp()
{
window.open('/privacy.php?1', 'Privacy','location=0,status=0,scrollbars=1,width=500,height=600');
}
