<?php
/*
Implements the processing of newly uploaded videos.  This is performed asynchronously to the user's site browsing.
Once complete, the video is marked as ready, making it available to the site.
*/
/*
$tmp = explode("/", $argv[0]);
array_pop($tmp);
$cwd = implode("/", $tmp); //get the path of the upload folder
$_SERVER['DOCUMENT_ROOT'] = $cwd . "/..";
*/
$_SERVER['DOCUMENT_ROOT'] = "/var/www/salthub.com/public_html";

//so we get the right db
$site = $argv[9];
include $_SERVER['DOCUMENT_ROOT'] . "/inc/inc.php";
include_once $_SERVER['DOCUMENT_ROOT'] . "/inc/createThumb.php";

$_SERVER['HTTP_HOST'] = SERVER_HOST;

$id = $argv[1];
$hash = $argv[2];
$width = $argv[3];
$height = $argv[4];
$dur = $argv[5];
$ext = $argv[6];
$framerate = intval($argv[7]);
$API->uid = intval($argv[8]);
$site = $argv[9];
$uidTarget = intval($argv[10]);

if ($framerate == 0)
	$framerate = 15;
elseif ($framerate > 30)
	$framerate = 30;

$source = "/tmp/vidupload-" . $hash;

//generate thumbnail
$newDir = "/tmp";

$cmd = "/usr/local/bin/ffmpeg -y -i $source -f image2 -vframes 1 -ss " . floor($dur / 2) . " $newDir/00000001.jpg";
exec($cmd);

if (!file_exists("$newDir/00000001.jpg"))
	exec("mplayer $source -ss 0 -frames 1 -vo jpeg:outdir=$newDir");

if (file_exists("$newDir/00000001.jpg"))
{
	rename("$newDir/00000001.jpg", "$newDir/$hash.jpg");
	chmod("$newDir/$hash.jpg", 0777);
}
else
{
	unlink($source);
	die("INVALID_VIDEO");
}
//end generate thumbnail

$target = "/tmp/v$hash";

$audBR = 128;
$vidBR = 768;
$audFreq = 44100;

//max dimensions are 514x388
$maxWidth = 514;
$maxHeight = 388;

if ($width > $maxWidth || $height > $maxHeight) //video is too big, we will shrink
{
	$scale = true;
	
	$ratio = $width / $height;
	
	if ($ratio > 1) //wider than it is tall
	{
		if ($width > $maxWidth)
			$newWidth = $maxWidth;
		else
			$newWidth = $width;

		$newHeight = floor($newWidth / $ratio);
	}
	else //taller than wide or square
	{
		if ($height > $maxHeight)
			$newHeight = $maxHeight;
		else
			$newHeight = $height;
		
		$newWidth = floor($newHeight * $ratio);
	}
	
	//dimensions must be multiples of 2
	$newWidth = (ceil($newWidth / 2) * 2);
	$newHeight = (ceil($newHeight / 2) * 2);
}
else //don't resize
{
	$scale = false;
	$newWidth = $width;
	$newHeight = $height;
}

$format = 1; //Indicates mp4 (h264) video format
$outext = "mp4";
$outext2 = "flv";
$outext3 = "ogv";
$l = fopen("/tmp/enc", "w");


$encodecommand = "export LD_LIBRARY_PATH=/usr/local/lib; /usr/local/bin/ffmpeg -i $source " . ($scale ? " -s $newWidth" . "x$newHeight" : "") . " $target.$outext";
fwrite($l, $encodecommand . "\n\n");
exec($encodecommand);

//  $encodecommand = "/usr/bin/ffmpeg -i $source" . ($scale ? " -s $newWidth" . "x$newHeight" : "") . " $target.$outext2";
$encodecommand = "export LD_LIBRARY_PATH=/usr/local/lib; /usr/local/bin/ffmpeg -i $source" . ($scale ? " -s $newWidth" . "x$newHeight" : "") . " -ar $audFreq -y $target.$outext2";
fwrite($l, $encodecommand . "\n\n");
exec($encodecommand);

$encodecommand = "export LD_LIBRARY_PATH=/usr/local/lib; /usr/local/bin/ffmpeg -i $source -b 1500k -vcodec libtheora -acodec libvorbis -ab 160000 -g 30 \"$target.$outext3\"";
fwrite($l, $encodecommand . "\n\n");
exec($encodecommand);


if (filesize("$target.$outext") == 0)
{

  $outext = "flv";
  $format = 0; //We had to fall back to .flv format!
	$encodecommand = "/usr/bin/mencoder $source -o $target.flv -ofps $framerate -of lavf -oac mp3lame" . ($scale ? " -vf scale=$newWidth:$newHeight" : "") . " -lameopts abr:br=$audBR -ovc lavc -lavcopts vcodec=flv:vbitrate=$vidBR:mbd=2:mv0:trell:v4mv:cbp:last_pred=3 -srate $audFreq";
	//$encodecommand = "mencoder $source -quiet -ofps $framerate -sws 9 -of lavf -lavfopts format=ipod" . ($scale ? " -vf scale=$newWidth:$newHeight" : "") . " -ovc lavc -lavcopts aglobal=1:vglobal=1:vcodec=mpeg4:vbitrate=$vidBR:acodec=libfaac:abitrate=$audBR -oac lavc -alang en -srate $audFreq -o $target";
	//$encodecommand = "mencoder $source -quiet -ofps $framerate -sws 9 -of lavf -lavfopts format=ipod" . ($scale ? " -vf scale=$newWidth:$newHeight" : "") . " -ovc x264 -x264encopts bitrate=$vidBR:vbv_maxrate=" . ($vidBR * 2) . ":vbv_bufsize=2000:nocabac:me=hex:subq=4:frameref=2:trellis=1:level_idc=30:global_header:threads=auto -oac faac -alang en -faacopts mpeg=4:object=2:br=$audBR:raw -channels 2 -srate $audFreq -o $target";

	fwrite($l, $encodecommand . "\n\n");
	exec($encodecommand);
}

// $encodecommand = "/usr/local/bin/ffmpeg -y -i $target.flv -acodec libfaac -ar 22050 -ab 96k -ac 2 -vcodec libx264 -flags +loop+mv4 -cmp 256 -partitions +parti4x4+partp8x8+partb8x8 -subq 7 -trellis 1 -refs 5 -coder 0 -me_range 16 -keyint_min 25 -sc_threshold 40 -i_qfactor 0.71 -bt 400k -b 400k -maxrate 400k -bufsize 1200k -rc_eq 'blurCplx^(1-qComp)' -qcomp 0.6 -qmin 10 -qmax 51 -qdiff 4 -level 30 -r 30 -g 90 -async 2 $target.mp4";
// fwrite($l, "Mobile: " . $encodecommand . "\n\n");
// exec($encodecommand);

fclose($l);

unlink($source);

if (filesize("$target.$outext") == 0)
{
	mysql_query("delete from videos where id=$id");
	die();
}


chmod("$target.$outext", 0777);

if( $format == 1 )
{
  chmod("$target.$outext2", 0777);
  chmod("$target.$outext3", 0777);
}

$jmp = shortenMediaURL("V", $id);



//Upload video to cloud

$thumb = "$newDir/$hash.jpg";
$video = "$target.$outext";

$temp_thumb = "/tmp/" . mysql_thread_id();
$file = createThumb( 119, 95, true, false, $id, $hash, $temp_thumb );

try {
  $API->uploadToCloud( $API->uid, $file, $hash . ".jpg" );
  $API->uploadToCloud( $API->uid, $video, $hash . ".$outext" );

  if( $format == 1 )
  {
    $API->uploadToCloud( $API->uid, "$target.$outext2", $hash . ".$outext2" );
    $API->uploadToCloud( $API->uid, "$target.$outext3", $hash . ".$outext3" );
  }
}
catch( Exception $e )
{
}

//The mysql server may drop due to the time it takes to upload a file and process the video, reestablish the connection is we need to.
$success = false;
while( !$success )
{
  mysql_query("update videos set format=$format,privacy=0, ready=1,jmp='$jmp',width='$newWidth',height='$newHeight' where id=$id");
  if( mysql_error() )
  {
    mysql_close();
    include $_SERVER['DOCUMENT_ROOT'] . "/inc/sql.php";
  }
  else
  {
    $success = true;
  }
}

unlink( $temp_thumb );
unlink( $thumb );
unlink( $target . "." . $outext );
unlink( $target . "." . $outext2 );
unlink( $target . "." . $outext3 );

?>
