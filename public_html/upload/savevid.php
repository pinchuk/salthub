<?php
/*
Called in response to a new video added to the site.  Adds a new video to the database,
sends notifications and adds news feeds.
*/

include "../inc/inc.php";

if (!$API->isLoggedIn())
	die("BAD_SESSION");

$err = "OK";

$id = intval($_POST['id']);
$gid = intval($_POST['gid']);

if ($id == 0) die("No ID");
$title = str_replace("\n", "", $_POST['title']);
//$descr = str_replace("\n", "", $_POST['descr']);
$descr = $API->convertChars($_POST['descr']);
$cat = intval($_POST['cat']);

if (empty($title) || empty($descr)) die("No title or description");

//update title and descr
mysql_query("update videos set lastviewed=Now(), title='$title',descr='$descr'" . ($site == "s" ? ",cat=$cat" : "") . " where id=$id and uid=" . $API->uid);

echo mysql_error();

//get video info needed for notification
$x = mysql_query("select id,title,descr,uid,jmp,hash from videos where id=$id");
$info = mysql_fetch_array($x, MYSQL_ASSOC);

//   Always send out the notifications here; previous it could have sent the notifications from compress.php too.
//if ($info['jmp']) //video has already been compressed, ready to notify the social sites
{
	$info['type'] = "V";

  $fbtarget = null;
  if( isset( $_POST['fbtarget'] ) )
  {
    $fbtarget = $_POST['fbtarget'];
    $_SESSION['fbpages']['selection'] = $fbtarget;
    if( $fbtarget == 0 )
      $fbtarget = null;
  }

  $info['fbtarget'] = $fbtarget;
  $info['gid'] = $gid;

	$err = $API->sendNotification(NOTIFY_VIDEO_UPLOADED, $info);
//	mysql_query("update videos set ready=1 where id=$id");

	//add the feed
	$target = intval($_POST['target']);
	if ($gid == 0)
	{
		if ($target == 0)
			$target = $API->uid;
	}
	else //make the target 0 if it's a page - we aren't posting to another user's log
		$target = 0;

  $to = $target;
  $from = $API->uid;
  if( $gid > 0 && $site == "s" )
  {
    $postAsPage = quickQuery( "select postAsPage from page_members where admin=1 and gid=$gid and uid=" . $API->uid );

    if( $postAsPage == 1 )
    {
      $to = -1;
      $from = -1;
    }
  }

	$API->feedAdd("V", $id, $to, $from, $gid);
    $isAdmin = quickQuery("select admin from page_members where gid = $gid and uid = ".$API->uid);
	if ($gid > 0 && $isAdmin == 1 || $API->isAdmin()){
		mysql_query("insert into page_media (type,id,gid) values ('V',$id,$gid)");
        mysql_query("update videos set on_page = 0 where id = $id and uid = ".$API->uid);
    }
}

echo $err . ":" . $API->getThumbURL(0, 250, 258, "/videos/$id/" . $info['hash'] . ".jpg");

?>