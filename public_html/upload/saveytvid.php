<?php
/*
Called in response to a new youtube video added to the site.  Adds a new video to the database,
sends notifications and adds news feeds.
*/

$badURL = "You have entered an invalid address.&nbsp; Please try again with a valid address.";

include "../inc/inc.php";
include "../inc/createThumb.php";

$err = "OK";

if (!$API->isLoggedIn())
	die("BAD_SESSION");

$url = $_REQUEST['url'];
$gid = intval($_POST['gid']);

if (empty($url)) die($badURL);

$i = strpos($url, "/watch?v=");

if (!$i)
	$i = strpos($url, 'youtu.be/');

if (!$i)
	die($badURL);

$url = substr($url, $i + 9);
$i = strpos($url, "&");

if ($i)
	$url = substr($url, 0, $i);

$file = "http://gdata.youtube.com/feeds/api/videos/$url";

function contents($parser, $data)
{
	//echo "CONTENTS $data<br/>";
	global $tag, $vid;
	if ($tag == "TITLE")
		$vid['title'] = $data;
	elseif ($tag == "CONTENT")
		$vid['descr'] .= $data;
}

function startTag($parser, $data, $attribs)
{
	global $tag, $vid;
	//echo "START $data<br/>";
	if ($attribs['DURATION'])
		$vid['dur'] = intval($attribs['DURATION']);
	$tag = $data;
}

function endTag($x, $y) {}

$xml_parser = xml_parser_create();
xml_set_element_handler($xml_parser, "startTag", "endTag");
xml_set_character_data_handler($xml_parser, "contents");

$data = implode("", file($file));

if (strpos($data, "<yt:noembed")) die("Sorry, embedding has been disabled for this video.");

if (!(xml_parse($xml_parser, $data)))
    die($badURL);

xml_parser_free($xml_parser); 

$title = $vid['title'];
$descr = $vid['descr'];

$vid['title'] = addslashes(str_replace("\n", "", $vid['title']));
//$vid['descr'] = addslashes(str_replace("\n", "", $vid['descr']));
$vid['descr'] = addslashes($API->convertChars($vid['descr']));

$isAdmin = quickQuery("select admin from page_members where gid = $gid and uid = ".$API->uid);
$postAsPage = quickQuery( "select postAsPage from page_members where admin=1 and gid=$gid and uid=" . $API->uid );
if ($gid > 0){
    mysql_query("insert into videos (on_page, privacy,ready,title,descr,uid,duration,width,height,hash) values (".($postAsPage==1 ? $gid : 0).", " . PRIVACY_EVERYONE . ",1,'" . $vid['title'] . "','" . $vid['descr'] . "'," . $API->uid . "," . $vid['dur'] . ",1,1,'" . addslashes($url) . "')");
}
else{
    mysql_query("insert into videos (privacy,ready,title,descr,uid,duration,width,height,hash) values (" . PRIVACY_EVERYONE . ",1,'" . $vid['title'] . "','" . $vid['descr'] . "'," . $API->uid . "," . $vid['dur'] . ",1,1,'" . addslashes($url) . "')");
}
$id = mysql_insert_id();

$vid['jmp'] = shortenMediaURL("V", $id);
mysql_query("update videos set lastviewed=Now(), jmp='" . $vid['jmp'] . "' where id=$id");

$vid['type'] = "V";
$vid['id'] = $id;

$target = intval($_POST['target']);

if ($gid == 0)
{
  if ($target == 0)
  	$target = $API->uid;
}
else //make the target 0 if it's a page - we aren't posting to another user's log
  $target = 0;

$to =$target > 0 ? $target : $API->uid;
$from = $API->uid;

  if( $gid > 0 && $site == "s" )
  {
    $postAsPage = quickQuery( "select postAsPage from page_members where admin=1 and gid=$gid and uid=" . $API->uid );

    if( $postAsPage == 1 )
    {
      $to = -1;
      $from = -1;
    }
  }

//$API->feedAdd("V", $id, $to, $from, $gid);
$isAdmin = quickQuery("select admin from page_members where gid = $gid and uid = ".$API->uid);
	if ($gid > 0 && $site == "s" && $isAdmin == 1 || $API->isAdmin()){
		mysql_query("insert into page_media (type,id,gid) values ('V',$id,$gid)");
//        mysql_query("update videos set on_page = 0 where id = $id and uid = ".$API->uid);
    }
echo implode(chr(1), array("OK", $id, $API->getMediaURL("V", $id, $title), substr($title, 0, 50), substr($descr, 0, 255)));

$hash = $url;
$temp_thumb = "/tmp/" . mysql_thread_id();

$file = createThumb( 119, 95, true, false, $id, $hash, $temp_thumb );
$API->uploadToCloud( $API->uid, $file, $hash . ".jpg" );

unlink( $temp_thumb );
?>