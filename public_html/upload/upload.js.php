<?php
/*
Contains javascript used throughout the site to handle file uploads.
*/

header("Content-type: text/javascript");
include "../inc/inc.php";
?>var vidswfu;
var photos_done_processing = 0;
var swfuPhotos;
var catSelected = 0;
var uploadingType;
var currentPage = 0;
var defaultTxtStatus = "What's on your mind?";
var totalCharsAllowed = <?=115 - strlen(SITE_VIA)?>;

var uploadCategoryHTML = '<div style="width: 350px; margin: 0 auto;" class="smtitle2" style="color: #808080;"><div class="smtitle" style="text-align: center; padding-bottom: 15px;">Select a category</div><?php
if ($site == "s")
{
	$left = true;
	$x = mysql_query("select * from categories where cat != 2 and cattype='M' order by industry,catname");
	while ($cat = mysql_fetch_array($x, MYSQL_ASSOC))
	{
		echo '<div style="' . ($left ? '' : 'float: right; ') . 'width: ' . (150 - ($left ? 0 : 20)) . 'px;" class="catchoice">';
		echo '	<a class="smtitle2" onclick="javascript:uploadCategorySelected(' . $cat['cat'] . ', \\\'' . addslashes($cat['catname']) . '\\\');" href="javascript:void(0);"><input type="radio" id="uploadcat-' . $cat['cat'] . '" name="category" value="' . $cat['cat'] . '" />&nbsp;' . addslashes($cat['catname']) . '</a>';
		echo '</div>';

		$left = !$left;
	}
}
?><div style="clear: both; padding-top: 15px; text-align: center;"><input type="button" class="button" value="&nbsp; OK &nbsp;" onclick="javascript:doneCategorySelected();" /></div></div>';

function showTweetBox()
{
	if (document.getElementById("divtb1"))
	{
		showPopUp("", '<div id="tbpopup"></div>', 0, "no", 0, tbReplace);
		setTimeout('document.getElementById("tbpopup").appendChild(document.getElementById("divtb1"));', 100);
	}
	else
	{
		showPopUp("", tbHTML, 0, "no", 0);
		tbInitialize();
	}
}

function tbReplace()
{
	document.getElementById("divtb1container").appendChild(document.getElementById("divtb1"));
}

function txtStatusFocused(e, isFocused)
{
	if (isFocused)
	{
		if (e.value == defaultTxtStatus)
			e.value = "";
	}
	else
	{
		if (e.value == "")
			e.value = defaultTxtStatus;
	}
}

var disableWallPosts = false;

function slideUpLogLink()
{
	$('.loglink').slideUp(400,
		function ()
		{
			$('.loglink').removeClass('loglink_show');
			//$('.loglink .img_container img').remove();
			//$(e).attr('data-linked', 'no');
		}
	);
}

function showLogboxLinks()
{
	$('.loglink .next').click();
	$('.loglink').addClass('loglink_show').slideDown();

    //initSWFuploadToLoglink();

}

function txtStatusChanged(e,event, text)
{ 
/*
	l = e.value.length;

	charsleft = totalCharsAllowed - l;

	if (l >= totalCharsAllowed)
		e.value = e.value.substring(0, totalCharsAllowed);

	if (charsleft < 0)
		return false;

	document.getElementById("wpcharsremain").innerHTML = charsleft + " characters remaining";
*/

  FitToContent( "txtstatus", 150 );

  if( typeof event != "undefined" )
  {
    var charCode = event.keyCode || event.which;
  }

  document.getElementById("wpcharsremain").innerHTML = "";

	//detect links
	//if ($(e).attr('data-linked') != 'yes')
	{
		val = $(e).val().toLowerCase() + ' ';

        if(typeof text != 'undefined')
        {
            val = text+' ';
        }
		
		search = ['http://', 'https://', 'www'];
		
		for (i = 0, pos = -1; i < search.length && pos == -1; i++)
			pos = val.indexOf(search[i]);
		
		if (pos > -1)
		{
			i = val.indexOf(' ', pos);
			if (i > -1)
			{
				url = $(e).val().substring(pos, i);
				

				if (url != $(e).attr('data-link-url'))
				{
					$(e).attr('data-linked', 'yes');
					$(e).attr('data-link-url', url);
					$.get('/ajax/get_meta_tags.php?url=' + escape(url))
						.success(
							function (json)
/*
				$.get('/ajax/get_meta_tags.php?url=' + escape(url))
					.success(
						function (json)
						{
							if (json.title == '')
								json.title = url;
							
							if (json.descr == '')
								json.descr = url;
							
							$('.loglink .title').val(json.title).trigger('autosize');
							$('.loglink .ilink').html(url);
							$('.loglink .descr').val(json.descr).trigger('autosize');
							
							imagesLoaded = 0;
							
							log(json);
							
							for (i in json.images)
*/
							{
								if (json.response_code != '200')
								{
									$(e).attr('data-linked', 'no');
									return false;
								}
								
								/*if (json.title == '')
									json.title = 'Please enter a title';
								
								if (json.descr == '')
									json.descr = 'Please enter a description';*/
								
								$('.loglink .title').val(json.title).autosize().trigger('autosize.resize');
								$('.loglink .ilink').html(url);
								$('.loglink .descr').val(json.descr);//.autosize().trigger('autosize.resize');
								
								imagesLoaded = 0;
								
								//log(json);

								$('.loglink .img_container').html('');
								for (i in json.images)
								{
									img = $('<img>');
									
									$(img).attr('src', json.images[i])
									.load(
										function ()
										{
											if ($(this).width() < 75)
												$(this).remove();
											else
												$(this).addClass('loaded');
											
											imagesLoaded++;
											if (imagesLoaded == json.images.length)
												showLogboxLinks();
	}
									).error(
										function ()
										{
											$(this).remove();

											imagesLoaded++;
											if (imagesLoaded == json.images.length)
												showLogboxLinks();
										}
									).appendTo('.loglink .img_container');
								}
								
								$('.loglink input[type="button"]').unbind('click').click(
									function ()
									{
										if ($(this).hasClass('next'))
										{
											show = $('.loglink .img_container img:visible').next();
											if (show.length == 0)
												show = $('.loglink .img_container img:first');
										}
										else if ($(this).hasClass('prev'))
										{
											show = $('.loglink .img_container img:visible').prev();
											if (show.length == 0)
												show = $('.loglink .img_container img:last');
										}
										
										$('.loglink .img_container img').hide();
										$(show).css('display','inline');
	//									$(show).show();

										$('.loglink .totalimages').html(($('.loglink .img_container img').index($(show)) + 1) + ' of ' + $('.loglink .img_container img').length);
									}
								);
								
								//$(e).attr('data-linked', 'no');
							}

						);
				}
/*
							
							$('.loglink input[type="button"]').click(
								function ()
								{
									if ($(this).hasClass('next'))
									{
										show = $('.loglink .img_container img:visible').next();
										if (show.length == 0)
											show = $('.loglink .img_container img:first');
									}
									else if ($(this).hasClass('prev'))
									{
										show = $('.loglink .img_container img:visible').prev();
										if (show.length == 0)
											show = $('.loglink .img_container img:last');
									}
									
									$('.loglink .img_container img').hide();
									$(show).css('display','inline');
//									$(show).show();

									$('.loglink .totalimages').html(($('.loglink .img_container img').index($(show)) + 1) + ' of ' + $('.loglink .img_container img').length);
								}
							);
						}
					);
				
				$(e).attr('data-linked', 'yes');
*/
			}
		}
	}
}

function FitToContent(id, maxHeight)
{
  var text = id && id.style ? id : document.getElementById(id);
  if ( !text )
    return;

  if (text.clientHeight == text.scrollHeight) text.style.height = "20px";

  var adjustedHeight = text.clientHeight;
  if ( !maxHeight || maxHeight > adjustedHeight )
  {
    adjustedHeight = Math.max(text.scrollHeight, adjustedHeight);
    if ( maxHeight )
       adjustedHeight = Math.min(maxHeight, adjustedHeight);
    if ( adjustedHeight > text.clientHeight )
       text.style.height = adjustedHeight + "px";
  }
}

function wallPost()
{
    if( disableWallPosts )
    {
        return; //This is to prevent someone from pressing enter a bunch of times and getting many posts.
    }
	
	/*post = document.getElementById("txtstatus").value;

    post = post.replace(
        /(^http\:\/\/(.*))/g,
        ''
    );
    post = post.replace(
        /(^(www){0,1}([\w]+\.)+.*)/g,
        ''
    );*/
	
	post = escape($('#txtstatus').val());

    var loglinkLink = $("#iloglink_box .loglink_show").find('.ilink').html();
    
	var loglinkData = new Object();
	
    var iloglinkContent = $("#iloglink_box .loglink_show");
    if(iloglinkContent.length > 0)
    {
        loglinkData.loglink_descr = $("#iloglink_box .loglink_show").find('.descr').val();
        loglinkData.loglink_link = loglinkLink;
        loglinkData.loglink_title = $("#iloglink_box .loglink_show").find('.title').val();
        loglinkData.loglink_img = $("#iloglink_box .loglink_show .img_container [style*='inline']").attr('src');
    }

	if (post == defaultTxtStatus && iloglinkContent.length == 0)
		return;
	
	if (post == defaultTxtStatus)
		post = '';
/*
    );

    var loglinkLink = $("#iloglink_box .loglink_show").find('.ilink').html();
    loglinkLink = loglinkLink.replace(/^http\:\/\//g,'');
    var loglinkData = '';
    var iloglinkContent = $("#iloglink_box .loglink_show");
    if(iloglinkContent.length > 0)
    {
        loglinkData += '&loglink_descr=' + $("#iloglink_box .loglink_show").find('.descr').val();
        loglinkData += '&loglink_link=' + loglinkLink;
        loglinkData += '&loglink_title=' + $("#iloglink_box .loglink_show").find('.title').val();
        loglinkData += '&loglink_img=' + $("#iloglink_box .loglink_show .img_container [style*='inline']").attr('src');
    }

	if (post == defaultTxtStatus) return;
    */

  disableWallPosts = true;

  //For posting to FB pages
  e = document.getElementById("target_id");
  fbtarget = 0;
  if( e )
    fbtarget = e.value;

  e = document.getElementById("postfb");
  postfb = 0;
  if( e )
    postfb = (e.checked ? 1 : 0);


	loglinkData.gid = currentPage;
	loglinkData.target = tbTarget;
	loglinkData.fbtarget = fbtarget;
	loglinkData.post = post;
	
	//postAjax("/wallpost.php", "gid=" + currentPage + "&target=" + tbTarget + "&fbtarget=" + fbtarget + "&post=" + post + loglinkData, "wallPostHandler");
	
	log(loglinkData);
	
	$.ajax({
		type: 'POST',
		url: '/wallpost.php',
		data: loglinkData,
		success: wallPostHandler,
        complete: function(){setTimeout(function(){showNewLogEntries();}, 1000)}
	});
/*

	if (site == "m")
  {
		postAjax("/updatesocialstatus.php", "target=" + tbTarget + "&fbtarget=" + fbtarget + "&post=" + post, "updateStatusHandler");
  }
	else
  {
		postAjax("/wallpost.php", "gid=" + currentPage + "&target=" + tbTarget + "&fbtarget=" + fbtarget + "&post=" + post + loglinkData, "wallPostHandler");
  }
*/

	document.getElementById("txtstatus").value = defaultTxtStatus;
    $('.loglink').removeClass('loglink_show');
}

function updateStatusHandler(data)
{
	x = data.split(":");
	if (x[0] == "OK")
		location = x[1];
	else
	{
		if (admin == 1)
			alert("Error: " + data);
		else
			alert("An unknown error has occurred.  Please try again later.");
	}
}

function wallPostHandler(data)
{

	disableWallPosts = false;

	json = json_decode(data);
waitingForLogEntries = false;
	if (json.success)
	{
		if (typeof showNewLogEntries == "function")
		{
			//log(data.fid);
            disableWallPosts = false;
            waitingForLogEntries = false;

			//showNewLogEntries(json.fid);
            //showNewLogEntries();
			resetUpload(true);
		}
		else
			location.reload(true);
	}
	else if( data.error == "FB" ) //Facebook Error
  {
    document.location = "/facebook_error.php";
  }
	else
		alert("An unknown error has occurred.  Please try again later.");
}

function tbInitialize()
{
	browse_html = '<div style="height: 25px; line-height: 1em; overflow: hidden;"><input type="button" class="button" value="Browse &#0133;" onclick="$(this).siblings(\'.browse\').click();"><br><br><input class="browse" type="file" name="Filedata[]"></div>';
	
	// video upload
	$('#placeholderVid').html(browse_html);
	
	$('#placeholderVid input').fileupload({
		url: "/upload/video.php?s=" + session_id,
		dataType: 'json',
		send: function (e, data)
		{
			f = new Object();
			f.name = data.files[0].name;
			vidfileQueued(f);
		},
		always: function (e, data)
		{
			console.log(data.result);
			
			if (typeof data.result.error == "string")
				vidsuccess("", data.result.error);
			else if (data.result.success)
				vidsuccess("", data.result.id + ":OK"); // earlier implementations did not use json
		},
		progressall: function (e, data)
		{
			viduploadProgress("", data.loaded, data.total);
		}
	});
	
	// photo upload
	$('#placeholderPhotos').html(browse_html);
	$('#placeholderPhotos').find('.browse').attr('multiple', true);
	
	phQueueTotal = 0;
	phUploadingNo = 1;
	
	$('#placeholderPhotos input').fileupload({
		url: "/upload/photo.php?json=1&s=" + session_id,
		dataType: 'json',
		send: function (e, data)
		{
			uploadingType = "p";
			phQueueTotal++;
			document.getElementById("phbrowse").style.height = "0px";
			document.getElementById("phprogressbarcontainer").style.display = "";
			document.getElementById("phinfo").style.display = "";
			document.getElementById("phdesc_container").style.display = "";
			document.getElementById("phdisclaimer").style.display = "none";
		},
		always: function (e, data)
		{
			photos_done_processing++;
			
			if (typeof data.result.error == "string")
				phUploadSuccess("", data.result.error, "");
			else if (data.result.success)
				phUploadSuccess("", data.result.id + ":" + data.result.hash); // earlier implementations did not use json
			
			if (phQueueTotal == photos_done_processing)
				phUploadComplete("");
		},
		progressall: function (e, data)
		{
			document.getElementById("phprogressbar").style.width = Math.round(data.loaded / data.total * 175) + "px";
			percent = parseFloat(Math.round(1000 * data.loaded / data.total) / 10).toFixed(1);
			
			if (phUploadingNo > phQueueTotal)
				document.getElementById("divuploadstatusph").innerHTML = "Your photos are processing &#0133;";
			else
				document.getElementById("divuploadstatusph").innerHTML = "Completed photos " + (phUploadingNo - 1) + " of " + phQueueTotal + " (" + percent + "%)";
		},
		progress: function (e, data)
		{
			if (data.loaded == data.total)
				phUploadingNo++;
		}
	});
	
	/*

	swfuPhotos = new SWFUpload({
			// Backend Settings

			// File Upload Settings
			file_upload_limit : 0,

			upload_url: "/upload/photo.php?s=" + session_id,
			flash_url: '/upload/swfupload.swf',
			flash9_url: '/upload/swfupload_fp9.swf',
			button_image_url: '/images/btnuploadimages.png',
			button_text: '',
			button_width: 105,
			button_height: 30,
			file_size_limit: "7 MB",
			disableDuringUpload: true,
			
			// Button Settings
			button_placeholder_id : "placeholderPhotos",
			button_text_style : '',
			button_text_top_padding: 0,
			button_text_left_padding: 0,
			button_window_mode: SWFUpload.WINDOW_MODE.TRANSPARENT,
			button_cursor: SWFUpload.CURSOR.HAND,
			file_types: "*.jpg;*.jpeg;*.png;*.gif",
			file_types_description: "Images",

			//file_queued_handler: fileQueuedPh,
			file_queue_error_handler : fileQueueError,
			//upload_start_handler : showProgressBarPh,
			file_dialog_complete_handler : phReadyForUpload,
			upload_progress_handler : phUploadProgress,
			//upload_error_handler : uploadError,
			upload_success_handler : phUploadSuccess,
			upload_complete_handler : phUploadComplete,

			// Debug Settings
			debug: false });*/

	if (typeof page == "undefined")
		document.getElementById("tbtitle").innerHTML = "<?=$site == "m" ? "Tweet something from $siteName" : "Add something to my log book"?> &#0133;";
	else
		document.getElementById("tbtitle").innerHTML = "Add something to the page log book &#0133;";

	resetUpload(true);

	e.value = "";
	e = document.getElementById("txtstatus");
	txtStatusChanged(e);
	e.value = defaultTxtStatus;

<?
$show_fbpages = false;

if( sizeof( $_SESSION['fbpages'] ) > 2 )
  $show_fbpages = true;
else if( sizeof( $_SESSION['fbpages'] ) == 1 && !array_key_exists( 'selection', $_SESSION['fbpages'] ) ) //In other words, we don't want to see this is the only thing set is "selection"
  $show_fbpages = true;

if( $show_fbpages )
{
?>
	e = document.getElementById("postas");

  if( e )
  {
    html =  'Posting as <span id="targetname">';
<?
    if( empty( $_SESSION['fbpages']['selection'] ) || $_SESSION['fbpages']['selection'] == '0' )
    {
      echo "html += '" . $API->name . "';";
    }
    else
    {
      $found = false;
      for( $i = 0; $i < sizeof( $_SESSION['fbpages'] ); $i++ )
        if( $_SESSION['fbpages']['selection'] == $_SESSION['fbpages'][$i]['page_id'] )
        {
          echo "html += '" . $_SESSION['fbpages'][ $i ]['name'] . "';";
          $found = true;
          break;
        }
      if( !$found  )
      {
        echo "html += '" . $API->name . "';";
      }
    }
?>
    html += ' (<a href="javascript:void(0);" onclick="javascript:document.getElementById(\'targetname\').style.display=\'none\'; document.getElementById(\'targetselection\').style.display=\'\';">Change</a>)</span> ';
    html += '<span style="display:none;" id="targetselection">'
    html += '<select id="target_id" name="target_id" style="width:95px;">';
    html += '<option value="0"><? echo $API->name; ?></option>';
<? for( $i = 0; $i <  sizeof( $_SESSION['fbpages'] ); $i++ ) { if($_SESSION['fbpages'][$i]['page_id'] == "" ) continue; ?>
    html += '<option value="<? echo $_SESSION['fbpages'][$i]['page_id'] ?>"<? if( $_SESSION['fbpages']['selection'] == $_SESSION['fbpages'][$i]['page_id'] ) echo " SELECTED"; ?>><? echo $_SESSION['fbpages'][$i]['name']; ?></option>';
<? } ?>
    html += '</select></span>';
    e.innerHTML = html;
  }
<?
}
?>
}

var phUploaded = [];
var phQueueTotal;
var phUploadingNo;

function savePh()
{
	ttitle = document.forms.phform.phtitle.value;	
	desc = document.forms.phform.phdesc.value;

    desc = desc.replace(/<br\s*\/?>/mg,"\n");
	
	if (ttitle == "" || desc == "")
	{
		alert("You must enter both a title and a description for the album.");
	}
	else
	{
		document.getElementById("phcontinue").style.display = "none";
		document.getElementById("phwait").style.display = "";
		
		photos = "";

		for (var k in phUploaded)
			if (phUploaded[k] == 1)
				photos += "," + k.substring(2);


    //For posting to FB pages
    e = document.getElementById("target_id");
    fbtarget = 0;
    if( e )
      fbtarget = e.value;

	  post = new Object();
	  post.gid = currentPage;
	  post.cat = catSelected;
	  post.target = tbTarget;
	  post.fbtarget = fbtarget;
	  post.ids = photos.substring(1);
	  post.m = phMainImage[0];
	  post.title = ttitle;
	  post.descr = escape(desc);

	  
		//post = "gid=" + currentPage + "&cat=" + catSelected + "&target=" + tbTarget + "&fbtarget=" + fbtarget + "&ids=" + photos.substring(1) + "&m=" + phMainImage[0] + "&title=" + escape(ttitle) + "&descr=" + escape(desc);
		//return console.log(post);

		//postAjax("/upload/newalbum.php", post, "savePhHandler");
		
		$.post('/upload/newalbum.php', post).success(
			function (data)
			{
				resetUpload(true);

				x = data.split(":");

				if (x[0] == "OK")
				{
					if (site == "m")
						window.location.href = '/photo.php?id=' + phMainImage[0];
					else
					{
						tagQueue = x[2].split(",");
						showTagPopUp("P", tagQueue[0], x[3]);
					}
				}
				else if( x[0] == "FB" )
				{
				document.location = "/facebook_error.php";
				}
				else if (admin == 1)
					alert("Error: " + data);
				else
					alert("An unknown error has occurred.");
			});
	}
}

function saveVid()
{
	ttitle = document.forms.vidform.vidtitle.value;
	desc = document.forms.vidform.viddesc.value;

	if (ttitle == "" || desc == "")
	{
		alert("You must enter both a title and a description for the video.");
	}
	else
	{
		document.getElementById("vidcontinue").style.display = "none";
		document.getElementById("vidwait").style.display = "";

    //For posting to FB pages
    e = document.getElementById("target_id");
    fbtarget = 0;
    if( e )
      fbtarget = e.value;

		post = "gid=" + currentPage + "&cat=" + catSelected + "&target=" + tbTarget + "&fbtarget=" + fbtarget + "&title=" + escape(ttitle) + "&descr=" + escape(desc) + "&id=" + vidnewid;

		postAjax("/upload/savevid.php", post, "saveVidHandler");
	}
}

function saveYTVid()
{
	if (ytId == 0) //step 1 get video info
	{
		url = document.forms.vidytform.vidyturl.value;

		if (url == "")
		{
			alert("You must enter a valid YouTube address.");
		}
		else
		{
			document.getElementById("vidytwait").style.display = "";
			document.getElementById("btnsaveyt").style.display = "none";

			postAjax("/upload/saveytvid.php", "gid=" + currentPage + "&target=" + tbTarget + "&url=" + escape(url), "saveYTVidHandler");
		}
	}
	else //step 2 update vid info
	{
		{
			ttitle = document.forms.vidytform.vidyttitle.value;
			desc = document.forms.vidytform.vidytdesc.value;

			if (ttitle == "" || desc == "")
				return alert("You must enter both a title and a description for the video.");

			document.getElementById("vidytwait").style.display = "";
			document.getElementById("btnsaveyt").style.display = "none";

      //For posting to FB pages
      e = document.getElementById("target_id");
      fbtarget = 0;
      if( e )
        fbtarget = e.value;

            //postAjax("/upload/saveytvid.php", "gid=" + currentPage + "&target=" + tbTarget + "&url=" + escape(url), "updateYTVidHandler");
			postAjax("/settings/updatemedia.php", "cat=" + catSelected + "&target=" + tbTarget + "&fbtarget=" + fbtarget + "&isyt=1&type=V&id=" + ytId + "&title=" + escape(ttitle) + "&descr=" + escape(desc), "updateYTVidHandler");
		}
	}
}

function updateYTVidHandler(data)
{
  resetUpload(true);

  if (site != "m")
  {
    loadShare("V", 1, ytId, currentPage);
  }
  else
  {
    if( data == "OK" )
    	document.location = ytLocation;
    else
      document.location = "/facebook_error.php";
  }
}
var ytId = 0;
var ytLocation = "";

function saveYTVidHandler(data)
{
	uploadingType = "y";

	document.getElementById("btnsaveyt").style.display = "";
	document.getElementById("btnsaveyt").blur();
	document.getElementById("vidytwait").style.display = "none";

	x = data.split(String.fromCharCode(1));

	if (x[0] == "OK")
	{
		document.getElementById("vidyterror").style.display = "none";
		//document.getElementById("vidyterror").style.color = "#008000";
		//document.getElementById("vidyterror").innerHTML = "Your video has been added to mediaBirdy.";

		document.getElementById("vidytaddress").style.display = "none";
		document.getElementById("vidytinput").style.display = "";

		if (site == "m")
			document.getElementById("vidytcategory").style.display = "none";

		document.getElementById("vidytdesc_container").style.display = "";

		document.forms.vidytform.vidyttitle.value = x[3];
		document.forms.vidytform.vidytdesc.value = x[4];

		ytId = x[1];
		ytLocation = x[2];
	}
  else if( x[0] == "FB" )
  {
    document.location = "/facebook_error.php";
  }
	else
	{
		document.getElementById("vidyterror").style.display = "";
		document.getElementById("vidyterror").innerHTML = data;
	}
}

function saveVidHandler(data)
{
  resetUpload(true);

	x = data.split(":");
	
	if (x[0] == "OK")
	{
		if (site == "m")
			window.location.href = '/video.php?id=' + vidnewid;
		else
		{
			//tagQueue = [vidnewid];
			//showTagPopUp("V", tagQueue[0], x[1]);
			loadShare("V", 1, vidnewid, currentPage);
		}
	}
	else if( x[0] == "FB" )
  {
    document.location = "/facebook_error.php";
  }
  else if (admin == 1)
		alert("Error: " + data);
	else
		alert("An unknown error has occurred.");
}

var numPhotosUploaded = 0;

function phUploadSuccess(obj, data, resp)
{	
	info = data.split(":");

//  alert( info );

	if (info.length == 1)
		alert("ERROR:  " + info[0]);
	else
	{
		if (phMainImage[0] == 0)
		{
			changeMainImage(info[0], info[1], 0);
			document.getElementById("phuploadedcontainer").style.display = "";
			document.getElementById("phmainimagetitle").style.visibility = "visible";
		}
		
		phUploaded['id' + info[0]] = 1;
		
		newdiv = document.createElement("div");
		newdiv.innerHTML = getPhotoHTML(info);
		
		document.getElementById("phuploadedcontainer").appendChild(newdiv);
		numPhotosUploaded++;
	}
}

function phUploadComplete(obj)
{
/*	phUploadingNo++;

	stats = swfuPhotos.getStats();
	if (stats.files_queued > 0)
		swfuPhotos.startUpload();
	else*/
	{
		document.getElementById("phcontinue").style.display = "";
		document.getElementById("phbrowse").style.height = "";
		document.getElementById("phprogressbarcontainer").style.display = "none";
	}
}

function doneCategorySelected()
{
	document.getElementById("upload-category").style.display = "none";

	if (uploadingType == "v")
		document.getElementById("vidinfo").style.display = "";
	else if (uploadingType == "p")
		document.getElementById("phinfo").style.display = "";
	else if (uploadingType == "y")
		document.getElementById("vidytinput").style.display = "";
}

function uploadCategorySelected(cat, catname)
{
	catSelected = cat;
	
	if (uploadingType == "v")
		document.getElementById("vidcategoryselected").innerHTML = catname;
	else if (uploadingType == "p")
		document.getElementById("phcategoryselected").innerHTML = catname;
	else if (uploadingType == "y")
		document.getElementById("vidytcategoryselected").innerHTML = catname;
	
	document.getElementById("uploadcat-" + cat).checked = true;
}

function showCategorySelect()
{
	e = document.getElementById("upload-category");

	e.innerHTML = uploadCategoryHTML;
	e.style.display = "";

	document.getElementById("phinfo").style.display = "none";
	document.getElementById("vidinfo").style.display = "none";
	document.getElementById("vidytinput").style.display = "none";
}
	
function phReadyForUpload(a, b, c) //obsolete, not used anymore
{
	uploadingType = "p";

	stats = swfuPhotos.getStats();
	
	if (stats.files_queued > 0)
	{
		phQueueTotal = stats.files_queued;
		phUploadingNo = 1;
		document.getElementById("phbrowse").style.height = "0px";
		document.getElementById("phprogressbarcontainer").style.display = "";
		if (site == "m")
			document.getElementById("phcategory").style.display = "none";
		document.getElementById("phinfo").style.display = "";
		document.getElementById("phdesc_container").style.display = "";
		document.getElementById("phdisclaimer").style.display = "none";
		//swfuPhotos.startUpload();
	}
}

function fileQueueError(obj, x, msg)
{
	alert(obj.name + ":  " + msg);
}

var vidnewid;
function vidsuccess(obj, data)
{
	if (typeof data !== "undefined")
	{
		document.getElementById("vidfile").style.display = "none";
		document.getElementById("vidbrowse").style.display = "none";
		document.getElementById("vidsuccess").style.display = "";

		x = data.split(":");
		if (x.length == 1)
		{
			document.getElementById("vidinfo").style.display = "none";
			e = document.getElementById("viderror");
			e.innerHTML = x[0];
			e.style.display = "";
		}
		else
		{
			vidnewid = x[0];
			//alert("SUCCESS:  " + data);
			//document.getElementById("vidthumb").style.display = "";
			//document.getElementById("vidthumb").innerHTML = "<img src='" + x[1] + "' alt='' />";
			document.getElementById("vidcontinue").style.display = "";
		}
	}
}

function vidfileQueued(obj)
{
	uploadingType = "v";

	document.getElementById("vidfile_file").innerHTML = obj.name;
	document.forms.vidform.vidtitle.value = obj.name;
	document.getElementById("vidprogressbarcontainer").style.display = "";
	document.getElementById("vidinfo").style.display = "";
	if (site == "m")
		document.getElementById("vidcategory").style.display = "none";
	document.getElementById("viddesc_container").style.display = "";
	document.getElementById("viddisclaimer").style.display = "none";
	//vidswfu.startUpload();
}

function viduploadProgress(obj, done, total)
{
	document.getElementById("vidprogressbar").style.width = Math.round(done / total * 300) + "px";
	percent = Math.round(1000 * done / total) / 10;
}

function resetUpload(showPlaceholder, resetStatus)
{
	if (typeof resetStatus == "undefined")
		resetStatus = false;

	catSelected = 0;

  if( document.getElementById("btnprocess") == null )
  {
//    location.reload();
    return;
  }

	document.getElementById("btnprocess").style.display = "";
	document.getElementById("upload-category").style.display = "none";
	document.getElementById("mediaselection").style.display = "none";
	document.getElementById("phadd").style.display = "none";
	document.getElementById("phchoices").style.display = "none";
	document.getElementById("phcategory").style.display = "";
	document.getElementById("phcategoryselected").innerHTML = "Select a category";
	document.getElementById("phcomputer").style.display = "none";
	document.getElementById("phdisclaimer").style.display = "";
	document.getElementById("phuploadedcontainer").style.display = "none";
	document.getElementById("phmainimagetitle").style.visibility = "hidden";
	document.getElementById("phcontinue").style.display = "none";
	document.getElementById("phwait").style.display = "none";
	document.getElementById("phbrowse").style.height = "";
	document.getElementById("phprogressbarcontainer").style.display = "none";
	document.getElementById("phprogressbar").style.width = "0px";
	document.getElementById("phinfo").style.display = "none";
	document.getElementById("phuploaded").innerHTML = '<div class="phnewparent" id="phnewparent-0"></div>';
	document.getElementById("phuploadedcontainer").innerHTML = '<div style="font-weight: bold; margin-bottom: 2px;">Manage Your Images</div><div style="font-size: 8pt;">Set your main image by selecting a photo, or delete an image by placing your cursor on it and selecting the &quot;X&quot;.</div>					<div id="phuploaded" style="margin-top: 5px; width:300px;"></div><div style="clear: both;"></div>';
	document.getElementById("mainimage-0").src = "/images/albumempty.png";
	document.forms.phform.phtitle.value = "";
	document.forms.phform.phdesc.value = "";


	phMainImage[0] = 0;
	phUploaded = new Array();
	numPhotosUploaded = 0;

	document.getElementById("vidadd").style.display = "none";
	document.getElementById("vidchoices").style.display = "none";
	document.getElementById("vidcomputer").style.display = "none";
	document.getElementById("vidyt").style.display = "none";
	document.getElementById("vidcategory").style.display = "";
	document.getElementById("vidcategoryselected").innerHTML = "Select a category";
	document.getElementById("vidbrowse").style.display = "";
	document.getElementById("vidfile").style.display = "";
	document.getElementById("viddisclaimer").style.display = "";
	document.getElementById("vidsuccess").style.display = "none";
	document.getElementById("vidthumb").style.display = "none";
	document.getElementById("viderror").style.display = "none";
	document.getElementById("vidinfo").style.display = "none";
	document.getElementById("vidcontinue").style.display = "none";
	document.getElementById("vidwait").style.display = "none";
	document.getElementById("vidprogressbar").style.width = "0";
	document.getElementById("vidprogressbarcontainer").style.display = "none";
	document.getElementById("vidfile_file").innerHTML = "";

	document.forms.vidform.vidtitle.value = "";
	document.forms.vidform.viddesc.value = "";

	document.forms.vidytform.vidyturl.value = "";
  document.forms.vidytform.vidyttitle.value = '';
	document.forms.vidytform.vidytdesc.value = '';

	document.getElementById("vidytaddress").style.display = "";
	document.getElementById("vidytinput").style.display = "none";
  document.getElementById("btnsaveyt").style.display = "";
  document.getElementById("vidytwait").style.display = "none";

	document.getElementById("vidytcategory").style.display = "";
	document.getElementById("vidytcategoryselected").innerHTML = "Select a category";
	
	document.getElementById("logbubble").style.display = "none";
	
	e = document.getElementById("txtstatus");

	if (e.value == "" || resetStatus)
	{
		e.value = "";
		txtStatusChanged(e);
		e.value = defaultTxtStatus;
	}

	document.getElementById("tbstatus").style.display = "";
	document.getElementById("btnprocess").style.display = "";
	document.getElementById("wpcharsremain").style.display = "";
}

function showUpload(showDivs)
{
resetUpload(false);

	if (document.getElementById("txtstatus").value == defaultTxtStatus)
		document.getElementById("txtstatus").value = "";

	document.getElementById("btnprocess").style.display = "none";
	document.getElementById("tbstatus").style.display = "none";
	document.getElementById("wpcharsremain").style.display = "none";

	document.getElementById("logbubble").style.display = "";

	for (i = 0; i < showDivs.length; i++)
	{
		document.getElementById(showDivs[i]).style.display = "";

		if (showDivs[i] == "phadd")
			document.getElementById("logbubble").style.backgroundPosition = "217px 0";
		else if (showDivs[i] == "vidadd")
			document.getElementById("logbubble").style.backgroundPosition = "136px 0";
	}
}

function showPhonePopup()
{
	window.open("/email.php?1", "Phone", "location=0,status=0,scrollbars=1,width=591,height=700");
}

function showTOSPopUp()
{
window.open('/tos.php?1', 'TOS','location=0,status=0,scrollbars=1,width=500,height=600');
}

function showPrivacyPopUp()
{
window.open('/privacy.php?1', 'Privacy','location=0,status=0,scrollbars=1,width=500,height=600');
}


$(function () {
    $('label.upload input').fileupload({
        dataType: 'json',
		progressall: function (e, data) {
			var progress = parseInt(data.loaded / data.total * 100, 10);
			$(this).parent().parent().find('.progressbar_container .progressbar').show().css(
				'width',
				progress + '%'
			);
		},
        done: function (e, data) {
            console.log(data.result);
			$('.loglink .img_container img').hide();
			$('.loglink .img_container').append('<img src="' + window.location.protocol + '//' + window.location.hostname + '/images/tmp_image.php?u=' + escape(data.result.image) + '" style="display: inline;" class="loaded" alt>');
			$('.loglink .prev').click();
			$('.loglink .next').click();
			
			$(this).parent().parent().find('.progressbar_container .progressbar').fadeOut();
        }
    });
});

function urlencode (str) {
  str = (str + '').toString();

  return encodeURIComponent(str).replace(/!/g, '%21').replace(/'/g, '%27').replace(/\(/g, '%28').
  replace(/\)/g, '%29').replace(/\*/g, '%2A').replace(/%20/g, '+');
}
