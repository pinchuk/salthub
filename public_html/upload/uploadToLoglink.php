<?php

header('Content-type: application/json');

include '../inc/inc.php';
include '../inc/simpleimage.php';

$image = new SimpleImage($_FILES["Filedata"]["tmp_name"]);

if ($image->get_width() >= 370)
{
	$image->fit_to_width(370);
	$tmp_file = 'wp_' . md5(microtime()) . '_l.jpg'; // "large"
}
else
{
	$image->best_fit(119, 95);
	$tmp_file = 'wp_' . md5(microtime()) . '_s.jpg'; // "small"
}

$success = $image->save("/tmp/$tmp_file", 80);

if ($success)
	$img = $API->getContainer() . '/' . $tmp_file;
else
	$img = '/images/noimage.jpg';

echo json_encode(array('image' => $img));

//fork process and upload the image to the cloud
$params = array('image' => $tmp_file, 'uid' => $API->uid);
$cmd = "/usr/bin/php " . $_SERVER['DOCUMENT_ROOT'] . "/upload/upload_temp_to_cloud.php " . base64_encode(json_encode($params)) . " > /dev/null 2>&1 &";
exec($cmd, $out);



/*
$name = md5(rand()).'_'.$_FILES["Filedata"]["name"];
$target = '/images/wallpost/'.$name;
if( move_uploaded_file(
    $_FILES["Filedata"]["tmp_name"],
    $_SERVER["DOCUMENT_ROOT"].$target
    )
)
{
    echo 'http://dev.salthub.com'.$target;
    exit;
}
echo 'error';
exit;

if (!$fromEmail)
	processPhoto();


function processPhoto($sourcee = null, $skipChecks = false)
{
  include_once $_SERVER["DOCUMENT_ROOT"] . "/inc/createThumb.php";

	global $API, $fromEmail, $hash, $source, $site;

	if (!empty($sourcee))
		$source = $sourcee;


	$isUserPic = isset($_GET['userpic']);
	
	if (!$fromEmail && !$skipChecks) //bypass certain checks if this script is being included as part of converting pictures emailed to the site
	{
		if (empty($_FILES['Filedata']['tmp_name']))
		{
			//echo "UNKNOWN_ERROR";
			invalidImage($isUserPic);
			return;
		}

		if (empty($_GET['s']))
		{
			if (empty($_COOKIE['PHPSESSID']))
			{
				echo "NO_SESSION";
				return;
			}
		}
		elseif (isset($_GET['s']))
			session_id($_GET['s']);
	}


	include_once $_SERVER["DOCUMENT_ROOT"] . "/inc/inc.php";
	
	if (empty($API->uid)) //we can't check the conventional way with isLoggedIn() because the facebook cookies aren't passed here
	{
		echo "BAD_SESSION";
		return;
	}

	if (empty($source)) //source might have been set by the email script
		$source = $_FILES['Filedata']['tmp_name'];

	$info = getimagesize($source);

	if ($fromEmail)
		echo "\nSource is $source\n\n";
	
	switch ($info['mime'])
	{
		case "image/jpeg":
		$img = @imagecreatefromjpeg($source);
		if (!$img)
		{
			invalidImage($isUserPic);
			return;
		}
		break;
		
		case "image/png":
		$img = @imagecreatefrompng($source);
		if (!$img)
		{
			invalidImage($isUserPic);
			return;
		}
		break;
		
		case "image/gif":
		$img = @imagecreatefromgif($source);
		if (!$img)
		{
			invalidImage($isUserPic);
			return;
		}
		break;

		default:
		invalidImage($isUserPic);
		return;
		break;
	}

	$width = $info[0];
	$height = $info[1];

	if ($isUserPic && $site == "m") // user pic - scale and crop
	{
		//48x48
		$target_width = 178;//48;
		$target_height = 266;//48;
		$tr = $target_width / $target_height;
		
		
		$widthOffest = 0;
		$heightOffest = 0;
		
		$ir = $width / $height;

		if ($ir > $tr) // too wide
			$target_height = $target_width / $ir;
		else // too tall
			$target_width = $target_height * $ir;
		
		$new_img = ImageCreateTrueColor($target_width, $target_height);
		imagecopyresampled($new_img, $img, 0, 0, round($widthOffset / 2), round($heightOffset / 2), $target_width, $target_height, $width - $widthOffset, $height - $heightOffset);
		
		$target_file = $_SERVER["DOCUMENT_ROOT"] . "/userpics/" . $API->uid . "_2.jpg";
	}
	else // photo upload - just scale
	{
		//875x1167
		$target_width = 875;
		$target_height = 1167;

		if ($width > $target_width || $height > $target_height)
		{
			$tr = $target_width / $target_height;
			$ir = $width / $height;

			if ($ir > $tr) // too wide
				$target_height = $target_width / $ir;
			else // too tall
				$target_width = $target_height * $ir;
		}
		else
		{
			$target_width = $width;
			$target_height = $height;
		}

		$ratio = $target_width / $target_height;



     $newAlbum = false;
		if ($site == "s")
		{
			$aid = quickQuery("select id from albums where uid=" . $API->uid . " and albType=" . ($isUserPic ? 2 : 1));

			if (empty($aid))
			{
        $newAlbum = true;
				$API->createSpecialAlbums();
				$aid = quickQuery("select id from albums where uid=" . $API->uid . " and albType=" . ($isUserPic ? 2 : 1));
			}

      if( $isUserPic || $_GET['pc'] == 1)
        $cat = 2;
      elseif( $_GET['pc'] == 2 )
        $cat = 1299;
      elseif( isset( $_GET['cat'] ) )
        $cat = $_GET['cat'];
      else
        $cat = 0;

			mysql_query("insert into photos (uid,hash,width,height,aid,cat) values (" . $API->uid . ",'$hash',$target_width,$target_height,$aid,$cat)");

		}
		else
			mysql_query("insert into photos (uid,hash,ratio) values (" . $API->uid . ",'$hash',$ratio)");

		$id = mysql_insert_id();


    if( $newAlbum )
    {
      mysql_query( "update albums set mainImage='{$id}' where uid='" . $API->uid . "' and albType='2'" );
    }

		$jmp = shortenMediaURL("P", $id);
		mysql_query("update photos set jmp='$jmp' where id=$id");

//		$newDir = $_SERVER["DOCUMENT_ROOT"] . "/photos/" . getPathFromPhotoID($id);
		$target_file = "/tmp/$hash.jpg";

		$new_img = ImageCreateTrueColor($target_width, $target_height);
		imagecopyresampled($new_img, $img, 0, 0, 0, 0, $target_width, $target_height, $width, $height);
	}


	imagejpeg($new_img, $target_file, 90);
	@chmod($target_file, 0777);

	imagedestroy($img);
	imagedestroy($new_img);



  //Upload Photo to Cloud
  //Original Photo
  
  if ($isFromEmail)
	echo "\nTarget is $target_file\n\n";
  
  $API->uploadToCloud( $API->uid, $target_file, $hash . ".jpg" );
  
  $temp_thumb = "/tmp/" . mysql_thread_id();

  if( file_exists( $temp_thumb ) ) unlink( $temp_thumb );

  $file = createThumb( 119, 95, true, true, $id, $hash, $temp_thumb );
  $API->uploadToCloud( $API->uid, $file, $hash . "_wide.jpg" );

  if( file_exists( $temp_thumb ) ) unlink( $temp_thumb );
  $file = createThumb( 178, 266, false, true, $id, $hash, $temp_thumb );
  $API->uploadToCloud( $API->uid, $file, $hash . "_tall.jpg" );

  if( file_exists( $temp_thumb ) ) unlink( $temp_thumb );
  $file = createThumb( 48, 48, true, true, $id, $hash, $temp_thumb );
  $API->uploadToCloud( $API->uid, $file, $hash . "_square.jpg" );

	if ($fromEmail)
		echo "\n\nTmp thumb is  $temp_thumb \n\n";
  //if( file_exists( $temp_thumb ) ) unlink( $temp_thumb );

  //Done Upload Photo to Cloud
  
  
  if (strpos($source, "/mnt/ss") === false) unlink($source);


	if ($isUserPic)
	{
    $active = intval( quickQuery( "select active from users where uid='" . $API->uid . "'" ) );
    if( $active == 0 )
    {
  		mysql_query("update photos set reported=2 where id=$id"); //Inactive users who are uploading their user photo should not have their photos immediately available
    }

		mysql_query("update photos set privacy=" . PRIVACY_EVERYONE . " where id=$id");

		$_POST['pic'] = $site == "m" ? 2 : $id; //for mediabirdy, they can only have 1 custom user pic, splash has an album so we send the photo id
		include "../settings/changeuserpic.php";

		if (isset($_POST['next']))
			header("Location: " . $_POST['next']);
		else
			header("Location: /settings?success");

		die();
	}
	elseif ($_GET['pc'] == 1 || $_GET['pc'] == 2 ) //from photo chooser module
	{
    if( $_GET['pc']  == 1 )
    {
  		mysql_query("update photos set privacy=" . PRIVACY_EVERYONE . " where id=$id");
  		echo json_encode(array("id" => $id, "thumb" => "/img/48x48/photos/" . $id . "/$hash.jpg", "img" => "/img/N178x266/photos/" .$id . "/$hash.jpg" ) ); //, "img_physical_path" => "/photos/" . getPathFromPhotoID($id) . "/$hash.jpg" ));
    }
    else
    {
	  	echo json_encode(array("id" => $id, "thumb" => "/img/48x48/photos/" . $id . "/$hash.jpg", "img" => "/img/100x75/photos/" .$id . "/$hash.jpg" ) ); //, "img_physical_path" => "/photos/" . getPathFromPhotoID($id) . "/$hash.jpg" ));
      mysql_query("update photos set privacy=" . PRIVACY_SELF . " where id=$id");
    }

	}
	else
	{
		if ($fromEmail)
			return $id;
		else
			echo "$id:$hash";
	}
}

function invalidImage($isUserPic)
{
	if ($isUserPic)
	{
		if (isset($_POST['from']))
			header("Location: " . $_POST['from'] . "?invalid_image");
		else
			header("Location: /settings?invalid_image");
	}
	
	echo "INVALID_IMAGE";
}
*/?>
