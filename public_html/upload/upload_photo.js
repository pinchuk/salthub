var phMainImage = [];
phMainImage[0] = 0;

function changeMainImage(id, hash, aid)
{
	e = document.getElementById("mainimage-" + aid);
	phMainImage[aid] = id;
	
	if (aid == 0) // from upload page
		e.src = "/showimage.php?crop=1&w=75&h=50&f=/photos/" + id + "/" + hash + ".jpg";
	else //from manage page
	{
		e.src = "/showimage.php?crop=1&w=100&h=75&f=/photos/" + id + "/" + hash + ".jpg";
		postAjax("/settings/changemainimage.php", "aid=" + aid + "&id=" + id);
	}
}

function deleteImage(id, hash, aid)
{
	if (phMainImage[aid] == id)
	{
		alert("You cannot delete the main image.  Change the main image and try again.");
	}
	else
	{
		postAjax("/phdelete.php", "id=" + id + "&hash=" + hash);
		if (typeof phUploaded != "undefined")
			phUploaded['id' + id] = 0;
		document.getElementById("uploadedph-" + id).style.display = "none";
	}
}

function getPhotoHTML(info) // gets the html for the photo with the red x that pops up.  info = array[id, hash, optional aid];
{
	if (info.length == 2)
		info[2] = 0;
	return '<span class="phnewcontainer" id="uploadedph-' + info[0] + '" onmouseover="javascript:document.getElementById(\'redx-' + info[0] + '\').style.display=\'\';" onmouseout="javascript:document.getElementById(\'redx-' + info[0] + '\').style.display=\'none\';"><img id="redx-' + info[0] + '" src="/images/redx.png" style="display: none; position: absolute; top: 5px; left: 5px; cursor: pointer;" onclick="javascript:deleteImage(\'' + info[0] + '\',\'' + info[1] + '\', ' + info[2] + ');"><a class="phnew" onclick="javascript:changeMainImage(' + info[0] + ', \'' + info[1] + '\', ' + info[2] + ');" href="javascript:void(0);"><img height="50" width="75" src="/showimage.php?crop=1&w=75&h=50&f=/photos/' + info[0] + '/' + info[1] + '.jpg"></a></span>';

//	return '<div class="phnewcontainer" id="uploadedph-' + info[0] + '" onmouseover="javascript:document.getElementById(\'redx-' + info[0] + '\').style.display=\'\';" onmouseout="javascript:document.getElementById(\'redx-' + info[0] + '\').style.display=\'none\';"><img id="redx-' + info[0] + '" src="/images/redx.png" style="display: none; position: absolute; top: 5px; left: 5px; cursor: pointer;" onclick="javascript:deleteImage(\'' + info[0] + '\',\'' + info[1] + '\', ' + info[2] + ');"><a class="phnew" onclick="javascript:changeMainImage(' + info[0] + ', \'' + info[1] + '\', ' + info[2] + ');" href="javascript:void(0);"><img src="/showimage.php?crop=1&w=75&h=50&f=/photos/' + info[0] + '/' + info[1] + '.jpg"></a></div>';
}