<?php

// takes an image that was uploaded through wallpost.php and uploads it
// to the cloud - this script is forked so the user doesn't have to wait
// for the image to upload to the cloud

if (isset($_SERVER['HTTP_HOST']))
	die('no');

//file_put_contents('/tmp/here', $argv[1]);

$params = json_decode(base64_decode($argv[1]), true);

$_SERVER['DOCUMENT_ROOT'] = "/var/www/salthub.com/public_html";
include $_SERVER['DOCUMENT_ROOT'] . "/inc/inc.php";

$API->uid = $params['uid'];

$tmp_file = $params['image'];
$ret = $API->uploadToCloud(null, "/tmp/$tmp_file", $tmp_file);

unlink("/tmp/$tmp_file");

?>