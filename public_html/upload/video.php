<?php

/*
Processes a video to determine it's size and type.  Launches the vide compression process.
*/

//ini_set('display_errors',1);
//error_reporting(E_ALL);

if (!$fromEmail)
{
	header('Content-type: application/json');
	processVideo();
}

function processVideo()
{
	global $API, $source, $fromEmail, $hash, $site;

  $_SERVER['DOCUMENT_ROOT'] = "/var/www/salthub.com/public_html";

	//if video was meant to be uploaded to someone else's profile, target is set
	$target = intval($_POST['target']);

	if (!$fromEmail) // if running from the email check script, skip some checks we know will pass
	{
		file_put_contents('/tmp/fd', var_export($_FILES, true));
		if (empty($_FILES['Filedata']['tmp_name'][0]))
			error("An unknown error occurred while uploading your video.");

		if (empty($_GET['s']))
			error("No session data sent.");
		
		session_id($_GET['s']);

		include_once $_SERVER["DOCUMENT_ROOT"] . "/inc/inc.php";

		if (empty($API->uid)) //we can't check the conventional way with isLoggedIn() because the facebook cookies aren't passed here
			error("Sorry, it looks like you logged out before your video finished uploading.&nbsp; Please log-in and try again.");

		$ext = strtolower(end(explode(".", $_FILES['Filedata']['name'][0])));
		if ($ext != "wmv")
			$ext = "xxx"; //we are just curious if this is a wmv file, we will have mencoder process (vs ffmpeg) later

		$source = "/tmp/vidupload-" . $hash;
	
		move_uploaded_file($_FILES['Filedata']['tmp_name'][0], $source);
		
		chmod($source, 0777);
	}
	else
		$ext = "xxx";

	$cmd = "mediainfo --Language=raw $source";
	exec($cmd, $info);

	$validVideo = false;
	$dur = 0;
	
	foreach ($info as $y)
	{
		$z = explode(":", $y, 2);
		$key = trim($z[0]);
		$val = trim($z[1]);
		
		if (count($z) == 1)
		{
			$heading = $key;
			continue;
		}

		if ($heading == "Video")
		{
			$validVideo = true;
			if ($key == "Width/String") //grab the width
				$width = current(explode(" ", $val));
			elseif ($key == "Height/String") //grab the width
				$height = current(explode(" ", $val));
			elseif ($key = "FrameRate/String") //grab framerate
				$framerate = intval(current(explode(" ", $val)));
		}

		if ($dur == 0 && $key == "Duration/String") //calculate video duration
		{
			$y = explode(" ", $val);
			foreach ($y as $d)
				if (substr($d, -2) == "hr")
					$dur += substr($d, 0, strlen($d) - 1) * 3600;
				elseif (substr($d, -2) == "mn")
					$dur += substr($d, 0, strlen($d) - 2) * 60;
				elseif (substr($d, -2) == "ms")
					$dur += substr($d, 0, strlen($d) - 2) / 100;
				elseif (substr($d, -1) == "s")
					$dur += substr($d, 0, strlen($d) - 1);
		}
	}

	if ($validVideo && $dur > 0)
	{
		mysql_query("insert into videos (duration,hash,uid) values ($dur,'$hash'," . $API->uid . ")");
		$id = mysql_insert_id();

		//thumbnail generation will be handled now in compress.php

//		$cmd = "/usr/local/bin/php " . $_SERVER['DOCUMENT_ROOT'] . "/upload/compress.php $id $hash $width $height $dur $ext $framerate " . $API->uid . " $site $target > /dev/null 2>&1 &";
		$cmd = "/usr/bin/php " . $_SERVER['DOCUMENT_ROOT'] . "/upload/compress.php $id $hash $width $height $dur $ext $framerate " . $API->uid . " $site $target > /dev/null 2>&1 &";

		if ($fromEmail)
			return array("id" => $id, "cmd" => $cmd);
		else
		{
			exec("export LD_LIBRARY_PATH=/usr/local/lib; " . $cmd, $out);
			echo json_encode(array("success" => true, "id" => $id, "msg" => $out));
		}
	}
	else
	{
		//unlink($source);
		error("You've uploaded an invalid video or one that we can't process.");
	}
}
	

function error($msg)
{
	echo json_encode(array("error" => $msg));
	die();
}
?>
