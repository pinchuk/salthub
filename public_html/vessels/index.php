<?php
/*
This area is a search tool to find vessels. Vessels_side.php Contains the search parameters;
 as parameters are chosen, new searches are carried out by vessels_search.php.

11/5/2011 - Created
*/

//Allow the FB crawler to see this site without logging in.


if( stristr( $_SERVER['HTTP_USER_AGENT'], "facebookexternal" ) !== false )
{
  $facebookCrawler = true;
    $noLogin = true;
}


include( "../inc/inc.php" );

$meta[] = array( "property" => "og:description", "content" => "An in-depth resource that provides information on thousands of vessels. You can see vessel locations, find past and present employees, ship particulars, photos, videos and more." );
$meta[] = array( "property" => "og:title", "content" => "Vessel Directory | $siteName" );
$meta[] = array( "property" => "og:url", "content" => "http://" . SERVER_HOST . "/vessels/" );
$meta[] = array( "property" => "og:image", "content" => "http://www.salthub.com/images/radar.jpg" );
$meta[] = array( "property" => "og:site_name", "content" => $siteName );
$meta[] = array( "property" => "og:type", "content" => "website" );
$meta[] = array( "property" => "fb:app_id", "content" => $fbAppId );
$title = "111Vessel Directory - Marine Traffic - AIS - Photos | SaltHub - Professional Maritime Network";
$descr = "111An in-depth resource that provides information on thousands of vessels. You can see vessel locations, find past and present employees, ship particulars, photos, videos and more.";

if( $facebookCrawler )
{
  $API->uid = 91;
}

$borderStyle = 1;

include( "../header.php" );


?>

<div style="float: left; <?=$borderStyle == 1 ? "border: 1px solid #d8dfea; border-top: 0;" : ""?>">
  <!-- Left panel -->
	<div class="profileleft" style="<?=$borderStyle == 1 ? "border-left: 0; border-bottom: 0;" : ""?>" id="profileleft">
		<div class="minheight" style="background-image: url(/images/bgprofilepic.png); background-position: 10px 0px; background-repeat: repeat-x;">&nbsp;</div>

		<div class="leftcolumn_container">
			<div class="profilepic" style="text-align: center;">
				<div class="borderhider">&nbsp;</div>

        <div class="profilepic">
          <img src="/images/vessel_dir.jpg" width="178" height="208"/>
        </div>
			</div>

      <? include( "vessels_side.php" ); ?>

		</div>
	</div>


  <!-- Main content -->
	<div class="profilecontent_container" id="profilecontent_container" style="<?=$borderStyle == 0 ? "" : "border-right: 0;"?>">
		<div class="minheight">&nbsp;</div>

		<div class="profilecontent<?=$borderStyle == 0 ? "" : " profilecontent_minus1"?>">
      <? include( "vessels_top_menu.php" ); ?>

      <div style="clear:both;"></div>


      <div class="strong" style="margin-left:10px; margin-top:15px; font-size:12pt; margin-bottom:0px; padding-bottom:0px;">
        <div style="float:left; margin-right:5px;"><img src="/images/disconnect.png" width="16" height="16" alt="" /></div>Connect with Vessels
      </div>

      <div style="font-size:9pt; margin-left:10px; margin-right:8px; margin-top:5px;">
      <?=$siteName?>'s Vessel Directory is an in-depth resource that provides information on thousands of vessels. You can see vessel locations, find past and present employees, ship particulars, photos, videos and more. You can also add and update vessel data.
      </div>

      <div id="searchResults" style="padding-left:10px;">
        <? include( "vessels_search.php" ); ?>
      </div>
		</div>

	  <div style="clear: both;"></div>
  </div>
</div>

<div style="float:left;">
  <!-- Advertisements -->
  <div style="width: 135px; float: left; position: relative;">
    <div style="position: absolute; top: 0; left: -3px; background: white; height: 26px;">&nbsp;</div>
    <? if( $API->adv ) { ?>
  	<div style="padding: <?=$site == "s" && $action == "about" ? 26 : 26?>px 0 0 15px;">
  		<?php showAd("skyscraper",4); ?>
  	</div>
    <? } ?>
  </div>
</div>

<script type="text/javascript">
<!--

function changePage( p,q  )
{
  getAjax("/vessels/vessels_search.php?p=" + p + q, "changePageResponse");
}

function changePageResponse( data )
{
  e = document.getElementById( "searchResults" );
  if( e )
  {
    e.innerHTML = data;
  }
}
-->
</script>

<?php
include "../footer.php";
?>
