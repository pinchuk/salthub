<?
/*
This is a container for the map popup called from a vessel page.  Opens vessel_map.php
*/

require( "../inc/inc.php" );

$page = $API->getPageInfo(intval( $_GET['gid'] ));
$name = $page['gname'];

$exnames = $page['boat']['exnames'];
if( strlen( $exnames ) > 0 ) $exnames = "(ex. " . $exnames . ")";

$url = urlencode("http://" . SERVER_HOST . $API->getPageURL($page['gid'], $page['gname']) ) . "/LiveTrack";
$jmp = getMediaJMPLink( $page['gid'], "G" );
?>

<div style="color:rgb(85,85,85); font-size:18pt; font-weight:bold;">
  <div style="float:left;">
    <? echo $name;?> <span style="font-size:8pt; font-weight:300;"><?=$exnames;?></span>
  </div>

  <div style="float:right;">
		<table border="0" cellpadding="0" cellspacing="0" class="mediaactions">
			<tr>
				<td style="padding-right: 10px; padding-top:4px;">
          <a type="button_count" href="javascript:void(0);" onclick="javascript: openPopupWindow( 'http://www.facebook.com/sharer/sharer.php?u=<? echo urlencode($url); ?>', 'Share', 550, 320 );">
            <img src="/images/facebook_share.png" width="58" height="20" alt="" />
          </a>
        </td>
				<td>
          <iframe data-twttr-rendered="true" title="Twitter Tweet Button" style="width: 95px; height: 20px;" class="twitter-share-button twitter-count-horizontal" src="http://platform.twitter.com/widgets/tweet_button.1347008535.html#_=1347731561517&amp;count=horizontal&amp;id=twitter-widget-0&amp;lang=en&amp;original_referer=<? echo $jmp; ?>&amp;size=m&amp;text=View Current Track and Location of <?=$name?> &amp;url=<? echo $jmp; ?>&amp;via=SaltHub" allowtransparency="true" frameborder="0" scrolling="no"></iframe>
        </td>
			</tr>
		</table>
  </div>
</div>



<iframe width="800" height="600" frameborder="0" scrolling="no" marginheight="0" marginwidth="2" id="map" class="map_canvas" src="/vessels/vessel_map.php?gid=<?=$_GET['gid']?>&hideOthers=<?=$_GET['hideOthers'];?>&navPoint=<?=$_GET['navPoint'];?>&showTrack=<?=$_GET['showTrack']?>"></iframe>
