<?

function navstatToStr( $navstat )
{
  if( $navstat == 5 ) return "Moored";
  if( $navstat == 0 || $navstat == 8 ) return "Underway";
  return "Anchored";
}

function getShape( $navstat, $length, $type, $heading, $sog, $isFocus )
{
  $path = "";
  $scale = 1.2;
  $color = "#000000";
  $rotation = 0;

  if( $length > 120 )
    $scale = 1.5;
  if( $length > 300 )
    $scale = 2.0;

  if( $sog < 0.1 ) $navstat = 1;

  switch( $navstat )
  {
    case 0:
    case 8:
      $path = "'M 10 -10 M -10 10 M 0 2 L -3 4 L 0 -6 L 3 4 z'";

      if( $heading < 360 )
        $rotation = $heading;
    break;

    default:
      $path = 'google.maps.SymbolPath.CIRCLE';
      $scale *= 3.0;
  }

  switch( $type )
  {
    case 1219: // Passenger
    case 1221: // Yacht
    case 1222: // Sailing
      $color = "#00FF00";
    break;

    case 1215: // Cargo
    case 1216: // Fishing
    case 1223: // Search and Rescue
    case 1224: // Special Craft
    case 1225: // Tanker
    case 1226: // Work boat
      $color = "#FF0000";
    break;

    case 1217: // HSLC
    case 1228: // Wing in ground
      $color = "#0000FF";
    break;

    case 1227: // Unspecified
    default:
      $color = "#00FF00";
  }

  if( $isFocus )
    $color = "#ff00ff";

  echo "fillColor: '$color',strokeColor:'#000000',path:$path,scale: $scale,rotation:$rotation,strokeOpacity:1";
}

?>