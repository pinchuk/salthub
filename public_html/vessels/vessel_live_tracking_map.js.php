<?
require( "../inc/inc.php" );

?>
var map;
var infoWindow;
var rects = Array();
var markers = Array();
var shapes = Array();
var prevZoomLevel = 3;
var marker_icon = "/images/sailing-ship-icon-grey.png";
var heatmap;
var heatmapEnabled = true;
var currentVesselPath = 0;
var vesselPath = null;
var pathMarkers = Array();
var firstLoad = true;
var loadingMarkers = false;

function initPage() {
  var myLatLng = new google.maps.LatLng(5, 0);
  var defaultZoom = 2;

  var mapOptions = {
    zoom: defaultZoom,
    center: myLatLng,
    mapTypeId: google.maps.MapTypeId.TERRAIN
  };

  map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);

  createHeatMap();
  createRects();

  infowindow = new google.maps.InfoWindow();

  google.maps.event.addListener(map, 'dragend', mapCenterChanged );
  google.maps.event.addListener(map, 'zoom_changed', mapZoomChanged );
  google.maps.event.addListener(map, 'click', mapDoubleClick );
  google.maps.event.addListener(map, 'dblclick', mapDoubleClick );

}

function createRects()
{
  <?
  $width  = 10;
  $height = 5;

  for( $x = 0; $x < (360); $x += $width )
    for( $y = 0; $y < (180); $y += $height )
    {
  ?>

  rect = new google.maps.LatLngBounds(
      new google.maps.LatLng(<?= $y - 90 ?>.0, <?= $x - 180; ?>.0),
      new google.maps.LatLng(<?= $y - 90 + $height ?>.0, <?= $x - 180 + $width; ?>.0)
    );

  rectObj = new google.maps.Rectangle({
    bounds: rect,
    strokeColor: '#FF00FF',
    strokeOpacity: 0.07,
    strokeWeight: 1,
    fillColor: '#FF00FF',
    fillOpacity: 0.05
  });

  rectObj.setMap(map);

  // Add a listener for the click event
  google.maps.event.addListener(rectObj, 'click', mouseClickRect);
  google.maps.event.addListener(rectObj, 'mouseover', mouseOverRect);
  google.maps.event.addListener(rectObj, 'mouseout', mouseOutRect);

  rects.push( rectObj );
  <?
    }
  ?>
}


function createHeatMap()
{
  var heatmapData = [
<?
  // Changed by AppDragon
  //  $q = mysql_query( "SELECT x_cell, y_cell, COUNT(*) as weight FROM boats Where lat !=0 and lon != 0 GROUP BY x_cell, y_cell");
  $q = mysql_query( "SELECT x_cell, y_cell, COUNT(*) as weight FROM boats Where lat !=0 and lon != 0 GROUP BY x_cell, y_cell");//order by weight desc" );

  $W = 2.5;
  $H = 2.5;
  $min = 50;
  $max = 3000;
  $first = true;
  while( $r = mysql_fetch_array( $q ) )
  {
    if( !$first ) echo ",";
    $first = false;
    $heat = $r['weight'];

    if( $heat > $max ) $heat = $max;
    if( $heat < $min ) $heat = $min;
    $heat /= $max;

    $x = $r['x_cell'] * $W - 180;
    $y = $r['y_cell'] * $H - 90;

?>
    {location: new google.maps.LatLng(<?=$y?>, <?=$x?>), weight: <?=$heat;?>}
<?
  }
?>

  ];

  heatmap = new google.maps.visualization.HeatmapLayer({
    data: heatmapData,
    dissipating: false,
    opacity:0.5,
    maxIntensity : 1.0,
    radius: 4
  });
  heatmap.setMap(map);
}

function disableHeatMap()
{
  heatmap.setOptions( {opacity:0} );
  heatmapEnabled = false;
}

function enableHeatMap()
{
  heatmap.setOptions( {opacity:0.6} );
}

function mouseClickRect( event )
{
  bounds = this.getBounds();
  map.panTo( bounds.getCenter() );
  map.setZoom( 7 );
  addMarkers();
}

function mouseOutRect( event )
{
  this.setOptions( { fillOpacity: 0.05 } );
}

function mouseOverRect( event )
{
  this.setOptions( { fillOpacity: 0.5 } );
}

function mapCenterChanged( event )
{
  if( map.getZoom() > 4 )
    addMarkers();
}

function mapDoubleClick( event )
{

  map.panTo( event.latLng );
  if( map.getZoom() < 7 )
  {
    map.setZoom( 7 );
  }
  else
    map.setZoom( map.getZoom() + 1 );
}

function destroyRects()
{
  removed = rects.splice( 0, rects.length );

  for( c = 0; c < removed.length; c++ )
  {
    removed[c].setMap( null );
    delete removed[c];
  }
}

function destroyMarkers( )
{
  for( var key in markers )
  {
    markers[key].setMap( null );
    delete markers[key];
  }

  for( var key in pathMarkers )
  {
    pathMarkers[key].setMap( null );
    delete pathMarkers[key];
  }

  if( vesselPath != null )
  {
    vesselPath.setMap( null );
    delete vesselPath;
  }

  markers = Array();
}

function addMarkers()
{
  if( !loadingMarkers )
  {
    post = ""

    kw1 = window.document.getElementById("kw");
    if( kw1 && kw1.value != "" )
      post += "&kw=" + encodeURIComponent( kw1.value );

    for( i = 0; i < conditions.length; i++ )
    {
      if( !conditions[i] ) continue;

      found = false;
      post += "&c" + i + "=";
      for( var j in conditions[i] )
      {
        if( conditions[i][j][0] > 0 )
        {
          found = true;
          post += conditions[i][j][0] + ",";
        }
      }
      if( !found )
        post+= "0";
      else
        post = post.substring( 0, post.length - 1 );
    }

    if( map.getZoom() >= 4 )
    {
      pos = map.getCenter();
      zoomLevel = map.getZoom();

      loadingMarkers = true;
      getAjax( "/vessels/vessel_map_query_markers.php?lat=" + pos.lat() + "&lon=" + pos.lng() + "&zoom=" + zoomLevel + post, queryMarkerResponse );
    }
  }
}

function queryMarkerResponse( data )
{
  for( var key in markers )
  {
    markers[key].markForDeletion = true;
  }

  if( map.getZoom() > 4 )
    eval( data );

  for( var key in markers )
  {
    if( markers[key].markForDeletion )
    {
      markers[key].setMap( null );
      delete markers[key];
    }
  }

  loadingMarkers = false;
}

function infoWindowResponse( response )
{
  data = response.split( String.fromCharCode(0) );
  gid = data[0];
  windowContent = data[1];

  if( markers[gid] != undefined )
  {
    var infoWindow = new google.maps.InfoWindow({ content: windowContent } );
    infoWindow.open( map, markers[gid] );
  }
}

function navDataResponse( response )
{
  data = response.split( String.fromCharCode(0) );
  navPoint = data[0];
  windowContent = data[1];

  if( pathMarkers[navPoint] != undefined )
  {
    var infoWindow = new google.maps.InfoWindow({ content: windowContent } );
    infoWindow.open( map, pathMarkers[navPoint] );
  }
}

function mapZoomChanged( event )
{
  var zoomLevel = map.getZoom();

  if( zoomLevel >= 5 )
    disableHeatMap();
  else
    enableHeatMap();


  if( prevZoomLevel <= 4 && zoomLevel > 4 ) //Transitioning to more detail
  {
    //Add markers
    addMarkers();

    destroyRects();
  }
  else if( prevZoomLevel >= 5 && zoomLevel <= 4 ) //Transition to less detail
  {
    //Remove markers here
    destroyMarkers();

    if( rects.length == 0 )
    {
      createRects();
    }
  }


  prevZoomLevel = zoomLevel;
}

function loginRequired( url )
{
  showPopUp2("Login Required", '<div style="text-align:center;">You must login to access this vessel\'s page.<br /><br /><input type="button" class="button" onclick="document.location.href=\'' + decodeURIComponent(url) + '\';" value="Login" />&nbsp;&nbsp;<input type="button" class="button" onclick="closePopUp();" value="Cancel" /></div>' );
//  alert( url );
}
