<?
/*
Implements the vessel map, supports several modes:
Vessel Navigation history - Shows the path that the vessel has taken recently
Single Vessel - Shows only one vessel on the map
'Live Map' - Shows all vessels in a particular area

vessel_map.php may also be accessed directly, which requires the user to zoom in before seeing any vessels.
*/

require( "../inc/inc.php" );

$scripts[] = "/simpleajax.js";
$gid = intval( $_GET['gid'] );
$hideOthers = intval( $_GET['hideOthers'] );
$navPoint = intval( $_GET['navPoint'] );
$showTrack = intval( $_GET['showTrack'] );

if( $gid > 0 )
{
   
  $pos = queryArray( "select lat, lon, name from boats where gid='$gid'" );

  if( $pos['lat'] == 0.0 && $pos['lon'] == 0.0 )
    $gid = 0;
}


?>

<!DOCTYPE html>
<html>
  <head>
<?
  loadJS($scripts);
?>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <title>SaltHub</title>
    <link href="vessel_map.css" rel="stylesheet">
    <script src="https://maps.googleapis.com/maps/api/js?sensor=false&libraries=visualization"></script>
    <script>

      var map;
      var infoWindow;
      var rects = Array();
      var markers = Array();
      var shapes = Array();
      var prevZoomLevel = 3;
      var marker_icon = "/images/sailing-ship-icon-grey.png";
      var heatmap;
      var heatmapEnabled = true;
      var currentVesselPath = 0;
      var vesselPath = null;
      var pathMarkers = Array();
      var firstLoad = true;
      var loadingMarkers = false;

      function initialize() {
        <? if( $gid > 0 ) { ?>
          var myLatLng = new google.maps.LatLng(<?=$pos['lat']?>, <?=$pos['lon']?>);
          <? if( $showTrack ) { ?>
          var defaultZoom = 8;
          <? } else if( $hideOthers ) { ?>
          var defaultZoom = 9;
          <? } else { ?>
          var defaultZoom = 13;
         <? } } else { ?>
          var myLatLng = new google.maps.LatLng(5, 0);
          var defaultZoom = 2;
        <? } ?>

        var mapOptions = {
          zoom: defaultZoom,
          center: myLatLng,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        map = new google.maps.Map(document.getElementById('map_canvas'),
            mapOptions);

        <? if( $gid == 0 ) { ?>
        createHeatMap();
          createRects();
        <? } else { ?>
          //disableHeatMap();
          addMarkers();
        <? } ?>

        infowindow = new google.maps.InfoWindow();

        google.maps.event.addListener(map, 'dragend', mapCenterChanged );
        google.maps.event.addListener(map, 'zoom_changed', mapZoomChanged );
        google.maps.event.addListener(map, 'click', mapDoubleClick );
        google.maps.event.addListener(map, 'dblclick', mapDoubleClick );

      }

    function createRects()
    {
      <?
      $width  = 10;
      $height = 5;

      for( $x = 0; $x < (360); $x += $width )
        for( $y = 0; $y < (180); $y += $height )
        {
      ?>

      rect = new google.maps.LatLngBounds(
          new google.maps.LatLng(<?= $y - 90 ?>.0, <?= $x - 180; ?>.0),
          new google.maps.LatLng(<?= $y - 90 + $height ?>.0, <?= $x - 180 + $width; ?>.0)
        );

      rectObj = new google.maps.Rectangle({
        bounds: rect,
        //strokeColor: '#FF00FF',
        strokeOpacity: 0.07,
        strokeWeight: 1,
        //fillColor: '#FF00FF',
        fillOpacity: 0.05
      });

      rectObj.setMap(map);

      // Add a listener for the click event
      google.maps.event.addListener(rectObj, 'click', mouseClickRect);
      google.maps.event.addListener(rectObj, 'mouseover', mouseOverRect);
      google.maps.event.addListener(rectObj, 'mouseout', mouseOutRect);

      rects.push( rectObj );
      <?
        }
      ?>
    }


    function createHeatMap()
    {
      var heatmapData = [
<? // Changed by AppDragon
  //  $q = mysql_query( "SELECT x_cell, y_cell, COUNT(*) as weight FROM boats Where lat !=0 and lon != 0 GROUP BY x_cell, y_cell");
      $q = mysql_query( "SELECT x_cell, y_cell, COUNT(*) as weight FROM boats Where lat !=0 and lon != 0 GROUP BY x_cell, y_cell");//order by weight desc" );

      $W = 2.5;
      $H = 2.5;
      $min = 50;
      $max = 3000;
      $first = true;
      while( $r = mysql_fetch_array( $q ) )
      {
        if( !$first ) echo ",";
        $first = false;
        $heat = $r['weight'];

        if( $heat > $max ) $heat = $max;
        if( $heat < $min ) $heat = $min;
        $heat /= $max;

        $x = $r['x_cell'] * $W - 180;
        $y = $r['y_cell'] * $H - 90;

?>
        {location: new google.maps.LatLng(<?=$y?>, <?=$x?>), weight: <?=$heat;?>}
<?
      }
?>

      ];

      heatmap = new google.maps.visualization.HeatmapLayer({
        data: heatmapData,
        dissipating: false,
        opacity:0.5,
        maxIntensity : 1.0,
        radius: 4
      });
      heatmap.setMap(map);
    }

    function disableHeatMap()
    {
      heatmap.setOptions( {opacity:0} );
      heatmapEnabled = false;
    }

    function enableHeatMap()
    {
      heatmap.setOptions( {opacity:0.6} );
    }

    function mouseClickRect( event )
    {
      bounds = this.getBounds();
      map.panTo( bounds.getCenter() );
      map.setZoom( 9 );
      addMarkers();
    }

    function mouseOutRect( event )
    {
      this.setOptions( { fillOpacity: 0.05 } );
    }

    function mouseOverRect( event )
    {
      this.setOptions( { fillOpacity: 0.5 } );
    }

    function mapCenterChanged( event )
    {
      if( map.getZoom() > 4 )
        addMarkers();
    }

    function mapDoubleClick( event )
    {

      map.panTo( event.latLng );
      if( map.getZoom() < 5 )
      {
        map.setZoom( 5 );
      }
      else
        map.setZoom( map.getZoom() + 1 );
    }

    function destroyRects()
    {
      removed = rects.splice( 0, rects.length );

      for( c = 0; c < removed.length; c++ )
      {
        removed[c].setMap( null );
        delete removed[c];
      }
    }

    function destroyMarkers( )
    {
      for( var key in markers )
      {
        markers[key].setMap( null );
        delete markers[key];
      }

      for( var key in pathMarkers )
      {
        pathMarkers[key].setMap( null );
        delete pathMarkers[key];
      }

      if( vesselPath != null )
      {
        vesselPath.setMap( null );
        delete vesselPath;
      }

      markers = Array();
    }

    function addMarkers()
    {
      if( !loadingMarkers )
      {
        if( map.getZoom() > 4 )
        {
          pos = map.getCenter();
          zoomLevel = map.getZoom();

          //if( firstLoad )
          //  zoomLevel = 12; //We only want the ships close by for the first loading so the user can see something.

          loadingMarkers = true;
          getAjax( "/vessels/vessel_map_query_markers.php?lat=" + pos.lat() + "&lon=" + pos.lng() + "&gid=" + <?=$gid?> + "&zoom=" + zoomLevel + "&hideOthers=<?=$hideOthers;?>&navPoint=<?=$navPoint?>&showTrack=<?=$showTrack?>", queryMarkerResponse );
        }
      }
    }

	
    function queryMarkerResponse( data )
    {
	
      if( map.getZoom() > 4 )
        eval( data );

      <?
      // Check to see if this vessel hasn't reported for 3 days.
      if( $gid > 0 ) { ?>
        if( markers[ <?=$gid?> ] == undefined )
        {

          var symbol = {
            //fillColor: '#fff000',
            //strokeColor:'#000000',
            path:google.maps.SymbolPath.CIRCLE,
            scale: 7.0,
            strokeOpacity:1,
            fillOpacity:1,
            strokeWeight:0.5
            };

          markers[ <?=$gid?> ] = new google.maps.Marker({
            position: new google.maps.LatLng(<?=$pos['lat']?>,<?=$pos['lon']?>),
            map: map,
            icon: symbol,
            title:"<?=$pos['name']?>"
          });
          google.maps.event.addListener( markers[ <?=$gid?> ], 'click', function(){ getAjax( "vessel_map_info_window.php?gid=<?=$gid?>", infoWindowResponse); } );
        }
      <? } ?>

      loadingMarkers = false;
/*
      if( firstLoad )
      {
        <? if( $navPoint > 0 ) { ?>
          getAjax( "vessel_map_info_window.php?gid=<?=$gid?>&navPoint=<?=$navPoint;?>", navDataResponse);
        <? } ?>

        //Now load the remaining ships.
        if( map.getZoom() < 12 )
          getAjax( "/vessels/vessel_map_query_markers.php?lat=" + pos.lat() + "&lon=" + pos.lng() + "&gid=" + <?=$gid?> + "&zoom=" + map.getZoom() + "&hideOthers=<?=$hideOthers;?>&navPoint=<?=$navPoint?>&showTrack=<?=$showTrack?>", queryMarkerResponse );

        firstLoad = false;
      }
*/
    }

    function infoWindowResponse( response )
    {
      data = response.split( String.fromCharCode(0) );
      gid = data[0];
      windowContent = data[1];

      if( markers[gid] != undefined )
      {
        var infoWindow = new google.maps.InfoWindow({ content: windowContent } );
        infoWindow.open( map, markers[gid] );
      }
    }

    function navDataResponse( response )
    {
      data = response.split( String.fromCharCode(0) );
      navPoint = data[0];
      windowContent = data[1];

      if( pathMarkers[navPoint] != undefined )
      {
        var infoWindow = new google.maps.InfoWindow({ content: windowContent } );
        infoWindow.open( map, pathMarkers[navPoint] );
      }
    }

    function mapZoomChanged( event )
    {
      var zoomLevel = map.getZoom();

      if( zoomLevel >= 5 )
        disableHeatMap();
      //else
      //  enableHeatMap();


      if( prevZoomLevel <= 4 && zoomLevel > 4 ) //Transitioning to more detail
      {
        //Add markers
        addMarkers();

        destroyRects();
      }
      else if( prevZoomLevel >= 5 && zoomLevel <= 4 ) //Transition to less detail
      {
        //Remove markers here
        /*
        destroyMarkers();

        if( rects.length == 0 )
        {
          createRects();
        }
        */
      }


      prevZoomLevel = zoomLevel;
    }

    </script>
  </head>
  <body onload="initialize()">
    <div id="map_canvas"></div>
  </body>
</html>
