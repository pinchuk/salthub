<?
/*
Retreives a list of vessels to be shown on the map.  This is called from vessel_map.php and vessel_live_tracking.php

This script serves a number of search functions from both pages including:
Showing vessel navigation history
Showing one vessel
Showing all vessels in the area
Searching by key word, size, vessel type, and flag.
*/

require( "../inc/inc.php" );

if( $isDevServer )
{
  mysql_close();
  mysql_pconnect("10.179.38.20", "web","XXjkjsaklsd33ssdf");
  mysql_select_db("salthub");
}

//Support for search queries from vessel_live_tracking.php
$sql = "";

$num_conditions = 4; //Number of search conditions

for( $c = 0; $c <= $num_conditions; $c++ )
{
  if( $_POST['c' . $c] != "" && $_POST['c' . $c] != "0")
    $conds[$c] = explode( ",", $_POST['c' . $c] );
  else if( $_GET['c' . $c] != "" && $_GET['c' . $c] != "0")
    $conds[$c] = explode( ",", $_GET['c' . $c] );
}

for( $i = 0; $i < $num_conditions; $i++ )
{
  if( sizeof( $conds[$i] ) > 0 )
  {
    $sql .= " and (";
    for( $c = 0; $c < sizeof( $conds[$i] ); $c++ )
    {
      if( $c > 0 ) $sql .= " OR ";
      switch( $i )
      {
        case 0:
        {
          switch( $conds[$i][$c] -1  )
          {
            case 0: $sql .= "boats.length < 35"; break;
            case 1: $sql .= "(boats.length > 35 AND boats.length <= 65)"; break;
            case 2: $sql .= "(boats.length > 65 AND boats.length <= 90)"; break;
            case 3: $sql .= "(boats.length > 90 AND boats.length <= 130)"; break;
            case 4: $sql .= "(boats.length > 130 AND boats.length <= 120)"; break;
            case 5: $sql .= "(boats.length > 150 AND boats.length <= 180)"; break;
            case 6: $sql .= "(boats.length > 180 AND boats.length <= 200)"; break;
            case 7: $sql .= "(boats.length > 200 AND boats.length <= 250)"; break;
            case 8: $sql .= "(boats.length > 250 AND boats.length <= 300)"; break;
            case 9: $sql .= "(boats.length > 300 AND boats.length <= 400)"; break;
            case 10: $sql .= "(boats.length > 400 AND boats.length <= 600)"; break;
            case 11: $sql .= "(boats.length > 600 AND boats.length <= 1000)"; break;
            case 12: $sql .= "(boats.length > 1000)"; break;
          }
        }
        break;

        case 1:
        {
          $sql .= "pages.cat='" . $conds[$i][$c] . "'";
        }
        break;

        case 2:
        {
          $country = quickQuery( "select country from loc_country where id='" . $conds[$i][$c] . "'" );
          $sql .= "boats.flag like '%" . $country . "%'";
        }
        break;

        case 3:
        {
          switch( $conds[$i][0] )
          {
            case 0: $sql .= "(1=1)"; break;
            case 1: $sql .= "(boats.last_update > DATE_SUB( Now(), INTERVAL 10 MINUTE ))"; break;
            case 2: $sql .= "(boats.last_update > DATE_SUB( Now(), INTERVAL 30 MINUTE ))"; break;
            case 3: $sql .= "(boats.last_update > DATE_SUB( Now(), INTERVAL 1 HOUR ))"; break;
            case 4: $sql .= "(boats.last_update > DATE_SUB( Now(), INTERVAL 2 HOUR ))"; break;
            case 5: $sql .= "(boats.last_update > DATE_SUB( Now(), INTERVAL 4 HOUR ))"; break;
            case 6: $sql .= "(boats.last_update > DATE_SUB( Now(), INTERVAL 8 HOUR ))"; break;
            case 7: $sql .= "(boats.last_update > DATE_SUB( Now(), INTERVAL 12 HOUR ))"; break;
            case 8: $sql .= "(boats.last_update > DATE_SUB( Now(), INTERVAL 1 DAY ))"; break;
            case 9: $sql .= "(boats.last_update > DATE_SUB( Now(), INTERVAL 7 DAY ))"; break;

          }
        }
        break;
      }

    }
    $sql .= ")";
  }
}

if( isset( $_GET['kw'] ) && $_GET['kw'] != "" )
{
  $kw = addslashes( $_GET['kw'] );
  $sql .= " AND (gname like '%" . $kw . "%' OR boats.exnames like '%" . $kw . "%')";
}
//End seach support

// Changed by AppDragon
//$subquery = "select boats.gid from boats left join pages on pages.gid=boats.gid where lat != 0 and lon != 0 and last_update > DATE_SUB( Now(), INTERVAL 14 DAY )";
$subquery = "select boats.gid from boats as boats left join pages on pages.gid=boats.gid where lat != 0 and lon != 0 and last_update > DATE_SUB( Now(), INTERVAL 14 DAY )";

if( $sql != "" )
{
  $subquery .= $sql;
}

$lat = $_GET['lat'];
$lon = $_GET['lon'];
$focus = intval( $_GET['gid'] );
$zoom = intval( $_GET['zoom'] );
$hideOthers = (intval($_GET['hideOthers']) == 1);
$navPoint = intval( $_GET['navPoint'] );
$showTrack = (intval($_GET['showTrack']) == 1);

if( $showTrack ) $hideOthers = true;

if( $focus == 0 )
  $hideOthers = false;

switch( $zoom )
{
  case 8:
    $distance = 300;
  break;

  case 9:
    $distance = 120;
  break;

  case 10:
    $distance = 80;
  break;

  case 11:
  case 12:
    $distance = 50;
  case 13:
  case 14:
  case 15:
    $distance = 30;
  break;

  default:
    $distance = 500;
}

if( !$hideOthers )
{
// Changed by AppDragon
/*
  $q = mysql_query( "SELECT ((ACOS(SIN( $lat * PI() / 180) * SIN(boats.lat * PI() / 180) + COS($lat * PI() / 180) * COS(boats.lat * PI() / 180) * COS( ( $lon - lon) * PI() / 180)) * 180 / PI() ) * 60 * 1.1515) AS `distance`,
     boats.lat, boats.lon, boats.gid, pages.gname as name, boats.length, boats.navstat, boats.cog as heading, pages.cat, boats.sog, boats.destination, boats.last_update
    FROM ( $subquery )
    SEARCH_RESULTS, boats
    INNER JOIN pages on pages.gid=boats.gid
    where boats.gid=SEARCH_RESULTS.gid HAVING `distance`<=$distance" );    
*/    
  $q = mysql_query( "SELECT ((ACOS(SIN( $lat * PI() / 180) * SIN(boats.lat * PI() / 180) + COS($lat * PI() / 180) * COS(boats.lat * PI() / 180) * COS( ( $lon - lon) * PI() / 180)) * 180 / PI() ) * 60 * 1.1515) AS `distance`,
     boats.lat, boats.lon, boats.gid, pages.gname as name, boats.length, boats.navstat, boats.cog as heading, pages.cat, boats.sog, boats.destination, boats.last_update
    FROM ( $subquery )
    SEARCH_RESULTS, boats as boats
    INNER JOIN pages on pages.gid=boats.gid
    where boats.gid=SEARCH_RESULTS.gid HAVING `distance`<=$distance" );
  echo mysql_error();

  if( mysql_num_rows( $q ) > 800 )
  {    
    $subquery .= " and (navstat = 0 OR navstat = 8) and sog > 0.1";
// Changed by AppDragon
/*
    $q = mysql_query( "SELECT ((ACOS(SIN( $lat * PI() / 180) * SIN(boats.lat * PI() / 180) + COS($lat * PI() / 180) * COS(boats.lat * PI() / 180) * COS( ( $lon - lon) * PI() / 180)) * 180 / PI() ) * 60 * 1.1515) AS `distance`,
       boats.lat, boats.lon, boats.gid, pages.gname as name, boats.length, boats.navstat, boats.cog as heading, pages.cat, boats.sog, boats.destination, boats.last_update
      FROM ( $subquery )
      SEARCH_RESULTS, boats
      INNER JOIN pages on pages.gid=boats.gid
      where boats.gid=SEARCH_RESULTS.gid HAVING `distance`<=$distance" ); 
 */      
    $q = mysql_query( "SELECT ((ACOS(SIN( $lat * PI() / 180) * SIN(boats.lat * PI() / 180) + COS($lat * PI() / 180) * COS(boats.lat * PI() / 180) * COS( ( $lon - lon) * PI() / 180)) * 180 / PI() ) * 60 * 1.1515) AS `distance`,
       boats.lat, boats.lon, boats.gid, pages.gname as name, boats.length, boats.navstat, boats.cog as heading, pages.cat, boats.sog, boats.destination, boats.last_update
      FROM ( $subquery )
      SEARCH_RESULTS, boats as boats
      INNER JOIN pages on pages.gid=boats.gid
      where boats.gid=SEARCH_RESULTS.gid HAVING `distance`<=$distance" );
  }

}
else
{
// Changed by AppDragon
/*
  $q = mysql_query( "SELECT boats.lat, boats.lon, boats.gid, pages.gname as name, boats.length, boats.navstat, boats.heading, pages.cat, boats.sog, boats.destination, boats.last_update
    FROM boats INNER JOIN pages on pages.gid=boats.gid  where boats.gid=$focus" ); 
 */    
  $q = mysql_query( "SELECT boats.lat, boats.lon, boats.gid, pages.gname as name, boats.length, boats.navstat, boats.heading, pages.cat, boats.sog, boats.destination, boats.last_update
    FROM boats as boats INNER JOIN pages on pages.gid=boats.gid  where boats.gid=$focus" );
}

while( $r = mysql_fetch_array( $q ) )
{
  $gid = $r['gid'];
  $name = addslashes($r['name'] . " - Dest: " . ucfirst( strtolower( $r['destination'] ) ) . " - " . getTimeAgo( strtotime( $r['last_update'] ) ));

  //if( $r['navstat'] == 1 || $r['navstat'] == 8 )
  {
?>
  if( markers[ <?=$gid?> ] == undefined ) {
    a = new google.maps.LatLng( <?=$r['lat'] . "," . $r['lon']?> );

    symbol = {
      <?=getShape( $r['navstat'], $r['length'], $r['cat'], $r['heading'], $r['sog'], ($gid==$focus) )?>,
      fillOpacity:1,
<? if( $gid == $focus ) { ?>
      strokeWeight:0.7
<? } else { ?>
      strokeWeight:0.5
<? } ?>
      };

    markers[ <?=$gid?> ] = new google.maps.Marker({
      position: a,
      map: map,
      icon: symbol,
      title:"<?=$name?>"
    });
    google.maps.event.addListener( markers[ <?=$gid?> ], 'click', function(){ getAjax( "vessel_map_info_window.php?gid=<?=$gid?>", infoWindowResponse); } );
  }
  else
  { markers[ <?=$gid?> ].markForDeletion = false; }
<?
  }
}

if( isset( $focus ) && $focus > 0 && $showTrack )
{
  $current_pos = array( "lat" => 1000, "lon" => 0 );

  $q = mysql_query( "SELECT lat, lon, cog as heading, id FROM boats_ais_history WHERE gid=$focus and last_update > DATE_ADD( NOW(), INTERVAL -7 DAY ) order by last_update desc" );

  $foundPoints = array();
  $points = array();
  while( $r = mysql_fetch_array( $q ) )
  {
    $distanceToCurrentPos = abs($r['lat'] - $current_pos['lat']) + abs($r['lon'] - $current_pos['lon']);

    if( $distanceToCurrentPos > 0.01 || $r['id'] == $navPoint )
    {
      $points[] = array( $r['lat'], $r['lon'], $r['heading'], $r['id'] );
      $foundPoints[] = $r['id'];

      $current_pos['lat'] = $r['lat'];
      $current_pos['lon'] = $r['lon'];
    }
  }

  if( sizeof( $points ) > 1 )
  {
?>
    var vesselPathCoordinates = [
<?
    for( $c = 0; $c < sizeof( $points ); $c++ ) {
      if( $c > 0 ) echo ',';

?>
      new google.maps.LatLng(<?=$points[$c][0];?>,<?=$points[$c][1]?>)
<?
    }
?>
    ];

    if( currentVesselPath != <?=$focus?> )
    {
      vesselPath = new google.maps.Polyline( {
        path: vesselPathCoordinates,
        strokeColor: '#FF0000',
        strokeOpacity: 1.0,
        strokeWeight: 2
      });

      vesselPath.setMap( map );

      currentVesselPath = <?=$focus?>;

      //Clear previous path symbols
      for( var key in pathMarkers )
      {
        pathMarkers[key].setMap( null );
        delete pathMarkers[key];
      }

<?
    for( $c = 0; $c < sizeof( $points ); $c++ ) {
?>
        var symbol = {
          <?=getShape( 0, 100, 0, $points[$c][2], 0.2 )?>,
          fillOpacity:0,
          strokeWeight:0.5
          };

        pathMarkers[<?=$points[$c][3]?>] = new google.maps.Marker({
          position: new google.maps.LatLng(<?=$points[$c][0];?>,<?=$points[$c][1]?>),
          map: map,
          icon: symbol,
          title:"Recorded Position"
          });

       google.maps.event.addListener( pathMarkers[<?=$points[$c][3]?>], 'click', function(){ getAjax( "vessel_map_info_window.php?gid=<?=$focus?>&navPoint=<?=$points[$c][3];?>", navDataResponse); } );

<?
    }

    if( $navPoint > 0 && !in_array( $foundPoints, $navPoint ) )
    {
      //If the navPoint we're interested in is not shown on the map, we need to add it.
      $points = queryArray( "SELECT lat, lon, cog as heading, id FROM boats_ais_history WHERE id='$navPoint'" );
?>
        var symbol = {
          <?=getShape( 0, 100, 0, $points['heading'], 0.2 )?>,
          fillOpacity:0,
          strokeWeight:0.5
          };

        pathMarkers[<?=$points['id']?>] = new google.maps.Marker({
          position: new google.maps.LatLng(<?=$points['lat'];?>,<?=$points['lon']?>),
          map: map,
          icon: symbol,
          title:"Recorded Position"
          });

       google.maps.event.addListener( pathMarkers[<?=$points['id']?>], 'click', function(){ getAjax( "vessel_map_info_window.php?gid=<?=$focus?>&navPoint=<?=$points['id'];?>", navDataResponse); } );
<?
    }
?>
    }
<?
  }
}

function getShape( $navstat, $length, $type, $heading, $sog, $isFocus )
{
  $path = "";
  $scale = 1.2;
  $color = "#000000";
  $rotation = 0;

  if( $length > 120 )
    $scale = 1.5;
  if( $length > 300 )
    $scale = 2.0;

  if( $sog < 0.1 ) $navstat = 1;

  switch( $navstat )
  {
    case 0:
    case 8:
      $path = "'M 10 -10 M -10 10 M 0 2 L -3 4 L 0 -6 L 3 4 z'";

      if( $heading < 360 )
        $rotation = $heading;
    break;

    default:
      $path = 'google.maps.SymbolPath.CIRCLE';
      $scale *= 3.0;
  }

  switch( $type )
  {
    case 1219: // Passenger
    case 1221: // Yacht
    case 1222: // Sailing
      $color = "#00FF00";
    break;

    case 1215: // Cargo
    case 1216: // Fishing
    case 1223: // Search and Rescue
    case 1224: // Special Craft
    case 1225: // Tanker
    case 1226: // Work boat
      $color = "#FF0000";
    break;

    case 1217: // HSLC
    case 1228: // Wing in ground
      $color = "#0000FF";
    break;

    case 1227: // Unspecified
    default:
      $color = "#00FF00";
  }

  if( $isFocus )
    $color = "#ff00ff";

  echo "fillColor: '$color',strokeColor:'#000000',path:$path,scale: $scale,rotation:$rotation,strokeOpacity:1";
}

mysql_close();
?>