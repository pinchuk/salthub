<?php
/*
This page shows the location of all of the vessels that the user is associated with.

11/4/2012 - Created
*/

//Allow the FB crawler to see this site without logging in.
if( stristr( $_SERVER['HTTP_USER_AGENT'], "facebookexternal" ) !== false )
{
  $facebookCrawler = true;
}


include( "../inc/inc.php" );

$meta[] = array( "property" => "og:description", "content" => "Vessel tracking and positions show real-time regional views of the worlds global vessel traffic. View, track & recieve status updates of vessels from around the world." );
$meta[] = array( "property" => "og:title", "content" => "Live Vessel Tracking" );
$meta[] = array( "property" => "og:url", "content" => "http://" . SERVER_HOST . "/vessels/vessel_live_tracking.php" );
$meta[] = array( "property" => "og:image", "content" => "http://www.salthub.com/images/salt_badge100.png" );
$meta[] = array( "property" => "og:site_name", "content" => $siteName );
$meta[] = array( "property" => "og:type", "content" => "website" );
$meta[] = array( "property" => "fb:app_id", "content" => $fbAppId );
$title = "Live Vessel Tracking | SaltHub - Professional Maritime Network";
$descr = "Vessel tracking and positions show real-time regional views of the worlds global vessel traffic. View, track & recieve status updates of vessels from around the world.";

if( $facebookCrawler )
{
  $API->uid = 91;
}

$borderStyle = 1;

$css[] = "vessel_map.css";
$scripts[] = "https://maps.googleapis.com/maps/api/js?sensor=false&libraries=visualization";
$scripts[] = "vessel_my_vessels_map.js.php";

include( "../header.php" );
?>

<div style="width: 1200px;">
  <div style="float: left; <?=$borderStyle == 1 ? "border: 1px solid #d8dfea; border-top: 0;" : ""?>">
    <!-- Left panel -->
  	<div class="profileleft" style="<?=$borderStyle == 1 ? "border-left: 0; border-bottom: 0;" : ""?>" id="profileleft">
  		<div class="minheight" style="background-image: url(/images/bgprofilepic.png); background-position: 10px 0px; background-repeat: repeat-x;">&nbsp;</div>

  		<div class="leftcolumn_container">
  			<div class="profilepic" style="text-align: center;">
  				<div class="borderhider">&nbsp;</div>

          <div class="profilepic">
            <!--<img src="/images/vessel_dir.jpg" width="178" height="208"/>-->
            <img src="/images/radar.jpg" width="180" height="192" alt="" />
          </div>
  			</div>

        <? include( "vessel_my_vessels_side.php" ); ?>

  		</div>
  	</div>


    <!-- Main content -->
  	<div class="profilecontent_container" id="profilecontent_container" style="width: 760px; <?=$borderStyle == 0 ? "" : "border-right: 0;"?>">
  		<div class="minheight">&nbsp;</div>

  		<div class="profilecontent<?=$borderStyle == 0 ? "" : " profilecontent_minus1"?>">

        <div style="width: 950px; overflow-x: visible; position: relative; z-index:100;">    <!--563px-->
          <div class="phead">
          	<div class="username">
          		<div>
                  <a href="/employment">My Vessels</a>
          		</div>
          	</div>
              <div style="height:28px; background-image: url(/images/pix_d8dfea.png); background-position: 0 26px; background-repeat: repeat-x; width:760px; margin-left:-10px;"></div>
          </div>
        </div>


    		<div class="profilecontent<?=$borderStyle == 0 ? "" : " profilecontent_minus1"?>" style="width:750px;">

          <div style="padding-left: 10px; clear:both; width:550px; font-size:10pt; padding-top:3px;">
            <div style="float:left;"><div style="float:left; margin-top:1px;"><a href="/vessels/"><img src="/images/add.png" width="16" height="16"/></a></div>&nbsp;<a href="/vessels/">Track a New Vessel</a></div>
            <div style="float:left; margin-left:20px;"><div style="float:left; margin-top:1px;"><a href="/vessels"><img src="/images/anchor.png" width="16" height="16"/></a></div>&nbsp;<a href="/vessels">Vessel Directory</a></div>
          </div>

          <div style="clear:both;"></div>

          <div class="strong" style="margin-left:10px; margin-top:15px; font-size:12pt; margin-bottom:0px; padding-bottom:0px;">
            <div style="float:left; margin-right:5px;"><img src="/images/map_magnify.png" width="16" height="16" alt="" /></div>Live View of My Vessels
          </div>

          <div style="font-size:9pt; margin-left:10px; margin-right:8px; margin-top:5px;">
            <?=$siteName?> enables you to track your vessel's latest activity. The map below shows the last reported fix of the vessels you follow. You can add more vessels by using the Vessel Directory or by 'Connecting' to any Vessels page. You can also remove vessels by removing yourself from any vessels page.
          </div>
        </div>

        <div style="clear:both;"></div>

        <div class="subhead" style="width: 740px; margin-left: 5px; margin-top: 10px; margin-bottom:0px; margin-right:8px;">
          Vessels
        </div>

        <div style="padding-left:10px; width:735px; margin-top:10px; margin-bottom:10px; margin-left:5px; height: 735px;" id="map_canvas">
        </div>
  		</div>

  	  <div style="clear: both;"></div>
    </div>
  </div>

  <div style="float:left;">
    <!-- Advertisements -->
    <div style="width: 135px; float: left; position: relative;">
      <div style="position: absolute; top: 0; left: -3px; background: white; height: 26px;">&nbsp;</div>
      <? if( $API->adv ) { ?>
    	<div style="padding: <?=$site == "s" && $action == "about" ? 26 : 26?>px 0 0 15px;">
    		<?php showAd("skyscraper", 4); ?>
    	</div>
      <? } ?>
    </div>
  </div>

</div>
<?php
include "../footer.php";
?>
