<?
require( "../inc/inc.php" );
require( "vessel_functions.php" );

$defaultZoom = 2;
$forward_to_page = $_GET['vessel_forward'];
if( isset( $_GET['zoom'] ) ) $defaultZoom = intval( $_GET['zoom'] );
?>
var map;
var infoWindow;
var markers = Array();
var shapes = Array();
var loadingMarkers = false;

function initPage() {
  var myLatLng = new google.maps.LatLng(5, 0);
  var defaultZoom = <?=$defaultZoom?>;

  var mapOptions = {
    zoom: defaultZoom,
    center: myLatLng,
    disableDefaultUI:true,
    mapTypeId: google.maps.MapTypeId.TERRAIN
  };

  map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);

  infowindow = new google.maps.InfoWindow();

  google.maps.event.addListener(map, 'click', mapClick );
  addMarkers();
}

function mapClick( event )
{
  map.panTo( event.latLng );
  map.setZoom( map.getZoom() + 1 );
}

function mapCenterChanged( event )
{

}

function addMarkers()
{
<?
// Changed by AppDragon
/*
$sql = "SELECT boats.lat, boats.lon, boats.gid, pages.gname as name, boats.length, boats.navstat, boats.cog as heading, pages.cat, boats.sog, boats.destination, boats.last_update FROM boats
        LEFT JOIN pages ON pages.gid=boats.gid
        LEFT JOIN page_members ON page_members.gid=boats.gid
        WHERE page_members.uid=" . $API->uid . " AND pages.type=" . PAGE_TYPE_VESSEL;
*/
  $sql = "SELECT boats.lat, boats.lon, boats.gid, pages.gname as name, boats.length, boats.navstat, boats.cog as heading, pages.cat, boats.sog, boats.destination, boats.last_update FROM boats as boats
          LEFT JOIN pages ON pages.gid=boats.gid
          LEFT JOIN page_members ON page_members.gid=boats.gid
          WHERE page_members.uid=" . $API->uid . " AND pages.type=" . PAGE_TYPE_VESSEL;
  $q = mysql_query( $sql );

  while( $r = mysql_fetch_array( $q ) )
  {
    $gid = $r['gid'];
    $name = addslashes($r['name'] . " - Dest: " . ucfirst( strtolower( $r['destination'] ) ) . " - " . getTimeAgo( strtotime( $r['last_update'] ) ));
?>
  a = new google.maps.LatLng( <?=$r['lat'] . "," . $r['lon']?> );

  symbol = {
    <?=getShape( $r['navstat'], $r['length'], $r['cat'], $r['heading'], $r['sog'], false )?>,
    fillOpacity:1,
    strokeWeight:0.5
    };

  markers[ <?=$gid?> ] = new google.maps.Marker({
    position: a,
    map: map,
    icon: symbol,
    title:"<?=$name?>"
  });

<? if( $forward_to_page ==1 ) { ?>
  google.maps.event.addListener( markers[ <?=$gid?> ], 'click', function(){ document.location='/page/<?=$gid?>-fwd'; } );
<? } else { ?>
  google.maps.event.addListener( markers[ <?=$gid?> ], 'click', function(){ getAjax( "/vessels/vessel_map_info_window.php?gid=<?=$gid?>", infoWindowResponse); } );
<?
 }

  }
?>
}

function queryMarkerResponse( data )
{
  for( var key in markers )
  {
    markers[key].markForDeletion = true;
  }

  eval( data );

  for( var key in markers )
  {
    if( markers[key].markForDeletion )
    {
      markers[key].setMap( null );
      delete markers[key];
    }
  }

  loadingMarkers = false;
}

function infoWindowResponse( response )
{
  data = response.split( String.fromCharCode(0) );
  gid = data[0];
  windowContent = data[1];

  if( markers[gid] != undefined )
  {
    var infoWindow = new google.maps.InfoWindow({ content: windowContent } );
    infoWindow.open( map, markers[gid] );
  }
}

<?
mysql_close();
?>
