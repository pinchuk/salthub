<?
/*
This display a table of the user's vessels on the left side of the "My Vessels" page.
*/
include_once( "../inc/inc.php" );
require( "vessel_functions.php" );
?>

<table border="0" cellpadding="0" cellspacing="0" class="mediaactions" style="margin-top:5px">
	<tr>
		<td style="padding-right: 0px;">
      <iframe src="http://www.facebook.com/plugins/like.php?app_id=<? echo $fbAppId; ?>&amp;href=<? echo urlencode("http://" . SERVER_HOST . "/vessels/vessel_live_tracking.php" ); ?>&amp;send=false&amp;layout=button_count&amp;width=85&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:85px; height:21px; margin-top:3px;" allowTransparency="true"></iframe>
    </td>
		<td>
      <a href="http://twitter.com/share" class="twitter-share-button" data-url="http://bit.ly/Pz7Uux" data-text="SaltHub's Vessel Pages provides information on thousands of vessels. See vessel locations, ship particulars, photos, videos and more." data-count="horizontal" data-via="<?=SITE_VIA?>">Tweet</a>
    </td>
	</tr>
</table>

<?
// Changed by AppDragon
/*
$sql = "SELECT boats.lat, boats.lon, boats.gid, pages.gname as name, boats.length, boats.navstat, boats.cog as heading, pages.cat, boats.sog, boats.destination, boats.last_update FROM boats
        LEFT JOIN pages ON pages.gid=boats.gid
        LEFT JOIN page_members ON page_members.gid=boats.gid
        WHERE page_members.uid=" . $API->uid . " AND pages.type=" . PAGE_TYPE_VESSEL;
*/
$sql = "SELECT boats.lat, boats.lon, boats.gid, pages.gname as name, boats.length, boats.navstat, boats.cog as heading, pages.cat, boats.sog, boats.destination, boats.last_update FROM boats as boats
        LEFT JOIN pages ON pages.gid=boats.gid
        LEFT JOIN page_members ON page_members.gid=boats.gid
        WHERE page_members.uid=" . $API->uid . " AND pages.type=" . PAGE_TYPE_VESSEL;
$q = mysql_query( $sql );


if( mysql_num_rows( $q ) == 0 )
{
?>
  <div style="font-size:9pt; margin-top:10px; margin-bottom:5px;">
    You currently aren't connected to any vessels. To make use of this tool, please <a href="/vessels/">join one or more vessel pages</a> first.
  </div>
<?
}
else
{
?>
<div style="height:735px; overflow-y:auto; margin-bottom:10px;">

<div class="subhead" style="margin-top:10px; margin-bottom:5px;">
  Details of Vessels
</div>
<?
$c = 0;
while( $r = mysql_fetch_array( $q ) )
{
  $img = $API->getPageImage($r['gid']);
  if( stristr( $img, "images" ) === false )
    $img = $API->getThumbURL(0, 48, 48, $img);
  $url=$API->getPageUrl( $r['gid'] );
  $last_update = strtotime( $r['last_update'] );

  if( $c > 0 )
  {
?>
  <div style="margin-top:5px; margin-bottom:5px; border-top:1px dotted rgb(125,176,214); width:100%; height:1px;"></div>
<? } ?>
<div style="font-size:9pt;">
  <a href="<?=$url?>"><?=$r['name'];?></a>
</div>
<div style="font-size:9pt;">
    <a href="<?=$url?>"><img width="48" height="48" src="<?=$img?>" alt="" style="float:left; margin-right:3px; float:left;"/></a>

    <div style="margin-top:3px;">Status: <?=navstatToStr( $r['navstat'] );?></div>
    <div style="margin-top:3px;">SOG: <?=$r['sog'];?></div>
    <div style="margin-top:3px;">COG: <?=$r['heading'];?></div>
    <div style="margin-top:3px;">Destination: <?=$r['destination'];?></div>
    <div style="margin-top:3px;">Fix Received: <?= getTimeAgo( $last_update ); ?></div>

</div>
<?
  $c++;
}
?>
</div>
<?
}

?>
