<?
/*
This script renders the "Vessel Specifications" section of the about Vessel (page)

These fields should be converted to select boxes when editing

1.       Flags
2.       Builder
3.       Engine Builder
4.       Manager Owner     ??
5.       Class Society
6.       Designer
7.       Decorator
8.       Stylist
*/

$section_names = array(
  "General Information",
  "Historical Data",
  "Capacities",
  "Engine",
  "Contacts",
  "Dimensions",
  "Additional Information",
  "LoadLine"
);

$fields =
array(
  // General Information
  array(
    //Fields are:  Public Title, field name, gt_data field name, special handling (e.g., selects)
    array( "IMO Number", "imo", "", 0 ),
    array( "MMSI Code", "mmsi", "", 0 ),
    array( "Vessel Type", "cat", "", 1 ),
    array( "Gross Tonnage", "gt_data", "GROSS TONNAGE", 0 ),
    array( "Summer DWT", "gt_data", "Summer DWT", 0 ),
    array( "Build", "year", "", 0 ),
    array( "Builder", "gt_data", "BUILDER", 0 ),
    array( "Max. Speed", "speedmax", "", 0 ),
    array( 0,0,0,0 ), //Indicates next table
    array( "Cruise Speed", "speedcruise", "", 0 ),
    array( "Flag", "flag", "", 1 ),
    array( "Last Known Flag", "gt_data", "LAST KNOWN FLAG", 0 ),
    array( "Home Port", "gt_data", "HOME PORT", 0 ),
    array( "Manager / Owner", "owner", "", 0 ),
    array( "Class Society", "class_society", "", 1 ),
    array( "Insurer", "class_society", "", 0 )
  ),

  //Historical Data
  array(
    array( "Former Names", "exnames", "", 0 ),
    array( "Yard Number", "gt_data", "YARD NUMBER", 0 ), //??
    array( "Date of Order", "gt_data", "DATE OF ORDER", 0 ), //??
    array( 0,0,0,0 ), //Indicates next table
    array( "Keel Laid", "gt_data", "KEEL LAID", 0 ), //??
    array( "Build End", "gt_data", "BUILD END", 0 ), //??
    array( "Delivery Date", "gt_data", "DELIVERY DATE", 0 ) //??
  )
);


function showVesselSpecifications( $gid )
{ // Changed by AppDragon  
  //$q = mysql_query( "select * from boats where gid='$gid'" );    
  $q = mysql_query( "select * from boats where gid='$gid'" );

  if( mysql_num_rows( $q ) == 0 )
    return false;

  $r = mysql_fetch_array( $q );

  for( $a = 0; $a < sizeof( $fields ); $a++ )
  {
?>
  <div >
    <div style="float:left; margin-right:20px;">
      <?= $section_names[$a]; ?>
    </div>


  </div>
<?
  }

  return true;
}
?>