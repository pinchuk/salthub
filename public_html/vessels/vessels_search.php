<?
/*
Performs the vessel search and displays the results.  This is called from index.php.  Search parameters are entered from vessels_side.php
*/

include_once( "../inc/inc.php" );
include_once( "employment_functions.php" );
$num_conditions = 3;

//Reconstruct Query String for sorting and changing pages.
$query = "";
foreach ($_POST as $k => $v)
	if ($k != "p")
		$query .= "&" . htmlentities("$k=$v");
foreach ($_GET as $k => $v)
	if ($k != "p")
		$query .= "&" . htmlentities("$k=$v");



for( $c = 0; $c <= $num_conditions; $c++ )
{
  if( $_POST['c' . $c] != "" && $_POST['c' . $c] != "0")
    $conds[$c] = explode( ",", $_POST['c' . $c] );
  else if( $_GET['c' . $c] != "" && $_GET['c' . $c] != "0")
    $conds[$c] = explode( ",", $_GET['c' . $c] );
}

//Page searches
$uids = "";
$gids = $_POST['c4'];
if( $_POST['c4'] != "" && $_POST['c5'] ) $gids .= ",";
$gids .= $_POST['c5'];
$gids = addslashes($gids); //This shouldn't do anything, but should prevent a SQL injection.

$page = 0;
if( isset( $_REQUEST['p'] ) )
  $page = $_REQUEST['p'];
$results_per_page = 20;
$result_index = $page * $results_per_page;


if( sizeof( $conds[4] ) > 0  || sizeof( $conds[5] ) > 0 )
{
  $uids = "0";
  $sql = "select distinct uid from personal where gid in ($gids)";
  $q = sql_query( $sql );

  while( $r = mysql_fetch_array( $q ) )
  {
    $uids .= "," . $r['uid'];
  }
}

// Edit by AppDragon
//$sql = "select pages.gid, gname, pid from pages left join boats on boats.gid=pages.gid where type='" . PAGE_TYPE_VESSEL . "'";
$sql = "select pages.gid, gname, pid from pages inner join boats as boats on boats.gid=pages.gid where type='" . PAGE_TYPE_VESSEL . "'";


for( $i = 0; $i < 4; $i++ )
{
  if( sizeof( $conds[$i] ) > 0 )
  {
    $sql .= " and (";
    for( $c = 0; $c < sizeof( $conds[$i] ); $c++ )
    {
      if( $c > 0 ) $sql .= " OR ";
      switch( $i )
      {
        case 0:
        {
          switch( $conds[$i][$c] -1  )
          {
            case 0: $sql .= "boats.length < 35"; break;
            case 1: $sql .= "(boats.length > 35 AND boats.length <= 65)"; break;
            case 2: $sql .= "(boats.length > 65 AND boats.length <= 90)"; break;
            case 3: $sql .= "(boats.length > 90 AND boats.length <= 130)"; break;
            case 4: $sql .= "(boats.length > 130 AND boats.length <= 120)"; break;
            case 5: $sql .= "(boats.length > 150 AND boats.length <= 180)"; break;
            case 6: $sql .= "(boats.length > 180 AND boats.length <= 200)"; break;
            case 7: $sql .= "(boats.length > 200 AND boats.length <= 250)"; break;
            case 8: $sql .= "(boats.length > 250 AND boats.length <= 300)"; break;
            case 9: $sql .= "(boats.length > 300 AND boats.length <= 400)"; break;
            case 10: $sql .= "(boats.length > 400 AND boats.length <= 600)"; break;
            case 11: $sql .= "(boats.length > 600 AND boats.length <= 1000)"; break;
            case 12: $sql .= "(boats.length > 1000)"; break;
          }
        }
        break;

        case 1:
        {
          $sql .= "pages.cat='" . $conds[$i][$c] . "'";
        }
        break;

        case 2:
        {
          $country = quickQuery( "select country from loc_country where id='" . $conds[$i][$c] . "'" );
          $sql .= "boats.flag like '%" . $country . "%'";
        }
        break;
      }

    }
    $sql .= ")";
  }
}

if( isset( $_REQUEST['kw'] ) && $_REQUEST['kw'] != "" )
{
  $kw = addslashes( $_REQUEST['kw'] );
  $sql .= " AND (gname like '%" . $kw . "%' OR boats.exnames like '%" . $kw . "%')";
}

/* Added by AppDragon */
$count_sql = str_replace('pages.gid, gname, pid' , 'COUNT(*)', $sql);
$count_res = sql_query($count_sql);
$count_pages = mysql_fetch_row($count_res);
$pages = $count_pages[0] / $results_per_page;


/* Disabled by AppDragon
$q = sql_query( $sql );
$pages = mysql_num_rows( $q ) / $results_per_page;
*/

if( isset( $_GET['s'] ) )
{
  $sort = intval( $_GET['s'] );
  setcookie( "vessel_sort", $sort, time() + 2592000 );
}
elseif( isset( $_COOKIE['vessel_sort'] ) )
  $sort = intval( $_COOKIE['vessel_sort'] );
else
  $sort = 0;

switch( $sort )
{
  case 1:
    $sql .= " AND length > 0 order by length";
  break;

  default:
    $sort = 0;
    $sql .= " order by length desc, LTRIM(gname)";
  break;
}

$sql .= " limit $result_index, $results_per_page";
//echo $sql;

$q = sql_query( $sql );
echo mysql_error();

while( $r = mysql_fetch_array( $q ) )
  $results[] = $r;


?>

<div class="subhead" style="cursor:pointer; margin-top: 10px; margin-bottom:-10px; margin-right:8px;">
  <span>Vessels</span>
</div>

<div style="font-size:9pt; margin-top:13px;">
<div style="float:left; margin-right:3px;"><img src="/images/application_view_list.png" width="16" height="16" alt="" /></div>
<div style="float:left;">
Sort By:
<?
$sortMethods = array( "Name", "Length" );
for( $c = 0; $c < sizeof( $sortMethods ); $c++ )
{

  if( $sort == $c ) echo '<span style="font-weight:bold;">';
  else echo "<span>";
  echo '<a href="javascript:void(0);" onclick="javascript: changePage(' . $page . ', \'' . $query . '&s=' . $c . '\');">';
  echo $sortMethods[$c];
  echo "</a></span>&nbsp;&nbsp;&nbsp;";
}
?>
</div>
<div style="clear:both;">
<span style="font-size:8pt; margin-top:tpx;">The results below show vessels of known length. Try the site search if you're looking for a specific vessel.</span>
</div>
</div>

<div style="width:100%; margin-top:10px; clear:both;">
<?
$rowCount = -1;

if( mysql_num_rows( $q ) == 0 )
{
?>
  <div>Sorry, no results were found matching your criteria.</div>
<?
}
else
foreach( $results as $r )
{
  $rowCount++;
  if( $rowCount == 4 )
  {
    echo '<div style="clear:both"></div>';
    $rowCount = 0;
  }

  $info = $API->getPageInfo( $r['gid'], true );

  $pageUrl = $API->getPageURL( $r['gid'] );
  $len = quickQuery( "select length from boats where gid='" . $r['gid'] . "'" );
  $imo = quickQuery( "select imo from boats where gid='" . $r['gid'] . "'" );
  $type = quickQuery( "select catname from categories where cat='" . $info['cat'] . "'" );
  $country = quickQuery( "select loc_country.id from loc_country inner join boats as boats on loc_country.country=boats.flag where boats.gid='" . $r['gid'] . "'" );

?>
  <div class="jobs-profile" style="padding-left:0px; padding-right:0px; width:125px; height:208px">
    <div style="left:auto; right:auto; height:188px; padding-left:3px;">
      <? if( $info['pid'] == 0 ) { ?>
        <a href="<?=$info['url'];?>"><img border="0" src="/images/no_vessel_119.png" width="119" height="95"/></a>
      <? } else { ?>
        <a href="<?=$info['url'];?>"><img border="0" src="/img/119x95/<?=$info['profile_pic']; ?>" width="119" height="95"/></a>
      <? } ?>
      <div><a href="<?=$info['url']; ?>"><? echo $info['gname']; ?></a></div>
      <? if( $imo > 0 ) { ?><div>IMO: <?=$imo; ?> <? if( $country > 1 ) { ?><img src="/images/flags/<?=$country?>.png" width="16" height="11"><? } ?></div><? } ?>
      <? if( $type != "" ) { ?><div>Type: <?=$type?></div><? } ?>
      <? if( $len > 0 ) { ?><div>LOA: <?=$len?> ft. / <?=number_format( $len * 0.3048 );?> m.</div><? } ?>
    </div>

    <div style="background-color:#fff8cc; color:#555; text-align:center; height:22px; border-top:1px solid rgb(216,223,234); padding-top:4px; font-size:8.5pt; font-weight:bold;">
<?
$connected = quickQuery( "select count(*) from page_members where gid='" . $r['gid'] . "' and uid='" . $API->uid . "'" );
      if( !$connected ) {
?>
      <a href="javascript:void(0);" onclick="javascript:joinPage( <?=$r['gid'];?>,'side','B');">Connect With Vessel</a>
<? } else { ?>
      Connected
<? } ?>
    </div>
  </div>

<?
}
?>
</div>



<div class="pagination" style="clear:both;">
<?


if ($pages > 0)
{
	if ($page > 0)
		echo '<a href="javascript:void(0);" onclick="javascript: changePage(' . ($page - 1) . ', \'' . $query . '\');">&lt;</a>&nbsp; ';
	else
		echo "&lt;&nbsp; ";

	$i = $page - 3;
	if ($i < 0) $i = 0;
	$j = $i + 7;

	for (; $i < $pages && $i < $j; $i++)
		if ($i == $page)
			echo "<b>" . ($i + 1) . "</b>&nbsp; ";
		else
			echo '<a href="javascript:void(0);" onclick="javascript: changePage(' . $i . ', \'' . $query . '\');">' . ($i + 1) . '</a>&nbsp; ';

	if ($page < $pages - 1)
		echo '<a href="javascript:void(0);" onclick="javascript: changePage(' . ($page + 1) . ', \'' . $query . '\');">&gt;</a>&nbsp; ';
	else
		echo "&gt;&nbsp; ";
}
?>
</div>


