<?
/*
Enables the user to enter search parameters for the vessel search.  Used by index.php
*/

include_once( "../inc/inc.php" );

$div = array( "size", "type", "flag" );
$type = 0;


?>

<script language="javascript" type="text/javascript">
<!--
var conditions = new Array();

function condChangeVessel( type, id, name, value, div )
{

  if( type != '' )
  {
    if( !conditions[type] ) conditions[type] = new Array();

    conditions[type][id] = new Array();
    conditions[type][id][0] = value;

    if( value > 0 )
    {
      e = document.getElementById( div + "-all" );
      if( e )
        e.checked = false;

      conditions[type][id][1] = name;
    }
    e = document.getElementById( div + "-compact" );

    if( e )
      e.innerHTML = '';
  }
  else
    e = false;


  post = "a=0";

  kw1 = window.document.getElementById("kw");
  if( kw1 && kw1.value != "" )
    post += "&kw=" + encodeURIComponent( kw1.value );

  for( i = 0; i < conditions.length; i++ )
  {
    if( !conditions[i] ) continue;

    found = false;
    post += "&c" + i + "=";
    for( var j in conditions[i] )
    {
      if( conditions[i][j][0] > 0 )
      {
        found = true;
        post += conditions[i][j][0] + ",";
        if( e && i == type )
          e.innerHTML += '<a href="javascript:void(0);" title="\''+div+'-'+conditions[i][j][0]+'\'" onclick="javascript:e=document.getElementById(\''+div+'-'+conditions[i][j][0]+'\'); if( e ) e.checked=false; condChangeVessel(\''+i+'\', \''+j+'\', \''+conditions[i][j][1]+'\',0,\''+div+'\');">X</a> ' + conditions[i][j][1] + '<br />';
      }
    }
    if( !found )
      post+= "0";
    else
      post = post.substring( 0, post.length - 1 );
  }

  postAjax("/vessels/vessels_search.php", post, "responseHandler");
}

function uncheckAll( div, type )
{

  i = type;

  for( var j in conditions[i] )
  {
    e = document.getElementById( div + '-' + j );
    conditions[i][j][0] = 0;
    if( e )
    {
      e.checked = false;
    }

  }

  condChangeVessel( type, j, '', 0, div );
}

function responseHandler( data )
{
  e = document.getElementById( "searchResults" );
  if( e )
  {
    e.innerHTML = data;
  }
}

function swapDivs( div )
{
  e = document.getElementById( div );
  f = document.getElementById( div + "-compact" );

  if(e&&f)
  {
    if( e.style.display == 'none' )
    {
      toggleSlide( div );
      f.style.display = 'none';
    }
    else
    {
      toggleSlide( div + "-compact" );
      e.style.display = 'none';
    }
  }

<? for( $c = 0; $c < sizeof( $div ); $c++ ) { ?>
  if( div != '<? echo $div[$c] ?>' )
  {
    e = document.getElementById( '<? echo $div[$c] ?>');
    f = document.getElementById( "<? echo $div[$c] ?>-compact" );

    if(e&&f)
    {
      e.style.display='none';
      f.style.display='block';
    }
  }
<? } ?>
}
-->
</script>

<table border="0" cellpadding="0" cellspacing="0" class="mediaactions" style="margin-top:5px">
	<tr>
		<td style="padding-right: 20px; padding-top:5px; padding-left:10px;">
      <a type="button_count" href="javascript:void(0);" onclick="javascript: openPopupWindow( 'http://www.facebook.com/sharer/sharer.php?u=<? echo urlencode("http://" . SERVER_HOST . "/vessels/" ); ?>', 'Share', 550, 320 );">
        <img src="/images/facebook_share.png" width="58" height="20" alt="" />
      </a>
    </td>
		<td>
      <a href="http://twitter.com/share" class="twitter-share-button" data-url="http://bit.ly/Pz7Uux" data-text="SaltHub's Vessel Pages provides information on thousands of vessels. See vessel locations, ship particulars, photos, videos and more." data-count="horizontal" data-via="<?=SITE_VIA?>">Tweet</a>
    </td>
	</tr>
</table>

<div class="subhead" style="cursor:pointer; margin-top: 10px;">
	<div>Keyword</div>
</div>

<div style="margin-top:5px; padding-left:5px;">
  <input id="kw" size="20" style="width:120px;"/>
  <input type="button" class="button" value="Go" onclick="javascript: condChangeVessel('','','','','');" />
</div>

<div class="subhead" style="cursor:pointer; margin-top: 10px;" onclick="swapDivs('<?=$div[$type]?>');">
	<div style="float: left;">Size</div>
  <div style="float:right;">+</div>
	<div style="clear: both;"></div>
</div>

<div class="jobs-side" id="<?=$div[$type]?>" style="display:block; height:270px;">
  <div><input type="checkbox" id="<?=$div[$type]?>-all" onchange="javascript:uncheckAll('<?=$div[$type]?>', '<?=$type?>');"/>
    <a href="javascript:void(0);" onclick="javascript:uncheckAll('<?=$div[$type]?>', '<?=$type?>');">All</a>
  </div>
<?
$size = array( "0 - 35 ft (0 - 10m)", "36 - 65 ft (10 - 19m)", "66 - 90 ft (19 - 27m)", "91 - 130 ft (27 - 39m)", "131 - 150 ft (39 - 45m)", "151 - 180 ft (45 - 54m)", "181 - 200 ft (54 - 60m)", "201 - 250 ft (60 - 76m)", "251 - 300 ft (76 - 91m)", "301 - 400 ft (91 - 121m)", "401 - 600 ft (121 - 182m)", "601 - 1000 ft (182 - 304m)", "1000ft+ (304m+)" );

for( $c = 0; $c < sizeof( $size ); $c++ )
{
  $val = $c+1;
  $name = $size[$c];


?>
  <div><input type="checkbox" id="<?=$div[$type]?>-<?=$val?>" value="<? echo $val; ?>" onchange="javascript: condChangeVessel('<?=$type;?>', <?=$val?>, '<?=$name?>', this.checked ? this.value : 0,'<?=$div[$type]?>' );"/><a href="javascript:void(0);" onclick="javascript:e=document.getElementById('<?=$div[$type]?>-<?=$val?>'); if(e) { e.checked = !e.checked; condChangeVessel('<?=$type;?>', <?=$val?>, '<?=$name?>', e.checked ? e.value : 0,'<?=$div[$type]?>' ); }"><?=$name?></a></div>
<?
}
?>
</div>

<div class="jobs-side" id="<?=$div[$type]?>-compact" style="display:none;"></div>

<!-- Position -->
<?$type++; ?>
<div class="subhead" style="cursor:pointer; margin-top: 10px;" onclick="swapDivs('<?=$div[$type]?>');">
	<div style="float: left;">Type</div>
  <div style="float:right;">+</div>
	<div style="clear: both;"></div>
</div>


<div class="jobs-side" id="<?=$div[$type]?>" style="display:none; height:250px; ">
  <div><input type="checkbox" id="<?=$div[$type]?>-all" onchange="javascript:uncheckAll('<?=$div[$type]?>', '<?=$type?>');"/>
    <a href="javascript:void(0);" onclick="javascript:uncheckAll('<?=$div[$type]?>', '<?=$type?>');">All</a>
  </div>
<?
$q = sql_query( "select cat, catname from categories where industry=" . PAGE_TYPE_VESSEL);
while( $r = mysql_fetch_array( $q ) )
{
  $val = $r['cat'];
  $name = $r['catname'];
?>
  <div><input type="checkbox" id="<?=$div[$type]?>-<?=$val?>" value="<? echo $val; ?>" onchange="javascript: condChangeVessel('<?=$type;?>', <?=$val?>, '<?=$name?>', this.checked ? this.value : 0,'<?=$div[$type]?>' );"/><a href="javascript:void(0);" onclick="javascript:e=document.getElementById('<?=$div[$type]?>-<?=$val?>'); if(e) { e.checked = !e.checked; condChangeVessel('<?=$type;?>', <?=$val?>, '<?=$name?>', e.checked ? e.value : 0,'<?=$div[$type]?>' ); }"><?=$name?></a></div>
<?
}
?>
</div>


<div class="jobs-side" id="<?=$div[$type]?>-compact" style="display:none;"></div>

<!-- Nationality -->
<?$type++; ?>
<div class="subhead" style="cursor:pointer; margin-top: 10px;" onclick="swapDivs('<?=$div[$type]?>');">
	<div style="float: left;">Flag</div>
  <div style="float:right;">+</div>
	<div style="clear: both;"></div>
</div>

<div class="jobs-side" id="<?=$div[$type]?>" style="display:none; height:200px; overflow-y:scroll;">
  <div><input type="checkbox" id="<?=$div[$type]?>-all" onchange="javascript:uncheckAll('<?=$div[$type]?>', '<?=$type?>');"/>
    <a href="javascript:void(0);" onclick="javascript:uncheckAll('<?=$div[$type]?>', '<?=$type?>');">All</a>
  </div>
<?
$q = sql_query( "select * from loc_country order by priority desc,country" );
while( $r = mysql_fetch_array( $q ) )
{
  $val = $r['id'];
  $name = $r['country'];
?>
  <div><input type="checkbox" id="<?=$div[$type]?>-<?=$val?>" value="<? echo $val; ?>" onchange="javascript: condChangeVessel('<?=$type;?>', <?=$val?>, '<?=addslashes($name);?>', this.checked ? this.value : 0,'<?=$div[$type]?>' );"/><a href="javascript:void(0);" onclick="javascript:e=document.getElementById('<?=$div[$type]?>-<?=$val?>'); if(e) { e.checked = !e.checked; condChangeVessel('<?=$type;?>', <?=$val?>, '<?=addslashes($name);?>', e.checked ? e.value : 0,'<?=$div[$type]?>' ); }"><img src="/images/flags/<?=$val?>.png" width="16" height="11"> <?=$name?></a></div>
<?
}
?>
</div>

<div class="jobs-side" id="<?=$div[$type]?>-compact" style="display:none;"></div>


<script language="javascript" type="text/javascript">
<!--

  conditions[0] = new Array();
  conditions[1] = new Array();

  conditions[0][6] = new Array();
  conditions[0][6][0] = 6;
  conditions[0][6][1] = "<?=$size[5]?>";

  conditions[1][1221] = new Array();
  conditions[1][1221][0] = 1221;
  conditions[1][1221][1] = "Yacht";

  conditions[1][1225] = new Array();
  conditions[1][1225][0] = 1225;
  conditions[1][1225][1] = "Tanker";

  document.getElementById("size-6").checked = true;
  document.getElementById("type-1225").checked = true;
  document.getElementById("type-1221").checked = true;


  condChangeVessel('','','','','');

  e = window.document.getElementById( "size-compact" );
  if( e )
  {
    e.innerHTML = '';

    div = "size";
    val = 6;
    name = "<?=$size[5]?>";
    e.innerHTML += '<a href="javascript:void(0);" title="\''+div+'-'+val+'\'" onclick="javascript:e=document.getElementById(\''+div+'-'+ val +'\'); if( e ) e.checked=false; condChangeVessel(\''+0+'\', \''+val+'\', \''+ name +'\',0,\''+div+'\');">X</a> ' + name + '<br />';
  }

  e = window.document.getElementById( "type-compact" );
  if( e )
  {
    e.innerHTML = '';

    div = "type";
    val = 1221;
    name = "Yacht";
    e.innerHTML += '<a href="javascript:void(0);" title="\''+div+'-'+val+'\'" onclick="javascript:e=document.getElementById(\''+div+'-'+ val +'\'); if( e ) e.checked=false; condChangeVessel(\''+1+'\', \''+val+'\', \''+ name +'\',0,\''+div+'\');">X</a> ' + name + '<br />';

    val = 1225;
    name = "Tanker";
    e.innerHTML += '<a href="javascript:void(0);" title="\''+div+'-'+val+'\'" onclick="javascript:e=document.getElementById(\''+div+'-'+ val +'\'); if( e ) e.checked=false; condChangeVessel(\''+1+'\', \''+val+'\', \''+ name +'\',0,\''+div+'\');">X</a> ' + name + '<br />';
  }


  swapDivs( "type" );
  swapDivs( "size" );

-->
</script>
