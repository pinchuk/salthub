<?php
/*
Adds a new wall post to the database.  Called from /inc/tweetbox.php

Change Log
10/27/2011 - Removed post-to-wall permission setting from facebook and twitter, only using checkbox under upload window now.
11/11/2012 - Changed to use session variables to determine if we're going to post to twitter and/or facebook
*/

include_once "inc/inc.php";

if (!$API->isLoggedIn()) die();

$to = intval($_POST['target']);
$gid = intval($_POST['gid']);

$fbtarget = null;
if( isset( $_POST['fbtarget'] ) )
{
  $fbtarget = $_POST['fbtarget'];
  $_SESSION['fbpages']['selection'] = $fbtarget;
  if( $fbtarget == 0 )
    $fbtarget = null;
}

if ($gid > 0)
	$to = 0;
elseif ($to == 0)
	$to = $API->uid;

$loglink = null;

$post = $API->convertChars($_POST['post']);

// check to see if a url was posted with the image/title/description
if (isset($_POST['loglink_descr']))
{
	include 'inc/simpleimage.php';
	
	$wp_data = array();
	
	if (!empty($_POST['loglink_img']))
	{
		// has an image
		
		$image = new SimpleImage($_POST['loglink_img']);
		
		if ($image->get_width() >= 370)
		{
			$wp_data['big_image'] = true;
			$tmp_file = 'wp_' . md5(microtime()) . '_l.jpg'; // "large"
		}
		else
			$tmp_file = 'wp_' . md5(microtime()) . '_s.jpg'; // "small"
		
		if (strpos($_POST['loglink_img'], $API->getContainer()))
		{
			// image already uploaded to the cloud (custom link image)
			$wp_data['image'] = $_POST['loglink_img'];
			
			// check to see if it was a temporary image (while being uploaded to cloud)
			$i = strpos($wp_data['image'], 'tmp_image.php?u=');
			if ($i > 0)
				$wp_data['image'] = substr($_POST['loglink_img'], $i + 16);
		}
		else
		{
			// we need to upload from the remote server
			if ($wp_data['big_image'])
				$image->fit_to_width(370);
			else
				$image->best_fit(119, 95);
			
			$image->save("/tmp/$tmp_file", 80);
			
			$wp_data['image'] = $API->getContainer() . '/' . $tmp_file;
			
			//fork and upload to cloud
			$params = array('image' => $tmp_file, 'uid' => $API->uid);
			$cmd = "/usr/bin/php " . $_SERVER['DOCUMENT_ROOT'] . "/upload/upload_temp_to_cloud.php " . base64_encode(json_encode($params)) . " > /dev/null 2>&1 &";
			exec($cmd, $out);
		}
	}
	
	$_POST['loglink_link'] = addhttp($_POST['loglink_link']);
	
	foreach (array('descr', 'link', 'title') as $key)
		$wp_data[$key] = $_POST['loglink_' . $key];
	
	$wp_data['post'] = $post;
	$post = json_encode($wp_data);
}

sql_query("insert into wallposts (uid,uid_to,post) values (" . $API->uid . ",$to,'" . addslashes($post) . $loglink . "')");
/*
if(isset($_POST['loglink_descr']))
{
    $sizeImage = getimagesize($_POST['loglink_img']);
    $widthImage = $sizeImage[0];
    $size = '';
    if($widthImage > 370)
    {
        $size = 'bigImage';
    }
    require_once('dadclass/view.php');
    $loglink = View::get(
        'wallpost/loglink',
        array(
            'descr' => $_POST['loglink_descr'],
            'link'  => $_POST['loglink_link'],
            'title' => $_POST['loglink_title'],
            'img'   => $_POST['loglink_img'],
            'size'  => $size
        )
    );
}
$loglink = addslashes($loglink);
sql_query("insert into wallposts (uid,uid_to,post) values (" . $API->uid . ",$to,'" . addslashes( $_POST['post'] ) . $loglink . "')");
*/

$id = mysql_insert_id();

$from = null;
if( $gid > 0 )
{
  $postAsPage = quickQuery( "select postAsPage from page_members where admin=1 and gid=$gid and uid=" . $API->uid );

  if( $postAsPage == 1 )
  {
    $to = -1;
    $from = -1;
  }

  $API->sendNotificationToPage( NOTIFY_GROUP_COMMENT, array( "from" => ($from == -1 ? 0 : $API->uid), "gid" => $gid ) );


  $jmp = quickQuery( "select jmp from pages where gid='" . $gid . "'" );
  if( $jmp == "" )
  {
    $jmp = shortenURL( "http://" . SERVER_HOST . $API->getPageURL($gid) . "/logbook");
    sql_query( "update pages set jmp='$jmp' where gid='" . $gid . "'" );
  }


}
else
{
//  $uname = quickQuery( "select username from users where uid='" . $API->uid . "'" );
  $jmp = quickQuery( "select jmp from users where uid='" . $API->uid . "'" );
  if( $jmp == "" )
  {
    $jmp = shortenURL( "http://" . SERVER_HOST . $API->getProfileURL($API->uid) . "/logbook");
    sql_query( "update users set jmp='$jmp' where uid='" . $API->uid . "'" );
  }
}


$fid = $API->feedAdd("W", $id, $to, $from, $gid);

$url = getFeedJMPLink( $fid );

$msg = $_POST['post'];


if( strlen($msg) < (130 - strlen(SITE_VIA)) ) //Check to see if we have room for "via XXXX"
  $twmsg .= $msg . " via @" . SITE_VIA;
else
{
  $twmsg = substr( $msg, 0, (110 - strlen(SITE_VIA)) );
  $twmsg .= " $url via @" . SITE_VIA;
}

$msg .= " $url via $siteName";

//Checking if we're going to post to twitter is determined in twUpdateStatus
$API->twUpdateStatus(null, substr($twmsg, 0, 139), true);

if( !isset( $_SESSION['postas'][SM_FACEBOOK][0] )
      || $_SESSION['postas'][SM_FACEBOOK][0] > 0 )
{
  if( !$API->fbStreamPublish(null, $msg, null, $fbtarget) )
  {
    $err = "FB";
  }
}

if (isset($err))
	$ret = array('success' => false, 'error' => $err, 'fid' => $fid);
else
	$ret = array('success' => true, 'fid' => $fid, 'tmp_image' => $tmp_file);

echo json_encode($ret);

?>