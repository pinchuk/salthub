<?php

$standalone = empty($script);

if ($standalone) // called as a standalone page
{
	include "inc/inc.php";
	?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
	<link rel="stylesheet" href="/style.css" type="text/css" />
	<title>mediaBirdy - tweet more efficiently, using media!</title>

</head>

<body style="background: #fff; margin: 10px;">
	<?php
}

?>

<div class="tabcontent" id="tabcontent2"<?=$standalone ? " style=\"border: 0; display: inline;\"" : ""?>>
	<div class="bigtext" style="padding: 10px 10px 0 10px;">
		Why should you join mediaBirdy?
	</div>
	<ul style="padding-right: 5px;">
		<li>
			The mediaBirdy team asks, &quot;<span class="smtitle">What are you seeing right now</span>,&quot; and allows you to <span class="smtitle">share <u>it</u></span> around the planet, immediately.
			<div class="whyicons"><img src="/images/globe.png" alt="" /></div>
		</li>
		<li>
			The platform is based on the idea that you should be able to &quot;<span class="smtitle">post-once, connect-to-all</span>.&quot;&nbsp; It provides a utility to spontaneously and continually connect its users across various social sites using media.
			<div class="whyicons"><img src="/images/television.png" alt="" /><img src="/images/photos.png" alt="" /></div>
		</li>
		<li>
			By using mediaBirdy, users not only have one more place to be heard and discovered, they also have a central staging area for all their content.&nbsp; Using mediaBirdy makes updating your social networks, whether it be <span class="smtitle">business or personal</span>, easy!
			<div class="whyicons"><img src="/images/f.png" alt="" /><img src="/images/twitter.png" alt="" /><img src="/images/myspace.png" alt="" /><img src="/images/youtube.png" alt="" /></div>
		</li>
		<li>
			The mediaBirdy team has also designed your profile on mediaBirdy to be your <span class="smtitle">social business card</span>, thus allowing new customers, fans, and friends to discover and interact with all your social sites from one place.
			<div class="whyicons"><img src="/images/vcard.png" alt="" /></div>
		</li>
		All this and so much more is &quot;<span class="smtitle">why you should join!</span>&quot;<?php if (!$API->isLoggedIn()) { ?>&nbsp; <a href="javascript:getStarted();">Sign in</a> and give it a try.<?php } ?></li>
	</ul>
</div>

<?php

if ($standalone) // called as a standalone page
	echo "</body></html>";

?>