SELECT  SQL_NO_CACHE

    SEARCH_RESULTS.*,
    
    IFNULL(photos.ptitle, albums.title) as title,    
    IFNULL(photos.pdescr, albums.descr) as descr,
    
    albums.id,
    photos.id,
    photos.created
    

FROM

(

    SELECT SQL_NO_CACHE
        MEDIA.id
        
    FROM
        
        albums AS ALBUMS
        
        INNER JOIN
       
            photos AS MEDIA
                
        ON
            
            MEDIA.aid = ALBUMS.id
            AND
            MEDIA.uid != 832 
            AND
            MEDIA.privacy=0 
            AND
            MEDIA.reported=0 
        
     
        INNER JOIN
        
            (
                SELECT SQL_NO_CACHE
                    MEDIA.id
                    
                FROM
                    
                    photos AS MEDIA

                WHERE
                    MEDIA.ptitle LIKE '%gray%'
            
            ) MEDIA_TEXT_SEARCH
            
            
        ON
            MEDIA.id = MEDIA_TEXT_SEARCH.id
            
        /* stack the additional search constraints here */
        /* LEFT JOIN FOR THE OR PART */
        
        INNER JOIN
        
            (
                SELECT SQL_NO_CACHE
                    ALBUMS.id
                    
                FROM
                    
                    albums AS ALBUMS

                WHERE
                    ALBUMS.title LIKE '%gray%'
                    OR
                    ALBUMS.descr LIKE '%gray%'
            
            ) ALBUM_TEXT_SEARCH
            
            
        ON
            MEDIA.aid = ALBUM_TEXT_SEARCH.id
            
        
    
) SEARCH_RESULTS,

photos,
albums
 
 
WHERE
    photos.id = SEARCH_RESULTS.id
    AND
    albums.id = photos.aid
    
 
ORDER BY 
    photos.views DESC


LIMIT 0, 12
