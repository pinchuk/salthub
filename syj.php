<?php

// get categories
echo "Retrieving category list ... ";
$html = file_get_contents("http://www.superyachtjobs.com/companydirectory.asp");
$tmp = explode('<a href="directory.asp?a=', $html);

echo "done.\n";

foreach ($tmp as $x)
{
	$i = strpos($x, '"');
	
	if ($i > 5) continue;
	
	$id = substr($x, 0, $i);
	$j = strpos($x, "<");
	$name = substr($x, $i + 2, $j - $i - 2);
	
	$cats[$id] = $name;
}

echo "Found " . count($cats) . " categories.\n";

// get directory for each category

foreach ($cats as $catid => $catname)
{
	for ($catpage = 1, $foundcat = true; $foundcat == true; $catpage++)
	{
		$foundcat = false;
		
		echo "Getting directory for category #{$catid} - page #{$catpage} - ({$catname}) ... ";
		
		$html = file_get_contents("http://www.superyachtjobs.com/directory.asp?a={$catid}&p={$catpage}");
		$tmp = explode('<table id="companyid', $html);
		
		echo "done.\n";
		
		foreach ($tmp as $x)
		{
			// get each company info
			
			$i = strpos($x, '<td class="companyname">');
			if ($i === false) continue;		
			
			$foundcat = true;
			
			$j = strpos($x, '</td>');
			$name = trim(substr($x, $i + 24, $j - $i - 24));
			
			$x = substr($x, $j + 5);
			$i = strpos($x, '<td>');
			$j = strpos($x, '</td>', $i);
			$city = trim(substr($x, $i + 4, $j - $i - 4));
			
			$x = substr($x, $j + 5);
			$i = strpos($x, '<td>');
			$j = strpos($x, '</td>');
			$country = trim(substr($x, $i + 4, $j - $i - 4));
			
			$i = strpos($x, 'CompanyDetails.asp?coid=');
			$j = strpos($x, '"', $i);
			$id = trim(substr($x, $i + 24, $j - $i - 24));
			
			$logo = "http://www.synfo.com/Directories/isadmin/images/CompanyLogos/{$id}x300.jpg";
			
			echo "- Getting information for company #{$id} ($name) ... ";
			
			$html = file_get_contents("http://www.superyachtjobs.com/CompanyDetails.asp?coid=$id");
			
			echo "done.\n";
			
			if (strpos($html, $logo) === false)
				unset($logo);
			
			$companies[$id] = array("catid" => $catid, "name" => $name, "city" => $city, "country" => $country, "logo" => $logo);
			
			$i = strpos($html, '<td width="470">');
			$j = strpos($html, '<td style="width:265px;">');
			$tmp = explode("<br/>", strip_tags(substr($html, $i, $j - $i), "<br/>"));
			
			for ($i = 0; $i < count($tmp); $i++)
			{
				$line = trim($tmp[$i]);
				
				if (strpos($line, ": ") === false)
					$companies[$id]['address'] .= "\n" . trim($line);
				else
				{
					$x = explode(": ", $line);
					$field = trim(strtolower($x[0]));
					if (!empty($field))
						$companies[$id][$field] = trim($x[1]);
				}
			}
			
			$companies[$id]['address'] = str_replace("\n", "|", trim($companies[$id]['address']));
		}
	}
}

file_put_contents("/tmp/parsed", var_export($cats, true) . "\n\n" . var_export($companies, true));

echo "All done.  Output saved to /tmp/parsed\n\n";

?>