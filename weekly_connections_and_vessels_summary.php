<?php
/*
Sends out the weekly vessels and connections email.

Users will not receive an email if there is nothing new that occurred with theri vessels or connections within the last week.
*/


$_SERVER['DOCUMENT_ROOT'] = "/var/www/salthub.com/public_html";
include $_SERVER['DOCUMENT_ROOT'] . "/inc/inc.php";
error_reporting(ERROR_SET_1);
include $_SERVER['DOCUMENT_ROOT'] . "/inc/feed_functions.php";

$site = "s";
$_SERVER['HTTP_HOST'] = "www." . $siteName . ".com";

$url = "http://www.salthub.com";
$dateRange = date( "M j", time() - 604800  ) . " - " . date( "M j");
$startTime = date( "Y-m-d", time() - 604800 );

$fromEmail = true; //Used for resolving feed items.

//$x = mysql_query("select * from users where email in ('tweider@pelorusholdings.com','mike.sh@oerinet.net') and active=1");
$x = mysql_query("select * from users where active=1");
while ($r = mysql_fetch_array($x, MYSQL_ASSOC))
{
  $API->uid = $r['uid'];

  if( $r['email'] == "" ) continue; //No email address?  Skip this user.

  if( !$API->isNotificationEnabled(  $r['uid'], NOTIFY_PREF_WEEKLY_CONNECTIONS_SUMMARY ) ) continue; //Emails turned off?  Skip this user.

  foreach (array_keys($_SESSION) as $k)
  	unset($_SESSION[$k]);

  foreach ($r as $k => $v)
  	$_SESSION[$k] = $v;

	$API->name = $_SESSION['name'];
	$API->pic = $_SESSION['pic'];
	$API->username = $_SESSION['username'];

  //Load blocked uids
  $blocked_uids = array();
  $q2 = mysql_query( "select blocked_uid from blocked_users where uid='" . $API->uid . "'" );
  while( $r2 = mysql_fetch_array( $q2 ) )
    $blocked_uids[] = $r2['blocked_uid'];

  //Load friends list
  $friends = $API->getFriendsUids();
    if (empty($friends)){
        $friends = 0;
    }
    else{
        $friends = implode( $friends, "," );
    }

  $name = $r['name'];
  $name = strtok( $name, " " );

  $body = "<div style=\"font-family:Arial;\">Hi " . $name . ",<br /><br />Please find some of your Network Updates, $dateRange <a style=\"text-decoration: none; color: #326798;\" href=\"$url\">view all updates</a><br /><br />";
  $body .= '[ORANGEBOX]<b>To view all updates, follow this link to your account or log in with Facebook or Twitter. <a style="color:#326798;" href="http://www.salthub.com">http://www.SaltHub.com</a></b>[/ORANGEBOX]<br />';

  $somethingToShow = false;

  $temp = getProfileUpdates();
  if( $temp )
  {
    $body .= tableHeader( "Profile Updates", "" );
    $body .= $temp;
    $body .= tableFooter();
    $somethingToShow = true;
  }

  $temp = getVesselUpdates();
  if( $temp )
  {
    $body .= tableHeader( "Vessel Updates", "" );
    $body .= $temp;
    $body .= tableFooter();
    $somethingToShow = true;
  }

  $temp = getConnectionUpdates();
  if( $temp )
  {
    $body .= tableHeader( "Connection Updates", "Tip: To make networking easy, view your connections to find people that matter most." );
    $body .= $temp;
    $body .= tableFooter();
    $somethingToShow = true;
  }

  $temp = getPostUpdates();
  if( $temp )
  {
    $body .= tableHeader( "Updated Posts", "" );
    $body .= $temp;
    $body .= tableFooter();
    $somethingToShow = true;
  }

  $body .= "<br />About SaltHub.com: SaltHub is a network for the maritime industry that provides access to resources, vessels, media, news and tools. Login to SaltHub and connect with professionals, businesses and explore new opportunities.<br /><br />Thanks,<br />The SaltHub Team<br /></div>";

  //echo $body;

  if( $somethingToShow )
  {
	if ($argv[1])
		echo "Sending email to: " . $r['name'] . " (" . $r['email'] . ")\n";
	else
    {
		emailAddress( $r['email'], "Your Vessel & Network Updates, " . $dateRange, $body );
    }
  }
}

function getProfileUpdates()
{
  global $API;
  global $friends;
  global $url;
  global $startTime;

  $resultsToShow = 8;

    $sql = "select SQL_CALC_FOUND_ROWS * from feed where uid IN (" . $friends . ") and (type like 'PR%' OR type='o') and ts>='$startTime' order by rand() limit $resultsToShow";

    $q = mysql_query( $sql );

  if( mysql_num_rows( $q ) )
  {
    $body = "";
    $totalRows = quickQuery( "select FOUND_ROWS()" );

    while( $r = mysql_fetch_array( $q ) )
    {
      $user = $API->getUserInfo( $r['uid_by'], "name,pic,uid" );
      $userURL = $url . $API->getProfileURL( $r['uid_by'] );

      $photoURL = $url . '/img/48x48' . $API->getUserPic( $r['uid_by'], $user['pic'] );
      if( $photoURL == $url."/img/48x48/images/nouser.jpg" ) $photoURL = $url."/images/nouser.jpg";

      $resolved = resolveFeed( $r );

/*
      if( $r['type'] == "L" )
      {
        $descr = "";
        $owner = 0;
        $owner_gid = 0;

        $word = typeToWord($resolved[1]['type']);

        switch( $resolved[1]['type'] )
        {
          case "P":
          case "V":
      			$descr = ($likes ? "Likes" : "Dislikes") . " {OWNER}'s <a href=\"" . $API->getMediaURL($resolved[1]['type'],$resolved[1]['id']) . "\">$word";
            $owner = quickQuery( "select uid from " . $word . "s where id='" . $resolved[1]['id'] . "'" );
          break;
          case "W":
    			  $descr = ($likes ? "Likes" : "Dislikes") . " {OWNER}'s $word";
            $owner = $result['resolved']['uid'];
          break;
          case "rss":
      			$descr = ($likes ? "Likes" : "Dislikes") . " {OWNER}'s $word";
            $owner_gid = $result['resolved']['gid'];
          break;
        }

        if( $owner > 0 )
        {
          $name = quickQuery( "select name from users where uid='" . $owner . "'" );
        } else {
          $name = quickQuery( "select gname from pages where gid='" . $owner_gid . "'" );
        }

        $descr = str_replace( "{OWNER}", $name, $descr );

        $resolved[1]['descr'] = $descr;
      }
*/

      if( $resolved[1]['descr'] == "" ) continue;

      $resolved[1]['descr'] = str_replace( 'href="', 'style="text-decoration:none; color: #326798;" href="' . $url, $resolved[1]['descr'] );

      $body .= '<tr><td>
        <table width="600" cellpadding="3" cellspacing="0" style="font-family:Arial; border-bottom: 1px solid #d8dfea; font-size:9pt;">
        <tr>
          <td width="50">';

      $body .= '<a style="text-decoration: none; color: #326798;" href="' . $userURL . '"><img src="' . $photoURL . '" width="48" height="48"></a></td>';
      $body .= '<td width="100"><a style="text-decoration: none; color:#326798;" href="' . $userURL . '/message">' . $user['name'] . '<br /><br /><span style="font-size:9pt;">send message</span></a></td>';
      $body .= '<td valign="top">' . $resolved[1]['descr'];
      $body .= '</td></tr></table>';
    }

    if( $totalRows > $resultsToShow )
      $body .= '<br><a style="text-decoration: none; color: #326798;" href="http://www.salthub.com">View ' . ($totalRows - $resultsToShow) . ' other Profile Updates</a>';

    $body .= '</td></tr>';

    return $body;
  }
  else
    return null;
}

function getVesselUpdates()
{
  global $API;
  global $url;
  global $startTime;

  $pages = array();

  $q = mysql_query( "select gid from page_members where uid='" . $API->uid . "'" );
  while( $r = mysql_fetch_array( $q ) ){
    $pages[] = $r['gid'];
  }
    if (empty($pages)){
        $pages = 0;
    }
    else{
        $pages = implode( $pages, "," );
    }
  $resultsToShow = 8;

  $sql = "select SQL_CALC_FOUND_ROWS * from feed where gid IN (" . $pages . ") and type like 'VP%' and ts>='$startTime' order by rand() limit $resultsToShow";

  $q = mysql_query( $sql );
    if ($q === false){
        echo $sql;
        var_dump($q);
        exit;
    }
  if( mysql_num_rows( $q ) )
  {
    $body = "";
    $totalRows = quickQuery( "select FOUND_ROWS()" );
//AppDragon: duplicate elimination
    $previousGname      = null;
    $previousEmailDesc  = null;
//
    while( $r = mysql_fetch_array( $q ) )
    {
        $page = $API->getPageInfo( $r['gid'], true );
        $pageURL = $url . $page['url'];
        $photoURL = $url . '/img/48x48' . $API->getUserPic( null, $page['pid'] );
        $resolved = resolveFeed( $r );
//AppDragon: duplicate elimination
        if($page['gname']==$previousGname && $resolved[1]['emaildesc']==$previousEmailDesc)
        {
            $previousGname      = $page['gname'];
            $previousEmailDesc  = $resolved[1]['emaildesc'];
            continue;
        }
        $previousGname          = $page['gname'];
        $previousEmailDesc      = $resolved[1]['emaildesc'];
//

      if( $resolved[1]['emaildesc'] == "" ) continue;

      $resolved[1]['emaildesc'] = str_replace( 'href="', 'style="text-decoration:none; color: #326798;" href="' . $url, $resolved[1]['emaildesc'] );

      $body .= '<tr><td>
        <table width="600" cellpadding="3" cellspacing="0" style="font-family:Arial; border-bottom: 1px solid #d8dfea; font-size:9pt;">
        <tr>
          <td valign="top" width="50">';

      $body .= '<a style="text-decoration: none; color: #326798;" href="' . $pageURL . '">
';
      $body .= '<img src="' . $photoURL . '" width="48" height="48"></a></td>
';
      $body .= '<td width="100" valign="top"><a style="text-decoration:none; color:#326798;" href="' . $pageURL . '">
';
      $body .= $page['gname'] . '</a></td>
';
      $body .= '<td valign="top">' . $resolved[1]['emaildesc'] . "</td>
";
      $body .= '<td width="120" align="right" valign="top">
';
      $body .= '<a style="text-decoration: none; color: #326798;" href="' . $pageURL . '?addfriends">Share Update</a>
';
      $body .= '</td></tr></table>
';
    }

    if( $totalRows > $resultsToShow )
    {
      $body .= '<br><a style="text-decoration: none; color: #326798;" href="http://www.salthub.com">';
      $body .= 'View ' . ($totalRows - $resultsToShow) . ' other Vessel Updates</a>';
    }

    $body .= '</td></tr>';

    return $body;
  }
  else
    return null;
}

function getConnectionUpdates()
{
  global $API;
  global $friends;
  global $url;
  global $startTime;

  $unique_target = array();

  $resultsToShow = 4;
  $uid = $API->uid;
  $sql = "select SQL_CALC_FOUND_ROWS * from feed where (uid IN (" . $friends. ") or uid_by IN (" . $friends . ")) AND uid!='$uid' AND uid_by!='$uid' and type='F' and ts>='$startTime' limit $resultsToShow";

  $q = mysql_query( $sql );

  if( mysql_num_rows( $q ) )
  {
    $body = "";
    $totalRows = quickQuery( "select FOUND_ROWS()" );

    while( $r = mysql_fetch_array( $q ) )
    {
      if( in_array( $r['uid'], $unique_target ) ) //Skip this entry if we've already seen this user in the list.
        continue;

      $unique_target[] = $r['uid'];

      $user = $API->getUserInfo( $r['uid_by'], "name,pic,uid" );
      $target = $API->getUserInfo( $r['uid'], "name,pic,uid" );

      //This is the query for all connections that the user has
      //$numConnections = quickQuery( "select count(*) from friends where (id1='" . $user['uid'] . "' or id2='" . $user['uid'] . "') and STATUS=1" );

      $targetUID = $r['uid'];
      $numConnections = quickQuery( "select count(*) from feed where (uid='$targetUID' or uid_by='$targetUID') and type='F' and ts>='$startTime'" );

      $userURL = $url . $API->getProfileURL( $r['uid_by'] );
      $targetURL = $url . $API->getProfileURL( $r['uid'] );

      $photoURL = $url . '/img/48x48' . $API->getUserPic( null, $user['pic'] );
      $targetPhotoURL = $url . '/img/48x48' . $API->getUserPic( null, $target['pic'] );

      if( $photoURL == $url."/img/48x48/images/nouser.jpg" ) $photoURL = $url."/images/nouser.jpg";
      if( $targetPhotoURL == $url."/img/48x48/images/nouser.jpg" ) $targetPhotoURL = $url."/images/nouser.jpg";

      $body .= '<tr><td>
        <table width="600" cellpadding="3" cellspacing="0" style="font-family:Arial; border-bottom: 1px solid #d8dfea; font-size:9pt;">
        <tr>
          ';

      $body .= '<td width="50"><a style="text-decoration: none; color: #326798;" href="' . $userURL . '/message"><img src="' . $photoURL . '" width="48" height="48"></a></td>';
      $body .= '<td width="25" align="center"><img src="' . $url . '/images/arrow_refresh_small.png" width="16" height="16"></td>';
      $body .= '<td width="70"><a style="text-decoration: none; color: #326798;" href="' . $targetURL . '"><img src="' . $targetPhotoURL . '" width="48" height="48"></a></td>';

      $body .= '<td valign="top"><a style="text-decoration: none; color: #326798;" href="' . $userURL . '">' . $user['name'] . '</a> is now connected with <a style="text-decoration: none; color: #326798;" href="' . $targetURL . '">' . $target['name'] . '</a>';
      if( $numConnections > 2 ) $body .= ' and <a style="color:#326798;" href="' . $userURL . '/connect">' . ($numConnections - 1) . '</a> other people';
      $body .= '</td>';
      $body .= '</tr></table>';
    }

    if( $totalRows > $resultsToShow )
      $body .= '<br><a style="text-decoration: none; color: #326798;" href="http://www.salthub.com">View ' . ($totalRows - $resultsToShow) . ' other Connection Updates</a>';

    $body .= '</td></tr>';

    return $body;
  }
  else
    return null;
}

function getPostUpdates()
{
  global $API;
  global $friends;
  global $url;
  global $startTime;

  $resultsToShow = 6;

  $types = array( "W", "P", "V", "C", "T", "t" );

  $sql = "select SQL_CALC_FOUND_ROWS f.fid,f.type,f.link,f.ts,f.uid,f.uid_by,f.gid,f.link as id from feed as f where uid_by IN (" . $friends . ") and type IN ('" . implode( $types, "','" ) . "') and ts>='$startTime' order by rand() limit $resultsToShow";

  $q = mysql_query( $sql );

  if( mysql_num_rows( $q ) )
  {
    $body = "";
    $totalRows = quickQuery( "select FOUND_ROWS()" );

    while( $r = mysql_fetch_array( $q ) )
    {
      $user = $API->getUserInfo( $r['uid_by'], "name,pic,uid" );
      $userURL = $url . $API->getProfileURL( $r['uid_by'] );
      $photoURL = $url . '/img/48x48' . $API->getUserPic( null, $user['pic'] );
      if( $photoURL == $url."/img/48x48/images/nouser.jpg" ) $photoURL = $url."/images/nouser.jpg";

      $html = feed_to_html( $r );
      if( $html == "" ) continue;

      $html = str_replace( 'href="', 'style="text-decoration:none; color: #326798;" href="' . $url, $html );

      $body .= '<tr><td>
        <table width="600" cellpadding="3" cellspacing="0" style="font-family:Arial; border-bottom: 1px solid #d8dfea; font-size:9pt;">
        <tr>
          <td width="50">';

      $body .= '<a style="text-decoration: none; color: #326798;" href="' . $userURL . '"><img src="' . $photoURL . '" width="48" height="48"></a></td>';
      $body .= '<td width="100"><a style="text-decoration: none; color: #326798;" href="' . $userURL . '/message">' . $user['name'] . '<br /><br /><span style="font-size:9pt;">send message</span></a></td>';
      $body .= '<td valign="top">';

      $body .= $html;

      $body .= '</td></tr></table>';
    }

    if( $totalRows > $resultsToShow )
      $body .= '<br><a style="text-decoration: none; color: #326798;" href="http://www.salthub.com">View ' . ($totalRows - $resultsToShow) . ' other Posts</a>';

    $body .= '</td></tr>';

    return $body;
  }
  else
    return NULL;
}


function tableHeader( $title, $tip = null )
{
  global $url;
  $body = '
    <table width="600" cellpadding="5" cellspacing="0" style="font-family:Arial;">
		<tr valign="top">
      <td>

  		<table width="600" cellpadding="0" cellspacing="0" style="border-bottom: 1px solid #d8dfea; font-size:9pt;">
  			<tr>
  				<td style="padding-top: 5px; color:#555; font-weight:bold; width:165px; font-size:11pt; font-family:Arial;">' . $title . '
          </td>';

  if( $tip != null )
    $body .= '<td style="padding-top: 5px; color:#555; font-size:8pt;">' . $tip . '</td>';
  else
    $body .= '<td style="padding-top: 5px; color:#555; font-weight:bold;"><a style="text-decoration: none; color: #326798;" href="' . $url . '">view all updates</a></td>';

  $body .= '
  			</tr>
  		</table>

      </td>
    </tr>
  ';
  return $body;
}

function tableFooter()
{
  return "</table><br />";
}


mysql_close();
?>
