<?php
/*
Sends out the weekly page summary email

Users will not receive this email if there are no updates to the pages that they're in.
*/


$_SERVER['DOCUMENT_ROOT'] = "/var/www/salthub.com/public_html";

include $_SERVER['DOCUMENT_ROOT'] . "/inc/inc.php";
error_reporting(ERROR_SET_1);

$site = "s";
$_SERVER['HTTP_HOST'] = "www." . $siteName . ".com";


$x = mysql_query("select * from users where active=1");
//$x = mysql_query("select * from users where uid=3082");

while ($r = mysql_fetch_array($x, MYSQL_ASSOC))
{
  $API->uid = $r['uid'];

  foreach (array_keys($_SESSION) as $k)
  	unset($_SESSION[$k]);

  foreach ($r as $k => $v)
  	$_SESSION[$k] = $v;

	$API->name = $_SESSION['name'];
	$API->pic = $_SESSION['pic'];
	$API->username = $_SESSION['username'];

  $name = $r['name'];
  $name = strtok( $name, " " );

  if( !$API->isNotificationEnabled(  $r['uid'], NOTIFY_PREF_WEEKLY_PAGES_SUMMARY ) ) continue;

  $body = "<div style=\"font-family:Arial\">Hi " . $name . ",<br /><br />There is new activity you may have missed on the Pages you're connected to on SaltHub. Activity includes new comments, replies, media, job postings and more.<br /><br />";
  $body .= '[ORANGEBOX]<b>To connect, follow this link to your account or log in with Facebook or Twitter. <a href="http://www.salthub.com">http://www.SaltHub.com</a></b>[/ORANGEBOX]<br />';

  $q2 = mysql_query( "select from_uid, id from notifications where uid=" . $r['uid'] . " AND ts>=DATE_ADD( DATE(NOW()), INTERVAL -7 DAY ) AND (SUBSTRING(type,1,1)='G' OR SUBSTRING(type,1,2)='VP')" );

  $total_notify_cnt = 0;
  $total_from_friends = 0;
  $notifications = array();
  $from_friends = array();

  if( mysql_num_rows( $q2 ) )
  {
    $friends = getFriends( $r['uid'] );

    while( $r2 = mysql_fetch_array( $q2 ) )
    {
      $gid = $r2['id'];

      if( empty( $notifications[ $gid ] ) )
        $notifications[ $gid ] = 1;
      else
        $notifications[ $gid ]++;

      if( in_array( $r2['from_uid'], $friends ) || $r2['from_uid'] == 0 )
      {
        if( empty( $from_friends[ $gid ] ) )
          $from_friends[ $gid ] = 1;
        else
          $from_friends[ $gid ]++;

        $total_from_friends++;
      }

      $total_notify_cnt++;
    }
  }

  if( $total_notify_cnt > 0 )
  {
    arsort( $notifications );

    $c = 0;
    foreach( $notifications as $gid => $notify_cnt )
    {
      if( quickQuery( "select COUNT(*) from pages where gid='$gid'" ) == 0 )
        continue;

      $data = $API->getPageInfo( $gid );
      $img = "http://www." . $siteName . ".com/img/48x48" . $data['profile_pic'];
      $url = "http://www." . $siteName . ".com" . $data['url'] . "/logbook";
      $job_url = "http://www." . $siteName . ".com/employment";
      $job_postings = quickQuery( "select count(*) from jobs where gid='$gid' and complete=2 and created>=DATE_ADD( DATE(NOW()), INTERVAL -7 DAY )" );

      $body .= '
          <table width="600" cellpadding="5" cellspacing="0">
						<tr valign="top">
							<td width="57" valign="top">
								<a href="' . $url . '" style="text-decoration: none; color: #326798;"><img width="48px" height="48px" src="' . $img . '" style="margin-top:15px; border: 0;" alt="" /></a>
							</td>
							<td>

								<table width="540" cellpadding="0" cellspacing="0" style="border-bottom: 1px solid #d8dfea;">
									<tr>
										<td style="padding-top: 5px;"><a href="' . $url . '" style="text-decoration: none; color: #326798;">' . $data['gname'] . '</a> Category: <span style="color:#326798">' . $data['catname'] . '</span></a></td>
									</tr>
								</table>

								<div style="padding-left: 5px; padding-top: 7px; font-size: 10pt; color: #000; width: 625px; font-family: arial;">' . $notify_cnt . ' new <a href="' . $url . '?notifications" style="text-decoration: none; color: #326798;">' . plural2( $notify_cnt, "notification" ) . '</a> in the past week. ';
      if( @$from_friends[$gid] > 0 ) $body .= $from_friends[$gid] . ' from your <a href="' . $url . '" style="text-decoration: none; color: #326798;">connections</a>.';
      if( $job_postings > 0 ) $body .= "<br />" . $job_postings . ' new <a href="' . $job_url . '" style="text-decoration: none; color: #326798;">' . plural2( $job_postings, "job posting" ) . "</a>";

      $body .= '<br /><br /></div>
								<div style="clear: both;"></div>
							</td>
						</tr>
					</table>
        ';

      $c++;
      if( $c >= 4 ) break;
    }

    $pages = getPages( $r['uid'] );
    $pages[]=0;
    $num_jobs = quickQuery( "select count(*) from jobs where gid IN (" . implode( $pages, "," ) . ") and complete=2" );
    $profile_url = "http://www.salthub.com" . $API->getProfileURL();

    $body .= '<br /><br /><span style="color:#326798"><b>Pages Summary:</b></span><br />You belong to ' . sizeof( $pages ) . ' <a href="http://www.salthub.com/pages" style="text-decoration: none; color: #326798;">' . plural2( sizeof( $pages ), "page" ) . '</a> with ' . $total_notify_cnt . ' <a href="'. $profile_url . '?notifications">' . plural2( $total_notify_cnt, "notification" ) . ".</a>";
    $body .= ' ' . $total_from_friends . ' from your <a href="' . $profile_url . '?connections">Connections</span>.<br />';
    $body .= 'There are ' . $num_jobs . ' <a href="http://www.salthub.com/employment" style="text-decoration: none; color: #326798;">Job Postings</a> in pages you\'re connected to.<br />';

//    $body .= "<br /><br />About SaltHub.com: SaltHub is a dynamic and collaborative utility consisting of many feature sets and tools for Professional Mariners and Maritime Businesses. The site has been designed to bring like minded professionals together, while allowing users to freely move and navigate the site across 9 maritime industries.<br /><br />Thanks,<br />The SaltHub Team<br /></div>";
    $body .= "<br /><br />About SaltHub.com: SaltHub is a network for the maritime industry that provides access to resources, vessels, media, news and tools. Login to SaltHub and connect with professionals, businesses and explore new opportunities.<br /><br />Thanks,<br />The SaltHub Team<br /></div>";

//    echo $body;

    if( $r['email'] != "" )
    {
	if ($argv[1])
		echo "Sending email to: " . $r['name'] . ' ' . $r['uid'] . " (" . $r['email'] . ")\n";
	else
		emailAddress( $r['email'], "Weekly Page Activity on SaltHub", $body );
    }
  }

}

function getPages( $uid )
{
  $pages = array();

  $x = sql_query("select gid from page_members where uid='" . $uid . "'");

  while ($y = mysql_fetch_array($x, MYSQL_ASSOC))
    $pages[] = $y['gid'];

  return $pages;
}

function getFriends( $uid )
{
  $friends = array();

  $x = sql_query("select if(id1=$uid,id2,id1) as uid from friends where (id1=$uid or id2=$uid) and status=1");

  while ($y = mysql_fetch_array($x, MYSQL_ASSOC))
    $friends[] = $y['uid'];

  return $friends;
}

mysql_close();
?>
