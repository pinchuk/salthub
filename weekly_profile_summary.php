<?php
/*
Sends out the weekly profile summary email.
*/


$_SERVER['DOCUMENT_ROOT'] = "/var/www/salthub.com/public_html";
include $_SERVER['DOCUMENT_ROOT'] . "/inc/inc.php";
error_reporting(ERROR_SET_1);

$site = "s";
$_SERVER['HTTP_HOST'] = "www." . $siteName . ".com";

$suggestions_quiet = true;
$pymk_quiet = true;

include_once 'public_html/getpymk.php';
include_once 'public_html/get_suggestions.php';

$job_postings = quickQuery( "select count(*) from jobs where complete=2 and created>=DATE_ADD( DATE(NOW()), INTERVAL -7 DAY )" );

$x = mysql_query("select * from users where active=1");
//$x = mysql_query("select * from users where uid=9581 and active=1 limit 1");

while ($r = mysql_fetch_array($x, MYSQL_ASSOC))
{
  $API->uid = $r['uid'];

  foreach (array_keys($_SESSION) as $k)
  	unset($_SESSION[$k]);

  foreach ($r as $k => $v)
  	$_SESSION[$k] = $v;

	$API->name = $_SESSION['name'];
	$API->pic = $_SESSION['pic'];
	$API->username = $_SESSION['username'];

  //Load blocked uids
  $blocked_uids = array();
  $q2 = mysql_query( "select blocked_uid from blocked_users where uid='" . $API->uid . "'" );
  while( $r2 = mysql_fetch_array( $q2 ) )
    $blocked_uids[] = $r2['blocked_uid'];

  if( !$API->isNotificationEnabled(  $r['uid'], NOTIFY_PREF_WEEKLY_PROFILE_SUMMARY ) ) continue;

  $name = $r['name'];
  $name = strtok( $name, " " );

  $url = "http://www.salthub.com";
  $body = "<div style=\"font-family:Arial\">Hi " . $name . ",<br /><br />There is new activity you may have missed on SaltHub. Activity includes new notifications, suggested connections, pending connection requests and more.<br /><br />";
  $body .= '[ORANGEBOX]<b>To connect, follow this link to your account or log in with Facebook or Twitter. <a href="http://www.salthub.com">http://www.SaltHub.com</a></b>[/ORANGEBOX]<br />';

  $q2 = mysql_query( "select from_uid, id from notifications where uid=" . $r['uid'] . " and ts>=DATE_ADD( DATE(NOW()), INTERVAL -7 DAY )" );

  $total_notify_cnt = 0;
  $total_from_friends = 0;
  $notifications = array();
  $from_friends = array();


  if( mysql_num_rows( $q2 ) )
  {
    $friends = getFriends( $r['uid'] );

    while( $r2 = mysql_fetch_array( $q2 ) )
    {
      $gid = $r2['id'];

      if( empty( $notifications[ $gid ] ) )
        $notifications[ $gid ] = 1;
      else
        $notifications[ $gid ]++;

      if( in_array( $r2['from_uid'], $friends ) || $r2['from_uid'] == 0 )
      {
        if( empty( $from_friends[ $gid ] ) )
          $from_friends[ $gid ] = 1;
        else
          $from_friends[ $gid ]++;

        $total_from_friends++;
      }

      $total_notify_cnt++;
    }
  }

  $pending_connections = quickQuery( "select count(*) from friends where id2='" . $API->uid . "' and status=0" );
  $unread_mail = $API->getUnreadMessageCount();//quickQuery( "select count(*) from messages where uid_to='" . $API->uid . "' and unreadByRecv=1 and delByRecv=0" );

  if( $total_notify_cnt == 0 ) $total_notify_cnt = "no";
  if( $total_from_friends == 0 ) $total_from_friends = "and none";

  $profile_url = $url . $API->getProfileURL();


  $body .= '

    <table width="600" cellpadding="5" cellspacing="0">
		<tr valign="top">
      <td>

  		<table width="600" cellpadding="0" cellspacing="0" style="border-bottom: 1px solid #d8dfea;">
  			<tr>
  				<td style="padding-top: 5px; color:#555; font-weight:bold;">
            Recent Activity
          </td>
  			</tr>
  		</table>

      </td>
    </tr>

    <tr>
			<td valign="top">
        You have ' . $total_notify_cnt . ' new <a style="text-decoration: none; color: #326798;" href="' . $profile_url . '?notifications">notifications</a> in the last week, ' . $total_from_friends . ' from your connections.';

    if( $pending_connections > 0 )
      $body .= '<br /><br />You have ' . $pending_connections . ' <a style="text-decoration: none; color: #326798;" href="http://www.salthub.com">pending connection ' . plural2( $pending_connections, "request" ) . "</a>.";

    if( $unread_mail > 0 )
      $body .= '<br /><br />You have ' . $unread_mail . ' <a style="text-decoration: none; color: #326798;" href="http://www.salthub.com/messaging/inbox.php">unread ' . plural2( $unread_mail, "message" ) . "</a>.";

    if( $job_postings > 0 )
      $body .= '<br /><br />There is ' . $job_postings . ' new <a href="http://www.salthub.com/employment">job ' . plural( $job_postings, "posting" ) . '</a> in the last week.';

    $body .= '
			</td>
  	</tr>

    </table><br />
    ';
/*
    if( $pending_connections > 0 )
    {

      $body .= '
    <table width="600" cellpadding="5" cellspacing="0">
		<tr valign="top">
      <td>

  		<table width="600" cellpadding="0" cellspacing="0" style="border-bottom: 1px solid #d8dfea;">
  			<tr>
  				<td style="padding-top: 5px; color:#555; font-weight:bold;">
            Connection Requests
          </td>
  			</tr>
  		</table>

      </td>
    </tr>

    <tr>
      <td>
        <table cellpadding="0" cellspacing="10">
        <tr>';

    $q2 = mysql_query( "select id1 as uid from friends where id2='" . $API->uid . "' and status=0 limit 3" );
    while( $r2 = mysql_fetch_array( $q2 ) )
    {
      $name = quickQuery( "select name from users where uid=" . $r2['uid'] );
      $img = "http://www.salthub.com/img/48x48" . $API->getUserPic( $r2['uid'] );
      $purl = "http://www.salthub.com" . $API->getProfileUrl( $r2['uid'] );
      $body .= '<td align="center" style="font-size:9pt; "><a style="text-decoration: none; color: #326798;" href="' . $purl . '"><img src="' . $img . '" width="48" height="48"></a></td>';
      $body .= '<td width="100" style="font-size:9pt;color:#555;"><b>' . $name . '</b><br /><a style="text-decoration: none; color: #326798;" href="' . $purl . '">Connect<br />Send message</a></td>';
    }

    $body .= '
        </tr>
        </table>
      </td>
    </td>
    </table><br />';

    }
*/
  $suggestions = getSuggestions( $API->uid );
  
    if( sizeof( $suggestions ) > 0 )
    {
      $body .= '
    <table width="600" cellpadding="5" cellspacing="0">
		<tr valign="top">
      <td>

  		<table width="600" cellpadding="0" cellspacing="0" style="border-bottom: 1px solid #d8dfea;">
  			<tr>
  				<td style="padding-top: 5px; color:#555; font-weight:bold;">
            Suggested Connections
          </td>
  			</tr>
  		</table>

      </td>
    </tr>

    <tr>
      <td>
        <table cellpadding="0" cellspacing="10">
        <tr>';

    for( $c = 0; $c < sizeof( $suggestions ) && $c < 3; $c++ )
    {
      $name = quickQuery( "select name from users where uid=" . $suggestions[$c] );
      if( $name == "" ) continue;
      $img = "http://www.salthub.com/img/48x48" . $API->getUserPic(  $suggestions[$c] );
      $purl = "http://www.salthub.com" . $API->getProfileUrl(  $suggestions[$c] );
      $body .= '<td align="center" style="font-size:9pt; ">
<a style="text-decoration: none; color: #326798;" href="' . $purl . '">
  <img src="' . $img . '" width="48" height="48">
</a>
</td>';
      $body .= '<td width="100" style="font-size:9pt;color:#555;">
<b>' . $name . '</b><br />
<a style="text-decoration: none; color: #326798;" href="' . $purl . '">Connect</a><br />
<a style="text-decoration: none; color: #326798;" href="' . $purl . '/message">Send message</a></td>';
      if( $c == 2 ) $body .= "</tr><tr>";
    }

    $body .= '
        </tr>
        </table>
      </td>
    </td>
    </table><br />';

    }

  $pymk = getPYMK( $API->uid );

    if( sizeof( $pymk ) > 0 )
    {
      $body .= '
    <table width="600" cellpadding="5" cellspacing="0">
		<tr valign="top">
      <td>

  		<table width="600" cellpadding="0" cellspacing="0" style="border-bottom: 1px solid #d8dfea;">
  			<tr>
  				<td style="padding-top: 5px; color:#555; font-weight:bold;">
            People You May Know
          </td>
  			</tr>
  		</table>

      </td>
    </tr>

    <tr>
      <td>
        <table cellpadding="0" cellspacing="10">
        <tr>';

    for( $c = 0; $c < sizeof( $pymk ) && $c < 3; $c++ )
    {
      $name = quickQuery( "select name from users where uid=" . $pymk[$c] );
      $img = "http://www.salthub.com/img/48x48" . $API->getUserPic(  $pymk[$c] );
      $purl = "http://www.salthub.com" . $API->getProfileUrl(  $pymk[$c] );
      $body .= '<td align="center" style="font-size:9pt"><a style="text-decoration: none; color: #326798" href="' . $purl . '"><img src="' . $img . '" width="48" height="48"></a></td>';
      $body .= '<td width="100" style="font-size:9pt;color:#555;"><b>' . $name . '</b><br /><a style="text-decoration: none; color: #326798;" href="' . $purl . '">Connect<br />Send message</a></td>';
      if( $c == 2 ) $body .= "</tr><tr>";
    }

    $body .= '
        </tr>
        </table>
      </td>
    </td>
    </table><br />';

    }


  $body .= getTrendingPages();

  $body .= getTrendingMedia();


  $body .= "<br />About SaltHub.com: SaltHub is a network for the maritime industry that provides access to resources, vessels, media, news and tools. Login to SaltHub and connect with professionals, businesses and explore new opportunities.<br /><br />Thanks,<br />The SaltHub Team<br /></div>";
//  $body .= "<br />About SaltHub.com: SaltHub is a dynamic and collaborative utility consisting of many feature sets and tools for Professional Mariners and Maritime Businesses. The site has been designed to bring like minded professionals together, while allowing users to freely move and navigate the site across 9 maritime industries.<br /><br />Thanks,<br />The SaltHub Team<br /></div>";

  if( $r['email'] != "" )
  {
//    echo $body;
	if ($argv[1])
		echo "Sending email to: " . $r['name'] . " (" . $r['email'] . ")\n";
	else
		emailAddress( $r['email'], "Weekly Profile Activity on " . $siteName, $body );
  }
}

function getPages( $uid )
{
  $pages = array();

  $x = sql_query("select gid from page_members where uid='" . $uid . "'");

  while ($y = mysql_fetch_array($x, MYSQL_ASSOC))
    $pages[] = $y['gid'];

  return $pages;
}

function getFriends( $uid )
{
  $friends = array();

  $x = sql_query("select if(id1=$uid,id2,id1) as uid from friends where (id1=$uid or id2=$uid) and status=1");

  while ($y = mysql_fetch_array($x, MYSQL_ASSOC))
    $friends[] = $y['uid'];

  return $friends;
}

function getSuggestions( $uid )
{
	global $API;
	$API->uid = $uid;
	
/*  $limit = 3;

  $uids = Array();
  $friends = Array($uid);

  $q = sql_query( "select if(id1=" . $uid . ",id2,id1) as uid,status from friends where (" . $uid . ") in (id1,id2)" );
  while( $r = mysql_fetch_array( $q ) )
    if( $r['status'] == 1 )
      $friends[] = $r['uid'];
    else
      $uids[] = $r['uid'];

  $q = sql_query( "select eid from contacts where site='2' and uid='" . $uid . "' and eid" );
  while( $r = mysql_fetch_array( $q ) )
    if( !in_array( $r['eid'], $friends ) && !in_array( $r['eid'], $uids ) )
      $uids[] = $r['eid'];

  $q = sql_query( "select users.uid from contacts left join users on users.email=contacts.eid where contacts.uid='" . $uid . "' and contacts.site > 2" );
  while( $r = mysql_fetch_array( $q ) )
    if( isset( $r['uid'] ) && !in_array( $r['uid'], $friends ) && !in_array( $r['uid'], $uids ))
      $uids[] = $r['uid'];

  global $blocked_uids;
  $uids = array_diff( $uids, $blocked_uids );

  shuffle($uids);

  $uids = array_unique( $uids );*/
  
	$suggestions_quiet = true;
	return doGetSuggestions();
}

function getPYMK( $uid )
{
	global $API;
	$API->uid = $uid;
	
  /*global $API;

  $results = array();

  $limit = 3;

  $friends = $API->getFriendsUids($uid);
  $fof = $API->getFriendsOfFriends(true);

  for( $c = 0; $c < sizeof( $fof ) && $c < $limit; $c++ )
    if( !in_array( $fof[$c], $friends ) )
      $results[] = $fof[$c];

  if( sizeof( $results ) < $limit )
  {
    $limit1 = $limit - sizeof( $results );

    $query = "select uid from users where active=1 and uid != " . $uid . " and pic>0 limit $limit1";
    $x = sql_query($query);

    while ($result = mysql_fetch_array($x, MYSQL_ASSOC))
      $results[] = $result['uid'];
  }

  global $blocked_uids;
  $results = array_diff( $results, $blocked_uids );

  shuffle( $results );

  $results = array_unique( $results );
  
  return $results;*/
  
  $pymk_quiet = true;
  return doGetPYMK();
}

function getTrendingMedia()
{
  global $API;
  $fof = $API->getFriendsOfFriends( true );

  if( !is_array( $fof ) ) $fof = array(0);

  $body = '
  <table width="600" cellpadding="5" cellspacing="0">
  <tr valign="top">
  <td>

  <table width="600" cellpadding="0" cellspacing="0" style="border-bottom: 1px solid #d8dfea;">
  <tr>
  <td style="padding-top: 5px; color:#555; font-weight:bold;">
        Trending Videos on SaltHub
      </td>
  </tr>
  </table>

  </td>
  </tr>

  <tr>
  <td>
    <table cellpadding="0" cellspacing="10">
    <tr>';


  $q2 = mysql_query( "select @type:='V' as type,privacy,reported,media.id,media.uid,title,hash,lastviewed,null as aid from videos as media where uid IN (0," . implode( ",", $fof ) . ") order by lastviewed desc limit 4" );
  if( mysql_num_rows( $q2 ) < 4 )
    $q2 = mysql_query( "select @type:='V' as type,privacy,reported,media.id,media.uid,title,hash,lastviewed,null as aid from videos as media order by lastviewed desc limit 4" );

  while( $y = mysql_fetch_array( $q2 ) )
  {
  	$purl = "http://www.salthub.com" . $API->getMediaURL($y['type'], $y['id'], $y['title']);
	  $img = "http://www.salthub.com" . $API->getThumbURL(1, 100, 75, "/" . typeToWord($y['type']) . "s/" . $y['id'] . "/" . $y['hash'] . ".jpg");
    $title = $y['title'];

    if( $title == "Profile Photos" )
      $title = quickQuery( "select name from users where uid='" . $y['uid'] . "'" );

    $body .= '<td align="center" style="font-size:9pt;" width="125" valign="top">
              ';
    $body .= '<a style="text-decoration: none; color: #326798;" href="' . $purl . '">
              ';
    $body .= '<img src="' . $img . '" width="100" height="75"><br />' . $title . '</a></td>';
  }

  $body .= '
    </tr>
    </table>
  </td>
  </td>
  </table><br />

  <table width="600" cellpadding="5" cellspacing="0">
  <tr valign="top">
  <td>

  <table width="600" cellpadding="0" cellspacing="0" style="border-bottom: 1px solid #d8dfea;">
  <tr>
  <td style="padding-top: 5px; color:#555; font-weight:bold;">
        Trending Photos on SaltHub
      </td>
  </tr>
  </table>

  </td>
  </tr>

  <tr>
  <td>
    <table cellpadding="0" cellspacing="10">
    <tr>';

  $q2 = mysql_query( "select @type:='P' as type,privacy,reported,media.id,media.uid,ptitle as title,hash,lastviewed,aid from photos as media where media.aid!=884372 and uid IN (0," . implode( ",", $fof ) . ") order by lastviewed desc limit 4" );
  if( mysql_num_rows( $q2 ) < 4 )
    $q2 = mysql_query( "select @type:='P' as type,privacy,reported,media.id,media.uid,ptitle as title,hash,lastviewed,aid from photos as media where media.aid!=884372 order by lastviewed desc limit 4" );

  while( $y = mysql_fetch_array( $q2 ) )
  {
	  $img = "http://www.salthub.com" . $API->getThumbURL(1, 100, 75, "/" . typeToWord($y['type']) . "s/" . $y['id'] . "/" . $y['hash'] . ".jpg");

    if( $y['type'] == 'P' && is_null( $y['title'] ) )
    {
      $y['title'] = quickQuery( "select title from albums where id='" . $y['aid'] . "'" );
    }

  	$purl = "http://www.salthub.com" . $API->getMediaURL($y['type'], $y['id'], $y['title']);

    $title = $y['title'];

    $body .= '<td align="center" style="font-size:9pt; " width="125" valign="top">
            ';
    $body .= '<a style="text-decoration: none; color: #326798;" href="' . $purl . '">
            ';
    $body .= '<img src="' . $img . '" width="100" height="75"><br />' . $title . '</a></td>';
  }

  $body .= '
    </tr>
    </table>
  </td>
  </td>
  </table><br />';

  return $body;
}


function getTrendingPages()
{
  global $API;

  $body = '
  <table width="600" cellpadding="5" cellspacing="0">
  <tr valign="top">
  <td>

  <table width="600" cellpadding="0" cellspacing="0" style="border-bottom: 1px solid #d8dfea;">
  <tr>
  <td style="padding-top: 5px; color:#555; font-weight:bold;">
        Trending Vessels on SaltHub
      </td>
  </tr>
  </table>

  </td>
  </tr>

  <tr>
  <td>';

  $fromEmail = true;
  ob_start();

  $type = "V";
  include( $_SERVER['DOCUMENT_ROOT'] . '/get_trending_pages.php' );

  $body .= ob_get_contents();
  ob_end_clean();

  $body .= '
  </td>
  </tr>

  <tr valign="top">
  <td>

  <table width="600" cellpadding="0" cellspacing="0" style="border-bottom: 1px solid #d8dfea;">
  <tr>
  <td style="padding-top: 5px; color:#555; font-weight:bold;">
        Trending Pages on SaltHub
      </td>
  </tr>
  </table>

  </td>
  </tr>

  <tr>
  <td>';

  ob_start();

  $type = "";
  include( $_SERVER['DOCUMENT_ROOT'] . '/get_trending_pages.php' );

  $body .= ob_get_contents();
  ob_end_clean();

  $body .= '
  </td>
  </tr>
  </table>';

  return $body;
}


mysql_close();
?>
